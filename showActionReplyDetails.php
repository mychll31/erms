<? @session_start();
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;

if($_GET['mode']=="actionReplyDetails")
{

	$rsReply = $objDocDetail->checkHistoryActions($_GET["ID"]);
	
	// Display submitted data
	$rsActionTaken = $objDocDetail->getActionTakenDetails($rsReply[0]['actionTakenCodeID']);
	$rsActionRequired = $objDocDetail->getActionRequiredDetails($rsReply[0]['actionCodeId']);
	
	
	$strRecipientUnit=$rsReply[0]['recipientUnit'];
	$strRecipientId = $rsReply[0]['recipientId'];
	
	$strSenderUnit=$rsReply[0]['senderUnit'];
	$strSenderId = $rsReply[0]['senderId'];
	
	
	$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
	$senderName = $objDocDetail->displayRecipientSenderNames($strSenderId, $strSenderUnit);
				
	
	if($rsReply[0]['restricted']==1) 
		$restricted = 'Yes';
	else
		$restricted = 'No'; 
	
	echo '<table width="100%"  border="0">
	  <tr>
		<td width="30%">Action Taken </td>
		<td width="70%">'.$rsActionTaken[0]['actionDesc'].'</td>
	  </tr>
	  <tr>
		<td width="30%">From </td>
		<td width="70%">'.$senderName.'</td>
	  </tr>
	  <tr>
		<td width="30%">To </td>
		<td width="70%">'.$recipientName.'</td>
	  </tr>
	  <tr>
		<td>Action Required </td>
		<td>'.$rsActionRequired[0]['actionDesc'].'</td>
	  </tr>
	  <tr>
		<td>Remarks</td>
		<td>'.$rsReply[0]['remarks'].'</td>
	  </tr>
	  <tr>
		<td>Restricted</td>
		<td>'.$restricted.'</td>
	  </tr>
	</table>';
	
	echo "<script language=\"JavaScript\">
	getData('showAction.php?mode=$mode&docID=$docID&div=$divId&historydiv=$historydiv','$divId');
	if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
	getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
	</script>";
	echo '<br><br>';
}
elseif($_GET['mode']=="actionSourceDetails")
{

	$rsReply = $objDocDetail->getHistoryActionSource($_GET["ID"]);
	
	// Display submitted data
	$rsActionTaken = $objDocDetail->getActionTakenDetails($rsReply[0]['actionTakenCodeID']);
	$rsActionRequired = $objDocDetail->getActionRequiredDetails($rsReply[0]['actionCodeId']);
	
	
	$strRecipientUnit=$rsReply[0]['recipientUnit'];
	$strRecipientId = $rsReply[0]['recipientId'];
	
	$strSenderUnit=$rsReply[0]['senderUnit'];
	$strSenderId = $rsReply[0]['senderId'];
	
	
	$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
	$senderName = $objDocDetail->displayRecipientSenderNames($strSenderId, $strSenderUnit);
				
	
	if($rsReply[0]['restricted']==1) 
		$restricted = 'Yes';
	else
		$restricted = 'No'; 
	
	echo '<table width="100%"  border="0">
	  <tr>
		<td width="30%">Action Taken </td>
		<td width="70%">'.$rsActionTaken[0]['actionDesc'].'</td>
	  </tr>
	  <tr>
		<td width="30%">From </td>
		<td width="70%">'.$senderName.'</td>
	  </tr>
	  <tr>
		<td width="30%">To </td>
		<td width="70%">'.$recipientName.'</td>
	  </tr>
	  <tr>
		<td>Action Required </td>
		<td>'.$rsActionRequired[0]['actionDesc'].'</td>
	  </tr>
	  <tr>
		<td>Remarks</td>
		<td>'.$rsReply[0]['remarks'].'</td>
	  </tr>
	  <tr>
		<td>Restricted</td>
		<td>'.$restricted.'</td>
	  </tr>
	</table>';
	
	echo "<script language=\"JavaScript\">
	getData('showAction.php?mode=$mode&docID=$docID&div=$divId&historydiv=$historydiv','$divId');
	if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
	getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
	</script>";
	
	echo '<br><br>';

}		
?>
