<? @session_start();
$i = 0;
include_once("class/General.class.php");
require_once("class/jqSajax.class.php");
$file=new General();
$ajax=new jqSajax();
$ajax->export("checkDbaseConfig","file->checkDbaseConfig");
$ajax->export("runBackupSched","file->runBackupSched");
$ajax->processClientReq();
?>	
<script type="text/javascript">
<?php $ajax->showJs(); ?>
</script>
<script language="javascript" type="text/javascript"> 
var strSched = '';
var arrSched = new Array();
datePickerController.create();
  jQuery("#backupRefresh").click( function(){ 
	jQuery('#BackupGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#BackupGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['Action','Filename','Date of Backup','Link'],
		colModel:[
			{name:'act',index:'act', width:20,sortable:false, align:'center'},
			{name:'filename',index:'filename', width:65},
			{name:'backupDate',index:'backupDate', width:90},
			{name:'data_link',index:'data_link',width:200, hidden:true}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30],
		imgpath: gridimgpath,
		pager: jQuery('#backupPager'),
		postData:{table:'tblBackup'},
		sortname: '',
		viewrecords: true,
		sortorder: "asc", //desc
		loadComplete: function(){
		var ids = jQuery("#BackupGrid").getDataIDs();
		for(var i=0;i<ids.length;i++){
			var cl = ids[i];
			var ret = jQuery("#BackupGrid").getRowData(cl); 
			ul = "<ul id='icons' class='ui-widget ui-helper-clearfix'>";
			del  = "<li class='ui-state-default ui-corner-all' onclick='Delete("+cl+");'><span class='ui-icon ui-icon-close' title='delete' ></span></li>";
			icon = "<li class='ui-state-default ui-corner-all'><a href='"+ret.data_link+"'><span class='ui-icon ui-icon-link' title='Download Backup' ></span></a></li>";			
			ul_end = "</ul>";
			jQuery("#BackupGrid").setRowData(ids[i],{act:ul+del+icon+ul_end})
		 }	
 	    },
		caption:"Backup Schedule"
	}).navGrid('#backupPager',{edit:false,add:false,del:false});}); 
  });
	
    $('#weekschd').click( function() { 
	 $('#monthctr').attr('disabled','disabled');
	 $('#weeksctr').removeAttr('disabled');
	});
	
    $('#monthschd').click( function() { 
	 $('#weeksctr').attr('disabled','disabled');
	 $('#monthctr').removeAttr('disabled');
	});
	
	$('#chkemail').click( function() {
	 if($('#email').attr('disabled') == true)
	  $('#email').removeAttr('disabled');
	 else
	  $('#email').attr('disabled','disabled');
	});
	
	$('#chkftp').click( function() {
	 if($('#ftp').attr('disabled') == true)
	  $('#ftp').removeAttr('disabled');
	 else
	  $('#ftp').attr('disabled','disabled');
	});
	
	$('#chkfname').click( function() {
	 if($('#fname').attr('disabled') == true)
	  $('#fname').removeAttr('disabled');
	 else
	  $('#fname').attr('disabled','disabled');
	});

	 function AddConfig(oper){
	  var time_internal=0;
	  email = $('#email').val();
	  ftp   = $('#ftp').val();
	  fname = $('#fname').val();
	  dir = "<?= LOCATION ?>UPLOAD/DATABASE_BACKUP/";
	  if($('#weekschd').attr('checked')==true){
	   time_ctr = 'weeks';
	   time_internal =  ($("#weeksctr").val() * 86400); }
	  else if($('#monthschd').attr('checked')==true){
	   time_ctr = 'months';
	   time_internal =  ($("#monthctr").val() * 4.445 * 86400); } 
      $("#OfficeGrid").setPostData({mode:oper,table:'tblBackupConfig',ID:'1',EMAIL:email,FTP:ftp,TIMEINTERNAL:time_internal,TIMECTR:time_ctr,FNAME:fname,DIR:dir,MODULE:'Backup'});
	  $("#OfficeGrid").trigger("reloadGrid"); }

	$('#set_button').click( function() { 
	  if($.x_file_checkDbaseConfig() != '') mode = 'save'; else mode = 'add';
	  AddConfig(mode);
	  LoadForm('view');
	});

	function Edit(){
	 var strRs = new Array();	
	 strRs = $.x_file_checkDbaseConfig();
 	 if(strRs[0][3] != ''){
	  $('#chkemail').click();
	  $('#email').val(strRs[0][3]);}
	 if(strRs[0][2] != ''){
	  $('#chkftp').click();
	  $('#ftp').val(strRs[0][2]);}
	 if(strRs[0][4]){
	  $('#chkfname').click();
	  $('#fname').val(strRs[0][4]);}
	 if(strRs[0][5] == 'weeks'){
	  $('#weekschd').click();
	  $('#optweeks').text(strRs[0][1]/86400); }
	 else{
	  $('#monthschd').click();
	  $('#optmonths').text(Math.round(strRs[0][1]/4.445/86400)); }
	  LoadForm('edit');
	}
	function Delete(id){ 
	jConfirm('Proceed deleting this record?', false, 'Confirmation Dialog', '', function(r) {
	 if(r==true){
	  $("#BackupGrid").setPostData({mode:"del",table:'tblBackup',ID:id});
      $("#BackupGrid").trigger("reloadGrid"); 
	  $('#backupRefresh').click();
	  jAlert('Succesfully deleted', 'Confirmation Results');}
	}); } 


	function switchDisplay(mode)
	{
	 if(mode == 'view'){
	  $('#_schdconf').css('display','none'); 
	  $('#_schdsaved').css('display','block')
	  $('#sched_title').text('Backup Schedule');
	  $('#set_button').css('visibility','hidden');
	  $('#reset').css('visibility','hidden');	  
	  $('#optional').css('display','none');	  
	  $('#grid').css('display','block');
	  $('#backupRefresh').click(); }
	 else{
	  $('#_schdconf').css('display','block'); 
	  $('#_schdsaved').css('display','none'); 
	  $('#set_button').css('visibility','visible');
	  $('#reset').css('visibility','visible');	  
	  $('#optional').css('display','block');	  
	  $('#sched_title').text('Configure Database Backup Schedule'); 
	  $('#grid').css('display','none');}
	}	
	function LoadForm(mode){
	var htm ='',msg='',strSched='';
	 if(mode=='view'){
	   arrSched = $.x_file_checkDbaseConfig();
	   if(arrSched[0][5] == 'weeks'){
	     weekscount = arrSched[0][1]/86400;
		 strSched = weekscount+' times a week'; }
	   else{ 
	     monthscount = arrSched[0][1]/4.445/86400;
	     strSched = monthscount+' times a month';}

		msg = "Database Backup was <a href='#' onclick='Edit()' style='text-decoration:none'><b>scheduled</b></a> to run " + strSched + " or you can run <a href='#' style='text-decoration:none' onclick='$.x_file_runBackupSched(); $(\"#backupRefresh\").click();'><b>Backup now</b></a>"; 
		htm +='<div class="ui-widget" style="width:95%">';
		htm +='<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> ';
		htm +='<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
		htm +='<strong>Notice: </strong>'+msg+'</p>';
		htm +='</div></div><br>';
		$('#_schdsaved').html(htm);
	 	switchDisplay(mode); }
	 else switchDisplay(mode); }
	 if($.x_file_checkDbaseConfig()!='') LoadForm('view');

</script>
<form id="frmBackup" autocomplete="off" method="get">
  <!--DWLayoutTable-->
  <fieldset id="_schdconf" style="border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0">
  <legend id="sched_title">Configure Database Backup Schedule</legend>
  <p>
  <fieldset style="border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0">
  <input name="schd" id="weekschd" type="radio" value="" checked /> week &nbsp;
   <select name="weeksctr" id="weeksctr">
   <? while($i<=5) { ?>
    <option value="<?= $i ?>" id="optweeks"><?= $i ?></option>
   <? $i++; } $i=0; ?>
   </select><span class="required" >&nbsp;&nbsp;schedule per week</span><br />
  <input name="schd" id="monthschd" type="radio" value=""/> month  
   <select name="monthctr" id="monthctr" disabled>
   <? while($i<=5) { ?>
    <option value="<?= $i ?>" id="optmonths"><?= $i ?></option>
   <? $i++; }?>
   </select><span class="required" >&nbsp;&nbsp;schedule per month</span>
   </fieldset>
   <fieldset id="optional" style="border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0">
   <legend>Optional Configuration</legend><p>
   <input name="chkemail" type="checkbox" id="chkemail"/>&nbsp;<input type="text" size="50" name="email" id="email" disabled/><span class="required" >&nbsp;&nbsp;allow to send backup via email</span>
   <br />
   <input name="chkftp" type="checkbox" id="chkftp"/>&nbsp;<input type="text" size="50" name="ftp" id="ftp" disabled/><span class="required" >&nbsp;&nbsp;allow to send backup via ftp </span>
   <br />
   <input name="chkfname" type="checkbox" id="chkfname"/>&nbsp;<input type="text" size="50" name="fname" id="fname" disabled/><span class="required" >&nbsp;&nbsp;filename to be used as backup name </span></p>
   </fieldset>
  <p>
  <input type="button" id="set_button" value="save"/>
  <input type="reset" id="reset" value="reset"/>
  <input type="hidden" id="backupRefresh"/>
  <input type="hidden" id="backupId" value=""></p></p>
  </fieldset>
<div id="_schdsaved"></div>  
<div id="grid"><table id="BackupGrid" class="scroll" cellpadding="0" cellspacing="0"></table></div>
<div id="backupPager" class="scroll" style="text-align:center;"></div>
</form>			