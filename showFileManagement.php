<? 
include_once("class/Incoming.class.php");
include('includes.php'); 
$objIncoming = new Incoming;
$rsFile = $objIncoming->getFiles($_GET['id']); 
if($_GET['mode']=='view')
?>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>File Management</title>
</head>

<body onLoad="showFiles();">
<div class="scrollable">
	<!-- root element for the items -->
	<div class="items">
		  <? for($i=0;$i<sizeof($rsFile);$i++) {
			  if(strchr($rsFile[$i]['filename'], 'pdf')) { ?>
			   <a id="<? echo $rsFile[$i]['fileId']; ?>" title="<? //echo $rsFile[$i]['filename']; ?>" href="pdfReader.php?filename=<? echo $rsFile[$i]['path'].'/'.$rsFile[$i]['filename']; ?>" rel="image">
			    <img src="images/pdf.jpg" class="reflected" style="border: 0px none ; display: block;" />
			   </a>
		  <? } else { 
		  		$src=$rsFile[$i]['path'].'/'.$rsFile[$i]['filename'];
				$rel="image";
		  		if(strchr($rsFile[$i]['filename'], 'doc') || strchr($rsFile[$i]['filename'], 'docx')){
				  $rel = ''; $src = 'images/word.jpg'; }
				if(strchr($rsFile[$i]['filename'], 'xls') || strchr($rsFile[$i]['filename'], 'xlsx')){
				  $rel = ''; $src = 'images/excel.png'; }  
				  if(strchr($rsFile[$i]['filename'], 'rar') || strchr($rsFile[$i]['filename'], 'zip')){
				  $rel = ''; $src = 'images/rar.png'; }  
		  ?>
			  <a id="<? echo $rsFile[$i]['fileId']; ?>" title="<? //echo $rsFile[$i]['filename']; ?>" href="<? echo $rsFile[$i]['path'].'/'.$rsFile[$i]['filename']; ?>" rel="<? echo $rel; ?>" onMouseOver="javascript:window.status = 'ERMS'">
			   <img src="<? echo $src; ?>" class="reflected" style="border: 0px none ; display: block;" />
			  </a>
		  <? }} ?>

	</div>
</div>


<div style="position: absolute; top: -110px; left: -2px; opacity: 1; display: none;" id="tooltip">
	<button type="submit" class="custom low" onClick="removeItem();"><span>remove</span></button>	
</div>
<!-- "next page" action -->

<p style="margin: 15px 0pt 0pt 37px;text-align:center;">
	<button type="submit" class="custom low" onClick="prev();"><span>&lt;&lt; previous</span></button>
	<button type="submit" class="custom low" onClick="addItem('<? echo $t_strNewId; ?>');"><span>add</span></button>
	<button type="submit" class="custom low" onClick="javascript:removeItem();"><span>remove</span></button>	
	<button type="submit" class="custom low" onClick="next();"><span>next &gt;&gt;</span></button>
</p>

<br clear="all">


<!-- overlay element -->
<div class="simple_overlay" id="gallery"><div class="close"></div>
	<a style="opacity: 0.8;" class="prev disabled">prev</a>
	<a style="opacity: 0.8;" class="next">next</a>
	<div style="opacity: 0.8;" class="info"></div>
	<img class="progress" src="img/thumb/loading.gif">
</div>

</body>
</html>
