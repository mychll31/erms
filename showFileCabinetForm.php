<? @session_start();?>
<script language="javascript" type="text/javascript"> 
alert('kiko');
  jQuery("#cabrefresh").click( function(){ 
   if($("#cabedit").val() == 'Save') $("#cabedit").val('Edit');
	jQuery('#CabinetGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#CabinetGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Label', 'Office Code'],
		colModel:[
			{name:'containerId',index:'containerId', width:65},
			{name:'label',index:'label', width:90},
			{name:'officeCode',index:'officeCode', width:100}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#cabpager'),
		postData:{table:'tblContainer'},
		sortname: 'containerId',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"File Cabinet"
	}).navGrid('#pager',{edit:false,add:false,del:false}); }); 
  });

//Edit & Save function	
	jQuery("#acttakenedit").click( function(){ 
	  var id = jQuery("#CabinetGrid").getGridParam('selrow'); 
	  var cabid = $("#cabId").val();
 	  var code = $("#cabLabel").val();
   	  var desc = '<? echo $_SESSION['office']; ?>';

	  if (id) {
	   var ret = jQuery("#CabinetGrid").getRowData(id); 
	   $("#cabId").val(ret.containerId); 	   
	   $("#cabLabel").val(ret.label); 
	
	   if($("#cabedit").val() == 'Save')
	   {
	    $("#cabedit").val('Edit');
	    $("#CabinetGrid").setPostData({mode:"save",table:'tblContainer',ID:cabId,CODE:code,DESC:desc});
	    $("#CabinetGrid").trigger("reloadGrid");
 	    $('#cabclear').click();
		}	   
	   else $("#cabedit").val('Save');
	   }
	   else { alert("Please select row to edit");} 
	}); 
	
//Add function	   
	jQuery("#cabadd").click( function(){ 
	  var cabId = $("#cabId").val();
 	  var label = $("#cabLabel").val();
   	  var ofcode = '<? echo $_SESSION['office']; ?>';
      $("#CabinetGrid").setPostData({mode:"add",table:'tblContainer',ID:cabId,CODE:label,DESC:ofcode});
	  $("#CabinetGrid").trigger("reloadGrid");
  	  $('#cabclear').click();
	}); 
	 
	$("#cabdel").click(function(){ 
	 var id = jQuery("#CabinetGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if( id != ''){ 
	  $("#CabinetGrid").setPostData({mode:"del",table:'tblContainer',ID:id});
	  $("#CabinetGrid").trigger("reloadGrid"); 
	  }
	 else alert("Please Select Row to delete!"); }); 
     $('#cabrefresh').click();
	 
</script>
<div align="center"><? echo $msg;?></div>
<form action="javascript:get(document.getElementById('frmCabinet'),'cabinet','showFileCabinetForm.php');" name="frmCabinet" id="frmCabinet">
<table width="60%" class="textbody">
  <!--DWLayoutTable-->
  <tr> 
    <td width="205" height="21" valign="top">Cabinet Label</td>
    <td width="300" rowspan="2" valign="top"><input type="text" class="caption" id="cabLabel" value="">      <span id="stractTakenCode_div" class="Takenuired"></span></td>
	<td width="1"></td>
	<input type="hidden" class="caption" id="cabId" value="">
  </tr>
  
  
  <tr><td height="26" colspan="3" align="center">
   <input type="button" value="Add" id="cabadd" class="caption" onClick="">
   <input type="button" value="Edit" id="cabedit" onClick="">
   <input type="reset" value="Clear" id="cabclear" class="caption" onClick="">
   <input type="button" value="delete" id="cabdel" onClick="">
   <input type="button" value="Refresh" id="cabrefresh" onClick="">
  </td></tr>
</table>
<table id="CabinetGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="cabpager" class="scroll" style="text-align:center;"></div>
</form>			