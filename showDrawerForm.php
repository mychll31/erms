<? @session_start();?>
<script language="javascript" type="text/javascript"> 
var validator='';

  	jQuery("#drawRefresh").click( function(){ 
    	//if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');
		jQuery('#drawerGrid').GridUnload();
		jQuery(document).ready(function(){
			jQuery("#drawerGrid").jqGrid({
				url:'xmlparser.php?nd='+new Date().getTime(),
				datatype: "xml",
				colNames:['ID','Cabinet ID','Drawer Label'],
				colModel:[
					{name:'container2Id',index:'container2Id', width:70},
					{name:'container',index:'container', width:90, hidden:true},
					{name:'label',index:'label', width:90}
					],
				rowNum:10,
				width: 700,
				rowList:[10,20,30,'All'],
				imgpath: gridimgpath,
				multiselect: true,
				pager: jQuery('#drawPager'),
				postData:{table:'tblContainer2',searchField:'container',searchString:$('#cabinet').val(),searchOn:'true',searchOper:'eq'},
				sortname: 'container2Id',
				viewrecords: true,
				sortorder: "asc", //desc
				caption:"Drawers"
			}).navGrid('#drawPager',{edit:false,add:false,del:false});

		}); 
  	});
  
    $('#drawClear').click( function(){ 
	   $("#drawId").val(''); 	   
	   $("#drawLabel").val(''); 
	   $("#drawSave").css('display','none');
	   $("#drawAdd").css('display','block');
	   $('#Clear_drawer').text(" Clear ");
	   $('#drawRefresh').click();
	   validator.resetForm();
    });	
	
//Edit & Save function	
	jQuery("#drawEdit").click( function(){ 
		var id = jQuery("#drawerGrid").getGridParam('selrow'); 
	
		if (id) 
		{
			var ret = jQuery("#drawerGrid").getRowData(id); 
			$("#drawId").val(ret.container2Id); 	   
			$("#drawLabel").val(ret.label); 
	
			if($("#drawEdit span").attr('class') == "ui-icon ui-icon-pencil")
			{
				$("#drawSave").css('display','block');
				$("#drawAdd").css('display','none');
				$('#Clear_drawer').text(" Cancel ");
			}
		}
	   else 
	   { 
	   		jAlert('Please select row to edit', 'Warning');
		} 
	}); 

	jQuery("#drawSave").click( function(){ 
		var drawId = $("#drawId").val();
 	  	var label = $("#drawLabel").val();
   	  	var cabId = $("#cabinet").val();
	    $("#drawerGrid").setPostData({mode:"save",
									  table:'tblContainer2',
									  ID:drawId,
									  CODE:cabId,
									  DESC:label,
									  MODULE:'Drawer'});
	    $("#drawerGrid").trigger("reloadGrid");
 	    $('#drawClear').click();
	});
	
	jQuery("#drawAdd").click( function(){ 
		$('#frmDrawer').submit();
	});
//Add function	   
	//$("#cabAdd").click( function(){ 
	function AddDrawer()
	{
	  	var drawId  = $("#drawId").val();
 	  	var label  = $("#drawLabel").val();
		var cabId  = $("#cabinet").val();
   	  	$("#drawerGrid").setPostData({mode:"add",table:'tblContainer2',ID:drawId,CODE:cabId,DESC:label,MODULE:'Cabinet'});
	  	$("#drawerGrid").trigger("reloadGrid");
  	  	$('#drawClear').click();
	  	jAlert('Succesfully Added','Information'); 
	}
	 
	 
	$("#drawDel").click(function(){ 
	 	var id = jQuery("#drawerGrid").getGridParam('selarrrow'); //selrow - for 1 row

		if(id!= '')
	 	{ 
  	  		if(id == '1')
			{ 
	   			jAlert('You can not delete this record', 'Warning');
	   			$('#drawClear').click(); }
      		else
			{
		 		jConfirm('Proceed deleting this record?',false, 'ERMS Confirmation Dialog', function(r) {
			 		if(r==true)
					{
			  			$("#drawerGrid").setPostData({mode:"del",table:'tblContainer2',ID:id.toString(),MODULE:'Drawer'});
			  			$("#drawerGrid").trigger("reloadGrid"); 
			  			$('#drawClear').click();
			  			jAlert('Succesfully deleted', 'Confirmation Results'); 
			  		} 
				}); 
			} 
		}
		else jAlert('Please select row to delete', 'Warning'); 
	});

    
	$('#drawRefresh').click();
	

//Form validation
 	$().ready(function() {
	 	validator = $("#frmDrawer").validate({
			rules: { drawLabel: "required" },
			messages: {	drawLabel: "Drawer Label is required" },
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo( element.parent().next().next() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.next() );
				else
					error.appendTo( element.parent().next() );
			},
			submitHandler: function() {	AddDrawer(); },
			success: function(label) { label.html("&nbsp;").addClass("checked"); }
		});
 	});	
	
	
</script>
<form id="frmDrawer" autocomplete="off" method="get">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Drawer Label</td>
    <td class="field"><input type="text" id="drawLabel" name="drawLabel" /></td>
	<td class="status"></td>
	<input type="hidden" id="drawId" value="">
  </tr>

 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="drawEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="drawDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="drawAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="drawSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="drawClear"><span title='Clear'></span><div id="Clear_drawer">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="drawRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
<table id="drawerGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="drawPager" class="scroll" style="text-align:center;"></div>
</form>			