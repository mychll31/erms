<?
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php");
include_once("General.class.php");
include_once("Login.class.php");
class show201 extends Login
{
###########################################
# Function:    getHRMISnames
# Parameters:  None
# Return Type: Array
# Description: returns list of names from HRMIS
###########################################
function getHRMISNames($empNum)
{
$sqlWhere= ($empNum=="")? "":"WHERE empNumber='".$empNum."'";
$sql="SELECT * FROM tblEmpPersonal ".$sqlWhere." ORDER BY surname ASC";
$sql1= new MySQLHandler2();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

function get201List($docId)
{
$sql="SELECT * FROM tbl201Documents WHERE documentId='".$docId."'";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

function add201($docId,$empNumber)
{
$sql="SELECT * FROM tbl201Documents WHERE documentId='".$docId."' AND empNumber='".$empNumber."'";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
if(sizeof($rs)==0)
{
	$sql="INSERT INTO tbl201Documents (documentId, empNumber) VALUES ('".$docId."','".$empNumber."')";
	$sql1= new MySQLHandler();
	$sql1->init();	
	if($rs=$sql1->Insert($sql))
		{
			$this->msg = MSG_ADD_SUCCESS;
			return 1;
		}
		else
		{
			$this->msg = MSG_ADD_UNSUCCESS;
			return 0;
		}
}
else{
	$this->msg='Already Added';
	return 0;
	}

}
function delete201($docId,$empNumber)
{
		$SQL = "DELETE FROM tbl201Documents WHERE documentId = '".$docId."' AND empNumber='".$empNumber."'";
		$sql1= new MySQLHandler();
		$sql1->init();	
		if($rs=$sql1->Delete($SQL))
		{
			$this->msg = MSG_DEL_SUCCESS;
			return 1;
		}
		else
		{
			$this->msg = '<font color="red">'.MSG_DEL_SYS_MALF.'</font>';
			return 0;
		}
		return $rs;


}

}
?>