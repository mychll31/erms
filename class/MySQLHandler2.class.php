<?php
// for HRMIS connection

/** This MySQLHandler2 class is edited by Jing Tuibeo
* adding Sorting function and pagination to the original
* SQL Statements  encapsulated in this 
* class January 3, 2004
/*
 * MySQLHandler2 :: mySQL Wrapper. Version 1.2
 * Copyright (C) 2002-2003 Andreas Norman. All rights reserved.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * This software is FREE FOR NON-COMMERCIAL USE, provided the following 
 * conditions are met:
 * 
 * The user must assume the entire risk of using this program.
 * 
 * You are hereby granted a license to to use and distribute this 
 * program in its original unmodified form and in the original 
 * distribution package. You can make as many copies as you want, 
 * and distribute it via electronic means. There is no fee for
 * any of the above.
 * 
 * You are specifically prohibited from charging, or requesting
 * donations, for any such copies, however made; and from distributing
 * the software and/or documentation with other products (commercial or
 * otherwise) without prior written permission.
 */
include_once("ERMS_define.php");
class MySQLHandler2 {

  // Change these variables to your own database settings
  var $DATABASE = dbhrmis;
  var $USERNAME = hrmis_user;
  var $PASSWORD = hrmis_pass;
  var $SERVER = hrmis_server;
  var $connection_id;
  var $result;
  var $record = array();
  var $LOGFILE = "c:/mysql2.log"; // full path to debug LOGFILE. Use only in debug mode!
  var $LOGGING = false; // debug on or off
  var $SHOW_ERRORS = true; // output errors. true/false
  var $USE_PERMANENT_CONNECTION = false;
  
  // Do not change the variables below
  var $CONNECTION;
  var $FILE_HANDLER;
  var $ERROR_MSG = '';


  function MySQLHandler2() {
  
  }
###########################################
# Function:    init
# Parameters:  N/A
# Return Type: boolean
# Description: initiates the MySQL Handler
###########################################
  function init() {
    $this->logfile_init();
    if ($this->OpenConnection()) {
      return true;
    } else {
      return false;
		}
	}

###########################################
# Function:    OpenConnection
# Parameters:  N/A
# Return Type: boolean
# Description: connects to the database
###########################################
	function OpenConnection()	{
    if ($this->USE_PERMANENT_CONNECTION) {
      $conn = mysql_pconnect($this->SERVER,$this->USERNAME,$this->PASSWORD);
    } else {
      $conn = mysql_connect($this->SERVER,$this->USERNAME,$this->PASSWORD);
    }
		if ((!$conn) || (!mysql_select_db($this->DATABASE,$conn))) {
      $this->ERROR_MSG = "\r\n" . "Unable to connect to database - " . date('H:i:s');
      $this->debug();
      return false;
    } else {
  		$this->CONNECTION = $conn;
	  	return true;
    }
	}

###########################################
# Function:    CloseConnection
# Parameters:  N/A
# Return Type: boolean
# Description: closes connection to the database
###########################################
	function CloseConnection() {
  	if (mysql_close($this->CONNECTION)) {
      return true;
    } else {
      $this->ERROR_MSG = "\r\n" . "Unable to close database connection - " . date('H:i:s');
      $this->debug();
      return false;
    }
	}

###########################################
# Function:    logfile_init
# Parameters:  N/A
# Return Type: N/A
# Description: initiates the logfile
###########################################
	function logfile_init() {
    if ($this->LOGGING) {
      $this->FILE_HANDLER = fopen($this->LOGFILE,'a') ;
  	  $this->debug();
    }
	}
	
###########################################
# Function:    logfile_close
# Parameters:  N/A
# Return Type: N/A
# Description: closes the logfile
###########################################
	function logfile_close() {
    if ($this->LOGGING) {
  		if ($this->FILE_HANDLER) {
  		  fclose($this->FILE_HANDLER) ;
  	  }
    }
	}

###########################################
# Function:    debug
# Parameters:  N/A
# Return Type: N/A
# Description: logs and displays errors
###########################################
  function debug() {
    if ($this->SHOW_ERRORS) {
      echo $this->ERROR_MSG;
    }
    if ($this->LOGGING) {
  		if ($this->FILE_HANDLER) {
  			fwrite($this->FILE_HANDLER,$this->ERROR_MSG);
  		} else {
  			return false;
  		}
    }
	}

###########################################
# Function:    Insert
# Parameters:  sql : string
# Return Type: integer
# Description: executes a INSERT statement and returns the INSERT ID
###########################################
	function Insert($sql) {
	  mysql_query("SET NAMES 'utf8'");
	  mysql_query("SET CHARACTER SET 'utf8'");	  
	  if ((empty($sql)) || (!eregi("^insert",$sql)) || (empty($this->CONNECTION))) {
		$this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> or not an INSERT - " . date('H:i:s');
		$this->debug();
		return false;
	  } else {
		  $conn = $this->CONNECTION;
		  $results = mysql_query($sql,$conn);
			if (!$results) { 
		  $this->ERROR_MSG = "\r\n" . mysql_error()." - " . date('H:i:s').$sql;
		  $this->debug();
		  return false;
		} else {
		  $result = mysql_affected_rows();//mysql_insert_id();
		  return $result;
		}
	  }
	}

###########################################
# Function:    Select
# Parameters:  sql : string
# Return Type: array
# Description: executes a SELECT statement and returns a
#              multidimensional array containing the results
#              array[row][fieldname/fieldindex]
###########################################
	function Select($sql)	{
	  mysql_query("SET NAMES 'utf8'");
	  mysql_query("SET CHARACTER SET 'utf8'");	  
	  if ((empty($sql)) || (!eregi("^select",$sql)) || (empty($this->CONNECTION))) {
		$this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> or not a SELECT - " . date('H:i:s');
		$this->debug();
		$this->CloseConnection();
		return false;
	  } else {
		  $conn = $this->CONNECTION;
		  $results = mysql_query($sql,$conn);
			if ((!$results) || (empty($results))) {
		  $this->ERROR_MSG = "\r\n" . mysql_error()." - " . date('H:i:s').$sql;
		  $this->debug();
		  $this->CloseConnection();
		  return false;
		} else {
		  $i = 0;
		  $data = array();
		  while ($row = mysql_fetch_array($results)) {
			  $data[$i] = $row;
			  $i++;
		  }
		  mysql_free_result($results);
		  $this->CloseConnection();
		  return $data;
		}
	  }
	}

###########################################
# Function:    Update
# Parameters:  sql : string
# Return Type: integer
# Description: executes a UPDATE statement 
#              and returns number of affected rows
###########################################
	function Update($sql)	{
	  mysql_query("SET NAMES 'utf8'");
	  mysql_query("SET CHARACTER SET 'utf8'");	  
		
	  if ((empty($sql)) || (!eregi("^update",$sql)) || (empty($this->CONNECTION))) {
		$this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> or not an UPDATE - " . date('H:i:s');
		$this->debug();
		return false;
	  } else {
		  $conn = $this->CONNECTION;
		  $results = mysql_query($sql,$conn);
			if (!$results) { 
		  $this->ERROR_MSG = "\r\n" . mysql_error()." - " . date('H:i:s');
		  $this->debug();
		  return false;
		} else {
			  return mysql_affected_rows();
		}
	  }
	}
  
###########################################
# Function:    Replace
# Parameters:  sql : string
# Return Type: boolean
# Description: executes a REPLACE statement 
###########################################
	function Replace($sql) {
		if ((empty($sql)) || (!eregi("^replace",$sql)) || (empty($this->CONNECTION))) {
      $this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> or not a REPLACE - " . date('H:i:s');
      $this->debug();
      return false;
    } else {
	  	$conn = $this->CONNECTION;
  		$results = mysql_query($sql,$conn);
		  if (!$results) { 
        $this->ERROR_MSG = "\r\n" . "Error in SQL Statement : ($sql) - " . date('H:i:s');
        $this->debug();
        return false;
      } else {
    		return true;
      }
    }
	}  

###########################################
# Function:    Delete
# Parameters:  sql : string
# Return Type: boolean
# Description: executes a DELETE statement 
###########################################
	function Delete($sql)	{
		if ((empty($sql)) || (!eregi("^delete",$sql)) || (empty($this->CONNECTION))) {
      $this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> or not a DELETE - " . date('H:i:s');
      $this->debug();
      return false;
    } else {
  		$conn = $this->CONNECTION;
	  	$results = mysql_query($sql,$conn);
		  if (!$results) { 
        $this->ERROR_MSG = "\r\n" . mysql_error()." - " . date('H:i:s');
        $this->debug();
        return false;
      } else {
    		return true;
      }
    }
	}
  
###########################################
# Function:    Query
# Parameters:  sql : string
# Return Type: boolean
# Description: executes any SQL Query statement 
###########################################
	function Query($sql)	{
		if ((empty($sql)) || (empty($this->CONNECTION))) {
      $this->ERROR_MSG = "\r\n" . "SQL Statement is <code>null</code> - " . date('H:i:s');
      $this->debug();
      return false;
    } else {
  		$conn = $this->CONNECTION;
	  	$results = mysql_query($sql,$conn);
		  if (!$results) { 
        $this->ERROR_MSG = "\r\n" . mysql_error()." - " . date('H:i:s');
        $this->debug();
        return false;
      } else {
    		return true;
      }
    }
	}
###########################################
# Function:    Sort
# Parameters:  sortv : string, sortdir : string
# Return Type: string
# Description: returns the sorting criteria 
###########################################
	function Sort($sortdir,$jsort)	{
		$mysort=" Order By " . $jsort ." ". $sortdir;
	  /*switch ($sortv)
  {
    case 1:
      if ($sortdir=="DESC")
      {

		$mysort=" Order BY $jsort DESC";
      }
        else
      {

		$mysort=" Order BY $jsort ASC";
      } 

      break;
    case 2:
      if ($sortdir=="DESC")
      {

		$mysort=" Order BY $jsort DESC";
      }
        else
      {

		$mysort=" Order BY $jsort ASC";
      } 

      break;
    case "Sort by Name":
      if ($sortdir=="DESC")
      {

		$mysort=" Order BY Build_Name DESC";
      }
        else
      {

		$mysort=" Order BY Build_Name ASC";
      } 

      break;
    case "Sort by Description":
      if ($sortdir=="DESC")
      {

		$mysort=" Order BY Facility_Desc DESC";
      }
        else
      {

		$mysort=" Order BY Facility_Desc ASC";
      } 

      break;
  } */
		return $mysort;
	}


###########################################
# Function:    Pagination
# Parameters:  totalrows : int,limit : int
# Parameters:  pagenum : int, link : string 
# Return Type: string
# Description: returns the sorting criteria 
###########################################
	function Paginate($totalrows,$limit,$pagenum,$link)	{

	if($pagenum != 1){ 
        $pageprev = $pagenum-1; 
         
   echo("<a href=$link?Id=".$pageprev."><<&nbsp;Previous</a>&nbsp;"); 
    }else{ 
        echo("<<&nbsp;Prev&nbsp;"); 
    } 

    $numofpages = $totalrows / $limit; 
     //echo $totalrows;
    for($i = 1; $i <= $numofpages; $i++)
	{ 
        if($i == $pagenum)
		{ 
            echo("<font color=#ffffff size=3><b>". "</b></font>&nbsp;"); 
        }else{ 
            echo("<a href=$link?Id=".$i.">"."</a>&nbsp;"); 
        } 
    } 


    if(($totalrows % $limit) != 0){ 
        if($i == $pagenum){ 
            echo("<font color=#ffffff size=3><b>". "</b></font>&nbsp;"); 
        }else{ 
            echo("<a href=$link?Id=".$i.">" ."</a>&nbsp;"); 
        } 
    } 

    if(($totalrows - ($limit * $pagenum)) > 0){ 
        $pagenext = $pagenum+1; 
          
        echo("<a href=$link?Id=".$pagenext.">Next>></a>"); 
    }else{ 
        echo("Next>>"); 
    } 
		return ;
	}
function f_ExecuteSql($sql = "")
	{
	$conn = $this->CONNECTION;
		unset($results);

		if ($sql != "")
			$results = mysql_query($sql, $conn);

		if ($results)
		{
			unset($this->record[$results]);
			return $results;
		}
	}
function f_GetSelectedRows($query_id = 0)
	{
		if( !$query_id ) $query_id = $results;

		return ( $query_id ) ? mysql_num_rows($query_id) : false;
	}
function f_GetRecord($query_id = 0)
    {
        if( !$query_id ) $query_id = $results;
        if ($query_id)
        {
         	$this->record = mysql_fetch_assoc($query_id);
            return $this->record;
        }
        else
            return false;
    }
	

	
}
?>
