<?php
define("AGENCY_CODE","CO");
define("AGENCY_NAME","DOST Central Office");
// Start of Office to be displayed
define("OFFICE_START", "exeoffice");
//define("OFFICE_START", "division");
//for section
define("RECORDS_ADMIN_TABLE","tblGroup4"); //table
define("RECORDS_ADMIN_FIELD","group4Custodian"); // table field
define("RECORDS_ADMIN_CODE_FIELD","group4Code");
define("RECORDS_ADMIN_VALUE","RMS");// Code
define("AR_PRINT","OSEC"); // OFFICE THAT ALLOW PRINTING OF AR
$HRMISlink="http://hrmis.dost.gov.ph";
define("OFFICE201","PD");
define("LOCATION","/var/www/erms/");
define("MAIN_OFFICE",1);// 1 for main office 0 for agency
//define for offices server IP
define("HTTP","http://");
define("root","/var/www/");
define("ERMS_SERVER",$_SERVER['SERVER_ADDR']);
define("RMS","10.10.100.15");
define("ITD","10.10.100.30");
define("PES","10.10.100.32");
define("OUSEC","10.10.100.34");
//define("OUSEC","10.10.100.36");
define("ALFA","10.10.100.14");

//connection for ERMS
define("dberms","");
define("erms_user","");
define("erms_pass","");
define("erms_server","");
define("ALLOWEMAIL",1);

//connection for HRMIS
define("dbhrmis","");
define("hrmis_user","");
define("hrmis_pass","");
define("hrmis_server","");

define("Group1","Executive Office");
define("Group2","Service");
define("Group3","Division");
define("Group4","Section");
define("Group5","");
?>