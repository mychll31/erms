<?
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php"); // For HRMIS Connection
include_once("General.class.php");

class DocDetail extends General
{
 
###########################################
# Function:    getDocDetails
# Parameters:  N/A
# Return Type: Array
# Description: returns details of the document
###########################################
function getDocDetails($strDocumentID)
{

	$sql="SELECT tblDocument.*,  tblDocumentType.documentTypeDesc FROM tblDocument 
				INNER JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId  
				WHERE documentId ='$strDocumentID'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getEmployees()
{		
	$sql="SELECT tblEmpPersonal.empNumber, tblEmpPersonal.firstname, tblEmpPersonal.middleInitial, tblEmpPersonal.surname 
		FROM tblEmpPersonal INNER JOIN tblEmpPosition ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber 
		WHERE tblEmpPosition.statusOfAppointment='In-Service' ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getOffices()
{
	$sql="SELECT * FROM tblExeOffice";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getAgencies()
{		
	$sql="SELECT * FROM tblOriginOffice WHERE ownerOffice='".$_SESSION['office']."' ORDER BY officeName";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getOfficeServices($txtOfficeCode)
{
	$sql="SELECT * FROM tblService WHERE eoCode='$txtOfficeCode' ORDER BY serviceName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getServiceDivisions($txtServiceCode)
{
	$sql="SELECT * FROM tblDivision WHERE serviceCode='$txtServiceCode' ORDER BY divisionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getOfficeDivisionsNoService($txtOfficeCode)
{
	$sql="SELECT * FROM tblDivision WHERE eoCode='$txtOfficeCode' AND serviceCode='' ORDER BY divisionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}


function getDivisionSections($txtDivisionCode)
{
	$sql="SELECT * FROM tblSection WHERE divisionCode='$txtDivisionCode' ORDER BY sectionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getDivisions()
{
	$sql="SELECT * FROM tblDivision ORDER BY divisionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionRequired()
{
	$sql="SELECT * FROM tblActionRequired ORDER BY actionDesc";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionRequiredDetails($intActionCodeId)
{
	$sql="SELECT * FROM tblActionRequired WHERE actionCodeId='".$intActionCodeId."'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionTaken()
{
	$sql="SELECT * FROM tblActionTaken ORDER BY actionDesc";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionTakenDetails($intActionCodeId)
{
	$sql="SELECT * FROM tblActionTaken WHERE actionCodeId='".$intActionCodeId."'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getOfficeDivisions($txtOfficeCode)
{
	$sql="SELECT * FROM tblDivision WHERE eoCode='$txtOfficeCode' ORDER BY divisionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function sendEmail($keyID)
{
 $con = new MySQLHandler();
 $con->init();
 $rs=$con->Select("Select * from tblHistory where historyId LIKE '$keyID'");
 $strEadd = $this->getEmailAdd($rs[0]['recipientUnit'],$rs[0]['restricted'],$rs[0]['recipientId']);
  foreach($strEadd as $email)
   $this->xmail($email,$rs[0]['senderId'], 'Sample email', 'THIS IS ERMS Sample EMAIL', 'NOREPLY.txt', $backup_type='', $newline='');
}


function addAction($arrFields)
{
	$intActionUnitSwitch = $arrFields['actionUnitSwitch'];
	
	// user class is 
	/*
		record=1
		custodian=2
		employee=3
	*/	
	// addSource = 1 = action reply is added by recipient, 2 = action reply is added by sender also
	
	if($arrFields['addSource']==1)
	{
	
		if($_SESSION['userType']=='1' || $_SESSION['userType']=='2')
		{
			$strSender = $_SESSION['office'];
			$strSenderUnit =  $_SESSION['userUnit'];
			
		}	
		elseif($_SESSION['userType']=='3')
		{
			$strSender = $_SESSION['userID'];
			$strSenderUnit = "employee";
		}	
	
	}
	else
	{
		$strSender = $arrFields['sender'];
		$strSenderUnit = $arrFields['senderUnit'];
	}
	
	if($intActionUnitSwitch==2) // if office
	{
		list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields['cmbOfficeCode']);

	}elseif($intActionUnitSwitch==1) // if employee
	{
		$strRecipientUnit="employee";
		$strRecipientId = $arrFields['cmbEmpNumber'];
	}elseif($intActionUnitSwitch==3) // if agency
	{
		$strRecipientUnit="agency";
		$strRecipientId = $arrFields['cmbAgency'];
	}
	
	if($arrFields['cbRestricted']=="")
		$intRestricted = 0;
	else
		$intRestricted = 1;
/*
	$SQL = "INSERT INTO tblHistory (documentId, fromRecipient, fromDate, toRecipient, toUnit, toDate, remarks, actionCodeId, addedById, dateAdded) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$strFromRecipient."', NOW(), '".$strToRecipient."', '".$strActionUnit."', NOW(), '".$arrFields['txtRemarks']."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW())";
*/		
	$SQL = "INSERT INTO tblHistory (documentId, referenceId, senderId, senderUnit, actionTakenCodeID, dateSent, recipientId, recipientUnit, remarks, actionCodeId, addedById, dateAdded, restricted) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$arrFields['txtReferenceID']."', '".$strSender."', '".$strSenderUnit."', '".$arrFields['cmbActionTaken']."', NOW(), '".$strRecipientId."', '".$strRecipientUnit."', '".$arrFields['txtRemarks']."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW(),'".$intRestricted."')";

			$sql1= new MySQLHandler();
			$sql1->init();	
			
			if(mysql_query($SQL))
			{
				$this->msg = MSG_ADD_SUCCESS;
				return mysql_insert_id(); 
			}
			else
			{
				$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'</font>';
				return 0;
			}

}

	
	
	function showActions($strDocumentID)
	{
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['office']."' ORDER BY tblHistory.dateSent";
		elseif($_SESSION['userType']==3) // employee
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['userID']."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function displayRecipientSenderNames($strRecipientSenderId, $strRecipientSenderUnit) 
	{
		if($strRecipientSenderUnit=="employee") // if recipient unit is employee
		{
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}	
		elseif($strRecipientSenderUnit=="exeoffice") // if recipient unit is exeoffice
		{
			$SQL = "SELECT eoName as name FROM tblExeOffice WHERE eoCode='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}		
		elseif($strRecipientSenderUnit=="service") // if recipient unit is service
		{
			$SQL = "SELECT serviceName as name FROM tblService WHERE serviceCode='".$strRecipientSenderId."'";	
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}		
		elseif($strRecipientSenderUnit=="division") // if recipient unit is division
		{
			$SQL = "SELECT divisionName as name FROM tblDivision WHERE divisionCode='".$strRecipientSenderId."'";		
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}	
		elseif($strRecipientSenderUnit=="section") // if recipient unit is section
		{
			$SQL = "SELECT sectionName as name FROM tblSection WHERE sectionCode='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}		
		elseif($strRecipientSenderUnit=="agency") // if recipient unit is agency
		{	
			$SQL = "SELECT officeName as name FROM tblOriginOffice WHERE originId='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			
		}		
			

		if($strRecipientSenderUnit=="employee")
		{
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		}elseif($strRecipientSenderUnit==$strRecipientSenderId) // for sender info, if sender is not employee
		{
			$strName =$strRecipientSenderId;
		}
		else
			$strName = $rs[0]['name'];
		
		return $strName;
	}
	
	function getEmployeeName($strEmpNumber) 
	{
		$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strEmpNumber."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
		$rs=$sql1->Select($SQL);

		$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
		$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		
		return $strName;		
	}
	function getCurrentReference($strDocumentID)
	{
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' ORDER BY tblHistory.dateSent DESC";

		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getInitialReference($strDocumentID)
	{
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' ORDER BY tblHistory.dateSent ASC";

		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkSenderUnit($strSenderUnit)
	{
		
		if($rsSenderUnit = $this->checkIfExeOffice($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfService($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfDivision($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfSection($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];

		return $senderUnit;
	}
	
	
	function checkIfExeOffice($strSenderUnit)
	{
		$SQL = "SELECT eoName as name FROM tblExeOffice WHERE eoCode='".$strSenderUnit."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfService($strSenderUnit)
	{
		$SQL = "SELECT serviceName as name FROM tblService WHERE serviceCode='".$strSenderUnit."'";	
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfDivision($strSenderUnit)
	{
		$SQL = "SELECT divisionName as name FROM tblDivision WHERE divisionCode='".$strSenderUnit."'";		
		//echo $SQL;
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfSection($strSenderUnit)
	{
		$SQL = "SELECT sectionName as name FROM tblSection WHERE sectionCode='".$strSenderUnit."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function displaySenderName($strSenderId, $strSenderUnit)
	{
			
		if($strSenderUnit=="employee")
		{
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strSenderId."'";
	
			$sql1= new MySQLHandler2();
			$sql1->init();	
	
			$rs=$sql1->Select($SQL);
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		}else
			$strName = $this->checkSenderUnit($strSenderUnit);;
		
		return $strName;
	}
	
	// SELECT HISTORY RECORD
	function selectAction($intHistoryID)
	{
		$SQL = "SELECT * FROM tblHistory WHERE historyID = '".$intHistoryID."'";
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
	
		return $rs;
	
	}
	// DELETE RECORD
	function deleteAction($intID)
	{
		$SQL = "SELECT * FROM tblHistory WHERE referenceId = '".$intID."'";
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
		
		if(count($rs)==0)
		{
			$SQL = "DELETE FROM tblHistory WHERE historyID = '".$intID."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			if($rs=$sql1->Delete($SQL))
			{
				$this->msg = MSG_DEL_SUCCESS;
				return 1;
			}
			else
			{
				$this->msg = '<font color="red">'.MSG_DEL_SYS_MALF.'</font>';
				return 0;
			}
		}
		else
		{
			$this->msg = '<font color="red">'.MSG_ERR_ACTION_DEL.'</font>';
			return 0;
		}
			
		return $rs;
	
	}
	
	function getReceivedActions($strDocumentID)
	{
		$empHead=$this->getOfficeHead($this->get('office'));
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND (tblHistory.recipientId='".$_SESSION['office']."' OR tblHistory.recipientId='$empHead' ) AND tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";
		elseif($_SESSION['userType']==3) // employee
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";
	

		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		
	
		return $rs;
	}
	
	function checkHistoryActions($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function getHistoryActionSource($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.historyId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.historyId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function checkRecipientHistoryActions($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['office']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function checkReplyActionSent($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['office']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	
	function checkSenderHistoryActions($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['office']."' AND tblHistory.historyId ='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.historyId ='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function getSenderDocument($strDocumentID, $strStatus)
	{
		$SQL="SELECT * FROM tblDocument WHERE documentId='$strDocumentID' AND status='$strStatus'";
		$sql1= new MySQLHandler();
		$sql1->init();	
		$rsDoc=$sql1->Select($SQL);
		
		
		if($strStatus==0) //incoming
		{
			$sender = $rsDoc[0]["sender"];
			$senderOffice = $rsDoc[0]["originId"];
			
			$SQL2="SELECT officeName FROM tblOriginOffice WHERE originId='$senderOffice'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rsDocSender=$sql1->Select($SQL2);
			if(strlen($rsDocSender[0]["officeName"])!=0)
				return $rsDocSender[0]["officeName"]."\n".$sender;
			else
				return $sender;
			
		}
		elseif($strStatus==1 || $strStatus==2) //outgoing or intraoffice
		{
			return AGENCY_CODE.'-'.$rsDoc[0]["officeSig"]."\n".$sender;

		}		

		
	}
	
	function displayDocDetail($arrFields2)
	{
		echo '<br>';
		
		// Display submitted data
		$rsActionTakenSubmitted = $objDocDetail->getActionTakenDetails($arrFields2['cmbActionTaken']);
		$rsActionRequiredSubmitted = $objDocDetail->getActionRequiredDetails($arrFields2['cmbActionRequired']);
			
		if($arrFields2['actionUnitSwitch']==2) // if office
		{
			list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields2['cmbOfficeCode']);
			$recipientUnitLabel = 'Office';
	
		}elseif($arrFields2['actionUnitSwitch']==1) // if employee
		{
			$strRecipientUnit="employee";
			$strRecipientId = $arrFields2['cmbEmpNumber'];
			$recipientUnitLabel = 'Employee';
		}
		
		$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
			
	
		echo '<table width="100%"  border="0">
		  <tr>
			<td width="30%">Action Taken </td>
			<td width="70%">'.$rsActionTakenSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Required </td>
			<td>'.$rsActionRequiredSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Unit </td>
			<td></td>
		  </tr>
		  <tr>
			<td>'.$recipientUnitLabel.' </td>
			<td>'.$recipientName.'</td>
		  </tr>
		  <tr>
			<td>Remarks</td>
			<td>'.$arrFields2['txtRemarks'].'</td>
		  </tr>
		  <tr>
			<td>Restricted</td>
			<td>'.$restricted.'</td>
		  </tr>
		</table>';

		echo "<script language=\"JavaScript\">
		getData('showAction.php?mode=$mode&docID=$docID&div=$divId&historydiv=$historydiv','$divId');
		if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
		getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
		</script>";
	}
	
	function displaySourceRecipient()
	{
	
		$objDocDetail = new DocDetail;
		
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" height="23" align="left"><input name="actionUnitSwitch" type="radio" value="2" onClick="enableOfficeCombo(this.form);">
				  Office: </td>
				<td width="79%">';
											$objDocDetail->displayOffice();
											
		echo '</td>
			  </tr>
			</table>';
			
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="1" onClick="enableNameCombo(this.form);">
				  Employee Name: </td>
				<td width="79%">';
					$rsEmployees = $objDocDetail->getEmployees();
		echo  '<select name="cmbEmpNumber" id="cmbEmpNumber" onFocus="enableNameCombo(this.form);">
				  <option value="-1">&nbsp;</option>';

							for($i=0;$i<sizeof($rsEmployees);$i++) 
							{ 		
								$extension = (trim($rsEmployees[$i]['nameExtension'])=="") ? "" : " ".$rsEmployees[$i]['nameExtension']; 
								$strName = $rsEmployees[$i]['surname'].", ".$rsEmployees[$i]['firstname'].$extension.' '.$rsEmployees[$i]['middleInitial'];
			
								echo "<OPTION value='".$rsEmployees[$i]['empNumber']."'>".$strName."</OPTION>\n"; 
							}

		echo '</select></td>
			  </tr>
			</table>';
			
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="3" >
				  Agency Name: </td>
				<td width="79%">';
					$rsAgencies = $objDocDetail->getAgencies();
				
		echo '	 <select name="cmbAgency" id="cmbAgency">
				  <option value="-1">&nbsp;</option>';

							for($i=0;$i<sizeof($rsAgencies);$i++) 
							{ 		
								
								echo "<OPTION value='".$rsAgencies[$i]['originId']."'>".$rsAgencies[$i]['officeName']."</OPTION>\n"; 
							}
					
		echo '	</select></td>
			  </tr>
			</table>';
	
	
	}
	
/*


function getServices()
{
	$sql="SELECT * FROM tblService";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}







function getOfficeEmployees($txtOfficeCode)
{
	if($txtOfficeCode!="")
		$queryOffice = " AND tblEmpPosition.officeCode='$txtOfficeCode' ";
		
	$sql="SELECT tblEmpPersonal.empNumber, tblEmpPersonal.firstname, tblEmpPersonal.middleInitial, tblEmpPersonal.surname 
		FROM tblEmpPersonal INNER JOIN tblEmpPosition ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber 
		WHERE tblEmpPosition.statusOfAppointment='In-Service' $queryOffice ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}
*/


}
?>