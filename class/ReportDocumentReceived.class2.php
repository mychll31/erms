<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentReceived extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		$arrDocIds = array();

		if($_GET['stat'] == 1):
			$this->SetWidths(array(10,28,45,25,25,25,25,25,27,35));
			$this->SetAligns(array('C','L','L','C','L','L','L','L','L'));
		else:
			$this->SetWidths(array(10,45,50,30,30,30,30,30));
			$this->SetAligns(array('C','L','L','C','L','L','L'));
		endif;
		$this->SetFont('Arial','',9);
		$ctr = 1;
		
		if($_GET['reportType']=='DMPR'){
			$docreceived = $objDocDetail->getDocumentReceivedByEmpNumber($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo']);
		}else{
			$docreceived = $objDocDetail->getDocumentReceivedByEmpNumber($_SESSION['office'], date('Y-m-d', strtotime($_GET['dateYear'].'-'.$_GET['dateMonth'].'-01')), date('Y-m-d', strtotime($_GET['dateYear'].'-'.$_GET['dateMonth'].'-31')));
		}
		foreach($docreceived as $doc):
			$documentId = $doc['documentId'];
			$docdetail = $objDocDetail->getDocumentDetails($documentId);
			$subject = strlen($docdetail['subject']) > 30 ? substr($docdetail['subject'], 0,30).'...' : $docdetail['subject'] ;
			$doctype = $docdetail['documentTypeAbbrev'];

			$docRecivedDetails = $objDocDetail->getDocumentHistory($documentId, 'ASC');
			$receivedBy = $docRecivedDetails['receivedBy'];
			$dateReceived = $docRecivedDetails['dateReceived'];
			$processingTime = '';
			if($receivedBy == ''):
				$docRecivedDetails = $objDocDetail->getReceivedDocByHistoryId($docRecivedDetails['historyId'], $documentId);
				$receivedBy = $docRecivedDetails[0]['receivedBy'];
				$dateReceived = $docRecivedDetails[0]['dateReceived'];
			endif;
			
			$docHistory = $objDocDetail->getDocumentHistory($documentId, 'DESC');
			$dateProcessed = $docHistory['dateAdded'];
			$processed_by = $objDocDetail->getProcessedbyName($docHistory['addedById']);

			if($_GET['detail'] == '1'):
				if($dateReceived != '' && $dateProcessed != ''):
					if(substr($dateReceived, 0,10) != substr($dateProcessed, 0,10)):
						$dates = $this->date_range($dateReceived, $dateProcessed);
						$totalDays = 0;
						foreach($dates as $thisdate):
							$isholiday = $objDocDetail->isHoliday($thisdate);
							if(date('D', strtotime($thisdate)) != 'Sat' && date('D', strtotime($thisdate)) != 'Sun' && $isholiday != 1):
								$totalDays += 1;
							endif;
						endforeach;
					endif;
					// startDate
					$processingTime = $this->getDateDiff($dateReceived, $dateProcessed);
				endif;
			endif;

			if($docdetail['isDelete'] == 0):
				if($_GET['stat'] == 1):
					$this->Row(array($ctr++,
							$documentId,
							$subject,
							$doctype,
							$receivedBy,
							$dateReceived,
							$dateProcessed,
							$processed_by,
							'',
							$processingTime), 1);
				else:
					$this->Row(array($ctr++,
							$documentId,
							$subject,
							$doctype,
							$receivedBy,
							$dateReceived,
							$dateProcessed,
							$processed_by), 1);
				endif;
			endif;
		endforeach;
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$report_status = ($_GET['stat'] == 1) ? 'Outgoing' : 'Incoming';
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($_GET['reportType']=='DMPR')
			if($_GET['dateFrom'] == $_GET['dateTo'])
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).')', 0, 1, 'C');
			else
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		if($_GET['stat'] == 1):
			$cell_w = array('10','28','45','25','25','25','25','25','27','35','10','28','45','25','25','25','25','25','27','35');
			$cell_caption = array(" ","DOC ID","SUBJECT","DOC TYPE","RECEIVED","DATE/TIME","DATE/TIME","PROCESSED",isset($_GET['detail']) ? "RELEASED TO" : "ACTION","PROCESSING"," ","","","","BY","RECEIVED","PROCESSED","BY",isset($_GET['detail']) ? "" : "TAKEN","TIME");
		else:
			$cell_w = array(10,45,50,30,30,30,30,30,10,45,50,30,30,30,30,30);
			$cell_caption = array(" ","DOC ID","SUBJECT","DOC TYPE","RECEIVED","DATE/TIME","DATE/TIME","PROCESSED"," ","","","","BY","RECEIVED","PROCESSED","BY");
		endif;

		foreach($cell_caption as $key=>$cell):
			if($_GET['stat'] == 1):
				$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'BRL' : 'TRL',$key == 9 ? 1 : 0,"C",1);
			else:
				$this->Cell($cell_w[$key],5,$cell,$key > 7 ? 'BRL' : 'TRL',$key == 7 ? 1 : 0,"C",1);
			endif;
		endforeach;
		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Cell(90,4,'Person Responsible', 0, 0, 'C');
		$this->Cell(90,4,'Verified By', 0, 0, 'C');
		$this->Cell(90,4,'Approved for filing', 0, 1, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(90,5,'', 'B', 0, 'C');
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}


	function times_counter($times)
	{
		$day = 0;
		$hou = 0;
		$min = 0;
		$sec = 0;
		$totaltime = '00:00:00';

		if(count($times) > 1) {
			if(is_array($times)){

	            $length = sizeof($times);

	            for($x=0; $x <= $length; $x++){
	                    $split = explode(":", @$times[$x]); 
	                    $this->hou += @$split[0];
	                    $this->min += @$split[1];
	                    $this->sec += @$split[2];
	            }

	            $seconds = $this->sec % 60;
	            $minutes = $this->sec / 60;
	            $minutes = (integer)$minutes;
	            $minutes += $this->min;
	            $hours = $minutes / 60;
	            $minutes = $minutes % 60;
	            $hours = (integer)$hours;
	            $hours += $this->hou % 24;
	        }
	    } else {
	    	$time = explode(':', $times[0]);
	    	$day = 0;
	    	$hours = $time[0];
	    	$minutes = $time[1];
	    	$seconds = $time[2];
	    }
	    $day = $hours / 24;
	    $day = intval($day);
	    $hours = $hours - ($day*24);
	    // echo $day;
	    // print_r($times);
	    $totaltime = $day.($day > 1 ? " Days " : " Day ").sprintf("%02d", $hours).($hours > 1 ? " Hours " : " Hour ").sprintf("%02d", $minutes).($minutes > 1 ? " Minutes " : " Minute ").sprintf("%02d", $seconds).($seconds > 1 ? " Seconds" : " Second");
            return $totaltime;
	}

	function date_range($start, $end, $step = '+1 day', $output_format = 'Y-m-d' ) {
	    $dates = array();
	    $current = strtotime($start);
	    $last = strtotime($end);

	    while( $current < $last ) {
	    	$current = strtotime($step, $current);
	    	$dates[] = date($output_format, $current);
	    }
	    unset($dates[count($dates)-1]);
	    return $dates;
	}

	function getDateDiff($dateReceived, $dateProcessed){
		$ts=$dateReceived;
		$ts1=substr($dateReceived, 0,10).' 18:00:00';
		
		//converting to time
		$start = strtotime($ts);
		$end = strtotime($ts1);

		//calculating the difference
		$difference = $end - $start;

		//calculating hours, minutes and seconds (as floating point values)
		$hours = $difference / 3600; //one hour has 3600 seconds
		$minutes = ($hours - floor($hours)) * 60;
		$seconds = ($minutes - floor($minutes)) * 60;

		//formatting hours, minutes and seconds
		$rec_hours = floor($hours);
		$rec_minutes = floor($minutes);
		$rec_seconds = floor($seconds);

		$ts=substr($dateProcessed, 0,10).' 07:00:00';
		$ts1=$dateProcessed;

		/** DATE RECEIVED**/

		//converting to time
		$start = strtotime($ts);
		$end = strtotime($ts1);
		//calculating the difference
		$difference = $end - $start;

		//calculating hours, minutes and seconds (as floating point values)
		$hours = $difference / 3600; //one hour has 3600 seconds
		$minutes = ($hours - floor($hours)) * 60;
		$seconds = ($minutes - floor($minutes)) * 60;

		//formatting hours, minutes and seconds
		$proc_hours = floor($hours);
		$proc_minutes = floor($minutes);
		$proc_seconds = floor($seconds);

		// TOTAL TIME
		$totalMins = 0; $totalHrs = 0;$day=0;

		$totalSecs = $rec_seconds + $proc_seconds;
		if($totalSecs > 60):
			$totalMins = floor($totalSecs / 60);
			$totalSecs = $totalSecs % 60;
		endif;

		$totalMins = $totalMins + $rec_minutes + $proc_minutes;
		if($totalMins > 60):
			$totalHrs = floor($totalMins / 60);
			$totalMins = $totalMins % 60;
		endif;

		$totalHrs = $totalHrs + $rec_hours + $proc_hours;
		if($totalHrs > 60):
			$day = floor($totalHrs / 60);
			$totalHrs = $totalHrs % 60;
		endif;


		$day = ($day > 0) ? ($day > 1) ? $day.' Days ' : $day.' Days ' : '';
		$hrs = ($totalHrs > 0) ? ($totalHrs > 1) ? $totalHrs.' Hrs ' : $totalHrs.' Hr ' : '';
		$mins = ($totalMins > 0) ? ($totalMins > 1) ? $totalMins.' Mins ' : $totalMins.' Min ' : '';
		$secs = ($totalSecs > 0) ? ($totalSecs > 1) ? $totalSecs.' Secs ' : $totalSecs.' Sec ' : '';
		return $day.$hrs.$mins.$secs;

	}

}
?>
