<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentDocumentSummary extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("IncomingList.class.php");
		$objIncoming = new IncomingList;
		$doc_status = $_GET['doc'];

		if($doc_status == '1'){
			$title = 'Pending';
		}else if($doc_status == '2'){
			$title = 'Overdue';
		}else if($doc_status == '3'){
			$title = 'Unacted';
		}else{
			$title = '';
		}
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=".$title.".xls");
		
		# begin pending
		if($doc_status == 1):
			$pendings = $objIncoming->getOverDue(0, 'all', 'pending');
			$no = 1;
			// print_r($pendings);
			echo '<table border=1>';
				echo '<tr>
						<td><b>NO</b></td>
						<td><b>DOC ID</b></td>
						<td><b>SUBJECT</b></td>
						<td><b>DATE SENT</b></td>
						<td><b>SENDER</b></td>
						<td><b>REMARKS</b></td>
					  </tr>';
				foreach($pendings as $pending):
					$pending['senderId'] = is_numeric($pending['senderId']) ? $pending['officeName'] : $pending['senderId'];
					echo '<tr>
							<td>'.$no++.'</td>
							<td>'.$pending['documentId'].'</td>
							<td>'.$pending['subject'].'</td>
							<td>'.$pending['dateSent'].'</td>
							<td>'.$pending['senderId'].'</td>
							<td>'.$pending['remarks'].'</td>
						  </tr>';
				endforeach;
			echo '</table>';
		endif;
		# end pending

		# begin overdue
		if($doc_status == 2):
			$overdues = $objIncoming->getOverDue(0, 'all', 'overdue');
			$no = 1;
			// print_r($pendings);
			echo '<table border=1>';
				echo '<tr>
						<td><b>NO</b></td>
						<td><b>DOC ID</b></td>
						<td><b>SUBJECT</b></td>
						<td><b>DATE SENT</b></td>
						<td><b>SENDER</b></td>
						<td><b>REMARKS</b></td>
					  </tr>';
				foreach($overdues as $odue):
					$odue['senderId'] = is_numeric($odue['senderId']) ? $odue['officeName'] : $odue['senderId'];
					echo '<tr>
							<td>'.$no++.'</td>
							<td>'.$odue['documentId'].'</td>
							<td>'.$odue['subject'].'</td>
							<td>'.$odue['dateSent'].'</td>
							<td>'.$odue['senderId'].'</td>
							<td>'.$odue['remarks'].'</td>
						  </tr>';
				endforeach;
			echo '</table>';
		endif;
		# end overdue

		# begin unacted
		if($doc_status == 3):
			$unacteddocs = $objIncoming->getUnactedDocuments(0, 'all');
			$no = 1;

			echo '<table border=1>';
				echo '<tr>
						<td><b>NO</b></td>
						<td><b>DOC ID</b></td>
						<td><b>SUBJECT</b></td>
						<td><b>DATE SENT</b></td>
						<td><b>REMARKS</b></td>
					  </tr>';
				foreach($unacteddocs as $udoc):
					echo '<tr>
							<td>'.$no++.'</td>
							<td>'.$udoc['documentId'].'</td>
							<td>'.$udoc['subject'].'</td>
							<td>'.$udoc['dateSent'].'</td>
							<td>'.$udoc['remarks'].'</td>
						  </tr>';
				endforeach;
			echo '</table>';
		endif;
		# end unacted

		die();
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($_GET['reportType']=='DMPR')
			if($_GET['dateFrom'] == $_GET['dateTo'])
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).')', 0, 1, 'C');
			else
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','30','20','45','25','25','25','25','27','35','10','30','20','45','25','25','25','25','27','35','10','30','20','45','25','25','25','25','27','35');
		$cell_caption = array("NO.","DOC ID","DOC TYPE","SUBJECT","DATE/TIME","DATE/TIME","PROCESS","DATE/TIME","RECEIVED","PROCESSING",
								" "," "," "," ","RECEIVED","PROCESSED","BY","RECEIVED","(RECIPIENT)","TIME",
								" "," "," "," ","","","","(RECIPIENT)","","");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'RL' : 'TRL',$key == 9 || $key == 19 ? 1 : 0,"C",1);
		endforeach;

		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Cell(90,4,'Person Responsible', 0, 0, 'C');
		$this->Cell(90,4,'Verified By', 0, 0, 'C');
		$this->Cell(90,4,'Approved for filing', 0, 1, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(90,5,'', 'B', 0, 'C');
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}

	function breakin_dates($dateRec, $dateProc)
	{
		$rDate = date('Y-m-d', strtotime($dateRec));
		$pDate = date('Y-m-d', strtotime($dateProc));

		$rTime_h = date('H', strtotime($dateRec));
		$rTime_m = date('i', strtotime($dateRec));
		$rTime_s = date('s', strtotime($dateRec));

		$pTime_h = date('H', strtotime($dateProc));
		$pTime_m = date('i', strtotime($dateProc));
		$pTime_s = date('s', strtotime($dateProc));

		if($rDate == $pDate):
			// get Seconds
			$borrow_m = 0;
			if($pTime_s < $rTime_s):
				$borrow_m = 1;
				$pTime_s = $pTime_s + 60;
			endif;
			$total_s = $pTime_s - $rTime_s;

			// get minutes
			$pTime_m = $pTime_m - $borrow_m;
			$borrow_h = 0;
			if($pTime_m < $rTime_m):
				$borrow_h = 1;
				$pTime_m = $pTime_m + 60;
			endif;
			$total_m = $pTime_m - $rTime_m;

			// get hour
			$pTime_h = $pTime_h - $borrow_h;
			$total_h = $pTime_h - $rTime_h;

			echo 'day/s: 0 hr/s: '.$total_h.' mins: '.$total_m.' secs: '.$total_s;

		elseif($rDate < $pDate):
			// echo 'MORE THAN 1 DAY';
			$begin = new DateTime($rDate);
			$end = new DateTime($pDate);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			$ctr = 0; $ctr_days = 0;

			foreach ($period as$dt):
				#remove first element
				if($ctr > 0):
					#check if weekend.
					if($dt->format("D") != 'Sat' && $dt->format("D") != 'Sun'):
						#check if holiday
						include_once("DocDetail.class.php");
						$objDocDetail = new DocDetail;
						$isholiday = $objDocDetail->isHoliday($dt->format("Y-m-d"));
						if(count($isholiday) < 1):
							$ctr_days++;
						endif;
					endif;
				endif;
				$ctr++;
			endforeach;

			// get Seconds
			$borrow_m = 0;
			if($pTime_s < $rTime_s):
				$borrow_m = 1;
				$pTime_s = $pTime_s + 60;
			endif;
			$total_s = $pTime_s - $rTime_s;

			// get minutes
			$pTime_m = $pTime_m - $borrow_m;
			$borrow_h = 0;
			if($pTime_m < $rTime_m):
				$borrow_h = 1;
				$pTime_m = $pTime_m + 60;
			endif;
			$total_m = $pTime_m - $rTime_m;

			// get hours
			$pTime_h = $pTime_h - $borrow_h;
			$borrow_d = 0;
			if($pTime_h < $rTime_h):
				if($ctr_days > 0):
					$borrow_d = 1;
					$pTime_h = $pTime_h + 24;
					$total_h = $pTime_h - $rTime_h;
				else:
					$total_h = 0;
				endif;
			endif;

			// get days
			$total_d = $ctr_days - $borrow_d;

			echo 'day/s: '.$total_d.' hr/s: '.$total_h.' mins: '.$total_m.' secs: '.$total_s;
		else:
			echo 'INVALID DATE';
		endif;

	}



	

}
?>
