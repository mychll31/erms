<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentHistory extends FPDF
{	
	var $ReportType;
	
	function generateReport()
	{
	
		include_once("DocDetail.class.php");
		//$objRecordDetail = new DocDetail;
		$objDocDetail = new DocDetail;

		include_once("Records.class.php");
		$objRecordDetail = new Records;

		$rsRecordHistory = $objRecordDetail->getOfficeDocumentHistory($_GET['documentID'],'');

		$w = array(15,25,25,40,50,50,70);
		$Ln = array('L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		foreach($rsRecordHistory as $rec):
			$historyid = $rec['historyId'];
			$refid = $rec['referenceId'];
			$datesent = $rec['dateSent'];

			$currentActionSender = $objDocDetail->displayRecipientSenderNames($rec['senderId'], $rec['senderUnit']);
			if($rec['senderId']=='CASHIER' || $rec['senderId']=='ALS' || $rec['senderId']=='OASECFALA'){
				$currentActionSender = $rec['senderId'];
			}
			$actionTaken = $rec['actionTakenDesc'].' ['.$currentActionSender.']';

			$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rec['recipientId'], $rec['recipientUnit']);
			$actionRequired = $rec['actionDesc'].' ['.$currentActionRecipient.']';

			$remarks = $rec['remarks'];

			$this->Row(array($ctr++,$historyid, $refid, $datesent, $actionTaken, $actionRequired, $remarks),1);
		endforeach;
		
		// for($i=0;$i<count($rsRecordHistory);$i++) 
		// {									
		
		// 		$ctr++;
		// 		$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsRecordHistory[$i]['senderId'], $rsRecordHistory[$i]['senderUnit']);
		// 		if($rsRecordHistory[$i]['senderId']=='CASHIER' || $rsRecordHistory[$i]['senderId']=='ALS' || $rsRecordHistory[$i]['senderId']=='OASECFALA')
		// 			$currentActionSender = $rsRecordHistory[$i]['senderId'];
		// 		$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsRecordHistory[$i]['recipientId'], $rsRecordHistory[$i]['recipientUnit']);
		// 		if(isset($rsRecordHistory[$i]['actionDesc']))
		// 			$rsRecordHistory[$i]['actionDesc']='';
		// 		if(isset($rsRecordHistory[$i]['actionTakenDesc']))
		// 			$rsRecordHistory[$i]['actionTakenDesc']='';
		// 		//Add Remarks validation
		// 		if(isset($rsRecordHistory[$i]['remarks']))
		// 			$rsRecordHistory[$i]['remarks']='';
				
		// 		$this->Row(array($ctr,$rsRecordHistory[$i]['historyId'], $rsRecordHistory[$i]['referenceId'], $rsRecordHistory[$i]['dateSent'], $rsRecordHistory[$i]['	'].' ['.$currentActionSender.']',$rsRecordHistory[$i]['actionDesc'].' ['.$currentActionRecipient.']', $rsRecordHistory[$i]['historyRemarks']),1);


		// 		// $this->Row(array($ctr,$rsRecordHistory[$i]['historyId'], $rsRecordHistory[$i]['referenceId'], $rsRecordHistory[$i]['dateSent'], $rsRecordHistory[$i]['actionTakenDesc'].' ['.$currentActionSender.']',$rsRecordHistory[$i]['actionDesc'].' ['.$currentActionRecipient.']', $rsRecordHistory[$i]['remarks']),1);
			
		// }
	
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();

		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report for ".$_GET['documentID']);

	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);				

		if($_SESSION['userType']==3){
			$officeCode = explode('~',$objDocDetail->getOfficeCode2($objDocDetail->get('userID')));
			$officeName = $officeCode[0];		
		}else{
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		}
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);
		
		$this->SetFont('Arial',B,11);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',11);
		$this->Ln(10);
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		$rsRecordDetail = $objRecordDetail->getDocumentDetails($_GET['documentID']);
		$this->Cell(10,7," ",0,0,"C");
		$this->Cell(50,7,"DOCUMENT ID",0,0,"L");
		$this->SetFont('Arial',B,11);
		$this->Cell(50,7,$_GET['documentID'],0,1,"L");
		$this->SetFont('Arial','',11);
		$this->Cell(10,7," ",0,0,"C");
		$this->Cell(50,7,"DOCUMENT TYPE",0,0,"L");
		$this->SetFont('Arial',B,11);
		$this->Cell(50,7,$objRecordDetail->getDocumentType($rsRecordDetail[0]['documentTypeId']),0,1,"L");
		$this->Cell(10,7," ",0,0,"C");
		$this->SetFont('Arial','',11);
		$this->Cell(50,7,"DOCUMENT DATE",0,0,"L");
		$this->SetFont('Arial',B,11);
		$this->Cell(50,7,$rsRecordDetail[0]['documentDate'],0,1,"L");

		$this->SetFont('Arial','',11);
		$this->Cell(10,7," ",0,0,"C");
		$this->Cell(50,7,"SUBJECT",0,0,"L");
		$this->SetFont('Arial',B,11);
		$subject = $rsRecordDetail[0]['subject'];
		if(strlen($subject) > 20):
			$this->SetFont('Arial',B,10);
			$subject = substr($subject,0,250)." ...";
		endif;
		$this->MultiCell(200,7,str_replace("\\n"," ",$subject),0,1,"L");
		$this->Cell(10,7," ",0,0,"C");
				$this->SetFont('Arial','',11);
		$this->Cell(50,7,"DATE ADDED",0,0,"L");
		$this->SetFont('Arial',B,11);
		$this->Cell(50,7,date("Y-M-d H:i", strtotime($rsRecordDetail[0]['dateAdded'])),0,1,"L");

		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C",1);
		$this->Cell(25,7,"ID #",1,0,"C",1);
		$this->Cell(25,7,"REF ID #",1,0,"C",1);
		$this->Cell(40,7,"DATE",1,0,"C",1);
		$this->Cell(50,7,"ACTION TAKEN",1,0,"C",1);
		$this->Cell(50,7,"ACTION REQUIRED",1,0,"C",1);
		$this->Cell(70,7,"REMARKS",1,0,"C",1);
		$this->Ln(10);


	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}

}
?>