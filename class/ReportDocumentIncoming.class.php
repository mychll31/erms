<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentIncoming extends FPDF
{	
	var $ReportType;

	function generateReport()
	{
	
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		
		if($_SESSION['office']=='RMS')
			$rsRecordDetail = $objRecordDetail->getDocListRMS($_SESSION['office'],0,$_GET['dateFrom'],$_GET['dateTo']);
		else
			$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
		
		$w = array(15,30,30,30,30,75,50);
		$Ln = array('L','L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;

		if($_SESSION['office']!='RMS'):
			for($i=0;$i<count($rsRecordDetail);$i++) 
			{									
			
				if($_GET['doctype']=='all'){
					if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==0)
					{
						$ctr++;
						$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
					}
				}else{
					if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==0 && $rsRecordDetail[$i]['documentTypeId']==$_GET['doctype'])
					{
						$ctr++;
						$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
					}	
				}
			}
		// if RMS
		else:
			$rowctr=0;
			for($i=0;$i<count($rsRecordDetail);$i++) 
			{
				$ctr++;	
				if($_GET['doctype']=='all'){
					$this->Row(array($ctr,
								$rsRecordDetail[$i]['documentId'],
								$objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),
								$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),
								$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),
								$rsRecordDetail[$i]['subject'],
								$rsRecordDetail[$i]['dateAdded']),1);
				}else{
					if($rsRecordDetail[$i]['documentTypeId']==$_GET['doctype']){
						$rowctr++;
						$this->Row(array($rowctr,
								$rsRecordDetail[$i]['documentId'],
								$objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),
								$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),
								$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),
								$rsRecordDetail[$i]['subject'],
								$rsRecordDetail[$i]['dateAdded']),1);
					}
				}
			}
		endif;
		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->ReportType = $objReport->getReportName($_GET['reportType']);
		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C",1);
		$this->Cell(30,7,"DOC ID",1,0,"C",1);
		$this->Cell(30,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(30,7,"STATUS",1,0,"C",1);
		$this->Cell(30,7,"ORIGIN",1,0,"C",1);
		$this->Cell(75,7,"SUBJECT",1,0,"C",1);
		$this->Cell(50,7,"DATE ENCODED",1,0,"C",1);
		$this->Ln(10);
	}
	
	function getReportType()
	{
		return $this->ReportType;
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
}
?>