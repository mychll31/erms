<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentReceived extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		$arrDocIds = array();
		
		$this->SetWidths(array(10,28,45,25,25,25,25,25,27,35));
		$this->SetAligns(array('L','L','L','L','L','L','L','L','L'));
		$this->SetFont('Arial','',9);
			
		$rsReceivedDocsFromHistory = null;
		if($_GET['reportType']=='DMPR'){
			$rsReceivedDocs = $objDocDetail->getReceivedDocs($_SESSION['userID'], $_GET['dateFrom'], $_GET['dateTo'], 0);						  # Get from table Received Documents					
			$rsReceivedDocsFromHistory = $objDocDetail->getReceivedDocsFromHistory($_SESSION['userID'], $_GET['dateFrom'], $_GET['dateTo'], 0);   # Get from table History
			$rsNewCreatedDocs = $objDocDetail->getNewCreatedDocument($_SESSION['userID'], $_GET['dateFrom'], $_GET['dateTo'], 0);   		  	  # Get from table Document
		}
		else
			$rsReceivedDocs = $objDocDetail->getReceivedDocs($_SESSION['userID'], $intMonth, $intYear, 1);

		$ctr = 1;

		##### NEW CREATED DOC #####
		$actionTaken = '';
		$processed_by = '';
		
		foreach($rsNewCreatedDocs as $newdoc):
			if(! in_array($newdoc['documentId'], $arrDocIds)){
				array_push($arrDocIds, $newdoc['documentId']);
				$processed_by = $objDocDetail->getProcessedbyName($newdoc['addedById']);
				$actionTaken = $objDocDetail->getActionTakenDetails($newdoc['actionTakenCodeID']);
				$this->Row(array($ctr++,
								$newdoc['documentId'],
								$newdoc['subject'],
								$newdoc['documentTypeDesc'],
								'',
								'',
								$newdoc['dateAdded'],
								$processed_by,
								'',
								''), 1);
			}
		endforeach;

		##### RECEIVED DOCUMENTS #####
		foreach($rsReceivedDocs as $received):
			$actionTaken = '';
			$receivedby = '';
			$datetime1 = '';
			$datetime2 = '';
			$processed_by = '';
			$processedTime = '';
			$processed_doc = '';
			if(! in_array($received['documentId'], $arrDocIds)){
				array_push($arrDocIds, $received['documentId']);
				$processed_doc = $objDocDetail->getReferenceFromHistory($received['historyId']);
				$processed_by = $objDocDetail->getProcessedbyName($processed_doc['addedById']);
				$actionTaken = $objDocDetail->getActionTakenDetails($processed_doc['actionTakenCodeID']);

				$datetime1 = new DateTime($received['dateReceived']);
				$datetime2 = new DateTime($processed_doc['dateAdded']);
				$interval = $datetime1->diff($datetime2);
				$processedTime = $interval->format('%h')." Hours ".$interval->format('%i')." Minutes ".$interval->format('%s')." Seconds";

				$this->Row(array($ctr++,
								$received['documentId'],
								$received['subject'],
								$received['documentTypeDesc'],
								$received['receivedBy'],
								$received['dateReceived'],
								$processed_doc['dateAdded'],
								$processed_by,
								$actionTaken[0]['actionDesc'],
								$received['dateReceived'] == '' || $processed_doc['dateAdded'] == '' ? '' : $processedTime), 1);
			}
		endforeach;

		##### HISTORY #####
		$actionTaken = '';
		$processed_by = '';
		
		foreach($rsReceivedDocsFromHistory as $rshistory):
			if(! in_array($rshistory['documentId'], $arrDocIds)){
				array_push($arrDocIds, $rshistory['documentId']);
				$processed_by = $objDocDetail->getProcessedbyName($rshistory['addedById']);
				$actionTaken = $objDocDetail->getActionTakenDetails($rshistory['actionTakenCodeID']);
				$this->Row(array($ctr++,$rshistory['documentId'], $rshistory['subject'], $rshistory['documentTypeDesc'],'','',$rshistory['dateAdded'],$processed_by,$actionTaken[0]['actionDesc'],''), 1);
			}
		endforeach;

		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($_GET['reportType']=='DMPR')
			if($_GET['dateFrom'] == $_GET['dateTo'])
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).')', 0, 1, 'C');
			else
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','28','45','25','25','25','25','25','27','35','10','28','45','25','25','25','25','25','27','35');
		$cell_caption = array(" ","DOC ID","SUBJECT","DOC TYPE","RECEIVED","DATE/TIME","DATE/TIME","PROCESSED","ACTION","PROCESSING"," ","","","","BY","RECEIVED","PROCESSED","BY","TAKEN","TIME");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'BRL' : 'TRL',$key == 9 ? 1 : 0,"C",1);
		endforeach;
		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Cell(90,4,'Person Responsible', 0, 0, 'C');
		$this->Cell(90,4,'Verified By', 0, 0, 'C');
		$this->Cell(90,4,'Approved for filing', 0, 1, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(90,5,'', 'B', 0, 'C');
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>