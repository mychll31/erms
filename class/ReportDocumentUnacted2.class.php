<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
include_once("Date.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentUnacted extends FPDF
{	

	var $ReportType;
	function generateReport()
	{
	
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		
		$rsOfficeRecord = $objDocDetail->getOfficeHistory($_SESSION['office'],$_GET['dateFrom'],$_GET['dateTo']);

		$w = array(15,30,40,30,60,30,110);
		$Ln = array('L','L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		
		for($i=0;$i<count($rsOfficeRecord);$i++) 
		{			
						
			$dateAdded = substr($rsOfficeRecord[$i]['dateAdded'],0,10);

			if($dateAdded>=$_GET['dateFrom'] && $dateAdded<=$_GET['dateTo'])
			{
				//$this->Row(array("TEST",0,0,0,0,0,0,0));				
				if($rsOfficeRecord[$i]['reply'] == 1)
				{
					$rsUnactedDocument = $objDocDetail->getDocumentUnactedReply($rsOfficeRecord[$i]["historyId"]);

					if(count($rsUnactedDocument)==0)
					{
						$ctr++;
						$recordSender = $objDocDetail->getOriginUnactedDoc($rsOfficeRecord[$i]['documentId']);
						$date = new Date($dateAdded);  
						$this->Row(array($ctr,$date->getAsStr("d M Y"),$rsOfficeRecord[$i]['documentId'], $objRecordDetail->getDocumentType($rsOfficeRecord[$i]['documentTypeId']),$recordSender,$objRecordDetail->getDocumentStatus($rsOfficeRecord[$i]['status']),$rsOfficeRecord[$i]['subject']),0);
						
						$this->SetFont('Arial','',8);
						$w2 = array(15,30,40,30,60,30,110);
						$Ln2 = array('L','L','L','L','L','L','C');
						$this->SetWidths($w2);
						$this->SetAligns($Ln2);
						$this->Row(array("","","","","","","HISTORY"),0);

						$rsOfficeDocumentHistory2 = $objDocDetail->getDocumentUnactedHistory($rsOfficeRecord[$i]['documentId'], $_SESSION['office']);
						
						for($i2=0;$i2<count($rsOfficeDocumentHistory2);$i2++) 
						{	
							// display history details including elapse time
						
							if($objRecordDetail->checkDocumentHistoryReply($rsOfficeDocumentHistory2))
							{
								
								$rsOfficeDocumentHistoryDetails = $objRecordDetail->getDocumentHistoryDetails($rsOfficeDocumentHistory2[$i2]['historyId']);
										$historyColumn = "";
										
										$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsOfficeDocumentHistoryDetails[0]['senderId'], $rsOfficeDocumentHistoryDetails[0]['senderUnit']);
										$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsOfficeDocumentHistoryDetails[0]['recipientId'], $rsOfficeDocumentHistoryDetails[0]['recipientUnit']);
										//$noofdayspassed = floor((time() - strtotime($rsOfficeDocumentHistoryDetails[0]['dateAdded']))/86400);
										$dateAdded2 = substr($rsOfficeDocumentHistoryDetails[0]['dateAdded'],0,10);
										$elapsedtime = $this->getElapsedTimeFromPresent($dateAdded2);
										
										$historyColumn .= "Date Sent: ".$dateAdded2."\n";
										$historyColumn .= "Action Taken : ".$rsOfficeDocumentHistoryDetails[0]['actionTakenDesc']."\n";
										$historyColumn .= "From : ".$currentActionSender."\n";
										$historyColumn .= "To : ".$currentActionRecipient."\n";
										//$historyColumn .= "Received By: ".$rsOfficeDocumentHistoryDetails[0]['receivedBy']."\n";
										$historyColumn .= "Action Required : ".$rsOfficeDocumentHistoryDetails[0]['actionDesc']."\n";
										$historyColumn .= "Remark : ".$rsOfficeDocumentHistoryDetails[0]['remarks']."\n";
										$historyColumn .= "Elapsed time: ".$elapsedtime."\n";
										$historyColumn .= "-----------------------------------------------------------"."\n";
									/*	
										if(count($rsRecordHistory)>1 && $linectr>1)
										{
											$historyColumn .= "-----------------------------------------------------------"."\n";
											$linectr=$linectr-1;
										}	
									*/	
										$w2 = array(15,30,40,30,60,30,110);
										$Ln2 = array('L','L','L','L','L','L','L');
										$this->SetWidths($w2);
										$this->SetAligns($Ln2);
										$this->SetFont('Arial','',8);
										$this->Row(array("","","","","","",$historyColumn),0);
										
			
								
									
									$this->SetFont('Arial','',9);

								
							} // if($objRecordDetail->checkDocumentHistoryReply($rsOfficeDocumentHistory))
							
						} // for($i2=0;$i2<count($rsRecordHistory);$i2++) 
					}
					
					
					
				
				}
				
				else
				{
					$rsDocumentReceived = 	$objDocDetail->getDocumentReceived($rsOfficeRecord[$i]["historyId"]);
					
					if(count($rsDocumentReceived)==0)
					{
						$ctr++;
						$recordSender = $objDocDetail->getOriginUnactedDoc($rsOfficeRecord[$i]['documentId']);
						$date = new Date($dateAdded);  
						$this->Row(array($ctr,$date->getAsStr("d M Y"),$rsOfficeRecord[$i]['documentId'], $objRecordDetail->getDocumentType($rsOfficeRecord[$i]['documentTypeId']),$recordSender,$objRecordDetail->getDocumentStatus($rsOfficeRecord[$i]['status']),$rsOfficeRecord[$i]['subject']),0);
						
						$this->SetFont('Arial','',8);
						$w2 = array(15,30,40,30,60,30,110);
						$Ln2 = array('L','L','L','L','L','L','C');
						$this->SetWidths($w2);
						$this->SetAligns($Ln2);
						$this->Row(array("","","","","","","HISTORY"),0);
						$rsOfficeDocumentHistory2 = $objDocDetail->getDocumentUnactedHistory($rsOfficeRecord[$i]['documentId'], $_SESSION['office']);
						
						for($i2=0;$i2<count($rsOfficeDocumentHistory2);$i2++) 
						{	
							// display history details including elapse time
						
							if($objRecordDetail->checkDocumentHistoryReply($rsOfficeDocumentHistory2))
							{
								
								$rsOfficeDocumentHistoryDetails = $objRecordDetail->getDocumentHistoryDetails($rsOfficeDocumentHistory2[$i2]['historyId']);
										$historyColumn = "";
										
										$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsOfficeDocumentHistoryDetails[0]['senderId'], $rsOfficeDocumentHistoryDetails[0]['senderUnit']);
										$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsOfficeDocumentHistoryDetails[0]['recipientId'], $rsOfficeDocumentHistoryDetails[0]['recipientUnit']);
										//$noofdayspassed = floor((time() - strtotime($rsOfficeDocumentHistoryDetails[0]['dateAdded']))/86400);
										$dateAdded2 = substr($rsOfficeDocumentHistoryDetails[0]['dateAdded'],0,10);
										$elapsedtime = $this->getElapsedTimeFromPresent($dateAdded2);
										
										$historyColumn .= "Date Sent: ".$dateAdded2."\n";
										$historyColumn .= "Action Taken : ".$rsOfficeDocumentHistoryDetails[0]['actionTakenDesc']."\n";
										$historyColumn .= "From : ".$currentActionSender."\n";
										$historyColumn .= "To : ".$currentActionRecipient."\n";
										//$historyColumn .= "Received By: ".$rsOfficeDocumentHistoryDetails[0]['receivedBy']."\n";
										$historyColumn .= "Action Required : ".$rsOfficeDocumentHistoryDetails[0]['actionDesc']."\n";
										$historyColumn .= "Remark : ".$rsOfficeDocumentHistoryDetails[0]['remarks']."\n";
										$historyColumn .= "Elapsed time: ".$elapsedtime."\n";
										$historyColumn .= "-----------------------------------------------------------"."\n";
									/*	
										if(count($rsRecordHistory)>1 && $linectr>1)
										{
											$historyColumn .= "-----------------------------------------------------------"."\n";
											$linectr=$linectr-1;
										}	
									*/	
										$w2 = array(15,30,40,30,60,30,110);
										$Ln2 = array('L','L','L','L','L','L','L');
										$this->SetWidths($w2);
										$this->SetAligns($Ln2);
										$this->SetFont('Arial','',8);
										$this->Row(array("","","","","","",$historyColumn),0);
										
			
								
									
									$this->SetFont('Arial','',9);

								
							} // if($objRecordDetail->checkDocumentHistoryReply($rsOfficeDocumentHistory))
							
						} // for($i2=0;$i2<count($rsRecordHistory);$i2++) 
					}
				}
		
			}	
		}
		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");

		
	}	
	
	function Header()
	{	
			include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->ReportType = $objReport->getReportName($_GET['reportType']);
		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		//$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C");
		$this->Cell(30,7,"DATE ENCODED",1,0,"C");
		$this->Cell(40,7,"DOC ID",1,0,"C");
		$this->Cell(30,7,"DOC TYPE",1,0,"C");
		$this->Cell(60,7,"SENDER",1,0,"C");
		$this->Cell(30,7,"STATUS",1,0,"C");
		$this->Cell(110,7,"SUBJECT",1,0,"C");

		$this->Ln(10);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getElapsedTimeFromPresent($datefrom)
	{
		
		
		list($year, $month, $day) = explode("-", date('Y-m-d'));
		list($hour, $min, $sec) = explode("-", date('G-i-s'));
		
		$epoch_1 = mktime($hour, ltrim($min,'0'), ltrim($sec,'0'), $month, $day, $year);
		$epoch_2 =  $this->mysqldatetime_to_timestamp($datefrom);
		


		$diff_seconds  = $epoch_1 - $epoch_2;
		$diff_weeks    = floor($diff_seconds/604800);
		$diff_seconds -= $diff_weeks   * 604800;
		$diff_days     = floor($diff_seconds/86400);
		$diff_seconds -= $diff_days    * 86400;
		$diff_hours    = floor($diff_seconds/3600);
		$diff_seconds -= $diff_hours   * 3600;
		$diff_minutes  = floor($diff_seconds/60);
		$diff_seconds -= $diff_minutes * 60;
		
		if($diff_weeks!=0)
			return "$diff_weeks weeks, $diff_days days, $diff_hours hours, $diff_minutes minutes and $diff_seconds seconds.";
		elseif($diff_days!=0)	
			return "$diff_days days, $diff_hours hours, $diff_minutes minutes and $diff_seconds seconds.";
		elseif($diff_days==0 && $diff_hours!=0)
			return "$diff_hours hours, $diff_minutes minutes and $diff_seconds seconds.";	
		elseif($diff_hours==0 && $diff_minutes!=0)	
			return "$diff_minutes minutes and $diff_seconds seconds.";
		elseif($diff_minutes==0)	
			return "$diff_seconds seconds.";
		


	}
	
	function mysqldatetime_to_timestamp($datetime = "")
	{
	  // function is only applicable for valid MySQL DATETIME (19 characters) and DATE (10 characters)
	  $l = strlen($datetime);
		if(!($l == 10 || $l == 19))
		  return 0;
	
		//
		$date = $datetime;
		$hours = 0;
		$minutes = 0;
		$seconds = 0;
	
		// DATETIME only
		if($l == 19)
		{
		  list($date, $time) = explode(" ", $datetime);
		  list($hours, $minutes, $seconds) = explode(":", $time);
		}
	
		list($year, $month, $day) = explode("-", $date);
	
		return mktime($hours, $minutes, $seconds, $month, $day, $year);
	}
	
	function getReportType()
	{
		return $this->ReportType;
	}

}
?>