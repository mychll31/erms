	<? 
session_start();
include_once("General.class.php");
class Report extends General {

	function getReportTypes()
	{
//pao
		if($_SESSION['userType']==3){		
			$sql = "SELECT * FROM tblReports WHERE reportCode = 'LOR' or reportCode = 'NDP' or reportCode = 'RA' or reportCode = 'RP'  ORDER BY reportName";
		}
		else
		{
			$sql = "SELECT * FROM tblReports ORDER BY reportName";
		}


		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	
	function getReportName($txtReportCode)
	{
		$sql = "SELECT reportName FROM tblReports WHERE reportCode='".$txtReportCode."'";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0]['reportName'];	
	}
	
	// Begin get reporttype name by id - Report with Barcode
	function getReportTypeById($typeid)
	{
		$sql = "SELECT *  FROM tblDocumentType WHERE documentTypeId = ".$typeid;
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0]['documentTypeDesc'];	
	}
	// End get reporttype name by id - Report with Barcode

	function comboYear($t_intYear='')
	{   
		if(strlen($t_intYear) == 0)   //if no month, the default value is the current month
		{
			$t_intYear = date('Y');
		}
		for($intCounter=2003; $intCounter<=date('Y'); $intCounter++)
		{
			if($t_intYear == $intCounter)
			{
				echo "<option value='$intCounter' selected>$intCounter</option>";	
			}
			else
			{
				echo "<option value='$intCounter'>$intCounter</option>";
			}
		}
	}


function comboMonth($t_intMonth='')
	{
		if(strlen($t_intMonth) == 0)   //if no month, the default value is the current month
		{
			$t_intMonth = date('n');
		}
		echo "<option value='' > </option>";
		for($intCounter=1; $intCounter<=12; $intCounter++)
		{
			$strMonthName = $this->intToMonthName($intCounter);
			if($t_intMonth == $intCounter)
			{
				echo "<option value='$intCounter' selected>$strMonthName</option>";	
			}
			else
			{
				echo "<option value='$intCounter'>$strMonthName</option>";
			}											
		}
	}

function intToMonthName($t_intMonth)
	{
		$arrMonths = array(1=>"Jan", 2=>"Feb", 3=>"Mar", 
						4=>"Apr", 5=>"May", 6=>"Jun", 
						7=>"Jul", 8=>"Aug", 9=>"Sep", 
						10=>"Oct", 11=>"Nov", 12=>"Dec");
		return $arrMonths[$t_intMonth];
	}

	//Select all maintained document, added by office and between given dates
	function getReportDocumentMLMD($date1, $date2)
	{
		$sql = "SELECT * , tblHistory.dateAdded AS  'dateofissue' FROM tblHistory
				left join tblDocument on tblDocument.documentId=tblHistory.documentId
				left join tblLocation ON tblLocation.documentId = tblHistory.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblHistory.documentId
					where tblHistory.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 1)
				group by tblHistory.documentId
				order by historyId DESC";

		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	
	//select all retained report between given dates, where the last recipient is itd
	function getReportDocumentMLRD($date1, $date2, $controlleddoc)
	{
		$sql = "SELECT * , tblHistory.dateAdded AS  'dateofissue' FROM tblHistory
				left join tblDocument on tblDocument.documentId=tblHistory.documentId
				left join tblDocumentType on tblDocumentType.documentTypeId=tblDocument.documentTypeId
				left join tblLocation ON tblLocation.documentId = tblHistory.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblHistory.documentId
					where tblHistory.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 2)
				group by tblHistory.documentId
				order by historyId DESC";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	//select all reports created by itd
	function getReportDocumentMLF($date1, $date2)
	{
		$sql = "SELECT * , tblDocument.documentDate AS  'dateofissue' FROM tblDocument
				left join tblDocumentType on tblDocumentType.documentTypeId=tblDocument.documentTypeId
				left join tblLocation ON tblLocation.documentId = tblDocument.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblDocument.documentId
					where tblDocument.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 3)";
				// and tblControlledDocument.lastupdated_date between '".$date1."' and '".$date2."'";

		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}

	//select all reports created by itd
	function getReportDocumentMLER($date1, $date2)
	{
		$sql = "SELECT * , tblDocument.documentDate AS  'dateofissue' FROM tblDocument
				left join tblDocumentType on tblDocumentType.documentTypeId=tblDocument.documentTypeId
				left join tblLocation ON tblLocation.documentId = tblDocument.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblDocument.documentId
					where tblDocument.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 4)";
				
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}

	//select all reports created by itd
	function getReportDocumentMLIR($date1, $date2)
	{
		$sql = "SELECT * , tblHistory.dateAdded AS  'dateofissue', tblHistory.senderId as 'er_sender' FROM tblHistory
				left join tblDocument on tblDocument.documentId=tblHistory.documentId
				left join tblDocumentType on tblDocumentType.documentTypeId=tblDocument.documentTypeId
				left join tblLocation ON tblLocation.documentId = tblHistory.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblHistory.documentId
					where tblHistory.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 5)
				and tblControlledDocument.lastupdated_date between '".$date1."' and '".$date2."'
				group by tblHistory.documentId
				order by historyId DESC";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}

	//select all reports created by itd
	function getReportDocumentMLOD($date1, $date2)
	{
		$sql = "SELECT * , tblDocument.dateAdded AS  'dateofissue' FROM tblDocument
				left join tblDocumentType on tblDocumentType.documentTypeId=tblDocument.documentTypeId
				left join tblLocation ON tblLocation.documentId = tblDocument.documentId
				left join tblControlledDocument on tblControlledDocument.documentId=tblDocument.documentId
					where tblDocument.documentId in (SELECT documentId FROM tblControlledDocument where addedby_office = '".$_SESSION['office']."'
				and controlleddoc = 6)
				and tblControlledDocument.lastupdated_date between '".$date1."' and '".$date2."'";

		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}

	//get drawer label
	function getDrawerLabels($cont1, $cont2, $cont3){
		$sql = "SELECT
					(SELECT label FROM tblContainer WHERE containerId=".$cont1.") as cont1,
					(SELECT label FROM tblContainer2 WHERE container2Id=".$cont2.") as cont2,
					(SELECT label FROM tblContainer3 WHERE container3Id=".$cont3.") as cont3";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if($rs[0]['cont1'] != '' or $rs[0]['cont2'] != '' or $rs[0]['cont3'] != '')
			return $rs[0]['cont1'].' > '.$rs[0]['cont2'].' > '.$rs[0]['cont3'];
		else
			return '';
	}
	
	
}//end Class
?>