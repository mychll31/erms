<?php
//(ISO code. e.g., PHL for Philippines)
define("COUNTRY_CODE", "PHL");

// SET THE LAST GLIDE NUMBER USED
define("LAST_GLIDE", 100);

// SET DIRECTORY FOR FILE UPLOADING
define("UPL_FLD","uploads/");

define("MSG_INV_FILETYPE","Invalid file type. File with .doc, .pdf, .xls, .txt, .gif, .jpeg or .png extension is only allowed.");

define("MAX_FILE_SIZE", 3000000);  // 3925kB


// VARIABLES FOR SYSTEM ALERTS/MESSAGES

define("MSG_ADD_EXS", "Record already exist.");

define("MSG_ADD_SUCCESS", "Record successfully added.");

define("MSG_ADD_UNSUCCESS", "Record unsuccessfully added.");

define("MSG_USER_EXS", "Username already exist. Please enter another username.");

define("MSG_UPD_SUCCESS", "Record successfully updated.");

define("MSG_UPD_UNSUCCESS", "No update has been made.");

define("MSG_DEL_USING", "Record cannot be deleted. Some tables are using it.");

define("MSG_DEL_SUCCESS", "Record successfully deleted.");

define("MSG_REM_SUCCESS", "Record successfully removed.");

define("MSG_REM_UNSUCCESS", "Record unsuccessfully removed.");

define("MSG_DEL_SYS_MALF", "Record unsuccessfully deleted due to system malfunction.");

define("MSG_ADD_CODE_EXS", "Code already used. Please enter another code.");

define("MSG_VIEW_NOREC", "No records added yet.");

define("MSG_SEARCH_NOREC", "No search results.");


define("MSG_LOGIN_UNSUCCESS", "Login failed. Invalid username/password combination.");

define("MSG_LOGIN_SUCCESS", "You have successfully logged in.");

define("MSG_LOGOUT", "You have been Logged out. Thank you.");


define("MSG_ERR_FILL_IN", "Please fill in ");

define("MSG_ERR_EXCEEDS", " exceeds ");

define("MSG_ERR_NOT_NUM", " is not a valid number.");

define("MSG_ERR_NOT_YR_VALID", "Year should not be ahead of current year or later than year 2000.");

define("MSG_ERR_SELECT", "Please select ");

define("MSG_ERR_ACCESS_INV", "You have no rights to modify the record");


define("MSG_ERR_ACTION_DEL", "You cannot delete this record. Action has already been made for this record.");



// DESIGN
define("TBL_BORDER_GREEN", "#339933");

define("TBL_HEADER_BACK_GREEN", "#99CC99");

define("TBL_ALT_ROW_YELLOW", "#FFFFCC");


?>