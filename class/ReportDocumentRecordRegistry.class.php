<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
include_once("Date.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentRecordRegistry extends FPDF
{	

	var $ReportType;

	function generateReport()
	{
		include_once("Records.class.php");
		include_once("Report.class.php");
		include_once("General.class.php");

		$objGen = new General;
		$objDocDetail = new DocDetail;
		$objRecordDetail = new Records;
		$objReport = new Report;

		$ReportType = $objReport->getReportTypeById($_GET['docType']);
		$rsRecordDetail = $objRecordDetail->getToSummary($_GET['dateFrom'], $_GET['dateTo'], $_GET['docType'], $_SESSION['office']);
		$this->SetFont('Arial','',8);
		$w2 = array(24,35,34,34,50,100,50);
		$Ln2 = array('C','C','C','C','C','L','L');
		$this->SetWidths($w2);
		$this->SetAligns($Ln2);
		$no = 1;
		
		$bsp = array();
		if($_GET['docType'] == '415'){
			//BSP
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 5) != 'BSP-A' and substr($data['docNum'], 0, 5) != 'BSP-T' and substr($data['docNum'], 0, 5) != 'BSP-G') {
					array_push($bsp, $data);
				}
			endforeach;
			//BSP-APPLICANT
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 5) === 'BSP-A') {
					array_push($bsp, $data);
				}
			endforeach;
			//BSP-TER
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 5) === 'BSP-T') {
					array_push($bsp, $data);
				}
			endforeach;
			//BSP-GEN FILES
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 5) === 'BSP-G') {
					array_push($bsp, $data);
				}
			endforeach;
			$rsRecordDetail = $bsp;
		}

		$hrdp = array();
		if($_GET['docType'] == '411'){
			// RE ENTRY
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 13) === 'HRDP-RE-ENTRY') {
					array_push($hrdp, $data);
				}
			endforeach;
			// MPM TED
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 12) === 'HRDP-MPM TED') {
					array_push($hrdp, $data);
				}
			endforeach;
			// MPA
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 8) === 'HRDP-MPA') {
					array_push($hrdp, $data);
				}
			endforeach;
			// FULLY PAID
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 15) === 'HRDP-FULLY PAID') {
					array_push($hrdp, $data);
				}
			endforeach;
			// RMS
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 8) === 'HRDP-RMS') {
					array_push($hrdp, $data);
				}
			endforeach;
			// AD-REF
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 11) === 'HRDP-AD REF') {
					array_push($hrdp, $data);
				}
			endforeach;
			// GIR
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 8) === 'HRDP-GIR') {
					array_push($hrdp, $data);
				}
			endforeach;
			// HRDP
			foreach($rsRecordDetail as $data):
				if(substr($data['docNum'], 0, 13) != 'HRDP-RE-ENTRY' and substr($data['docNum'], 0, 12) != 'HRDP-MPM TED' and substr($data['docNum'], 0, 8) != 'HRDP-MPA' and substr($data['docNum'], 0, 15) != 'HRDP-FULLY PAID' and substr($data['docNum'], 0, 8) != 'HRDP-RMS' and substr($data['docNum'], 0, 11) != 'HRDP-AD REF' and substr($data['docNum'], 0, 8) != 'HRDP-GIR') {
					array_push($hrdp, $data);
				}
			endforeach;
			$rsRecordDetail = $hrdp;
		}

		foreach($rsRecordDetail as $data):
			$doctype = $objGen->getDocTypeByDocId($data["documentId"]);
			$location = $objGen->getLocation($data["documentId"]);
			if($location)
				$locname = $location['label1'].' > '.$location['label2'].' > '.$location['label3'];
			else
				$locname = '';

			$this->Row(array($no++,$data['documentId'], $doctype['documentTypeAbbrev'], $data['docNum'], $doctype['retentionPeriod'], $data['subject'], $locname) ,1);
		endforeach;

	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);				

		if($_SESSION['userType']==3){
			$officeCode = explode('~',$objDocDetail->getOfficeCode2($objDocDetail->get('userID')));
			$officeName = $officeCode[0];		
		}else{
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		}

		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,'MASTER LIST OF RECORDS REGISTRY', 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($dateFrom->getAsStr("d M Y") == $dateTo->getAsStr("d M Y"))
			$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',11);
		$this->Cell(24,7,"ITEM NO",1,0,"C",1);
		$this->Cell(35,7,"DOC ID",1,0,"C",1);
		$this->Cell(34,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(34,7,"DOC NUM",1,0,"C",1);
		$this->Cell(50,7,"RETENTION PERIOD",1,0,"C",1);
		$this->Cell(100,7,"SUBJECT",1,0,"C",1);
		$this->Cell(50,7,"LOCATION",1,0,"C",1);
		$this->ln();
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>