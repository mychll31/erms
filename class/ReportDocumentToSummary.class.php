<?php
ob_start();
include('pdf_html_barcode.php');
$content = ob_get_clean();

require_once('html2pdf/html2pdf.class.php');
try
{
	$html2pdf = new HTML2PDF('L', 'A4', 'fr');
	$html2pdf->pdf->SetDisplayMode('real');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('exres.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>