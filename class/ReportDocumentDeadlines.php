<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentDeadlines extends FPDF
{	
	function generateReport()
	{
	
		/*
		//hazards profile
		include_once("ProgressReport.class.php");
		$newProgressReport = new ProgressReport;
		include_once("Disaster.class.php");	
		$newDisaster = new Disaster;
		$rsReport = $newProgressReport->viewHazardsProfile("","",$_GET['GID'],$_GET['RID'],"");
		$rsProfile = $newProgressReport->getDisasterProfile($_GET['GID']);
		$rsEvent = $newProgressReport->getDisasterEvent($rsProfile[0]['eventCode']);
		*/


		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
		
		$w = array(15,30,30,30,100,50);
		$Ln = array('L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									
		
			
			if($rsRecordDetail[$i]['deadline']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['deadline']<=$_GET['dateTo'])
			{
				$ctr++;
				$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['deadline']),1);
			}	
		}
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);
		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$objReport->getReportName($_GET['reportType']), 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->Cell(15,7," ",1,0,"C",1);
		$this->Cell(30,7,"DOC ID",1,0,"C",1);
		$this->Cell(30,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(30,7,"STATUS",1,0,"C",1);
		$this->Cell(100,7,"SUBJECT",1,0,"C",1);
		$this->Cell(50,7,"DEADLINE",1,0,"C",1);
		$this->Ln(10);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
}
?>