<?php
@session_start();
//ini_set("session.gc_maxlifetime","600");
include_once("ERMS_define.php");
include_once("MySQLHandler2.class.php");
include_once("General.class.php");
class Login extends General
{
	public $blnAgencyUser=false;
	protected $t_arrRs=array();
	
	function validateAccount(){
	  include("Securelogin.class.php");
	  $auth = new securelogin;
	  $idle = false;
	  $className = 'Login';
	  $auth->handler['checklogin'] = array($this,'checkLogin');
	  $auth->post_index = array('user' => 't_strUsername' ,'pass' => 't_strPassword');
	  $auth->use_cookie = (isset($_POST['RememberMeTextBox']) || (isset($_COOKIE['t_strUsername']) && trim($_COOKIE['t_strUsername']) != ""))?true:false;
	  $auth->use_session = true;
	  $auth->use_auth = false;

	  if(@$_GET['logout']) $auth->clearlogin();
	  if ($auth->haslogin(true)) {
		  
		  ###########################################################
		  ## Login Expire Time                                     ##
		  ###########################################################
		  $idle_time = 3600;									   ##
		  if(isset($_SESSION['timeout']) ) {					   ##
		   $session_life = time() - $_SESSION['timeout'];          ##
		   if($session_life > $idle_time){                         ##
			 $auth->clearlogin();                                  ##
			 $idle = true;                                         ##
		   }                                                       ##
		  }                                                        ##
		  ###########################################################
		  //set all needed sessions
		  if($this->get('userID') == ''){


//			$t_strOffice = $this->getUserOffice($this->t_arrRs[0]["empNumber"]);
//			$t_strGroup = $this->getOfficeGroupEmployee($this->t_arrRs[0]["empNumber"]);
//			$t_strUserUnit = ($userType=='1' || $userType == '2')?$this->getUserUnit($this->t_arrRs[0]["empNumber"]):"employee";

			$t_strOffice = $this->getUserOffice($this->t_arrRs[0]["empNumber"]);
			$t_strGroup = $this->getOfficeGroupEmployee($this->t_arrRs[0]["empNumber"]);
			$userType =  ($this->checkAdmin($this->t_arrRs[0]['empNumber'])?1:($this->checkHead($this->t_arrRs[0]['empNumber'])?2:3));	

			if ($t_strGroup[0]['groupCode']!=""){
					$t_strGroupCode = $t_strGroup[0]['groupCode'];
					$t_strUserUnit = "Project";
			} else	{	
					$t_strGroupCode = "";
					$t_strUserUnit = ($userType=='1' || $userType == '2')?$this->getUserUnit($this->t_arrRs[0]["empNumber"]):"employee";
				}
					
//pao 031715 if agency use agencyCode as office						
			if($this->blnAgencyUser){
				 $t_strOffice= $this->t_arrRs[0]["agencyCode"];
				 
			}

			$userType =  ($this->checkAdmin($this->t_arrRs[0]['empNumber'])?1:($this->checkHead($this->t_arrRs[0]['empNumber'])?2:3));
			$module = ($userType==1?'records':($userType==2?'custodian':'employee'));
			$arrSession = array(
				  'auth_user' => $auth->username,
				  'auth_pass' => $auth->passhash,
				  'empNum' => $this->t_arrRs[0]['empNumber'],
				  'strLoginName' => $t_strUsername,
				  'userType' => $userType,
				  'userID' => $this->t_arrRs[0]["empNumber"],
				  'blnAgencyUser' => $this->blnAgencyUser,
				  'userOffice' => $t_strOffice, 
				  'office' => $t_strOffice,
				  'grp' => $t_strGroupCode,  				  
				  'userUnit' => $t_strUserUnit,
				  'module' => $module,
				  'timeout' => time()		
			);

				  //'userOffice' => $t_strGroup[0]['groupCode']!=""?$t_strGroup[0]['groupCode']:$t_strOffice,
				  //'office' => $t_strGroup[0]['groupCode']!=""?$t_strGroup[0]['groupCode']:$t_strOffice,
			$auth->savelogin($arrSession);
		  }
		  echo "<script language='javascript'>location.href='main.php?mode=show';</script>"; 
	  }
	  if(isset($_POST['Submit']) && trim($_POST['Submit'])!='')
	   return "You have entered invalid information. Please re-enter your Username and Password.";	
	}
	function checkLogin($t_strUsername,$t_strPassword)
	{
		$passHash=md5($t_strPassword);

		# prevent sql injenction
		if (strpos($t_strUsername, '\'') !== false) {
			return false;
		}

		$sql = "SELECT tblEmpAccount.empNumber,tblEmpAccount.userName,tblEmpAccount.userPassword 
				FROM `tblEmpAccount` 
					INNER JOIN tblEmpPosition 
						ON tblEmpAccount.empNumber = tblEmpPosition.empNumber 
				WHERE 
					tblEmpPosition.statusOfAppointment = 'In-Service' 
					AND tblEmpAccount.userName = '$t_strUsername' 
					AND tblEmpAccount.userPassword = '".$t_strPassword."'";				
				 
		$sql1= new MySQLHandler2();
		$sql1->init();
		$rs=$sql1->Select($sql);
		$this->blnAgencyUser = FALSE;
		if(count($rs)==0)
		{
		  $sql = "SELECT empNumber,userName,userPassword,agencyCode FROM `tblUserAccount` 
				   WHERE userName = '$t_strUsername' 
				   AND userPassword = '".$t_strPassword."'";
		  $sql2= new MySQLHandler();
		  $sql2->init();
		  $rs=$sql2->Select($sql);
		  $this->blnAgencyUser = $rs[0]['agencyCode']==0?$this->blnAgencyUser:TRUE;
//pao 012815   
		  if ($this->blnAgencyUser) $_SESSION["office"] = $rs[0]['agencyCode']; 

/*		   echo "<script type='text/javascript'>alert('hello ".$_SESSION["office"]. "');</script>";  */		

		}
		if(count($rs)){
			$this->t_arrRs = $rs;
			return true;
		}
		return false;
	}
	
	function checkHead($t_strUserID)
	{
		$sql = "SELECT * FROM tblCustodian WHERE empNumber = '$t_strUserID'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs))	
			return 1;
	}
	
	function checkAdmin($t_strUserID)
	{
		$sql = "SELECT * FROM tblCustodian WHERE empNumber = '$t_strUserID' AND admin='1'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs))	
			return 1;
	}

	function getUserUnit($t_strUserID)
	{
	$t_strOffice = $this->checkOffice($t_strUserID);
	$t_strOfficeCode = $this->getOffice($t_strUserID);

	switch($t_strOffice)
	{	
		case "Group1":
			$t_strField = Group1;
			break;
		case "Group2":
			$t_strField = Group2;
			break;
		case "Group3":
			$t_strField = Group3;
			break;			
		case "Group4":
			$t_strField = Group4;
			break;
		case "Group5":
			$t_strField = Group5;
			break;	
		default:
			return 0; break;
	}
	return $t_strField;
	}

	function getUserOffice($t_strUserID)
	{
	$t_strOffice = $this->checkOffice($t_strUserID);
	$t_strOfficeCode = $this->getOffice($t_strUserID);

	return ($t_strOfficeCode);
	}
	
	function checkOffice($t_strUserID)
	{
	$sql = "SELECT group5,group4,group3,group2,group1 FROM tblEmpPosition WHERE empNumber='".$t_strUserID."'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))		
		{
		if($rs[0]['group1']<>"")
			$t_strOffice = "Group1";
		if($rs[0]['group2']<>"")
			$t_strOffice = "Group2";
		if($rs[0]['group3']<>"")
			$t_strOffice = "Group3";		
		if($rs[0]['group4']<>"")
			$t_strOffice = "Group4";
		if($rs[0]['group5']<>"")
			$t_strOffice = "Group5";
		}
	else
	{
		$sql = "SELECT officeCode FROM tblUserAccount WHERE empNumber='".$t_strUserID."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs))		
		{
		$t_strOffice = $rs[0]['officeCode'];
		}
	}
	return $t_strOffice;	
	}
	
	
	function getOffice($t_strUserID)
	{
	$sql = "SELECT group5,group4,group3,group2,group1 FROM tblEmpPosition WHERE empNumber='".$t_strUserID."'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs)>0)		
		{
		if($rs[0]['group1']<>"")
			$t_strOffice = $rs[0]['group1'];
		if($rs[0]['group2']<>"")
			$t_strOffice = $rs[0]['group2'];
		if($rs[0]['group3']<>"")
			$t_strOffice = $rs[0]['group3'];		
		if($rs[0]['group4']<>"")
			$t_strOffice = $rs[0]['group4'];
		if($rs[0]['group5']<>"")
			$t_strOffice = $rs[0]['group5'];
		}
	else
	{
		$sql = "SELECT officeCode FROM tblUserAccount WHERE empNumber='".$t_strUserID."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		$t_strOffice = $rs[0]['officeCode'];
	}
	
	return $t_strOffice;	
	}	
	
	function getUserGroup($t_strUserID)
	{
		$sql = "SELECT groupCode FROM tblCustodian WHERE empNumber='".$t_strUserID."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0]['groupCode'];
	}
}
?>