<?
include_once("MySQLHandler.class.php");
include_once("Incoming.class.php");

class Outgoing extends Incoming
{

###########################################
# Function:    addOutDocument
# Parameters:  formFields/value , docStat (0 incoming, 1 outgoing, 2 intra office
# Return Type: Boolean
# Description: returns insert result. 
###########################################

function addOutDocument($arFields,$oldDocument=0)
{
if(trim($arFields['t_strDocType'])=="") $docTypeId="-1";
else
{
	$rsDocDetail=$this->getDocTypeFromAbbrev($arFields['t_strDocType']);
	if( $rsDocDetail[0]['documentTypeId']=="") 
		{
		$this->msg = '<font color="red">Invalid Document Type</font>';
		return 0;
		}
	else $docTypeId=$rsDocDetail[0]['documentTypeId'];

}
$docTypeId=$arFields["cmbDocType"];//addded for combo document type

// start of validation
		if(!$this->checkValidId($arFields['t_strDocId'],'outg'))
		{
		$this->msg = '<font color="red">Invalid Document ID</font>';
		return 0;
		}
		
		if(!$this->check_valid_date($arFields['documentDateOutgoing']))
		{
		$this->msg = '<font color="red">Invalid Document Date</font>';
		return 0;
		}
		if(!$this->check_valid_date($arFields['deadlineOutgoing']))
		{
		$this->msg = '<font color="red">Invalid Date of Deadline</font>';
		return 0;
		}
		if($this->hasInvalidChar($arFields['t_strDocNum'],"!$%^*+={}\\|`~"))
		{
		$this->msg = '<font color="red">Invalid Document Number</font>';
		return 0;
		}

// end of validation
$groupCode=$arFields['cmbGroupCodeOutgoing'];
//i add ung sender;
$added_date= $oldDocument==1?"2011-01-01 00:00:00":date("Y-m-d H:i:s");
$confidential=($arFields['t_intConfidential']==""?0:1);
$documentOwner=($this->get("blnAgencyUser"))?trim($this->getOfficeCode2($this->get('userID'))):$arFields['documentOwner'];

	if(isset($arFields['t_intContainer2']))
	{
		$sql_location="INSERT INTO tblLocation (documentId, cabinetId, drawerId, containerId, addedby_unit, addedby_office, lastupdate_by, lastupdated_date)
						VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_intContainer']."', '".$arFields['t_intContainer2']."', '".$arFields['t_intContainer3']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Insert($sql_location);
	}

	if(isset($arFields['t_contdoc']))
	{
		$sql_insertCDoc="INSERT INTO tblControlledDocument (documentId, controlleddoc, copyno, copyholder, mannerOfDisposal, revisionno, personUnitResponsible, withrawalno, addedby_unit, addedby_office, lastupdated_by, lastupdated_date)
						VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_contdoc']."', '".$arFields['txt_conta_copyno']."', '".$arFields['txt_contb_copyholder']."', '".$arFields['txt_contc_mannerdisposal']."', '".$arFields['txt_contd_revisionno']."', '".$arFields['txt_conte_unitres']."', '".$arFields['txt_withrawal']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Insert($sql_insertCDoc);
	}

	$SQL="INSERT INTO tblDocument (documentId, referenceId,docNum, documentTypeId, subject, documentDate, deadline, remarks, confidential,addedById,addedByOfficeId,officeSig,originId , originGroupId, sender, dateAdded,status) VALUES('".$arFields['t_strDocId']."','".$arFields['t_strReferenceId']."','".$arFields['t_strDocNum']."','".$docTypeId."','".addslashes($arFields['t_strSubject'])."','".$arFields['documentDateOutgoing']."','".$arFields['deadlineOutgoing']."','".addslashes($arFields['t_strRemarks'])."','".$confidential."','".$this->get('userID')."','".$documentOwner."','','".$arFields['cmbOriginOutgoing']."','".$groupCode."','".$arFields['signatoryOutgoing']."','".$added_date."','1')"; //0 for incoming


$sql1= new MySQLHandler();
$sql1->init();	
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");
if($rs=$sql1->Insert($SQL))
	{
		$this->msg = MSG_ADD_SUCCESS;
		return 1;
	}
	else
	{
		$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'<br>'.$sql1->ERROR_MSG.'</font>';
		return 0;
	}
}


function updateOutDocument($arFields)
{
//i add ung sender;

if(trim($arFields['t_strDocType'])=="") $docTypeId="-1";
else
{
	$rsDocDetail=$this->getDocTypeFromAbbrev($arFields['t_strDocType']);
	if( $rsDocDetail[0]['documentTypeId']=="") 
		{
		$this->msg = '<font color="red">Invalid Document Type</font>';
		return 0;
		}
	else $docTypeId=$rsDocDetail[0]['documentTypeId'];

}
$docTypeId=$arFields["cmbDocType"];//addded for combo document type

// start of validation
		
		if(!$this->check_valid_date($arFields['documentDateOutgoing']))
		{
		$this->msg = '<font color="red">Invalid Document Date</font>';
		return 0;
		}
		if(!$this->check_valid_date($arFields['deadlineOutgoing']))
		{
		$this->msg = '<font color="red">Invalid Date of Deadline</font>';
		return 0;
		}
		if($this->hasInvalidChar($arFields['t_strDocNum'],"!$%^*+={}\\|`~"))
		{
		$this->msg = '<font color="red">Invalid Document Number</font>';
		return 0;
		}

// end of validation

$groupCode=$arFields['cmbGroupCodeOutgoing'];

$confidential=($arFields['t_intConfidential']==""?0:1);
$documentOwner=($this->get("blnAgencyUser"))?trim($this->getOfficeCode2($this->get('userID'))):$arFields['documentOwner'];

	# BEGIN UPDATE location if exists Update
	$sql_updateLocation="INSERT INTO tblLocation (documentId, cabinetId, drawerId, containerId, addedby_unit, addedby_office, lastupdate_by, lastupdated_date)
						VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_intContainer']."', '".$arFields['t_intContainer2']."', '".$arFields['t_intContainer3']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))
								ON DUPLICATE KEY UPDATE
								cabinetId = '".$arFields['t_intContainer']."', 
								drawerId = '".$arFields['t_intContainer2']."', 
								containerId = '".$arFields['t_intContainer3']."',
								lastupdate_by = '".$_SESSION['rs_empNumber']."',
								lastupdated_date = NOW() ";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs = $sql1->Insert($sql_updateLocation);
	$rs = $sql1->Update($sql_updateLocation);

	/* BEGIN add controlled document if exists update */
	$sql_updateControlledDoc="INSERT INTO tblControlledDocument (documentId, controlleddoc, copyno, copyholder, mannerOfDisposal, revisionno, personUnitResponsible, withrawalno, addedby_unit, addedby_office, lastupdated_by, lastupdated_date)
					VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_contdoc']."', '".$arFields['txt_conta_copyno']."', '".$arFields['txt_contb_copyholder']."', '".$arFields['txt_contc_mannerdisposal']."', '".$arFields['txt_contd_revisionno']."', '".$arFields['txt_conte_unitres']."', '".$arFields['txt_withrawal']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))
								ON DUPLICATE KEY UPDATE
								controlleddoc = '".$arFields['t_contdoc']."', 
								copyno = '".$arFields['txt_conta_copyno']."', 
								copyholder = '".$arFields['txt_contb_copyholder']."',
								mannerOfDisposal = '".$arFields['txt_contc_mannerdisposal']."',
								revisionno = '".$arFields['txt_contd_revisionno']."',
								personUnitResponsible = '".$arFields['txt_conte_unitres']."',
								withrawalno = '".$arFields['txt_withrawal']."',
								lastupdated_by = '".$_SESSION['rs_empNumber']."',
								lastupdated_date = NOW( ) ";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs = $sql1->Insert($sql_updateControlledDoc);
	$rs = $sql1->Update($sql_updateControlledDoc);

	$SQL="UPDATE tblDocument SET documentTypeId='".$docTypeId."',referenceId = '".$arFields['t_strReferenceId']."',subject='".addslashes($arFields['t_strSubject'])."', documentDate='".$arFields['documentDateOutgoing']."', deadline='".$arFields['deadlineOutgoing']."', remarks='".addslashes($arFields['t_strRemarks'])."',confidential='".$confidential."',officeSig='".$arFields['cmbOriginOutgoing']."',originGroupId='".$groupCode."',sender='".$arFields['signatoryOutgoing']."',docNum='".$arFields['t_strDocNum']."', addedByOfficeId='".$documentOwner."' WHERE documentId = '".$arFields['t_strDocId']."'";

$sql1= new MySQLHandler();
$sql1->init();	
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");
$rs=$sql1->Update($SQL);
if($rs===false)
{
	
	$this->msg = '<font color="red">'.MSG_UPD_UNSUCCESS.'</font>';
	return false;
}
else
{	
	if($rs==0)$this->msg = '<font color="red">'.MSG_UPD_UNSUCCESS.'</font>';
	else 	$this->msg = MSG_UPD_SUCCESS;
	return 1;
	
}
}
	//Begin updating container 2 and 3
	// File Container
	function getContainer($office,$Id)
	{
	if($Id=="") $sql="SELECT * FROM tblContainer WHERE officeCode='$office' ORDER BY label ASC";
	else $sql="SELECT * FROM tblContainer WHERE containerId='$Id' AND officeCode='$office'";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);
	return $rs;

	}

	// get 2nd Container
	function getContainerTwo($containerId,$id="")
	{
		if($id=="") $sql="SELECT * FROM tblContainer2 WHERE container=$containerId";
		else $sql="SELECT * FROM tblContainer2 WHERE container2Id=$id";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;

	}

	function getContainerThree($container2Id,$id="")
		{
		if($id=="")	$sql="SELECT * FROM tblContainer3 WHERE container2=$container2Id";
		else $sql="SELECT * FROM tblContainer3 WHERE container3Id=$id";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;

	}
	//End updating container 2 and 3
	
	//Additional for OriginRelatedRecord
	function getOriginRelatedRecord($docid){
		
		$query = "select *, @pv:=documentId as documentId from tblDocument join (select @pv:='$docid')tmp where referenceId=@pv";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs;
	}

	function getRelatedDocsByRefId($refid){
		
		$query = "SELECT * FROM  `tblDocument` WHERE  `documentId` IN (".$refid.")";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs;
	}

	function getRelatedDocsByParentDoc($docid){
		
		$query = "SELECT *  FROM `tblDocument` WHERE `referenceId` LIKE '%".$docid."%'";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs[0];
	}
}
?>