<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentCustom1 extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;

		// $this->SetWidths(array(10,30,20,45,25,25,25,25,27,35));
		// $this->SetAligns(array('C','L','C','L','L','L','L','L','L'));
		// $this->SetFont('Arial','',9);

		if($_GET['status'] != '2'):
			$title = $_GET['status'] == '0' ? 'INCOMING' : 'OUTGOING';
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=".$title.".xls");

			// BEGIN INCOMING/OUTGOING
			$ctr = 1;
			$incomingdocs = $objDocDetail->getIncomingDocs($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo'], $_GET['daterad'], $_GET['status']);
			echo '<table border=1>
					<tr><td colspan=13><center><h1>'.$title.'</h1></center></td></tr>
					<tr>
						<td><center><b>NO.</b></center></td>
						<td><center><b>DOC ID</b></center></td>
						<td><center><b>DOC TYPE</b></center></td>
						<td><center><b>SUBJECT</b></center></td>
						<td><center><b>SENDER</b></center></td>
						<td><center><b>DATE/TIME RECEIVED</b></center></td>
						<td><center><b>RECEIVED BY</b></center></td>
						<td><center><b>DATE/TIME PROCESS</b></center></td>
						<td><center><b>PROCESS BY</b></center></td>
						<td><center><b>SENT TO</b></center></td>
						<td><center><b>DATE/TIME RECEIVED (RECIPIENT)</b></center></td>
						<td><center><b>RECEIVED BY (RECIPIENT)</b></center></td>
						<td><center><b>REMARKS</b></center></td>
						<td><center><b>PROCESSING TIME</b></center></td>
					</tr>';
			
			foreach($incomingdocs as $inc):
				#doc sent by RMS
				$processdate = $objDocDetail->getProcessByDateSent($inc['documentId'], $_SESSION['office'], $inc['dateReceived']);
				// $rowspan = count($processdate) > 0 ? 'rowspan = "'.count($processdate).'"' : '';
				$inc['senderId'] = is_numeric($inc['senderId']) ? $inc['officeName'] : $inc['senderId'];
				echo '	<tr>
							<td><center>'.$ctr++.'</center></td>
							<td><center>'.$inc['documentId'].'</center></td>
							<td><center>'.$inc['documentTypeDesc'].'</center></td>
							<td>'.$inc['subject'].'</td>
							<td><center>'.$inc['senderId'].'</center></td>
							<td><center>'.$inc['dateReceived'].'</center></td>
							<td><center>'.$inc['receivedBy'].'</center></td>';
				
				if(count($processdate) > 0):
					$arrTemp = array();
					$ctrpdate = 0;
					if(count($processdate) > 1):
						echo '<td colspan=7><table border=1>';
						foreach($processdate as $key => $pdate):
							if($ctrpdate > 0):
								if($arrTemp['addedById'] == $pdate['addedById'] && $arrTemp['recipientId'] == $pdate['recipientId'] && substr($arrTemp['dateAdded'], 0,14) == substr($pdate['dateAdded'], 0,14) ):
									#if temp received by is not null
									if(count($processdate) == ($ctrpdate+1)):
										$pdate['recipientId'] = is_numeric($pdate['recipientId']) ? $pdate['officeName'] : $pdate['recipientId'];
										$processby1 = $objDocDetail->getEmployeeName($pdate['addedById']);
										$sentTo1 = $pdate['recipientId'];
										if (strpos($pdate['recipientId'], '-') !== false) {
										    $sentTo1 = $objDocDetail->getEmployeeName($pdate['recipientId']);
										}
										echo '<tr>
												<td><center>'.$pdate['dateAdded'].'</center></td>
												<td><center>'.$processby1.'</center></td>
												<td><center>'.$sentTo1.'</center></td>
												<td><center>'.$pdate['dateReceived'].'</center></td>
												<td><center>'.$pdate['receivedBy'].'</center></td>
												<td><center>'.$pdate['remarks'].'</center></td>
												<td><center>';
											 	  if($pdate['dateAdded']!=''):
											 	  		$recdate = $inc['dateReceived'];
											 	  		$procdate = $pdate['dateAdded'];
											 	  		$this->breakin_dates($recdate, $procdate);
											 	  endif;
											 echo '</center></td></tr>';
									endif;
									// echo '<tr>
									// 			<td>NOT SHOW <hr>key='.$key.' '.$pdate['addedById'].' arrTemp='.$arrTemp['receivedBy'].' receivedBy='.$pdate['receivedBy'].'</td>
									// 			<td>'.$pdate['officeName'].'</td>
									// 			<td>'.$pdate['dateReceived'].'</td>
									// 			<td>'.$pdate['receivedBy'].'</td>
									// 			<td>'.substr($pdate['dateAdded'], 0,14).' historyId='.$pdate['historyId'].'</td>
									// 		  </tr>';
								else:
									$arrTemp['recipientId'] = is_numeric($arrTemp['recipientId']) ? $arrTemp['officeName'] : $arrTemp['recipientId'];
									$processby2 = $objDocDetail->getEmployeeName($arrTemp['addedById']);
									$sentTo2 = $arrTemp['recipientId'];
									if (strpos($arrTemp['recipientId'], '-') !== false) {
									    $sentTo2 = $objDocDetail->getEmployeeName($arrTemp['recipientId']);
									}
									echo '<tr>
											 <td><center>'.$arrTemp['dateAdded'].'</center></td>
											 <td><center>'.$processby2.'</center></td>
											 <td><center>'.$sentTo2.'</center></td>
											 <td><center>'.$arrTemp['dateReceived'].'</center></td>
											 <td><center>'.$arrTemp['receivedBy'].'</center></td>
											 <td><center>'.$arrTemp['remarks'].'</center></td>
											 <td><center>';
											 	  if($arrTemp['dateAdded']!=''):
											 	  		$recdate = $inc['dateReceived'];
											 	  		$procdate = $arrTemp['dateAdded'];
											 	  		$this->breakin_dates($recdate, $procdate);
											 	  endif;
											 echo '</center></td></tr>';
								endif;
							endif;
							$arrTemp = $pdate;
							$ctrpdate++;
						endforeach;
						echo '</table></td></tr>';
					else:
						$processdate[0]['recipientId'] = is_numeric($processdate[0]['recipientId']) ? $processdate[0]['officeName'] : $processdate[0]['recipientId'];
						// get processing time
						$processing_time = '';
						if($processdate[0]['dateAdded']!=''):
							$processing_time = 'notnull';
						endif;
						$processby = $objDocDetail->getEmployeeName($processdate[0]['addedById']);
						$sentTo = $processdate[0]['recipientId'];
						if (strpos($processdate[0]['recipientId'], '-') !== false) {
						    $sentTo = $objDocDetail->getEmployeeName($processdate[0]['recipientId']);
						}
						echo '<td><center>'.$processdate[0]['dateAdded'].'</center></td>
							  <td><center>'.$processby.'</center></td>
							  <td><center>'.$sentTo.'</center></td>
							  <td><center>'.$processdate[0]['dateReceived'].'</center></td>
							  <td><center>'.$processdate[0]['receivedBy'].'</center></td>
							  <td><center>'.$processdate[0]['remarks'].'</center></td>
							  <td><center>';
							  if($processdate[0]['dateAdded']!=''):
							  		$recdate = $inc['dateReceived'];
							  		$procdate = $processdate[0]['dateAdded'];
							  		$this->breakin_dates($recdate, $procdate);
							  endif;
						echo '</center></td></tr>';
					endif;
					
				else:
					echo '<td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				endif;

			endforeach;
			echo '</table>';
			// END INCOMING
			exit;
		else:
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=SUMMARY-INCOMING-OUTGOING.xls");
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$total_created_incs = $objDocDetail->getTotalDocs($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo'], 0);
			$total_created_outgs = $objDocDetail->getTotalDocs($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo'], 1);
			$total_incs_co = $objDocDetail->getTotalDocs('ALLCO', $_GET['dateFrom'], $_GET['dateTo'], 0, 0);
			$total_incs_agencies = $objDocDetail->getTotalDocs('AGENCIES', $_GET['dateFrom'], $_GET['dateTo'], 0, 0);
			$total_outg_toAgencies = $objDocDetail->getTotalDocsByLibcap($_GET['dateFrom'], $_GET['dateTo'], 0);
			$total_outg_toLibcap = $objDocDetail->getTotalDocsByLibcap($_GET['dateFrom'], $_GET['dateTo'], 1);
			echo '<table border=1>
					<tr><td colspan=4><center><h1>SUMMARY OF INCOMING</h1></center></td></tr>
					<tr>
						<td><b><center>OFFICE</center></b></td>
						<td><b><center>RMS CREATED <br>(ALL TYPES)</center></b></td>
						<td><b><center>INCOMING <br>DOCUMENTS <br>(CENTRAL OFFICE)</center></b></td>
						<td><b><center>INCOMING <br>DOCUMENTS <br>(AGENCIES)</center></b></td>
					</tr>
					<tr>
						<td><center>'.$officeName.'</center></td>
						<td><center>'.count($total_created_incs).'</center></td>
						<td><center>'.count($total_incs_co).'</center></td>
						<td><center>'.count($total_incs_agencies).'</center></td>
					</tr>';
			echo '<table>';
			echo '<br><br>';
			echo '<table border=1>
					<tr><td colspan=4><center><h1>SUMMARY OF OUTGOING</h1></center></td></tr>
					<tr>
						<td><b><center>OFFICE</center></b></td>
						<td><b><center>RMS CREATED <br>(ALL TYPES)</center></b></td>
						<td><b><center>RELEASE <br>DOCUMENTS <br>(AGENCIES)</center></b></td>
						<td><b><center>RELEASE <br>DOCUMENTS <br>(LIBCAP)</center></b></td>
					</tr>
					<tr>
						<td><center>'.$officeName.'</center></td>
						<td><center>'.count($total_created_outgs).'</center></td>
						<td><center>'.count($total_outg_toAgencies).'</center></td>
						<td><center>'.count($total_outg_toLibcap).'</center></td>
					</tr>';
			echo '<table>';
		endif;
		die();
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($_GET['reportType']=='DMPR')
			if($_GET['dateFrom'] == $_GET['dateTo'])
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).')', 0, 1, 'C');
			else
				$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','30','20','45','25','25','25','25','27','35','10','30','20','45','25','25','25','25','27','35','10','30','20','45','25','25','25','25','27','35');
		$cell_caption = array("NO.","DOC ID","DOC TYPE","SUBJECT","DATE/TIME","DATE/TIME","PROCESS","DATE/TIME","RECEIVED","PROCESSING",
								" "," "," "," ","RECEIVED","PROCESSED","BY","RECEIVED","(RECIPIENT)","TIME",
								" "," "," "," ","","","","(RECIPIENT)","","");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'RL' : 'TRL',$key == 9 || $key == 19 ? 1 : 0,"C",1);
		endforeach;

		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Cell(90,4,'Person Responsible', 0, 0, 'C');
		$this->Cell(90,4,'Verified By', 0, 0, 'C');
		$this->Cell(90,4,'Approved for filing', 0, 1, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(90,5,'', 'B', 0, 'C');
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}

	function breakin_dates($dateRec, $dateProc)
	{
		$rDate = date('Y-m-d', strtotime($dateRec));
		$pDate = date('Y-m-d', strtotime($dateProc));

		$rTime_h = date('H', strtotime($dateRec));
		$rTime_m = date('i', strtotime($dateRec));
		$rTime_s = date('s', strtotime($dateRec));

		$pTime_h = date('H', strtotime($dateProc));
		$pTime_m = date('i', strtotime($dateProc));
		$pTime_s = date('s', strtotime($dateProc));

		if($rDate == $pDate):
			// get Seconds
			$borrow_m = 0;
			if($pTime_s < $rTime_s):
				$borrow_m = 1;
				$pTime_s = $pTime_s + 60;
			endif;
			$total_s = $pTime_s - $rTime_s;

			// get minutes
			$pTime_m = $pTime_m - $borrow_m;
			$borrow_h = 0;
			if($pTime_m < $rTime_m):
				$borrow_h = 1;
				$pTime_m = $pTime_m + 60;
			endif;
			$total_m = $pTime_m - $rTime_m;

			// get hour
			$pTime_h = $pTime_h - $borrow_h;
			$total_h = $pTime_h - $rTime_h;

			echo 'day/s: 0 hr/s: '.$total_h.' mins: '.$total_m.' secs: '.$total_s;

		elseif($rDate < $pDate):
			// echo 'MORE THAN 1 DAY';
			$begin = new DateTime($rDate);
			$end = new DateTime($pDate);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			$ctr = 0; $ctr_days = 0;

			foreach ($period as$dt):
				#remove first element
				if($ctr > 0):
					#check if weekend.
					if($dt->format("D") != 'Sat' && $dt->format("D") != 'Sun'):
						#check if holiday
						include_once("DocDetail.class.php");
						$objDocDetail = new DocDetail;
						$isholiday = $objDocDetail->isHoliday($dt->format("Y-m-d"));
						if(count($isholiday) < 1):
							$ctr_days++;
						endif;
					endif;
				endif;
				$ctr++;
			endforeach;

			// get Seconds
			$borrow_m = 0;
			if($pTime_s < $rTime_s):
				$borrow_m = 1;
				$pTime_s = $pTime_s + 60;
			endif;
			$total_s = $pTime_s - $rTime_s;

			// get minutes
			$pTime_m = $pTime_m - $borrow_m;
			$borrow_h = 0;
			if($pTime_m < $rTime_m):
				$borrow_h = 1;
				$pTime_m = $pTime_m + 60;
			endif;
			$total_m = $pTime_m - $rTime_m;

			// get hours
			$pTime_h = $pTime_h - $borrow_h;
			$borrow_d = 0;
			if($pTime_h < $rTime_h):
				if($ctr_days > 0):
					$borrow_d = 1;
					$pTime_h = $pTime_h + 24;
					$total_h = $pTime_h - $rTime_h;
				else:
					$total_h = 0;
				endif;
			endif;

			// get days
			$total_d = $ctr_days - $borrow_d;

			echo 'day/s: '.$total_d.' hr/s: '.$total_h.' mins: '.$total_m.' secs: '.$total_s;
		else:
			echo 'INVALID DATE';
		endif;

	}



	

}
?>
