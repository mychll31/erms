<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
include_once("Date.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentProfile extends FPDF
{	
	function generateReport()
	{
	
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		
		$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
		
		$w = array(15,30,40,30,60,30,110);
		$Ln = array('L','L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									

			
			if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'])
			{
					
				$ctr++;
				$recordSender = $objDocDetail->getSenderDocument($rsRecordDetail[$i]['documentId'], $rsRecordDetail[$i]['status']);
				
				 $date = new Date($rsRecordDetail[$i]['dateAdded']);  // $row['date'] = MySQL-TimeStamp, Unix-Timestamp or DateTime
 				
 				//$date->getAsStr( $format );
				
				$this->Row(array($ctr,$date->getAsStr("d M Y"),$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$recordSender,$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject']),0);
				
				
				$rsRecordHistory = $objDocDetail->getCurrentReference($rsRecordDetail[$i]['documentId']);
				
	
				if(count($rsRecordHistory)) //if document has history
				{
					// display HISTORY
					$this->SetFont('Arial','',8);
					$w2 = array(15,30,40,30,60,30,110);
					$Ln2 = array('L','L','L','L','L','L','C');
					$this->SetWidths($w2);
					$this->SetAligns($Ln2);
					$this->Row(array("","","","","","","HISTORY"),0);
					
					//$ctr=0;	
					$linectr = count($rsRecordHistory); 
					for($i2=0;$i2<count($rsRecordHistory);$i2++) 
					{									
							$historyColumn = "";
							//$ctr++;
							$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsRecordHistory[$i2]['senderId'], $rsRecordHistory[$i2]['senderUnit']);
							$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsRecordHistory[$i2]['recipientId'], $rsRecordHistory[$i2]['recipientUnit']);
							
							$historyColumn .= "From : ".$currentActionSender."\n";
							$historyColumn .= "Date Sent: ".$rsRecordHistory[$i2]['dateAdded']."\n";
							$historyColumn .= "Action Taken : ".$rsRecordHistory[$i2]['actionTakenDesc']."\n";
							$historyColumn .= "To : ".$currentActionRecipient."\n";
							$historyColumn .= "Received By: ".$rsRecordHistory[$i2]['receivedBy']."\n";
							$historyColumn .= "Action Required : ".$rsRecordHistory[$i2]['actionDesc']."\n";
							$historyColumn .= "Remark : ".$rsRecordHistory[$i2]['remarks']."\n";
							
							
							if(count($rsRecordHistory)>1 && $linectr>1)
							{
								$historyColumn .= "-----------------------------------------------------------"."\n";
								$linectr=$linectr-1;
							}	
							$w2 = array(15,30,40,30,60,30,110);
							$Ln2 = array('L','L','L','L','L','L','L');
							$this->SetWidths($w2);
							$this->SetAligns($Ln2);
							$this->SetFont('Arial','',8);
							$this->Row(array("","","","","","",$historyColumn),0);
							

					}
						
						$this->SetFont('Arial','',9);
					
				}
		
				$this->Cell(315,7,"",B,0,C);			
				$this->Ln(10);
			}	
		}
		
	}	
	
	function Header()
	{	
			include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$objReport->getReportName($_GET['reportType']), 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		//$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C");
		$this->Cell(30,7,"DATE ENCODED",1,0,"C");
		$this->Cell(40,7,"DOC ID",1,0,"C");
		$this->Cell(30,7,"DOC TYPE",1,0,"C");
		$this->Cell(60,7,"SENDER",1,0,"C");
		$this->Cell(30,7,"STATUS",1,0,"C");
		$this->Cell(110,7,"SUBJECT",1,0,"C");

		$this->Ln(10);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
}
?>