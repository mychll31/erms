<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
include_once("Date.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentRecordRegistry extends FPDF
{	

	var $ReportType;

	function generateReport()
	{
	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;

		$rsRecRegistry = $objDocDetail->getAllIncomingByDate($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo']);
		$this->SetFont('Arial','',8);
		$w2 = array(10,40,30,60,30,110,30);
		$Ln2 = array('L','L','L','L','C','L','C');
		$this->SetWidths($w2);
		$this->SetAligns($Ln2);
		$no = 1;
		foreach($rsRecRegistry as $recregistry):
			$this->Row(array($no++, $recregistry['documentId'], $recregistry['documentTypeDesc'], $recregistry['sender'], $recregistry['status']==1 ? 'Incoming' : 'Outgoing', $recregistry['subject'], ""),1);
		endforeach;

		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");

	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);				

		if($_SESSION['userType']==3){
			$officeCode = explode('~',$objDocDetail->getOfficeCode2($objDocDetail->get('userID')));
			$officeName = $officeCode[0];		
		}else{
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		}

		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->ReportType = $objReport->getReportName($_GET['reportType']);
		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		if($dateFrom->getAsStr("d M Y") == $dateTo->getAsStr("d M Y"))
			$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").')', 0, 1, 'C');
		else
			$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(10,7,"NO",1,0,"L",1);
		$this->Cell(40,7,"DOC ID",1,0,"C",1);
		$this->Cell(30,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(60,7,"SENDER",1,0,"C",1);
		$this->Cell(30,7,"STATUS",1,0,"C",1);
		$this->Cell(110,7,"SUBJECT",1,0,"C",1);
		$this->Cell(30,7,"REMARKS",1,1,"C",1);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>