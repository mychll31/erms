<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentActionsRecorded extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
	
		//include_once("Records.class.php");
		//$objRecordDetail = new Records;
		
		
		//$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
		
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		
		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$rsActionsRecorded = $objDocDetail->getActionsRecorded($_SESSION['userID'], $intMonth, $intYear);

		
		//$this->Cell(count($rsRecordDetail));
		$w = array(10,25,80,30,30,30,30,30);
		$Ln = array('L','L','L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	

		for($i=0;$i<count($rsActionsRecorded);$i++) 
		{									
		
			//$dateAdded = substr($rsRecordDetail[$i]['dateAdded'],0,10);
		//	if($dateAdded>=$_GET['dateFrom'] && $dateAdded<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==1)
			//{
				$ctr++;
				
				$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsActionsRecorded[$i]['senderId'], $rsActionsRecorded[$i]['senderUnit']);
				$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsActionsRecorded[$i]['recipientId'], $rsActionsRecorded[$i]['recipientUnit']);


				$this->Row(array($ctr,$rsActionsRecorded[$i]['documentId'], $rsActionsRecorded[$i]['subject'], $rsActionsRecorded[$i]['documentTypeDesc'],$rsActionsRecorded[$i]['dateSent'],
				$rsActionsRecorded[$i]['actionTakenDesc'].' ['.$currentActionSender.']',$rsActionsRecorded[$i]['actionDesc'].' ['.$currentActionRecipient.']',$rsActionsRecorded[$i]['remarks']),1);
			//}	
		}
				
		/*
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									
		
			$dateAdded = substr($rsRecordDetail[$i]['dateAdded'],0,10);
			if($dateAdded>=$_GET['dateFrom'] && $dateAdded<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==1)
			{
				$ctr++;
				$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
			}	
		}
		*/
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
			include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		$this->Cell(0,5,'Processed by: '.$objDocDetail->getProcessedbyName($_SESSION['userID']), 0, 1, 'L');
		$this->Ln(5);

		//dateFrom = new Date($_GET['dateFrom']);
		//$dateTo = new Date($_GET['dateTo']);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(10,7," ",1,0,"C",1);
		$this->Cell(25,7,"DOC ID",1,0,"C",1);
		$this->Cell(80,7,"SUBJECT",1,0,"C",1);
		$this->Cell(30,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(30,7,"ACTION DATE",1,0,"C",1);
		$this->Cell(30,7,"ACTION TAKEN",1,0,"C",1);
		$this->Cell(30,7,"ACTION REQUIRED",1,0,"C",1);
		$this->Cell(30,7,"REMARKS",1,0,"C",1);
		$this->Ln(10);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>