<?
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php"); // For HRMIS Connection
include_once("General.class.php");

class DocDetail extends General
{
 
###########################################
# Function:    getDocDetails
# Parameters:  N/A
# Return Type: Array
# Description: returns details of the document
###########################################
function getDocDetails($strDocumentID)
{

	$sql="SELECT tblDocument.*,  tblDocumentType.documentTypeDesc FROM tblDocument 
				LEFT JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId  
				WHERE documentId ='$strDocumentID'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	
	$rs=$sql1->Select($sql);
	return $rs;
}

function getEmployees()
{		
	$sql="SELECT tblEmpPersonal.empNumber, tblEmpPersonal.firstname, tblEmpPersonal.middleInitial, tblEmpPersonal.surname 
		FROM tblEmpPersonal INNER JOIN tblEmpPosition ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber 
		WHERE tblEmpPosition.statusOfAppointment='In-Service' ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getGroup1()
{
	$sql="SELECT * FROM tblGroup1 ORDER BY group1Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	
	//added to enable proper displaying of � 
	$rs=$sql1->Select($sql);
	return $rs;
}

function getAgencies()
{		
	$sql="SELECT * FROM tblOriginOffice WHERE ownerOffice='".$_SESSION['office']."' OR ownerOffice='system' ORDER BY officeName";
	//$sql="SELECT * FROM tblGroup WHERE groupCode='".$_SESSION['office']."' ORDER BY groupCode";	
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getAgenciesRMS()
{		
	$sql="SELECT * FROM tblOriginOffice WHERE ownerOffice='RMS' ORDER BY officeName";
	//$sql="SELECT * FROM tblGroup WHERE groupCode='".$_SESSION['office']."' ORDER BY groupCode";	
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getComplianceDocs($y,$m,$d)
{		
	$sql="SELECT tblHistory.documentId, tblHistory.remarks, tblHistory.actionTakenCodeID, tblDocument.subject, tblActionRequired.actionDesc as 'actionRequired', tblActionTaken.actionDesc as 'actionTaken', tblDocumentType.documentTypeAbbrev
			FROM `tblHistory`
			LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
			LEFT JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId
			LEFT JOIN tblActionRequired on tblActionRequired.actionCodeId = tblHistory.actionCodeId
			LEFT JOIN tblActionTaken on tblActionTaken.actionCodeId = tblHistory.actionTakenCodeID
			WHERE tblHistory.actionTakenCodeID in ('63','64','65','66','67','68') and tblHistory.remarks like '%".$m."%".$d."%".$y."%'";
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getGroup2($txtOfficeCode="")
{
	if($txtOfficeCode != "")
		$t_strWhere = " WHERE group1Code='".$txtOfficeCode."' ";
	$sql="SELECT * FROM tblGroup2 $t_strWhere ORDER BY group2Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getGroup3($txtServiceCode="")
{
	if($txtServiceCode != "")
		$t_strWhere = " WHERE group2Code='".$txtServiceCode."' ";
	$sql="SELECT * FROM tblGroup3 $t_strWhere ORDER BY group3Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getGroup1NoGroup2($txtOfficeCode)
{
	$sql="SELECT * FROM tblGroup3 WHERE group1Code='$txtOfficeCode' AND group2Code='' ORDER BY group3Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}


function getGroup4($txtDivisionCode="")
{
	if($txtDivisionCode != "")
		$t_strWhere = " WHERE group3Code='".$txtDivisionCode."' ";
	$sql="SELECT * FROM tblGroup4 $t_strWhere ORDER BY group4Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getDivisions()
{
	$sql="SELECT * FROM tblDivision ORDER BY divisionName";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionRequired()
{
	$sql="SELECT * FROM tblActionRequired ORDER BY actionDesc";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionRequiredDetails($intActionCodeId)
{
	$sql="SELECT * FROM tblActionRequired WHERE actionCodeId='".$intActionCodeId."'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionTaken()
{
	$sql="SELECT * FROM tblActionTaken ORDER BY actionDesc";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getActionTakenDetails($intActionCodeId)
{
	$sql="SELECT * FROM tblActionTaken WHERE actionCodeId='".$intActionCodeId."'";
	//echo $sql1;
	$sql1= new MySQLHandler();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function getOfficeDivisions($txtOfficeCode)
{
	$sql="SELECT * FROM tblGroup3 WHERE group1Code='$txtOfficeCode' ORDER BY group3Name";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}

function sendEmail($keyID)
{
 $con = new MySQLHandler();
 $objDocs = new MySQLHandler();
 $con->init();
 
 $rs=$con->Select("Select * from tblHistory where historyId LIKE '$keyID'");

 $objDocs->init();
 $rsDocs=$objDocs->Select("SELECT *, (select actionDesc from tblActionRequired ar where ar.actionCodeId = h.actionCodeId) as Action from tblDocument as d inner join tblHistory as h on d.documentId = h.documentId where h.historyId = '$keyID'");
 
 $docdetails="{$rsDocs[0]['documentId']};{$rsDocs[0]['Action']};{$rsDocs[0]['remarks']}";
 /************** Displays Error.. paki check ***************/
 $strSenderEadd = $this->getRecipientInfo('employee','1',$rsDocs[0]['addedById'],'email');
 $strEadd = $this->getRecipientInfo($rs[0]['recipientUnit'],$rs[0]['restricted'],$rs[0]['recipientId'],'email');
 /*********************************************************/

 //print_r($strEadd);	
 //foreach($strEadd as $email)
   //$this->xmail($email,$strSenderEadd[0],$rsDocs[0]['addedByOfficeId'],$rsDocs[0]['subject'],$docdetails);
 
}

function addAction_ORIGINAL($arrFields)
{
	//$intActionUnitSwitch = $arrFields['actionUnitSwitch'];
	$arrActUnit = $arrFields['txtActionUnit'];
	if($arrActUnit=="")
		$arrActUnit = $arrFields['actionUnitSwitch'];
	
	// user class is 
	/*
		record=1
		custodian=2
		employee=3
	*/	
	// addSource = 1 = action reply is added by recipient, 2 = action reply is added by sender also
	//echo count($arrActUnit);exit(1);
	for($i=0;$i<sizeof($arrActUnit);$i++)	
	{
	
	$arrActionUnit = explode(":", $arrActUnit[$i]);
	$intActionUnitSwitch = $arrActionUnit[0];	
	
	if($arrFields['addSource']==1)
	{
	
		if($_SESSION['userType']=='1' || $_SESSION['userType']=='2')
		{
			$strSender = $_SESSION['office'];
			$strSenderUnit =  $_SESSION['userUnit'];
			
		}	
		elseif($_SESSION['userType']=='3')
		{
			$strSender = $_SESSION['userID'];
			$strSenderUnit = "employee";
		}
	
	}
	else
	{
		$strSender = $arrFields['sender'];
		$strSenderUnit = $arrFields['senderUnit'];
	}
	
	if($intActionUnitSwitch=='2') // if office
	{
		if($arrFields['txtActionUnit']=="")
			list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields['cmbOfficeCode']);
		else
			{	
			$strRecipientUnit = $arrActionUnit[1];
			$strRecipientId = $arrActionUnit[2];
			}
		$strMode = 'int';

	}elseif($intActionUnitSwitch=='1') // if employee
	{
		//$strRecipientId = $arrFields['cmbEmpNumber'];
		if($arrFields['txtActionUnit']=="")
			$strRecipientId = $arrFields['cmbEmpNumber'];
		else	
			{
			$strRecipientUnit="employee";
			$strRecipientId = $arrActionUnit[1];
			}
		$strMode = 'int';
	}elseif($intActionUnitSwitch=='3') // if agency
	{
		//$strRecipientId = $arrFields['cmbAgency'];
		if($arrFields['txtActionUnit']=="")
			$strRecipientId = $arrFields['cmbAgency'];
		else
			{
			$strRecipientUnit="agency";
			$strRecipientId = $arrActionUnit[1];
			}
		$strMode = 'ext';
	}
	elseif($intActionUnitSwitch=='0')//all offices/employees
	{
		$strRecipientUnit="All offices";
		$strRecipientId = 0;
	}
	
	if($arrFields['cbRestricted']=="")
		$intRestricted = 0;
	else
		$intRestricted = 1;
/*
	$SQL = "INSERT INTO tblHistory (documentId, fromRecipient, fromDate, toRecipient, toUnit, toDate, remarks, actionCodeId, addedById, dateAdded) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$strFromRecipient."', NOW(), '".$strToRecipient."', '".$strActionUnit."', NOW(), '".$arrFields['txtRemarks']."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW())";
*/		
	$SQL = "INSERT INTO tblHistory (documentId, referenceId, senderId, senderUnit, actionTakenCodeID, dateSent, recipientId, recipientUnit, mode, remarks, actionCodeId, addedById, dateAdded, restricted) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$arrFields['txtReferenceID']."', '".$strSender."', '".$strSenderUnit."', '".$arrFields['cmbActionTaken']."', NOW(), '".$strRecipientId."', '".$strRecipientUnit."', '$strMode', '".addslashes($arrFields['txtRemarks'])."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW(),'".$intRestricted."')";
			//echo $SQL."<br><br>";
			
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs = $sql1->Insert($SQL);
			
			if(count($rs))
			{
				$historyId = mysql_insert_id();
				$rsAction = $this->selectAction($historyId);
				
				$actionRecipient = $this->displayRecipientSenderNames($rsAction[0]['recipientId'], $rsAction[0]['recipientUnit']);
				$sql1->changelog($SQL,"INSERT",strtoupper($_POST['mode']),"sent Document ".$arrFields['txtDocumentID']." for action to ".$actionRecipient);
				$this->msg = MSG_ADD_SUCCESS;
				//if($arrFields['txtActionUnit']=="")
				//return $historyId; //should return mysql_insert_id() on INSERT success(required by kiko for sending of email notification)
				$arrReturn[]=$historyId;
			}
			
	}//end loop
	return $arrReturn;
}


function addAction($arrFields)
{
	//$intActionUnitSwitch = $arrFields['actionUnitSwitch'];
	$arrActUnit = $arrFields['txtActionUnit'];
	if($arrActUnit=="")
		$arrActUnit = $arrFields['actionUnitSwitch'];
	
	// user class is 
	/*
		record=1
		custodian=2
		employee=3
	*/	
	// addSource = 1 = action reply is added by recipient, 2 = action reply is added by sender also
	//echo count($arrActUnit);exit(1);
	for($i=0;$i<sizeof($arrActUnit);$i++)	
	{
	
	$arrActionUnit = explode(":", $arrActUnit[$i]);
	$intActionUnitSwitch = $arrActionUnit[0];	
	
	if($arrFields['addSource']==1)
	{
	
		if($_SESSION['userType']=='1' || $_SESSION['userType']=='2')
		{
//pao031915		$strSender = $_SESSION['office'];
			$strSender = $_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'];
			$strSenderUnit =  $_SESSION['userUnit'];
			
		}	
		elseif($_SESSION['userType']=='3')
		{  
//pao 012815		
			if ($_SESSION['blnAgencyUser']){
			$strSender = $this->getOfficeCode($_SESSION['userID']);
			$strSenderUnit = "agency";
			}else{
			$strSender = $_SESSION['userID'];
			$strSenderUnit = "employee";
			}
//pao
		}
	
	}
	else
	{
		$strSender = $arrFields['sender'];
		$strSenderUnit = $arrFields['senderUnit'];
	}
	
	if($intActionUnitSwitch=='2') // if office
	{
		if($arrFields['txtActionUnit']=="")
			list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields['cmbOfficeCode']);
		else
			{	
			$strRecipientUnit = $arrActionUnit[1];
			$strRecipientId = $arrActionUnit[2];
			}
		$strMode = 'int';

	}elseif($intActionUnitSwitch=='1') // if employee
	{
		//$strRecipientId = $arrFields['cmbEmpNumber'];
		if($arrFields['txtActionUnit']=="")
			$strRecipientId = $arrFields['cmbEmpNumber'];
		else	
			{
			$strRecipientUnit="employee";
			$strRecipientId = $arrActionUnit[1];
			}
		$strMode = 'int';
	}elseif($intActionUnitSwitch=='3') // if agency
	{
		//$strRecipientId = $arrFields['cmbAgency'];
		if($arrFields['txtActionUnit']=="")
			$strRecipientId = $arrFields['cmbAgency'];
		else
			{
			$strRecipientUnit="agency";
			$strRecipientId = $arrActionUnit[1];
			}
		$strMode = 'ext';
	}
	elseif($intActionUnitSwitch=='0')//all offices/employees
	{
		$strRecipientUnit="All offices";
		$strRecipientId = 0;
	}
	
	if($arrFields['cbRestricted']=="")
		$intRestricted = 0;
	else
		$intRestricted = 1;
	
	if($arrFields['cbReply']=="")
		$intReply = 0;
	else
		$intReply = 1;		
		
		
/*
	$SQL = "INSERT INTO tblHistory (documentId, fromRecipient, fromDate, toRecipient, toUnit, toDate, remarks, actionCodeId, addedById, dateAdded) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$strFromRecipient."', NOW(), '".$strToRecipient."', '".$strActionUnit."', NOW(), '".$arrFields['txtRemarks']."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW())";
*/		
//pao 121214
	$SQL = "INSERT INTO tblHistory (documentId, referenceId, senderId, senderUnit, actionTakenCodeID, dateSent, recipientId, recipientUnit, mode, remarks, actionCodeId, addedById, dateAdded, restricted, reply) 
				VALUES ('".$arrFields['txtDocumentID']."', '".$arrFields['txtReferenceID']."', '".$strSender."', '".$strSenderUnit."', '".$arrFields['cmbActionTaken']."', NOW(), '".$strRecipientId."', '".$strRecipientUnit."', '$strMode', '".addslashes($arrFields['txtRemarks'])."', '".$arrFields['cmbActionRequired']."', 
				'".$_SESSION['userID']."', NOW(),'".$intRestricted ."', '" .$intReply."')";
			//echo $SQL."<br><br>";
			
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs = $sql1->Insert($SQL);
			
			if(count($rs))
			{
				$historyId = mysql_insert_id();
				$rsAction = $this->selectAction($historyId);
				
				$actionRecipient = $this->displayRecipientSenderNames($rsAction[0]['recipientId'], $rsAction[0]['recipientUnit']);
				$sql1->changelog($SQL,"INSERT",strtoupper($_POST['mode']),"sent Document ".$arrFields['txtDocumentID']." for action to ".$actionRecipient);
				$this->msg = MSG_ADD_SUCCESS;
				//if($arrFields['txtActionUnit']=="")
				//return $historyId; //should return mysql_insert_id() on INSERT success(required by kiko for sending of email notification)
				$arrReturn[]=$historyId;
			}
			
	}//end loop
	return $arrReturn;
}

	
	
	function showActions_ORIGINAL($strDocumentID)
	{
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['office']."' ORDER BY tblHistory.dateSent";
		elseif($_SESSION['userType']==3) // employee
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['userID']."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
		return $rs;
	}


	function showActions($strDocumentID)
	{

		
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			//$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['office']."' ORDER BY tblHistory.dateSent";
		
		
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."'  ORDER BY tblHistory.dateSent DESC";
			   
			
		elseif($_SESSION['userType']==3) // employee
//pao 012815
			if ($_SESSION['blnAgencyUser'])
				$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' ORDER BY tblHistory.dateSent DESC"; 
			else			
				$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' AND tblHistory.senderId='".$_SESSION['userID']."' ORDER BY tblHistory.dateSent DESC"; 
		
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
		return $rs;
	}


	
	function displayRecipientSenderNames1($strRecipientSenderId, $strRecipientSenderUnit) 
	{
		
		if($strRecipientSenderUnit=="employee") // if recipient unit is employee
		{
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}	
		elseif($strRecipientSenderUnit==Group1) // if recipient unit is group1
		{
			$SQL = "SELECT group1Name as name FROM tblGroup1 WHERE group1Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}		
		elseif($strRecipientSenderUnit==Group2) // if recipient unit is group2
		{
			$SQL = "SELECT group2Name as name FROM tblGroup2 WHERE group2Code='".$strRecipientSenderId."'";	
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}		
		elseif($strRecipientSenderUnit==Group3) // if recipient unit is group3
		{
			$SQL = "SELECT group3Name as name FROM tblGroup3 WHERE group3Code='".$strRecipientSenderId."'";		
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}	
		elseif($strRecipientSenderUnit==Group4) // if recipient unit is group4
		{
			$SQL = "SELECT group4Name as name FROM tblGroup4 WHERE group4Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}
		elseif($strRecipientSenderUnit==Group5) // if recipient unit is group4
		{
			$SQL = "SELECT group5Name as name FROM tblGroup5 WHERE group5Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		}
		elseif($strRecipientSenderUnit=="agency") // if recipient unit is agency
		{	
			$SQL = "SELECT officeName as name FROM tblOriginOffice WHERE originId='".trim($strRecipientSenderId)."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			
		}
		elseif($strRecipientSenderUnit=="Project") // if recipient unit is project
		{	
			$SQL = "SELECT groupName as name FROM tblGroup WHERE groupCode='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			
		}				
			

		if($strRecipientSenderUnit=="employee")
		{
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		}elseif($strRecipientSenderUnit==$strRecipientSenderId) // for sender info, if sender is not employee
		{
			$strName =$strRecipientSenderId;
		}
		else
			$strName = $rs[0]['name'];
		
		return $strName;
	}
	
	
	function displayRecipientSenderNames($strRecipientSenderId, $strRecipientSenderUnit) 
	{
		//if($strRecipientSenderUnit=="employee") // if recipient unit is employee
		//{
		
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		//}	
		if(count($rs)==0) // if recipient unit is group1
		{
			$SQL = "SELECT group1Name as name FROM tblGroup1 WHERE group1Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		
		
		if(count($rs)==0) // if recipient unit is group2
		{
			$SQL = "SELECT group2Name as name FROM tblGroup2 WHERE group2Code='".$strRecipientSenderId."'";	
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
				
		if(count($rs)==0) // if recipient unit is group3
		{
			$SQL = "SELECT group3Name as name FROM tblGroup3 WHERE group3Code='".$strRecipientSenderId."'";		
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			
		if(count($rs)==0) // if recipient unit is group4
		{
			$SQL = "SELECT group4Name as name FROM tblGroup4 WHERE group4Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		
		if(count($rs)==0) // if recipient unit is group4
		{
			$SQL = "SELECT group5Name as name FROM tblGroup5 WHERE group5Code='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
		
		if(count($rs)==0) // if recipient unit is agency
		{	
			$SQL = "SELECT officeName as name FROM tblOriginOffice WHERE originId='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			
		
		if(count($rs)==0) // if recipient unit is project
		{	
			$SQL = "SELECT groupName as name FROM tblGroup WHERE groupCode='".$strRecipientSenderId."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rs=$sql1->Select($SQL);
			if(count($rs)!=0) // if recipient unit is project
				{$units="groups";}
		}
		 if(count($rs)==0) // if recipient unit is "all employees" 
		 	{
//pao
				if($_SESSION['userType']==3){				
					$officeCode = explode('~',$this->getOfficeCode2($this->get('userID')));
					return "".$officeCode[0];
				}else {
					return $strRecipientSenderUnit;
				}
//pao				return $strRecipientSenderUnit;
			}		
		}
		}
		}
		}
		}
		}else{$units="employee";}

		if($units=="employee")
		{
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		}elseif($strRecipientSenderUnit==$strRecipientSenderId) // for sender info, if sender is not employee
		{
			$strName =$strRecipientSenderId;
		}
		else
			$strName = $rs[0]['name'];
		
		return $strName;
	}
	
	
	function getEmployeeName_ORIGINAL($strEmpNumber) 
	{
		$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strEmpNumber."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
		$rs=$sql1->Select($SQL);

		$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
		$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		
		return $strName;		
	}

	function getEmployeeName($strEmpNumber) 
	{
		$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strEmpNumber."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
		$rs=$sql1->Select($SQL);

		$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
		$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
	
//pao012815		
		if(count($rs)) 	return $strName;	
		else
		{
		$sql = "SELECT firstname,surname FROM tblUserAccount WHERE empNumber='".$strEmpNumber."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs)) 	return $rs[0]['surname'].", ".$rs[0]['firstname'];
		}
		
	}





	function getCurrentReference($strDocumentID)
	{
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' ORDER BY tblHistory.dateSent DESC";

		//echo $SQL;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getDocumentReceiver($t_intHistoryID,$t_strDocID,$t_strRecipient){
		$query = "SELECT tblDocumentReceived.* FROM tblDocumentReceived inner join tblHistory on tblDocumentReceived.historyId = tblHistory.historyId 
				  WHERE tblDocumentReceived.documentId LIKE '$t_strDocID' AND tblDocumentReceived.historyId = $t_intHistoryID AND recipientId LIKE '$t_strRecipient'";
		$sql= new MySQLHandler();
		$sql->init();	
		$rs=$sql->Select($query);
		return $rs;
	}
	function getInitialReference($strDocumentID)
	{
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.documentId='".$strDocumentID."' ORDER BY tblHistory.dateSent ASC";

		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkSenderUnit($strSenderUnit)
	{
		
		if($rsSenderUnit = $this->checkIfExeOffice($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfService($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfDivision($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfSection($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];
		elseif($rsSenderUnit = $this->checkIfGroup5($strSenderUnit))
			$senderUnit = $rsSenderUnit[0]['name'];

		return $senderUnit;
	}
	
	
	function checkIfExeOffice($strSenderUnit)
	{
		$SQL = "SELECT group1Name as name FROM tblGroup1 WHERE group1Code='".$strSenderUnit."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfService($strSenderUnit)
	{
		$SQL = "SELECT group2Name as name FROM tblGroup2 WHERE group2Code='".$strSenderUnit."'";	
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfDivision($strSenderUnit)
	{
		$SQL = "SELECT group3Name as name FROM tblGroup3 WHERE group3Code='".$strSenderUnit."'";		
		//echo $SQL;
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkIfSection($strSenderUnit)
	{
		$SQL = "SELECT group4Name as name FROM tblGroup4 WHERE group4Code='".$strSenderUnit."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	function checkIfGroup5($strSenderUnit)
	{
		$SQL = "SELECT group5Name as name FROM tblGroup5 WHERE group5Code='".$strSenderUnit."'";
		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	function displaySenderName($strSenderId, $strSenderUnit)
	{
			
		if($strSenderUnit=="employee")
		{
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strSenderId."'";
	
			$sql1= new MySQLHandler2();
			$sql1->init();	
	
			$rs=$sql1->Select($SQL);
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
		}else
			$strName = $this->checkSenderUnit($strSenderUnit);;
		
		return $strName;
	}
	
	// SELECT HISTORY RECORD
	function selectAction($intHistoryID)
	{
		$SQL = "SELECT * FROM tblHistory WHERE historyID = '".$intHistoryID."'";
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
	
		return $rs;
	
	}
	// DELETE RECORD
	function deleteAction($intID, $docID)
	{
		$SQL = "SELECT * FROM tblHistory WHERE referenceId = '".$intID."'";
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
		
		if(count($rs)==0)
		{
		
			$rsAction = $this->selectAction($intID);
			
			$SQL = "DELETE FROM tblHistory WHERE historyID = '".$intID."'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			if($rs=$sql1->Delete($SQL))
			{
				$this->msg = MSG_DEL_SUCCESS;
				
				$actionRecipient = $this->displayRecipientSenderNames($rsAction[0]['recipientId'], $rsAction[0]['recipientUnit']);
				$sql1->init();
				$sql1->changelog($SQL,"DELETE",strtoupper($_GET['mode']),"undo action sent to ".$actionRecipient." for Document ".$docID);
//pao 				$this->msg = MSG_ADD_SUCCESS;

				return 1;
			}
			else
			{
				$this->msg = '<font color="red">'.MSG_DEL_SYS_MALF.'</font>';
				return 0;
			}
		}
		else
		{
			$this->msg = '<font color="red">'.MSG_ERR_ACTION_DEL.'</font>';
			return 0;
		}
			
		return $rs;
	
	}
	


	function getReceivedActions_ORIGINAL($strDocumentID)
	{
		$empHead=$this->getOfficeHead($this->get('office'));
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(sizeof($rsGroup)>1)
			{
			for($i=0;$i<sizeof($rsGroup);$i++)
				$t_strWhereGroup = $t_strWhereGroup . " OR tblHistory.recipientId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		else
			$t_strWhereGroup = " OR tblHistory.recipientId = '".$rsGroup[0]['groupCode']."' ";
					
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			{
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			(tblHistory.recipientId='".$_SESSION['office']."' OR tblHistory.recipientId='$empHead' $t_strWhereGroup) 
			AND tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";
			}
		elseif($_SESSION['userType']==3) // employee
			{
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			tblHistory.recipientId='".$_SESSION['userID']."' AND 
			tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";
			}

		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		
	
		return $rs;
	}
	

//PAO 010915	
	function getReceivedActions($strDocumentID)
	{
		$empHead=$this->getOfficeHead($this->get('office'));
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(sizeof($rsGroup)>1)
			{
			for($i=0;$i<sizeof($rsGroup);$i++)
				$t_strWhereGroup = $t_strWhereGroup . " OR tblHistory.recipientId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		else
			$t_strWhereGroup = " OR tblHistory.recipientId = '".$rsGroup[0]['groupCode']."' ";
					
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			{
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			(tblHistory.recipientId='".$_SESSION['office']."' OR tblHistory.recipientId='$empHead' $t_strWhereGroup) 
			AND tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";



			}
		elseif($_SESSION['userType']==3) // employee
			{
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			tblHistory.recipientId='".$_SESSION['userID']."' AND 
			tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";

			}

		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		
	
		return $rs;
	}




	function checkHistoryActions_ORIGINAL($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}


	function checkHistoryActions($intHistoryID)
	{
		$empHead=$this->getOfficeHead($this->get('office'));
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(sizeof($rsGroup)>1)
			{
			for($i=0;$i<sizeof($rsGroup);$i++)
				$t_strWhereGroup = $t_strWhereGroup . " OR tblHistory.senderId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		else
			$t_strWhereGroup = " OR tblHistory.senderId = '".$rsGroup[0]['groupCode']."' ";
			 
			
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
		WHERE tblHistory.referenceId='".$intHistoryID."' 
		AND	(tblHistory.senderId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."' OR tblHistory.senderId='$empHead' $t_strWhereGroup) 
 		AND tblHistory.referenceId!='0' ORDER BY tblHistory.dateSent"; 
		 
		elseif($_SESSION['userType']==3) // employee
		{	if ($_SESSION['blnAgencyUser'])		
				$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
				WHERE tblHistory.referenceId='".$intHistoryID."' 		
				AND tblHistory.referenceId!='0'	AND tblHistory.senderId='".$this->getOfficeCode($this->get('userID'))."' ORDER BY tblHistory.dateSent";
		 	else
			 	$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
				WHERE tblHistory.referenceId='".$intHistoryID."' 		
				AND tblHistory.referenceId!='0'	AND tblHistory.senderId='".$_SESSION['userID']."' ORDER BY tblHistory.dateSent";
		}
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	


	function getActionsRecorded($strEmpNuber, $intMonth, $intYear)
	{
	
		
		$SQL = "SELECT tblHistory.*,  tblDocument.documentId, tblDocumentType.documentTypeDesc, tblDocument.subject, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
		LEFT JOIN tblDocument ON tblHistory.documentId = tblDocument.documentId 
		LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId 
		LEFT JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
		LEFT JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
		WHERE tblHistory.addedById='".$strEmpNuber."' AND tblHistory.dateAdded LIKE '".$intYear.'-'.$intMonth."%' ORDER BY tblHistory.dateAdded";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	
	function getReceivedDocs($strEmpNuber, $intMonth, $intYear, $intReportType)
	{
		if($intReportType == 1)
			$ext_sql = "AND tblDocumentReceived.dateReceived LIKE '".$intYear.'-'.$intMonth."%'";
		else
			$ext_sql = "AND tblDocumentReceived.dateReceived BETWEEN '".$intMonth." 00:00:00' and '".$intYear." 23:59:59'";

		$SQL = "SELECT tblDocumentReceived.dateReceived, tblDocumentReceived.receivedBy, tblDocument.documentId, tblDocumentType.documentTypeDesc, tblDocument.subject, tblDocument.officeSig, tblDocumentReceived.historyId
		 FROM tblDocumentReceived 
			LEFT JOIN tblDocument ON tblDocumentReceived.documentId = tblDocument.documentId 
			LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId 
		WHERE tblDocumentReceived.empNumber='".$strEmpNuber."' ".$ext_sql;
		
		// echo $SQL;
		// exit;
		// return $SQL;
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	function getDocumentHistoryByEmpByDateAdded($addedby, $dateaddedFrom, $dateaddedTo, $arrdocumentId)
	{
		// YY-MM-DD
		if($_GET['reportType']=='DMPR'){
			$sql = "SELECT * FROM `tblHistory` WHERE addedById = '".$addedby."' and documentId not in (".$arrdocumentId.") and dateAdded between '".$dateaddedFrom." 00:00:00' and '".$dateaddedTo." 11:59:59'";
		}else{
			$sql = "SELECT * FROM `tblHistory` WHERE addedById = '".$addedby."' and documentId not in (".$arrdocumentId.") and dateAdded LIKE '%".$dateaddedTo."-".$dateaddedFrom."%'";
		}
		
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}

	function getDocumentReceivedByEmpNumber($addedby, $dateaddedFrom, $dateaddedTo)
	{
		// YY-MM-DD
		$sql = "SELECT *  FROM `tblDocumentReceived` WHERE empNumber = '".$addedby."' and dateReceived between '".$dateaddedFrom." 00:00:00' and '".$dateaddedTo." 23:59:59'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}

	function getDocumentDetails($docid)
	{
		$sql = "SELECT * FROM `tblDocument`
				LEFT JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId
				WHERE documentId = '".$docid."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0];
	}

	// function get_docs_notinReceived($addedby, $dateaddedFrom, $dateaddedTo)
	// {
	// 	$sql = "SELECT documentId, documentTypeAbbrev, dateAdded, subject FROM `tblDocument`
	// 			LEFT JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId
	// 			WHERE `addedById` LIKE '".$addedby."' and dateAdded between '".$dateaddedFrom." 00:00:00' and '".$dateaddedTo." 23:59:59'";
	// 	$sql1= new MySQLHandler();
	// 	$sql1->init();
	// 	$rs=$sql1->Select($sql);
	// 	return $rs;
	// }

	function getDocumentHistory($docid, $office)
	{
		$sql = "SELECT * FROM `tblHistory`
				WHERE documentId = '".$docid."' AND senderId = '".$office."' ORDER BY `tblHistory`.`dateSent` DESC";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0];
	}

	function getReceivedDocByrefIdByDocId($historyid, $docid)
	{
		$sql = "SELECT *  FROM `tblDocumentReceived` WHERE `historyId` = '".$historyid."' AND `documentId` LIKE '".$docid."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0];
	}

	function isHoliday($t_strDate)
	{
		$isHoliday = false;
		$date = explode("-", $t_strDate);
		$month = $date[1];
		$day = $date[2];
		
		$sql = "SELECT holidayCode FROM tblHolidayYear WHERE holidayDate='".$t_strDate."' && (holidayCode<>'HDAM' || holidayCode<>'HDPM' || holidayCode!='WS')";
		$sql1= new MySQLHandler2();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs) > 0)
			return true;
		else
			return false;
	}

	function getReferenceFromHistory($referenceId)
	{
		$SQL = "SELECT * FROM tblHistory where referenceId = '".$referenceId."' ORDER BY  `tblHistory`.`historyId` DESC ";
		
		// echo $SQL;
		// exit;
		// return $SQL;
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs[0];
	}

	function getReceivedDocsFromHistory($strEmpNumber, $sdate, $edate, $intReportType)
	{
		if($intReportType == 1)
			$ext_sql = "AND tblDocumentReceived.dateReceived LIKE '".$sdate.'-'.$edate."%'";
		else
			$ext_sql = "AND tblHistory.dateAdded BETWEEN '".$sdate." 00:00:00' and '".$edate." 23:59:59'";

		$SQL =  "SELECT COUNT( * ) AS 'ctrext', tblHistory.documentId, tblHistory.addedById, tblHistory.actionTakenCodeID, tblHistory.dateAdded, tblDocumentType.documentTypeDesc, tblDocument.subject
					FROM `tblHistory`
					LEFT JOIN tblDocument ON tblHistory.documentId = tblDocument.documentId
					LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId
					WHERE tblHistory.addedById='".$strEmpNumber."' ".$ext_sql." GROUP BY tblHistory.documentId";
		
		// echo $SQL;
		// exit;
		// return $SQL;
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	function getNewCreatedDocument($strEmpNumber, $sdate, $edate, $intReportType)
	{
		if($intReportType == 1)
			$ext_sql = "AND tblDocument.dateAdded LIKE '".$sdate.'-'.$edate."%'";
		else
			$ext_sql = "AND tblDocument.dateAdded BETWEEN '".$sdate." 00:00:00' and '".$edate." 23:59:59'";

		$SQL =  "SELECT tblDocument.documentId, tblDocument.dateAdded, tblDocumentType.documentTypeDesc, tblDocument.addedById, tblDocument.subject
					FROM `tblDocument`
					LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId
					WHERE tblDocument.addedById='".$strEmpNumber."' ".$ext_sql;
		
		// echo $SQL;
		// exit;
		// return $SQL;
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	function checkifhasReceived($documentId){
		$sql = "SELECT count(*) as cnt FROM `tblDocumentReceived` WHERE `documentId` LIKE '".$documentId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs[0]['cnt'];
	}


	function getDocListEncoded2($txtUserID,$intStatus,$intMonth, $intYear)
	{
		
		$sql = "SELECT COUNT(documentId) AS _total FROM tblDocument 
			 WHERE addedById = '".$txtUserID."' AND status ='".$intStatus."' AND dateAdded LIKE '".$intYear.'-'.$intMonth."%'";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	
	

	function getHistoryActionSource($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.historyId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.historyId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function checkRecipientHistoryActions($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	

		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function checkReplyActionSent($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['office']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	
	function checkSenderHistoryActions($intHistoryID)
	{
	
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['office']."' AND tblHistory.historyId ='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.recipientId='".$_SESSION['userID']."' AND tblHistory.historyId ='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function getSenderDocument($strDocumentID, $strStatus)
	{
		$SQL="SELECT * FROM tblDocument WHERE documentId='$strDocumentID' AND status='$strStatus'";
		$sql1= new MySQLHandler();
		$sql1->init();	
		$rsDoc=$sql1->Select($SQL);
		
		
		if($strStatus==0) //incoming
		{
			$sender = $rsDoc[0]["sender"];
			$senderOffice = $rsDoc[0]["originId"];
			
			$SQL2="SELECT officeName FROM tblOriginOffice WHERE originId='$senderOffice'";
			$sql1= new MySQLHandler();
			$sql1->init();	
			$rsDocSender=$sql1->Select($SQL2);
			if(strlen($rsDocSender[0]["officeName"])!=0)
				return $rsDocSender[0]["officeName"]."\n".$sender;
			else
				return $sender;
			
		}
		elseif($strStatus==1 || $strStatus==2) //outgoing or intraoffice
		{
			return AGENCY_CODE.'-'.$rsDoc[0]["officeSig"]."\n".$sender;

		}		

		
	}
	
	function displayDocDetail($arrFields2)
	{
		echo '<br>';
		
		// Display submitted data
		$rsActionTakenSubmitted = $objDocDetail->getActionTakenDetails($arrFields2['cmbActionTaken']);
		$rsActionRequiredSubmitted = $objDocDetail->getActionRequiredDetails($arrFields2['cmbActionRequired']);
			
		if($arrFields2['actionUnitSwitch']==2) // if office
		{
			list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields2['cmbOfficeCode']);
			$recipientUnitLabel = 'Office';
	
		}elseif($arrFields2['actionUnitSwitch']==1) // if employee
		{
			$strRecipientUnit="employee";
			$strRecipientId = $arrFields2['cmbEmpNumber'];
			$recipientUnitLabel = 'Employee';
		}
		
		$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
			
	
		echo '<table width="100%"  border="0">
		  <tr>
			<td width="30%">Action Taken </td>
			<td width="70%">'.$rsActionTakenSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Required </td>
			<td>'.$rsActionRequiredSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Unit </td>
			<td></td>
		  </tr>
		  <tr>
			<td>'.$recipientUnitLabel.' </td>
			<td>'.$recipientName.'</td>
		  </tr>
		  <tr>
			<td>Remarks</td>
			<td>'.$arrFields2['txtRemarks'].'</td>
		  </tr>
		  <tr>
			<td>Restricted</td>
			<td>'.$restricted.'</td>
		  </tr>
		</table>';

		echo "<script language=\"JavaScript\">
		getData('showAction.php?mode=$mode&docID=$docID&div=$divId&historydiv=$historydiv','$divId');
		if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
		getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
		</script>";
	}
	
		function displaySourceRecipient()
	{
	
		$objDocDetail = new DocDetail;
		
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" height="23" align="left"><input name="actionUnitSwitch" type="radio" value="2" onClick="enableOfficeCombo(this.form);">
				  Office: </td>
				<td width="79%">';
											$objDocDetail->displayOffice();
											
		echo '</td>
			  </tr>
			</table>';
			
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="1" onClick="enableNameCombo(this.form);">
				  Employee Name: </td>
				<td width="79%">';
					$rsEmployees = $objDocDetail->getEmployees();
		echo  '<select name="cmbEmpNumber" id="cmbEmpNumber" onFocus="enableNameCombo(this.form);">
				  <option value="-1">&nbsp;</option>';

							for($i=0;$i<sizeof($rsEmployees);$i++) 
							{ 		
								$extension = (trim($rsEmployees[$i]['nameExtension'])=="") ? "" : " ".$rsEmployees[$i]['nameExtension']; 
								$strName = $rsEmployees[$i]['surname'].", ".$rsEmployees[$i]['firstname'].$extension.' '.$rsEmployees[$i]['middleInitial'];
			
								echo "<OPTION value='".$rsEmployees[$i]['empNumber']."'>".$strName."</OPTION>\n"; 
							}

		echo '</select></td>
			  </tr>
			</table>';
			
		echo '<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="3"  onClick="enableAgencyCombo(this.form);">
				  Agency Name: </td>
				<td width="79%">';
					$rsAgencies = $objDocDetail->getAgencies();
				
		echo '	 <select name="cmbAgency" id="cmbAgency" onFocus="enableAgencyCombo(this.form);">
				  <option value="-1">&nbsp;</option>';

							for($i=0;$i<sizeof($rsAgencies);$i++) 
							{ 		
								
								echo "<OPTION value='".$rsAgencies[$i]['originId']."'>".$rsAgencies[$i]['officeName']."</OPTION>\n"; 
							}
					
		echo '	</select></td>
			  </tr>
			</table>';
	
	
	}
	
	function addActionDeadline($t_historyID,$t_actionDeadline)
	{
		$sql = "UPDATE tblHistory SET recipientDeadline = '".$t_actionDeadline."' WHERE historyId = '".$t_historyID."'";	
		//return $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);	
		
		if(count($rs))
		{
			$this->msg = MSG_UPD_SUCCESS;
			return $rs;
		}
		else
		{
			$this->msg = MSG_UPD_UNSUCCESS;
			return 0;
		}
			
		
	} 
	
	function selectActionDeadline($intHistoryID)
	{
	
		$SQL = "SELECT * FROM tblHistory WHERE historyId ='".$intHistoryID."' AND (recipientDeadline!='000-00-00' AND recipientDeadline!='')";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function getProcessedbyName($strEmpNumber)
	{
		
			$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblEmpPersonal WHERE empNumber='".$strEmpNumber."'";
			$sql1= new MySQLHandler2();
			$sql1->init();	
			$rs=$sql1->Select($SQL);	
	
			if(!count($rs)>0)
			{
				
				$SQL = "SELECT surname, firstname, middleInitial, nameExtension FROM tblUserAcount WHERE empNumber='".$strEmpNumber."'";
				
				$sql1= new MySQLHandler();
				$sql1->init();	
			
				$rs=$sql1->Select($SQL);
			}
			
					
			$extension = (trim($rs[0]['nameExtension'])=="") ? "" : " ".$rs[0]['nameExtension']; 
			$strName = $rs[0]['surname'].", ".$rs[0]['firstname'].$extension.' '.$rs[0]['middleInitial'];
			
			return $rs!=null ? $strName : '';
	}


///// PAO ADDED CLASS /////////////////


	function isCurrUserDocOwner($strDocID) //PAO
	{
		$SQL = "SELECT addedById, addedByOfficeId FROM tblDocument WHERE documentId ='".$strDocID."'";
		
		$sql1= new MySQLHandler();
		$sql1->init(); 
	
		$rs=$sql1->Select($SQL);

		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		{	if ($rs[0]['addedById'] == $_SESSION['userID'] || $rs[0]['addedByOfficeId'] == ($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']))
				return 1;
			else return 0;
		}elseif($_SESSION['userType']==3) // if employee
		{	
			if ($_SESSION['blnAgencyUser']) // if agency (bcoz userType for Agency is 3 ALSO)
			{	if ($rs[0]['addedById'] == $_SESSION['userID'] || $rs[0]['addedByOfficeId'] == $_SESSION['office'] )
					return 1;
				else return 0;
			}else {
				if ($rs[0]['addedById'] == $_SESSION['userID'])
					return 1;
				else return 0;
			} 
		}
	}	


	function getForActions($strDocumentID)
	{
		$empHead=$this->getOfficeHead($this->get('office'));
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(sizeof($rsGroup)>1)
			{
			for($i=0;$i<sizeof($rsGroup);$i++)
				$t_strWhereGroup = $t_strWhereGroup . " OR tblHistory.recipientId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		else
			$t_strWhereGroup = " OR tblHistory.recipientId = '".$rsGroup[0]['groupCode']."' ";
					
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			{
					
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			(tblHistory.recipientId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."' OR tblHistory.recipientId='$empHead' $t_strWhereGroup) 
			ORDER BY tblHistory.dateSent DESC LIMIT 1";
			// GROUP BY tblHistory.documentId
			// ORDER BY tblHistory.dateSent DESC";
//			AND tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";			
			}
		elseif($_SESSION['userType']==3) // employee
			{

//pao 012815				
			$message = $_SESSION['blnAgencyUser'];
		echo "<script type='text/javascript'>alert('$message);</script>"; 
  
			if ($_SESSION['blnAgencyUser'])
			{
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND 
			tblHistory.recipientId='".$this->getOfficeCode($this->get('userID'))."' 
			ORDER BY tblHistory.dateSent DESC LIMIT 1";
			// GROUP BY tblHistory.documentId
			// ORDER BY tblHistory.dateSent DESC";
			
			}else { 
			$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
			INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
			INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE documentId='".$strDocumentID."' AND 
			recipientId='".$_SESSION['userID']."' 
			ORDER BY tblHistory.dateSent DESC LIMIT 1";
			// GROUP BY tblHistory.documentId
			// ORDER BY tblHistory.dateSent DESC";
			
			}
			  
//			tblHistory.receivedBy!='' ORDER BY tblHistory.dateSent";			
			}

		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		
	
		return $rs;
	}	


	function checkRecipientHistoryReceived($t_intHistoryID, $t_strDocID )
	{
	 
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT * FROM tblDocumentReceived
				  WHERE historyId = '".$t_intHistoryID."'";

				   
		elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT * FROM tblDocumentReceived
				  WHERE historyId = '".$t_intHistoryID."'";

		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}


	function isCurrUserActionSender($strHistoryID) //PAO
	{
		
		$SQL = "SELECT senderId, addedById FROM tblHistory WHERE historyId ='".$strHistoryID."'";
		
		$sql1= new MySQLHandler();
		$sql1->init(); 
	
		$rs=$sql1->Select($SQL);
		  
		// if CurrUser is the Sender and of the same Office
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
			if ( $rs[0]['senderId'] == ($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])|| $rs[0]['addedById'] == $_SESSION['userID'] )		
				return 1;
			else return 0;
		elseif($_SESSION['userType']==3) // if employee 
			if ( $rs[0]['senderId'] == $_SESSION['userID'] || $rs[0]['addedById'] == $_SESSION['userID'] )		
				return 1;
			else return 0;
		
		
	}

//pao for Report
	function getReceivedDocs3($strHistoryId, $intDateFrom, $intDateTo)
	{
	  

		$SQL = "SELECT tblDocumentReceived.dateReceived, tblDocument.documentId, tblDocumentType.documentTypeDesc, tblDocument.subject, tblDocument.officeSig
		 FROM tblDocumentReceived 
			LEFT JOIN tblDocument ON tblDocumentReceived.documentId = tblDocument.documentId 
			LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId 
		WHERE tblDocumentReceived.historyId='".$strHistoryId."'";
		
		//return $SQL;
		
		$sql1= new MySQLHandler(); 
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	function getIncomingListX($strEmpOffice, $strEmpNumber, $intDateFrom, $intDateTo ) //pao
	{
		 
		
		if  ($intDateFrom == $intDateTo )
		$SQL = "SELECT tblHistory.historyId, tblHistory.reply, tblDocument.documentId, tblDocument.isDelete
			FROM tblHistory
			LEFT JOIN tblDocument ON tblHistory.documentId = tblDocument.documentId 
			WHERE tblDocument.isDelete=0
			AND tblHistory.recipientId = '" .$strEmpOffice ."'
			AND tblHistory.dateSent LIKE '".$intDateFrom."%'
			AND  tblHistory.historyId NOT IN (SELECT historyId FROM tblTrash)";
		else
		$SQL = "SELECT tblHistory.historyId, tblHistory.reply, tblDocument.documentId, tblDocument.isDelete
			FROM tblHistory
			LEFT JOIN tblDocument ON tblHistory.documentId = tblDocument.documentId 
			WHERE tblDocument.isDelete=0
			AND tblHistory.recipientId = '" .$strEmpOffice ."'
			AND (tblHistory.dateSent >= '".$intDateFrom." 00:00:00' AND tblHistory.dateSent <='" .$intDateTo
			." 24:00:00') AND  tblHistory.historyId NOT IN (SELECT historyId FROM tblTrash)";
		

 
		//echo "DATE FROM " .$intDateFrom;
		
		$sql1= new MySQLHandler(); 
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}



	 function checkRecipientHistoryActions2($intHistoryID, $strOffCode)
	{
	 
		if($_SESSION['userType']==1 || $_SESSION['userType']==2) // if custodian or records officer
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.senderId='". $strOffCode ."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
			elseif($_SESSION['userType']==3) // employee
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID WHERE tblHistory.senderId='". $strOffCode."' AND tblHistory.referenceId='".$intHistoryID."' ORDER BY tblHistory.dateSent";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		 	 
		return $rs;
	}



///////// END PAO ADDED CLASS /////////////

/*


function getServices()
{
	$sql="SELECT * FROM tblService";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}







function getOfficeEmployees($txtOfficeCode)
{
	if($txtOfficeCode!="")
		$queryOffice = " AND tblEmpPosition.officeCode='$txtOfficeCode' ";
		
	$sql="SELECT tblEmpPersonal.empNumber, tblEmpPersonal.firstname, tblEmpPersonal.middleInitial, tblEmpPersonal.surname 
		FROM tblEmpPersonal INNER JOIN tblEmpPosition ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber 
		WHERE tblEmpPosition.statusOfAppointment='In-Service' $queryOffice ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	//echo $sql1;
	$sql1= new MySQLHandler2();
	$sql1->init();	

	$rs=$sql1->Select($sql);
	return $rs;
}
*/

//------------------function added by emma for unacted documents forwarded to department------

function getOfficeHistory($office,$dtfrom,$dtTo)
	{
		$SQL = "SELECT * from tblHistory
				INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId
				WHERE tblHistory.recipientId = '".$office."'
				AND tblHistory.dateAdded BETWEEN '".$dtfrom."' AND '".$dtTo."'";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		//echo $SQL;
		return $rs;
	}
	
	
function getDocumentUnactedReply($historyId)
	{
		$SQL = "SELECT * from tblHistory where referenceId = '".$historyId."' AND senderID = '".$_SESSION['office']."'";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		 	 
		return $rs;
	}

function getDocumentReceived($historyId)
	{
		$SQL = "SELECT * from tblDocumentReceived where historyId = '".$historyId."'";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		 	 
		return $rs;
	}
	
function getDocumentUnactedHistory($strDocumentId)	
	{
		$SQL = "SELECT tblDocument.*, tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
					INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId 
					INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
					INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
					WHERE tblHistory.documentId='".$strDocumentId."' 
					AND (tblHistory.actionCodeId!=10 AND tblHistory.actionCodeId!=1) ORDER BY tblHistory.dateSent DESC";
			// exclude histories in which action required are "FYI", "For 201 Update", etc.
		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
function getOriginUnactedDoc($strDocId)
	{
		$sql = "SELECT tblDocument.addedByOfficeId, tblUserAccount.agencyCode FROM tblDocument 
					INNER JOIN tblUserAccount ON tblDocument.addedById=tblUserAccount.empID
					where documentId = '".$strDocId."'";
					
		$sql1= new MySQLHandler();
		$sql1->init();	
		$rs=$sql1->Select($sql);
		
		
		if($rs[0]["agencyCode"]!=0) //from agency
			{
				$sender = $rs[0]["agencyCode"];
				$SQL2="SELECT officeName FROM tblOriginOffice WHERE originId='$sender'";
				$sql3= new MySQLHandler();
				$sql3->init();	
				$rsDocSender=$sql3->Select($SQL2);
				if(strlen($rsDocSender[0]["officeName"])!=0)
					return $rsDocSender[0]["officeName"]."\n";
				else
					return $sender;
				
			}
		else // Central Office
			{
				$sql4 = "SELECT addedByOfficeId FROM tblDocument 
							where documentId = '".$strDocId."'";
					
				$sql5= new MySQLHandler();
				$sql5->init();	
				$rs2=$sql5->Select($sql4);
				
				$sender = $rs2[0]["addedByOfficeId"];
				return 'CO - ' .$sender;
	
			}	
	}	
//// end of classes added by emma for unacted documents 

	// begin usql statement for nacted document
	function getAllUnactedDocs($office, $empNumber){
		$sql = "SELECT tblDocument.*, tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc
					FROM tblHistory
						INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId
						INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID
						INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID
							AND (tblHistory.actionCodeId!=10 AND tblHistory.actionCodeId!=1)
						WHERE (recipientId = '".$office."' OR recipientId = '".$empNumber."') AND tblDocument.documentId not in (select documentId from tblunacted where isunacted=1)
						AND dateSent >= DATE_SUB(NOW() , INTERVAL 1 YEAR)
						GROUP BY tblHistory.documentId
						ORDER BY tblDocument.documentId,dateSent desc";
		$stmt= new MySQLHandler();
		$stmt->init();
		$res=$stmt->Select($sql);
		return $res;	
	}
	// end sql statement for unacted document


	//begin getDocumentBy Reply
	function getPendingDocs($dateSent, $office){
		$sql="SELECT * FROM ( SELECT MAX( e.historyId ) AS hid FROM tblHistory e GROUP BY e.documentId ) b
				INNER JOIN tblHistory m ON m.historyId = b.hid
					WHERE 1 
						AND dateSent >= '2015-05-01'
						AND dateSent <= '2015-05-15'
						AND recipientId = '".$office."'
						AND reply =1";
		$stmt= new MySQLHandler();
		$stmt->init();
		$res=$stmt->Select($sql);
		return $res;
	}
	//end getDocumentBy Reply

	function completeDoc($t_documentId, $empNumber, $officeid)
	{
		$sql = "UPDATE  `tblDocument` SET  `iscomplete` =  1, `datecomplete` = NOW( ) , `completed_by` = '".$empNumber."' , `completed_byOffice` = '".$officeid."' WHERE  `tblDocument`.`documentId` =  '".$t_documentId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);	
		
		if(count($rs))
			return 'Transaction for this document is now complete.';
		else
			return 'Error Occur. Please try again or contact administrator.';
		
	}

	function uncompleteDoc($t_documentId)
	{
		$sql = "UPDATE  `tblDocument` SET  `iscomplete` =  0, `datecomplete` = NULL , `completed_by` = NULL , `completed_byOffice` = NULL WHERE  `tblDocument`.`documentId` =  '".$t_documentId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);	
		
		if(count($rs))
			return 'Undo Action Completed!. Please don\'t forget to click the Action Completed button when the process is complete. Thank you';
		else
			return 'Error Occur. Please try again or contact administrator.';
		
	}

	function getAllIncomingByDate($t_stroffice, $datefrom, $dateto)
	{
		$sql="SELECT tblDocument.*,  tblDocumentType.documentTypeDesc FROM tblDocument 
					LEFT JOIN tblDocumentType on tblDocument.documentTypeId = tblDocumentType.documentTypeId  
					WHERE `status` = 1 AND `addedByOfficeId` = '".$t_stroffice."' AND `dateAdded` >= '".$datefrom."' AND `dateAdded` < '".date('Y-m-d',strtotime($dateto . "+1 days"))."'";
		$sql1= new MySQLHandler();
		$sql1->init();	
		$rs=$sql1->Select($sql);
		return $rs;
	}

	function getProcessedTime($historyId, $senderId, $documentId)
	{
		$SQL = "SELECT *  FROM `tblHistory` where historyId > ".$historyId." and documentId = '".$documentId."' and senderId = '".$senderId."' order by historyId asc limit 1";
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs[0];
	}

	function getReceivedDocByUserID($userid, $datefrom, $dateto)
	{
		$SQL = "SELECT tblDocument.documentDate, tblDocumentReceived.documentId, subject, tblDocumentReceived.dateReceived, receivedBy FROM `tblDocumentReceived`
				left join tblDocument on tblDocument.documentId = tblDocumentReceived.documentId
				WHERE tblDocumentReceived.dateReceived between '".$datefrom." 00:00:00' and '".$dateto." 23:59:59' and empNumber = '".$userid."'";
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}

	function getAllRemarks($documentId)
	{
		$SQL = "SELECT remarks, dateSent, senderUnit FROM `tblHistory` WHERE `documentId`
					LIKE '".$documentId."' ORDER BY `tblHistory`.`dateSent` ASC";
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		$history = "";
		foreach($rs as $rec):
			if($rec['remarks']!=''){
				$history.= 'Sender: '.$rec['senderUnit']."\n".'Remarks: '.$rec['remarks']."\n";
			}
		endforeach;
		return $history;
	}

}
?>