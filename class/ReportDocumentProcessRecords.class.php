<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentProcessRecords extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		$arrDocIds = array();

		$this->SetWidths(array(10,28,45,25,25,25,25,25,27,35));
		$this->SetAligns(array('C','L','L','C','L','L','L','L','L'));
		$this->SetFont('Arial','',9);
		$ctr = 1;
		
		$docreceived = $objDocDetail->getDocumentProcessedBYOffice($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo']);
		
		$arrDocumentId = array();
		//BEGIN FROM DOCUMENT RECEIVED
		foreach($docreceived as $doc):
			$documentId = $doc['documentId'];
			array_push($arrDocumentId, "'".$documentId."'");

			$docdetail = $objDocDetail->getDocumentDetails($documentId);
			$receivedData = $objDocDetail->getDocumentReceived($doc['historyId']);

			$subject = strlen($docdetail['subject']) > 50 ? substr($docdetail['subject'], 0,50).' ...' : $docdetail['subject'];
			$doctype = $docdetail['documentTypeAbbrev'];
			$receivedBy = $receivedData[0]['receivedBy']." (".$doc['recipientId'].")";
			$dateReceived = $receivedData[0]['dateReceived'];

			$dateProcessed = $doc['dateAdded'];
			$processed_by = $objDocDetail->getProcessedbyName($doc['addedById']);
			$process_date = date('Y-m-d', strtotime($doc['dateAdded']));
			$process_time = date('H:i:s', strtotime($doc['dateAdded']));
			$received_date = date('Y-m-d', strtotime($dateReceived));
			$received_time = date('H:i:s', strtotime($dateReceived));
			
			// $action_taken = $objDocDetail->getActionTakenDetails($doc['actionTakenCodeID']);
			// $actionTaken = $action_taken[0]['actionDesc'];
			$actionTaken = '';
			$totaltime = array();
				
			$dates = array($received_date);
			$thisdate = date('Y-m-d', strtotime(end($dates)));
			$days = 0;
			$totalPTime = '';
			// do{
			// 	$isholiday = $objDocDetail->isHoliday($thisdate);
			// 	if(date('D', strtotime($thisdate)) != 'Sat' && date('D', strtotime($thisdate)) != 'Sun' && $isholiday != 1){
			// 		if($received_date != $process_date){
			// 			# RECEIVED TIME
			// 			if($thisdate == $received_date) {
			// 				$rt = new DateTime($received_time);
			// 				$fixrt = new DateTime('18:00:00');
			// 				$interval = $rt->diff($fixrt);
			// 				$receivedTime = $interval->format('%H').":".$interval->format('%I').":".$interval->format('%S');
			// 				array_push($totaltime, $receivedTime);
			// 			}
			// 			# PROCESSED TIME
			// 			else if($thisdate == $process_date) {
			// 				$pt = new DateTime($process_time);
			// 				$fixpt = new DateTime('07:00:00');
			// 				$interval = $fixpt->diff($pt);
			// 				$processedTime = $interval->format('%H').":".$interval->format('%I').":".$interval->format('%S');
			// 				array_push($totaltime, $processedTime);
			// 			}
			// 			# 1 DAY
			// 			else {
			// 				$days++;
			// 			}

			// 		} else {
			// 			$pt = new DateTime($process_time);
			// 			$rd = new DateTime($received_time);
			// 			$interval = $rd->diff($pt);
			// 			$processedTime = $interval->format('%H').":".$interval->format('%I').":".$interval->format('%S');
			// 			array_push($totaltime, $processedTime);
			// 		}
			// 	}
			// 	$thisdate = date('Y-m-d', strtotime(end($dates).' +1 day'));
			// 	$dates[] = $thisdate;
			// }while(end($dates) <= date('Y-m-d', strtotime($doch['dateAdded'])));
			// $totalPTime = $this->times_counter($totaltime);

			$this->Row(array($ctr++,
								$documentId,
								$subject,
								$doctype,
								$receivedBy,
								$dateReceived,
								$dateProcessed,
								$processed_by." (".$doc['senderId'].")",
								$actionTaken,
								$totalPTime), 1);
		endforeach;
		//END FROM DOCUMENT RECEIVED
		
		// if($_GET['reportType']=='DMPR'){
		// 	$doc_history = $objDocDetail->getDocumentHistoryByEmpByDateAdded($_SESSION['userID'], $_GET['dateFrom'], $_GET['dateTo'], implode(',', $arrDocumentId));
		// }else{
		// 	$doc_history = $objDocDetail->getDocumentHistoryByEmpByDateAdded($_SESSION['userID'], $_GET['dateMonth'], $_GET['dateYear'], implode(',', $arrDocumentId));
		// }

		// foreach($doc_history as $doc):
		// 	$receivedId = $doc['receivedId'];
		// 	$documentId = $doc['documentId'];
		// 	$docdetail = $objDocDetail->getDocumentDetails($documentId);
		// 	$subject = $docdetail['subject'];
		// 	$doctype = $docdetail['documentTypeAbbrev'];
		// 	$dateProcessed = $docHistory['dateAdded'];
		// 	$processed_by = $objDocDetail->getProcessedbyName($doc['addedById']);
		// 	$action_taken = $objDocDetail->getActionTakenDetails($doc['actionTakenCodeID']);
		// 	$actionTaken = $action_taken[0]['actionDesc'];

		// 	$this->Row(array($ctr++,
		// 						$documentId,
		// 						$subject,
		// 						$doctype,
		// 						'',
		// 						'',
		// 						$dateProcessed,
		// 						$processed_by,
		// 						$actionTaken,
		// 						''), 1);
		// endforeach;

		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		
		$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','28','45','25','25','25','25','25','27','35','10','28','45','25','25','25','25','25','27','35');
		$cell_caption = array(" ","DOC ID","SUBJECT","DOC TYPE","RECEIVED","DATE/TIME","DATE/TIME","PROCESSED","RELEASED TO","PROCESSING"," ","","","","BY","RECEIVED","PROCESSED","BY","","TIME");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'BRL' : 'TRL',$key == 9 ? 1 : 0,"C",1);
		endforeach;
		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Cell(90,4,'Person Responsible', 0, 0, 'C');
		$this->Cell(90,4,'Verified By', 0, 0, 'C');
		$this->Cell(90,4,'Approved for filing', 0, 1, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(80,5,'', 'B', 0, 'C');
		$this->Cell(10,5,'', 0, 0, 'C');
		$this->Cell(90,5,'', 'B', 0, 'C');
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}


	function times_counter($times)
	{
		$day = 0;
		$hou = 0;
		$min = 0;
		$sec = 0;
		$totaltime = '00:00:00';

		if(count($times) > 1) {
			if(is_array($times)){

	            $length = sizeof($times);

	            for($x=0; $x <= $length; $x++){
	                    $split = explode(":", @$times[$x]); 
	                    $this->hou += @$split[0];
	                    $this->min += @$split[1];
	                    $this->sec += @$split[2];
	            }

	            $seconds = $this->sec % 60;
	            $minutes = $this->sec / 60;
	            $minutes = (integer)$minutes;
	            $minutes += $this->min;
	            $hours = $minutes / 60;
	            $minutes = $minutes % 60;
	            $hours = (integer)$hours;
	            $hours += $this->hou % 24;
	        }
	    } else {
	    	$time = explode(':', $times[0]);
	    	$day = 0;
	    	$hours = $time[0];
	    	$minutes = $time[1];
	    	$seconds = $time[2];
	    }
	    $day = $hours / 24;
	    $day = intval($day);
	    $hours = $hours - ($day*24);
	    // echo $day;
	    // print_r($times);
	    $totaltime = $day.($day > 1 ? " Days " : " Day ").sprintf("%02d", $hours).($hours > 1 ? " Hours " : " Hour ").sprintf("%02d", $minutes).($minutes > 1 ? " Minutes " : " Minute ").sprintf("%02d", $seconds).($seconds > 1 ? " Seconds" : " Second");
            return $totaltime;
	}

}
?>
