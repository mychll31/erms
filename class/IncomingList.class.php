<?php
// session_start();
include_once("General.class.php");
include_once("MySQLHandler2.class.php");
class IncomingList extends General {

	function getIncomingList($t_strSearch="", $page="", $limit="", $ascdesc="", $blnCount=false,$_strOperaor='',$t_blnAgencyDocsOnly=FALSE,$t_strSource='module')
	{
		if($t_strSearch!="")
		{
			$strSearch .= " AND ";
			switch($_strOperaor){
				case 'OR': $arrSearch = explode('OR',$t_strSearch);
						   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
					   					 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
						   break;
				case 'EXCEPT': break;				
				case 'AND': $arrSearch = explode(' ',$t_strSearch);	
							$strSearch .= (count($arrSearch)>1 && $arrSearch[1] != '')?
									  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
									  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
									  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
							 break;				
				case 'DOCTYPE': 
					$arrSearch = explode(' ',$t_strSearch);
					$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
					$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
								break;								
				case 'CONTAINS': 
					$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
					$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
					break;				
			}
		}
		//filter for groups
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(count($rsGroup)>0) 
		{
			for($i=0;$i<count($rsGroup);$i++){
				$t_strOrWhere .= " OR tblHistory.recipientId = '".$rsGroup[$i]['groupId']."' ";	
				$t_strOrWhere .= " OR tblHistory.recipientId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		}
		/*$arrAgency = $this->getOfficeGroupEmployee($this->get('userID'));
		$arrOffice = $this->getAgency($arrAgency[0]['groupCode']);
		if(count($arrOffice)>0){
		 for($i=0;$i<count($arrOffice);$i++)
		  $arrOriginId[] = $arrOffice[$i]['originId'];	
		  $t_strWhereGroup .= " OR tblHistory.recipientId IN (".implode(',',$arrOriginId).") ";	
		}*/
		
		//else
			//$t_strWhereGroup = " OR tblHistory.recipientId = '".$rsGroup[0]['groupCode']."' ";	
		//filter for heads of the office
		$officeHead = $this->getOfficeHead($this->get("office"));
		if($officeHead<>"")
			$t_strOrWhere .= " OR tblHistory.recipientId='".$officeHead[0]["empNumber"]."'";
/*		
		//filter for documents in their head office (e.g. eo,service)
		$res = $this->getOrgStructureUnder($this->get("userID"));
		//print_r($res);
		if (count($res)!=0){
		  $t_strOrWhere .= " OR tblHistory.recipientId='".$res[0]."' ";
		}
		for($t=1;$t<count($res);$t++){
		  $t_strOrWhere .= " OR tblHistory.recipientId='".$res[$t]."' ";
		}

*/
		if($t_strOrWhere!=""){
		  $t_strAndWhere .= ($t_strSource=='module')?" AND tblHistory.receivedBy is NULL ":" AND tblHistory.receivedBy is NULL ";									
		}
//		$t_strOrWhere .= " OR tblHistory.recipientId='0' ";
		$whereInOriginId = '';		
		if($t_blnAgencyDocsOnly){
		 $sql1= new MySQLHandler();
		 $sql1->init();
		 $s_sql = "SELECT empNumber FROM `tblUserAccount` WHERE `agencyCode` !=0";
		 $rs = $sql1->Select($s_sql); 
		 foreach($rs as $originId){
			 $arrInOriginId[] =$originId[0];
		 }
		 $strInOriginId ="('".implode("','", $arrInOriginId)."')";
		 $whereInOriginId = " AND tblDocument.addedById in $strInOriginId ";
		}
		
		$strIncomingGroup = $this->get('grp')!="" ? $this->get('grp') : $this->get('office');
		$t_strOrWhere .= " OR (tblHistory.recipientId='0' AND tblHistory.senderId <>'".$strIncomingGroup ."')";	

		$strSqlIN = "SELECT MAX( tblHistory.historyId ) FROM `tblHistory`  WHERE (tblHistory.recipientId='".$strIncomingGroup ."' $t_strOrWhere) $t_strAndWhere GROUP BY tblHistory.documentId";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs = $sql1->Select($strSqlIN);
		foreach($rs as $historyId){
			 $arrIN[] =$historyId[0];
		}
		$strIn ="('".implode("','", $arrIN)."')";
		$page = ($page==0||$page=="")?1:$page;
		$offset = ($page - 1) * $limit;

	    if(strlen($page)>0 && strlen($limit)>0)
		  $strLimit = " LIMIT $offset, $limit";		

		$arrSelect = array('total'=>'count(*) as _total',
							'data'=>'tblDocument. * , tblHistory.receivedBy, tblHistory.historyId, tblHistory.recipientId,tblHistory.recipientUnit,tblHistory.actionCodeId, tblHistory.senderId,tblHistory.senderUnit, tblHistory.flag, tblHistory.priority, tblHistory.dateSent, tblHistory.restricted,f1.path, f1.filename as attachment');
		foreach( $arrSelect as $idx=>$select){
		  $strSqlLimit = $idx=='data'?$strLimit:'';	
		  $strSql = "SELECT $select
			  FROM tblHistory
			  LEFT JOIN tblDocument ON tblDocument.documentId = tblHistory.documentId
			  LEFT JOIN tblTrash ON tblHistory.historyId = tblTrash.historyId
			  LEFT JOIN tblFIle f1 ON tblDocument.documentId=f1.documentId
			  LEFT OUTER JOIN tblFIle f2 on (tblDocument.documentId=f2.documentId AND f1.fileId < f2.fileId)
			  WHERE (tblHistory.recipientId='".$strIncomingGroup."' $t_strOrWhere) $t_strAndWhere
			  AND tblDocument.documentId NOT IN (SELECT tblDocumentFolders.documentId FROM tblDocumentFolders)
			  AND tblHistory.historyId NOT IN (SELECT tblTrash.historyId FROM tblTrash)
			  AND tblDocument.isDelete = 0
			  AND tblHistory.historyId IN
			  $strIn
			  $strSearch 
			  $whereInOriginId 
			  AND f2.fileId IS NULL
			  ORDER BY tblHistory.dateAdded DESC $strSqlLimit";

		  $sql1= new MySQLHandler();
		  $sql1->init();
		  $rs=$sql1->Select($strSql);

		  if($blnCount) return $rs;
		  $arrRes[$idx] = $rs;
		}				
		
	  //$sql =  implode(';',$arrSql);
	  //$sql1= new MySQLHandler();
	  //$sql1->init();
	  //$rs=$sql1->Select($sql);
	  return $arrRes;	
	}

	function getIncomingListTablet($t_strSearch="", $page="", $limit="", $ascdesc="", $blnCount=false,$_strOperaor='',$t_blnAgencyDocsOnly=FALSE,$t_strSource='module')
	{
		$strIncomingGroup = $this->get('grp')!="" ? $this->get('grp') : $this->get('office');
		$t_strOrWhere .= " OR (tblHistory.recipientId='0' AND tblHistory.senderId <>'".$strIncomingGroup ."')";

		$strSql = "SELECT * FROM tblHistory
					WHERE (tblHistory.recipientId = '$strIncomingGroup' $t_strOrWhere)
					AND (tblHistory.receivedBy = '' or tblHistory.receivedBy is null)
					ORDER BY dateAdded DESC;";
		// echo $strSql;
		// die();
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($strSql);
		echo count($rs);
		echo '<pre>';
		print_r($rs);
		die();


		if($t_strSearch!="")
		{
			$strSearch .= " AND ";
			switch($_strOperaor){
				case 'OR': $arrSearch = explode('OR',$t_strSearch);
						   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
					   					 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
						   break;
				case 'EXCEPT': break;				
				case 'AND': $arrSearch = explode(' ',$t_strSearch);	
							$strSearch .= (count($arrSearch)>1 && $arrSearch[1] != '')?
									  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
									  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
									  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
							 break;				
				case 'DOCTYPE': 
					$arrSearch = explode(' ',$t_strSearch);
					$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
					$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
								break;								
				case 'CONTAINS': 
					$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
					$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
					break;				
			}
		}

		//filter for groups
		$rsGroup = $this->getUserProject($this->get('userID'));
		if(count($rsGroup)>0) 
		{
			for($i=0;$i<count($rsGroup);$i++){
				$t_strOrWhere .= " OR tblHistory.recipientId = '".$rsGroup[$i]['groupId']."' ";	
				$t_strOrWhere .= " OR tblHistory.recipientId = '".$rsGroup[$i]['groupCode']."' ";	
			}
		}

		$officeHead = $this->getOfficeHead($this->get("office"));
		if($officeHead<>"")
			$t_strOrWhere .= " OR tblHistory.recipientId='".$officeHead[0]["empNumber"]."'";
		if($t_strOrWhere!=""){
		  $t_strAndWhere .= ($t_strSource=='module')?" AND tblHistory.receivedBy is NULL ":" AND tblHistory.receivedBy is NULL ";									
		}

		$whereInOriginId = '';		
		if($t_blnAgencyDocsOnly){
		 $sql1= new MySQLHandler();
		 $sql1->init();
		 $s_sql = "SELECT empNumber FROM `tblUserAccount` WHERE `agencyCode` !=0";
		 $rs = $sql1->Select($s_sql); 
		 foreach($rs as $originId){
			 $arrInOriginId[] =$originId[0];
		 }
		 $strInOriginId ="('".implode("','", $arrInOriginId)."')";
		 $whereInOriginId = " AND tblDocument.addedById in $strInOriginId ";
		}
		
		$strIncomingGroup = $this->get('grp')!="" ? $this->get('grp') : $this->get('office');
		$t_strOrWhere .= " OR (tblHistory.recipientId='0' AND tblHistory.senderId <>'".$strIncomingGroup ."')";	

		$strSqlIN = "SELECT MAX( tblHistory.historyId ) FROM `tblHistory`  WHERE (tblHistory.recipientId='".$strIncomingGroup ."' $t_strOrWhere) $t_strAndWhere GROUP BY tblHistory.documentId";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs = $sql1->Select($strSqlIN);
		foreach($rs as $historyId){
			 $arrIN[] =$historyId[0];
		}
		$strIn ="('".implode("','", $arrIN)."')";
		$page = ($page==0||$page=="")?1:$page;
		$offset = ($page - 1) * $limit;

	    if(strlen($page)>0 && strlen($limit)>0)
		  $strLimit = " LIMIT $offset, $limit";		

		$arrSelect = array('total'=>'count(*) as _total',
							'data'=>'tblDocument. * , tblHistory.receivedBy, tblHistory.historyId, tblHistory.recipientId,tblHistory.recipientUnit,tblHistory.actionCodeId, tblHistory.senderId,tblHistory.senderUnit, tblHistory.flag, tblHistory.priority, tblHistory.dateSent, tblHistory.restricted,f1.path, f1.filename as attachment');
		foreach( $arrSelect as $idx=>$select){
		  $strSqlLimit = $idx=='data'?$strLimit:'';	
		  $strSql = "SELECT $select
			  FROM tblHistory
			  LEFT JOIN tblDocument ON tblDocument.documentId = tblHistory.documentId
			  LEFT JOIN tblTrash ON tblHistory.historyId = tblTrash.historyId
			  LEFT JOIN tblFIle f1 ON tblDocument.documentId = f1.documentId
			  LEFT OUTER JOIN tblFIle f2 on (tblDocument.documentId = f2.documentId AND f1.fileId < f2.fileId)
			  WHERE (tblHistory.recipientId='".$strIncomingGroup."' $t_strOrWhere) $t_strAndWhere
			  AND tblDocument.documentId NOT IN (SELECT tblDocumentFolders.documentId FROM tblDocumentFolders)
			  AND tblHistory.historyId NOT IN (SELECT tblTrash.historyId FROM tblTrash)
			  AND tblDocument.isDelete = 0
			  AND tblHistory.historyId IN
			  $strIn
			  $strSearch 
			  $whereInOriginId 
			  AND f2.fileId IS NULL
			  ORDER BY tblHistory.dateAdded DESC $strSqlLimit";

		  $sql1= new MySQLHandler();
		  $sql1->init();
		  $rs=$sql1->Select($strSql);

		  if($blnCount) return $rs;
		  $arrRes[$idx] = $rs;
		}				

	  return $arrRes;	
	}


function getPickUpList($t_strOffice, $page="", $limit="", $order="", $ascdesc="",$t_strSearch='',$t_strOperator='')
	{
	if($t_strSearch!="") 
	{
		$strSearch .= " AND ";
		switch($t_strOperator){
			case 'OR': $arrSearch = explode('OR',$t_strSearch);
					   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
									 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
					   break;
			case 'EXCEPT': break;				
			case 'AND': $arrSearch = explode(' ',$t_strSearch);	
						$strSearch .= (count($arrSearch)>1 && $arrSearch[1] != '')?
								  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
								  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
								  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
						 break;				
			case 'DOCTYPE': 
				$arrSearch = explode(' ',$t_strSearch);
				$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
				$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
							break;								
			case 'CONTAINS': 
				$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
				$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
				break;				
		}
	}
	$res = $this->getOrgStructureUnder($this->get("userID"));
	for($t=0;$t<count($res);$t++)
		{
		//$t_strWhereUnder .= " OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy<>'') ";
		
		if ($t==0)  $t_strWhereUnder .= "(tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1)";	
		
		$t_strWhereUnder .= "OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1)";		
		}
		
//		echo "" . $t_strWhereUnder ;
	
//	if($this->getOfficeHead($this->get('office'))==$this->get('userID'))
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.receivedBy<>'') ";
//	    $t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' ) ";		
//	else
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy is NULL) ";	
//		$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1) ";	
	
//	if($t_blnAgencyDocsOnly){
		 $sql1= new MySQLHandler();
		 $sql1->init();
		 $s_sql = "SELECT agencyCode FROM `tblUserAccount` WHERE `agencyCode` !=0";
		 $rs = $sql1->Select($s_sql); 
		 foreach($rs as $originId){
			 $arrInOriginId[] =$originId[0];
		 }
		 $strInOriginId ="('".implode("','", $arrInOriginId)."')";
		 $whereInOriginId = " AND tblHistory.recipientId in $strInOriginId ";
//		}

//		echo " // " .  $whereInOriginId ;
	
	$t_strWhereFilter = " OR (tblHistory.recipientId='0' AND tblHistory.restricted=0)";
	$t_strWhereAgency = " AND tblHistory.recipientUnit='agency' AND tblHistory.mode='ext'";
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ( SELECT DISTINCT tblDocument.*,tblHistory.receivedBy,tblHistory.historyId,tblHistory.actionCodeId,tblHistory.flag,tblHistory.priority ,tblHistory.dateSent,tblHistory.senderID, tblHistory.recipientId FROM tblDocument 
	LEFT JOIN tblHistory ON tblDocument.documentId=tblHistory.documentId 
	WHERE tblHistory.senderId='RMS' $t_strWhereAgency   
	AND tblDocument.isDelete=0  AND tblHistory.actionCodeId ='23' 
	AND tblHistory.historyId NOT IN  (SELECT tblTrash.historyId FROM tblTrash)
	
	$strSearch
	
	HAVING (SELECT MAX(dateSent) FROM tblHistory)
	ORDER BY tblHistory.dateSent DESC, tblDocument.documentId ASC) result ,
	(SELECT FOUND_ROWS() AS '_total') tot 
	 ";
	//GROUP BY tblHistory.documentId 	
//	WHERE (tblHistory.senderId='".$this->get('userID')."' 	$t_strWhereAgency) 	

		$page = ($page==0||$page=="")?1:$page;
		$offset = ($page - 1) * $limit;
		if(strlen($page)>0 && strlen($limit)>0)
		$sql .= " LIMIT $offset, $limit";		
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}
	
	function receiveDocument($t_strDocID,$t_strReceivedBy,$t_strHistoryID,$t_blnMode='')
	{
		$sql = "UPDATE tblHistory SET receivedBy='".$t_strReceivedBy."',dateReceived='".date("Y-m-d H:i:s")."' WHERE historyId='".$t_strHistoryID."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);
		
		$sql = $t_blnMode=='UPDATE'?"UPDATE tblHistory SET receivedBy='".$t_strReceivedBy."',dateReceived='".date("Y-m-d H:i:s")."' WHERE historyId='".$t_strHistoryID."'":	
						   "INSERT INTO tblDocumentReceived (historyId,empNumber,documentId,dateReceived,receivedBy) values('$t_strHistoryID','".$this->get('userID')."','$t_strDocID',NOW(),'$t_strReceivedBy')"; 
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$t_blnMode=='UPDATE'?$sql1->Update($sql):$sql1->Insert($sql);	

		return $rs;
	}

	function receiveDocumentPickUp($t_strDocID,$t_strReceivedBy,$t_strHistoryID,$t_strRecipientID,$t_blnMode='')
	{
		$sql = "UPDATE tblHistory SET receivedBy='".$t_strReceivedBy."',dateReceived='".date("Y-m-d H:i:s")."' WHERE historyId='".$t_strHistoryID."'";
						   
		$sqll = "INSERT INTO tblDocumentReceived (historyId,empNumber,documentId,dateReceived,receivedBy) values('$t_strHistoryID','".$t_strRecipientID."','$t_strDocID',NOW(),'$t_strReceivedBy')";
			
		$sql2= new MySQLHandler();
		$sql2->init();
		$rs2=$sql2->Update($sql);	
						   
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Insert($sqll);	
		
		return $rs;
	}

	function removeReceiveDocument($t_intHistoryId)
	{
		$sql = "UPDATE tblHistory SET receivedBy=NULL,dateReceived='' WHERE historyId='".$t_intHistoryId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);
		return $rs;	
	}
	
	function getHistoryStatus($t_strHistoryId)
	{
		$sql = "SELECT status FROM tblDocument LEFT JOIN tblHistory 
				ON tblDocument.documentId=tblHistory.documentId 
				WHERE historyId='".$t_strHistoryId."'";	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);	
		return $rs[0]['status'];		
	}
	
	function getStatusPageLink($t_strStatus)
	{
		switch($t_strStatus)
		{
		case '0': $page = "showIncoming.php"; break;
		case '1': $page = "showOutgoing.php"; break;
		default: $page = "#"; break;
		}
		return $page;	
	}
	
	function checkReadHistory($t_strDocumentId,$t_strUserId)
	{
		$sql = "SELECT * FROM tblReadHistory WHERE userId='".$t_strUserId."' AND documentId='".$t_strDocumentId."'";	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);	
		return $rs;	
	}
		
	function markRead($t_strDocumentId,$t_strUserId)
	{
		if(count($this->checkReadHistory($t_strDocumentId,$t_strUserId))==0)
			{
			$sql = "INSERT INTO tblReadHistory(userId,documentId) VALUES('".$t_strUserId."', '".$t_strDocumentId."')";
			$sql1= new MySQLHandler();
			$sql1->init();
			$rs=$sql1->Insert($sql);				
			if(count($rs))
				return 1;				
			}
	}
	

	
	function markUnread($t_strDocumentId,$t_strUserId)
	{
		if(count($this->checkReadHistory($t_strDocumentId,$t_strUserId))==1)
			{
			$sql = "DELETE FROM tblReadHistory WHERE userId='".$t_strUserId."' AND documentId='".$t_strDocumentId."'";	
			$sql1= new MySQLHandler();
			$sql1->init();
			$rs=$sql1->Delete($sql);	
			if(count($rs))
				return 1;				
			}
	}
	
	function flagDocument($t_intHistoryId, $t_intFlag='')
	{
		$sql = "SELECT flag FROM tblHistory WHERE historyId='".$t_intHistoryId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs1=$sql1->Select($sql);
		$t_intFlag = $rs1[0]['flag'];
		if($t_intFlag)
			$t_intFlag = 0;
		else
			$t_intFlag = 1;	
		
		$sql2 = "UPDATE tblHistory SET flag=$t_intFlag WHERE historyId='".$t_intHistoryId."'";	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs2=$sql1->Update($sql2);
		if(count($rs2))
			{
			if($t_intFlag)
				echo "<span class='ui-icon ui-icon-flag' title='Flag' ></span>";
			}		
	}

	function priorDocument($t_intHistoryId, $t_intPrior='')
	{
		$sql = "SELECT priority FROM tblHistory WHERE historyId='".$t_intHistoryId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs1=$sql1->Select($sql);
		$t_intPrior = $rs1[0]['priority'];
		if($t_intPrior)
			$t_intPrior = 0;
		else
			$t_intPrior = 1;	
		
		$sql2 = "UPDATE tblHistory SET priority=$t_intPrior WHERE historyId='".$t_intHistoryId."'";	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs2=$sql1->Update($sql2);
		if(count($rs2))
			{
			if($t_intPrior)
				echo "<span class='ui-icon ui-icon-notice' title='Priority' ></span>";
			}		
	}
	
	function checkDocReceivedStatus($t_strHistoryID){
		$arrCustodian = $this->getCustodian();
		$strOffice = $this->get('office');
		$activeUserID = $this->get('userID');
		$arrCustodian = $this->getCustodian($strOffice);
		$sqlAND = "AND empNumber LIKE '$activeUserID'";
		//this checks for custodian of same office code
		if(count($arrCustodian)>0){
		  $strCustodian = "'".implode("','",$arrCustodian)."'";
		  $sqlAND = "AND empNumber IN ($strCustodian) ";
		} 
		$query = "SELECT count(*) as COUNT FROM tblDocumentReceived WHERE historyId = $t_strHistoryID $sqlAND ";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return ($rs[0]['COUNT']>0)?TRUE:FALSE;
	}

	function checkDocReceivedStatusPickUp($t_strHistoryID){
		
		$query = "SELECT count(*) as COUNT FROM tblDocumentReceived WHERE historyId = $t_strHistoryID";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return ($rs[0]['COUNT']>0)?TRUE:FALSE;
	}	

	function checkExistsById($docId){
		
		$query = "SELECT count(*) as COUNT FROM tblDocument WHERE documentId = '".$docId."'";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return ($rs[0]['COUNT']>0)?TRUE:FALSE;
	}
	
	/*function getFolderList($t_strUserID)
	{
		$sql = "SELECT * FROM tblDocumentFolders 
				LEFT JOIN tblFolders ON tblDocumentFolders.folderID = tblFolders.folderID 
				WHERE tblFolders.userID='".$t_strUserID."'";	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);	
		return $rs;	
	}*/ 

	function getUnactedDocuments($page, $mode, $report=0, $complete=0){
		$sel = 'COUNT( * ) AS _total';
		$ctrLimit = $report==0 ? 15 : 100;

		if($mode == 'all'){
			$sel = '*';
		}else{
			if($mode!='total'){
				$lmt = "LIMIT ".($page*15).",15";
				$sel = '*';
			}
		}

		$query = "SELECT ".$sel." FROM (SELECT historyId AS hid FROM tblHistory e GROUP BY e.documentId) b
					INNER JOIN tblHistory m ON m.historyId = b.hid
					LEFT JOIN tblDocument d ON d.documentId = m.documentId
					WHERE 1 
					AND m.documentId IS NOT NULL 
					AND m.documentId <>  ''
					AND d.iscomplete = ".$complete."
					AND d.dateAdded >=  '2017-07-31'
					AND (m.recipientId = '".$this->get("office")."' OR m.recipientId = '".$this->get('userID')."')
					AND DATE_ADD( m.dateSent, INTERVAL 15 DAY ) < CURDATE( ) 
					ORDER BY m.dateSent desc ".$lmt;
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function getOverDue($page, $mode, $status){
		$sel = 'COUNT( * ) AS _total';
		$psdate = 'm.dateSent';
		$pedate = 'DATE_ADD(m.dateSent, INTERVAL 3 DAY)';
		$oedate = 'DATE_ADD(d.dateAdded, INTERVAL 15 DAY)';

		if($mode == 'all'){
			$sel = '*';
		}else{
			if($mode!='total'){
				$lmt = "LIMIT ".($page*15).",15";
				$sel = '*';
			}
		}

		$query = "SELECT ".$sel." FROM (SELECT MAX( e.historyId ) AS hid FROM tblHistory e GROUP BY e.documentId) b
					INNER JOIN tblHistory m ON m.historyId = b.hid
					LEFT JOIN tblDocument d ON d.documentId = m.documentId
					LEFT JOIN tblOriginOffice on tblOriginOffice.originId = m.senderId
					WHERE 1 
					AND m.documentId IS NOT NULL 
					AND m.documentId <>  ''
					AND d.iscomplete = 0
					AND (m.recipientId = '".$this->get("office")."' OR m.recipientId = '".$this->get('userID')."')";
		if($status=="pending")
			$query.=" AND (m.dateSent <= DATE_ADD( m.dateSent, INTERVAL 3 DAY) AND DATE_ADD( m.dateSent, INTERVAL 3 DAY) >= CURDATE( ))";
		else if($status=="overdue")
			$query.=" AND CURDATE( ) between ".$pedate." AND ".$oedate;
		else if($status=="deadlines")
			$query.=" AND (CURDATE( ) = d.deadline or m.recipientDeadline = CURDATE( ))";
		else
			$query.=" AND (m.dateSent <= DATE_ADD( m.dateSent, INTERVAL 15 DAY) AND DATE_ADD( m.dateSent, INTERVAL 15 DAY) >= CURDATE( ))";
		$query.=" ORDER BY d.dateAdded desc ".$lmt;
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	## TABLET FUNCTION ##
	function getPendingList($senderId, $recipientId)
	{
		if($recipientId == 'all'){
			$query = "SELECT tblHistory.historyId, senderId, tblHistory.documentId, tblDocument.docNum, tblDocumentType.documentTypeAbbrev, tblHistory.dateSent  FROM `tblHistory`
					LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
					LEFT JOIN tblDocumentType on tblDocumentType.documentTypeId = tblDocument.documentTypeId
					WHERE `senderId` LIKE '".$senderId."' AND `dateSent` LIKE '2018%' AND receivedBy IS null
					ORDER BY dateSent DESC LIMIT 0,100";	
		}else{
			$query = "SELECT tblHistory.historyId, senderId, tblHistory.documentId, tblDocument.docNum, tblDocumentType.documentTypeAbbrev, tblHistory.dateSent  FROM `tblHistory`
					LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
					LEFT JOIN tblDocumentType on tblDocumentType.documentTypeId = tblDocument.documentTypeId
					WHERE `senderId` LIKE '".$senderId."' AND recipientId = '".$recipientId."' AND `dateSent` LIKE '2018%' AND receivedBy IS null
					ORDER BY dateSent DESC";	
		}
		
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function getOfficeList($senderId)
	{
		$query = "SELECT recipientId, officeName
					FROM `tblHistory` 
					Left join tblOriginOffice on tblHistory.recipientId = tblOriginOffice.originId
					WHERE `senderId` LIKE '".$senderId."' AND `dateSent` LIKE '2018%' and receivedBy is null and recipientId REGEXP '^[0-9]+$' and recipientId != '0' and recipientId is not null
					group by recipientId order by officeName ASC";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function getOfficeList2($senderId)
	{
		$query = "SELECT recipientId, recipientId as officeName FROM `tblHistory` WHERE `senderId` LIKE '".$senderId."' AND `dateSent` LIKE '2018%' and receivedBy is null and recipientId REGEXP '^[A-Z]+$' and recipientId != '0' and recipientId is not null group by recipientId";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function getOfficeList3($senderId)
	{
		$query = "SELECT recipientId FROM `tblHistory` WHERE `senderId` LIKE '".$senderId."' AND `dateSent` LIKE '2018%' and receivedBy is null and recipientId NOT REGEXP '^[A-Z]+$' and recipientId NOT REGEXP '^[0-9]+$' and recipientId != '0' and recipientId is not null group by recipientId";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		$empNumber = implode(',', array_map(function ($entry) {return "'".$entry['recipientId']."'";}, $rs));

		$query = "SELECT 'user' as recipientId, empNumber, concat(surname,', ',firstname) as officeName FROM `tblEmpPersonal` where empNumber in (".$empNumber.") order by officeName ASC";
		$stmt= new MySQLHandler2();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function getPendingList2($dateFrom, $dateTo, $office, $receiverId='')
	{
		if($receiverId != ''):
			$SQLreceiverId = "AND recipientId = '".$receiverId."'";
		endif;

		$query = "SELECT *  FROM `tblHistory`
					LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
					LEFT JOIN tblDocumentType on tblDocumentType.documentTypeId = tblDocument.documentTypeId
					WHERE senderId = '".$office."' ".$SQLreceiverId." AND dateSent between '".$dateFrom." 00:00:00' and '".$dateTo." 23:59:59'
					AND (receivedBy = '' OR receivedBy	IS NULL)";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}

	function ifHasReceivedDoc($historyId)
	{
		$query = "SELECT *  FROM `tblDocumentReceived` WHERE `historyId` = ".$historyId." LIMIT 1";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return count($rs) > 0 ? 1 : 0;
	}

	function saveSignature($arrdocId, $historyId, $filePath, $receivedby)
	{
		$query = '';
		foreach($arrdocId as $key=>$docid):
			$query = "INSERT INTO tblSignature (historyId, documentId, signature_location) VALUES ('".$historyId[$key]."','".$docid."','".$filePath."')";
			$sql= new MySQLHandler();
			$sql->init();
			$rs=$sql->Insert($query);
		endforeach;
	}

	function getImgLocation($docid, $historyid)
	{
		$query = "SELECT signature_location FROM `tblSignature` WHERE `historyId` = ".$historyid." AND `documentId` = '".$docid."' LIMIT 1";
		$stmt= new MySQLHandler();
		$stmt->init();
		$rs=$stmt->Select($query);
		return $rs;
	}
	
}

?>