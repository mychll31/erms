<?php
	session_start();
	header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	define('FPDF_FONTPATH','font/');
	require_once('fpdf.php');

	class ReportDocumentAckReceipt extends FPDF
	{	

		var $ReportType;
		function generateReport()
		{
			$this->SetFont('Arial','B',12);
			$len = strlen(strtoupper($_GET['to']))+37+18;
			$this->Cell(0,8,'ACKNOWLEDGEMENT RECEIPT',0,1,'C');
			
			if($_GET['docid']!=''):
				include_once("Incoming.class.php");
				include_once("General.class.php");
				$objIncDocDetail = new Incoming;
				$objGen = new General;

				$this->documentId = $objIncDocDetail->getDocument($_GET['docid']);
				$this->documentId = $this->documentId[0];
				$this->originId = $objIncDocDetail->getOriginOfficeById($this->documentId['originId']);
				$officename_owner = $objGen->getOfficeNameBYAbbv($_SESSION['office']);

				$address = '';
				$officename = '';
				$contact = '837-2071 to 82 / 837-3171 to 89';
				if($this->documentId['originUnit']=='agency'){
					$officename = $this->originId[0]['officeName'];
					$address = $this->originId[0]['address'];
				}else{
					$officename = $objGen->getOfficeNameBYAbbv($this->documentId['originId']);
					$address = 'Gen. Santos Ave., Bicutan, Taguig City';
				}

				$this->Ln(10);
				$this->SetFont('Arial','',11);
				$this->Cell(0,5,' '.ucfirst(date("d F Y")),0,1,'R');
				$this->Ln(10);
				$this->SetFont('Arial','',11);
				$this->MultiCell(0,5,'The Office of the Secretary hereby acknowledges the receipt of your letter which has been uploaded to the Electronic Records Management System (eRMS) with the following information:',0,'J',false);
				$this->Ln(8);

				$this->infoLabel('Date Received:', ucfirst(date('d F Y', strtotime($this->documentId['dateReceived']))));
				$this->infoLabel('Sender:', ucfirst($this->documentId['sender']));
				$this->infoLabel(ucfirst($this->documentId['originUnit']).':', ucfirst($officename));
				$this->infoLabel('Address:', ucfirst($address));
				$this->Cell(30,7,'Re:',0,0,'L');
				$this->MultiCell(0,5,ucfirst($this->documentId['subject']),0,'J',false);
				$this->Ln(8);

				// $this->SetFont('','');
				$this->WriteHTML('The said document was referred to the <b>'.strtoupper($_GET['to']).'</b> on <b>'.strtoupper(date('d F Y', strtotime($_GET['dateSent']))).'</b>.');
				// $this->Write(5,'The said document was referred to the ');
				// $this->SetFont('','B');
				// $this->Write(5, strtoupper($_GET['to']));
				// $this->SetFont('','');
				// $this->Write(5,' on ');
				// $this->SetFont('','B');
				// $this->Write(5, strtoupper(date('d F Y', strtotime($_GET['dateSent']))));
				// $this->SetFont('','');
				// $this->Write(5,'.');
				$this->Ln(8);

				// $this->SetFont('','');
				$this->WriteHTML('To follow up for further action on the document, you may contact their office through DOST Trunk Line '.$contact.'. Please  cite the  eRMS  Document  Tracking  Number <b>'.strtoupper($_GET['docid']).'</b>.');
				// $this->MultiCell(0,5,'To Follow up for further action on the document, you may contact their office through DOST Trunk Line '.$contact.'. Please  cite the  eRMS  Document  Tracking  Number',0,'J',false);
				// $this->SetFont('','B');
				// if($len >= 78)
				// 	$this->SetY(176);
				// if($len >= 101)
				// 	$this->SetY(176);
				// if($len >= 80 and $len < 100)
				// 	$this->SetY(176);
				// if($len >= 35 and $len < 80)
				// 	$this->SetY(171);
				// if($len >= 135)
				// 	$this->SetY(176);
				// else if(strlen(strtoupper($_GET['to'])) <= 25)
				// 	$this->SetY(171);
				// else
				// 	$this->SetY(170);
				// $this->cell(30, 5, '              '.strtoupper($_GET['docid']).'.',0,0,'L');
				$this->Ln(8);
				// $this->SetFont('','');
				$this->WriteHTML('This is a computer generated communication and does not require a signature.');
				// $this->WriteHTML('awerwer<b>WOErui</b>');
			else:
				$this->SetTextColor(255,0,0);
				$this->Cell(0,8,'NO DOCUMENT ID. Please contact administrator',0,0,'C');
				$this->SetTextColor(0,0,0);
			endif;
		}

		function Header() {
			$imgsrc = dirname(__FILE__).'/dostlogo.png';
			$this->ImagePngWithAlpha($imgsrc,25,10,20,20);
		    $this->SetFont('Arial','B',11);
		    $this->Ln(9);
		    $this->Cell(20);
		    $this->Cell(100,6,'Republic of the Philippines',0,0,'L');
		    $this->SetFont('Arial','i',7);
		    $this->Cell(50,6,'',0,1,'L');
		    $this->Cell(20);
		    $this->SetTextColor(2, 101, 203);
		    $this->SetFont('Arial','B',11);
		    $this->Cell(100,6,'DEPARTMENT OF SCIENCE AND TECHNOLOGY',0,0,'L');
		    $this->SetFont('Arial','B',12);
		    $this->SetTextColor(0, 0, 0);
		    $this->Cell(50,6,'',0,1,'C');
		    $this->Ln(20);
		}

		function Footer()
		{
		    $this->SetY(-15);
		    $this->SetFont('Arial','',8);
		    $this->Cell(100,5,'Head Office: Gen. Santos Ave., Bicutan, Taguig City',0,0,'L');
		    $this->Cell(100,5,'Postal Address: P.O. Box 3596 Manila',0,1,'L');
		    $this->Cell(100,5,'Website: www.dost.gov.ph',0,0,'L');
		    $this->Cell(100,5,'Tel. No. 837-2071 to 82 / 837-3171 to 89',0,0,'L');
		}

		function infoLabel($label, $value)
		{
			$this->SetFont('Arial','',11);
			$this->Cell(30,7,$label,0,0,'L');
			$this->Cell(0,7,$value,0,1,'L');
		}

	}
?>