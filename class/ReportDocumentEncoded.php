<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentEncoded extends FPDF
{	
	function generateReport()
	{
	
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		$rsRecordDetailIncoming = $objRecordDetail->getDocListEncoded($_SESSION['userID'],0,$_GET['dateFrom'],$_GET['dateTo']);
		$rsRecordDetailOutgoing = $objRecordDetail->getDocListEncoded($_SESSION['userID'],1,$_GET['dateFrom'],$_GET['dateTo']);
		$rsRecordDetailIntra = $objRecordDetail->getDocListEncoded($_SESSION['userID'],2,$_GET['dateFrom'],$_GET['dateTo']);
		if(count($rsRecordDetailIncoming))
			$intTotalIncoming = count($rsRecordDetailIncoming);
		else
			$intTotalIncoming = '0';
		if(count($rsRecordDetailOutgoing))
			$intTotalOutgoing = count($rsRecordDetailOutgoing);
		else
			$intTotalOutgoing = '0';
		if(count($rsRecordDetailIntra))
			$intTotalIntra = count($rsRecordDetailIntra);
		else
			$intTotalIntra = '0';
			
		
		$rsRecordHistory = $objRecordDetail->checkHistoryActions($_SESSION['userID'],$_GET['dateFrom'],$_GET['dateTo']);
		if(count($rsRecordHistory))
			$intTotalActions = count($rsRecordHistory);
		else
			$intTotalActions = '0';
			
		$this->SetFont('Arial','B',10);
		$this->Cell(50,7," ",0,0,"C");
		$this->Cell(30,7,"DOCUMENT ENCODED",0,1,"L");
//		$this->Cell(30,7,"100",0,1,"R");
		$this->Cell(70,7," ",0,0,"C");
		$this->Cell(30,7,"INCOMING",0,0,"L");
		$this->Cell(10,7,$intTotalIncoming,0,1,"R");
		$this->Cell(70,7," ",0,0,"C");
		$this->Cell(30,7,"OUTGOING",0,0,"L");
		$this->Cell(10,7,$intTotalOutgoing,0,1,"R");
		$this->Cell(70,7," ",0,0,"C");
		$this->Cell(30,7,"INTRA-OFFICE",0,0,"L");
		$this->Cell(10,7,$intTotalIntra,0,1,"R");
		$this->Ln(5);
		$this->Cell(50,7," ",0,0,"C");
		$this->Cell(30,7,"ACTION RECORDED",0,0,"L");
		$this->Cell(30,7,$intTotalActions,0,1,"R");
		$this->Ln(10);

/*		
		$w = array(15,30,30,30,100,50);
		$Ln = array('L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									
		
			
			if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==2)
			{
				$ctr++;
				$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
			}	
		}
*/		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$objReport->getReportName($_GET['reportType']), 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		

	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
}
?>