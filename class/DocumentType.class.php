<?
include_once("MySQLHandler.class.php");
include_once("General.class.php");

class DocType extends General
{


###########################################
# Function:    getNewId
# Parameters:  N/A
# Return Type: String
# Description: returns newly generated id
###########################################
function getNewId()
{
$strIdConstructor = $this->idConstructor();
$sql="SELECT * FROM tblDocument WHERE documentId LIKE '%$strIdConstructor%' ORDER BY documentId DESC";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
if(sizeof($rs)==0)
{
$strNewId = $strIdConstructor."-00000";
}
else
{
$arID=explode("-",$rs[0]['documentId']);
$intNextNum = (int)$arId[2]+1;
$strNewId =$strIdConstructor."-".str_pad($intNextNum, 5, "0", STR_PAD_LEFT);
}
return $strNewId;
}

###########################################
# Function:    idConstructor
# Parameters:  N/A
# Return Type: String
# Description: returns office-yr pattern  
#			   for creating new document id.
#			   pattern should b same with
#			   user's office and current yr
###########################################

function idConstructor()
{
$userName = $this->get("userName");
$sql="SELECT * FROM tblOffice INNER JOIN tblUser on tblUser.officeId=tblOffice.officeId WHERE tblUser.userName='$userName'";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs[0]['officeCode']."-".date("y");
}


###########################################
# Function:    getDocType
# Parameters:  N/A
# Return Type: Array
# Description: returns list of document types 
###########################################

function getDocType()
{
$sql="SELECT * FROM tblDocumentType ORDER BY documentTypeAbbrev ASC";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}


###########################################
# Function:    getOriginOffice
# Parameters:  N/A
# Return Type: Array
# Description: returns list of Origins 
###########################################

function getOriginOffice()
{
$sql="SELECT o.originId,o.officeName,o.contact,o.address,CONCAT( u.firstname,  ' ', u.middleInitial,  ' ', u.surname ) AS contactPerson,o.ownerOffice FROM tblOriginOffice o LEFT JOIN tblUserAccount u ON o.originId = u.agencyCode ORDER BY o.officeName ASC";

$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

###########################################
# Function:    addDocument
# Parameters:  formFields/value
# Return Type: Boolean
# Description: returns  
###########################################

function addDocument($arFields)
{
$SQL="INSERT INTO tblDocumentType (documentTypeAbbrev ,documentTypeDesc , retentionPeriod ) 
      VALUES('".$arFields['docTypeAbbrv']."' ,'".$arFields['docTypeDesc']."','".$arFields['retentionPeriod']."')"; 
$sql1= new MySQLHandler();
$sql1->init();	
if($rs=$sql1->Insert($SQL))
	{
		$this->msg = MSG_ADD_SUCCESS;
		return 1;
	}
	else
	{
		$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'</font>';
		return 0;
	}
}


}
?>