<?php 
@session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

include_once("DocDetail.class.php");
include_once("Records.class.php");
include_once("Report.class.php");
include_once("General.class.php");

$objGen = new General;
$objDocDetail = new DocDetail;
$objRecordDetail = new Records;
$objReport = new Report;

$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
$dateFrom = new Date($_GET['dateFrom']);
$dateTo = new Date($_GET['dateTo']);

$ReportType = $objReport->getReportTypeById($_GET['docType']);
$rsRecordDetail = $objRecordDetail->getToSummary($_GET['dateFrom'], $_GET['dateTo'], $_GET['docType'], $_SESSION['office']);
?>

<style type="text/css">
table {
    border-collapse: collapse;
}

th, td {
    border: 1px solid black;
    padding: 7px;
    word-wrap: break-word;
    vertical-align: top;
}

th {
	text-align: center;
	background-color: #ddd;
}

.content {
	padding: 30px;
}
</style>
<div class="content">
	DOST Central Office
	<br><?=$officeName?>

	<br><br>
	<div align="center">
		<b>MASTER LIST OF RECORDS REGISTRY<?php #$ReportType?></b>
		<br>(<?=$dateFrom->getAsStr("d M Y")?> to <?=$dateTo->getAsStr("d M Y")?>)
	</div>
	<br><br>

	<table border="1">
		<tr>
			<th>ITEM NO.</th>
			<th>DOC&nbsp;ID</th>
			<th>DOCNUM</th>
			<th>DOCTYPE</th>
			<th>RETENTION PERIOD</th>
			<th>SUBJECT</th>
			<th>LOCATION</th>
		</tr>
		<?php
			for ($i=0; $i < count($rsRecordDetail); $i++) { 
				$doctype = $objGen->getDocTypeByDocId($rsRecordDetail[$i]["documentId"]);
				$location = $objGen->getLocation($rsRecordDetail[$i]["documentId"]);
				if($location)
					$locname = $location['label1'].' > '.$location['label2'].' > '.$location['label3'];
				else
					$locname = '';
					
				// echo '<tr><td>'.($i+1).'</td><td></td><td>'.$rsRecordDetail[$i]["docNum"].'</td><td style="width: 550px">'.$rsRecordDetail[$i]["documentId"].'</td><td><img style="width: 120px;" src="http://10.10.10.54/erms/class/html2pdf/barcode.php?text='.$rsRecordDetail[$i]["documentId"].'"></td></tr>';
				echo '<tr><td>'.($i+1).'</td><td>'.$rsRecordDetail[$i]["documentId"].'</td><td>'.$rsRecordDetail[$i]["docNum"].'</td><td>'.$doctype['documentTypeAbbrev'].'</td><td>'.$doctype['retentionPeriod'].'</td><td style="width: 300px">'.$rsRecordDetail[$i]["subject"].'</td><td style="width: 50px">'.$locname.'</td></tr>';
			}
		 ?>

	</table>
</div>