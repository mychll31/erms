<?php
include_once("Date.class.php");
include_once("General.class.php");
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php");

class Tracking extends Date
{
	public function GetDocStatus($documentID)
	{
		$SQL = "SELECT tblDocument.confidential, tblHistory.dateAdded, tblHistory.documentID, tblHistory.dateSent, tblActionTaken.actionDesc as actionTaken, tblActionRequired.actionDesc as actionRequired, tblHistory.senderId, tblHistory.recipientId, tblHistory.recipientUnit, tblHistory.senderId, tblHistory.senderUnit
				FROM  `tblHistory`
				left join tblDocument on tblDocument.documentID = tblHistory.documentID
				left join tblActionTaken on tblActionTaken.actionCodeId = tblHistory.actionTakenCodeID
				left join tblActionRequired on tblActionRequired.actionCodeId = tblHistory.actionCodeId
				WHERE  tblHistory.documentId LIKE  '".$documentID."'
				ORDER BY  `tblHistory`.`historyId` DESC";

		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);

		if($rs[0]['confidential'] == 1):
			return false;
		else:
			return $rs;
		endif;
	}

	public function GetOfficeName($officeCode)
	{
		$SQL = "select tblGroup.groupname from tblGroup where groupcode = '".$officeCode."'
				union
				select tblGroup1.group1name from tblGroup1 where group1code = '".$officeCode."'
				union
				select tblGroup2.group2name from tblGroup2 where group2code = '".$officeCode."'
				union
				select tblGroup3.group3name from tblGroup3 where group3code = '".$officeCode."'
				union
				select tblGroup4.group4name from tblGroup4 where group4code = '".$officeCode."'
				union
				select tblGroup5.group5name from tblGroup5 where group5code = '".$officeCode."'";

		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;	
	}

	public function GetEmpName($empNumber)
	{
		$SQL = "SELECT firstname, surname FROM `tblEmpPersonal` WHERE `empNumber` LIKE '".$empNumber."'";

		$sql1= new MySQLHandler2();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;	
	}
	
}
?>