<?php
// session_start();
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php");
include_once("Records.class.php");
class Search {

	function doSearch($t_strText,$searchBy='',$searchyear='',$type='',$status='', $dtmDate='',$dateFrom='',$dateTo='',$page="", $limit="", $order="", $ascdesc="")
	{
	if($t_strText<>"")
	{
		
	$objRecords = new Records;
	
	if($searchBy == "remarks")//if remarks
		{$table = "tblHistory";$field=$t_strText;$searchBy=='remarks';}
	else if($searchBy == "recipientId")//if recipientId
		{$table = "tblHistory";$field=$this->getDestinationId($t_strText);}
	else if($searchBy == "sender")//if sender
		{$table = "tblDocument";$field=$t_strText;}
	else if($searchBy == "originId")//if originId
		{$table = "tblDocument";$field=$objRecords->getOfficeId($t_strText);}
	else if($searchBy == "documentId")
		{$table = "tblDocument";$field=trim($t_strText);$searchBy='documentId';}		
	else if($searchBy == "subject")
		{$table = "tblDocument";$field=$t_strText;$searchBy=='subject';}
	else
		{$table = "tblDocument";$field=$t_strText;$searchBy='';}
	
	//for searching documents by year
	if($searchyear<>""){$searchyear = " AND tblDocument.dateAdded LIKE '".$searchyear."%'";}

	//for searching documents by document type
	if($type<>""){$t_strWhereType = " AND tblDocument.documentTypeId=$type ";}

	//for searching documents by status
	if($status<>"")
		{
		if($objRecords->get("userType")==3) //employee
			$t_strWhereStatus = " AND (tblDocument.status=2) ";
		else
			$t_strWhereStatus = " AND tblDocument.status=$status ";
		}
	else
		if($objRecords->get("userType")==3) //employee
			$t_strWhereStatus = " AND (tblDocument.status=2) ";
	
	//for searching documents in a given duration date
	if($dateFrom<>"" && $dateTo <> "")	{ $t_strWhereDate = " AND tblDocument.$dtmDate>='$dateFrom' AND tblDocument.$dtmDate<='$dateTo' ";}
	
	//for searching documents for specific office
	//$t_strWhereOffice = " AND (tblDocument.addedByOfficeId = '".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."'  OR tblHistory.recipientId='".$objRecords->get('userID')."' OR tblHistory.recipientId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."'  OR tblHistory.senderId= '".$objRecords->get('userID'). "') ";
	
	if($_SESSION['blnAgencyUser'])
	{
		$t_strWhereStatus = "";
		$t_strWhereOffice = " AND (tblDocument.addedById = '".$objRecords->get('userID')."'  OR tblHistory.recipientId='".$objRecords->get('userID')."' OR tblHistory.recipientId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."'  OR tblHistory.senderId= '".$objRecords->get('userID')."') ";
	}
	else
	{
		$t_strWhereOffice = " AND (tblDocument.addedByOfficeId = '".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."'  OR tblHistory.recipientId='".$objRecords->get('userID')."' OR tblHistory.recipientId='".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'])."'  OR tblHistory.senderId= '".$objRecords->get('userID')."') ";
	
		if($searchBy == "recipientId")//if destination
			$t_strWhereOffice = " AND (tblDocument.addedByOfficeId = '".($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']) ."'  OR tblHistory.senderId= '".$objRecords->get('userID')."') ";		

	}


	//for restricted files
	//if($objRecords->get("userType")==3)
	//	$t_strWhereRestricted = "AND tblHistory.restricted<>1";
	
	//echo "searchBy=".$searchBy;
	if($searchBy=="")
		$t_strWhereSearchBy = "$table.subject LIKE '%".$field."%' OR $table.documentId LIKE '%".$field."%'";
	else {
		if($searchBy == "remarks")//if remarks
			$t_strWhereSearchBy = "$table.$searchBy LIKE '% ".$field."%' ";	
		if($searchBy == "recipientId")//if remarks			
			$t_strWhereSearchBy = "$table.$searchBy = '".$field."'";
		else
			$t_strWhereSearchBy = "$table.$searchBy LIKE '%".$field."%' ";
	}
	$sql = "SELECT tblDocument.*, tblHistory.senderId, tblHistory.historyId FROM tblDocument LEFT JOIN tblHistory ON tblDocument.documentId=tblHistory.documentId WHERE ($t_strWhereSearchBy)  $t_strWhereType $t_strWhereStatus $t_strWhereOffice $t_strWhereDate $t_strWhereRestricted $searchyear AND tblDocument.isDelete=0 GROUP BY tblDocument.documentId ";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	//echo $field;
	// if(strlen($order)>0)
	// 	$sql = $sql . " ORDER BY $order $ascdesc"; 
	$sql = $sql . " ORDER BY tblDocument.documentDate DESC";
	if($page==0||$page=="")
		$page = 1;
	$offset = ($page - 1) * $limit;
	if(strlen($page)>0 && strlen($limit)>0)
		$sql = $sql . " LIMIT $offset, $limit";		

	$rs=$sql1->Select($sql);	
	return $rs;
	}
	
	else
		return "<br>Please input a search string";
	
	
	}//end function
	
	function getDestinationId($t_strText)
	{
	$t_strEmp = $this->getEmployeeId($t_strText);
	$t_strOff = $this->getOfficeId($t_strText);
	$t_strOrigOff = $this->getOriginOfficeId($t_strText);
	if(count($t_strEmp))
		return $t_strEmp;
	else if(count($t_strOff))
		return $t_strOff;
	else if(count($t_strOrigOff))
		return $t_strOrigOff;
	else
		return 0;
	}
	
	function getOriginOfficeId($t_strText)
	{
	$sql = "SELECT originId FROM tblOriginOffice WHERE officeName LIKE '%$t_strText%'";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return $rs[0]['originId'];
	}
	function getEmployeeId($t_strText)
	{
	$sql = "SELECT empNumber FROM tblEmpPersonal WHERE firstname LIKE '%$t_strText%' OR surname LIKE '%$t_strText%' OR empNumber = '$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return $rs[0]['empNumber'];
	}	
	function getOfficeId($t_strText)
	{
	if(count($this->checkGroup5($t_strText)))
		{$field='group5Code';$table='tblGroup5';}
	else if(count($this->checkSection($t_strText)))
		{$field='group4Code';$table='tblGroup4';}
	else if(count($this->checkDivision($t_strText)))
		{$field='group3Code';$table='tblGroup3';}
	else if(count($this->checkService($t_strText)))
		{$field='group2Code';$table='tblGroup2';}
	else if(count($this->checkOffice($t_strText)))
		{$field='group1Code';$table='tblGroup1';}
	else
		{
		//echo $field."/".$table;
		}
	
	$sql = "SELECT $field FROM $table WHERE $field='$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	if($field<>"" && $table <> "")
		$rs=$sql1->Select($sql);
	if(count($rs))
		return $rs[0][$field];		
	}
	
	function checkOffice($t_strText)
	{
	$sql = "SELECT group1Code FROM tblGroup1 WHERE group1Name LIKE '%$t_strText%' OR group1Code = '$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return 'group1Code';
	}
	function checkService($t_strText)
	{
	$sql = "SELECT group2Code FROM tblGroup2 WHERE group2Name LIKE '%$t_strText%' OR group2Code='$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return 'group2Code';
	}
	function checkDivision($t_strText)
	{
	$sql = "SELECT group3Code FROM tblGroup3 WHERE group3Name LIKE '%$t_strText%' OR group3Code = '$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return 'group3Code';
	}			
	function checkSection($t_strText)
	{
	$sql = "SELECT group4Code FROM tblGroup4 WHERE group4Name LIKE '%$t_strText%' OR group4Code='$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return 'group4Code';
	}	
	function checkGroup5($t_strText)
	{
	$sql = "SELECT group5Code FROM tblGroup5 WHERE group5Name LIKE '%$t_strText%' OR group5Code='$t_strText'";
	$sql1= new MySQLHandler2();
	$sql1->init();
	$rs=$sql1->Select($sql);
	if(count($rs))
		return 'group5Code';
	}	
}
?>