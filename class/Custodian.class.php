<? 
session_start();
include_once("General.class.php");
class Custodian extends General {

function getCustDocList($t_strOffice, $t_strGroup, $t_strSearch="", $t_strStatus="", $t_strGroupFilter="", $page="", $limit="", $order="", $ascdesc="",$cnt="",$t_strDocType="",$t_strOperator='')
	{
		/*$sql = "SELECT DISTINCT tblDocument.* FROM tblDocument 
		LEFT JOIN tblTrash ON tblDocument.documentId=tblTrash.documentId 
		WHERE addedByOfficeId = '".$t_strOffice."'";*/
		if($t_strDocType!=""){
			$strDocType=" AND tblDocument.documentTypeId='$t_strDocType'";
		}
		if($t_strSearch!="")
		{
			$strSearch .= " AND ";
			switch($t_strOperator){
				case 'OR': $arrSearch = explode('OR',$t_strSearch);
						   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
					   					 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
						   break;
				case 'EXCEPT': break;				
				case 'AND': $arrSearch = explode(' ',$t_strSearch);	
							$strSearch .= (count($arrSearch)>1 && $arrSearch[1]!='')?
									  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
									  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
									  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
							 break;				
				case 'DOCTYPE': 
					$arrSearch = explode(' ',$t_strSearch);
					$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
					$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
								break;								
				case 'CONTAINS': 
					$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
					$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
					break;				
			}
		}
			
		if($t_strStatus<>"")	
			$strStatus = " AND tblDocument.status=".$t_strStatus;		
		

		if($t_strGroupFilter<>"")	
			$strFilter = " AND tblDocument.addedByOfficeId = '".$t_strGroupFilter."'";
		else
		{	
			if(sizeof($t_strGroup)>0 && $t_strGroup[0]['groupCode']<>"")
				for($i=0;$i<sizeof($t_strGroup);$i++)
					$strUnder = $strUnder." AND tblDocument.addedByOfficeId = '".$t_strGroup[$i]['groupCode']."' ";
			else
				$strUnder = "AND tblDocument.addedByOfficeId IN ('".$t_strOffice."'".$this->getDocsUnder($this->get("office")).")";	
		}		
		
		//if($_SERVER["REMOTE_ADDR"]=='202.90.141.59')
			//echo $strUnder."<br><br>";
		if($cnt=='1')
			{	
			$sql = "SELECT count(DISTINCT tblDocument.documentId) as _total FROM tblDocument
				LEFT JOIN tblHistory ON tblDocument.documentId = tblHistory.documentId 
				LEFT JOIN tblFIle ON tblDocument.documentId=tblFIle.documentId
				WHERE isDelete=0 $strSearch $strUnder $strStatus $strFilter $strDocType";
			}
		else
			{
			$sql = "SELECT DISTINCT tblDocument.documentId,tblDocument.*, tblFIle.path, tblFIle.filename as attachment FROM tblDocument
				LEFT JOIN tblHistory ON tblDocument.documentId = tblHistory.documentId 
				LEFT JOIN tblFIle ON tblDocument.documentId=tblFIle.documentId
				WHERE isDelete=0 $strSearch $strUnder $strStatus $strFilter $strDocType";			
			}		
		if(strlen($order)>0)
			$sql = $sql . " ORDER BY $order $ascdesc";
		else
			$sql = $sql . " ORDER BY tblDocument.dateAdded DESC, tblDocument.documentId ASC";		
		if($page==0||$page=="")
			$page = 1;
		$offset = ($page - 1) * $limit;
		if(strlen($page)>0 && strlen($limit)>0)
			$sql = $sql . " LIMIT $offset, $limit";		

		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		//if($_SERVER["REMOTE_ADDR"]=='202.90.141.59')
			//echo $sql."<br><br>";
		return $rs;
	}	



function showComboFolders($t_strName){
	
	$cmb =  '
	<select name="'.$t_strName.'" id="'.$t_strName.'">
		<option value="" selected>-Select Folder-</option>';
	$sql = "SELECT * FROM `tblFolders` WHERE userID = '".$this->get("userID")."'";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);		
	for($t=0;$t<count($rs);$t++) 
	{
	$cmb = $cmb . '<option value="'.$rs[$t]['folderID'].'">'.$rs[$t]['folderName'].'</option>';
	}
	$cmb = $cmb . '</select>';
	return $cmb;
}

}//end Class
?>