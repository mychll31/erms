<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentCompliance extends FPDF
{	
	var $ReportType;
	function generateReport()
	{

		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		
		$sdate = $_GET['dateFrom'];
		$edate = $_GET['dateTo'];

		$datesArr = array();
		while (strtotime($sdate) <= strtotime($edate)) {
                array_push($datesArr, $sdate);
                $sdate = date ("Y-m-d", strtotime("+1 day", strtotime($sdate)));
		}

		// begin getting records
		$this->SetWidths(array(10,35,50,30,35,35,30,40));
		$this->SetAligns(array('C','L','L','C','L','L','L','L'));
		$this->SetFont('Arial','',9);
		$ctr = 1;
		foreach($datesArr as $ddate):
			$d = explode('-', $ddate);
			$records = $objDocDetail->getComplianceDocs($d[0],$d[1],$d[2]);
			// print_r($records);
			foreach($records as $rec):
				$this->Row(array($ctr++,
								$rec['documentId'],
								$rec['subject'],
								$rec['documentTypeAbbrev'],
								$rec['actionRequired'],
								$rec['actionTaken'],
								$rec['remarks'],
								date('m-d-Y', strtotime($ddate. ' + 15 days'))),1);
			endforeach;
		endforeach;
		// end getting records
		// die();
		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.date('F d, Y',strtotime($_GET['dateFrom'])).' to '.date('F d, Y',strtotime($_GET['dateTo'])).')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','35','50','30','35','35','30','40');
		$cell_caption = array(" ","DOC ID","SUBJECT","DOC TYPE","ACTION REQUIRED","ACTION TAKEN","DATE","DATE OF EFFECTIVITY");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'BRL' : 'TRL',$key == 9 ? 1 : 0,"C",1);
		endforeach;
		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}


}
?>
