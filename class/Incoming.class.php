<? @session_start();
include_once("MySQLHandler.class.php");
include_once("MySQLHandler2.class.php");
include_once("General.class.php");

class Incoming extends General
{


###########################################
# Function:    getNewId
# Parameters:  N/A
# Return Type: String
# Description: returns newly generated id
###########################################
function getNewId($mode,$year=0)
{ 

if($year!=2000):
	$year = $year==0?0:date("y", mktime(0,0,0,1,1,$year));
endif;

$strIdConstructor = $this->idConstructor($mode,true,$year);
$sql="SELECT * FROM tblDocument WHERE documentId LIKE '$strIdConstructor%' ORDER BY documentId DESC";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
$sql1->init();
if(sizeof($rs)==0){ 
 $sql2= new MySQLHandler();
 $sql2->init(); 
 $strIdConstructor = $this->idConstructor($mode);
 $sql="SELECT * FROM tblDocument WHERE documentId LIKE '$strIdConstructor%' ORDER BY documentId DESC";
 $rs=$sql2->Select($sql);
}
$strIdConstructor = $this->idConstructor($mode,true,$year); //get new doc id format

if(sizeof($rs)==0)
{
 $strNewId = $strIdConstructor."-00001";
}
else
{
 $arID=explode("-",$rs[0]['documentId']);
 $index=sizeof($arID)-1;
 $intNextNum = (int)$arID[$index]+1;
 $strNewId =$strIdConstructor."-".str_pad($intNextNum, 5, "0", STR_PAD_LEFT);
}
return $strNewId;
}

###########################################
# Function:    idConstructor
# Parameters:  N/A
# Return Type: String
# Description: returns office-yr pattern  
#			   for creating new document id.
#			   pattern should b same with
#			   user's office and current yr
###########################################

function idConstructor($mode,$bool_newdocidformat=false,$year=0)
{
$status=(($mode=="outg")?"OUTG-":(($mode=="intra")?"INTRA-":"INC-"));
if($bool_newdocidformat) $status=""; //this line is for new docid format 
if($_REQUEST["getOfficeID"]=="")
{
 $rsManagedOffice=$this->getManagedOffice($this->get("empNum"));
 $officeCode=($rsManagedOffice[0]["groupCode"]=="") ? $rsManagedOffice[0]["officeCode"]:$rsManagedOffice[0]["groupCode"];
}
else{
 $officeCode=$_REQUEST["getOfficeID"];
}
//$officeCode=$this->get("office");
if(AGENCY_CODE=="") {$str_aname="";}
else $str_aname=AGENCY_CODE."-";
if($bool_newdocidformat) $str_aname=""; //this line is for new docid format 

// added for encoding of old documents. used to change year in document ID
	if($year!=2000):
		$year = $year==0?date("y"):$year; 
	else:
		$year = "00";
	endif;
$officeCode = ($this->get("blnAgencyUser"))?trim($this->getOfficeCode2($this->get('userID'))):$officeCode;
return $str_aname.$officeCode."-".$status.$year;
}


###########################################
# Function:    getDocType
# Parameters:  N/A
# Return Type: Array
# Description: returns list of document types 
###########################################

function getDocType($Id)
{
if ($Id=="") $sql="SELECT * FROM tblDocumentType ORDER BY documentTypeDesc ASC";
else $sql="SELECT * FROM tblDocumentType WHERE documentTypeId='$Id'";
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

###########################################
# Function:    getDocument
# Parameters:  N/A
# Return Type: Array
# Description: returns list of document
###########################################

function getDocument($Id)
{
if ($Id=="") $sql="SELECT * FROM tblDocument ORDER BY dateAdded ASC";
else $sql="SELECT * FROM tblDocument WHERE documentId='$Id'";
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");	
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}


###########################################
# Function:    getOriginOffice
# Parameters:  N/A
# Return Type: Array
# Description: returns list of Origins 
###########################################

function getOriginOffice($id='')
{
$where = $id==""?"":"o.originId='$id' AND";
$sql="SELECT o.originId,o.officeName,o.contact,o.address,CONCAT( u.firstname,  ' ', u.middleInitial,  ' ', u.surname ) AS contactPerson,o.ownerOffice FROM tblOriginOffice o LEFT JOIN tblUserAccount u ON o.originId = u.agencyCode WHERE $where o.ownerOffice='".$this->get("office")."' ORDER BY o.officeName ASC";

$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

# BEGIN function need for AReceipt
function getOriginOfficeById($id='')
{
	$sql="SELECT * FROM `tblOriginOffice` WHERE originId=".$id;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);
	return $rs;
}
# END function need for AReceipt

###########################################
# Function:    addDocument
# Parameters:  formFields/value
# Return Type: Boolean
# Description: returns insert result
###########################################

function addDocument($arFields,$oldDocument=0)
{

if(trim($arFields['t_strDocType'])=="") $docTypeId="-1";
else
{
	$rsDocDetail=$this->getDocTypeFromAbbrev($arFields['t_strDocType']);
	if( $rsDocDetail[0]['documentTypeId']=="") 
		{
		$this->msg = '<font color="red">Invalid Document Type</font>';
		return 0;
		}
	else $docTypeId=$rsDocDetail[0]['documentTypeId'];

}
$docTypeId=$arFields["cmbDocType"];//addded for combo document type
// start of validation
		if(!$this->checkValidId($arFields['t_strDocId'],'inc'))
		{
		$this->msg = '<font color="red">Invalid Document ID</font>';
		return 0;
		}

		if(!$this->check_valid_date($arFields['documentDate']))
		{
		$this->msg = '<font color="red">Invalid Document Date</font>';
		return 0;
		}
		
		if(!$this->check_valid_date($arFields['deadline']))
		{
		$this->msg = '<font color="red">Invalid Date of Deadline</font>';
		return 0;
		}
		
		if(!$this->check_valid_date($arFields['dateReceived']))
		{
		$this->msg = '<font color="red">Invalid Date Received</font>';
		return 0;
		}
		if($this->hasInvalidChar($arFields['t_strDocNum'],"!$%^*+={}\\|`~"))
		{
		$this->msg = '<font color="red">Invalid Document Number</font>';
		return 0;
		}
		
// end of validation
$added_date= $oldDocument==1?$arFields['cmbDocYear']."-01-01 00:00:00":date("Y-m-d H:i:s");
$confidential=($arFields['t_intConfidential']==""?0:1);
if($arFields['actionUnitSwitch']==2) 
{
$originUnit="office";
$origin=$arFields['cmbOfficeCode'];
$groupCode=$arFields['cmbGroupCode'];
}
else if ($arFields['actionUnitSwitch']==3) 
{
$originUnit="agency";
$origin=$arFields['cmbAgency'];
$groupCode="";
}

/**
if($_SESSION['office']!='ITD')
	$arFields['t_contdoc'] = '0';

//if else edit by emma
if(isset($arFields['t_intContainer2']))
{
	$SQL="INSERT INTO tblDocument (documentId, documentTypeId,referenceId, docNum, subject, documentDate, originId,originGroupId,originUnit, sender, deadline, remarks, fileContainer, dateReceived, confidential,addedById,addedByOfficeId,dateAdded,status,fileCOntainer2,fileContainer3,controlleddoc) VALUES('".$arFields['t_strDocId']."','".$docTypeId."','".$arFields['t_strReferenceId']."','".$arFields['t_strDocNum']."','".addslashes($arFields['t_strSubject'])."','".$arFields['documentDate']."','".$origin."','".$groupCode."','".$originUnit."','".$arFields['t_strSender']."','".$arFields['deadline']."','".addslashes($arFields['t_strRemarks'])."','".$arFields['t_intContainer']."','".$arFields['dateReceived']."','".$confidential."','".$this->get('userID')."','".$arFields['documentOwner']."','".$added_date."','0','".$arFields['t_intContainer2']."','".$arFields['t_intContainer3']."',".$arFields['t_contdoc'].")"; //0 for incoming

}
else
{
	$SQL="INSERT INTO tblDocument (documentId, documentTypeId,referenceId, docNum, subject, documentDate, originId,originGroupId,originUnit, sender, deadline, remarks, fileContainer, dateReceived, confidential,addedById,addedByOfficeId,dateAdded,status,controlleddoc) VALUES('".$arFields['t_strDocId']."','".$docTypeId."','".$arFields['t_strReferenceId']."','".$arFields['t_strDocNum']."','".addslashes($arFields['t_strSubject'])."','".$arFields['documentDate']."','".$origin."','".$groupCode."','".$originUnit."','".$arFields['t_strSender']."','".$arFields['deadline']."','".addslashes($arFields['t_strRemarks'])."','".$arFields['t_intContainer']."','".$arFields['dateReceived']."','".$confidential."','".$this->get('userID')."','".$arFields['documentOwner']."','".$added_date."','0',".$arFields['t_contdoc'].")"; //0 for incoming

}
**/

if(isset($arFields['t_intContainer2']))
{
	$sql_location="INSERT INTO tblLocation (documentId, cabinetId, drawerId, containerId, addedby_unit, addedby_office, lastupdate_by, lastupdated_date)
					VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_intContainer']."', '".$arFields['t_intContainer2']."', '".$arFields['t_intContainer3']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['empNum']."', NOW( ))";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Insert($sql_location);
}

if(isset($arFields['t_contdoc']))
{
	$sql_insertCDoc="INSERT INTO tblControlledDocument (documentId, controlleddoc, copyno, copyholder, mannerOfDisposal, revisionno, personUnitResponsible, withrawalno, addedby_unit, addedby_office, lastupdated_by, lastupdated_date)
					VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_contdoc']."', '".$arFields['txt_conta_copyno']."', '".$arFields['txt_contb_copyholder']."', '".$arFields['txt_contc_mannerdisposal']."', '".$arFields['txt_contd_revisionno']."', '".$arFields['txt_conte_unitres']."', '".$arFields['txt_withrawal']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Insert($sql_insertCDoc);
}

$SQL="INSERT INTO tblDocument (documentId, documentTypeId, referenceId, docNum, subject, documentDate,
								originId, originGroupId, originUnit, sender, deadline, remarks,
								dateReceived, confidential, addedById, addedByOfficeId, dateAdded, status)
				VALUES ('".$arFields['t_strDocId']."', '".$docTypeId."', '".$arFields['t_strReferenceId']."', '".$arFields['t_strDocNum']."', '".addslashes($arFields['t_strSubject'])."', '".$arFields['documentDate']."',
						'".$origin."', '".$groupCode."', '".$originUnit."','".$arFields['t_strSender']."', '".$arFields['deadline']."', '".addslashes($arFields['t_strRemarks'])."',
						'".$arFields['dateReceived']."', '".$confidential."', '".$this->get('userID')."', '".$arFields['documentOwner']."', '".$added_date."', 0)";

$sql1= new MySQLHandler();
$sql1->init();	
//added to enable proper displaying of � 
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");
if($rs=$sql1->Insert($SQL))
	{
		$sql1->changelog($SQL,"INSERT","INCOMING","added incoming document with document number ".$arFields['t_strDocId']);
		$this->msg = MSG_ADD_SUCCESS;
		return 1;
	}
	else
	{
		$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'<br>'.$sql1->ERROR_MSG.'</font>';
		return 0;
	}
}

###########################################
# Function:    	updateDocument
# Parameters:  	formFields/value
# Return Type: 	Boolean
# Description: 	returns 1 for succesfull update
#				0 for unsuccesfull update
#				sets msg for message
###########################################


function updateDocument($arFields)
{

if(trim($arFields['t_strDocType'])=="") $docTypeId="-1";
else
{
	$rsDocDetail=$this->getDocTypeFromAbbrev($arFields['t_strDocType']);
	if( $rsDocDetail[0]['documentTypeId']=="") 
		{
		$this->msg = '<font color="red">Invalid Document Type</font>';
		return 0;
		}
	else $docTypeId=$rsDocDetail[0]['documentTypeId'];

}
$docTypeId=$arFields["cmbDocType"]; //addded for combo document type
// start of validation

		if(!$this->check_valid_date($arFields['documentDate']))
		{
		$this->msg = '<font color="red">Invalid Document Date</font>';
		return 0;
		}
		
		if(!$this->check_valid_date($arFields['deadline']))
		{
		$this->msg = '<font color="red">Invalid Deadline</font>';
		return 0;
		}
		
		if(!$this->check_valid_date($arFields['dateReceived']))
		{
		$this->msg = '<font color="red">Invalid Date Received</font>';
		return 0;
		}
		if($this->hasInvalidChar($arFields['t_strDocNum'],"!$%^*+={}\\|`~"))
		{
		$this->msg = '<font color="red">Invalid Document Number</font>';
		return 0;
		}
//end of validation


$confidential=($arFields['t_intConfidential']==""?0:1);
if($arFields['actionUnitSwitch']==2) 
{
$originUnit="office";
$origin=$arFields['cmbOfficeCode'];
$groupCode=$arFields['cmbGroupCode'];
}
else if ($arFields['actionUnitSwitch']==3) 
{
$originUnit="agency";
$origin=$arFields['cmbAgency'];
$groupCode="";
}

/**
if($_SESSION['office']!='ITD')
	$arFields['t_contdoc'] = '0';

//echo $arFields['actionUnitSwitch'];
//COMMENTED:: $SQL="UPDATE tblDocument SET documentTypeId='".$docTypeId."', subject='".addslashes($arFields['t_strSubject'])."', documentDate='".$arFields['documentDate']."', originId='".$origin."',originGroupId='".$groupCode."',originUnit='".$originUnit."', sender='".$arFields['t_strSender']."', deadline='".$arFields['deadline']."', remarks='".addslashes($arFields['t_strRemarks'])."', fileContainer='".$arFields['t_intContainer']."',dateReceived='".$arFields['dateReceived']."',confidential='".$confidential."',docNum='".$arFields['t_strDocNum']."',referenceId='".$arFields['t_strReferenceId']."', addedByOfficeId='".$arFields['documentOwner']."' WHERE documentId = '".$arFields['t_strDocId']."'";

// Begin Container Label
$SQL="UPDATE tblDocument SET documentTypeId='".$docTypeId."', subject='".addslashes($arFields['t_strSubject'])."', documentDate='".$arFields['documentDate']."', originId='".$origin."',originGroupId='".$groupCode."',originUnit='".$originUnit."', sender='".$arFields['t_strSender']."', deadline='".$arFields['deadline']."', remarks='".addslashes($arFields['t_strRemarks'])."', fileContainer='".$arFields['t_intContainer']."',dateReceived='".$arFields['dateReceived']."',confidential='".$confidential."',docNum='".$arFields['t_strDocNum']."',referenceId='".$arFields['t_strReferenceId']."', addedByOfficeId='".$arFields['documentOwner']."' , fileContainer2 = '".$arFields['t_intContainer2']."', fileContainer3 = '".$arFields['t_intContainer3']."', controlleddoc = '".$arFields['t_contdoc']."' WHERE documentId = '".$arFields['t_strDocId']."'";
// END Container Label
**/

	# BEGIN UPDATE location if exists Update
	$sql_updateLocation="INSERT INTO tblLocation (documentId, cabinetId, drawerId, containerId, addedby_unit, addedby_office, lastupdate_by, lastupdated_date)
						VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_intContainer']."', '".$arFields['t_intContainer2']."', '".$arFields['t_intContainer3']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['empNum']."', NOW( ))
								ON DUPLICATE KEY UPDATE
								cabinetId = '".$arFields['t_intContainer']."', 
								drawerId = '".$arFields['t_intContainer2']."', 
								containerId = '".$arFields['t_intContainer3']."',
								lastupdate_by = '".$_SESSION['empNum']."',
								lastupdated_date = NOW() ";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs = $sql1->Insert($sql_updateLocation);
	$rs = $sql1->Update($sql_updateLocation);
	# END UPDATE location if exists Update

	/* BEGIN add controlled document if exists update */
	$sql_updateControlledDoc="INSERT INTO tblControlledDocument (documentId, controlleddoc, copyno, copyholder, mannerOfDisposal, revisionno, personUnitResponsible, withrawalno, addedby_unit, addedby_office, lastupdated_by, lastupdated_date)
					VALUES ('".$arFields['t_strDocId']."', '".$arFields['t_contdoc']."', '".$arFields['txt_conta_copyno']."', '".$arFields['txt_contb_copyholder']."', '".$arFields['txt_contc_mannerdisposal']."', '".$arFields['txt_contd_revisionno']."', '".$arFields['txt_conte_unitres']."', '".$arFields['txt_withrawal']."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))
								ON DUPLICATE KEY UPDATE
								controlleddoc = '".$arFields['t_contdoc']."', 
								copyno = '".$arFields['txt_conta_copyno']."', 
								copyholder = '".$arFields['txt_contb_copyholder']."',
								mannerOfDisposal = '".$arFields['txt_contc_mannerdisposal']."',
								revisionno = '".$arFields['txt_contd_revisionno']."',
								personUnitResponsible = '".$arFields['txt_conte_unitres']."',
								withrawalno = '".$arFields['txt_withrawal']."',
								lastupdated_by = '".$_SESSION['rs_empNumber']."',
								lastupdated_date = NOW( ) ";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs = $sql1->Insert($sql_updateControlledDoc);
	$rs = $sql1->Update($sql_updateControlledDoc);
	/* END add controlled document if exists update */

//echo $arFields['actionUnitSwitch'];
$SQL="UPDATE tblDocument SET documentTypeId='".$docTypeId."', subject='".addslashes($arFields['t_strSubject'])."', documentDate='".$arFields['documentDate']."', originId='".$origin."',originGroupId='".$groupCode."',originUnit='".$originUnit."', sender='".$arFields['t_strSender']."', deadline='".$arFields['deadline']."', remarks='".addslashes($arFields['t_strRemarks'])."',dateReceived='".$arFields['dateReceived']."',confidential='".$confidential."',docNum='".$arFields['t_strDocNum']."',referenceId='".$arFields['t_strReferenceId']."', addedByOfficeId='".$arFields['documentOwner']."' WHERE documentId = '".$arFields['t_strDocId']."'";

$sql1= new MySQLHandler();
$sql1->init();	
//added to enable proper displaying of � 
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");
if($rs=$sql1->Update($SQL))
{
	$this->msg = MSG_UPD_SUCCESS;
	$sql1->changelog($SQL,"UPDATE","INCOMING","updated metadata of document with document number ".$arFields['t_strDocId']);
	return 1;
}
else
{
	$this->msg = '<font color="red">'.MSG_UPD_UNSUCCESS.'</font>';
	if($rs===false){
	return 0;
	}
	else return 1;
	
}
}


###########################################
# Function:    getContainer
# Parameters:  N/A
# Return Type: Array
# Description: returns list of containers defending on the office 
###########################################

function getContainer($office,$Id)
{
if($Id=="") $sql="SELECT * FROM tblContainer WHERE officeCode='$office' ORDER BY label ASC";
else $sql="SELECT * FROM tblContainer WHERE containerId='$Id' AND officeCode='$office'";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;

}

// 2 functions added by Emma 
function getContainerTwo($containerId,$id="")
{
	if($id=="") $sql="SELECT * FROM tblContainer2 WHERE container=$containerId";
	else $sql="SELECT * FROM tblContainer2 WHERE container2Id=$id";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);
	return $rs;

}

function getContainerThree($container2Id,$id="")
	{
	if($id=="")	$sql="SELECT * FROM tblContainer3 WHERE container2=$container2Id";
	else $sql="SELECT * FROM tblContainer3 WHERE container3Id=$id";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);
	return $rs;

}


###########################################
# Function:    getRelatedDocs
# Parameters:  N/A
# Return Type: string
# Description: returns whole thread of related documents
###########################################
function getRelatedDocs($docId,$haystacks)
{
if($haystacks=="")
	{
	$haystacks=array();
	$haystacks[]=$docId;
	}
else
	{
	if(in_array($docId,$haystacks)) return NULL;
	else $haystacks[]=$docId;	
	}
$returnvar="";
$sql="SELECT documentId,referenceId FROM tblDocument WHERE documentId='$docId'";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
if(count($rs)>0)
{
//$returnvar=$rs[0]["documentId"];
if($rs[0]["referenceId"]!="") 
{
	$arrdoc=$this->getDocument($rs[0]["referenceId"]);
		if(count($arrdoc)>0)
		{
			$returnvar=array();
			$returnvar[0]=array();
			$returnvar[0]["documentId"]=$arrdoc[0]["documentId"];
			$returnvar[0]["referenceId"]=$arrdoc[0]["referenceId"];
			$returnvar[0]["status"]=$arrdoc[0]["status"];
			
			if(in_array($rs[0]["referenceId"],$haystacks)) return NULL;
			$relateddocs=$this->getRelatedDocs($rs[0]["referenceId"],$haystacks);
		}
}
$returnvar=array_merge($returnvar,(array)$relateddocs);
}
return $returnvar;
}


###########################################
# Function:    getOfficeDetails
# Parameters:  N/A
# Return Type: Array
# Description: returns details of the office 
###########################################

function getOfficeDetails($Id)
{
$sql1= new MySQLHandler2();
$sql1->init();
$sql="SELECT tblGroup1.group1Code as oCode, tblGroup1.group1Name as oName, tblGroup1.empNumber as empNumber, concat(tblEmpPersonal.surname,', ',tblEmpPersonal.firstname,' ',tblEmpPersonal.middleInitial) as oHead FROM tblGroup1 LEFT JOIN tblEmpPersonal ON tblGroup1.empNumber=tblEmpPersonal.empNumber WHERE tblGroup1.group1Code='$Id'";
//echo $sql;
$rs=$sql1->Select($sql);
if(sizeof($rs)==0)
{
$sql1= new MySQLHandler2();
$sql1->init();
$sql="SELECT tblGroup2.group2Code as oCode, tblGroup2.group2Name as oName, tblGroup2.empNumber as empNumber, concat(tblEmpPersonal.surname,', ',tblEmpPersonal.firstname,' ',tblEmpPersonal.middleInitial) as oHead FROM tblGroup2 LEFT JOIN tblEmpPersonal ON tblGroup2.empNumber=tblEmpPersonal.empNumber WHERE tblGroup2.group2Code='$Id'";
	//echo $sql;
$rs=$sql1->Select($sql);
	if(sizeof($rs)==0)
	{
	$sql1= new MySQLHandler2();
	$sql1->init();
	$sql="SELECT tblGroup3.group3Code as oCode, tblGroup3.group3Name as oName, tblGroup3.empNumber as empNumber, concat(tblEmpPersonal.surname,', ',tblEmpPersonal.firstname,' ',tblEmpPersonal.middleInitial) as oHead FROM tblGroup3 LEFT JOIN tblEmpPersonal ON tblGroup3.empNumber=tblEmpPersonal.empNumber WHERE tblGroup3.group3Code='$Id'";
		//echo $sql;
	$rs=$sql1->Select($sql);
		if(sizeof($rs)==0)
		{
		$sql1= new MySQLHandler2();
		$sql1->init();
		$sql="SELECT tblGroup4.group4Code as oCode, tblGroup4.group4Name as oName, tblGroup4.empNumber as empNumber, concat(tblEmpPersonal.surname,', ',tblEmpPersonal.firstname,' ',tblEmpPersonal.middleInitial) as oHead FROM tblGroup4  LEFT JOIN tblEmpPersonal ON tblGroup4.empNumber=tblEmpPersonal.empNumber WHERE tblGroup4.group4Code='$Id'";
		//echo $sql;
		$rs=$sql1->Select($sql);
			if(sizeof($rs)==0)
			{
			$sql1= new MySQLHandler2();
			$sql1->init();
			$sql="SELECT tblGroup5.group5Code as oCode, tblGroup5.group5Name as oName, tblGroup5.empNumber as empNumber, concat(tblEmpPersonal.surname,', ',tblEmpPersonal.firstname,' ',tblEmpPersonal.middleInitial) as oHead FROM tblGroup5  LEFT JOIN tblEmpPersonal ON tblGroup5.empNumber=tblEmpPersonal.empNumber WHERE tblGroup5.group5Code='$Id'";
			//echo $sql;
			$rs=$sql1->Select($sql);
			}
		}
	}
}
return $rs;
}

###########################################
# Function:    checkValidId
# Parameters:  id to check, mode (inc,outg)
# Return Type: 1/0
# Description: Checks for invalid id. returns 1 for valid  
#			   returns 0 for invalid. if id has the auto generate pattern
#			   which is agency-office-status-yr-XXXXX, this function checks only
#			   the XXXXX part of numberic chars only
###########################################


function checkValidId($id,$mode)
{
$strIdConstructor = $this->idConstructor($mode);
if(strstr($id,$strIdContructor)===true)		//accept if not in format agency-office-yr-num
	{
	return 1;
	}
else
	{
	$arID=explode("-",$id);
	$index=sizeof($arID)-1;
	//echo "s1".strlen($arID[$index]).$index;
		if(strlen($arID[$index])!=5)				//invalidates if num in format(agency-office-yr-num) has length !=5
			{
				return 0;
			}
		else
			{
			
			if($this->is_int_val($arID[$index])===true)
				{
				return 1;
				}
			else return 0;
			}
		
	}
}
###########################################
# Function:    is_int_val
# Parameters:  data to check
# Return Type: true/false
# Description: checks for numeric value either on int format or str format
#			   returns 1 for valid, returns 0 for invalid. 
#			   
###########################################

function is_int_val($data) 
{
if(strstr($data,"e")===false)
{
	if (is_int($data) === true) 
		return true;
	elseif (is_string($data) === true && is_numeric($data) === true) 
	{
		return (strpos($data, '.') === false);
	} 
	return false;
}
else return false;
}

###########################################
# Function:    check_valid_date
# Parameters:  date (string)
# Return Type: true/false
# Description: checks the date for the format yyyy-mm-dd or yyyy/mm/dd
#			   returns 1 for valid, returns 0 for invalid. 
#			   
###########################################


function check_valid_date($date)
{
if(strlen(trim($date))==0) return 1;
if($date=="0000-00-00") return 1;
if(strstr($date,"-")===false)
	{
		if(strstr($date,"/")===false) return 0;
		else
		{
		$arDate=explode("/",$date);
		}
	}
else
	{
	$arDate=explode("-",$date);
	}


if(sizeof($arDate)!=3) return 0;
if(checkdate($arDate[1],$arDate[2],$arDate[0]))
	  return true;
	else
	 return false;
}

###########################################
# Function:    hasInvalidChar
# Parameters:  haystack(string to search),limit(set of chars(str format) to find)
# Return Type: true/false
# Description: checks the haystack for the occurence of any char in the limit
#			   returns 1 if found, returns 0 if not found. 
#			   
###########################################
function hasInvalidChar($haystack,$limit)
{
if (strlen($haystack) != strcspn($haystack,$limit))  return 1;
else return 0;

}

function getDocTypeFromAbbrev($strAbbrev)
{
$sql="SELECT * FROM tblDocumentType WHERE documentTypeAbbrev='".trim($strAbbrev)."'";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

function getManagedOffice($id)
{
$sql="SELECT * FROM tblCustodian WHERE empNumber='".trim($id)."'";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}

function isCustodian($id,$officeid)
{
$sql="SELECT * FROM tblCustodian WHERE empNumber='".trim($id)."' AND (groupCode='$officeid' OR ( groupCode='' AND officeCode='$officeid')) ";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
if($this->get("blnAgencyUser")) return true;
if(count($rs)==0)  return false;
else return true;
}

function updateDocNo($t_arrPost,$module='incg'){
	$sender = ($module == 'outg')?", sender = '".$t_arrPost['signatory']."'":'';
	$SQL="UPDATE tblDocument SET docNum='".$t_arrPost['docNO']."', documentDate = '".$t_arrPost['docDate']."' $sender , subject = '".addslashes($t_arrPost['docSubj'])."' WHERE documentId = '".$t_arrPost['docID']."'";
	$sql1= new MySQLHandler();
	$sql1->init();	
	$rs=$sql1->Update($SQL);
	return $SQL;
}
//Additional for OriginRelatedRecord
function getRelatedDocsOrigin($docid)
{
$sql="select *, @pv:=documentId as documentId from tblDocument
		join
		(select @pv:='$docid')tmp
		where referenceId=@pv";
//echo $sql;
$sql1= new MySQLHandler();
$sql1->init();
$rs=$sql1->Select($sql);
return $rs;
}
	
	//Additional for OriginRelatedRecord
	function getOriginRelatedRecord($docid){
		
		$query = "select *, @pv:=documentId as documentId from tblDocument join (select @pv:='$docid')tmp where referenceId=@pv";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs;
	}

	function getRelatedDocsByRefId($refid){
		
		$query = "SELECT * FROM  `tblDocument` WHERE  `documentId` IN (".$refid.")";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs;
	}

	function getRelatedDocsByParentDoc($docid){
		
		$query = "SELECT *  FROM `tblDocument` WHERE `referenceId` LIKE '%".$docid."%'";
		$sql= new MySQLHandler();
		$sql->init();
		$rs=$sql->Select($query);
		return $rs[0];
	}
	
	function updateLocation($docid, $cont1, $cont2, $cont3){
		
		$sql_updateLocation="INSERT INTO tblLocation (documentId, cabinetId, drawerId, containerId, addedby_unit, addedby_office, lastupdate_by, lastupdated_date)
						VALUES ('".$docid."', '".$cont1."', '".$cont2."', '".$cont3."', '".$_SESSION['userUnit']."', '".$_SESSION['office']."', '".$_SESSION['rs_empNumber']."', NOW( ))
								ON DUPLICATE KEY UPDATE
								cabinetId = '".$cont1."', 
								drawerId = '".$cont2."', 
								containerId = '".$cont3."',
								lastupdate_by = '".$_SESSION['rs_empNumber']."',
								lastupdated_date = NOW() ";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs = $sql1->Insert($sql_updateLocation);
		$rs = $sql1->Update($sql_updateLocation);
		return $rs;
	}
}
?>