<? 
@session_start();
include_once("General.class.php");
class Records extends General {

	function getDocList($t_strOffice, $t_strGroup="", $t_strSearch="", $t_strStatus="", $t_strGroupFilter="", $page="", $limit="", $order="", $ascdesc="",$cnt="",$t_strDocType,$t_strRecord,$t_strOperator='',$t_dtCmbYear='')
	{
		
		if($t_strRecord=="true" || $t_strRecord==""){
			$strRecord= " AND addedById='".$_SESSION['empNum']."' ";
		}
		if($t_strDocType!=""){
			$strDocType=" AND tblDocument.documentTypeId='$t_strDocType'";
		}
		if($t_dtCmbYear!=''){
			$strYear=" AND YEAR(tblDocument.dateAdded) = '$t_dtCmbYear' ";
		}
		if($t_strSearch!="")
		{
			$strSearch .= " AND ";
			switch($t_strOperator){
				case 'OR': $arrSearch = explode('OR',$t_strSearch);
						   $strSearch .= "(tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
					   					 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%')";
						   break;
				case 'EXCEPT': break;				
				case 'AND': $arrSearch = explode(' ',$t_strSearch);	
							$strSearch .= (count($arrSearch)>1)?
									  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
									  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%')":
									  " (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%') ";
							 break;				
				case 'DOCTYPE': 
					$arrSearch = explode(' ',$t_strSearch);
					$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
					$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
								break;								
				case 'CONTAINS': 
					$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
					$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%') "; 
					break;				
			}
		}
		
		/*	
			$arrSearch = explode(' ',$t_strSearch);
			if(count($arrSearch)>1)
			{
				$strSearch = " AND ( ";
				for($i=0;$i<sizeof($arrSearch);$i++)
				{
					$strSearch = $i>0?$strSearch." OR ":$strSearch;
					$strSearch .= " tblDocument.documentId LIKE '%".$arrSearch[$i]."%' OR tblDocument.subject LIKE '%".$arrSearch[$i]."%' OR tblDocument.docNum LIKE '%".$arrSearch[$i]."%' OR tblDocumentType.documentTypeAbbrev LIKE '%".$arrSearch[$i]."%' OR tblOriginOffice.officeName LIKE '%".$arrSearch[$i]."%'";
				}	
				$strSearch .= ")";
			}
			else
				$strSearch = " AND (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%' OR tblDocumentType.documentTypeAbbrev LIKE '%".$t_strSearch."%' OR tblOriginOffice.officeName LIKE '%".$t_strSearch."%') ";
		}*/

		if($t_strStatus<>"")	
			$strStatus = " AND tblDocument.status=".$t_strStatus;
			
		//if(sizeof($t_strGroup)>1) 
			//for($i=0;$i<sizeof($t_strGroup);$i++)
				//$group = $group . " ,'".$t_strGroup[$i]['groupCode']."'";	
		//else
			//$group = " ,'".$t_strGroup[0]['groupCode']."'";	
		
		if($t_strGroupFilter<>"" && $t_strGroupFilter <> "All")	
			$strFilter = " AND tblDocument.addedByOfficeId = '".$t_strGroupFilter."'";
		else
			{			
			$strUnder = " AND tblDocument.addedByOfficeId IN ('".$t_strOffice."'$group".$this->getDocsUnder($this->get("office")).")";
			}
		if($cnt=='1')
			{	
			$sql = "SELECT count(tblDocument.documentId) as _total FROM tblDocument 
			LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId 
			LEFT JOIN tblOriginOffice ON tblDocument.originId = tblOriginOffice.originId
			WHERE 1 $strFilter $strUnder $strYear AND tblDocument.isDelete=0 $strSearch $strStatus $strDocType $strRecord";
			}
		else
			{
			$sql = "SELECT DISTINCT tblDocument.documentId,tblDocument.documentTypeId,
		tblDocument.subject,tblDocument.docNum,tblDocument.documentDate,tblDocument.officeSig,tblDocument.originId,
		tblDocument.originUnit,tblDocument.status, tblDocument.dateAdded, tblDocument.deadline,
		tblDocumentType.documentTypeDesc, tblFIle.path, tblFIle.filename as attachment FROM tblDocument 
			LEFT JOIN tblDocumentType ON tblDocument.documentTypeId = tblDocumentType.documentTypeId 
			LEFT JOIN tblOriginOffice ON tblDocument.originId = tblOriginOffice.originId
			LEFT JOIN tblFIle ON tblDocument.documentId=tblFIle.documentId
			WHERE 1 $strFilter $strUnder $strYear AND tblDocument.isDelete=0 $strSearch $strStatus $strDocType $strRecord
			GROUP BY tblDocument.documentId";
		}
		$sql .= strlen($order)>0?" ORDER BY $order $ascdesc":" ORDER BY dateAdded DESC, documentId ASC";
		
		if($page==0||$page=="")
			$page = 1;
		$offset = ($page - 1) * $limit;
		if(strlen($page)>0 && strlen($limit)>0)
			$sql .= " LIMIT $offset, $limit";
			//echo $sql;
		$sql1=new MySQLHandler();		
		
		$sql1->init();
		$rs = $sql1->SELECT($sql);
		return $rs;	
	}
	
	function getDocListEncoded($txtUserID,$intStatus,$dateFrom, $dateTo)
	{
		$sql = "SELECT COUNT(documentId) AS _total FROM tblDocument 
			 WHERE addedById = '".$txtUserID."' AND status ='".$intStatus."' AND SUBSTR(dateAdded,1,10) >='".$dateFrom."' AND SUBSTR(dateAdded,1,10) <='".$dateTo."'";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	
	function getActionListEncoded($txtUserID,$dateFrom, $dateTo)
	{
		$sql = "SELECT DISTINCT tblDocument.* FROM tblDocument LEFT JOIN tblTrash ON tblDocument.documentId=tblTrash.documentId WHERE addedById = '".$txtUserID."' AND status ='".$intStatus."' AND dateAdded >='".$dateFrom."' AND dateAdded <='".$dateTo."'";
		//echo $sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;	
	}
	
	function checkHistoryActions($txtUserID,$dateFrom, $dateTo)
	{
	
		$SQL = "SELECT tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
		INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
		INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
		WHERE tblHistory.addedById='".$txtUserID."'  AND SUBSTR(dateAdded,1,10) >='".$dateFrom."' AND SUBSTR(dateAdded,1,10) <='".$dateTo."'";
		
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
			
		return $rs;
	}
	
	function getDocumentDetails($txtDocumentID)
	{
		$sql="SELECT * FROM tblDocument WHERE documentId='$txtDocumentID'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}
	
	function getOfficeDocumentHistory($strDocumentID, $strOfficeID)
	{
		
		if(strlen($strOfficeID)>0)
			$strWhere = " AND  tblDocument.addedByOfficeId = '".$strOfficeID."'";
			
		$SQL = "SELECT tblDocument.*, tblHistory.*, tblHistory.remarks as historyRemarks, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' $strWhere ORDER BY tblHistory.dateSent DESC";

		// echo $SQL;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getOfficeOtherDocumentHistory($strDocumentID, $strOfficeID)
	{
		$SQL = "SELECT tblDocument.*, tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.documentId='".$strDocumentID."' AND tblDocument.addedByOfficeId = '".$strOfficeID."' AND (tblHistory.actionCodeId!=10 AND tblHistory.actionCodeId!=1) ORDER BY tblHistory.dateSent DESC";
			// exclude histories in which action required are "FYI", "For 201 Update", etc.
		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function checkDocumentHistoryReply($arrDocumentHistoryID)
	{
	
		for($i=0;$i<count($arrDocumentHistoryID);$i++) 
		{
			
			if(count($this->getDocumentHistoryReply($arrDocumentHistoryID[$i]['historyId'])))
				return 0;
			else
				return 1;	// if no response yet
			
						
		} // for($ireply=0;$ireply<count($rsOfficeDocumentHistory);$ireply++) 	
		

	}
	
	function getDocumentHistoryReply($intHistoryId)
	{
		$SQL = "SELECT tblDocument.*, tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
			WHERE tblHistory.referenceId='".$intHistoryId."'";

		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getDocumentHistoryDetails($intHistoryId)
	{
		$SQL = "SELECT tblDocument.*, tblHistory.*, tblActionRequired.actionDesc as actionDesc, tblActionTaken.actionDesc as actionTakenDesc FROM tblHistory 
				INNER JOIN tblDocument ON tblDocument.documentId=tblHistory.documentId 
					INNER JOIN tblActionRequired ON tblHistory.actionCodeID=tblActionRequired.actionCodeID 
						INNER JOIN tblActionTaken ON tblHistory.actionTakenCodeID=tblActionTaken.actionCodeID 
							WHERE tblHistory.historyId='".$intHistoryId."'";

		//echo $sql1;
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getToSummary($date_from, $date_to, $doctype, $strOffice){
		// $SQL = "SELECT *, CAST( docNUm AS UNSIGNED ) AS docNumInt FROM `tblHistory`
		// 		left join tblDocument on tblDocument.documentId = tblHistory.documentId
		// 		where tblDocument.documentTypeId = ".$doctype." and recipientId = 'RMS'
		// 		and tblHistory.dateSent between '".$date_from."' and '".$date_to."'
		// 		group by tblHistory.documentId
		// 		order by docNumInt, tblHistory.documentId";
		if($strOffice != ''){
			$sqloffice = " and addedByOfficeId = '".$strOffice."'";
		}else{
			$sqloffice = "";
		}
		
		if($doctype == '263' or $doctype == '419'){
			$sqldocumentType = "and (documentTypeId = 263 or documentTypeId = 419)";
		}elseif($doctype == 'all'){
			$sqldocumentType = "";
		}
		else{
			$sqldocumentType = "documentTypeId = ".$doctype;
		}

		$SQL = "SELECT * from tblDocument
				where 1 ".$sqldocumentType.$sqloffice."
				and documentDate between '".$date_from."' and '".$date_to."'
				and isDelete = 0
				order by docNum+0, documentId, subject";

		// echo $SQL;
		// die();
		$sql1= new MySQLHandler();
		$sql1->init();	
	
		$rs=$sql1->Select($SQL);
		return $rs;
	}
	
	function getDocListRMS($t_strOffice, $status, $startdate, $enddate)
	{	
		$sql = "SELECT DISTINCT tblDocument.documentId, tblDocument.documentTypeId, tblDocument.subject, tblDocument.docNum, tblDocument.documentDate, tblDocument.officeSig, tblDocument.originId, tblDocument.originUnit, tblDocument.status, tblDocument.dateAdded, tblDocument.deadline
			FROM tblDocument
			WHERE tblDocument.isDelete =0
			AND tblDocument.status =$status AND tblDocument.dateAdded BETWEEN  '$startdate' AND  '$enddate'
			GROUP BY tblDocument.documentId
			ORDER BY dateAdded DESC , documentId ASC";

		$sql1=new MySQLHandler();				
		$sql1->init();
		$rs = $sql1->SELECT($sql);
		return $rs;	
	}
}//end Class
?>