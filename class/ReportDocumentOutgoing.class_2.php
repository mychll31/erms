<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentOutgoing extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
	
		
		//if employee
		if($_SESSION['userType']==3){
		 
		include_once("class/Custodian.class.php");
		$objRecordDetail = new Custodian;
		
		$rsCustodianOffice = $objRecordDetail->getOfficeEmployee($objRecordDetail->get('userID'));

		for($i=0;$i<sizeof($rsCustodianOffice);$i++)
		{
			if($rsCustodianOffice[$i]['groupCode']=="")
			{
			$t_strGroupUnder = $objRecordDetail->get('office').$objRecordDetail->getDocsUnder($objRecordDetail->get("office"));
			$t_strGroupUnder = str_replace("'","",$t_strGroupUnder);
			$rsGroupUnder[$i] = explode(",",$t_strGroupUnder);
			}
			else
			{
			$rsGroupUnder[$i] = $rsGroupUnder[$i].$rsCustodianOffice[$i]['groupCode'];	
			}
		}	
		
		$rsGroup = $objRecordDetail->getCustodianOffice($objRecordDetail->get("userID"));
		$rsRecordDetail = $objRecordDetail->getCustDocList(trim($objRecordDetail->getOfficeCode2($objRecordDetail->get('userID'))),$rsGroup,'','','','','','','ASC',0);	
 

		}else{

		include_once("Records.class.php");
		$objRecordDetail = new Records;
 		$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
		    
		}

		//$this->Cell(count($rsRecordDetail));
		$w = array(15,30,30,30,100,50);
		$Ln = array('L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									
		
			$dateAdded = substr($rsRecordDetail[$i]['dateAdded'],0,10);
			if($dateAdded>=$_GET['dateFrom'] && $dateAdded<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==1 && $rsRecordDetail[$i]['documentTypeId']==$_GET['doctype'])
			{
				$ctr++;
				$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
			}	
		}
		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
			include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$this->SetFont('Arial','',11);				

		if($_SESSION['userType']==3){
			$officeCode = explode('~',$objDocDetail->getOfficeCode2($objDocDetail->get('userID')));
			$officeName = $officeCode[0];		
		}else{
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		}

		$this->Cell(0,5,$officeName, 0, 1, 'L'); 
		$this->Ln(5);
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C",1);
		$this->Cell(30,7,"DOC ID",1,0,"C",1);
		$this->Cell(30,7,"DOC TYPE",1,0,"C",1);
		$this->Cell(30,7,"STATUS",1,0,"C",1);
		$this->Cell(100,7,"SUBJECT",1,0,"C",1);
		$this->Cell(50,7,"DATE ENCODED",1,0,"C",1);
		$this->Ln(10);
	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>