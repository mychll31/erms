<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentEncoded extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
	
		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		//include_once("Records.class.php");
		//$objRecordDetail = new Records;
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;

		$rsRecordDetailIncoming = $objDocDetail->getDocListEncoded2($_SESSION['userID'],0, $intMonth, $intYear);
		$rsRecordDetailOutgoing = $objDocDetail->getDocListEncoded2($_SESSION['userID'],1, $intMonth, $intYear);
		//$rsRecordDetailIntra = $objRecordDetail->getDocListEncoded($_SESSION['userID'],2,$_GET['dateFrom'],$_GET['dateTo']);
		//$this->Cell(400,50,"outgoing=".$rsRecordDetailOutgoing,0,1,"R");
		$intTotalIncoming = $rsRecordDetailIncoming[0]['_total'];
		$intTotalOutgoing = $rsRecordDetailOutgoing[0]['_total'];
		/*
		if(sizeof($rsRecordDetailIncoming))
			$intTotalIncoming = $rsRecordDetailIncoming[0]['_total'];
		else
			$intTotalIncoming = '0';
		if(sizeof($rsRecordDetailOutgoing))
			$intTotalOutgoing = $rsRecordDetailOutgoing[0]['_total'];
		else
			$intTotalOutgoing = '0';
		*/	
		/*
		if(count($rsRecordDetailIntra))
			$intTotalIntra = count($rsRecordDetailIntra);
		else
			$intTotalIntra = '0';
		*/	
		
		$rsRecordHistory = $objDocDetail->getActionsRecorded($_SESSION['userID'], $intMonth, $intYear);
		
		$rsReceivedDocs = $objDocDetail->getReceivedDocs($_SESSION['userID'], $intMonth, $intYear);
		
		
		if(count($rsRecordHistory))
			$intTotalActions = count($rsRecordHistory);
		else
			$intTotalActions = '0';
			
		if(count($rsReceivedDocs))
			$intTotalReceived = count($rsReceivedDocs);
		else
			$intTotalReceived = '0';	
			
		$this->SetFont('Arial','B',10);
		$this->Cell(50,7," ",0,0,"C");
		$this->Cell(30,7,"DOCUMENT ENCODED",0,1,"L");
//		$this->Cell(30,7,"100",0,1,"R");
		$this->Cell(70,7," ",0,0,"C");
		$this->Cell(30,7,"INCOMING",0,0,"L");
		$this->Cell(10,7,$intTotalIncoming,0,1,"R");
		$this->Cell(70,7," ",0,0,"C");
		$this->Cell(30,7,"OUTGOING",0,0,"L");
		$this->Cell(10,7,$intTotalOutgoing,0,1,"R");
		//$this->Cell(70,7," ",0,0,"C");
		//$this->Cell(30,7,"INTRA-OFFICE",0,0,"L");
		//$this->Cell(10,7,$intTotalIntra,0,1,"R");
		$this->Ln(5);
		$this->Cell(50,7," ",0,0,"C");
		$this->Cell(30,7,"ACTION RECORDED",0,0,"L");
		$this->Cell(30,7,$intTotalActions,0,1,"R");
		$this->Ln(5);
		$this->Cell(50,7," ",0,0,"C");
		$this->Cell(30,7,"RECEIVED DOCUMENTS",0,0,"L");
		$this->Cell(30,7,$intTotalReceived,0,1,"R");
		$this->Ln(10);
		
/*		
		$w = array(15,30,30,30,100,50);
		$Ln = array('L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetFont('Arial','',9);
		$ctr=0;	
		for($i=0;$i<count($rsRecordDetail);$i++) 
		{									
		
			
			if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==2)
			{
				$ctr++;
				$this->Row(array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']),1);
			}	
		}
*/		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");

	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];


		$this->SetFont('Arial','',11);				

		if($_SESSION['userType']==3){
			$officeCode = explode('~',$objDocDetail->getOfficeCode2($objDocDetail->get('userID')));
			$officeName = $officeCode[0];	
			$this->Cell(0,5,$officeName, 0, 1, 'L'); 
			$this->Ln(3);
 			$this->Cell(0,5,'Processed by: '.$objDocDetail->getEmpName($objDocDetail->get('userID')), 0, 1, 'L');
				
		}else{
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
			$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
			$this->Cell(0,5,$officeName, 0, 1, 'L'); 
			$this->Ln(5);
			$this->Cell(0,5,'Processed by: '.$objDocDetail->getProcessedbyName($_SESSION['userID']), 0, 1, 'L');
		}

		
		$this->Ln(5);		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);

		$this->ReportType = $objReport->getReportName($_GET['reportType']);
		
		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.date('F Y',strtotime($intYear.'-'.$intMonth.'-01')).')', 0, 1, 'C');

		//$this->Cell(0,5,'('.$dateFrom->getAsStr("d M Y").' to '.$dateTo->getAsStr("d M Y").')', 0, 1, 'C');
		$this->Ln(8);
		

	}
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>