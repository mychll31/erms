<? include_once("ERMS_define.php");
include_once("MySQLHandler2.class.php");
include_once("MySQLHandler.class.php");
include_once("General.class.php");
class ManageCustodian extends General
{

/*function getEmployeeList($tag,$oCode,$empNumber)
{
	if($tag == Group4)
		{
			$tWhere = "WHERE tblEmpPosition.group4='$oCode'";
		}
	elseif($tag == Group3)
		{
			$tWhere = "WHERE tblEmpPosition.group3='$oCode'";
		}
	elseif($tag == Group2)
		{
			$tWhere = "WHERE tblEmpPosition.group2='$oCode'";
		}
	elseif($tag == Group1)
		{
			$tWhere = "WHERE tblEmpPosition.group1='$oCode'";
		}
		$tWhere.=" AND tblEmpPersonal.empNumber NOT IN (SELECT empNumber FROM tblCustodian WHERE empNumber<>'$empNumber')";
		
	$sql = "SELECT tblEmpPersonal.empNumber, tblEmpPersonal.surname, 	tblEmpPersonal.firstname,tblEmpPersonal.middleInitial,tblEmpPersonal.nameExtension,tblEmpPosition.statusOfAppointment  
							FROM tblEmpPersonal
							INNER JOIN tblEmpPosition
								ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber
								INNER JOIN tblPosition
									ON tblEmpPosition.positionCode = tblPosition.positionCode
									"
							.$tWhere.
							" ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	
	$sql1= new MySQLHandler2();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs))
			return $rs;	
}*/



function getEmployeeList($tag,$oCode,$empNumber)
{
	$SQL="SELECT empNumber FROM tblCustodian WHERE empNumber<>'$empNumber'";
	$sql2= new MySQLHandler();
		$sql2->init();
		$RS=$sql2->Select($SQL);
	$RScount=count($RS);
	
	$custodianList="";
	
	if($RScount>0)
	{
	$custodianList="'".$RS[0]["empNumber"]."'";
	for($i=1;$i<$RScount;$i++)
	{
	$custodianList.=", '".$RS[$i]["empNumber"]."'";
	}
	}
	if($tag == Group4)
		{
			$tWhere = "WHERE tblEmpPosition.group4='$oCode'";
		}
	elseif($tag == Group3)
		{
			$tWhere = "WHERE tblEmpPosition.group3='$oCode'";
		}
	elseif($tag == Group2)
		{
			$tWhere = "WHERE tblEmpPosition.group2='$oCode'";
		}
	elseif($tag == Group1)
		{
			$tWhere = "WHERE tblEmpPosition.group1='$oCode'";
		}
		
		if($tWhere!='') $tWhere.=" AND tblEmpPosition.statusOfAppointment = 'In-Service' ";
		//$tWhere.=" AND tblEmpPersonal.empNumber NOT IN ($custodianList)";
		
	$sql = "SELECT tblEmpPersonal.empNumber, tblEmpPersonal.surname, 	tblEmpPersonal.firstname,tblEmpPersonal.middleInitial,tblEmpPersonal.nameExtension,tblEmpPosition.statusOfAppointment  
							FROM tblEmpPersonal
							LEFT JOIN tblEmpPosition
								ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber
								LEFT JOIN tblPosition
									ON tblEmpPosition.positionCode = tblPosition.positionCode
									"
							.$tWhere. 
							" ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
	
	$sql1= new MySQLHandler2();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs)){
				
			/*if($tag == Group4)
				{
					$tWhere = "WHERE tblUserAccount.group4='$oCode'";
				}
			elseif($tag == Group3)
				{
					$tWhere = "WHERE tblUserAccount.group3='$oCode'";
				}
			elseif($tag == Group2)
				{
					$tWhere = "WHERE tblUserAccount.group2='$oCode'";
				}
			elseif($tag == Group1)
				{
					$tWhere = "WHERE tblUserAccount.group1='$oCode'";
				}*/
				$tWhere = "WHERE tblUserAccount.officeCode='$oCode'";
				//$tWhere.=" AND tblUserAccount.empNumber NOT IN ($custodianList)";
				
				
			$SQL2="SELECT empNumber,surname,firstname, middleInitial, nameExtension,'contractual' AS statusOfAppointment FROM tblUserAccount ".$tWhere." ORDER BY surname";
			$sql3= new MySQLHandler();
			$sql3->init();
			$RS2=$sql3->Select($SQL2);
			$RScount=count($RS2);
			//print_r($RS2);
			if($RScount>0){
			for($i=0;$i<$RScount;$i++)	$rs[]=$RS2[$i];
			}
			return $rs;	
			}
}

function getCustodians()
	{
		$SQL="SELECT empNumber FROM tblCustodian";
		
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($SQL);	
		
		$RScount=count($rs);
		$custodianList="";
		
		if($RScount>0)
		{
			$custodianList="'".$rs[0]["empNumber"]."'";
			for($i=1;$i<$RScount;$i++)
			{
			$custodianList.=", '".$rs[$i]["empNumber"]."'";
			}
		}
		
		$empList = "SELECT tblEmpPersonal.empNumber, tblEmpPersonal.surname, 	tblEmpPersonal.firstname,tblEmpPersonal.middleInitial,tblEmpPersonal.nameExtension,tblEmpPosition.statusOfAppointment, tblEmpPosition.officeCode, tblEmpPosition.divisionCode  
							FROM tblEmpPersonal
							LEFT JOIN tblEmpPosition
								ON tblEmpPersonal.empNumber = tblEmpPosition.empNumber
							LEFT JOIN tblPosition
								ON tblEmpPosition.positionCode = tblPosition.positionCode
							WHERE tblEmpPersonal.empNumber IN (".$custodianList.")
							AND tblEmpPosition.statusOfAppointment = 'In-Service'
							ORDER BY tblEmpPersonal.surname, tblEmpPersonal.firstname";
		
		$sql2= new MySQLHandler2();
		$sql2->init();
		$rs2=$sql2->Select($empList);
		return $rs2;
		
	}

}
?>