<? 
session_start();
include_once("General.class.php");
class Dispose extends General {

function getForDisposalList($t_strSearch="", $page="", $limit="", $order="", $ascdesc="")
	{
	$t_dtmNow = date("Y-m-d");
	$arrDate = explode("-",$t_dtmNow);
	
	if($t_strSearch<>"")
			$strSearch = " AND (tblDocument.documentId = '".$t_strSearch."' OR tblDocument.subject LIKE '%".$t_strSearch."%') ";
	
	$sql = "SELECT DISTINCT
	tblDocument.subject,
	tblDocument.status,
	CAST(SUBSTR(tblDocument.documentDate,1,4)AS UNSIGNED) as documentYear,
	tblDocumentType.retentionPeriod,
	tblDocument.documentId,
	tblDocument.documentTypeId,		
	tblDocument.documentDate, 
	tblDocument.originId,
	tblDocumentType.retentionPeriod
	FROM tblDocument LEFT JOIN tblDocumentType ON tblDocument.documentTypeId=tblDocumentType.documentTypeId
	WHERE tblDocument.isDispose=0 AND 
	(CAST(SUBSTR(tblDocument.documentDate,1,4)AS UNSIGNED)+tblDocumentType.retentionPeriod<='".$arrDate[0]."' 
	AND CAST(SUBSTR(tblDocument.documentDate,6,2)AS UNSIGNED)<='".$arrDate[1]."' 
	AND CAST(SUBSTR(tblDocument.documentDate,9,2)AS UNSIGNED)+1<='".$arrDate[2]."') 
	AND tblDocument.addedByOfficeId = '".$this->get('office')."' 
	AND tblDocumentType.retentionPeriod>0 $strSearch";
	
	if(strlen($order)>0)
		$sql = $sql . " ORDER BY $order $ascdesc";
	else
		$sql = $sql . " ORDER BY tblDocument.documentDate DESC, tblDocument.documentId ASC";	
	if($page==0||$page=="")
		$page = 1;
	$offset = ($page - 1) * $limit;
	if(strlen($page)>0 && strlen($limit)>0)
		$sql = $sql . " LIMIT $offset, $limit";		
	$sql1= new MySQLHandler();
	$sql1->init();
	//echo $sql."<br><br>";
	$rs=$sql1->Select($sql);	
	return $rs;
	}

function getDisposedList($t_strSearch,$t_strType)
	{
	if($t_strType=="records")
		$sqlType = "AND disposeRemark='Transferred to Records Center' ";
	else if($t_strType=="archives")
		$sqlType = "AND disposeRemark='Transferred to National Archives' ";
	else
		$sqlType = "AND (disposeRemark='Sold' OR disposeRemark='Destroyed') ";	
			
	
	if(strlen(trim($t_strSearch))>0)
			$strSearch = " AND (tblDocument.documentId = '".$t_strSearch."' OR tblDocument.subject LIKE '%".$t_strSearch."%') ";	
	$sql = "SELECT DISTINCT documentId,subject,documentTypeId,documentDate,originId,disposeRemark,officeSig,status FROM tblDocument WHERE isDispose=1 AND addedByOfficeId='".$this->get("office")."' $sqlType $strSearch";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);	
	if(count($rs))
		return $rs;
	}

function disposeDocument($t_strDocId, $t_strRemark)
	{
		// Update Disposal Table
		$sql = "INSERT INTO tblDisposal (documentId, disposeRemark, userid) VALUES('".$t_strDocId."', '".$t_strRemark."', '".$this->get('userID')."')";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Insert($sql);

		$sql = "UPDATE tblDocument SET isDispose=1,disposeRemark='".$t_strRemark."' WHERE documentId='".$t_strDocId."'";
		//echo "<br>".$sql;
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Update($sql);	
	}

function getDisposedBy($t_strDocId)
	{
		$sql = "SELECT * FROM tblDisposal LEFT JOIN tblUserAccount ON tblUserAccount.empNumber = tblDisposal.userid where documentId='".$t_strDocId."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);	
		if(count($rs))
			return $rs;
	}
}
?>