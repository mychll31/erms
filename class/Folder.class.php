<?php 
include_once("MySQLHandler.class.php");
include_once("General.class.php");
class Folder extends General{
function addFolder($t_intUserID, $t_strFolderName)
{
	
	$SQL="INSERT INTO tblFolders (userID, folderName) 
			VALUES('".$t_intUserID."','".$t_strFolderName."')";
	$sql1= new MySQLHandler();
	$sql1->init();	
	//echo $SQL;
	if($rs=$sql1->Insert($SQL))
		{
			//$this->msg = MSG_ADD_SUCCESS;
			echo "<script language='javascript'>getData('showFolder.php?mode=show','folders');getData('showFolder.php?mode=list','folder_list');</script>";
			return 1;
		}
		else
		{
			//$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'</font>';
			return 0;
		}
}//end addFolder 

function renameFolder($t_intFolderID, $t_strFolderName)
{
	
	$SQL="UPDATE tblFolders SET folderName = '".$t_strFolderName."'
			WHERE folderID = '".$t_intFolderID."'";
	
	$sql1= new MySQLHandler();
	$sql1->init();	
	if($rs=$sql1->Update($SQL))
		{
			//$this->msg = MSG_ADD_SUCCESS;
			echo "<script language='javascript'>getData('showFolder.php?mode=show','folders');getData('showFolder.php?mode=list','folder_list');</script>";
			//return 1;
		}
		else
		{
			//$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'</font>';
			//return 0;
		}
}//end renameFolder 

function deleteFolder($t_intFolderID)
{
	
	$SQL="DELETE FROM tblFolders WHERE folderID = '".$t_intFolderID."'";
	$sql1= new MySQLHandler();
	$sql1->init();	
	//echo $SQL;
	if($rs=$sql1->Delete($SQL))
		{
			//$this->msg = MSG_ADD_SUCCESS;
			echo "<script language='javascript'>getData('showFolder.php?mode=show','folders'); getData('showFolder.php?mode=list','folder_list');</script>";
			return 1;
		}
		else
		{
			//$this->msg = '<font color="red">'.MSG_ADD_UNSUCCESS.'</font>';
			return 0;
		}
}//end renameFolder 

function getEmpFolders($t_intUserID){
	$rs = $this->getSqlEmpFolders($t_intUserID);
	if(count($rs))
	{
	echo "<table align=center width=95% cellspacing=0 cellpadding=0>";
	for($t=0;$t<count($rs);$t++) 
		{
		$t_intTotal = count($this->getFolderCount($rs[$t]['folderID'],$this->get("userID")));
		echo "<tr>
			<td width=30%>".$rs[$t]['folderName']."</td>
			<td width=1%>&nbsp;|&nbsp;</td>
			<td width=29%>&nbsp;".$t_intTotal."&nbsp;&nbsp;document(s)</td>
			<td width=15%><a href='#' onClick=\"javascript:getData('showFolder.php?mode=rename&fname=".$rs[$t]['folderName']."&fid=".$rs[$t]['folderID']."','folders');\">rename</a></td>
			<td width=15%><a href='#' onClick=\"javascript:getData('showFolder.php?mode=delete&fname=".$rs[$t]['folderName']."&fid=".$rs[$t]['folderID']."','folders');\">delete</a></td></tr>";
		}//end for loop
	echo "</table>";
	}//end if statement	
}//end function getEmpFolders


function getFolderCount($t_intFolderID,$t_strUserID)
	{
	$sql = "SELECT DISTINCT tblDocumentFolders.* FROM tblDocumentFolders 
			LEFT JOIN tblFolders ON tblDocumentFolders.folderID = tblFolders.folderID
			WHERE tblDocumentFolders.folderID = '".$t_intFolderID."' AND tblFolders.userID = '".$t_strUserID."'";
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);		
	return $rs;		
	}

function getSqlEmpFolders($t_intUserID)
	{
		$sql = "SELECT * FROM `tblFolders` WHERE userID = '".$t_intUserID."' order by folderName";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);		
		return $rs;
	}

}	
?>