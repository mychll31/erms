<? 
error_reporting(E_ALL);
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentReceived_daily extends FPDF
{	
	var $ReportType;
	function generateReport()
	{
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;

		$this->SetFont('Arial','',9);
		$this->SetWidths(array(10,35,40,30,30,30,67,28));
		$this->SetAligns(array('C','C','L','C','L','C','L','L'));

		$records = $objDocDetail->getReceivedDocByUserID($_SESSION['empNum'], $_GET['dateFrom'], $_GET['dateTo']);
		$ctr = 1;
		foreach($records as $record):
			$remarks = $objDocDetail->getAllRemarks($record['documentId']);
			$this->Row(array($ctr++,
								$record['documentId'],
								$record['subject'],
								$record['dateReceived'],
								$record['receivedBy'],
								$record['documentDate'],
								$remarks,
								''));

		endforeach;
		
		include_once("MySQLHandler.class.php");
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		$intYear = $_GET['dateYear'];
		
		$this->SetFont('Arial','',11);
		$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5);
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,11);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',9);
		$this->Ln(8);
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);

		## HEADER COLUMN NAME
		$cell_w = array('10','35','40','30','30','30','67','28');
		$cell_caption = array("","DOC ID","SUBJECT","DATE RECEIVED","RECEIVED BY","DOCUMENT DATE","SENDER / REMARKS","REFFERED TO");

		foreach($cell_caption as $key=>$cell):
			$this->Cell($cell_w[$key],5,$cell,$key > 9 ? 'BRL' : 'TRL',$key == 9 ? 1 : 0,"C",1);
		endforeach;
		$this->Ln();
	}
	
	function footer()
	{
		$this->Ln();
		$this->SetFont('Arial','',11);
		$this->Ln();
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}


	function times_counter($times)
	{
		$day = 0;
		$hou = 0;
		$min = 0;
		$sec = 0;
		$totaltime = '00:00:00';

		if(count($times) > 1) {
			if(is_array($times)){

	            $length = sizeof($times);

	            for($x=0; $x <= $length; $x++){
	                    $split = explode(":", @$times[$x]); 
	                    $this->hou += @$split[0];
	                    $this->min += @$split[1];
	                    $this->sec += @$split[2];
	            }

	            $seconds = $this->sec % 60;
	            $minutes = $this->sec / 60;
	            $minutes = (integer)$minutes;
	            $minutes += $this->min;
	            $hours = $minutes / 60;
	            $minutes = $minutes % 60;
	            $hours = (integer)$hours;
	            $hours += $this->hou % 24;
	        }
	    } else {
	    	$time = explode(':', $times[0]);
	    	$day = 0;
	    	$hours = $time[0];
	    	$minutes = $time[1];
	    	$seconds = $time[2];
	    }
	    $day = $hours / 24;
	    $day = intval($day);
	    $hours = $hours - ($day*24);
	    // echo $day;
	    // print_r($times);
	    $totaltime = $day.($day > 1 ? " Days " : " Day ").sprintf("%02d", $hours).($hours > 1 ? " Hours " : " Hour ").sprintf("%02d", $minutes).($minutes > 1 ? " Minutes " : " Minute ").sprintf("%02d", $seconds).($seconds > 1 ? " Seconds" : " Second");
            return $totaltime;
	}

}
?>
