<? 
session_start();
include_once("MySQLHandler2.class.php");
include_once("Login.class.php");
include_once("General.class.php");
class Employee extends General {

function getEmpDocList($t_strOffice, $page="", $limit="", $order="", $ascdesc="",$t_strSearch='',$t_strOperator='')
	{
	if($t_strSearch!="")
	{
		$strSearch .= " AND ";
		switch($t_strOperator){
			case 'OR': $arrSearch = explode('OR',$t_strSearch);
					   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
									 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
					   break;
			case 'EXCEPT': break;				
			case 'AND': $arrSearch = explode(' ',$t_strSearch);	
						$strSearch .= (count($arrSearch)>1 && $arrSearch[1] != '')?
								  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
								  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
								  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
						 break;				
			case 'DOCTYPE': 
				$arrSearch = explode(' ',$t_strSearch);
				$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
				$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
							break;								
			case 'CONTAINS': 
				$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
				$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
				break;				
		}
	}
	$res = $this->getOrgStructureUnder($this->get("userID"));
	for($t=0;$t<count($res);$t++)
		{
		//$t_strWhereUnder .= " OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy<>'') ";
		$t_strWhereUnder .= " OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1) ";		
		}
	

	if($this->getOfficeHead($this->get('office'))==$this->get('userID'))
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.receivedBy<>'') ";
	    $t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' ) ";		
	else {
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy is NULL) ";	
		if ($_SESSION['blnAgencyUser'])
		$t_strWhereOfficeHead = " OR tblHistory.recipientId='".$t_strOffice."'";
		else
		$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1) ";		
			
	}

	$t_strWhereFilter = " OR (tblHistory.recipientId='0' AND tblHistory.restricted=0)";
	$t_strWhereAgency = " OR (tblHistory.recipientUnit='agency' AND tblHistory.recipientId REGEXP '^[0-9]+$' AND tblHistory.mode='ext')";
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ( SELECT DISTINCT tblDocument.*,tblHistory.receivedBy,tblHistory.historyId,tblHistory.actionCodeId,tblHistory.flag,tblHistory.priority ,tblHistory.dateSent FROM tblDocument 
	LEFT JOIN tblHistory ON tblDocument.documentId=tblHistory.documentId 
	WHERE (tblHistory.recipientId='".$this->get('userID')."' 
	$t_strWhereOfficeHead
	$t_strWhereUnder) 
	AND tblDocument.isDelete=0
	AND tblHistory.historyId NOT IN (SELECT historyId FROM tblTrash WHERE userID = '".$this->get('userID')."')
	$strSearch
	GROUP BY tblHistory.documentId HAVING (SELECT MAX(dateSent) FROM tblHistory) ORDER BY tblHistory.dateSent DESC ) result,
	(SELECT FOUND_ROWS() AS '_total') tot ";

		$page = ($page==0||$page=="")?1:$page;
		$offset = ($page - 1) * $limit;
		if(strlen($page)>0 && strlen($limit)>0)
			$sql .= " LIMIT $offset, $limit";		

		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}


function getEmpDocList2($t_strOffice, $page="", $limit="", $order="", $ascdesc="",$t_strSearch='',$t_strOperator='')
	{
	if($t_strSearch!="")
	{
		$strSearch .= " AND ";
		switch($t_strOperator){
			case 'OR': $arrSearch = explode('OR',$t_strSearch);
					   $strSearch .= "((tblDocument.documentId LIKE '%".trim($arrSearch[0])."%' OR tblDocument.documentId LIKE '%".trim($arrSearch[1])."%') OR
									 (tblDocument.subject LIKE '%".trim($arrSearch[0])."%' OR tblDocument.subject LIKE '%".trim($arrSearch[1])."%'))";
					   break;
			case 'EXCEPT': break;				
			case 'AND': $arrSearch = explode(' ',$t_strSearch);	
						$strSearch .= (count($arrSearch)>1 && $arrSearch[1] != '')?
								  " ((tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.documentId LIKE '%".$arrSearch[1]."%')
								  OR (tblDocument.subject LIKE '%".$arrSearch[0]."%' AND tblDocument.subject LIKE '%".$arrSearch[1]."%'))":
								  " (tblDocument.documentId LIKE '%".$arrSearch[0]."%' OR tblDocument.subject LIKE '%".$arrSearch[0]."%' OR tblDocument.docNum LIKE '%".$arrSearch[0]."%') ";
						 break;				
			case 'DOCTYPE': 
				$arrSearch = explode(' ',$t_strSearch);
				$searchDocType = substr($arrSearch[1],1,strlen($arrSearch[1]));
				$strSearch .= "(tblDocument.documentId LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%') OR (tblDocument.subject LIKE '%{$arrSearch[0]}%' AND tblDocumentType.documentTypeAbbrev LIKE '%{$searchDocType}%')";								
							break;								
			case 'CONTAINS': 
				$t_strSearch = substr($t_strSearch,1,strlen($t_strSearch));
				$strSearch.=" (tblDocument.documentId LIKE '%".$t_strSearch."%' OR tblDocument.subject LIKE '%".$t_strSearch."%' OR tblDocument.docNum LIKE '%".$t_strSearch."%') "; 
				break;				
		}
	}
	$res = $this->getOrgStructureUnder($this->get("userID"));
	for($t=0;$t<count($res);$t++)
		{
		//$t_strWhereUnder .= " OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy<>'') ";
		$t_strWhereUnder .= " OR (tblHistory.recipientId='".$res[$t]."' AND tblHistory.restricted<>1) ";		
		}
		
//		echo "" . $t_strWhereUnder ;
	
//	if($this->getOfficeHead($this->get('office'))==$this->get('userID'))
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.receivedBy<>'') ";
//	    $t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' ) ";		
//	else
		//$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1 AND tblHistory.receivedBy is NULL) ";	
//		$t_strWhereOfficeHead = " OR (tblHistory.recipientId='".$t_strOffice."' AND tblHistory.restricted<>1) ";	
	
//	if($t_blnAgencyDocsOnly){
		 $sql1= new MySQLHandler();
		 $sql1->init();
		 $s_sql = "SELECT empNumber FROM `tblUserAccount` WHERE `agencyCode` !=0";
		 $rs = $sql1->Select($s_sql); 
		 foreach($rs as $originId){
			 $arrInOriginId[] =$originId[0];
		 }
		 $strInOriginId ="('".implode("','", $arrInOriginId)."')";
		 $whereInOriginId = " AND tblDocument.addedById in $strInOriginId ";
//		}

//		echo " // " .  $whereInOriginId ;
//	echo "".$t_strOffice;
	$t_strWhereFilter = " OR (tblHistory.recipientId='0' AND tblHistory.restricted=0)";
	$t_strWhereAgency = " AND tblHistory.recipientUnit='agency' AND tblHistory.mode='ext'";
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ( SELECT DISTINCT tblDocument.*,tblHistory.receivedBy,tblHistory.historyId,tblHistory.actionCodeId,tblHistory.flag,tblHistory.priority ,tblHistory.dateSent,tblHistory.senderID, tblHistory.recipientId FROM tblDocument 
	LEFT JOIN tblHistory ON tblDocument.documentId=tblHistory.documentId 
	
	WHERE tblHistory.senderId='RMS' $t_strWhereAgency   
	AND tblDocument.isDelete=0  AND tblHistory.actionCodeId ='23' 
	AND  tblHistory.recipientId = '".$t_strOffice  ."' AND tblHistory.restricted<>1
	AND tblHistory.historyId NOT IN (SELECT historyId FROM tblTrash WHERE userID = '".$this->get('userID')."')
	$strSearch
	
	HAVING (SELECT MAX(dateSent) FROM tblHistory)
	ORDER BY tblHistory.dateSent DESC, tblDocument.documentId ASC) result ,
	(SELECT FOUND_ROWS() AS '_total') tot 
	 ";
	
		$page = ($page==0||$page=="")?1:$page;
		$offset = ($page - 1) * $limit;
		if(strlen($page)>0 && strlen($limit)>0)
	//		$sql .= "  LIMIT $offset, $limit";		
		$sql .= "  LIMIT $offset, $limit";		
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		return $rs;
	}

	
	function checkRestrictions($t_strOffice,$t_strUserID,$t_intID)
	{
	$sql = "SELECT restricted FROM tblHistory WHERE historyId = '".$t_intID."'";
	$res= new MySQLHandler();
	$res->init();
	$rs=$res->Select($sql);			
	if($rs[0]['restricted'])
	{
		$objLogin = new Login;
		$t_strOfficeCode = $objLogin->checkOffice($t_strUserID);
		if($t_strOfficeCode)
		{
			switch($t_strOfficeCode)
			{
			case "Group1": 
				$sqlHead = "SELECT group1Code as code FROM tblGroup1 WHERE empNumber='".$t_strUserID."'"; break;
			case "Group2": 
				$sqlHead = "SELECT group2Code as code FROM tblGroup2 WHERE empNumber='".$t_strUserID."'"; break;
			case "Group3": 
				$sqlHead = "SELECT group3Code as code FROM tblGroup3 WHERE empNumber='".$t_strUserID."'"; break;
			case "Group4": 
				$sqlHead = "SELECT group4Code as code FROM tblGroup4 WHERE empNumber='".$t_strUserID."'"; break;
			case "Group5": 
				$sqlHead = "SELECT group5Code as code FROM tblGroup5 WHERE empNumber='".$t_strUserID."'"; break;
			default: 
				return 0; break;
			}
		}
		$sql1= new MySQLHandler2();
		$sql1->init();
		$rsHead=$sql1->Select($sqlHead);

		$sqlEmp = "SELECT documentId FROM tblHistory WHERE recipientId = '".$t_strUserID."' AND historyId = '".$t_intID."'";
		$sql2= new MySQLHandler();
		$sql2->init();
		$rsEmp=$sql2->Select($sqlEmp);		
		//echo $sqlEmp;
		if(count($rsHead))
		{
			if($rsHead[0]['code']==$t_strOffice)
					return 1;
			else
					return 0;
		}
		else if(count($rsEmp))
			{
			return 1;
			}
		else
			return 0;
	}
	else
		return 1;
	
	}
	


	
function showComboFolders($t_strName){
	$cmb =  '
	<select name="'.$t_strName.'" id="'.$t_strName.'">
		<option selected>-Select Folder-</option>';
	$sql = "SELECT * FROM `tblFolders` WHERE userID = '".$this->get("userID")."'";
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Select($sql);		
	for($t=0;$t<count($rs);$t++) 
	{
	$cmb = $cmb . '<option value="'.$rs[$t]['folderID'].'">'.$rs[$t]['folderName'].'</option>';
	}
	$cmb = $cmb . '</select>';
	return $cmb;
}



}//end Class
?>