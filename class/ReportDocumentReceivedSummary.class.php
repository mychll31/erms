<? 
session_start();
header("Cache- Control: no-cache, must-revalidate" ); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
define('FPDF_FONTPATH','font/');
require_once('fpdf.php');
//include_once("MySQLHandler.class.php");
include_once("DocDetail.class.php");
$objDocDetail = new DocDetail;

class ReportDocumentReceivedSummary extends FPDF
{	
	var $ReportType;
	function generateReport()  
	{
	
		
		include_once("MySQLHandler.class.php");
		include_once("DocDetail.class.php");
		
		$objDocDetail = new DocDetail;
		include_once("Records.class.php");
		$objRecordDetail = new Records;
		
		$intDateFrom = $_GET['dateFrom'];
		$intDateTo = $_GET['dateTo'];
		
		$offCode = "RMS";
		  
		//if($_SESSION['userType']==1 || $_SESSION['userType']==2){
			
			if ($_SESSION['userID']=='1010-CO0-2012' || $_SESSION['userID']=='1301-CO0-2008'){
				$offCode2 = array("OASECFALA", "ALS", "LD", "GSD", "PSS", "GSS","RMS","TELECOM","MOTORPOOL","BGMU","CASHIER","PROCUREMENT", "BAC","PD", "DDCC", "FMS", "BD", "AD", "MD", "DLLO");
	
			}else{ 
				$offCode2 = array(); 
				$offCode2[] = ($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']);
				 
				$offNme = "". $objDocDetail->displayRecipientSenderNames($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'], $_SESSION['userUnit']);	
				}  
		//}


/*							 
		$offCode2 = array("ALS","RMS", "PD","FMS", "BD", "ITD");		
*/							 
		
		
							 
		$arrlength = count($offCode2);
		$currOffCode = "";
		$currOffCode4 = "";
		
//		$arrAllUnacted =
		

		$w = array(15,80,30,30,30,30,50);
		$Ln = array('L','L','L','L','L','L','L');
		$this->SetWidths($w);
		$this->SetAligns($Ln);
		$this->SetLeftMargin(30); 
		$this->SetFont('Arial','',9);	
		$this->SetFillColor("250","250","250");
		
	
		
		for($x = 0; $x < $arrlength; $x++) { 
		
			$intTotalIncoming = 0;
			$intTotalReceived = 0;
			$intTotalRecorded = 0;
			$intTotalUnActed = 0;
			
			
			
			
								
			$rsCustodian = $objDocDetail->getCustodian($offCode2[$x]);		
			if (count($rsCustodian)) {
				$arrlength2 = count($rsCustodian);
				for($y = 0; $y < $arrlength2; $y++) { 
			
				
					//for Incoming 
					if ($currOffCode != $offCode2[$x]) { 
					$rsIncomingDocs = $objDocDetail->getIncomingListX($offCode2[$x], $rsCustodian[$y], $intDateFrom, $intDateTo);			
					
					//$rsReceivedDocs = $objDocDetail->getIncomingListX($offCode2[$x], $rsCustodian[$y], $intDateFrom, $intDateTo);   
					
					
					
					
					  
					if(count($rsIncomingDocs)) 
						$intTotalIncoming = $intTotalIncoming + count($rsIncomingDocs);
					else
						$intTotalIncoming = $intTotalIncoming;
					
					
					
					/*
					for($j=0;$j<count($rsIncomingDocs);$j++)
					{
					 	
					//for Received		
					$rsReceivedDocs = $objDocDetail->getReceivedDocs3($rsIncomingDocs[$j]["historyId"], $intDateFrom, $intDateTo);			
									  
					if(count($rsReceivedDocs)) 
						$intTotalReceived = $intTotalReceived + 1;
					else
						$intTotalReceived = $intTotalReceived;
							
						
						
									  
						if($rsIncomingDocs[$j]["reply"]==1)
						{										
						//for ActionRecorded	
						$intTotalUnActed =  $intTotalUnActed + 1;
						  
						//$rsAction = $objDocDetail->showActions($rsIncomingDocs[$j]["documentId"]);
						//for($i=0;$i<count($rsAction);$i++) 
						//{	 
						$checkRecipientReply = $objDocDetail->checkRecipientHistoryActions2($rsIncomingDocs[$j]["historyId"],$offCode2[$x] );
							  
							if(count($checkRecipientReply)>=1  )  
							{					
								$intTotalRecorded = $intTotalRecorded + 1;	
								$intTotalUnActed =  $intTotalUnActed - 1;
							
							}
							else 
							{   
								$intTotalRecorded = $intTotalRecorded;	
								 
							}
							
						 
						}
					
					
					
					}// end for $rsIncomingDocs
					*/
											
					$currOffCode =  "".$offCode2[$x];
					
					}
				 
				
					
			/*			//for Received					
				$rsReceivedDocs = $objDocDetail->getReceivedDocs3($rsCustodian[$y], $intDateFrom, $intDateTo);			
								  
				if(count($rsReceivedDocs)) 
					$intTotalReceived = $intTotalReceived + count($rsReceivedDocs);
				else
					$intTotalReceived = $intTotalReceived;
			*/		  
			
				}//for custodian
			}else{		
			$intTotalIncoming = 0;
			$intTotalReceived = 0;
			$intTotalRecorded = 0;
			$intTotalUnActed  = 0;						
			}
			
		if ($_SESSION['userID']=='1010-CO0-2012' || $_SESSION['userID']=='1301-CO0-2008')
			$offNme ="".$offCode2[$x];
		$this->Cell(15,7,$x+1,1,0,"C",1);
		$this->Cell(100,7,"$offNme",1,0,"L",1);
		$this->Cell(30,7,"$intTotalIncoming",1,0,"C",1); 
		$this->Cell(30,7,"$intTotalReceived",1,0,"C",1);
		$this->Cell(30,7,"$intTotalRecorded",1,0,"C",1);
		$this->Cell(30,7,"$intTotalUnActed",1,0,"C",1);
//		$this->Cell(50,7," - ",1,0,"C",1);
		$this->Ln(7);			

		}
		
		
		
		$objLog = new MySQLHandler;
		$objLog->init();
		$objLog->changelog("","GENERATE","REPORTS","generated ".$this->getReportType()." report");
		
	}	
	
	function Header()
	{	
		include_once("DocDetail.class.php");
		$objDocDetail = new DocDetail;
		include_once("Report.class.php");
		$objReport = new Report;

		//$intMonth = str_pad($_GET['dateMonth'], 2, "0", STR_PAD_LEFT);
		//$intYear = $_GET['dateYear'];
		
		$dateFrom = new Date($_GET['dateFrom']);
		$dateTo = new Date($_GET['dateTo']);
		if ($dateFrom == $dateTo)
			$strDateRange = "".$dateFrom->getAsStr("M d Y");  
		else
			$strDateRange = "".$dateFrom->getAsStr("d M Y")." to ".$dateTo->getAsStr("d M Y");		
		
		$this->SetFont('Arial','',11);
		
		
		if ($_SESSION['userID']=='1010-CO0-2012' || $_SESSION['userID']=='1301-CO0-2008') 
			$officeName = $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit']);
		else 
			$officeName= "".$objDocDetail->getEmpName($objDocDetail->get('userID'));
		
		$this->Cell(0,5,AGENCY_NAME, 0, 1, 'L');
		$this->Cell(0,5,$officeName, 0, 1, 'L');
		$this->Ln(5); 
		//$this->Cell(0,5,'Processed by: '.$objDocDetail->getProcessedbyName($_SESSION['userID']), 0, 1, 'L');
		//$this->Ln(5);

		//dateFrom = new Date($_GET['dateFrom']);
		//$dateTo = new Date($_GET['dateTo']); 
		
		$this->ReportType = $objReport->getReportName($_GET['reportType']);

		$this->SetFont('Arial',B,12);
		$this->Cell(0,5,$this->ReportType, 0, 1, 'C');
		$this->SetFont('Arial','',12);
		$this->Cell(0,5,'('.$strDateRange.')', 0, 1, 'C');
		$this->Ln(8);
		$this->SetLeftMargin(30); 
		$this->Cell(15,7,'', 0, 1, 'C');
		$this->SetFillColor("210","210","210");
		$this->SetFont('Arial','B',9);
		$this->Cell(15,7," ",1,0,"C",1);
		$this->Cell(100,7,"OFFICE",1,0,"C",1);
		$this->Cell(30,7,"INCOMING",1,0,"C",1); 
		$this->Cell(30,7,"RECEIVED",1,0,"C",1);
		$this->Cell(30,7,"ACTED UPON",1,0,"C",1);
		$this->Cell(30,7,"UNACTED",1,0,"C",1);
//		$this->Cell(50,7,"---",1,0,"C",1);
		$this->Ln(7);
	} 
	
	function footer()
	{
		$this->intPageNo = $this->PageNo();
		$this->SetFont('Arial','',9);
		$this->SetY(-15);   // print date and time
		$this->Cell(0,5,'Page '.$this->intPageNo.' of {nb}', 0, 1, 'R');
		
		$this->Ln(5);
		$this->Cell(5, 4, date("m/d/Y H:i:s"), 0, 0, 'L');
	}	
	
	function getReportType()
	{
		return $this->ReportType;
	}
}
?>