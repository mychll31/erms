<? @session_start();?>
<script language="javascript" type="text/javascript"> 
var validator='';
  jQuery("#ofrefresh").click( function(){ 
  //if($("#ofedit").val() == 'Save')  $("#ofedit").val('Edit');
	jQuery('#OfficeGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#OfficeGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Name', 'Contact Number', 'Address', 'Contact Person', 'Owner Office'],
		colModel:[
			{name:'originId',index:'originId', width:65},
			{name:'officeName',index:'officeName', width:90},
			{name:'contact',index:'contact', width:100},
			{name:'address',index:'address', width:90},
			{name:'contactPerson',index:'contactPerson', width:100},
			{name:'ownerOffice',index:'ownerOffice', width:100, hidden:true}			
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#ofpager'),
		postData:{table:'tblOriginOffice',searchField:'ownerOffice',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew'},
		sortname: 'originId',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"Office"
	}).navGrid('#ofpager',{edit:false,add:false,del:false});}); 
  });

    $('#ofclear').click( function(){ 
	   $("#officeId").val(''); 	   
	   $("#officeName").val(''); 
   	   $("#contact").val(''); 
	   $("#address").val(''); 
   	   $("#contactperson").val(''); 
	   $("#ofsave").css('display','none');
	   $("#ofadd").css('display','block');
	   $('#Clear_office').text(" Clear ");
 	   validator.resetForm();
	   $('#ofrefresh').click();
    });
//Edit & Save function	
	jQuery("#ofedit").click( function(){ 
	  var id = jQuery("#OfficeGrid").getGridParam('selrow'); 
	  if (id) {
	   var ret = jQuery("#OfficeGrid").getRowData(id); 
	   $("#officeId").val(ret.originId); 	   
	   $("#officeName").val(ret.officeName); 
   	   $("#contact").val(ret.contact); 
	   $("#address").val(ret.address); 
   	   $("#contactperson").val(ret.contactPerson); 
	   if($("#ofedit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
	    $("#ofsave").css('display','block');
		$("#ofadd").css('display','none');
		$('#Clear_office').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	});

	jQuery("#ofsave").click( function(){ 
	  var id = jQuery("#OfficeGrid").getGridParam('selrow'); 
	  var ofid = $("#officeId").val();
 	  var name = $("#officeName").val();
   	  var contact = $("#contact").val();
	  var add = $("#address").val();	  
	  var person = $("#contactperson").val();
	  var owner = "<? echo $_SESSION['office']; ?>";	  
	   $("#ofsave").css('display','none');
	   $("#ofadd").css('display','block'); 
	   $("#OfficeGrid").setPostData({mode:"save",table:'tblOriginOffice',ID:ofid,NAME:name,CONTACT:contact,ADDRESS:add,PERSON:person,OWNER:owner,searchField:'ownerOffice',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew',MODULE:'Office'});
	   $("#OfficeGrid").trigger("reloadGrid");
 	   $('#ofclear').click();
	}); 
	
//Add function	   
//	$("#ofadd").click( function(){ 
//$('.ui-icon-search').click(function() { } );
    function AddRec(){
	  var ofid = $("#officeId").val();
 	  var name = $("#officeName").val();
   	  var contact = $("#contact").val();
	  var add = $("#address").val();	  
	  var person = $("#contactperson").val();	  
	  var owner = "<? echo $_SESSION['office']; ?>";	  
      $("#OfficeGrid").setPostData({mode:"add",table:'tblOriginOffice',ID:ofid,NAME:name,CONTACT:contact,ADDRESS:add,PERSON:person,OWNER:owner,searchField:'ownerOffice',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew',MODULE:'Office'});
	  $("#OfficeGrid").trigger("reloadGrid");
  	  $('#ofclear').click();

	  jAlert('Succesfully Added','Information'); }
//Delete func	 
	$("#ofdelete").click(function(){ 
	 var id = jQuery("#OfficeGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if( id != ''){ 
	 jConfirm('Proceed deleting this record?', false,'ERMS Confirmation Dialog', function(r) {
	 if(r==true){
	  $("#OfficeGrid").setPostData({mode:"del",table:'tblOriginOffice',ID:id.toString(),searchField:'ownerOffice',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew',MODULE:'Office'});
	  $("#OfficeGrid").trigger("reloadGrid"); 
	  $('#ofclear').click();
	  jAlert('Succesfully deleted', 'Confirmation Results');} });
	  }
	 else{ jAlert('Please select row to delete', 'Warning'); 
    }}); 
    $('#ofrefresh').click();
	
	$('#ofadd').click(function() {
     $('#frmOffice').submit();
    });
//Form validation
 	$().ready(function() {
	 validator = $("#frmOffice").validate({
		rules: {
			officeName: "required",
			//contact: { required: true, digits: true },
			//address: "required",
			contactperson: "required"
		},
		messages: {
			officeName: "Office name is required",
			//contact: { required: "Contact person is required", digits: "Invalid Input" },
			//address: "Address is required",
			contactperson: "Contact Person is required"
		},
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	
 	 

</script>
<form action="get" id="frmOffice" autocomplete="off">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Office Name</td>
    <td class="field"><input type="text" id="officeName" name="officeName" /></td>
	<td class="status"></td>
	<input type="hidden" id="officeId" value="">
  </tr>
  <tr>
    <td class="label">Contact Number</td>
    <td class="field"><input type="text" id="contact" name="contact" /></td>	
    <td class="status"></td>
  </tr>
  <tr>
    <td class="label">Address</td>
    <td class="field"><input type="text" id="address" name="address" /></td>	
    <td class="status"></td>
  </tr>
  <tr>
    <td class="label">Contact Person</td> 
    <td class="field"><input type="text" id="contactperson" name="contactperson" /></td>	
    <td class="status"></td>
  </tr>
  <!--tr>
    <td class="label">Email</td> 
    <td class="field"><input type="text" id="email" name="email" /></td>	
    <td class="status"></td>
  </tr-->
  
  <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="ofedit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="ofdelete"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="ofadd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="ofsave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="ofclear"><span title='Clear'></span><div id="Clear_office">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="ofrefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
  </tr>
  
</table>
</form>		
<table id="OfficeGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="ofpager" class="scroll" style="text-align:center;"></div>
	