
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<style>
body {
	padding:150px 50px;
	font-family:"Lucida Grande","Lucida Sans Unicode","bitstream vera sans","trebuchet ms",verdana;
}

a:active {
  outline:none;
}

:focus {
  -moz-outline-style:none;
}
</style>

<script src="facefiles/jquery-1.3.2.min.js" type="text/javascript"></script>
<link href="facefiles/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="facefiles/facebox.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox() 
    })
</script>
<body>
<link rel="stylesheet" type="text/css" href="css/scrollable-horizontal.css" />
<link rel="stylesheet" type="text/css" href="css/scrollable-buttons.css" />	

<a class="prevPage browse left"></a>
<div class="scrollable" id=infinite>	
	<div class="items">
	
		<!-- 1-5 -->
		<img src="http://farm1.static.flickr.com/143/321464099_a7cfcb95cf_t.jpg" />
		<img src="http://farm4.static.flickr.com/3089/2796719087_c3ee89a730_t.jpg" />
		<img src="http://farm1.static.flickr.com/79/244441862_08ec9b6b49_t.jpg" />
		<img src="http://farm1.static.flickr.com/28/66523124_b468cf4978_t.jpg" />

		<img src="http://farm1.static.flickr.com/164/399223606_b875ddf797_t.jpg" />
		
		<!-- 5-10 -->
		<img src="http://farm1.static.flickr.com/163/399223609_db47d35b7c_t.jpg" />
		<img src="http://farm1.static.flickr.com/135/321464104_c010dbf34c_t.jpg" />
		<img src="http://farm1.static.flickr.com/40/117346184_9760f3aabc_t.jpg" />
		<img src="http://farm1.static.flickr.com/153/399232237_6928a527c1_t.jpg" />
		<img src="http://farm1.static.flickr.com/50/117346182_1fded507fa_t.jpg" />
		
		<!-- 10-15 -->
		<img src="http://farm4.static.flickr.com/3629/3323896446_3b87a8bf75_t.jpg" />

		<img src="http://farm4.static.flickr.com/3023/3323897466_e61624f6de_t.jpg" />
		<img src="http://farm4.static.flickr.com/3650/3323058611_d35c894fab_t.jpg" />
		<img src="http://farm4.static.flickr.com/3635/3323893254_3183671257_t.jpg" />
		<img src="http://farm4.static.flickr.com/3624/3323893148_8318838fbd_t.jpg" />
	</div>
	
</div>

<!-- "next page" action -->
<a class="nextPage browse right"></a>


<br clear="all" />

<!-- javascript coding -->
<script>
// What is $(document).ready ? See: http://flowplayer.org/tools/using.html#document_ready
$(document).ready(function() {

// initialize scrollable together with the circular plugin
$("#infinite").scrollable().circular();	
});
</script>



