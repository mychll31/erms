<?php session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/Employee.class.php");
include_once("class/DocDetail.class.php");
require_once("class/jqSajax.class.php");
require_once("class/IncomingList.class.php");
$objDocDetail = new DocDetail;
$objEmp = new Employee;
$file = new Employee;
$objList = new IncomingList;
$ajax=new jqSajax();
$ajax->export("Load","file->Load");
$ajax->export("_decode","file->_decode");
$ajax->export("getTargetFile","file->getTargetFile");
$ajax->export("getTargetFile","file->removeItem");
$ajax->export("checkifConfidential","file->checkifConfidential");
$ajax->export("fileExist","file->fileExist");
$ajax->export("checkNotification","file->checkNotification");

$ajax->processClientReq();

?>
<script language="javascript">
<?php $ajax->showJs(); ?>

</script>
<?php
$dateImplemented = $objDocDetail->getDateImplemented();
if($_REQUEST['mode']=="show")
{
	$officeCode = explode('~',$objEmp->getOfficeCode($objEmp->get('userID')));
	//echo "office=".$officeCode[0];
	//$objList->getManageFolderLink();
	
	$objEmp->displayNotification($_REQUEST['msg']);//for system message
	//$rsCount = $objEmp->getEmpDocList($officeCode[0]);
	//echo "rsCount=".count($rsCount);
	$addURLString = '#';
	$limit = 10;
	if($page == "") 
		$page = 0;
	else if($page<>"")
		$page = $_REQUEST['page'];
	if($_REQUEST['ascdesc']=="ASC")
		$ascdesc = "DESC";
	else
		$ascdesc = "ASC";		
  
	$rs = $objList->getPickUpList($officeCode[0],$page, $limit, $_REQUEST['order'], $ascdesc,$_REQUEST['t_strSearch'],$_REQUEST['operator']);
	$total_pages = $rs[0]['_total'];
	if(count($rs)) 
		{	
echo "<ul id='icons' class='ui-widget ui-helper-clearfix'>";
			if($rs[$t]['flag']=='1')
				$t_intFlag = "0";
			else
				$t_intFlag = "1";	
			echo "<li class='ui-state-default ui-corner-all' onClick='getEmpChk();'><span class='ui-icon ui-icon-flag' title='Flag' ></span></li>";

			echo "<li class='ui-state-default ui-corner-all' onClick='getEmpChkPriority();'><span class='ui-icon ui-icon-notice' title='Priority' ></span></li>";
			
			//echo "<li class='ui-state-default ui-corner-all' onClick='markAsRead();'><span class='ui-icon ui-icon-mail-open' title='Mark as Read' ></span></li>";			
				
			echo "<li class='ui-state-default ui-corner-all' onClick='doDeleteEmpDocDialog();'><span class='ui-icon ui-icon-trash' title='Delete' ></span></li>
		<li class='ui-state-default ui-corner-all' onClick='moveEmpDocCustomFolder();'><span class='ui-icon ui-icon-copy' title='Move Document' ></span></li>
		<li style='float:right'>
			<div style='float: right; margin-right: 30px;'>
			<div data-tip='This is an autocomplete search box'>
			<input type='textbox' class='search_icon' id='t_strSearchIncomingList' value='".$_REQUEST['t_strSearch']."'>
			<input type='hidden' id='txtsearchbox' value=''>
			</div>
			</td>			

		</li></ul>";			
echo '<div id="doc_listings">';			
echo '<form name="frmEmpDocList" id="frmEmpDocList" method="post"><div id="data"><table cellpadding="0" cellspacing="0" align="center" class="listings" width="99%">
		<tr class="listheader">
		<td width=1%><input type="checkbox" onClick=\'checkedAll("frmEmpDocList",this);\'></td>
		<td width=2%><span class="ui-icon ui-icon-flag"></td>
		<td width=2%><span class="ui-icon ui-icon-notice"></td>
		<td align="center" width=13%>ACTION REQUIRED</td>
		<td align="center" width=5%>SENDER</td>
		<td align="center" width=18%>DOCID</td>
		<td align="center">SUBJECT</td>
		<td align="center" width=5%>STATUS</td>
		<td align="center" width=10%>DOC TYPE</td>
		<td align="center" width=10%>DATE SENT</td>
		<td align="center" width=7%>RECIPIENT</td>
		</tr>';
	$i=0;	 

	for($t=0;$t<count($rs);$t++)
		{	
			
		//$rs2 = $objEmp->checkTrash($objEmp->get("userID"),$rs[$t]['historyId']);	
		//$rs3 = $objEmp->checkRestrictions($objEmp->get("office"),$objEmp->get("userID"),$rs[$t]['historyId']);
		 
	 
		$rsActionRequired = $objDocDetail->getActionRequiredDetails($rs[$t]['actionCodeId']);
		$strActionRequired = $rsActionRequired[0]['actionDesc'];
		if(1)//count($rs2) && $rs3<>0)		
		{				
		$intStatus = $objEmp->getDocumentStatus($rs[$t]['status']);
		$intDocType = $objEmp->getDocumentType($rs[$t]['documentTypeId']);
		$strSender = $objEmp->getSender($rs[$t]['historyId']);
		
		
		
		/*				$rs2 =  $objGen->getOfficeEmployee($objGen->get('userID'));
						$office =  $rs2[0]['officeCode'];
						if(trim($office)=="")
							$office = $objGen->get('office');
						if($objGen->get('userType')=="3"){
							$strOfficeName = "&nbsp; (".$objGen->getOfficeCode2($objGen->get('userID')).")";
						}
						else
							$strOfficeName = "&nbsp; (".$office.$group.")";	
		*/
		
		//$strOfficeName = $objEmp->getOfficeName($rs[$t]['recipientId'],$rs[$t]['officeSig'],$rs[$t]['status'],'agency');
		$strRecipient = "".$objEmp->getOfficeNamePickUp($rs[$t]['recipientId']);
		
		if ($strRecipient=="") $strRecipient = "".$objEmp->getOfficeName($rs[$t]['recipientId'],$rs[$t]['officeSig'],$rs[$t]['status'],'agency');		
		
		//echo " ".$strRecipient;
		
		//$intReadHistory = $objList->checkReadHistory($rs[$t]['documentId'],$objEmp->get('userID'));		
		$t_strBgcolor = $objEmp->altRowBgColor($i);
		$dateSent = explode(' ',$rs[$t]['dateSent']);
		
		if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
			$onclick = ($objList->checkDocReceivedStatusPickUp($rs[$t]['historyId']))?
				'onClick="getData(\'showIncoming.php?mode=view&id='.$rs[$t]["documentId"].'&sender='.$strSender.'&src=receive\',\'pickup\');" title="SUBJECT: '.$rs[$t]['subject'].'"':
				'onClick=\'doReceiveDocument("'.$rs[$t]['documentId'].'","'.$rs[$t]['historyId'].'","'.$rs[$t]['recipientId'].'","'.$strRecipient.'","'.$page.'");\'';			
			$onclick = ($rs[$t]['actionCodeId'] == '23' && !$objList->checkDocReceivedStatusPickUp($rs[$t]['historyId']))?
				//	'onClick="alert(\' 1 For pickup documents can only be received through RMS KIOSK \'); return false;"'
				  'onClick=\'doReceiveDocument("'.$rs[$t]['documentId'].'","'.$rs[$t]['historyId'].'","'.$rs[$t]['recipientId'].'","'.$strRecipient.'","'.$page.'");\''
				    :$onclick;
		}
		else{
			$onclick = ($rs[$t]['receivedBy']<>NULL)?
				'onClick="getData(\'showIncoming.php?mode=view&id='.$rs[$t]["documentId"].'&sender='.$strSender.'&src=receive\',\'doclist\');" title="SUBJECT: '.$rs[$t]['subject'].'"':
				'onClick=\'doReceiveDocument("'.$rs[$t]['documentId'].'","'.$rs[$t]['historyId'].'","'.$rs[$t]['recipientId'].'","'.$strRecipient.'","'.$page.'");\'';			
			$onclick = ($rs[$t]['actionCodeId'] == '23' && $rs[$t]['receivedBy']==NULL)?
//					'onClick="alert(\'2 For pickup documents can only be received through RMS KIOSK \'); return false;"'
	'onClick=\'doReceiveDocument("'.$rs[$t]['documentId'].'","'.$rs[$t]['historyId'].'","'.$rs[$t]['recipientId'].'","'.$strRecipient.'","'.$page.'");\''
				    :$onclick;
		}
		
		
		$intReadHistory = $objList->checkReadHistory($rs[$t]['documentId'],$objEmp->get('userID'));
/*		if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
  		   $t_strClass = (!$objList->checkDocReceivedStatus($rs[$t]['historyId']))?"unread":"data";
		} 
		else $t_strClass = ($rs[$t]['receivedBy']==NULL)?"unread":"data";		
*/		
			if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
			   $t_strClass = (!$objList->checkDocReceivedStatusPickUp($rs[$t]['historyId']))?"unread":"data";
			}
			else 
			{$t_strClass = (count($rs[$t]['receivedBy'])==0 || count($intReadHistory)==0)?"unread":"data";
			}
		
		
		
		//echo "=>".$rs[$t]['receivedBy'].$rs[$t]['dateReceived'];
		echo '<tr class="'.$t_strClass.'" onMouseOver="this.bgColor=\'#FFFFCC\'; this.style.cursor=\'pointer\';" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'">';
		echo '<td><input type="checkbox" name="chk" id="chk" value="'.$rs[$t]['historyId'].'"><input type="hidden" name="t_strDocId'.$t.'" id="t_strDocId'.$t.'" value="'.$rs[$t]['documentId'].'"></td>';
		//falgging of documents
		if($rs[$t]['flag']=='1')
			echo "<td><div id='flag".$t."'><span class='ui-icon ui-icon-flag' title='Flag' ></span></div></td>";
		else
			echo "<td><div id='flag".$t."'><span>&nbsp;</span>&nbsp;</div></td>";		
		//priority of documents
		if($rs[$t]['priority']=='1')
			echo "<td><div id='priority".$t."'><span class='ui-icon ui-icon-notice' title='Priority' ></span></div></td>";
		else
			echo "<td><div id='priority".$t."'><span>&nbsp;</span>&nbsp;</div></td>";
//		$t_dtmDate = $rs[$t]['documentDate']=="0000-00-00"?"-":$rs[$t]['documentDate'];		
//		.$objList->printDateFormat(substr($rs[$t]['dateSent'],0,10)
		$t_dtmDate = $rs[$t]['dateSent']=="0000-00-00"?"-":$objList->printDateFormat(substr($rs[$t]['dateSent'],0,10));		
		echo	'<td '.$onclick.'>&nbsp;'.$strActionRequired.'</td>
				<td style="text-align:center;" '.$onclick.'>'.$strSender.'</td>
				<td '.$onclick.'><strong>&nbsp;'.$rs[$t]['documentId'].'</strong></td>
				<td '.$onclick.'>'.strtoupper($objList->limitText($rs[$t]['subject'],30)).'</td>
				<td style="text-align:center;" '.$onclick.'>'.$intStatus.'</td>
				<td style="text-align:center;" '.$onclick.'>'.$intDocType.'</td>
				<td style="text-align:center;" '.$onclick.'>'.$t_dtmDate.'</td>
				<td style="text-align:center;" '.$onclick.'>'.$strRecipient.'</td>';
		echo '</tr>';
		
		$i++;
		}	
		
			
		} 
echo 	'</table></div></form><br>';
echo $objList->showPagination($addURLString, $page, $total_pages, $limit,"pickup",'showIncomingAgency.php?mode=show');		

}		
	else
		echo "<br><br>No records found<br><br>";
		echo '</div>';

}
if($_REQUEST['mode']=="delete_true")
{
	$rs2 = $objEmp->trashIncDocument($objEmp->get("userID"),$_REQUEST['hid']);	
	echo "<script language='javascript'>
				getData('showIncomingAgency.php?mode=show','pickup');
				jQuery.facebox('Document Moved to Trash');
		  </script>";	
} 

if($_REQUEST['mode']=="receive")
{
	$t_intHistoryId = $_REQUEST['hid'];
	$strStatus = $objList->getHistoryStatus($t_intHistoryId);
	$strPageLink = $objList->getStatusPageLink($strStatus); 
	//$strPageLink = 'showIntraOffice.php';
	//echo $strStatus."/".$strPageLink."/".$t_intHistoryId;
//	receiveDocumentPickUp($t_strDocID,$t_strReceivedBy,$t_strHistoryID,$t_strRecipientID,$t_blnMode=''
//getData('showIncomingAgency.php?mode=receive&id='+docID+'&hid='+hID+'&t_strReceivedBy='+rb+'&senderId='+sID+'&src='+src+'&receive='+receive+'&dateSent='+dateSent+'&rid='+rID,'pickup');
	
	$test = $_REQUEST['page'];
	$rs = $objList->receiveDocumentPickUp($_REQUEST['id'],$_REQUEST['t_strReceivedBy'],$t_intHistoryId, $_REQUEST['rid']);	
   echo "<script language='javascript'>
			getData('showIncomingAgency.php?mode=show&page=$test','pickup');
		</script>";	
		//getPageData('showIncomingAgency.php?mode=show','pickup')
		
	//added as part of realtime-notification (hack121113)
   //exec("holler http://erms.dost.gov.ph:1337 log '".$_REQUEST['t_strReceivedBy']." already received your document ".$_REQUEST['id']."~".$_REQUEST['senderId']."' ");
		
}


if($_REQUEST['mode']=="move")
{
	$t_intID = $_REQUEST['id'];
	$t_intFolderID = $_REQUEST['fid'];
	$arr = explode(';',$t_intID);
	if(count($arr)>1) {
		for($i=0;$i<count($arr);$i++)
			$rs = $objEmp->moveDocument("",$t_intFolderID,$arr[$i]);	 
	}
	else
		$rs = $objEmp->moveDocument("",$t_intFolderID,$t_intID);		
			
	$t_strFolderName = $objList->getFolderName($_REQUEST['fid']);	
	echo "<script language='javascript'>getData('showIncomingAgency.php?mode=show&id=".$_REQUEST['id']."&msg=document moved to $t_strFolderName','pickup');</script>";
}

if($_REQUEST['mode']=="flag")
{
	$rs = $objList->flagDocument($_REQUEST['hid']);
	
	if(count($rs))
		echo "<script language='javascript'>getData('showIncomingAgency.php?mode=show','pickup');</script>";	
}

if($_REQUEST['mode']=="priority")
{
	$rs = $objList->priorDocument($_REQUEST['hid'], $_REQUEST['t_intPriorStatus']);
	echo "<script language='javascript'>getData('showIncomingAgency.php?mode=show&action=priority','pickup');</script>";	
}

if($_REQUEST['mode']=="mark")
{
	if($_REQUEST['mark']=='0')
		$rs = $objList->markUnread($_REQUEST['id'],$objList->get('userID'));
	else
		$rs = $objList->markRead($_REQUEST['id'],$objList->get('userID'));
	
}

?>
<script type="text/javascript">
$(function() {
	$('a[rel*=facebox]').facebox({
	loading_image : '/facebox/loading.gif',
	close_image   : '/facebox/closelabel.gif'
	});
	$(document).ready(function(){
  		var keystring = '';
	    $("#t_strSearchIncomingList").live('keyup', function(event){
	    	var keyword=($(this).val() + String.fromCharCode(event.which));
	    	if(event.which == 13){
	    		$('#txtsearchbox').val(keyword);
	    		var operator = (keyword.search("OR") >=0)?'OR':
					   (keyword.charAt(0) == '"')?'CONTAINS':
					   //(keyword.search("-") >= 0)?'EXCEPT':
					   (keyword.search(/\*/g) >= 0)?'DOCTYPE':'AND';
			$( "#doc_listings" ).load( "showIncomingAgency.php #doc_listings",{ operator:operator,mode:'show',t_strSearch: $('#txtsearchbox').val()}, 
			function(data) {
			  console.log(keyword);
			});
	    	}
        });
	});
	// $('#t_strSearchIncomingList').keyup(function(event){
	// 	var keyword=($(this).val() + String.fromCharCode(event.which));
	// 	var operator = (keyword.search("OR") >=0)?'OR':
	// 				   (keyword.charAt(0) == '"')?'CONTAINS':
	// 				   //(keyword.search("-") >= 0)?'EXCEPT':
	// 				   (keyword.search(/\*/g) >= 0)?'DOCTYPE':'AND';
			
	// 		$( "#doc_listings" ).load( "showIncomingAgency.php #doc_listings",{ operator:operator,mode:'show',t_strSearch:$('#t_strSearchIncomingList').val() }, 
	// 		function(data) {
	// 		  console.log(data);
	// 		});
	// });
	
});

	
function doDeleteEmpDocDialog()
{
	var checked = false;
	for(var i=0; i < document.forms['frmEmpDocList'].chk.length; i++){
	  if(document.forms['frmEmpDocList'].chk[i].checked) checked=true;
	}
	if(!checked){ alert('Pls. check row to be deleted'); return false; }

	 var msg = confirm("Are you sure you want to delete this Document?");
	 if(msg){
		$('#frmEmpDocList tr:not(.listheader)').each(function(){
			if($(this).find("td:first-child input[type='checkbox']").is(':checked')){
			  var hid = $(this).find("td:first-child input[type='checkbox']").val();
			  var id = $(this).find("td:first-child input[type='hidden']").val();
			  getData('showIncomingAgency.php?mode=delete_true&hid='+hid+'&id='+id,'pickup');
			}
		})
	 }
}

function moveEmpDocCustomFolder()
{
	var empdocs = '';
	if(document.forms['frmEmpDocList'].chk.length == undefined)
		{
		if(document.forms['frmEmpDocList'].chk.checked)	
			empdocs = document.getElementById('t_strDocId0').value;
		}
	else
		{
		for(var i=0; i < document.forms['frmEmpDocList'].chk.length; i++){
			if(document.forms['frmEmpDocList'].chk[i].checked)
				{
				var id = 't_strDocId'+i;
				//if(i>1)
					empdocs = empdocs + document.getElementById(id).value + ';';
				//else
					//empdocs = document.getElementById(id).value;	
				}	
		}
	}
	
if(empdocs!="")
	{	
	//document.getElementById('docid').innerHTML=empdocs;
	//document.getElementById('moveid').innerHTML='".$rs[$t]['documentId']."'; 
	//document.getElementById('fid').innerHTML=document.getElementById('t_intFolderID').value;
	//$('#movedialog2').dialog('open');
	var url = 'showMoveDialog.php?mode=move&docid='+empdocs+'&url=showIncomingAgency';
	jQuery.facebox({ ajax: url });
	}
}

function doReceiveDocument(docid,hid,rid,srid,page)
	{
	var url = 'showReceiveDialog2.php?mode=receive&docid='+docid+'&hid='+hid+'&rid='+rid+'&srid='+srid+'&page='+page;
	jQuery.facebox({ ajax: url });	
	}
	
function getEmpChk() {
if(document.forms['frmEmpDocList'].chk.length == undefined)
	{
	if(document.forms['frmEmpDocList'].chk.checked)
		{
		getData('showIncomingAgency.php?mode=flag&hid='+document.forms['frmEmpDocList'].chk.value,'flag0');
		}
	}
else
	{
	for(var i=0; i < document.forms['frmEmpDocList'].chk.length; i++){
			if(document.forms['frmEmpDocList'].chk[i].checked)
				getData('showIncomingAgency.php?mode=flag&hid='+document.forms['frmEmpDocList'].chk[i].value,'flag'+i);
		}	
	}
}

function getEmpChkPriority() {	

if(document.forms['frmEmpDocList'].chk.length == undefined)
	{
	if(document.forms['frmEmpDocList'].chk.checked)
		getData('showIncomingAgency.php?mode=priority&hid='+document.forms['frmEmpDocList'].chk.value,'priority0');
	}
else
	{
	for(var i=0; i < document.forms['frmEmpDocList'].chk.length; i++){
			if(document.forms['frmEmpDocList'].chk[i].checked)
				getData('showIncomingAgency.php?mode=priority&hid='+document.forms['frmEmpDocList'].chk[i].value,'priority'+i);
		}	
	}
//getData('showIncomingList.php?mode=show&action=priority','doclist');
}	

	function setNotifyEmail()
	{
	var chk=$('#chkNotifyEmail:checked').val();
	$.ajax ({
			type: "POST",
			url: 'getNotifyEmailData.php?action=setnotifyemail&chk='+chk,					
			cache: false,
			success: function(html)
				{				
				//alert(html);					
				}
		});
	}
</script>