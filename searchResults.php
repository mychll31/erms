<? session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>ERMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/doclist_ajax.js"></script>
<? include("includes.php");?>	
</head>

<? 
include("class/Search.class.php");
$objSearch = new Search;

?>
<body>

<? include("templates/header_logo.php");?>
<table width="877" cellpadding="0" cellspacing="0" align="center" class="textbody">
  <!--DWLayoutTable-->
  <tr> 
    <td height="20" colspan="2" align="right" valign="middle"> 
      <? include("templates/search.php");?>
    </td>
  </tr>
  <tr> 
    <td width="109" height="198" align="center" valign="top"> 
      <? include("templates/menu.php");?>
    </td>
    <td width="764" valign="top">
	<div id="tabs">
	<ul>
	<li><a href="#Search">Search Results</a></li>
	</ul>
	<!-- 	*************		Search Results		*****************	-->
		<div id="searchResults" align="center">
		<script language="javascript">getData('showSearch.php?mode=doSearch&t_strText=<? echo $_POST['t_strText'];?>','searchResults');</script>
		</div>
		<div id="inc" align="center"></div>
		<div id="outg" align="center"></div>
		<div id="intra" align="center"></div>
	</div>	
	&nbsp;
	</td>
  </tr>
</table>
<? include("includes/search_include.php");?>
<? include("templates/footer.php");?>
</body>
</html>
