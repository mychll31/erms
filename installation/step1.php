<? @session_start();
$classSC="stephighlight";
$arrError=array();
$arrRecommended=array();
$arrsupported_module=get_loaded_extensions();
$_SESSION["allowemail"]=0;
//mysql (34)
if (in_array("mysql",$arrsupported_module,false)==false) $arrError[]="MYSQL Support";
//xml (1)
if(in_array("xml",$arrsupported_module,false)==false) $arrError[]="XML Support";
//session (12)
if(in_array("session",$arrsupported_module,false)==false) $arrError[]="Session Support";
//ftp (20)
if(in_array("ftp",$arrsupported_module,false)==false)$arrError[]="FTP Support";

$arrCWD=explode("/",getcwd());
if(count($arrCWD)==1)       //checking for windows directory
{ 
$arrCWD=explode("\\",getcwd());
$ostype="windows";
}
for($i=0;$i<count($arrCWD)-1;$i++)
{
if($ostype=="windows")$tmpCWD.=$arrCWD[$i]."\\";
else $tmpCWD.=$arrCWD[$i]."/";
}
$tmpCWD.="UPLOAD";
$uploadfolderoctal=substr(sprintf('%o', fileperms($tmpCWD)), -4);
if($uploadfolderoctal!="1777" && $uploadfolderoctal!="0777") $arrError[]="UPLOAD folder not writable(".$uploadfolderoctal.")";

/*
if($ostype!="windows"){
$tempfolderoctal= substr(sprintf('%o', fileperms('/tmp')), -4);
if($tempfolderoctal!="1777") $arrError[]="Temp folder not writable";
}*/

$tmpfile = tempnam("dummy","");
$path = dirname($tmpfile);
//echo $path;
unlink($tmpfile);

$tempfolderoctal= substr(sprintf('%o', fileperms($path)), -4);
if($tempfolderoctal!="1777" && $tempfolderoctal!="0777") $arrError[]="Temp folder not writable(".$tempfolderoctal.")";



if(get_cfg_var('display_errors')==1) $arrRecommended[]="Set display_errors to 0";
if(get_cfg_var('register_globals')==0)$arrRecommended[]="Set register_globals to 1";
if(get_cfg_var('session.auto_start')==1)$arrRecommended[]="Set session.auto_start to 0";
if(get_cfg_var('file_uploads')==0) $arrRecommended[]="Set file_uploads to 1";
if(get_cfg_var('safe_mode')==1) $arrRecommended[]="Set safe_mode to 0";
if(get_cfg_var('magic_quotes_runtime')==1) $arrRecommended[]="Set magic_quotes_runtime to 0";
$maxfilesize=get_cfg_var('upload_max_filesize');
if((int)$maxfilesize<2) $arrRecommended[]="Set upload_max_filesize to 2M or higher";

//display_errors=0
//register_globals=1
//session.auto_start=0
//file_uploads=1
//safe_mode=0
//magic_quotes_runtime=0
//upload_max_filesize>=2M
//UPLOAD folder to chmod 01777

//max_input_time=60
//memory_limit=32M
//max_execution_time=30
//post_max_size=8M
//SMTP (mail do not check)

$cError=count($arrError);
$cRecommended=count($arrRecommended);
if($cError==0 && $cRecommended==0)
{
	?>
	<script type="text/javascript" language="javascript">
	setTimeout("getData(\"step2.php?\",\"contentbox\")",1000);
	$("#tryagain").hide();
	</script>
	<?

}
else{
?>
	<script type="text/javascript" language="javascript">
	$("#tryagain").click(function(){
	getData("step1.php?","contentbox");
	});
	$("#nextbutton").click(function(){
	getData("step2.php?","contentbox");
	});
	
	$("#cancel").click(function(){
	if (typeof window.home == 'function'){ // The rest of the world
        window.home();
      } else if (document.all) { // For IE 
        window.location.href = "about:blank";
	  }
	  else{window.location = "about:home";}
	});
	</script>

<?
}
?>
<div class="leftcolumn"><? include("leftmenu.php");?>		</div>
<div class="rightcolumn"><div class="configmaincontainer">
<div class="steptitle">System Check</div>
<div class="configtext"><? if($cError==0 && $cRecommended==0){?>
Checking please wait...&nbsp;&nbsp;<img src="images/loading.gif" width="25" height="25">
<? } else {?>
<table width="500">
	<? if($cError>0){ ?>
	<tr>
		<td valign="top"><img src="images/wrongx_icon.png" width="20" height="20" /> </td>
		<td>Installation cannot continue until the following configuration(s) are fixed:<br /><br />
			<?
			for($i=0;$i<$cError;$i++)
				{
				echo "&nbsp;&nbsp;&nbsp;* ".$arrError[$i]."<br/>";
				}
			?>
			<br />Please fix the configuration(s) above and click "Try Again" to continue the installation. Click Cancel to exit Setup.
		</td>
	</tr>
	<? }
	if($cRecommended>0){	
	?>
	<tr><td colspan="2">&nbsp;</td></tr>

	<tr>
	<td valign="top"><img src="images/warning.png" width="20" height="20" /></td>
	<td>These setting(s) should be fixed in order for ERMS to run smoothly:<br /><br />
		<?
			for($i=0;$i<$cRecommended;$i++)
				{
				echo "&nbsp;&nbsp;&nbsp;* ".$arrRecommended[$i]."<br/>";
				}
			?>
			<br /> Note: ERMS will still run even if this setting(s) is/are not fixed
	</td>
	</tr>
	<? }?>
</table>
<? }?>
</div>
</div><div class="buttoncontainer">
<table border="0" cellpadding="0" cellspacing="0"> 
	<tr>
		<td><button id="cancel">&nbsp;Cancel&nbsp;</button></td>
		<? 
		if($cError==0 && $cRecommended>0){
		?>
		<td align="center"><button id="nextbutton">&nbsp;Continue Installation&nbsp;</button></td>
		<?
		}
		?>
		<td align="right"><button id="tryagain">&nbsp;Try Again&nbsp;</button></td>
	</tr></table>
</div>
</div>