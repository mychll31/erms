<?  @session_start();
	$classSC="stepfin";
	$classLA="stepfin";
	$classOP="stephighlight";
	
	if($_SESSION["Rgroup"]=="group1") $group1="selected=\"selected\"";
	if($_SESSION["Rgroup"]=="group2") $group2="selected=\"selected\"";
	if($_SESSION["Rgroup"]=="group3") $group3="selected=\"selected\"";
	if($_SESSION["Rgroup"]=="group4") $group4="selected=\"selected\"";
	if($_SESSION["Rgroup"]=="group5") $group5="selected=\"selected\"";
?>

<script language="javascript">
$("#addip").click(function(){

objcode=$("#Ocode").val();
objip=$("#Oip").val();
Sname=objcode+objip;
divSname="div"+Sname.replace(/\./g,"_");
Sval=objcode+":"+objip;
err=0;
if(objcode=="")
{
$("#imgocode").show();
err++;
}
if(objip=="")
{
$("#imgoip").show();
err++;
}

if(err>0) return;

objcheckboxes=$(":checkbox");
errortrap=true;
$.each(objcheckboxes,function(index,value){
var tmpocode=value.value.split(":");
if(tmpocode[0]==objcode)
	{
	alert("Office code already in the list... please uncheck to delete the office");
	errortrap=false;
	return false;
	}
});
if(errortrap===false) return;
//alert(objcode+objip);
//$("#fileserverlist").add("<input name=\""+Sname+"\" type=\"checkbox\" value=\"value\" checked><label for'"+Sname+"'>"+objcode+" : "+objip+"</label>").html("#fileserverlist");
$("#fileserverlist").append("<p id='"+divSname+"' class='serverlist'><input name=\""+"chkbox[]"+"\" type=\"checkbox\" value=\""+Sval+"\" checked  id='"+Sname+"'><label for='"+Sname+"'>"+objcode+" : "+objip+"</label><br></p>");
clicka();
$("#imgocode").hide();
$("#imgoip").hide();
$("#Ocode , #Oip").val("");
$("#Ocode").focus();
});


function clicka(){
$(".serverlist").click(function(){
$(".serverlist").remove("#"+this.id);
});
}

$("#imgocode").hide();
$("#imgoip").hide();

$("#backbutton").click(function(){
getData("step2.php?","contentbox");
});

$('#Ocode,#Oip').keypress(function(e){
      if(e.which == 13){
	  $('#addip').click();
	}
});

$("#wee").click(function(){ // Hack for bug when enter button is pressed on any text field
});							// causing it to click the addip button (first button seen)
$("#wee").hide();

$("#nextbutton").click(function(){
get(document.getElementById('frmOptions'),'contentbox','step4.php');
});
clicka();
	$("#cancel").click(function(){
	if (typeof window.home == 'function'){ // The rest of the world
        window.home();
      } else if (document.all) { // For IE 
        window.location.href = "about:blank";
	  }
	  else{window.location = "about:home";}
	});
</script>

<div class="leftcolumn"><? include("leftmenu.php");?></div>
<form id="frmOptions" onSubmit="return false"><div class="rightcolumn"><div class="configmaincontainer"><div class="steptitle">Options</div>
<button id="wee">weee</button> <!-- Hack for bug when enter button is pressed on any text field-->
<div class="configtext" style="overflow:scroll;">
<table class="optgroup">
<tr><th colspan="3">HRMIS CONFIGURATION</th></tr>
<tr>
	<td width="160px">HRMIS database:</td>
	<td><input type="text" id="Hdatabase" name="Hdatabase" value="<? echo $_SESSION["Hdatabase"];?>"></td>
	<td rowspan="5" class="instruction" align="left">ERMS needs to connect to HRMIS. Input here necessary data for ERMS to connect to HRMIS</td>
</tr>
<tr>
	<td>HRMIS database server:</td>
	<td><input type="text" id="Hserver" name="Hserver" value="<? echo $_SESSION["Hserver"];?>"></td>
</tr>
<tr>
	<td>HRMIS database username:</td>
	<td><input type="text" id="Huname" name="Huname" value="<? echo $_SESSION["Huname"];?>"></td>
</tr>

<tr>
	<td>HRMIS database password:</td>
	<td><input type="text" id="Hpass" name="Hpass" value="<? echo $_SESSION["Hpass"];?>"></td>
</tr>
<tr>
	<td>HRMIS url:</td>
	<td><input type="text" id="Hurl" name="Hurl" value="<? echo $_SESSION["Hurl"];?>"></td>
</tr>
</table>

<table class="optgroup">
<tr><th colspan="3">AGENCY DETAILS</th></tr>
<tr>
	<td width="160px">Agency Code:</td>
	<td><input type="text" id="Acode" name="Acode" value="<? echo $_SESSION["Acode"];?>"></td>
	<td rowspan="4" class="instruction" align="left">Input here details of the Agency you belong. You should also define the Records admin code for you to be able to create an admin account</td>
</tr>
<tr>
	<td>Agency Name:</td>
	<td><input type="text" id="Aname" name="Aname" value="<? echo $_SESSION["Aname"];?>"></td>
</tr>
<tr>
	<td>Records Admin Code:</td>
	<td><input type="text" id="RAcode" name="RAcode" value="<? echo $_SESSION["RAcode"];?>"></td>
</tr>
<tr>
	<td>Records Admin Group:</td>
	<td><select id="Rgroup" name="Rgroup">
			<option value="group1" <? echo $group1;?>>Group 1</option>
			<option value="group2" <? echo $group2;?>>Group 2</option>
			<option value="group3" <? echo $group3;?>>Group 3</option>
			<option value="group4" <? echo $group4;?>>Group 4</option>
			<option value="group5" <? echo $group5;?>>Group 5</option>
			</select>
	</td>
</tr>
</table>
<table class="optgroup">
	<tr><th colspan="3">Office Level Structure</th></tr>
	<tr>
		<td width="160px">Level 1:</td>
		<td><input type="text" id="Group1" name="Group1" value="<? echo $_SESSION["Group1"];?>"></td>
		<td rowspan="5" class="instruction" align="left">Specify your agency's organizational structure, having Level 1 as the highest office.
e.g. Level 1(Executive Office), Level 2 (Service), etc.</td>
	</tr>
	<tr>
		<td width="160px">Level 2:</td>
		<td><input type="text" id="Group2" name="Group2" value="<? echo $_SESSION["Group2"];?>"></td>
	</tr>
	<tr>
		<td width="160px">Level 3:</td>
		<td><input type="text" id="Group3" name="Group3" value="<? echo $_SESSION["Group3"];?>"></td>
	</tr>
	<tr>
		<td width="160px">Level 4:</td>
		<td><input type="text" id="Group4" name="Group4" value="<? echo $_SESSION["Group4"];?>"></td>
	</tr>
	<tr>
		<td width="160px">Level 5:</td>
		<td><input type="text" id="Group5" name="Group5" value="<? echo $_SESSION["Group5"];?>"></td>
	</tr>
</table>
<table class="optgroup">
<tr><th colspan="3">FILE SERVER CONFIGURATION</th></tr>
<tr>
	<td width="160px">Office Code</td>
	<td><input type="text" id="Ocode" name="Ocode"> <img src="images/wrongx_icon.png" id="imgocode" height="20" width="20" style="vertical-align:bottom"></td>
	<td rowspan="2" class="instruction" align="left">Specify here the Office code and it's file server IP. ERMS saves uploaded documents to these file server per office.</td>
</tr>
<tr  >
	<td width="160px">Office IP</td>
	<td valign="bottom"><input type="text" id="Oip" name="Oip"> <img src="images/wrongx_icon.png" id="imgoip" height="20" width="20" vspace="0" style="vertical-align:bottom"></td>
</tr>
<tr>
	<td colspan="3" style="padding-left:240px"><button id="addip">Add</button></td>
</tr>
<tr><td colspan="3"><b>File Servers:</b></td></tr>
<tr><td colspan="3"><fieldset><div id="fileserverlist"><? 
foreach($_SESSION["chkbox"] as $key=>$val)
{
$arrVal=explode(":",$val);
$oCode=$arrVal[0];
$oVal=$arrVal[1];
$divSname="div".$oCode.str_replace(".","_",$oVal);
echo "<p id='".$divSname."' class='serverlist'><input name=\""."chkbox[]"."\" type=\"checkbox\" value=\"".$oCode.":".$oVal."\" checked  id='".$oCode.$oVal."'><label for='".$oCode.$oVal."'>".$oCode." : ".$oVal."</label><br></p>";
//echo $val."<br>";
}
?>
</div></fieldset></td></tr>
</table>
</div></div><div class="buttoncontainer">
<table border="0" cellpadding="0" cellspacing="0"> 
	<tr>
		<td><button id="cancel">&nbsp;Cancel&nbsp;</button></td>
		<td align="right"><button id="backbutton">&nbsp;&laquo; Back&nbsp;</button>&nbsp;&nbsp;&nbsp;<button id="nextbutton">&nbsp;Next &raquo;&nbsp;</button></td>
	</tr>
</table>
</div>
</div></form>