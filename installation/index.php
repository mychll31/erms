<? @session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ERMS Web Installer</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<!--[if IE]>
<style>
#contentbox{
width:798px;
}
</style>
<![endif]-->
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript">
$(function() {
		$( ".mainbox" ).draggable({ handle: '.header' });
		//$( ".mainbox" ).draggable();
		$(".getstarted a").click(function(){
			getData("step1.php?","contentbox");
			return false;
		});
	});
</script>
</head>
<div class="mainbox">
	<div class="header">Electronic Records Management Setup</div>
	<div id="contentbox">
		<div style="padding-left:20px; padding-top:20px;"><img src="images/ERMS logo.png" /></div>
		<br />
		<div align="center"><img src="images/laptop.png" /></div>
		<br />
		<div align="center"><img src="images/welcome.png" /><br /><br />
		<span class="installertext">Welcome to the installation of ERMS, your friend in record keeping.<br /> This assistant will help you through the installation process.</span>
		<br /><br />
		<div class="getstarted"><a href="#">Get Started</a></div>
		</div>
	</div>
	</div>
</div>
<body>
</body>
</html>
