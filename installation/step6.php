<? @session_start();
   $classSC="stepfin";
   $classLA="stepfin";
   $classOP="stepfin";
   $classIL="stepfin";
   $classSU="stepfin";
   $classIN="stephighlight";
  $configFile="../class/ERMS_define.php";
  $configScript="<? \n/**************** AGENCY DETAILS ****************/\n";
  $configScript.="define(\"AGENCY_CODE\",\"".$_SESSION["Acode"]."\");\n";
  $configScript.="define(\"AGENCY_NAME\",\"".$_SESSION["Aname"]."\");\n";
  $configScript.="define(\"RECORDS_ADMIN_VALUE\",\"".$_SESSION["RAcode"]."\");\n";
  if($_SESSION["Rgroup"]=="group1")
  {
	$configScript.="define(\"RECORDS_ADMIN_TABLE\",\"tblGroup1\");\n";
	$configScript.="define(\"RECORDS_ADMIN_FIELD\",\"group1Custodian\");\n";
	$configScript.="define(\"RECORDS_ADMIN_CODE_FIELD\",\"group1Code\");\n";
  }
  elseif($_SESSION["Rgroup"]=="group2")
  {
	$configScript.="define(\"RECORDS_ADMIN_TABLE\",\"tblGroup2\");\n";
	$configScript.="define(\"RECORDS_ADMIN_FIELD\",\"group2Custodian\");\n";
	$configScript.="define(\"RECORDS_ADMIN_CODE_FIELD\",\"group2Code\");\n";
  }
  elseif($_SESSION["Rgroup"]=="group3")
  {
	$configScript.="define(\"RECORDS_ADMIN_TABLE\",\"tblGroup3\");\n";
	$configScript.="define(\"RECORDS_ADMIN_FIELD\",\"group3Custodian\");\n";
	$configScript.="define(\"RECORDS_ADMIN_CODE_FIELD\",\"group3Code\");\n";
  }
  elseif($_SESSION["Rgroup"]=="group4")
  {
	$configScript.="define(\"RECORDS_ADMIN_TABLE\",\"tblGroup4\");\n";
	$configScript.="define(\"RECORDS_ADMIN_FIELD\",\"group4Custodian\");\n";
	$configScript.="define(\"RECORDS_ADMIN_CODE_FIELD\",\"group4Code\");\n";
  }
  elseif($_SESSION["Rgroup"]=="group5")
  {
	$configScript.="define(\"RECORDS_ADMIN_TABLE\",\"tblGroup5\");\n";
	$configScript.="define(\"RECORDS_ADMIN_FIELD\",\"group5Custodian\");\n";
	$configScript.="define(\"RECORDS_ADMIN_CODE_FIELD\",\"group5Code\");\n";
  }
 
 $configScript.="\n define(\"Group1\",\"".$_SESSION["Group1"]."\");\n";
 $configScript.="define(\"Group2\",\"".$_SESSION["Group2"]."\");\n";
 $configScript.="define(\"Group3\",\"".$_SESSION["Group3"]."\");\n";
 $configScript.="define(\"Group4\",\"".$_SESSION["Group4"]."\");\n";
 $configScript.="define(\"Group5\",\"".$_SESSION["Group5"]."\");\n";
 
 $configScript.="\n/**************** HRMIS Configuration ****************/\n";
 $configScript.="define(\"dbhrmis\",\"".$_SESSION["Hdatabase"]."\");\n";
 $configScript.="define(\"hrmis_server\",\"".$_SESSION["Hserver"]."\");\n";
 $configScript.="define(\"hrmis_user\",\"".$_SESSION["Huname"]."\");\n";
 $configScript.="define(\"hrmis_pass\",\"".$_SESSION["Hpass"]."\");\n";
 $configScript.="\$HRMISlink=\"".$_SESSION["Hurl"]."\";\n";
 $configScript.="\n/**************** File Servers ****************/\n";
 foreach ($_SESSION["chkbox"] as $key =>$val)
 {
 $arVal=explode(":",$val);
 $configScript.="define(\"".$arVal[0]."\",\"".$arVal[1]."\");\n";
 }
 $configScript.="\n/**************** ERMS Configuration ****************/\n";
 $configScript.="define(\"dberms\",\"".$_SESSION["Edatabase"]."\");\n";
 $configScript.="define(\"erms_user\",\"".$_SESSION["Euname"]."\");\n";
 $configScript.="define(\"erms_pass\",\"".$_SESSION["Epass"]."\");\n";
 $configScript.="define(\"erms_server\",\"". $_SESSION["Eserver"]."\");\n";
 $configScript.="define(\"ERMS_SERVER\",\$_SERVER['SERVER_ADDR']);\n";
 //$configScript.="define(\"ALLOWEMAIL\",". $_SESSION["allowemail"].");\n";
 $arCurrent=explode("/",getcwd());
 $strCurrent="";
 for($i=0;$i<count($arCurrent)-1;$i++)
 {
 $strCurrent.=$arCurrent[$i]."/";
 }
 $configScript.="define(\"LOCATION\",\"".$strCurrent."\");\n";
 $configScript.="define(\"HTTP\",\"http://\");\n";
 array_pop($arCurrent);
 array_pop($arCurrent);
 $strCurrent="";
 for($i=0;$i<count($arCurrent);$i++)
 {
 $strCurrent.=$arCurrent[$i]."/";
 }
 $configScript.="define(\"root\",\"".$strCurrent."\");\n";
 $configScript.="?>";
 
 
 if(is_writable($configFile)) 							//checking if Config.php exists and writable
{
$p2fConfig = fopen($configFile, 'w');
fwrite($p2fConfig, $configScript);
fclose($p2fConfig);
$unabletowrite=0;
}
else
{
session_register("configScript");
$_SESSION["configScript"]=$configScript;
$unabletowrite=1;
}

?>
<script type="text/javascript" language="javascript">
function installscript()
{
$(".configtext").html("Installing... ");
<? if($unabletowrite==1) 
{
?>
getData("step7.php?success=0","contentbox");
<? 
}
else{
?>
getData("step7.php?success=1","contentbox");
<? }?>
}
	$("#cancel").click(function(){
	if (typeof window.home == 'function'){ // The rest of the world
        window.home();
      } else if (document.all) { // For IE 
        window.location.href = "about:blank";
	  }
	  else{window.location = "about:home";}
	});
setTimeout("installscript()",1000);

</script>

<div class="leftcolumn"><? include("leftmenu.php");?>		</div>
<div class="rightcolumn">
	<div class="configmaincontainer">
		<div class="steptitle">Install</div>
		<div class="configtext">
		Creating install script please wait...&nbsp;&nbsp;<img src="images/loading.gif" width="25" height="25">
		<!--textarea style="height:300px; width:300px" rows="30"><? echo $configScript;?></textarea-->
		</div>
	</div>
	<div class="buttoncontainer">
	<table border="0" cellpadding="0" cellspacing="0"> 
		<tr>
			<td><button id="cancel">&nbsp;Cancel&nbsp;</button></td>
			<td align="right"></td>
		</tr>
	</table>
	</div>
</div>