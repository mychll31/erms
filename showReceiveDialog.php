<?php session_start(); ?>

<script language="javascript" type="text/javascript">
function doReceive()
	{
	var docID = $("#docid").val();
	var hID = $("#hid").val();
	var rb = $("#t_strReceivedBy").val();
	var sID = $("#senderId").val();
    var receive = $('#receive').val();
	var src = $('#src').val();   
	var dateSent = $('#datesent').val();   
	//alert(docID+'/'+hID+'/'+rb);
	if(rb!="")
		{
		getData('showIncomingList.php?mode=receive&actioncode=<?=$_GET["actioncode"]?>&id='+docID+'&hid='+hID+'&t_strReceivedBy='+rb+'&senderId='+sID+'&src='+src+'&receive='+receive+'&dateSent='+dateSent,'doclist');
		jQuery.facebox("Document Received Successfully");	
		}
	else
		alert('Please fill in Received By!');	
	}
</script>
<? 
include_once("class/General.class.php");
$objGen = new General;
$senderId = $objGen->getSender($_GET['hid'],TRUE);
$emp_name = $objGen->getEmpName($objGen->get('userID'));
$receiver = $emp_name;
$readonly = $_SESSION['userType']==3?"readonly":"";
if($_GET['mode']=="receive")
{
	echo '
		<table align=center class=listings width=100%>
			<tr class=listheader><td colspan=2 class=listheader style="text-align:left">&nbsp;Receive Document</td></tr>
			<tr><td colspan=2>&nbsp;</td></tr>
			<tr>
				<td align=right width=40%>Document ID  :&nbsp;</td>
				<td align=left>&nbsp;<input type="hidden" id="hid" value="'.$_GET['hid'].'">
				<input type="hidden" id="senderId" value="'.$senderId.'">
				<input type="hidden" id="docid" value="'.$_GET['docid'].'">'.$_GET['docid'].
				'<input type="hidden" id="receive" value="'.$_GET['receive'].'">
				<input type="hidden" id="src" value="'.$_GET['src'].'">
				<input type="hidden" id="datesent" value="'.$_GET['dateSent'].'"></td>				
			</tr>
			<tr>
				<td align=right width=40%>Recipient :&nbsp;</td>
				<td align=left>&nbsp;'.$_GET['rid'].'</td>
			</tr>
			<tr>
				<td align=right width=40%>Received By :&nbsp;</td>
				<td align=left>&nbsp;<input type="text" name="t_strReceivedBy" id="t_strReceivedBy" value="'.$receiver.'" '.$readonly.'></td>
			</tr>
			</table>
		<table class=listings border=0 align=center width=25>
			<tr onmouseover="this.style.cursor=\'pointer\'; this.bgColor=\'#FFFFCC\'; this.style.border=\'thin solid\';" onmouseout="this.bgColor=\'#FFFFFF\'; this.style.border=\'none\';">
				<td align=right width=5><span class="ui-icon ui-icon-pencil" title="Receive" onClick=\'doReceive();\'></span></td>
				<td align=left width=20 onClick=\'doReceive();\'>Receive</td>
			</tr>			
		</table>
		<table class=listings border=0 align=center width=80%>
			<tr>
				<td colspan=2 align=left style="color:red">Note: Check the "Update Action" tab for documents that require action/reply. If document was received but action is not made, the document will be considered "Unacted"</td>
			</tr>
		</table>
		
	';
//				Check the "Update Action" tab for documents that require action/reply. If document was received but actions are not made, the document will be considered "Unacted"
}
?>