<? 
@session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Report.class.php");
include_once("class/DocumentType.class.php");
$objGen = new General;
$objReport = new Report;
$objDocuType = new DocType;

?>
<script language="JavaScript">
	document.getElementById("sentdateFrom").disabled = true;
	document.getElementById("sentdateTo").disabled = true;
	document.getElementById("createddateFrom").disabled = true;
	document.getElementById("createddateTo").disabled = true;
	
	function radchange(val){
		if(val == 0){
			document.getElementById("recdateFrom").disabled = false;
			document.getElementById("recdateTo").disabled = false;
			document.getElementById("sentdateFrom").disabled = true;
			document.getElementById("sentdateTo").disabled = true;
			document.getElementById("createddateFrom").disabled = true;
			document.getElementById("createddateTo").disabled = true;
		}
		if(val == 1){
			document.getElementById("recdateFrom").disabled = true;
			document.getElementById("recdateTo").disabled = true;
			document.getElementById("sentdateFrom").disabled = false;
			document.getElementById("sentdateTo").disabled = false;
			document.getElementById("createddateFrom").disabled = true;
			document.getElementById("createddateTo").disabled = true;
		}
		if(val == 2){
			document.getElementById("recdateFrom").disabled = true;
			document.getElementById("recdateTo").disabled = true;
			document.getElementById("sentdateFrom").disabled = true;
			document.getElementById("sentdateTo").disabled = true;
			document.getElementById("createddateFrom").disabled = false;
			document.getElementById("createddateTo").disabled = false;
		}
		document.getElementById("radvalue").value = val;
	}
	function getReportVal(val)
	{
		document.getElementById("txtreport").value = val;
		if(val==2){
			document.getElementById("recdateFrom").disabled = true;
			document.getElementById("recdateTo").disabled = true;
			document.getElementById("sentdateFrom").disabled = true;
			document.getElementById("sentdateTo").disabled = true;
			document.getElementById("createddateFrom").disabled = false;
			document.getElementById("createddateTo").disabled = false;
			radiobtn = document.getElementById("radcustom1");
			radiobtn.checked = true;
		}
	}
	function openPrint()
	{
	//var glideNo = "<? echo $_GET['GID'] ?>";
	//var reportType = document.all.dtmReportType.value;
	var reportType = document.getElementById("dtmReportType").options[document.getElementById("dtmReportType").selectedIndex].value;
	//strPage = "ReportDocumentDeadlines.php?RID="+regionCode+'&GID='+glideNo+'&_=<? echo time();?>';
	//alert(regionCode);
	var GID="test";
	
	var showWindow = 1;
	var notedbyname = '';
	var notedbyposition = '';

	// alert(reportType);
	//if(reportType=="LRD" || reportType=="LIR" || reportType=="LOR" || reportType=="LIOR" || reportType=="NDP" || reportType=="RP" || reportType=="UD")
	if(reportType=="IAO" || reportType=="LRD" || reportType=="LIR" || reportType=="MLMD" || reportType=="MLF" || reportType=="MLER" ||  reportType=="MLOD" ||  reportType=="MLIR" || reportType=="MLRD" || reportType=="LOR" || reportType=="LIOR" || reportType=="RP" || reportType=="UD" || reportType=="SRD" || reportType=="UDSD" || reportType=="BRCD" || reportType=="RRDR" || reportType=="DMPR" || reportType=="LRR" || reportType=="MCSRR" || reportType=="MPRFS" || reportType=="MLOC")
	{

		dateFrom = document.getElementById("dateFrom").value;
		dateTo = document.getElementById("dateTo").value;

		// if(reportType=="LOR"){
		// 	alert('MAINTENANCE. PLEASE CONTACT ADMINISTRATOR');
		// }

		if(reportType=="IAO" || reportType=="LIR" || reportType=="LOR" || reportType=="BRCD")
			seldoctype = document.getElementById("seldoctype").value;
		
		if(dateFrom.length == 0)
		{
			alert("Please select Date From!");
			document.getElementById("dateFrom").focus();
			showWindow = 0;		
		}
		else if(dateTo.length == 0) 
		{
			alert("Please select Date To!");
			document.getElementById("dateTo").focus();
			showWindow = 0;		
		} 
		else if(dateFrom>dateTo)
		{
			alert("Please change date range!");
			document.getElementById("dateFrom").focus();
			showWindow = 0;		
		}
		
	}	
	else if(reportType=="RA")
	{
		documentID = document.getElementById("documentID").value;
		if(documentID.length == 0)
		{
			alert("Please input Document ID!");
			document.getElementById("documentID").focus();
			showWindow = 0;		
		}
	}	 
	
	else if(reportType=="NDP" || reportType=="MMPR" || reportType=="AR" || reportType=="LRR" || reportType=="MCSRR")
	{
		//documentID = document.getElementById("documentID").value;
		dateYear = document.getElementById("cboLtrYear").value;
		dateMonth = document.getElementById("cboLtrMonth").value;
		if(dateYear.length == 0)
		{
			alert("Please select Year!");
			document.getElementById("cboLtrYear").focus();
			showWindow = 0;		
		}
		 
		if(dateMonth.length == 0)
		{
			alert("Please select Month!");
			document.getElementById("cboLtrMonth").focus();
			showWindow = 0;		
		}
	}
	else if(reportType=="CUSTOM1")
	{
		daterad = document.getElementById("radvalue").value;
		if(daterad == 0){
			dateFrom = document.getElementById("recdateFrom").value;
			dateTo = document.getElementById("recdateTo").value;
		}
		if(daterad == 1){
			dateFrom = document.getElementById("sentdateFrom").value;
			dateTo = document.getElementById("sentdateTo").value;
		}
		if(daterad == 2){
			dateFrom = document.getElementById("createddateFrom").value;
			dateTo = document.getElementById("createddateTo").value;
		}
		// alert(dateFrom);
		// alert(dateTo);

	}
	else if(reportType=="SOD")
	{
		if(document.getElementById("raddoc-p").checked == true){
			raddoc = 1;
		} else if(document.getElementById("raddoc-o").checked == true){
			raddoc = 2;
		} else if(document.getElementById("raddoc-u").checked == true){
			raddoc = 3;
		} else {
			raddoc = 0;
		}
	}

	if(reportType=="MLMD" || reportType=="MLRD" || reportType=="MLF" || reportType=="MLER" || reportType=="MLIR" || reportType == "MLOD")
	{
		notedbyname = document.getElementById("txtnotedbyname").value;
		notedbyposition = document.getElementById("txtnotedbyposition").value;
	}

	if(reportType=="DMPR" || reportType=="MMPR")
	{
		alert('Please take note that generating numerous data could take a while. Thank you!');
		if(document.getElementById("radstatinc").checked){
			docstat = 0;
		}else{
			docstat = 1;
		}

		if(document.getElementById("chknodetails").checked){
			docdetail = 0;
		}else{
			docdetail = 1;
		}
	}
	
	if(reportType!=""){
		if(reportType=="LRD")
			strPage = "reportDocumentDeadlines.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLMD")
			strPage = "reportDocumentMLMDExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLRD")
			strPage = "reportDocumentMLRDExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLF")
			strPage = "reportDocumentMLFExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLER")
			strPage = "reportDocumentMLERExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLIR")
			strPage = "reportDocumentMLIRExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLOD")
			strPage = "reportDocumentMLODExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="RRDR")
			strPage = "reportDocumentRecordRegistry.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="DMPR"){
			strPage = "reportDocumentReceived.php?stat="+docstat+"&detail="+docdetail+"&reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="LIR"){
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentIncomingExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';	
			}
			else
			{ // added by emma END
			strPage = "reportDocumentIncoming.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		}
		else if(reportType=="LOR"){
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentOutgoingExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
			else
			{ // added by emma END
			strPage = "reportDocumentOutgoing.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		}
		else if(reportType=="IAO"){
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentIncOutExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';		
			}
			else
			{ // added by emma END
			strPage = "reportDocumentIncOut.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		}
		else if(reportType=="LIOR"){
			strPage = "reportDocumentIntra.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		//else if(reportType=="NDP")
		//	strPage = "reportDocumentEncoded.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="RA"){
			strPage = "reportDocumentHistory.php?reportType="+reportType+'&documentID='+documentID+'&_=<? echo time();?>';
		}
		else if(reportType=="RP"){
			strPage = "reportDocumentProfile.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="UD"){
			strPage = "reportDocumentUnacted.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';	
		}
		else if(reportType=="UDSD"){
			strPage = "reportDocumentUnacted2.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="NDP"){
			strPage = "reportDocumentEncoded.php?reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';
		}
		else if(reportType=="MMPR"){
			strPage = "reportDocumentReceived.php?stat="+docstat+"&detail="+docdetail+"&reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';
		}
		else if(reportType=="AR"){
			strPage = "reportDocumentActionsRecorded.php?reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';	
		}
		else if(reportType=="SRD"){
			strPage = "reportDocumentReceivedSummary.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';	
		}
		else if(reportType=="BRCD"){
			strPage = "reportDocumentRecordRegistry.php?reportType="+reportType+"&docType="+seldoctype+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="LRR"){
			strPage = "reportDocumentReceived_daily.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="MCSRR"){
			strPage = "reportDocumentCompliance.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="MPRFS"){
			strPage = "reportDocumentProcessRecords.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="MLOC"){
			strPage = "reportDocumentConfidential.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		}
		else if(reportType=="CUSTOM1"){
			strPage = "reportDocumentCustom1.php?reportType="+reportType+'&daterad='+daterad+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&status='+document.getElementById("txtreport").value+'&_=<? echo time();?>';
		}
		else if(reportType=="SOD"){
			strPage = "reportDocumentDocumentSummary.php?reportType="+reportType+'&doc='+raddoc+'&_=<? echo time();?>';
		}
		// strPage = "reportDocumentToSummary.php?reportType="+reportType+"&docType="+doctypebarcode+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
	}else
	{
		showWindow = 0;
		alert("Please select Report Type!");
		document.getElementById("dtmReportType").focus();
	}	
	/*
	if(reportType!='')
		window.open(strPage, '_blank','toolbar=no,location=no,directories=no,status=0,menubar=0,scrollbars=1,resizable=1,width=780,height=528');
	else
		alert('Error:\nPlease select a Region');
	*/	
		if(showWindow)
			window.open(strPage, '_blank','toolbar=no,location=0,directories=no,status=0,menubar=0,scrollbars=1,resizable=1,width=780,height=528');
	}
	
	function changeReportType()
	{
	var value = document.getElementById("dtmReportType").value;
		getData('showReports.php?mode=show&reportType='+value,'reports');
	}
</script>
<?
//javascript:check(document.getElementById('frmReport'),'reports','showReports.php');
?>

<form name="frmReport" id="frmReport">
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="textbody" align="center">
  <!--DWLayoutTable-->
  <tr>
    <td width="110" height="1"></td>
    <td width="157"></td>
    <td width="41"></td>
    <td width="172"></td>
    <td width="129"></td>
    <td width="104"></td>
  </tr>
  <tr>
    <td height="22"></td>
    <td valign="top"><div align="right"><strong>Report Type &nbsp;&nbsp; </strong></div></td>
    <td colspan="2" valign="top">
	<?php 
	
		$rsReport = $objReport->getReportTypes();
		?>
	<select name="dtmReportType" id="dtmReportType" class="textbody" onchange="changeReportType();">
	<option value="-1">&nbsp;</option>
	<?php
		for($i=0; $i<count($rsReport); $i++){
			if($_GET['reportType']==$rsReport[$i]['reportCode'])
				echo "<OPTION value='".$rsReport[$i]['reportCode']."' SELECTED>".$rsReport[$i]["reportName"]."</OPTION>\n";
			else
				echo "<OPTION value='".$rsReport[$i]['reportCode']."'>".$rsReport[$i]["reportName"]."</OPTION>\n";
		}
	?>
    </select></td>
    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td></td>
  </tr>
  <!-- Begin Document Type -->
  <?php 
  	if($_GET['reportType']=="IAO" || $_GET['reportType']=="LRD" || $_GET['reportType']=="MLMD" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLOD" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="LIOR" || $_GET['reportType']=="RP" || $_GET['reportType']=="UD" || $_GET['reportType']=="SRD"  || $_GET['reportType']=="UDSD" || $_GET['reportType']=="BRCD" || $_GET['reportType']=="RRDR" || $_GET['reportType']=="DMPR" || $_GET['reportType']=="LRR" || $_GET['reportType']=="MCSRR")
			if($_GET['reportType']=="IAO" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="BRCD") 
			{
				echo '
					  <tr>
					    <td height="22"></td>
					    <td valign="top"><div align="right"><strong>Document Type &nbsp;&nbsp; </strong></div></td>
					    <td colspan="2" valign="top">';
					    	$rsDoctypes = $objDocuType->getDocType();
				echo '
							<select style="width: 231px;" name="seldoctype" id="seldoctype">';
								if($_GET['reportType']=="")
									echo '<option value=""></option>';
								else
									echo '<option value="all">ALL</option>';
					    		foreach($rsDoctypes as $doctype):
					    			if($_GET['reportType']==""){
					    				if(in_array($doctype['documentTypeId'], array('177','410','161','263','189','413','369','411','412','413','414','415')))
					    					echo '<option value='.$doctype['documentTypeId'].'>'.$doctype['documentTypeAbbrev'].'</option>';
					    			}
					    			else
					    				echo '<option value='.$doctype['documentTypeId'].'>'.$doctype['documentTypeAbbrev'].'</option>';
					    		endforeach;
				echo '
							</select>
						</td>
					    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
					    <td></td>
					  </tr>
				';
			}
   ?>
   <!-- End Document Type -->
  <tr>
    <td height="5"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> 
  <tr>
    <td height="22"></td>
    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td colspan="4" valign="top">
	
	<table width="82%"  border="0">
	<?php 
 	if($_GET['reportType']=="IAO" || $_GET['reportType']=="LRD" || $_GET['reportType']=="MLMD" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLOD" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="LIOR" || $_GET['reportType']=="RP" || $_GET['reportType']=="UD" || $_GET['reportType']=="SRD"  || $_GET['reportType']=="UDSD" || $_GET['reportType']=="BRCD" || $_GET['reportType']=="RRDR" || $_GET['reportType']=="DMPR" || $_GET['reportType']=="LRR" || $_GET['reportType']=="MCSRR" || $_GET['reportType']=="MPRFS" || $_GET['reportType']=="MLOC" || $_GET['reportType']=="CUSTOM1" || $_GET['reportType']=="SOD")
		 if($_GET['reportType']=="RRDR" || $_GET['reportType']=="DMPR" || $_GET['reportType']=="LRR" || $_GET['reportType']=="MCSRR"){
		 	echo '
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateFrom" name="dateFrom" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateTo" name="dateTo" value="'.date('Y-m-d').'"></td>
			  </tr>';
		 }
		 if($_GET['reportType']=="SOD"){
		 	echo '
			  <tr>
				<td colspan=2>
					<input type="radio" value="1" name="raddoc" id="raddoc-p"> Pending
					<input type="radio" value="2" name="raddoc" id="raddoc-o"> Overdue
					<input type="radio" value="3" name="raddoc" id="raddoc-u"> Unacted
				</td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
			  </tr>';
		 }else if($_GET['reportType']=="CUSTOM1"){
		 	echo '
		 	  <tr><input type="hidden" id="radvalue" value="0" >
		 	  	<td colspan=2><input type="radio" name="radcustom1" id="radcustom3" value="0" onchange="radchange(0)" checked> Date Received BY '.$_SESSION['office'].'<br><br></td></tr>
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="recdateFrom" name="recdateFrom" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="recdateTo" name="recdateTo" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr><td colspan=2><input type="radio" name="radcustom1" id="radcustom2" value="1" onchange="radchange(1)"> Date Sent BY '.$_SESSION['office'].'<br><br></td></tr>
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="sentdateFrom" name="sentdateFrom" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="sentdateTo" name="sentdateTo" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr><td colspan=2><input type="radio" name="radcustom1" id="radcustom1" value="2" onchange="radchange(2)"> Date Created BY '.$_SESSION['office'].'<br><br></td></tr>
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="createddateFrom" name="createddateFrom" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="createddateTo" name="createddateTo" value="'.date('Y-m-d').'"></td>
			  </tr>
			  <tr>
				<td colspan=2>
					<input type="hidden" id="txtreport" value="0">
					<input type="radio" name="chkexceltype" onchange="getReportVal(0)" checked> Incoming
					<input type="radio" name="chkexceltype" onchange="getReportVal(1)"> Outgoing
					<input type="radio" name="chkexceltype" onchange="getReportVal(2)"> Summary
				</td>
			  </tr>';
		 }else{
		 	echo '
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateFrom" name="dateFrom"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateTo" name="dateTo"></td>
			  </tr>';
		 }
		  //// added by emma for excel format of report
			if($_GET['reportType']=="IAO" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR") 
			{
				echo '<tr>
						<td>Excel Format</td>
						<td><input type="checkbox" id="excelType" name="excelType" value="1"></td>
			  		  </tr>';
			}

			// begin with details
			if($_GET['reportType']=="DMPR") 
			{
			  	echo '<tr>
						<td>Status</td>
						<td>
							<input type="radio" id="radstatinc" name="radstatus" value="1" checked>W/o Released to and Processing Time &nbsp;
							<br>
							<input type="radio" id="radstatoug" name="radstatus" value="0">With Released to and Processing Time
						</td>
			  		  </tr>';
			  	echo '<tr>
						<td>No details</td>
						<td><input type="checkbox" id="chknodetails" name="chknodetails" value="1" checked></td>
			  		  </tr>';
			}
			// end with details
			
			/// added by emma end
	elseif($_GET['reportType']=="RA") 
		  echo ' 
			  <tr>
				<td>Document ID</td>
				<td><input type="text" id="documentID" name="documentID"></td>
			  </tr>
			 '; 
	elseif($_GET['reportType']=="NDP" || $_GET['reportType']=="MMPR"  || $_GET['reportType']=="AR" )
	{
		  echo ' 
			  <tr>
				<td>Year</td>
				<td>
					<select name="cboLtrYear" id="cboLtrYear">
					 ';
					 $objReport->comboYear($cboYear);
                     
					echo '
					</select>
				</td>
			  </tr>
  			  <tr>
				<td>Month</td>
				<td>
					<select name="cboLtrMonth" id="cboLtrMonth">
					';
					$objReport->comboMonth($cboMonth); 
                    
                    echo '
					</select>
				</td>
			  </tr>
			 ';
			 // begin with details
			if($_GET['reportType']=="MMPR") 
			{
				echo '<tr>
						<td>Status</td>
						<td>
							<input type="radio" id="radstatinc" name="radstatus" value="1" checked>W/o Released to and Processing Time &nbsp;
							<br>
							<input type="radio" id="radstatoug" name="radstatus" value="0">With Released to and Processing Time
						</td>
			  		  </tr>';
				echo '<tr>
						<td>No details</td>
						<td><input type="checkbox" id="chknodetails" name="chknodetails" value="1" checked></td>
			  		  </tr>';
			}
			// end with details
	}
	if($_GET['reportType']=="MLMD" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLOD"){
			echo '
				<tr>
					<td><b>Noted BY</b></td>
					<td><input type="text" name="txtnotedbyname" id="txtnotedbyname"></td>
				</tr>
				<tr>
					<td><b>Position</b></td>
					<td><input type="text" name="txtnotedbyposition" id="txtnotedbyposition"></td>
				</tr>
			';
	}
	?>		 
	   <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
 
  
  <tr>
    <td height="5"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="24"></td>
    <td colspan="4" align="center" valign="top" style="text-align:center;"><input type="button" value="Generate" name="Generate" class="btn" onClick="openPrint();"></td>
    <td></td>
  </tr>
  </table>

</form>
