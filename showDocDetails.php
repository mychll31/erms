<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");
include_once("class/DocDetail.class.php");

$docID = "ITD-09-00000"; //temporary only... will be replace with GET

$objDocDetail = new DocDetail;
$arrFields = $objDocDetail->getDocDetails($docID);

?>
<style>
#headerDiv, #contentDiv {
float: left;
width: 510px;
}
#titleText {
float: left;
font-size: 1.2em;
font-weight: bold;
margin: 5px 10px;
}
#headerDiv {
background-color: #0037DB;
color: #9EB6FF;
}
#contentDiv {
background-color: #FFE694;
}
#myContent {
margin: 5px 10px;
}
#headerDiv a {
float: right;
margin: 10px 10px 5px 5px;
}
#headerDiv a:hover {
color: #FFFFFF;
}
</style>


<table align="center" width="60%" class="textbody">
  <!--DWLayoutTable-->
  <tr>
    <td width="97">Document ID </td>
    <td width="175"><?php echo $arrFields[0]['documentId']; ?>&nbsp;</td>
    <td width="93"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td width="217"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  <tr>
    <td>Subject</td>
    <td><?php echo $arrFields[0]['subject']; ?>&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  <tr>
    <td>Document Type </td>
    <td><?php echo $arrFields[0]['documentTypeDesc']; ?>&nbsp;</td>
    <td>Document Date </td>
    <td><?php echo $arrFields[0]['documentDate']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Origin:</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td>Date Received </td>
    <td><?php echo $arrFields[0]['dateReceived']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Confidential</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td>File Container </td>
    <td><?php echo $arrFields[0]['fileContainer']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4">
	
	<!-- cut here -->
		<script language="JavaScript">
	
		function enableNameCombo()
		{
			document.getElementById("cmbEmpNumber").disabled = false;
			document.getElementById("cmbOfficeCode").disabled = true;
			radiovar = document.getElementById("actionUnitSwitch");
			radiovar[0].checked = true;
		}
			
		function enableOfficeCombo()
		{
			document.getElementById("cmbEmpNumber").disabled = true;
			document.getElementById("cmbOfficeCode").disabled = false;
			radiovar = document.getElementById("actionUnitSwitch");
			radiovar[1].checked = true;
		}
	
		</script>
	
					<?php
				// ADDING A RECORD
				$arrFields2 = $objDocDetail->getUserFields();

						if($t_submit)
						{
							
							if(!$objDocDetail->addAction($arrFields2))
							{
								//$intActionCodeId = $arrFields2['cmbActionRequired'];
								//$intActionUnitSwitch = $arrFields2['actionUnitSwitch'];
								//$strRemarks = $arrFields2['txtRemarks'];
							}
							
							$msg = $objDocDetail->getValue('msg');
							
							
						}
						
							echo $msg;
							
						// DELETING A RECORD
						
						if($_GET['ac']=='del')
						{
							$objDocDetail->deleteAction($_GET['ID']);
							$msg = $objDocDetail->getValue('msg');
						}
						
						?>
	<table width="100%"  border="0">
      <tr>
        <td>
		<form action="javascript:get(document.getElementById('frmUserAction'),'docDetails','showDocDetails.php');" name="frmUserAction" id="frmUserAction" >
		<table width="100%"  border="1" align="center">
          <tr>
            <td>Action Taken </td>
            <td><?
				$rsActionTaken = $objDocDetail->getActionTaken();
			?>
			<select name="cmbActionTaken" id="cmbActionTaken">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionTaken);$i++) 
				{ 		
					echo "<OPTION value='".$rsActionTaken[$i]['actionCodeId']."'>".$rsActionTaken[$i]["actionDesc"]."</OPTION>\n"; 
				}
			?>		  
			</select></td>
          </tr>
          <tr>
            <td width="16%">Action Required </td>
            <td width="84%">
			<?
				$rsActionRequired = $objDocDetail->getActionRequired();
			?>
			<select name="cmbActionRequired" id="cmbActionRequired">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionRequired);$i++) 
				{ 		
					echo "<OPTION value='".$rsActionRequired[$i]['actionCodeId']."'>".$rsActionRequired[$i]["actionDesc"]."</OPTION>\n"; 
				}
			?>		  
			</select>
			</td>
          </tr>
          <tr>
            <td colspan="2">Action Unit</td>
          </tr>
          <tr>
            <td colspan="2">
<table width="100%"  border="1">
  <tr>
    <td width="15%" align="right"><input name="actionUnitSwitch" type="radio" value="1" onClick="enableNameCombo();"></td>
    <td width="85%"> Employee Name: 
	
	<?
		$rsEmployees = $objDocDetail->getEmployees();
	?>			<select name="cmbEmpNumber" id="cmbEmpNumber" onFocus="enableNameCombo();">
      <option value="-1">&nbsp;</option>
      <?php
				for($i=0;$i<sizeof($rsEmployees);$i++) 
				{ 		
					echo "<OPTION value='".$rsEmployees[$i]['empNumber']."'>".$rsEmployees[$i]["surname"]."</OPTION>\n"; 
				}
			?>
    </select></td>
  </tr>
</table>
<table width="100%"  border="1">
  <tr>
    <td width="15%" align="right"><input name="actionUnitSwitch" type="radio" value="2" onClick="enableOfficeCombo();"></td>
    <td width="85%"> Office: 
      <?php  
								$objDocDetail->displayOffice();
								/*
								$rsOffice = $objDocDetail->getOffices(); 
							
								echo '<SELECT name="txtOfficeCode">';
							  	echo '<OPTION value="">&nbsp;</OPTION>';
								
								  for($i=0;$i<sizeof($rsOffice);$i++) 
								  { 		
									$ctr++;
									echo "<OPTION value='".$rsOffice[$i]['eoCode']."'>".$rsOffice[$i]["eoName"]."</OPTION>\n"; 
									
									//Get Office' Service
									$rsOfficeService = $objDocDetail->getOfficeServices($rsOffice[$i]['eoCode']); 
									 for($i2=0;$i2<sizeof($rsOfficeService);$i2++) 
								 	 {
									 	$ctr2++;
										echo "<OPTION value='".$rsOfficeService[$i2]['serviceCode']."'>&nbsp;&nbsp;&nbsp;&nbsp;".$rsOfficeService[$i2]["serviceName"]."</OPTION>\n"; 
										
										
										//Get Service's Divisions
										$rsServiceDivision = $objDocDetail->getServiceDivisions($rsOfficeService[$i2]['serviceCode']); 
										for($i3=0;$i3<sizeof($rsServiceDivision);$i3++) 
										{
											$ctr3++;
											echo "<OPTION value='".$rsServiceDivision[$i3]['divisionCode']."'>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;".$rsServiceDivision[$i3]["divisionName"]."</OPTION>\n"; 

										}
										 
									 }
									
									
								  }
								  
								  if($ctr==0)
									  echo '<OPTION value="">No Office added yet.</OPTION>';
							   echo '</SELECT>&nbsp;*';
							   */
						   ?>
					
					
</td>
  </tr>
</table>
			
				 </td>
            </tr>
          <tr>
            <td width="16%">Remarks</td>
            <td width="84%"><input name="txtRemarks" type="text" size="50"><input type="hidden" name="txtDocumentID" value="<? echo $arrFields[0]['documentId'];?>">
			</td>
          </tr>
		    <tr align="center">
            <td colspan="2" align="center"><input type="submit" name="btnAdd" value="Submit"></td>
            </tr>
        </table>
	</form>
		</td>
      </tr>
    </table>

<table width="100%"  border="0" id="listRecipient">
  <tr>
    <td>
	&nbsp;
	<table width="100%"  border="1">
  <tr>
    <td align="center" width="20%">Action Taken </td>
    <td align="center" width="20%">Date</td>
    <td align="center" width="23%">Recipient</td>
    <td align="center" width="24%">Action Required </td>
    <td align="center" width="13%">&nbsp;</td>
  </tr>
    <?php
	
	$rsAction = $objDocDetail->showActions();
	
	for($i=0;$i<count($rsAction);$i++) 
	{
		$actionRecipient = $objDocDetail->displayRecipientNames($rsAction[$i]['recipientId'], $rsAction[$i]['recipientUnit']);
	  echo '<tr>
	    <td>'.$rsAction[$i]['actionTakenDesc'].'</td>
		<td>'.$rsAction[$i]['dateSent'].'</td>
		<td>'.$actionRecipient.'</td>
		<td>['.$rsAction[$i]['actionDesc'].']<br><i>'.$rsAction[$i]['remarks'].'</i></td>
		<td><a href=',$_SERVER['PHP_SELF'],'?'.'&ac=del&ID='.$rsAction[$i]["historyID"].' onClick="javascript: return confirm(\'','Are you sure you want to delete this record?','\');return (false);">Delete</a>
</td>
	  </tr>';
  	}
  ?>
</table>

	</td>
  </tr>
</table>

	
	</td>
  </tr>
  <tr>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  <tr>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><table width="100%"  border="1">
      <tr>
        <td>History</td>
        <td>Attached Document </td>
      </tr>
      <tr>
        <td width="50%" valign="top" align="center"> 
<br>
<div style="border : solid 1px #ff0000; background : #f2f2f2; color : #000000; padding : 4px; width : 250px; height : 600px; overflow : auto; ">
<b>You do hereby acknowledge that... </b>
<hr>
<br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
You do hereby acknowledge that... <br>
<hr color="#999999" align="center" noshade width="100%">
</div> 

</td>
        <td width="46%" valign="top"><table width="100%"  border="1" align="center">
          <tr>
            <td width="49%">File Name </td>
            <td width="51%">Preview Open/Save</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">
			
			
			
			<table width="100%"  border="1">
			  <tr>
				<td><embed src="files/SQL.pdf" width="375" height="475">&nbsp;</td>
			  </tr>
			</table>

			
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
  
  <tr><td colspan="4" align="center"><input type="submit" value="Save" class="caption"><input type="reset" value="Clear" class="caption"></td></tr>
</table>
</form>			

