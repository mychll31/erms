<?php
include_once("class/MySQLHandler.class.php");
include_once("class/Custodian.class.php");
include_once("class/IncomingList.class.php");
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
$objCust = new Custodian;
$objList = new IncomingList;
$dateImplemented = $objDocDetail->getDateImplemented();
$objList->displayNotification($_REQUEST['msg']);

	if($_REQUEST['mode']=="show")
	{		
	$t_strSearch = $_REQUEST['t_strSearch'];
	$addURLString = '#';
	$limit = 15;
	$page = $_REQUEST['page']==""?1:$_REQUEST['page'];
	$ascdesc = $_REQUEST['ascdesc']=="ASC"?"DESC":"ASC";

	
	$total_pages = $objList->getOverDue(0,'total', 'deadlines');
	$total_pages = $total_pages[0]['_total'];

	$arrRes = $objList->getOverDue(($page-1), '', 'deadlines');
	$arrRes['data'] = $arrRes;

	if($_REQUEST['action']<>'priority') {
	echo "<table width=100% align=center border=0 cellpadding=0 cellspacing=0>
		<tr>
		<td>&nbsp;";
		if(count($arrRes['data'])) {
			echo "<ul id='icons' class='ui-widget ui-helper-clearfix'>";
			if($arrRes['data'][$t]['flag']=='1')
				$t_intFlag = "0";
			else
				$t_intFlag = "1";	
			//echo "<li class='ui-state-default ui-corner-all' onClick='getChk();'><span class='ui-icon ui-icon-flag' title='Flag' ></span></li>";

			//echo "<li class='ui-state-default ui-corner-all' onClick='getChkPriority();'><span class='ui-icon ui-icon-notice' title='Priority' ></span></li>";
			
			//echo "<li class='ui-state-default ui-corner-all' onClick='markAsRead();'><span class='ui-icon ui-icon-mail-open' title='Mark as Read' ></span></li>";			
				
			//echo "<li class='ui-state-default ui-corner-all' onClick=\" doDeleteIncomingDialog('".$arrRes['data'][$t]['documentId']."','".$arrRes['data'][$t]['historyId']."'); \"><span class='ui-icon ui-icon-trash' title='Delete' ></span></li>";
			
			//echo "<li class='ui-state-default ui-corner-all' onClick=\"moveCustDoc();  \"><span class='ui-icon ui-icon-copy' title='Move Document' ></span></li>";
			
			//echo "<li class='ui-state-default ui-corner-all' onclick=\"moveCustDocCustomFolder();\"><span class='ui-icon ui-icon-copy' title='Move Document' >&nbsp;</span></li></ul>";	
			}
			echo "<td align=right width=18% valign=bottom>
			<div data-tip='This is an autocomplete search box'>
			<input type='textbox' class='search_icon' id='t_strSearchIncomingList' value='".$_REQUEST['t_strSearch']."'></td></div>".
			
		/*<td align=left width=12% valign=bottom>
			<div style='float:left;height:19px;' class='ui-state-default ui-corner-all' onmouseover=\"this.style.cursor='pointer';\">
			<span class='ui-icon ui-icon-search' title='Search' style='float:left;'></span>
			<span style='font-size:11px'  style='float:left;'>Search&nbsp;&nbsp;</span></div></td>*/
	"</tr></table>";			
	}

	if(count($arrRes['data']))
		{	

echo '<div id="rdoc_listings">';				
		echo '<form name="frmIncomingList" id="frmIncomingList" method="post"><div id="data">
			<table cellpadding="0" cellspacing="0" align="center" class="listings" width="99%">
			<tr class="listheader" style="border:thin solid;">
				<td width=5><input type=checkbox onClick=\'checkedAll("frmIncomingList");\'></td>		
				<td width=5><span class="ui-icon ui-icon-flag"></td>
				<td width=5><span class="ui-icon ui-icon-notice"></td>
				<td width=20 style="text-align:center;">SENDER</td>
				<td width=155 style="text-align:center;">DOCID</td>
				<td width=30 style="text-align:center;"><img style="height:12px; padding-top:2px" src="css/images/file_attachment.png" /></td>										
				<td style="text-align:center;">DOC NO</td>				
				<td style="text-align:center;">DOC TYPE</td>								
				<td style="text-align:center;">SUBJECT</td>
				<td width=10 style="text-align:center;">ORIGIN</td>
				<td width=60 style="text-align:center;">DATE</td>
			</tr>';
		$i=0;
		for($t=0;$t<count($arrRes['data']);$t++)
			{
			if(count($arrRes['data']))
			{	

			$strStatus = $objCust->getDocumentStatus($arrRes['data'][$t]['status']);
			$strSender = $objCust->getSender($arrRes['data'][$t]['historyId']);
			/*if($objCust->isAgencyUser($strSender)){
				$agencyOriginId = $objCust->getOfficeCode($strSender);
				$strSender = $objCust->getOfficeName($agencyOriginId,'','','agency');
			}*/
				
			if($arrRes['data'][$t]['senderUnit']== "agency")
			{
				$strSender = $objList->agencyName($arrRes['data'][$t]['senderId']);//added by emma for name of agencies 1-26-2016
			}
			$intDocType = $objCust->getDocumentType($arrRes['data'][$t]['documentTypeId']);
			$strOfficeName = $objCust->getOfficeName($arrRes['data'][$t]['originId'],$arrRes['data'][$t]['officeSig'],$arrRes['data'][$t]['status'],$arrRes['data'][$t]['originUnit']);			
			$rsActionRequired = $objDocDetail->getActionRequiredDetails($arrRes['data'][$t]['actionCodeId']);
			$strActionRequired = $rsActionRequired[0]['actionDesc'];
			$intReadHistory = $objList->checkReadHistory($arrRes['data'][$t]['documentId'],$objCust->get('userID'));
			$strPageLink = ($strStatus=="inc")?"showIncoming.php":"showOutgoing.php";	
			$dateSent = explode(' ',$arrRes['data'][$t]['dateSent']);
			if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
			   $t_strClass = (!$objList->checkDocReceivedStatus($arrRes['data'][$t]['historyId']))?"":"data";
			}
			else $t_strClass = (count($arrRes['data'][$t]['receivedBy'])==0 || count($intReadHistory)==0)?"":"data";
			
			$t_strBgcolor = ($i%2)? "#F2F2F2":"#FFFFFF";
			$t_strBgcolor = $objCust->altRowBgColor($i);
			$arrDocIDdetails = explode('-',$arrRes['data'][$t]['documentId']);
			$blnEditable = ($objCust->get("office") == $arrDocIDdetails[0])?true:false;

			if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
				$onclick = 'onClick="getData(\''.$strPageLink.'?mode=view&id='.$arrRes['data'][$t]["documentId"].'&src=receive&edit='.$blnEditable.'\',\'doclist\');"';
			 	/*$onclick = ($objList->checkDocReceivedStatus($arrRes['data'][$t]['historyId']))?
				 'onClick="getData(\''.$strPageLink.'?mode=view&id='.$arrRes['data'][$t]["documentId"].'&src=receive&edit='.$blnEditable.'\',\'doclist\');"':
				  'onClick=\'doReceiveDocument("'.$arrRes['data'][$t]['documentId'].'","'.$arrRes['data'][$t]['historyId'].'","'.$arrRes['data'][$t]['recipientId'].'","'.$blnEditable.'");\'';
				**/
			}
			else{
				$onclick = 'onClick="getData(\'showIncoming.php?mode=view&id='.$arrRes['data'][$t]["documentId"].'&sender='.$strSender.'&src=receive\',\'doclist\');" title="SUBJECT: '.$arrRes['data'][$t]['subject'].'"';
				/*$onclick = ($arrRes['data'][$t]['receivedBy']<>NULL)?
					'onClick="getData(\'showIncoming.php?mode=view&id='.$arrRes['data'][$t]["documentId"].'&sender='.$strSender.'&src=receive\',\'doclist\');" title="SUBJECT: '.$arrRes['data'][$t]['subject'].'"':
					'onClick=\'doReceiveDocument("'.$arrRes['data'][$t]['documentId'].'","'.$arrRes['data'][$t]['historyId'].'","'.$arrRes['data'][$t]['recipientId'].'");\'';
					**/
			}

			echo '<tr class="'.$t_strClass.'" onMouseOver="this.bgColor=\'#FFFFCC\'; this.style.cursor=\'pointer\';" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'" title="SUBJECT : '.htmlentities($arrRes['data'][$t]['subject']).'
		DOCUMENT TYPE : '.$intDocType.' 
		ACTION REQUIRED : '.$strActionRequired.'">';
			
			echo '<td><input type="checkbox" name="chk" id="chk" value="'.$arrRes['data'][$t]['historyId'].'"><input type="hidden" name="t_strDocId'.$t.'" id="t_strDocId'.$t.'" value="'.$arrRes['data'][$t]['documentId'].'"></td>';

		//flagging of documents
		if($arrRes['data'][$t]['flag']=='1')
			echo "<td><div id='flag".$t."'><span class='ui-icon ui-icon-flag' title='Flag' ></span></div></td>";
		else
			echo "<td><div id='flag".$t."'>&nbsp;</div></td>";	

		//priority of documents
		if($arrRes['data'][$t]['priority']=='1')
			echo "<td><div id='priority".$t."'><span class='ui-icon ui-icon-notice' title='Priority' ></span></div></td>";
		else
			echo "<td><div id='priority".$t."'><span>&nbsp;</span>&nbsp;</div></td>";
			
		echo   '<td style="text-align:center;" '.$onclick.'>'.$strSender.'</td>';

		/*
		// receive documents		
		if($arrRes['data'][$t]['receivedBy']<>NULL)
			{
			echo 	'<a href="#" onClick="getData(\''.$strPageLink.'?mode=view&id='.$arrRes['data'][$t]["documentId"].'&src=receive&edit=false\',\'doclist\');">&nbsp;'.$arrRes['data'][$t]['documentId'].'</a>';
			}
		else
			{
			//echo '<a href="#" onClick=\'document.getElementById("receiveid").innerHTML="'.$arrRes['data'][$t]['documentId'].'"; document.getElementById("hid").innerHTML="'.$arrRes['data'][$t]['historyId'].'"; $("#receiveDialog").dialog("open");\'>&nbsp;'.strtoupper($arrRes['data'][$t]['documentId']).'</a>';
			echo '<a href="#" onClick=\'doReceiveDocument("'.$arrRes['data'][$t]['documentId'].'","'.$arrRes['data'][$t]['historyId'].'");\'>&nbsp;'.strtoupper($arrRes['data'][$t]['documentId']).'</a>';
			}
		*/
		$fielExt = end(explode('.', $arrRes['data'][$t]['attachment']));
		$fileReader = (in_array($fielExt,array("gif", "png", "jpg")))?"":"pdfReader.php?filename=";
		$attachment = $arrRes['data'][$t]['attachment']!=''?'<a href="'.$fileReader.$objCust->_decode($arrRes['data'][$t]['path']).DIRECTORY_SEPARATOR.urlencode($arrRes['data'][$t]['attachment']).'" rel="image" name="0" alt="'.$arrRes['data'][$t]['attachment'].'" onmouseover="window.status=\'ERMS\'; return true;"><img src="css/images/file_attachment.png" style="height:12px;"></a>':"";
		echo	'<td '.$onclick.'>&nbsp;'.$arrRes['data'][$t]['documentId'].'</td>
				<td '.$onclick.'>&nbsp;'.$attachment.'</td>
				<td '.$onclick.'>&nbsp;'.$arrRes['data'][$t]['docNum'].'</td>
				<td '.$onclick.'>&nbsp;'.$intDocType.'</td>
				<td '.$onclick.'>&nbsp;'.str_replace($t_strSearch,"<font color=red><strong>".strtoupper($t_strSearch)."</strong></font>",$objList->limitText($arrRes['data'][$t]['subject'],51)).'</td>
				<td style="text-align:center;" '.$onclick.'>&nbsp;'.$strOfficeName.'</td>
				<td style="text-align:center;" '.$onclick.'>&nbsp;'.$objList->printDateFormat(substr($arrRes['data'][$t]['dateSent'],0,10)).'</td>
				';
		
		echo '</tr>';		
		$i++;
		}//end if
		}//end loop
		echo '</table><input type="hidden" name="t_intFolderID" id="t_intFolderID" value="'.$_REQUEST['fid'].'"></div><br>';
		echo $objList->showPagination($addURLString, $page, $total_pages, $limit, "doclist", "showOverDueList.php?mode=show&blnAgencyDocsOnly=".$_REQUEST['blnAgencyDocsOnly']."&operator=".$_REQUEST['operator']."&t_strSearch=".$_REQUEST['t_strSearch']);
		echo '<br></form>';		

}		
	else
		echo "<br><br><br>No records found<br><br><br>";
		echo '</div>';
}


if($_REQUEST['mode']=="delete_true")
{
	//echo "hid=".$_REQUEST['hid']."/id=".$_REQUEST['id'];
	$rs = $objList->trashIncDocument($objList->get("userID"),$_REQUEST['hid']);	
	$objList->trashFromFolder($objList->get("userID"), $_REQUEST['id']);
	echo '<script language="javascript">
			getData("showOverDueList.php?mode=show&fid='.$t_intOldFolderID.'","doclist");			
			jQuery.facebox("Document Moved to Trash");
			//jQuery.facebox("'.$_REQUEST['url'].'");
		</script>';
	
}

if($_REQUEST['mode']=="move")
{
	$t_strId = str_replace(";","",$_REQUEST['id']);
	$rs = $objList->moveDocument($_REQUEST['f2id'],$_REQUEST['fid'],$t_strId);	
	$t_strFolderName = $objList->getFolderName($_REQUEST['fid']);
	
	if(count($rs))
		echo "<script language='javascript'>getData('showOverDueList.php?mode=show&id=".$_REQUEST['id']."&msg=document moved to $t_strFolderName','doclist');</script>";	
}

if($_REQUEST['mode']=="flag")
{
	$rs = $objList->flagDocument($_REQUEST['hid'], $_REQUEST['t_intFlagStatus']);
	
	if(count($rs))
		echo "<script language='javascript'>getData('showOverDueList.php?mode=show','doclist');</script>";	
}

if($_REQUEST['mode']=="priority")
{
	$rs = $objList->priorDocument($_REQUEST['hid'], $_REQUEST['t_intPriorStatus']);
	echo "<script language='javascript'>getData('showOverDueList.php?mode=show&action=priority','data');</script>";	
}

if($_REQUEST['mode']=="mark")
{
	if($_REQUEST['mark']=='0')
		{
		$rs = $objList->markUnread($_REQUEST['id'],$objList->get('userID'));
	    $rs2 = $objList->removeReceiveDocument($_REQUEST['hid']);
		}
	else if($_REQUEST['mark']=='1')
		{
		$rs = $objList->markRead($_REQUEST['id'],$objList->get('userID'));
		
		}
	if(count($rs) && count($rs2))
		echo "<script language='javascript'>getData('showOverDueList.php?mode=show','data');</script>";	
}
if($_REQUEST['ACTION'] == 'autoreceive') echo "<input type='hidden' id='auto-docid' value='".$_REQUEST['DOCID']."' />";
?>
<script type="text/javascript">
$(function () {
 if($('#auto-docid').length==1){
  var strdocid = $('#auto-docid').val();
  $("#frmIncomingList td:contains('"+strdocid+"')").click();
 }
});

function moveCustDocCustomFolder()
{
	var custdocs = '';
	if(document.frmIncomingList.chk.length == undefined)
		{
		if(document.frmIncomingList.chk.checked)	
			custdocs = document.getElementById('t_strDocId0').value;
		}
	else
		{
		for(var i=0; i < document.frmIncomingList.chk.length; i++){
			if(document.frmIncomingList.chk[i].checked)
				{
				var id = 't_strDocId'+i;
				//if(i>1)
					custdocs = custdocs + document.getElementById(id).value + ';';
				//else
					//custdocs = document.getElementById(id).value;	
				}	
		}
	}
	
if(custdocs!="")
	{	
	//document.getElementById('docid').innerHTML=custdocs;
	//document.getElementById('moveid').innerHTML='".$rs[$t]['documentId']."'; 
	//document.getElementById('fid').innerHTML=document.getElementById('t_intFolderID').value;
	//$('#movedialog2').dialog('open');
	var url = 'showMoveDialog.php?mode=move&docid='+custdocs+'&url=showOverDueList';
	jQuery.facebox({ ajax: url });
	}
}

function doReceiveDocument(docid,hid,rid,blnEditable)
	{
	var url = 'showReceiveDialog.php?mode=receive&docid='+docid+'&hid='+hid+'&rid='+rid+'&edit='+blnEditable+'&src=receive';
	jQuery.facebox({ ajax: url });	
	}

$(function(){

	$('#t_strSearchIncomingList').keyup(function(event){ 
		var keyword=($(this).val() + String.fromCharCode(event.which));
		var operator = (keyword.search("OR") >=0)?'OR':
					   (keyword.charAt(0) == '"')?'CONTAINS':
					   //(keyword.search("-") >= 0)?'EXCEPT':
					   (keyword.search(/\*/g) >= 0)?'DOCTYPE':'AND';

			$( "#rdoc_listings" ).load( 
			       "showOverDueList.php #rdoc_listings",
				   { 
				    operator:operator,
					mode:'show',
					t_strSearch:$('#t_strSearchIncomingList').val(),
					blnAgencyDocsOnly: "<?php echo $_REQUEST['blnAgencyDocsOnly']; ?>"
				   }, 
				  function(data) {
					//console.log(data);
				  });
	});

});
</script>
 