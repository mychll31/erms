<? @session_start();
include_once("class/General.class.php");
$objCustodian = new General;
$arOffices = $objCustodian->getOfficeFromExeOffice();
$c = sizeof($arOffices);
?>

<script language="javascript" type="text/javascript"> 
var validator='';
var tmpEmpNum="";
var map={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.str_replace("&nbsp;","",$arOffices[$i]['officeName']).'":'.$i.',';
		}
		echo '"'.trim($arOffices[$i]['officeName']).'":'.$i.'';
		?>
		}

var mapOffice={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'",';
		}
		echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'"';
		?>
		}
		
 jQuery("#groupRefresh").click( function(){ 
    //if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');
	jQuery('#groupGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#groupGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Office Code','Group Code','Group Desc'],
		colModel:[
			{name:'groupId',index:'groupId',width:65,  hidden:true},
			{name:'officeCode',index:'officeCode', width:100},
			{name:'groupCode',index:'groupCode', width:65},
			{name:'groupName',index:'groupName', width:65}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#groupPager'),
		postData:{table:'tblGroup',GETNAME:true,searchField:'officeCode',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'false',searchOper:'ew'},
		sortname: 'officeCode',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"Office Groups"
	}).navGrid('#groupPager',{edit:false,add:false,del:false});}); 
  });
   $('#groupRefresh').click();
   
   function AddRec2(){
	  var groupId  = $("#groupId").val();
 	  var gCode  = $("#gCode").val();
   	  var officeGroup = $("#officeGroup").val();
	  var gDes  = $("#gDes").val();
      $("#groupGrid").setPostData({mode:"add",table:'tblGroup',ID:groupId,CODE:officeGroup,DESC:gCode,PERIOD:gDes,GETNAME:true,MODULE:'OfficeGroup'});
	  $("#groupGrid").trigger("reloadGrid");
  	  $('#groupClear').click();
	  jAlert('Succesfully Added','Information'); }
	  
	 $("#groupAdd").click(function(){
	 $("#frmOfficeGroups").submit();
	 });
	 
	$('#groupClear').click( function(){ 
	   $("#officeGroup").attr("disabled",false);
	   $("#officeGroup").attr("selectedIndex",0);
	   $("#gCode").val("");
	   $("#gDes").val("");
	   $("#groupSave").css('display','none');
	   $("#groupAdd").css('display','block');
	   $('#Clear_group').text(" Clear ");
	   $('#groupRefresh').click();
	   tmpEmpNum="";
	   validator.resetForm();
	});	
	
	$("#groupEdit").click( function(){ 
	  var id = jQuery("#groupGrid").getGridParam('selrow'); 

	  if (id) {
	   var ret = jQuery("#groupGrid").getRowData(id); 
	   $("#officeGroup").attr("selectedIndex",map[ret.officeCode]);
	   //$("#officeGroup").attr("disabled","disabled");
	   $("#groupId").val(ret.groupId);
	   
	   //alert( $("#custId").val());
	   //tmpEmpNum=ret.empNumber;
	   //$("#custOffice").change();
//	   $("#cabLabel").val(ret.label); 
		$("#gCode").val(ret.groupCode); 
		$("#gDes").val(ret.groupName); 
	   if($("#groupEdit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
   	    $("#groupSave").css('display','block');
		$("#groupAdd").css('display','none');
		$('#Clear_group').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 
	
	
	jQuery("#groupSave").click( function(){ 
	  var groupId  = $("#groupId").val();
 	  var gCode  = $("#gCode").val();
   	  var officeGroup = $("#officeGroup").val();
	  var gDes  = $("#gDes").val();
	    $("#groupGrid").setPostData({mode:"save",table:'tblGroup',ID:groupId,CODE:officeGroup,DESC:gCode,PERIOD:gDes,GETNAME:true,MODULE:'OfficeGroup'});
	    $("#groupGrid").trigger("reloadGrid");
 	    $('#groupClear').click();
	});
	
	$("#groupDel").click(function(){ 
	 var id = jQuery("#groupGrid").getGridParam('selarrrow'); //selrow - for 1 row

	 if(id!= ''){ 
		 jConfirm('Proceed deleting this record?',false, 'ERMS Confirmation Dialog', function(r) {
		 if(r==true){
		  $("#groupGrid").setPostData({mode:"del",table:'tblGroup',ID:id.toString(),MODULE:'OfficeGroup'});
		  $("#groupGrid").trigger("reloadGrid"); 
		  $('#groupClear').click();
		  jAlert('Succesfully deleted', 'Confirmation Results'); } }); } 
	  else jAlert('Please select row to delete', 'Warning');  });
	
	
$().ready(function() {
	 validator = $("#frmOfficeGroups").validate({
		rules: { gCode: "required",
				 gDes: "required"
				 },
		messages: {	gCode: "Group Code is required",
					gDes: "Group Description is required"
				 },
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec2();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	 	 
</script>

<form id="frmOfficeGroups" autocomplete="off" method="get">
<table >
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Office</td>
    <td class="field" colspan="2"><select id="officeGroup" name="officeGroup">
					<? 
					for($i=0;$i<$c;$i++)
					{
					echo "<option value=".$arOffices[$i]['officeCode'].">".$arOffices[$i]['officeName']."</option>";					}
					?>
					</select></td>
  </tr>
  <tr> 
    <td class="label">Group Code</td>
    <td class="field" style="width:30px;"><input type="text" id="gCode" name="gCode"></td>
	<td class="status" style="width:auto">&nbsp;</td>
  </tr>
  <tr> 
    <td class="label">Group Desription</td>
    <td class="field" style="width:30px;"><input type="text" id="gDes" name="gDes"></td>
	<td class="status" style="width:auto">&nbsp;</td>
	<input type="hidden" id="groupId" name="groupId" value="">
  </tr>
 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="groupEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="groupDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="groupAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="groupSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="groupClear"><span title='Clear'></span><div id="Clear_group">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="groupRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
<table id="groupGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="groupPager" class="scroll" style="text-align:center;"></div>
</form>		