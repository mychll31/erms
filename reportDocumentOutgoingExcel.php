<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ListOfOutgoing.xls");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

include_once("class/Records.class.php");
$objRecordDetail = new Records;
		
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
		
$outgoingDocs = $objDocDetail->getDocumentOutgoingByOffice($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo'], $_GET['doctype']);

echo "DOST CENTRAL OFFICE<br>";
echo $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit'])."<br>";

echo "<strong><center>LIST OF OUTGOING RECORDS</center></strong><br>";
echo "<strong><center>(".date('d M Y',strtotime($_GET['dateFrom']))." TO ".date('d M Y',strtotime($_GET['dateTo'])).")</center></strong><br>";
?>

<table border=1>
    <thead>
        <tr bgcolor="#CCCCCC">
            <th ></th>
            <th width="100">DOC ID</th>
            <th width="150">DOC TYPE</th>
            <th>STATUS</th>
            <th>ORIGIN</th>
            <th>SUBJECT</th>
            <th>DATE ENCODED</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $arrdocId = array();
            $no = 1;
            foreach($outgoingDocs as $doc):
                array_push($arrdocId, $doc['documentId']);
                $subject = $doc['subject'];
                $arrData = array($no++,$doc['documentId'],$doc['documentTypeAbbrev'],$doc['docNum'],$doc['referenceId'],$subject,$doc['dateSent']);
                echo '<tr>';
                    foreach($arrData as $data):
                        echo '<td>'.$data.'</td>';
                    endforeach;
                echo '</tr>';
                // $this->Row(array($no++,$doc['documentId'],$doc['documentTypeAbbrev'],$doc['docNum'],$doc['referenceId'],$subject,$doc['dateSent']),1);
            endforeach;

            $outgoingDocs2 = $objDocDetail->getDocumentOutgoingNoHistory($_SESSION['office'], $_GET['dateFrom'], $_GET['dateTo'], $_GET['doctype'], $arrdocId, 1);
            if($outgoingDocs2!=null):
                foreach($outgoingDocs2 as $doc):
                    $subject = $doc['subject'];
                    $arrData = array($no++,$doc['documentId'],$doc['documentTypeAbbrev'],$doc['docNum'],$doc['referenceId'],$subject,$doc['dateAdded']);
                    echo '<tr>';
                        foreach($arrData as $data):
                            echo '<td>'.$data.'</td>';
                        endforeach;
                    echo '</tr>';
                    // $this->Row(array($no++,$doc['documentId'],$doc['documentTypeAbbrev'],$doc['docNum'],$doc['referenceId'],$subject,$doc['dateAdded']),1);
                endforeach;
            endif;
         ?>
    </tbody>
</table>