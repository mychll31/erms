<? session_start(); ?>
<script type="text/javascript">
getCustDocList('showCustDocList.php?mode=show','doclist');
function doDelete(ID)
{
	var docID = ID;
	getData('showCustDocList.php?mode=delete_true&id='+docID,'doclist');
	$("#backgroundPopup").fadeOut("slow");
	$("#popupContact").fadeOut("slow");
	
}

function doCustDelete(ID)
{
	var docID = ID;
	var msg = confirm("Are you sure you want to delete this Document with Document Code : "+docID);
	if(msg)
		getData('showCustDocList.php?mode=delete_true&id='+docID,'doclist');	
}
$(function(){
  var tabs = $('#tabs'),
  tab_a_selector = 'ul.ui-tabs-nav a';
  tabs.tabs({ event: 'change' });
  
  tabs.find( tab_a_selector ).click(function(){ 
   var state = {},
   id = $(this).closest( '#tabs' ).attr( 'id' ),
   idx = $(this).parent().prevAll().length;
   state[ id ] = idx;
   $.bbq.pushState( state ); 
   $selectfromid=false;
  });
  
 $(window).bind( 'hashchange', function(e) { 
   tabs.each(function(){  
    var idx = $.bbq.getState( this.id, true ) || 0; 
	idx = ($selectfromid)?$selectedtab:idx; 
    $(this).find( tab_a_selector ).eq( idx ).triggerHandler( 'change' );
	$selectfromid=false;
   });
 })
$(window).trigger( 'hashchange' );
});


</script>
<? include_once("includes.php");?>
    <td width="764" valign="top">
		<div id="tabs">
			<ul>
				<li><a href="#doclist" onClick="getCustDocList('showCustDocList.php?mode=show','doclist');">Document List</a></li>
				<li><a href="#inc" onClick="getData('showIncoming.php?mode=new','inc');">Incoming</a></li>
				<li><a href="#outg" onClick="getData('showOutgoing.php?mode=new','outg');">Outgoing</a></li>
				<li><a href="#reports" onClick="getData('showReports.php?mode=show','reports');">Reports</a></li>
			</ul>
			<!-- 	***************************		Document List	********************************************	-->
			<div id="doclist" align="center">
			</div>
			<!-- 	***************************		Incoming		********************************************	-->			
			<div id="inc" align="center">
			</div>
			<!-- 	***************************		Outgoing		********************************************	-->						
			<div id="outg" align="center">
			</div>
			<!-- 	***************************		Reports		    ********************************************	-->			
			<div id="reports" align="center">
			</div>
		</div>	<br />
	</td>