<?php 
@session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
$objGen = new General;

$objGen->displayNotification($_GET['msg']);//for system message

if($_GET['mode']=="trash")
{
	if($objGen->get("userType")==1 || $objGen->get("userType")==2)
	{
		// deleted by document owner
		$rsGroup = $objGen->getCustodianOffice($objGen->get("userID"));
		$sql = "SELECT DISTINCT tblDocument.* FROM tblDocument
		WHERE tblDocument.isDelete=1 AND tblDocument.addedByOfficeId = '".$objGen->get("office")."'";	
		//echo "<br><br>".$sql;
	}
	// deleted by any account from incoming list
	$sql2 = "SELECT DISTINCT tblDocument.*,tblHistory.historyId FROM tblDocument
	LEFT JOIN tblHistory ON tblDocument.documentId = tblHistory.documentId
	LEFT JOIN tblTrash ON tblHistory.historyId = tblTrash.historyId
	WHERE tblTrash.userID = '".$objGen->get("userID")."'";
	
	if($objGen->get("userType")==1 || $objGen->get("userType")==2)
	{	
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
	}
    else
	{
		$sql1= new MySQLHandler();
		$sql1->init();		
		$rs2=$sql1->Select($sql2);	
		//echo $sql."<br><br>".$sql2;
	}
	
	if( count($rs)  || count($rs2) )
	{	
    
	echo '<br><br><table cellpadding="0" cellspacing="0" align="center" class="listings" width="98%" height=99%>
		<tr class="listheader">
		<td align="center" width=140>DOCID</td>
		<td align="center">SUBJECT</td>
		<td align="center" width=30>STATUS</td>
		<td align="center" width=60>DOC TYPE</td>
		<td align="center" width=60>DOC DATE</td>
		<td align="center" width=50>ORIGIN</td>
		<td align="center" colspan=2>&nbsp;</td>
		</tr>';
	for($t=0;$t<count($rs);$t++)
		{
		$intStatus = $objGen->getDocumentStatus($rs[$t]['status']);
		$intDocType = $objGen->getDocumentType($rs[$t]['documentTypeId']);
		$strOfficeName = $objGen->getOfficeName($rs[$t]['originId'],$rs[$t]['officeSig'],$rs[$t]['status'],$rs[$t]['originUnit']);
		$t_dtmDate = $rs[$t]['documentDate']=="0000-00-00"?"-":$rs[$t]['documentDate'];
		$strPageLink = $intStatus=="inc"?"showIncoming.php":"showOutgoing.php";
		$t_strBgcolor = $objGen->altRowBgColor($t);
		$t_strStatus = $intStatus=="inc"?"Incoming":"Outgoing";
		echo '<tr onMouseOver="this.bgColor=\'#FFFFCC\'; this.style.cursor=\'pointer\';" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'" title="SUBJECT: '.$rs[$t]['subject'].'
DOC TYPE: '.$intDocType.'
STATUS: '.$t_strStatus.'">';
		
		echo "<td align='center'><strong><a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$rs[$t]['documentId']."&edit=false','trash');\">&nbsp;".$rs[$t]['documentId']."</a></strong></td>";
		echo	'<td align="center">&nbsp;'.$rs[$t]['subject'].'</td>
				<td style="text-align:center">&nbsp;'.$intStatus.'</td>
				<td align="center">&nbsp;'.$intDocType.'</td>
				<td style="text-align:center">&nbsp;'.$t_dtmDate.'</td>
				<td align=center>&nbsp;'.$strOfficeName.'</td>';
		echo "<td align='center' width=1>
				<span class='ui-icon ui-icon-circle-check' title='Move to Documents' onClick=\"getData('showTrash.php?mode=restore&id=".$rs[$t]['documentId']."&hid=".$rs[$t]['historyId']."','trash');\"></span></td>
				<td align='center' width=1>
				<span class='ui-icon ui-icon-circle-close' title='Delete' onClick=\"getData('showTrash.php?mode=delete&id=".$rs[$t]['documentId']."','trash');\"></span></td>";
		echo " </tr>";		
		}

	for($t=0;$t<count($rs2);$t++)
		{
		$intStatus = $objGen->getDocumentStatus($rs2[$t]['status']);
		$intDocType = $objGen->getDocumentType($rs2[$t]['documentTypeId']);
		$strOfficeName = $objGen->getOfficeName($rs2[$t]['originId'],$rs2[$t]['officeSig'],$rs2[$t]['status'],$rs2[$t]['originUnit']);
		$t_dtmDate = $rs2[$t]['documentDate']=="0000-00-00"?"-":$rs2[$t]['documentDate'];
		$strPageLink = $intStatus=="inc"?"showIncoming.php":"showOutgoing.php";
		$t_strBgcolor = $objGen->altRowBgColor($t);
		$t_strStatus = $intStatus=="inc"?"Incoming":"Outgoing";
		echo '<tr  onMouseOver="this.bgColor=\'#FFFFCC\'; this.style.cursor=\'pointer\';" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'" title="SUBJECT: '.$rs2[$t]['subject'].'
DOC TYPE: '.$intDocType.'
STATUS: '.$t_strStatus.'">';
		echo "<td align='center'><strong><a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$rs[$t]['documentId']."&edit=false','trash');\">&nbsp;".$rs2[$t]['documentId']."</a></strong></td>";
		echo	'<td align="center">&nbsp;'.$rs2[$t]['subject'].'</td>
				<td style="text-align:center">&nbsp;'.$intStatus.'</td>
				<td align="center">&nbsp;'.$intDocType.'</td>
				<td style="text-align:center">&nbsp;'.$t_dtmDate.'</td>
				<td align=center>&nbsp;'.$strOfficeName.'</td>
				';
		echo "<td align='center' width=1>
				<span class='ui-icon ui-icon-circle-check' title='Restore Document' onClick=\"getData('showTrash.php?mode=restore&id=".$rs2[$t]['documentId']."&hid=".$rs2[$t]['historyId']."','trash');\"></span></td>
				<td align='center' width=1>
				<span class='ui-icon ui-icon-circle-close' title='Delete' onClick=\"getData('showTrash.php?mode=delete&id=".$rs2[$t]['documentId']."','trash');\"></span></td>";
		echo " </tr>";		
		}		
echo 	'</table><br><br>';		
	}
	else
		echo "<br><br><br>No records found<br><br><Br>";

}
if($_GET['mode']=="delete")
{
	echo '<br><table border=0>
	<tr><td align=center>Are you sure you want to delete this document with Document ID :'.$_GET['id'].'</td></tr>
	<tr><td style="text-align:center"><input type="button" value="Yes" class="btn" onClick="getData(\'showTrash.php?mode=delete_true&id='.$_GET['id'].'\',\'trash\');">&nbsp;<input type="button" value="No" class="btn" onClick="getData(\'showTrash.php?mode=trash\',\'trash\');"></td></tr></table><br>';
}
if($_GET['mode']=="delete_true")
{
	$sql = "UPDATE tblTrash SET isDelete=1 WHERE historyId='".$_GET['id']."' AND userID='".$objGen->get('userID')."'";	
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Update($sql);
	
	$sql2 = "UPDATE tblDocument SET isDelete=2 WHERE documentId='".$_GET['id']."'";	
	//echo $sql;
	$rs2=$sql1->Update($sql2);		
	if(count($rs) || count($rs2))
		echo "<script language='javascript'>getData('showTrash.php?mode=trash&msg=document: ".$_GET['id']." successfully deleted','trash');</script>";
}
	
if($_GET['mode']=="restore")
{
	$sql = "DELETE FROM tblTrash WHERE historyId='".$_GET['hid']."' AND userID='".$objGen->get("userID")."'";	
	//echo $sql;
	$sql1= new MySQLHandler();
	$sql1->init();
	$rs=$sql1->Delete($sql);	
	$sql2 = "UPDATE tblDocument SET isDelete=0 WHERE documentId='".$_GET['id']."'";
	$sql1->init();	
	$rs2=$sql1->Update($sql2);	
	if(count($rs) || count($rs2))
		echo "<script language='javascript'>getData('showTrash.php?mode=trash&msg=document: ".$_GET['id']." successfully restored','trash');</script>";
}

?>