<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script src="facebox/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
$(function() {
	$('a[rel*=facebox]').facebox({
	loading_image : '/facebox/loading.gif',
	close_image   : '/facebox/closelabel.gif'
	});
	
});

function getChkFlagCustomFolder() {	

if(document.frmCustomFolderDocList.chk.length == undefined)
	{
	if(document.frmCustomFolderDocList.chk.checked)
		getData('showCustomFolder.php?mode=flag&hid='+document.frmCustomFolderDocList.chk.value,'flag0');
	}
else
	{
	for(var i=0; i < document.frmCustomFolderDocList.chk.length; i++){
			if(document.frmCustomFolderDocList.chk[i].checked)
				getData('showCustomFolder.php?mode=flag&hid='+document.frmCustomFolderDocList.chk[i].value,'flag'+i);
		}	
	}
}

function getChkPriorityCustomFolder(fid) {	

if(document.frmCustomFolderDocList.chk.length == undefined)
	{
	if(document.frmCustomFolderDocList.chk.checked)
		getData('showCustomFolder.php?mode=priority&hid='+document.frmCustomFolderDocList.chk.value+'&fid='+fid,'priority0');
	}
else
	{
	for(var i=0; i < document.frmCustomFolderDocList.chk.length; i++){
			if(document.frmCustomFolderDocList.chk[i].checked)
				getData('showCustomFolder.php?mode=priority&hid='+document.frmCustomFolderDocList.chk[i].value+'&fid='+fid,'priority'+i);
		}	
	}
}

function moveCustDocCustomFolder()
{
	
	var custdocs = '';
	if(document.frmCustomFolderDocList.chk.length == undefined)
		{
		if(document.frmCustomFolderDocList.chk.checked)	
			custdocs = document.getElementById('t_strCustomFolderDocId0').value;
		}
	else
		{
		for(var i=0; i < document.frmCustomFolderDocList.chk.length; i++){
			if(document.frmCustomFolderDocList.chk[i].checked)
				{
				var id = 't_strCustomFolderDocId'+i;
				//if(i>1)
					custdocs = custdocs + document.getElementById(id).value + ';';
				//else
					//custdocs = custdocs + ';' + document.getElementById(id).value;	
				//alert('custdocs='+custdocs+'i='+i);	
				}	
		}
	}
//alert(custdocs);	
if(custdocs!="")
	{	
	//document.getElementById('docid').innerHTML=custdocs;
	//document.getElementById('moveid').innerHTML='".$rs[$t]['documentId']."'; 
	//document.getElementById('fid').innerHTML=document.getElementById('t_intFolderID').value;
	//$('#movedialog2').dialog('open');
	//alert($('#t_intOldFolderId').val());
	var url = 'showMoveDialog.php?mode=move&docid='+custdocs+'&oldFolderID='+$('#t_intOldFolderId').val()+'&url=showCustomFolder';
	jQuery.facebox({ ajax: url });
	}
}

  </script>
<?
session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/CustomFolder.class.php");
include_once("class/Search.class.php");
include_once("class/DocDetail.class.php");
require_once("class/IncomingList.class.php");
$objCust = new CustomFolder;
$objSearch = new Search;	
$objDocDetail = new DocDetail;	
$objList = new IncomingList;

if($_GET['mode']=="show")
{
	$objCust->getManageFolderLink();
	$objCust->displayNotification($_GET['msg']);
	$rsCount = $objCust->getCustDocList($objCust->get("office"), $_GET['fid']);
	//echo "rsCount=".count($rsCount);
	if(count($rsCount))
	{
	$total_pages = count($rsCount);
	$addURLString = '#';
	$limit = 10;
	if($page == "") 
		$page = 0;
	else if($page<>"")
		$page = $_GET['page'];
	if($_GET['ascdesc']=="ASC")
		$ascdesc = "DESC";
	else
		$ascdesc = "ASC";		
	}
	$rs = $objCust->getCustDocList($objCust->get("office"), $_GET['fid'], $page, $limit, $_GET['order'], $ascdesc);

	if(count($rs))
		{
			echo "<input type='hidden' id='t_intOldFolderId' value='".$_GET['fid']."'>";
			echo "<ul id='icons' class='ui-widget ui-helper-clearfix'>";
			if($rs[$t]['flag']=='1')
				$t_intFlag = "0";
			else
				$t_intFlag = "1";	
			echo "<li class='ui-state-default ui-corner-all' onClick='getChkFlagCustomFolder();'><span class='ui-icon ui-icon-flag' title='Flag' ></span></li>";

			echo "<li class='ui-state-default ui-corner-all' onClick=\"getChkPriorityCustomFolder('".$_GET['fid']."');\"><span class='ui-icon ui-icon-notice' title='Priority' ></span></li>";
				
			echo "<li class='ui-state-default ui-corner-all' onClick=\" document.getElementById('deleteid').innerHTML='".$rs[$t]['documentId']."';  doDeleteCustomFolder('".$rs[$t]['documentId']."','".$rs[$t]['historyId']."'); \"><span class='ui-icon ui-icon-trash' title='Delete' ></span></li>";
			
			//echo "<li class='ui-state-default ui-corner-all' onClick=\"moveCustDocCustomFolder();   \"><span class='ui-icon ui-icon-copy' title='Move Document' ></span></li>";
			
			echo "<li class='ui-state-default ui-corner-all' onclick=\"moveCustDocCustomFolder();\"><span class='ui-icon ui-icon-copy' title='Move Document' >&nbsp;</span></li></ul>";


echo '<form name="frmCustomFolderDocList" method="post"><div id="data">
<table cellpadding="0" cellspacing="0" align="center" class="listings" width="98%" border="1">
		<tr class="listheader">
		<td width=1% align=center><input type=checkbox onClick=
		\'checkedAll("frmCustDocList");\'></td>
		<td width=2%><span class="ui-icon ui-icon-flag"></td>
		<td width=2%><span class="ui-icon ui-icon-notice"></td>
		<td width=7%>ACTION REQUIRED</td>
		<td width=6%>SENDER</td>		
		<td align="center" width=19%><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=documentId&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>DOCID</a></td>
		<td align="center"><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=subject&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>SUBJECT</a></td>
		<td align="center" width=5%><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=status&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>STATUS</a></td>
		<td align="center" width=8%><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=documentTypeId&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>DOC TYPE</a></td>
		<td align="center" width=8%><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=documentDate&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>DOC DATE</a></td>
		<td align="center" width=7%><a href="#" onClick=\'getData("showCustomFolder.php?mode=show&page='.$_GET['page'].'&order=originId&fid='.$_GET['fid'].'&ascdesc='.$ascdesc.'","doclist");\'>ORIGIN</a></td>		
		</tr>';
	for($t=0;$t<count($rs);$t++)
		{
		$rsDetails = $objCust->getCustDocListDetails($rs[$t]['folderID']);
		$rs2 = $objCust->checkTrash($objCust->get("userID"),$rsDetails[$t]['documentId']);	
		if(!count($rs2))		
		{
		$rsActionRequired = $objDocDetail->getActionRequiredDetails($rsDetails[$t]['actionCodeId']);
		$strActionRequired = $rsActionRequired[0]['actionDesc'];				
		$intStatus = $objCust->getDocumentStatus($rsDetails[$t]['status']);
		$intDocType = $objCust->getDocumentType($rsDetails[$t]['documentTypeId']);
		$strSender = $objCust->getSender($rsDetails[$t]['historyId']);
		$strOfficeName = $objCust->getOfficeName($rsDetails[$t]['originId'],$rsDetails[$t]['officeSig'],$rsDetails[$t]['status'],$rsDetails[$t]['originUnit']);
		$intReadHistory = $objList->checkReadHistory($rsDetails[$t]['documentId'],$objCust->get('userID'));
		if(count($intReadHistory)==0)
			$t_strClass = "class=\"unread\"";
		else
			$t_strClass = "class=\"data\"";
		echo '<tr '.$t_strClass.' onMouseOver="this.bgColor=\'#DDECEF\'" onMouseOut="this.bgColor=\'#FFFFFF\'">';
		
		echo '<td><input type="checkbox" name="chk" id="chk" value="'.$rsDetails[$t]['historyId'].'"><input type="hidden" name="t_strCustomFolderDocId'.$t.'" id="t_strCustomFolderDocId'.$t.'" value="'.$rsDetails[$t]['documentId'].'"></td>';

		// flagging of documents
		if($rs[$t]['flag']=='1')
			echo "<td><div id='flag".$t."'><span class='ui-icon ui-icon-flag' title='Flag' ></span></div></td>";
		else
			echo "<td><div id='flag".$t."'><span>&nbsp;</span>&nbsp;</div></td>";	
		//priority of documents
		if($rs[$t]['priority']=='1')
			echo "<td><div id='priority".$t."'><span class='ui-icon ui-icon-notice' title='Priority' ></span></div></td>";
		else
			echo "<td><div id='priority".$t."'><span>&nbsp;</span>&nbsp;</div></td>";

		echo '<td align=center>'.$strActionRequired.'</td>
				<td align=center>'.$strSender.'</td><td align="center"><a href="#" onClick="getData(\'showIncoming.php?mode=view&id='.$rsDetails[$t]["documentId"].'&fid='.$_GET['fid'].'\',\'doclist\');selecttab(\'#doclist\');">'.$rsDetails[$t]['documentId'].'</a></td>
				<td align="center">&nbsp;'.$rsDetails[$t]['subject'].'</td>
				<td align="center">&nbsp;'.$intStatus.'</td>
				<td align="center">&nbsp;'.$intDocType.'</td>
				<td align="center">&nbsp;'.$rsDetails[$t]['documentDate'].'</td>
				<td align="center">&nbsp;'.$strOfficeName.'</td>';
			  echo '</tr>';		
		}
		}
echo 	'</table><input type="hidden" name="t_intFolderID" id="t_intFolderID" value="'.$_GET['fid'].'"></div></form><br><br>';	
echo $objCust->showPagination($addURLString, $page, $total_pages, $limit);	
}		
	else
		echo "<br><rr><div align=center>No records found</div><br><br>";
}

if($_GET['mode']=="delete_true")
{
	$rs2 = $objCust->trashIncDocument($objCust->get("userID"),$_GET['hid']);	
	//echo "rs2=".$rs2."/userID=".$objCust->get("userID")."/hid=".$_GET['hid'];
}
/*
if($_GET['mode']=="delete_true")
{
	$rs = $objCust->trashDocument($objCust->get("userID"),$_GET['id']);	
	$objCust->trashFromFolder($objCust->get("userID"), $_GET['id']);
	if(count($rs))
		echo "<script language='javascript'>getData('showCustomFolder.php?mode=show&id=".$_GET['id']."&fid=".$_GET['fid']."&msg=document moved to trash','doclist');</script>";
}
*/
if($_GET['mode']=="move")
{
	/*
	//echo "<br>".$_GET['oldID']."/".$_GET['fid']."/".$_GET['id']."<br>";
	$rs = $objCust->moveDocument($_GET['oldID'], $_GET['fid'],$_GET['id']);	
	
	$t_strFolderName = $objCust->getFolderName($_GET['fid']);
	
	if(count($rs))
		echo "<script language='javascript'>getData('showCustomFolder.php?mode=show&id=".$_GET['id']."&fid=".$_GET['oldID']."','doclist');</script>";	
*/

	$t_intID = $_GET['docid'];
	$t_intFolderID = $_GET['fid'];
	$t_intOldFolderID = $_GET['oldID'];
	$arr = explode(';',$t_intID);
	//echo $t_intID."/".$t_intFolderID."/".$t_intOldFolderID;
	//print_r($arr);
	for($i=0;$i<count($arr);$i++)
		{
		$rs = $objCust->moveDocument($t_intOldFolderID,$t_intFolderID,$arr[$i]);	
		//echo $rs."<br>";
		}	
}

if($_GET['mode']=="priority")
{
	$rs = $objList->priorDocument($_GET['hid'], $_GET['t_intPriorStatus']);
	/*
	echo "<script language='javascript'>getData('showCustomFolder.php?mode=show&action=priority&fid=".$_GET['fid']."','doclist');</script>";	
	*/
}

if($_GET['mode']=="flag")
{
	$rs = $objList->flagDocument($_GET['hid']);
	
	if(count($rs))
		echo "<script language='javascript'>getData('showCustomFolder.php?mode=show','doclist');</script>";	
}

if($_GET['mode']=="mark")
{

	if($_GET['mark']=='0')
		$rs = $objList->markUnread($_GET['id'],$objList->get('userID'));
	else if($_GET['mark']=='1')
		$rs = $objList->markRead($_GET['id'],$objList->get('userID'));

}

if($_GET['mode']=="clear")
{
	echo "";
}
?>
