<?php
@session_start();
if($_SESSION['auth_user'] == NULL)
 echo "<script language='javascript'>location.replace('login.php')</script>";
if($_POST['blnBarcodeInput']){
	echo $_POST['barcode'];
}
//$_SESSION["userID"]='0002-CO0-2009 ';
include_once("class/Security.class.php");
require_once("class/jqSajax.class.php");
include_once("class/Incoming.class.php");
require_once('class/MobileDetect.class.php');

$client = new Mobile_Detect;
$objSecurity = new Security;
$file = new Security;
switch($_SESSION['module']){
case "records": $objSecurity->checkRecordUserRights(); break;
case "custodian": $objSecurity->checkCustodianUserRights(); break;
//case "employee": $objSecurity->checkEmployeeUserRights(); break;
}
$ajax=new jqSajax();
$ajax->export("Load","file->Load");
$ajax->export("_decode","file->_decode");
$ajax->export("getTargetFile","file->getTargetFile");
$ajax->export("getTargetFile","file->removeItem");
$ajax->export("checkifConfidential","file->checkifConfidential");
$ajax->export("getFileOrigin","file->getFileOrigin");
$ajax->export("fileExist","file->fileExist");

$ajax->processClientReq();
$objIncoming = new Incoming;
$arDocType=$objIncoming->getDocType("");
?>
<!DOCTYPE HTML>
<html><head>
<title>ERMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
@import url('css/General.css');
@import url('css/datepicker.css');
@import url('css/Validation.css');
@import url('css/scrollable-horizontal.css');
@import url('css/scrollable-buttons.css');
@import url('css/jquery.fancybox.css');
@import url('css/footer.css');
@import url('css/jquery.alerts.css');
@import url('fcbkcomplete/style.css');
<?php if($client->isTablet()) echo "@import url('css/mobile.override.css')"; ?>
</style>
	
<? include("includes.php");?>
<script type="text/javascript"><?php $ajax->showJs(); ?></script>
<?php
switch($_SESSION['module']){
case "records": echo "<script type='text/javascript' src='js/doclist_ajax.js?t=".number_format(microtime(true)*1000,0,'.','')."'></script>"; break;
case "custodian": echo "<script type='text/javascript' src='js/custdoclist_ajax.js?t=".number_format(microtime(true)*1000,0,'.','')."'></script>"; break;
case "employee": echo "<script type='text/javascript' src='js/empdoclist_ajax.js?t=".number_format(microtime(true)*1000,0,'.','')."'></script>"; break;
}
?>


			<style type="text/css">
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			label { display: inline-block; width: 8em; }
			p { margin: 20px 0; }
			li { margin-left: 10px; }
			</style>

</head>
<? 
switch($_SESSION['module']){
 case "records":   $urlRedirect = "getDocList('showDocList.php?mode=show','doclist');"; 
 				   $getData_url = "records.php?mode=show&fid=";
 				   break;
 case "custodian": $urlRedirect = "getCustDocList('showCustDocList.php?mode=show','doclist');"; 
				   $getData_url = "custodian.php?mode=show&fid=";
 				   break;
 case "employee":  $urlRedirect = $objIncoming->get("blnAgencyUser")?
 									"getEmpDocList('showEmpDocList.php?mode=show','doclist');":
										"getEmpDocList('showEmpIncomingList.php?mode=show','doclist');"; 
				   $menuClicked = $objIncoming->get("blnAgencyUser")?'docs':'rdocs';
				   $getData_url = "employee.php?mode=show&fid=&menu=".$menuClicked;
 				   break;
}
if($_GET['mode']=='search')
 $urlRedirect = "getData('showDocList.php?mode=show&t_strSearch=".$_GET['bcode']."&cmbStatus=&cmbGroup=','doclist');"; 
$mainPage = ($client->isTablet())?
	 'employee.php?mode=show&fid=&menu=rdocs':
	   $getData_url;

?>
 
<body onLoad="getData('<? echo $mainPage; ?>','rightPane_content'); <? //pao 022515  echo $urlRedirect;?> statusBar(); $(window).focus();">
<? include("templates/header_logo.php");?>

<table width="877" cellpadding="0" cellspacing="0" align="center" class="textbody">
    <tr> 
    <td height="20" colspan="2" align="right" valign="middle"> 
      <? include("templates/search.php");?>
    </td>
  </tr>
  <tr> 
    <td width="109" height="198" align="center" valign="top"> 
     <? //include("templates/menu.php");?>
     
	 <?php if(!$client->isTablet())  include("templates/menu2.php"); ?>
    </td>
    <td width="764" valign="top">
	<div id="rightPane_content"></div>
	</td>
  </tr>
</table>
<br /><br /><br /><br /><br /><br />
<? 
 if($_SESSION['module']=='records') { include("includes/actionReply.include.php"); }
//include("includes/incomingList.include.php");
if(!$client->isTablet()) include("templates/footer.php");
?>
</body>
</html>
<div id="autobarcode" title="Barcode Notification" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
	
	</p>
</div>
<div id="aboutdialog" title="About ERMS" style="display:none">
<iframe src="includes/About.php" width="550px" height="350px" style="border:none"></iframe>
</div>


<script type="text/javascript">
<?php $ajax->showJs(); ?>
$(document).ready(function(){
<?php if($_SESSION['module']=='records' || $_SESSION['module']=='custodian') { ?>
  var i=0;
  $(window).focus(function(){
   var charCode="";
   var code="";
   var barCode=""; 
  
   $(this).keydown(function(event) {
	 charCode += event.which+',';//String.fromCharCode(event.which);
	 
	 if(event.keyCode==113 && charCode!=''){
	  var arrCharCode = charCode.split(',');
	  var alphanumeric=/^[\-a-zA-Z0-9]+$/;
	  for(char in arrCharCode){
		 code = String.fromCharCode(arrCharCode[char]);
		 if(code.match(alphanumeric) || arrCharCode[char] == 189){
			 barCode+=(arrCharCode[char] == 189)?'-':code;
		 }
	  }
	  barCode = barCode.slice(0,barCode.length-1);
	  getData('showDocList.php?mode=show&t_strSearch='+barCode+'&cmbStatus=&cmbGroup=&chkMyRecord=false&operator=AND','doclist');  
	  charCode=code=barCode=""; 
	 }
   });
  });
  
  $("input[name='barcodesearch']").blur(function(){ $("input[name='barcodesearch']").val(''); });
<?php } ?>
//detects tablet user for compatibility in receive docs module
$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

var divid = "doclist"; 
var url = "showDocList.php?mode=show"; 
var seconds = 90;
var module = "<? echo $_SESSION['module']; ?>";


// Start the refreshing process 
//window.onload = function startrefresh(){ 
//setTimeout('getDocList(url,divid)',seconds*1000); 
//} 
//window.onload = getData("bottom.php?","bottom");
//$(function(){





 if(module == 'records' || module == 'custodian'){ 
  $.ajax({ 
  	url: "manageBarcodeConf.php",
	data: "mode=select&employee_num=<? echo $objIncoming->get('userID'); ?>",
	success: function(data){ 
	 if(data==0){
	   jConfirm('Do you want to enable automatic barcode printing for this account?' ,'Never for this account' , 'ERMS Confirmation Dialog', function(r) 	   {
	   	  var autobarcode_val = (r==true)?1:2;
		  if(r){ 
			$.ajax({ 
			 url: "manageBarcodeConf.php",
			 data: "mode=insert&employee_num=<? echo $objIncoming->get('userID'); ?>&auto_barcode_val="+autobarcode_val,
		  	 success: function(data){ if(r==true) jAlert('You can now print document barcode automatically', 'ERMS Notification');  }
			});		   
		  }
       });
	 }
 	}
  });
 }
//});

//$(document).ready(function(e) {
$("#imgfaq").click(function(e) {
	$.facebox({ajax:"ERMSFAQs/index.php", onClose:function(){
	$("#aboutfacebox").remove();}
});

setTimeout(function(){
$("#aboutfacebox").remove();
$("#facebox").find(".footer").append("<img src='images/About.png'/ id='aboutfacebox' onclick='showAbout()'>");
},500);
});
});
</script>