<div id="accordion">
   
<h3>1.How do I send a document?</h3>
    <div>
       <ol>
<li>To send a document, specify the <strong>Action Taken</strong> and <strong>Action Required</strong> from the drop-down lists in  the <a href="javascript:void(0)" onclick="loadpopup('../Employee/images/Figure3-8UpdateAction.jpg', 'Update Action');">Update Action</a> tab.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency</strong> on the <strong>Action Unit</strong> entry box or specify it from the drop-down list.</li>
<li>Type in <strong>Remarks</strong> (if there is any)  on the entry box provided.</li>
<li>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted.</li>
<li>Place a tick mark on the <strong>Reply Expected</strong> checkbox if reply is expected.</li>
<li>When done, click <strong>Submit</strong> button  . Action made to the document will be displayed in the Update Action tab.</li>

<p align="center"><img src="images/Figure3-9UpdatedDocumentAction.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Updated Document Action</p></div>

</ol>
</div>

<h3>2. How do I update status of a sent document?</h3>
<div>
<?php
include ('../../UserManual/Employee/HowtoAddReply-Emp.php')
?>
</div>

<h3>3. How can I view document history?</h3>
<div>
<?php
include ('../../UserManual/Employee/ViewHistory-Emp.php')
?>
</div>