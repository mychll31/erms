<div id="accordion">
<h3>1. How do I add Incoming document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoAddIncomingDoc-RO.php')
?>
</div>

<h3>2. How do I add Outgoing document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoAddOutgoingDoc-RO.php')
?>
</div>

 <h3>3. How do I send a document?</h3>
    <div>
        <ol>
<li>To send a document, specify the <strong>Action Taken</strong> and <strong>Action Required</strong> from the drop-down lists in the <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-13UPdateAction.jpg', 'Update Action');"> Update Action</a> tab.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency </strong> that will receive the document on the <strong>Action Unit</strong> entry box or specify it from the drop-down list.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted.</li>
<li>Place a tick mark on the <strong>Reply Expected </strong>checkbox if reply is expected.</li>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitButton.jpg" height="20"> . Action made to the document will be displayed in the Update Action tab.</li>

<p align="center"><img src="images/Figure3-14UpdatedDocAction.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Updated Document Action</p></div>
</ol>
</div>

<h3>4. How do I update status of a sent document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoUpdateAction-RO.php')
?>
</div>


<h3>5. How do I receive a document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoReceiveDocs-RO.php')
?>
</div>

<h3>6. How can I reply to a received document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoAddActionReply-RO.php')
?>
</div>

<h3>7. How can I search a document?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoSearchDoc-RO.php')
?>
</div>


<h3>8. How can I upload digitized copy of files?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoUploadDigitizedFiles-RO.php')
?>
</div>

<h3>9. How to add Records Custodian?</h3>
<div>
<?php
include ('../../UserManual/RecordsOfficer/HowtoAddCustodian-RO.php')
?>
</div> 
</div>