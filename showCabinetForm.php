<? @session_start();?>
<script language="javascript" type="text/javascript"> 
var validator='';
  	jQuery("#cabRefresh").click( function(){ 
    	//if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');
		jQuery('#cabinetGrid').GridUnload();
		jQuery(document).ready(function(){
			jQuery("#cabinetGrid").jqGrid({
				url:'xmlparser.php?nd='+new Date().getTime(),
				datatype: "xml",
				colNames:['ID','Label', 'Office Code'],
				colModel:[
					{name:'containerId',index:'containerId', width:65},
					{name:'label',index:'label', width:90},
					{name:'officeCode',index:'officeCode', width:100, hidden:true}
					],
				rowNum:10,
				width: 700,
				rowList:[10,20,30,'All'],
				imgpath: gridimgpath,
				multiselect: true,
				pager: jQuery('#cabPager'),
				postData:{table:'tblContainer',searchField:'officeCode',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew'},
				sortname: 'containerId',
				viewrecords: true,
				sortorder: "asc", //desc
				caption:"File Cabinet"
			}).navGrid('#cabPager',{edit:false,add:false,del:false});
		}); 
  	});
  
    $('#cabClear').click( function(){ 
	   $("#cabId").val(''); 	   
	   $("#cabLabel").val(''); 
	   $("#cabSave").css('display','none');
	   $("#cabAdd").css('display','block');
	   $('#Clear_cabinet').text(" Clear ");
	   $('#cabRefresh').click();
	   validator.resetForm();
    });	
	
//Edit & Save function	
	jQuery("#cabEdit").click( function(){ 
		var id = jQuery("#cabinetGrid").getGridParam('selrow'); 
	
		if (id) 
		{
			var ret = jQuery("#cabinetGrid").getRowData(id); 
			$("#cabId").val(ret.containerId); 	   
			$("#cabLabel").val(ret.label); 
	
			if($("#cabEdit span").attr('class') == "ui-icon ui-icon-pencil")
			{
				$("#cabSave").css('display','block');
				$("#cabAdd").css('display','none');
				$('#Clear_cabinet').text(" Cancel ");
			}
		}
	   else 
	   { 
	   		jAlert('Please select row to edit', 'Warning');
		} 
	}); 

	jQuery("#cabSave").click( function(){ 
		var cabId = $("#cabId").val();
 	  	var label = $("#cabLabel").val();
   	  	var ofCode = "<? echo $_SESSION['office']; ?>";
	    $("#cabinetGrid").setPostData({mode:"save",table:'tblContainer',ID:cabId,CODE:label,DESC:ofCode,MODULE:'Cabinet'});
	    $("#cabinetGrid").trigger("reloadGrid");
 	    $('#cabClear').click();
	});
	
	jQuery("#cabAdd").click( function(){ 
		$('#frmCabinet').submit();
	});
//Add function	   
	//$("#cabAdd").click( function(){ 
	function AddCabinet()
	{
	  	var cabId  = $("#cabId").val();
 	  	var label  = $("#cabLabel").val();
   	  	var ofCode = "<? echo $_SESSION['office']; ?>";
      	$("#cabinetGrid").setPostData({mode:"add",table:'tblContainer',ID:cabId,CODE:label,DESC:ofCode,MODULE:'Cabinet'});
	  	$("#cabinetGrid").trigger("reloadGrid");
  	  	$('#cabClear').click();
	  	jAlert('Succesfully Added','Information'); 
	}
	 
	 
	$("#cabDel").click(function(){ 
	 	var id = jQuery("#cabinetGrid").getGridParam('selarrrow'); //selrow - for 1 row

		if(id!= '')
	 	{ 
  	  		if(id == '1')
			{ 
	   			jAlert('You can not delete this record', 'Warning');
	   			$('#cabClear').click(); }
      		else
			{
		 		jConfirm('Proceed deleting this record?',false, 'ERMS Confirmation Dialog', function(r) {
			 		if(r==true)
					{
			  			$("#cabinetGrid").setPostData({mode:"del",table:'tblContainer',ID:id.toString(),MODULE:'Cabinet'});
			  			$("#cabinetGrid").trigger("reloadGrid"); 
			  			$('#cabClear').click();
			  			jAlert('Succesfully deleted', 'Confirmation Results'); 
			  		} 
				}); 
			} 
		}
		else jAlert('Please select row to delete', 'Warning'); 
	});

    
	$('#cabRefresh').click();
	

//Form validation
 	$().ready(function() {
	 	validator = $("#frmCabinet").validate({
			rules: { cabLabel: "required" },
			messages: {	cabLabel: "Cabinet Label is required" },
			errorPlacement: function(error, element) {
				if ( element.is(":radio") )
					error.appendTo( element.parent().next().next() );
				else if ( element.is(":checkbox") )
					error.appendTo ( element.next() );
				else
					error.appendTo( element.parent().next() );
			},
			submitHandler: function() {	AddCabinet(); },
			success: function(label) { label.html("&nbsp;").addClass("checked"); }
		});
 	});	
	
	
</script>
<form id="frmCabinet" autocomplete="off" method="get">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Cabinet Label</td>
    <td class="field"><input type="text" id="cabLabel" name="cabLabel" /></td>
	<td class="status"></td>
	<input type="hidden" id="cabId" value="">
  </tr>

 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="cabEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="cabDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="cabAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="cabSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="cabClear"><span title='Clear'></span><div id="Clear_cabinet">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="cabRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
<table id="cabinetGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="cabPager" class="scroll" style="text-align:center;"></div>
</form>			