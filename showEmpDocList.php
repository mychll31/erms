<?php
session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/Custodian.class.php");
include_once("class/Incoming.class.php");
include_once("class/CustomFolder.class.php");

$objInc = new Incoming;
$objCust = new Custodian;
$objCustFolder = new CustomFolder;		
$file = new Custodian;

$rsCustodianOffice = $objCust->getOfficeEmployee($objCust->get('userID'));
$arManagedOffice = $objInc->getManagedOffice($objCust->get("userID"));
$arrType= $objInc->getDocType();
for($i=0;$i<sizeof($rsCustodianOffice);$i++)
{
	if($rsCustodianOffice[$i]['groupCode']=="")
		{
		$t_strGroupUnder = $objCust->get('office').$objCust->getDocsUnder($objCust->get("office"));
		$t_strGroupUnder = str_replace("'","",$t_strGroupUnder);
		$rsGroupUnder[$i] = explode(",",$t_strGroupUnder);
		}
	else
		{
		$rsGroupUnder[$i] = $rsGroupUnder[$i].$rsCustodianOffice[$i]['groupCode'];	
		}
}	
  
	  
echo "<table width=100% align=center border=0 cellpadding=0 cellspacing=0>
		<tr>
			<td style='float: right; margin-right: 30px;' width=18% valign=bottom>
			<div data-tip='This is an autocomplete search box'>
			<input type='textbox' class='search_icon' id='t_strSearch' value='".$_REQUEST['t_strSearch']."'></td>			
		</tr>
	  </table>";

$objCust->displayNotification($_REQUEST['msg']);
if($_REQUEST['mode']=="show")
{
	
	$rsGroup = $objCust->getCustodianOffice($objCust->get("userID"));
	
	$rsCount = $objCust->getCustDocList(trim($objCust->getOfficeCode2($objCust->get('userID'))),$rsGroup,$_REQUEST['t_strSearch'], ''/*$_REQUEST['cmbStatus']*/, $_REQUEST['cmbGroup'],"","","","",1,''/*$_REQUEST['cmbDocType']*/);
	$addURLString = '#';
	$limit = 15;
	$page = $page == ""?0:$_REQUEST['page'];
	$ascdesc = $_REQUEST['ascdesc']=="ASC"?"DESC":"ASC";
	$total_pages = $rsCount[0]['_total'];		
	
	$rs = $objCust->getCustDocList(trim($objCust->getOfficeCode2($objCust->get('userID'))),$rsGroup,$_REQUEST['t_strSearch'], $_REQUEST['cmbStatus'], $_REQUEST['cmbGroup'] , $page, $limit, $_REQUEST['order'], $ascdesc,0,$_REQUEST['cmbDocType']);	

	if(count($rs))
		{
echo '<div id="doc_listings">';
echo '<form name="frmCustDocList" method="post">
<table cellpadding="0" cellspacing="0" align="center" class="listings" width="99%" >

		<tr class="listheader" >
		
		<td align="center" width=150><a href="#" onClick=\'getData("showEmpDocList.php?mode=show&page='.$_REQUEST['page'].'&order=documentId&ascdesc='.$ascdesc.'&cmbStatus='.$_REQUEST['cmbStatus'].'","doclist");\'>DOCID</a></td>
		<td align="center" width=50><a href="#"><img style="height:12px; padding-top:2px" src="css/images/file_attachment.png" /></a></td>
		<td align="center" width=50><a href="#">STATUS</a></td>
		
		<td align="center"><a href="#" onClick=\'getData("showEmpDocList.php?mode=show&page='.$_REQUEST['page'].'&order=subject&ascdesc='.$ascdesc.'&cmbStatus='.$_REQUEST['cmbStatus'].'","doclist");\'>SUBJECT</a></td>
		
		<td align="center" width=70><a href="#" onClick=\'getData("showEmpDocList.php?mode=show&page='.$_REQUEST['page'].'&order=documentDate&ascdesc='.$ascdesc.'&cmbStatus='.$_REQUEST['cmbStatus'].'","doclist");\'>DATE</a></td>
		
		<td align="center" width=40><a href="#" onClick=\'getData("showEmpDocList.php?mode=show&page='.$_REQUEST['page'].'&order=originId&ascdesc='.$ascdesc.'&cmbStatus='.$_REQUEST['cmbStatus'].'","doclist");\'>ORIGIN</a></td>
		
		<td align="center" width=3% colspan=2>&nbsp;</td>
		</tr>';

	for($t=0;$t<count($rs);$t++)
		{
		$rs2 = $objCust->checkTrash($objCust->get("userID"),$rs[$t]['documentId']);	
		if(!count($rs2) && !count($rs3))		
		{				
		$intStatus = $objCust->getDocumentStatus($rs[$t]['status']);
		$intDocType = $objCust->getDocumentType($rs[$t]['documentTypeId']);
		$t_strOriginGroup = $rs[$t]['originGroupId']==""?$rs[$t]['officeSig']:$rs[$t]['originGroupId'];
		$t_strOriginId = $rs[$t]['originGroupId']==""?$rs[$t]['originId']:$rs[$t]['originGroupId'];
		$strOfficeName = $objCust->getOfficeName($t_strOriginId,$t_strOriginGroup,$rs[$t]['status'],$rs[$t]['originUnit']);
		
		if($intStatus=="inc") // Incoming
			{$strPageLink= "showIncoming.php";}
		elseif($intStatus=="outg")//Outgoing
			{$strPageLink= "showOutgoing.php";}
		else //Intra
			{$strPageLink= "showIntraOffice.php";}		
		//echo "officename=".$strOfficeName;
		$t_strBgcolor = $t%2?"#F2F2F2":"#FFFFFF";
		$str = $_REQUEST['t_strSearch'];				
		$t_strDocType = $intStatus=="inc"?"Incoming":"Outgoing";
		echo '<tr class="data" onMouseOver="this.bgColor=\'#FFFFCC\'; this.style.cursor=\'pointer\';" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'" title="SUBJECT: '.htmlentities($rs[$t]['subject']).'
STATUS: '.$t_strDocType.'
DOC TYPE: '.$intDocType.'" onClick="getData(\''.$strPageLink.'?mode=view&id='.$rs[$t]["documentId"].'&sender='.$_SESSION['office'].'\',\''.$intStatus.'\');selecttab(\'#'.$intStatus.'\');">';
				$t_strDocId = str_replace(strtoupper($_REQUEST['t_strSearch']),"<font color=red><strong>".strtoupper($_REQUEST['t_strSearch'])."</strong></font>",strtoupper($rs[$t]['documentId']));
				echo '<td align="center"><strong><font size="1px">'.$t_strDocId.'</font></strong></td>';
				
	    $fielExt = end(explode('.', $rs[$t]['attachment']));
		$fileReader = (in_array($fielExt,array("gif", "png", "jpg")))?"":"pdfReader.php?filename=";
		$attachment = $rs[$t]['attachment']!=''?'<a href="'.$fileReader.$objCust->_decode($rs[$t]['path']).DIRECTORY_SEPARATOR.urlencode($rs[$t]['attachment']).'" rel="image" name="0" alt="'.$rs[$t]['attachment'].'" onmouseover="window.status=\'ERMS\'; return true;"><img src="css/images/file_attachment.png" style="height:12px;"></a>':"";
		
		echo "<td style='text-align:center'>".$attachment."</td>";		
				
				echo '<td align="center">'.$intStatus.'</td>';
				$subject = str_replace($str,"<font color=red><strong>".$str."</strong></font>",$rs[$t]['subject']);
				$subject = $objCust->limitText($subject,70);
				$strOfficeName = $strOfficeName<>"-1"?$strOfficeName:"";
				$t_dtmDate = $rs[$t]['documentDate']=="0000-00-00"?"-":$rs[$t]['documentDate'];	
				echo '<td align="center">&nbsp;'.$subject.'</td>

				<td align="center">&nbsp;'.$t_dtmDate.'</td>
				<td align="center">&nbsp;'.$strOfficeName.'</td>';
				echo "
				<td>
					<span class='ui-icon ui-icon-trash' title='Delete' onClick=\"doAgencytDelete('".$rs[$t]['documentId']."');\" ></span>
				</td>";
			  echo '</tr>';		
		}
		}
echo 	'</table></form>';	
echo $objCust->showPagination($addURLString, $page, $total_pages, $limit);	
}		
	else
		echo "No records found";
echo '</div>';		
}

if($_REQUEST['mode']=="delete")
{
	echo '<table border=0>
	<tr><td align=center>Are you sure you want to move this document with Document ID :'.$_REQUEST['id'].' to Trash?</td></tr>
	<tr><td style="text-align:center"><input type="button" value="Yes" class="btn" onClick="getData(\'showEmpDocList.php?mode=delete_true&id='.$_REQUEST['id'].'\',\'doclist\');">&nbsp;<input type="button" value="No" class="btn" onClick="cancelDelete();"></td></tr></table>';
}

if($_REQUEST['mode']=="delete_true")
{
	
	$rs = $objCust->trashDocument($objCust->get("userID"),$_REQUEST['id']);	
	
		echo "<script type='text/javascript' language='javascript'>getData('showEmpDocList.php?mode=show&id=".$_REQUEST['id']."&msg=document moved to trash','doclist');</script>";
		
}

?>
<script type="text/javascript">
$(function() { 
$("a[rel*=image]").fancybox({'frameWidth' : 750,'frameHeight': 600,'overlayShow': true,'overlayOpacity': 0.7,'hideOnContentClick': false});
	$('#t_strSearch').keyup(function(event){
		var keyword=($(this).val() + String.fromCharCode(event.which));
		var operator = (keyword.search("OR") >=0)?'OR':
					   (keyword.charAt(0) == '"')?'CONTAINS':
					   //(keyword.search("-") >= 0)?'EXCEPT':
					   (keyword.search(/\*/g) >= 0)?'DOCTYPE':'AND';

			$( "#doc_listings" ).load( "showEmpDocList.php #doc_listings",{ operator:operator,mode:'show',t_strSearch:$('#t_strSearch').val() }, 
			function(data) {
			  //console.log(data);
			});
	});
	$('form').submit(function(e){ e.preventdefault(); });

});
</script> 