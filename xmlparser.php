<?php
/* 
Name: Xmlparser.php
Author: Hack
Decription: Dynamically Converts Mysql data into xml to 
            be use in Grid from 4 tables 
            (tblActionRequired,tblActionTaken,tblDocumentType,tblOOffice)
*/
// connect to the database
//include_once("class/General.class.php");
require_once("class/Incoming.class.php");
$sql = new MySQLHandler();
$sql->init();	


$examp = $_REQUEST["q"]; // query number
$page = $_REQUEST['page']; // get the requested page
$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
$sord = $_REQUEST['sord']; // get the direction
if(!$sidx) $sidx =1;

$mode = Strip($_REQUEST["mode"]); //mode - add,edit,del,save

$modes = array("add"=>"INSERT","save"=>"UPDATE","del"=>"DELETE"); //use for log section 
$module = Strip($_REQUEST["MODULE"]); // use for log

$table = Strip($_REQUEST["table"]); //table name
$id = explode(',',$_REQUEST["ID"]); //parameter1
$code = Strip($_REQUEST["CODE"]);//parameter2
$desc = Strip($_REQUEST["DESC"]);//parameter3
$period = Strip($_REQUEST["PERIOD"]);//parameter4
$name = Strip($_REQUEST["NAME"]);
$person = Strip($_REQUEST["PERSON"]);
$add = Strip($_REQUEST["ADDRESS"]);
$contact = Strip($_REQUEST["CONTACT"]);
$owner = Strip($_REQUEST["OWNER"]);
//$em = Strip($_REQUEST["EM"]);
$getOffName=Strip($_REQUEST["GETNAME"]);


//for Backup Fields
$timeInternal = Strip($_REQUEST["TIMEINTERNAL"]);
$email = Strip($_REQUEST["EMAIL"]);
$ftp = Strip($_REQUEST["FTP"]);
$fname = Strip($_REQUEST["FNAME"]);
$timectr = Strip($_REQUEST['TIMECTR']);
$location = Strip($_REQUEST['DIR']);
$time_last_run = 0;
//Get Field List
$values = "'$code','$desc'"; 
$fields_query = mysql_query("show fields from ".$table);
while($fields = mysql_fetch_array($fields_query)){
 if(strchr(strtoupper($fields['Field']),'CONTACT') != ''){ $values = "'$name','$contact','$add','$person','$owner'";  break; }
 if(strchr(strtoupper($fields['Field']),'RETENTION')!='' || strchr(strtoupper($fields['Field']),'GROUPCODE')!='' ){ $values =  "'$code','$desc','$period'"; break; }
 if(strchr(strtoupper($fields['Field']),'EMAIL')!=''){ $values =  "'$timeInternal','$ftp','$email','$fname','$timectr','$location','$time_last_run'"; break; }}

$fields_query = mysql_query("show fields from ".$table);
while($fields = mysql_fetch_array($fields_query)){
 if(strchr(strtoupper($fields['Field']),'ID')=='ID') $field_ID = $fields['Field'];
 else $field_list[] = $fields['Field'];}

switch($mode)
{
 case "add": $SQL = "INSERT INTO " . $table . " (" . implode(",  ", $field_list) . ") VALUES ($values)";
 			 mysql_query($SQL);
		     $sql->changelog($SQL,$modes[$mode],$module,"Added new ".$module." information");
 			 break;
 case "del": foreach($id as $ID){ //for multiple row deletion(not workin)
 			  $SQL = "DELETE from ".$table." where $field_ID = '".$ID."'";
			  mysql_query($SQL);}
			  $sql->changelog($SQL,$modes[$mode],$module,"Deleted ".$module." information");
 			  break;
 case "save":if(strlen($contact)>0 OR strlen($name)>0) 
			  $SQL = "UPDATE $table set 
			  					  $field_list[0] = '$name',
								  $field_list[1] = '$contact',
								  $field_list[2] = '$add',
								  $field_list[3] = '$person', 
								  
								  $field_list[4] = '$owner'  
						   where $field_ID = '".$id[0]."'";//$field_list[4] = '$em', 
			 else if(strlen($timeInternal)>0)
			  $SQL = "UPDATE $table set 
			  					  $field_list[0] = '$timeInternal',
								  $field_list[1] = '$email',
								  $field_list[2] = '$ftp',
								  $field_list[3] = '$fname',
								  $field_list[4] = '$timectr',
								  $field_list[5] = '$location'
						   where $field_ID = '".$id[0]."'";
			 else{
			  if(strlen($period)>0) $rPeriod = ",$field_list[2] =  '".$period."'";
			   $SQL = "UPDATE ".$table." set $field_list[0] = '".$code."',$field_list[1] = '".$desc."'".
			 			   $rPeriod."where $field_ID = '".$id[0]."'"; }
				mysql_query($SQL);
				$sql->changelog($SQL,$modes[$mode],$module,"Edited ".$module." information");
			 break;			 
// default :			 
}


$wh = "";
$searchOn = Strip($_REQUEST['_search']);
$search_On = Strip($_REQUEST['searchOn']);
if($searchOn=='true' || $search_On == 'true') {
	$fld = Strip($_REQUEST['searchField']);
	//if( $fld=='documentTypeId' || $fld =='documentTypeAbbrev' || $fld=='documentTypeDesc' || $fld=='retentionPeriod' ) {
		$fldata = Strip($_REQUEST['searchString']);
		$foper = Strip($_REQUEST['searchOper']);
		// costruct where
		$wh .= " where ".$fld;
		switch ($foper) {
			case "bw":
				$fldata .= "%";
				$wh .= " LIKE '".$fldata."'";
				break;
			case "eq":
				$wh .= (is_numeric($fldata))?" = ".$fldata:" = '".$fldata."'";
				break;
			case "ne":
				$wh .=(is_numeric($fldata))?" <> ".$fldata:" <> '".$fldata."'";
				break;
			case "lt":
				$wh .=(is_numeric($fldata))?" < ".$fldata:" < '".$fldata."'";
				break;
			case "le":
				$wh .=(is_numeric($fldata))?" <= ".$fldata:" <= '".$fldata."'";
				break;
			case "gt":
				$wh .=(is_numeric($fldata))?" > ".$fldata:" > '".$fldata."'";
				break;
			case "ge":
				$wh .=(is_numeric($fldata))?" >= ".$fldata:" >= '".$fldata."'";
				break;
			case "ew":
				$wh .= " LIKE '%".$fldata."'";
				break;
			case "cn":
				$wh .= " LIKE '%".$fldata."%'";
				break;
			default :
				$wh = "";
		//}
	}
}

if($table=='tblContainer3') // Added by Emma for library of containers/cabinet/drawers
{
		$result = mysql_query("SELECT COUNT(*) AS count FROM tblContainer3
								 INNER JOIN tblContainer2 ON tblContainer3.container2=tblContainer2.container2Id
								 INNER JOIN tblContainer ON tblContainer2.container=tblContainer.containerId
								 WHERE tblContainer.officeCode = '".$_REQUEST['searchString']."'");
		//echo $result;
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		$count = $row['count'];
		
		if( $count >0 ) {
			$total_pages = ceil($count/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit; // do not put $limit*($page - 1)
		if($start<0) $start=0;
		$LIMIT=($limit == 'All')?"":" LIMIT ".$start." , ".$limit;
		
		
		$SQL = "SELECT tblContainer3.container3Id,tblContainer.label as cabinet,tblContainer3.container2, tblContainer2.label as drawer, tblContainer3.label as container, tblContainer3.container3Id count FROM tblContainer3
					 INNER JOIN tblContainer2 ON tblContainer3.container2=tblContainer2.container2Id
					 INNER JOIN tblContainer ON tblContainer2.container=tblContainer.containerId
					 WHERE tblContainer.officeCode = '".$_REQUEST['searchString']."' ".$LIMIT; 
		
		//echo $SQL;
		$result = mysql_query( $SQL ) or die("Couldn't execute query.".mysql_error()."<br>".$SQL);
		
		if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8"); } else {
		header("Content-type: text/xml;charset=utf-8");
		}
		$et = ">";
		
		echo "<?xml version='1.0' encoding='utf-8'?$et\n";
		echo "<rows>";
		echo "<page>".$page."</page>";
		echo "<total>".$total_pages."</total>";
		echo "<records>".$count."</records>";
		// be sure to put text data in CDATA
		while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
			echo "<row id='". $row[$field_ID]."'>";			
				echo "<cell>". $row[$field_ID]."</cell>";
				echo "<cell><![CDATA[". $row['cabinet']."]]></cell>";
				echo "<cell>". $row['container2']."</cell>";
				echo "<cell><![CDATA[". $row['drawer']."]]></cell>";
				echo "<cell>". str_replace("&", "&amp;", $row['container'])."</cell>";	
			echo "</row>";
		}
		echo "</rows>";			
}
else
{
		$result = mysql_query("SELECT COUNT(*) AS count FROM $table ".$wh);
		//echo $result;
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		$count = $row['count'];
		
		if( $count >0 ) {
			$total_pages = ceil($count/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit; // do not put $limit*($page - 1)
		if($start<0) $start=0;
		$LIMIT=($limit == 'All')?"":" LIMIT ".$start." , ".$limit;
		$SQL = "SELECT * FROM $table ".$wh." ORDER BY ".$sidx." ". $sord.$LIMIT; 
		
		//echo $SQL;
		$result = mysql_query( $SQL ) or die("Couldn't execute query.".mysql_error()."<br>".$SQL);
		
		if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8"); } else {
		header("Content-type: text/xml;charset=utf-8");
		}
		$et = ">";
		
		echo "<?xml version='1.0' encoding='utf-8'?$et\n";
		echo "<rows>";
		echo "<page>".$page."</page>";
		echo "<total>".$total_pages."</total>";
		echo "<records>".$count."</records>";
		// be sure to put text data in CDATA
		while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
			echo "<row id='". $row[$field_ID]."'>";			
			echo "<cell>". $row[$field_ID]."</cell>";
			if($getOffName=="true") $field0=getName($row[$field_list[0]]);
			else $field0=$row[$field_list[0]];
			echo "<cell><![CDATA[". $field0."]]></cell>";
			echo "<cell><![CDATA[". $row[$field_list[1]]."]]></cell>";
			if(strlen($field_list[2])>0)
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[2]])."</cell>";
			if(strlen($field_list[3])>0)	
			 echo "<cell>". str_replace("&", "&amp;", $row[$field_list[3]])."</cell>";	
			if(strlen($field_list[4])>0)	
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[4]])."</cell>";	
			if(strlen($field_list[5])>0)	
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[5]])."</cell>";
			if(strlen($field_list[6])>0)	
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[6]])."</cell>";
			if(strlen($field_list[7])>0){	
			   $obj_inc = new Incoming();
			   $officeName  = $obj_inc->getOriginOffice($row[$field_list[7]]);
			   $field_value = ($officeName[0]['officeName']!=''?$officeName[0]['officeName']:($row[$field_list[7]]==0?"":$row[$field_list[7]]));
			   echo "<cell>". str_replace("&", "&amp;",$field_value)."</cell>";
			}
			if(strlen($field_list[8])>0)	
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[8]])."</cell>";
			if(strlen($field_list[9])>0)	
			echo "<cell>". str_replace("&", "&amp;",$row[$field_list[9]])."</cell>";
			
			echo "</row>";
		}
		echo "</rows>";		
}

mysql_close($db);

function Strip($value)
{
	if(get_magic_quotes_gpc() != 0)
  	{
    	if(is_array($value))  
			if ( array_is_associative($value) )
			{
				foreach( $value as $k=>$v)
					$tmp_val[$k] = stripslashes($v);
				$value = $tmp_val; 
			}				
			else  
				for($j = 0; $j < sizeof($value); $j++)
        			$value[$j] = stripslashes($value[$j]);
		else
			$value = stripslashes($value);
	}
	return $value;
}

function getName($offName)
{
$SQL="SELECT group1Name as officeName FROM tblGroup1 WHERE group1Code='$offName'";
$sql1 = new MySQLHandler2();
$sql1->init();
$row=$sql1->Select($SQL);
if(count($row)==0){
$SQL="SELECT group2Name as officeName FROM tblGroup2 WHERE group2Code='$offName'";
$sql2 = new MySQLHandler2();
$sql2->init();
$row=$sql2->Select($SQL);
}
if(count($row)==0){
$SQL="SELECT group3Name as officeName FROM tblGroup3 WHERE group3Code='$offName'";
$sql3 = new MySQLHandler2();
$sql3->init();
$row=$sql3->Select($SQL);
}
if(count($row)==0){
$SQL="SELECT group4Name as officeName FROM tblGroup4 WHERE group4Code='$offName'";
$sql4 = new MySQLHandler2();
$sql4->init();
$row=$sql4->Select($SQL);
}
if(count($row)==0){
$SQL="SELECT group5Name as officeName FROM tblGroup5 WHERE group5Code='$offName'";
$sql5 = new MySQLHandler2();
$sql5->init();
$row=$sql5->Select($SQL);
}
if(count($row)==0) return $offName;
return $row[0]['officeName'];
}
function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}
?>

