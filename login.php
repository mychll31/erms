<?php 
include("class/Login.class.php");
$login = new Login;
$msg = $login->validateAccount();
?>

<html>
<head>
<title>E-Record Management System Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/footer.css" rel="stylesheet" type="text/css"/>
<style>
label{
	font-size:12px; 
	font-family:Arial, Helvetica, sans-serif; 
	font-weight:bold; 
	/*color:#CCCCCC;*/
	color:#A0BDA9;
}
.diverms {
	background: #102602;
    padding: 10px;
    line-height: 21px;
    font-size: small;
    font-weight: normal;
}
</style>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow:hidden" onLoad="setFocus();">
<!-- ImageReady Slices (Main Login page7-26-2010.psd) -->
<form id="frmLogin" name="frmLogin" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
<table width="100%" cellspacing="0" cellpadding="0" border="0" background="login3/rightside_bg.gif" height="98%" >
<tr>
<td width='50%' style="height:649;background:url('login3/leftside_bg.gif'); BACKGROUND-REPEAT: repeat-x;"></td>
<td align="center">
<table id="Table_01" width="1002" height="649" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="login3/erms_loginpage_01.jpg" width="248" height="40" alt=""></td>
		<td colspan="2">
			<img src="login3/erms_loginpage_02.jpg" width="383" height="40" alt=""></td>
		<td colspan="6">
			<img src="login3/erms_loginpage_03.jpg" width="370" height="40" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="login3/erms_loginpage_04.jpg" width="249" height="2" alt=""></td>
		<td rowspan="2">
			<img src="login3/erms_loginpage_05.jpg" width="382" height="93" alt=""></td>
		<td colspan="6" rowspan="2">
			<img src="login3/erms_loginpage_06.jpg" width="370" height="93" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="login3/erms_loginpage_07.jpg" width="248" height="91" alt=""></td>
		<td>
			<img src="login3/erms_loginpage_08.jpg" width="1" height="91" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="91" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="login3/erms_loginpage_09.jpg" width="248" height="20" alt=""></td>
		<td colspan="2">
			<img src="login3/erms_loginpage_10.jpg" width="383" height="20" alt=""></td>
		<td colspan="6">
			<img src="login3/erms_loginpage_11.jpg" width="370" height="20" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="20" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="3">
			<img src="login3/erms_loginpage_12.jpg" width="633" height="61" alt=""></td>
		<td colspan="2">
			<img src="login3/erms_loginpage_13.jpg" width="273" height="1" alt=""></td>
		<td colspan="3" rowspan="2">
			<img src="login3/erms_loginpage_14.jpg" width="95" height="60" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<img src="login3/erms_loginpage_15.jpg" width="273" height="60" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="59" alt=""></td>
	</tr>
	<tr>
		<td rowspan="8">
			<img src="login3/erms_loginpage_16.jpg" width="42" height="362" alt=""></td>
		<td colspan="2" rowspan="2">
			<img src="login3/erms_loginpage_17.jpg" width="53" height="149" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" rowspan="8" valign="top"><span style="color:#FFFFFF"><br><br><h3>Welcome to DOST Electronic Records Management System</h3>
		
		<ul type="circle">
		<li> Automated management of records on a centralized approach</li>
		<li>Tracking and notification of the document status and current location</li>
		<li>Handling of digitized document</li>
		<li>Report Generation</li>
		<li>Integrated with HRMIS</li>
		</ul>

		<!-- begin tracking sheet -->
		<fieldset style="padding: 10px; width: 250px; position: absolute;">
			<legend><b>Document Status</b></legend>
			Number:<br>
			<input type="Text" id="txtDocumentNumber" name="" maxlength="20">
			<input type="button" value="Search" id="checkstatus">
			<br><br>
			<div class="diverms">
				Date: <span id="spnDateSent"></span><br>
				Action Taken: <span id="spnActionTaken"></span><br>
				From: <span id="spnFrom"></span><br>
				To: <span id="spnTo"></span><br>
				Action Required: <span id="spnActionRequired"></span>
				<!-- Received BY: <span id="spnReceivedBy"></span><br> -->
			</div>
			<div class="diverms-warning" style="color: #e27d5c"></div>
		</fieldset>
		<!-- end tracking sheet -->

		</span>
			</td>
		<td colspan="2" rowspan="2">
			<img src="login3/erms_loginpage_19.jpg" width="52" height="149" alt=""></td>
		<td rowspan="2">
			<img src="login3/erms_loginpage_20.jpg" width="223" height="149" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="148" alt=""></td>
	</tr>
	<tr>
		<td rowspan="5">
			<img src="login3/erms_loginpage_21.jpg" width="6" height="212" alt=""></td>
		<td rowspan="7">
			<img src="login3/erms_loginpage_22.jpg" width="47" height="287" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<img src="login3/erms_loginpage_23.jpg" width="52" height="171" alt=""></td>
		<td>
			<!--img src="login3/erms_loginpage_24.jpg" width="223" height="170" alt=""-->
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#0B541D">
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td colspan="2"><label for="t_strUsername"> Username:</label></td></tr>
			<tr><td colspan="">
			<input type="text" class='input-login' id='t_strUsername' 
                        onkeydown="if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('ibtnLogin').click();return false;}} else {return true}; " 
                        name='t_strUsername'>
			</td></tr>
			<tr><td colspan="2"><label for="t_strPassword"> Password:</label></td></tr>
			<tr><td colspan="2"><input class='input-login' id='t_strPassword'
                        onkeydown="if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('ibtnLogin').click();return false;}} else {return true}; " 
                        type='password' name='t_strPassword'></td></tr>
			<tr valign="middle"><td colspan="2" width="100%" height="10px"><img src="login3/erms_loginpage_hr.jpg"></td></tr>
			<tr><td><input id="RememberMeTextBox" type='checkbox' name="RememberMeTextBox" class="chkbox">
            		<label for="RememberMeTextBox">Remember me</label>
            </td><td><input id='ibtnLogin'  onMouseOver="this.src='login3/login_mouseover.gif'" onMouseOut="this.src='login3/login_button.gif'"
                        onclick="javascript:return validateLogin();" type='image' class="image"
                        src="login3/login_button.gif" name="ibtnLogin" width="60" height="25"> 
						</div>
						<input type="hidden" name="Submit" value="Log-in"></td></tr>
						<tr><td colspan="2">&nbsp;</td></tr>
			</table>
			</td>
		<td>
			<img src="login3/spacer.gif" width="1" height="170" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="login3/erms_loginpage_25.jpg" width="223" height="41" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="login3/erms_loginpage_26.jpg" width="52" height="38" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="login3/erms_loginpage_27.jpg" width="2" height="77" alt=""></td>
		<td>
			<img src="login3/erms_loginpage_28.jpg" width="50" height="2" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<img src="login3/erms_loginpage_29.jpg" width="273" height="75" alt=""></td>
		<td rowspan="2">
			<img src="login3/erms_loginpage_30.jpg" width="6" height="75" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="login3/erms_loginpage_31.jpg" width="42" height="74" alt=""></td>
		<td>
			<img src="login3/spacer.gif" width="1" height="74" alt=""></td>
	</tr>
</table>
</td>
<td width='50%'><div style="height:649;background:url('login3/rightside_bg.gif'); BACKGROUND-REPEAT: repeat-x;"></div></td>
</tr>
<tr><td  colspan="3" width="100%" ><div  id="botts" style="background:url('login3/bottom_bg.gif'); BACKGROUND-REPEAT: repeat; height:100%"></div></td></tr>
</table></FORM>
<!-- End ImageReady Slices -->
<div id="footpanel" style="width:100%; margin:0 0 0 0;">Electronic Records Management System. All Rights Reserved</div>
</body>
</html>
<script type="text/javascript" src="js/General.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jsmd5.js"></script>
<script type="text/javascript">   

    setCookie('sysNotification','0',365);	
	function setFocus() {
		var loginForm = document.getElementById("frmLogin");
		loginForm.t_strUsername.select();
		loginForm.t_strUsername.focus();
	}
	
	function validateLogin()
		    {
			    //var PUserId    = document.getElementById("ctl00_Cphmiddlecontent_UserIDTextBox");
			    var PUserId    = document.getElementById("t_strUsername");
			    //var PassWord    = document.getElementById("ctl00_Cphmiddlecontent_PasswordTextBox");
			    var PassWord    = document.getElementById("t_strPassword");
			    //var frm = document.LogInForm; 
    			
			    if((PUserId.value) == "")
			    {
				    alert("Enter User Name");
				    PUserId.focus();
				    return false;
			    }
    						
			    if((PassWord.value) =="")
			    {
				    alert("Enter Password");
				    PassWord.focus();
				    return false;
			    }
				var submitForm = document.getElementById("frmLogin");
				submitForm.t_strPassword.value = md5(submitForm.t_strPassword.value);
				submitForm.submit();
		    } 
			function OpenPopUp(UserId)
			{
			  var nLeft	= 0;
			  var nTop	= 0;
			  var nWidth	= 660;
			  var nHeight	= 440;
			  nLeft		= (window.screen.width - nWidth)/2;
			  nTop		= (window.screen.height - nHeight)/2;
			  window.open("ChangeLoginId.aspx?UserId="+UserId, "UserName", "width=" + nWidth + ", height=" + nHeight + ", location=no,left=" + nLeft + ",top=" + nTop + ", menubar=no, status=no, toolbar=no, scrollbars=yes, resizable=yes");
			  return false;
			
			}   
<?php
 if($msg<>""){
   echo 'alert("'.$msg.'");';
 }
?>

$('.diverms').hide();
$('.diverms-warning').hide();
$('#checkstatus').click(function(){
	if($('#txtDocumentNumber').val() == ''){
		$('.diverms').hide();
		$('.diverms-warning').show();
		$('.diverms-warning').html('ERMS Number must not be empty.');
	}else{
		$.ajax({
			url: 'Tracking.php?id='+$('#txtDocumentNumber').val(),
			success: function(data) {
				console.log(data);
				if(data == 'confidential'){
					$('.diverms').hide();
					$('.diverms-warning').show();
					$('.diverms-warning').html('You seem to have input a wrong or confidential document number, please check the number and try again.');
				}else{
					$('.diverms').show();
					$('.diverms-warning').hide();
					ermsDocument = JSON.parse(data);
					$('#spnDateSent').html(ermsDocument.dateAdded);
					$('#spnActionTaken').html(ermsDocument.actionTaken);
					$('#spnFrom').html(ermsDocument.sender);
					$('#spnActionRequired').html(ermsDocument.actionRequired);
					$('#spnTo').html(ermsDocument.recipient);
					// $('#spnReceivedBy').html('');
				}
			}
		});
	}
});
</SCRIPT>
