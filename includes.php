<? 
@session_start(); 
include_once("class/ERMS_define.php");
?>
<link href="css/General.css" rel="stylesheet" type="text/css"/>
<link href="css/datepicker.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/Validation.css" />
<link rel="stylesheet" type="text/css" href="css/scrollable-horizontal.css">
<link rel="stylesheet" type="text/css" href="css/scrollable-buttons.css">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
<link href="css/footer.css" rel="stylesheet" type="text/css"/>
<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />

<style type="text/css">

a:active {
  outline:none;
}

:focus {
  -moz-outline-style:none;
}

a.navi {
	margin-top:55px;
}

/*** override styling in external stylesheets ***/



/* remove margins from the image */
.items img {
	margin:0;
}

/* make A tags our floating scrollable items */
.items a {
	display:block;
	float:left;
	margin:15px;
}

/* this is how you can show the tooltips above the scrollable */
.scrollable {
/*	padding-top:120px;
	margin-top:-120px;
*/
	border:0px;
}

/* tooltip styling */

#tooltip {
	display:none;
	background:transparent url(css/images/black_arrow.png) no-repeat;
	filter:alpha(opacity=90);
	opacity: 0.9;
	font-size:9px;
	height:70px;
	width:160px;
	padding:22px;
	color:#fff;	
}

#tip {
	padding:5px 10px;
	border:1px solid #195fa4;
	background:#195fa4 url(images/bg2.gif) repeat-x;
	color:#fff;
}


/* scrollable should not disable gallery navigation */
#gallery .disabled {
	visibility:visible !important;
}

#gallery .inactive {
	visibility:hidden !important;
}
.ui-datepicker{ font-size:12px;}
#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
ul#icons {margin: 0; padding: 0;}
ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
ul#icons span.ui-icon {float: left; margin: 0 4px;}


</style>
<link href="style.css" rel="stylesheet" type="text/css">
<!--[if IE]>
<style type="text/css">
#tooltip {
	background-color:#fff;
}
@import "http://static.flowplayer.org/css/ie6.css?v=0.2";
</style>
<![endif]-->

<!--if lt IE 8>
<style>
	html { overflow-x:hidden; }
	#content ol, #content ul  {margin-left: 50px; }
</style>
<endif-->

<!--[if ie 7]>
<style type="text/css">
#fixed { 
	position: fixed; 
	bottom:1px; 
	left: auto; 
	color:#111111;
	right:auto; 
	font-size:11px; 
	border:	1px solid #A6C9E2;
	width:90%; 
	height:25px; 
	text-align: left; 
}
</style>
<![endif]-->

		<script type="text/javascript" src="js/General.js"></script>
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
		
		<script src="js/jquery.tools.min.js"></script>
	    <script src="js/reflection.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.jgrowl.js"></script>
		<script type="text/javascript" src="js/jquery.uploadify.js"></script>				
		<script type="text/javascript" src="js/jquery.fancybox-1.2.1.js"></script>				
		<script type="text/javascript" src="js/tooltip.min.js"></script>						
		<script type="text/javascript" src="js/suggest.js"></script>
		<script type="text/javascript" src="js/suggestion.js"></script>
	    <script type="text/javascript" src="js/jquery.validate.js"></script>		

		<script src="js/jquery.alerts.js" type="text/javascript"></script>
		<script src="facebox/facebox.js" type="text/javascript"></script>
		<script src="fcbkcomplete/fcbkcomplete.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/jquery.address-1.4.min.js" type="text/javascript"></script>		
		<script type="text/javascript" src="js/datepicker.js"></script>        
		<script type="text/javascript" src="js/datepicker.extension.js"></script>        
		<script type="text/javascript" src="js/multiple_function_inc.js"></script>
        <script src="js/jquery.layout.js" type="text/javascript"></script>
        <script src="js/i18n/grid.locale-en.js" type="text/javascript"></script>
        <script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="js/jquery.tablednd.js" type="text/javascript"></script>
        <script src="js/jquery.contextmenu.js" type="text/javascript"></script>
        <script src="js/Notification.js" type="text/javascript"></script>        
		<script type="text/javascript">
			var gridimgpath = 'images/themes/basic/images';
		    var popupStatus = 0;	
			var docID=0,strfileID='',itemID='',fileman=0,divID='',addID='',docSender='';
			var html='',htm='';
			var $selectedtab=0;
			var $selectfromid=false;
			html += '<div id="popupContact">';
			html += '<p id="contactArea"><div id="header">';
			html += '<ul><a id="popupContactClose"><img src="images/close.png"></a><li><a href="#header"><b id="head"></b></a></li>';
			html += '</ul><div id="tab">';
			html += '<div id ="winContent">';
			html += '</div></div></div></p></div>';
			html += '<div id="backgroundPopup"></div>';
			
			htm += '<div id="popWin">';
			htm += '<p id="contactArea"><div id="popWinHeader">';
			htm += '<ul><a id="popWinClose"><img src="images/close.png"></a><li><a href="#header"><b>Attach File(s)</b></a></li>';
			htm += '</ul><div id="tab">';
			htm += '<div id ="popWinContent">';
			htm += '<fieldset style="border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0">';
			htm += '<legend>Add Attachment</legend><div id="fileUpload">You have a problem with your javascript. Please install <a href="https://get.adobe.com/flashplayer/?no_redirect">Adobe Flash Player</a> to continue</div>';
			htm += '<br></fieldset><fieldset id="upbtn" style="border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0; display:none;">';
			htm += '<input type="button" onClick="javascript:startUpload(\'fileUpload\');" value="Start Upload">';
			htm += '<input type="button" onClick="javascript:$(\'#fileUpload\').fileUploadClearQueue(); $(\'#upbtn\').css(\'display\',\'none\' ); $(\'#popupContact\').css(\'height\',\'243\');" value="Cancel">';
			htm += '<p></p></fieldset>';
			htm += '</div></div></div></p></div>';
			htm += '<div id="bgPopWin"></div>';

			$(function(){
				$('#content').html(html);
				$('#popWinContent').html(htm);
				// Tabs
				$('#tabs').tabs();
				$('#header').tabs(); 
			    $('#head').tabs();
				$('#popWinHeader').tabs();
				$("#folders").accordion({ header: "h3" });
				$("#folders2").accordion({ header: "h3" });
				
				$('a[rel*=facebox]').facebox({
        			loading_image : 'facebox/loading.gif',
        			close_image   : 'facebox/closelabel.gif'
      			}); 
				
		  
        
				//$('#icons, #icons li').hover(
					//function() { $(this).addClass('ui-state-hover'); }, 
					//function() { $(this).removeClass('ui-state-hover'); }
				//);
				
				$('#movedialog').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"Cancel": function() { 
							$(this).dialog("close"); 
						}, 
						"Move": function() { 
							$(this).dialog("close"); 
							var docID = document.getElementById("docid").innerHTML;
							var f2ID = document.getElementById("fid").innerHTML;
							var fID = document.getElementById("t_strFolder").options[document.getElementById("t_strFolder").selectedIndex].value;
							if(fID!='')
								getData('showIncomingList.php?mode=move&id='+docID+'&fid='+fID+'&f2ID='+f2ID+'','intra'); 			
							else
								{
								alert('Please Select Folder');
								$('#movedialog').dialog('open');
								}				
						} 
					}
				});

				$('#movedialog2').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"Cancel": function() { 
							$(this).dialog("close"); 
						}, 
						"Move": function() { 
							$(this).dialog("close"); 
							var docID = document.getElementById("moveid").innerHTML;
							var oldID = document.getElementById("fid").innerHTML;
							var fID = document.getElementById("t_strFolder2").options[document.getElementById("t_strFolder2").selectedIndex].value;
							if(document.frmCustDocList.chk.length == undefined)
								{
								getData('showCustomFolder.php?mode=move&id='+docID+'&fid='+fID+'&oldID='+oldID,'doclist'); 	getData('showCustomFolder.php?mode=show&fid='+oldID,'doclist');						
								}
							else
								{
							for(var i=0; i < document.frmCustDocList.chk.length; i++){
								if(document.frmCustDocList.chk[i].checked)
									{
									getData('showCustomFolder.php?mode=move&id='+docID+'&fid='+fID+'&oldID='+oldID,'doclist'); 		getData('showCustomFolder.php?mode=show&fid='+oldID,'doclist');					
									}
							}
							}							
							
						} 
					}
				});
				
				$('#deleteDialog').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"No": function() { 
							$(this).dialog("close"); 
						}, 
						"Yes": function() { 
							$(this).dialog("close");
							var docID = document.getElementById("deleteid").innerHTML;
							getData('showCustDocList.php?mode=delete_true&id='+docID,'doclist'); 
						} 
					}
				});				

				$('#deleteDialog2').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"No": function() { 
							$(this).dialog("close"); 
						}, 
						"Yes": function() { 
							$(this).dialog("close");
							var docID = document.getElementById("deleteid2").innerHTML;
							var fID = document.getElementById("deletefid").innerHTML;
							getData('showCustomFolder.php?mode=delete_true&id='+docID+'&fid='+fID,'doclist'); 
						} 
					}
				});
				
				

				$('#receiveDialog').dialog({
					autoOpen: false,
					width: 400,
					height: 220,
					resizable: false,
					modal: true,
					buttons: {
						"Cancel": function() { 
							$(this).dialog("close"); 
						}, 
						"Receive": function() { 
							$(this).dialog("close");
							var docID = document.getElementById("receiveid").innerHTML;
							var hID = document.getElementById("hid").innerHTML;
							var rb = document.getElementById("t_strReceivedBy").value;
							getData('showIncomingList.php?mode=receive&id='+docID+'&hid='+hID+'&t_strReceivedBy='+rb,'intra');							
							
						} 
					}
				});	


				$('#moveEmpDialog').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"Cancel": function() { 
							$(this).dialog("close"); 
						}, 
						"Move": function() { 
							$(this).dialog("close"); 
							var docID = document.getElementById("docid").innerHTML;
							//var f2ID = document.getElementById("fid").innerHTML;
							var fID = document.getElementById("t_strFolder").options[document.getElementById("t_strFolder").selectedIndex].value;
							if(document.frmEmpDocList.chk.length == undefined)
								{
								getData('showEmpDocList.php?mode=move&id='+docID+'&fid='+fID,'doclist'); 							getData('showEmpDocList.php?mode=show','doclist');
								}
							else
								{
							for(var i=0; i < document.frmEmpDocList.chk.length; i++){
								if(document.frmEmpDocList.chk[i].checked)
									{
									getData('showEmpDocList.php?mode=move&id='+docID+'&fid='+fID,'doclist'); 						getData('showEmpDocList.php?mode=show','doclist');	
									}
							}
							}
						} 
					}
				});
				
				$('#deleteEmpDialog').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"No": function() { 
							$(this).dialog("close"); 
						}, 
						"Yes": function() { 
							$(this).dialog("close");
							var docID = document.getElementById("deleteid").innerHTML;
							getData('showEmpDocList.php?mode=delete_true&id='+docID,'doclist'); 
						} 
					}
				});				
				

				$('#movePermission').dialog({
					autoOpen: false,
					width: 400,
					height: 200,
					resizable: false,
					modal: true,
					buttons: {
						"Close": function() { 
							$(this).dialog("close"); 
						} 
					}
				});

				$('#viewDocDialog').dialog({
					autoOpen: false,
					width: 650,
					height: 500,
					resizable: false,
					modal: true,
					position: 'center',
					buttons: {
						"Close": function() { 
							$(this).dialog("close"); 
						}
					}
				});

			});
			//end

		


		
			function selecttab(anchorbutter)
			{
			$('#tabs').tabs('select',anchorbutter);
			$selectedtab=$('#tabs').tabs( "option", "selected" );
			$selectfromid = true;
			}
			
			function startUpload(id)
			{
			$('#'+id).fileUploadSettings('scriptData','&id='+docID+'&folder=UPLOAD&office=<? echo $_SESSION['office']; ?>');
			$('#'+id).fileUploadStart();
			}
			
			function showUploadWindow(ID,whouploaded){
				docID = ID;
				fileman = whouploaded;
			    $('#upbtn').css("display","none" );
			    $('#popWin').css("height","243");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = $("#popWin").height();
				var popupWidth = $("#popWin").width();
				//centering
				$("#popWin").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2
				});
				//only need force for IE6
				
				$("#bgPopWin").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#bgPopWin").css({
						"opacity": "0.7"
					});
					$("#bgPopWin").fadeIn("slow");
					$("#popWin").fadeIn("slow");
					popupStatus = 1;
				}
			}
			
			function showReceiveDialog(ID,hID){
				var docID = ID;
				var historyID = hID

				var html='';				
				html += '<br><br><div id="receiveDialog" align="center"><br><br><table><tr><td colspan=2>Document ID  : '+docID+'</td></tr><tr><td align=right>Received By:</td><td align=left><input type="text" name="t_strReceivedBy" id="t_strReceivedBy"></td></tr><tr><td colspan=2 align=center><br><input type="button" value="Receive" onclick="doReceiveDialog(\''+docID+'\',\''+historyID+'\')"></td></tr></table><br><br><br>';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Receive Document");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 290;
				var popupWidth = 290;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			}
			
			function showSetDeadlineDialog(ID, DOCID, MODE, DIVID, HISTORYDIV){
				var id = ID;
				var docid = DOCID;
				var mode = MODE;
				var divid = DIVID;
				var historydiv = HISTORYDIV;

				var html='';				
				html += '<br><br><div id="SetDeadlineDialog" align="center">';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Set Own Deadline");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 150;
				var popupWidth = 350;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("showSetDeadline.php?ID="+id+'&DOCID='+docid+'&MODE='+mode+'&DIVID='+divid+'&HISTORYDIV='+historydiv,"SetDeadlineDialog"); 
			}
			
			function showDeleteDialog(ID){
				var docID = ID;
				var html='';				
				html += '<br><br><div id="deleteDialog" align="center"><br><br><table><tr><td align=center>Document ID  : '+docID+'</td></tr><tr><td align=center>Are you sure you want to move this document to Trash?</td></tr><tr><td align=center><input type="button" value="Yes" onClick="doDelete(\''+docID+'\');"><input type="button" value="No" onClick="cancelDelete();"></td></tr></table><br><br><br>';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Delete Document");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 250;
				var popupWidth = 320;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			}			

	function show201Dialog(ID){
				var docID = ID;

				var html='';				
				html += '<br><br><div id="201Dialog" align="center">';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Update 201");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 250;
				var popupWidth = 320;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("show201.php?mode=new&ID="+ID,"201Dialog");
			}

			function showActionReplyDialog(ID, DOCID, MODE, DIVID, HISTORYDIV){
				var id = ID;
				var docid = DOCID;
				var mode = MODE;
				var divid = DIVID;
				var historydiv = HISTORYDIV;

				var html='';				
				html += '<br><br><div id="ActionReplyDialog" align="center">';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Add Action Reply");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 270;
				var popupWidth = 350;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("showActionReply.php?mode=actionReply&ID="+id+'&DOCID='+docid+'&MODE='+mode+'&DIVID='+divid+'&HISTORYDIV='+historydiv+'&ADDSOURCE=1',"ActionReplyDialog"); 
			}
			
			function showActionReplyDialog2(ID, DOCID, MODE, DIVID, HISTORYDIV){
				var id = ID;
				var docid = DOCID;
				var mode = MODE;
				var divid = DIVID;
				var historydiv = HISTORYDIV;

				var html='';				
				html += '<br><br><div id="ActionReplyDialog" align="center">';
				html += 'hi</div>';
				$('#winContent').html(html);
				$('#head').html("Action Reply");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 270;
				var popupWidth = 350;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("showActionReply.php?mode=actionReply&ID="+id+'&DOCID='+docid+'&MODE='+mode+'&DIVID='+divid+'&HISTORYDIV='+historydiv+'&ADDSOURCE=2',"ActionReplyDialog"); 
			}
			
			function showActionReplyDetails(ID, DOCID, MODE, DIVID, HISTORYDIV){
				var id = ID;
				var docid = DOCID;
				var mode = MODE;
				var divid = DIVID;
				var historydiv = HISTORYDIV;

				var html='';				
				html += '<br><br><div id="ActionReplyDetailsDialog" align="center">';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Action Reply Details");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 270;
				var popupWidth = 350;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("showActionReplyDetails.php?mode=actionReplyDetails&ID="+id+'&DOCID='+docid+'&MODE='+mode+'&DIVID='+divid+'&HISTORYDIV='+historydiv+'&ADDSOURCE=2',"ActionReplyDetailsDialog"); 
			}

			function showActionSourceDetails(ID, DOCID, MODE, DIVID, HISTORYDIV){
				var id = ID;
				var docid = DOCID;
				var mode = MODE;
				var divid = DIVID;
				var historydiv = HISTORYDIV;

				var html='';				
				html += '<br><br><div id="ActionReplyDetailsDialog" align="center">';
				html += '</div>';
				$('#winContent').html(html);
				$('#head').html("Action Source Details");
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupHeight = 270;
				var popupWidth = 350;
				//centering
				$("#popupContact").css({
					"position": "absolute",
					"top": windowHeight/2-popupHeight/2,
					"left": windowWidth/2-popupWidth/2,
					"width": popupWidth,
					"height": popupHeight
				});
				
				//only need force for IE6
				
				$("#backgroundPopup").css({
					"height": windowHeight
				});
				
				if(popupStatus==0){
					$("#backgroundPopup").css({
						"opacity": "0.7"
					});
					$("#backgroundPopup").fadeIn("slow");
					$("#popupContact").fadeIn("slow");
					popupStatus = 1;
				}
			getData("showActionReplyDetails.php?mode=actionSourceDetails&ID="+id+'&DOCID='+docid+'&MODE='+mode+'&DIVID='+divid+'&HISTORYDIV='+historydiv+'&ADDSOURCE=2',"ActionReplyDetailsDialog"); 
			}


			function cancelDelete()
			{
				$("#backgroundPopup").fadeOut("slow");
				$("#popupContact").fadeOut("slow");
				popupStatus = 0;
			}

			$(document).ready(function(){
			 $("#fileUpload").fileUpload({
					'uploader': 'images/uploader.swf',
					'cancelImg': 'images/cancel.png',
					'script': 'upload.php',
					'multi': false,
					'displayData': 'speed',
					'buttonImg': 'images/browseBtn.png',
					'fileExt': '*.pdf;*.jpg;*.gif;*.png;*.ppt;*.pptx;*.doc;*.docx;*.txt;*.ppt;*.pptx;*.xls;*.xlsx',
					//'fileDesc':'Allowed Files(*.pdf;*.jpg;*.gif;*.png;*.ppt;*.pptx;*.doc;*.docx;*.txt;*.ppt;*.pptx;*.xls;*.xlsx)',
					'width': 80,
					'height': 24,
					'rollover': true,
					onComplete: function (evt, queueID, fileObj, response, data) {
					    response = response.split('*');
						alert("Successfully uploaded: "+response[0]); 
						$('#upbtn').css("display","none" );
						$('#popWin').css("height","243");
						console.log(response);
						if(response[1]=='confidential'){
						 $.ajax({ type: "GET",
						 		  //url: "http://<? #echo constant($_SESSION['office']); ?>/UPLOADLOCAL/getRemoteFile.php", 
						 		  url: "http://<?php if ($_SERVER['HTTP_CLIENT_IP']!=''){ $ip = $_SERVER['HTTP_CLIENT_IP']; }elseif ($_SERVER['HTTP_X_FORWARDED_FOR']!=''){ $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; }else{ $ip = $_SERVER['REMOTE_ADDR']; } echo $ip; ?>/UPLOADLOCAL/getRemoteFile.php", 
								  data: "filename="+response[0]+"&mode=create&office=<? echo $_SESSION['office']; ?>&s=<? echo ERMS_SERVER; ?>&tfile="+response[2]+"&dateY=<? echo date('Y'); ?>&dateF=<? echo date('F'); ?>&dated=<? echo date('d'); ?>",
						 		  dataType: "script",
								  success: function() { $.ajax({ type: "GET", url: "upload.php", data: { mode:'destroy' } }) 
								  }
						 }); 
						}
						if(fileman==1) { showManageFiles(docID,divID,addID,docSender); }
				    },
					onSelect: function (evt, queueID, fileObj) {
					  $('#upbtn').css("display","block" );
					  $('#popWin').css("height","300");
					}
				});
   		    });



			$(document).ready(function(){
				$("#popupContactClose").click(function(){
				  if(popupStatus==1){
					$("#backgroundPopup").fadeOut("slow");
					$("#popupContact").fadeOut("slow");
					popupStatus = 0;
				}
				});
				//Click out event!
				$("#backgroundPopup").click(function(){
				  if(popupStatus==1){
					$("#backgroundPopup").fadeOut("slow");
					$("#popupContact").fadeOut("slow");
					
					popupStatus = 0;
				}
				});
				//Press Escape event!
				$(document).keypress(function(e){
					if(e.keyCode==27 && popupStatus==1){
  					 if(popupStatus==1){
						$("#backgroundPopup").fadeOut("slow");
						$("#popupContact").fadeOut("slow");
						
						popupStatus = 0;
					 }
					}
				});
				
				$("#popWinClose").click(function(){
				  if(popupStatus==1){
					$("#bgPopWin").fadeOut("slow");
					$("#popWin").fadeOut("slow");
					$('#fileUpload').fileUploadClearQueue();
					popupStatus = 0;
				}
				});
				//Click out event!
				$("#bgPopWin").click(function(){
				  if(popupStatus==1){
					$("#bgPopWin").fadeOut("slow");
					$("#popWin").fadeOut("slow");
					
					popupStatus = 0;
				}
				});
				//Press Escape event!
				$(document).keypress(function(e){
					if(e.keyCode==27 && popupStatus==1){
  					 if(popupStatus==1){
						$("#bgPopWin").fadeOut("slow");
						$("#popWin").fadeOut("slow");
						
						popupStatus = 0;
					 }
					}
				});
			
			});


function addItem(docID) {
//	showUploadWindow(docID);
/*	api.getItemWrap().append(
		'<img src="img/thumb/Letter.php" />'
	);
*/	
//	api.reload();
var api = $("div.scrollable").scrollable();
loadName();

}
function displayDialog(lnk)
	{
	var url = lnk;
	url = url;
	jQuery.facebox({ ajax: url });	
	}
function removeItem() {
var api = $("div.scrollable").scrollable();
var reply=confirm("Delete this file?");
var confile = $.x_file_checkifConfidential(docID);
var tfile = $.x_file_getTargetFile(strfileID);
var file='',i=0;
if(reply==true){ 
// if($.x_file_fileExist(tfile,confile)){
  if(confile){
    tfile = tfile.split('/');   
    for(i=4;i<tfile.length;i++) file += '/'+tfile[i];
    //OLDCODE:: $.ajax({ type: "GET",url: "http://<? #echo constant($_SESSION['office']); ?>/UPLOADLOCAL/getRemoteFile.php?filename="+file+"&mode=destroy",dataType: "script" });
	$.ajax({ type: "GET",url: "http://<? if ($_SERVER['HTTP_CLIENT_IP']!=''){ $ip = $_SERVER['HTTP_CLIENT_IP']; }elseif ($_SERVER['HTTP_X_FORWARDED_FOR']!=''){ $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; }else{ $ip = $_SERVER['REMOTE_ADDR']; } echo $ip; ?>/UPLOADLOCAL/getRemoteFile.php?filename="+file+"&mode=destroy",dataType: "script" });
   }  
   if($.x_file_removeItem(strfileID,confile))
   api.getItems().eq(itemID).remove();
   alert('succesfully deleted');
   showManageFiles(docID,divID,addID,docSender);
// }
// else alert("Can't delete file, File doesn't exist");
 }
}

function next(){
	var api = $("div.scrollable").scrollable();
	api.nextPage();
}

function prev(){
	var api = $("div.scrollable").scrollable();
	api.prevPage();
}

function loadFiles(addanchor) //hack100609
{
var html='',k=0,msg='';
var arr = $.x_file_Load(docID).replace(/['"]/g,'').split('*');
var log_office = "<? echo $_SESSION['office']; ?>";
var log_user = "<? echo $_SESSION['empNum']; ?>";
var r= arr[0];
var c= arr[1];
var a= (arr[2]!='~')?arr[2]:"";

msg = "<strong>Notice: </strong>NO FILE(S) UPLOADED.";
var file_owner = $.x_file_getFileOrigin(docID);

if($.x_file_checkifConfidential(docID)){ //Error msg if the server is down.
 console.log($.x_file_fileExist(docID,1));
 if($.x_file_fileExist(docID,1)){
   $('#loading_widget').hide();
   if(file_owner == log_office){
   	// begin coll.related doc
   		if(addID != 'addIncreldocs')
   			$('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');
   	// end coll.related doc
   }
   //COMMENTED $('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');
 }
 else{ 
    $('#loading_widget').hide();      
	msg=(file_owner == log_office)?"<strong>Error: </strong>"+file_owner+" File Server is down. You can not upload/view file.":
		"<strong>Error: </strong>"+file_owner+" File Server is down. You can not view file.";

    $('#' + addID).html('');
    a = false;
 }
}

else{ 
  $('#loading_widget').hide();
	// begin coll.related doc
  	if(addID != 'addIncreldocs')
  		$('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');
  	// end coll.related doc	
  //COMMENTED $('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');	   
}

	if(a){ 
		a = a.split('~');
		console.log(a);
		console.log('<?php echo $_SESSION["empNum"];?>');
		html += '<div class="scrollable"><div class="items">';
		$('#' + divID).attr('align','');	
		for(i=0;i<r;i++){
		  var col = i*c;
		  var path = $.x_file__decode(a[4+col]);
		  var title = "title: <b><i>"+ a[2+col] +"</i></b><br> upload date: "+ a[6+col];
		  if(docSender == log_office || log_user == a[5]) //dissallow delete feature to unauthorized employee
		   title += "<br><br><a title='Delete' onclick='removeItem();'><img src='images/cancel-12.png' align='buttom'/></a>";
		  
		  if(a[2+col].search('.pdf') > 0 || a[2+col].search('.txt') > 0) //PDF file
		  { 
		   src = (a[2+col].search('.pdf') > 0)?'images/pdf.jpg':'images/notepad.jpg'; 
		   html+='<a id="'+ a[0+col] +'" href="pdfReader.php?filename='+path+'/'+a[2+col].replace(/&/g,"%26")+'" rel="image" name="'+ i +'" alt="'+a[2+col]+'" title="'+ title +'" onmouseover="window.status=\'ERMS\'; return true;">';
		   html+='<img src="'+ src +'" class="reflected" style="border: 0px none ; display: block;" /></a>'; 
		  }
		  else //ms office documents
		  {
		   var src='"'+path+'/'+a[2+col]+'"';
		   var rel="image"; 
		   var onclickEvent="";
		   if(a[2+col].search('.doc') > 0 || a[2+col].search('.docx') > 0)
		   {
			rel = ''; 
			src = 'images/word.jpg'; 
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }	  		
		   if(a[2+col].search('.xls') > 0 || a[2+col].search('.xlsx') > 0)
		   {
			rel = ''; 
			src = 'images/excel.png'; 
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }
		   if(a[2+col].search('.ppt') > 0 || a[2+col].search('.pptx') > 0)
		   {
			rel = ''; 
			src = 'images/ppt.jpg';  
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }	
		   if(a[2+col].search('.rar') > 0 || a[2+col].search('.zip') > 0)
		   {
			rel = ''; 
			src = 'images/rar.png';  
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }	
		   html+='<a id="'+a[0+col]+'" onclick="'+ onclickEvent +'" href="'+ path+'/'+ a[2+col] +'" rel="'+ rel +'" title="'+ title +'" name="'+ i +'" onMouseOver="window.status=\'ERMS\'; return true;">';
		   html+='<img src='+ src +' class="reflected" style="border: 0px none ; display: block;" /></a>';  
	   	  } //end ms office documents
		}//end for
	html+='</div></div>';
	
	html+='<div style="position: absolute; top: -110px; left: -2px; display: none; text-align:left" id="tooltip">';
	//html+='<a href="#" onclick="removeItem();"><img src="images/cancel.png" align="buttom"/></a>';	
	html+='</div>';
	
	if(r>5){
		html+='<p style="margin: 15px 0pt 0pt 37px;text-align:center;">';
		html+='<input type="submit" class="btn" onclick="prev();" value="&lt;&lt; previous" />';
		html+='<input type="submit" class="btn" onclick="next();" value ="next &gt;&gt;   " />';
		html+='</p>'; 
	}
		html+='<br clear="all">';
  }//end if(a)
  else{
	$('#' + divID).attr('align','center');	
	html+='<div class="ui-widget" style="width:55%" align="center">';
	html+='<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
	html+='<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
	html+=msg+'</p>';
	html+='</div></div><br>';
  }
$('#' + divID).html(html);
}


function showManageFiles(documentid,anchorID,addanchor,sender){
  docID = documentid; divID = anchorID; addID=addanchor; docSender = sender;
  //$('input.btn').remove();
  if($('#'+anchorID).css('display')== 'block') {
	 $('#loading_widget').show(100,function(){ 
		setTimeout( function() { 
		    loadFiles(); 		
			$("a[rel*=image]").fancybox({
				 'frameWidth' : 750, 
				 'frameHeight': 600, 
				 'overlayShow': true, 
				 'overlayOpacity': 0.7,
				 'hideOnContentClick': false,
				 'modal' : true,
				 'showNavArrows' : false
			}); 
		
			$.easing.bouncy = function (x, t, b, c, d) { 
				var s = 1.70158; 
				if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b; 
				return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b; 
			} 
			$.tools.tooltip.addEffect("bouncy", 
				function(done) { 
					this.getTip().animate({top: '+=15'}, 500, 'bouncy', done).show(); 
				}, 
				function(done) { 
					this.getTip().animate({top: '-=15'}, 500, 'bouncy', function()  { 
						$(this).hide(); 
						done.call(); 
					}); 
				} 
			);
			//$(".scrollable img").reflect({height: 0.5, opacity: 0.6});
			var items = $(".scrollable ").scrollable({item: 'a'}).mousewheel().find("a").tooltip({
				tip: '#tooltip',
				effect: 'bouncy', //'toggle'
				offset: [0, 5]
			});
			items.each(function() { 
				$(this).tooltip().onBeforeShow(function() {
					var visible = $(".scrollable").scrollable().getVisibleItems()
						 offset = this.getConf().offset,
						 trigger = this.getTrigger();
						 strfileID = trigger.attr('id');
						 itemID = trigger.attr('name');
				})
			});
		}, 2000 );
	});
  }
//});
}

// related doc files
function loadFilesreldocs(addanchor) //hack100609
{
var html='',k=0,msg='';
var arr = $.x_file_Load(docID).replace(/['"]/g,'').split('*');
var log_office = "<? echo $_SESSION['office']; ?>";
var log_user = "<? echo $_SESSION['empNum']; ?>";
var r= arr[0];
var c= arr[1];
var a= (arr[2]!='~')?arr[2]:"";

msg = "<strong>Notice: </strong>NO FILE(S) UPLOADED.";
var file_owner = $.x_file_getFileOrigin(docID);

if($.x_file_checkifConfidential(docID)){ //Error msg if the server is down.
 if($.x_file_fileExist(docID,1)){
   $('#loading_widget2').hide();
   if(file_owner == log_office)
   		if(addID != 'addIncreldocs')
   			$('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');
 }
 else{ 
    $('#loading_widget2').hide();
	msg=(file_owner == log_office)?"<strong>Error: </strong>"+file_owner+" File Server is down. You can not upload/view file.":
		"<strong>Error: </strong>"+file_owner+" File Server is down. You can not view file.";

    $('#' + addID).html('');
    a = false;
 }
}

else{ 
  $('#loading_widget2').hide();
  if(addID != 'addIncreldocs')
  	$('#' + addID).html('<input type="submit" value="Add New" class="btn" onclick="showUploadWindow(\''+docID+'\',1);" style="display:'+ $('#'+divID).css("display") +'"/>');	   
}

	if(a){ 
		a = a.split('~');
		console.log(a);
		console.log('<?php echo $_SESSION["empNum"];?>');
		html += '<div class="scrollable"><div class="items">';
		$('#' + divID).attr('align','');	
		for(i=0;i<r;i++){
		  var col = i*c;
		  var path = $.x_file__decode(a[4+col]);
		  var title = "title: <b><i>"+ a[2+col] +"</i></b><br> upload date: "+ a[6+col];
		  if(docSender == log_office || log_user == a[5]) //dissallow delete feature to unauthorized employee
		   title += "<br><br><a title='Delete' onclick='removeItem();'><img src='images/cancel-12.png' align='buttom'/></a>";
		  if(a[2+col].search('.pdf') > 0 || a[2+col].search('.txt') > 0) //PDF file
		  { 
		   src = (a[2+col].search('.pdf') > 0)?'images/pdf.jpg':'images/notepad.jpg'; 
		   html+='<a id="'+ a[0+col] +'" href="pdfReader.php?filename='+path+'/'+a[2+col].replace(/&/g,"%26")+'" rel="image" name="'+ i +'" alt="'+a[2+col]+'" title="'+ title +'" onmouseover="window.status=\'ERMS\'; return true;">';
		   html+='<img src="'+ src +'" class="reflected" style="border: 0px none ; display: block;" /></a>'; 
		  }
		  else //ms office documents
		  {
		   var src='"'+path+'/'+a[2+col]+'"';
		   var rel="image"; 
		   var onclickEvent="";
		   if(a[2+col].search('.doc') > 0 || a[2+col].search('.docx') > 0)
		   {
			rel = ''; 
			src = 'images/word.jpg'; 
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }	  		
		   if(a[2+col].search('.xls') > 0 || a[2+col].search('.xlsx') > 0)
		   {
			rel = ''; 
			src = 'images/excel.png'; 
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }
		   if(a[2+col].search('.ppt') > 0 || a[2+col].search('.pptx') > 0)
		   {
			rel = ''; 
			src = 'images/ppt.jpg';  
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }		
		   if(a[2+col].search('.rar') > 0 || a[2+col].search('.zip') > 0)
		   {
			rel = ''; 
			src = 'images/rar.png';  
			onclickEvent = "javascript:location.href = '"+path+"/"+a[2+col]+"'"; 
		   }
		   html+='<a id="'+a[0+col]+'" onclick="'+ onclickEvent +'" href="'+ path+'/'+ a[2+col] +'" rel="'+ rel +'" title="'+ title +'" name="'+ i +'" onMouseOver="window.status=\'ERMS\'; return true;">';
		   html+='<img src='+ src +' class="reflected" style="border: 0px none ; display: block;" /></a>';  
	   	  } //end ms office documents
		}//end for
	html+='</div></div>';
	
	html+='<div style="position: absolute; top: -110px; left: -2px; display: none; text-align:left" id="tooltip">';
	//html+='<a href="#" onclick="removeItem();"><img src="images/cancel.png" align="buttom"/></a>';	
	html+='</div>';
	
	if(r>5){
		html+='<p style="margin: 15px 0pt 0pt 37px;text-align:center;">';
		html+='<input type="submit" class="btn" onclick="prev();" value="&lt;&lt; previous" />';
		html+='<input type="submit" class="btn" onclick="next();" value ="next &gt;&gt;   " />';
		html+='</p>'; 
	}
		html+='<br clear="all">';
  }//end if(a)
  else{
	$('#' + divID).attr('align','center');	
	html+='<div class="ui-widget" style="width:55%" align="center">';
	html+='<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
	html+='<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
	html+=msg+'</p>';
	html+='</div></div><br>';
  }
$('#' + divID).html(html);
}
// related doc files

// begin show related docs files
function showManageFilesReldocs(documentid,anchorID,addanchor,sender){
  docID = documentid; divID = anchorID; addID=addanchor; docSender = sender;
  if($('#'+anchorID).css('display')== 'block') {
  	loadFilesreldocs();
  }
}
// end show related docs files

function checkNotification(module,office)
{
 arrInfo = $.x_file_checkNotification(module,office);
 if(arrInfo!=''){
	if($('#footnot').attr('src') == 'images/Notification.png')
     $('#footnot').attr('src','images/Notification2.png');
	else $('#footnot').attr('src','images/Notification.png'); }
}

function loading(load_status)
{	
	if(load_status=='1' && $('#loading').css('display')=="none")				
	{
		$('#loading').show();
	}
	if(load_status=='0' && $('#loading').css('display')=="block")	
	{
		$('#loading').hide();				
	}
		
}

function getPageData(url,div)
{
	loading(1);
	$.ajax ({
		type: "POST",
		url: url,
		cache: false,
		success: function(html)
		{					
		$("#"+div).html(html);
		loading(0);
		} 
	});		
}
</script>

<div id="content"></div>
<div id="popWinContent"></div>