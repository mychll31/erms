<?php 
// ini_set('display_errors','On');
//@session_start();
session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
$objGeneral = new General;
//$arrFields2 = $objDocDetail->getUserFields();

/////////////////////////////////
//filename 		:	showHstory.php
//description	:	ajaxpage for saving deleting document history
//parameters	:	mode = new for Get; mode="" for post
//					docID for document ID
//					div for the container div of this ajaxpage
//other param	:	note(this parameters for delete only)
//					ac = del for delete action
//					ID = historyid for history id to be deleted
/////////////////////////////////

if($_GET['mode']=="") 

$arrFields2 = $objDocDetail->getUserFields();

else $arrFields2 = $objDocDetail->getUserFields1();

$docID = $arrFields2['docID'];
$divId = $arrFields2['div'];
$arrFields = $objDocDetail->getDocDetails($docID);
$mode = $arrFields2['mode']; //  incoming ; outgoing ; intra
$historydiv = $arrFields2['historydiv'];
//echo $mode;
$refID = "";
$chkReply =  "checked";
$chkRestricted = "checked";
$cmbActReq = "";
$hideActionForm = TRUE;
if($_GET['ac']=="reply") 
{
	
	$refID = $_GET['ID']; // set for Reference ID
	$rsReply = $objDocDetail->getHistoryActionSource($refID);
	
	$strRecipientUnit=$rsReply[0]['recipientUnit'];
	$strRecipientId = $rsReply[0]['recipientId'];
	
	$strSenderUnit=$rsReply[0]['senderUnit']; 
	$strSenderId = $rsReply[0]['senderId'];	
	
	$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
	$senderName = $objDocDetail->displayRecipientSenderNames($strSenderId, $strSenderUnit);

	$chkReply =  "checked"; 
	$cmbActReq = "selected";
	$chkRestricted = "checked";
	$hideActionForm = FALSE; // this will show the Form
	 
	
// Need to check if Reply's Sender is also the Sender of the Action
// use Recipient
 	if ($objDocDetail->isCurrUserActionSender($refID))
	{
		if ($strRecipientUnit=='employee')	$intActionUnitSwitch= 1 ;
		else if ($strRecipientUnit=='agency')	$intActionUnitSwitch= 3 ;
		else $intActionUnitSwitch= 2 ;
		$strSenderUnit = $strRecipientUnit;
		$strSenderId = $strRecipientId;
		$senderName = $recipientName;
	}else{
		if ($strSenderUnit=='employee')	$intActionUnitSwitch= 1 ;
		else if ($strSenderUnit=='agency')	$intActionUnitSwitch= 3 ;
		else $intActionUnitSwitch= 2 ;
	}

	if ($rsReply[0]['restricted']==FALSE) $chkRestricted = "unchecked";
	if ($refID==0) $chkRestricted = "checked";
	
}

if($_GET['ac']=="add") 
{
	$refID = $_GET['ID'];
	
}
//print_r($arrFields2);

?>

<!-- cut here -->
<script language="JavaScript" id="historyjs">
		$(function(){
			$.facebooklist('#txtActionUnit', '#preadded', '#facebook-auto',{url:'fcbkcomplete/fetched2.php',cache:1}, 10, {userfilter:1,casesensetive:0});
			//$('.maininput').attr('disabled',"<?= $objDocDetail->get("blnAgencyUser"); ?>"==1?'disabled':''); //disable Action Unit if Agency User
			//$('#cmbActionTaken').val("<?= $objDocDetail->get("blnAgencyUser"); ?>"==1?10:0).attr('disabled',"<?= $objDocDetail->get("blnAgencyUser"); ?>"==1?'disabled':'');//set value to 'Processed' then disable for Agency User
			$('#cbRestricted').attr('disabled',"<?= $objDocDetail->get("blnAgencyUser"); ?>"==1?'disabled':''); //disable for Agency User
		});

		

		function test()
		{
		objcheckboxes=$("INPUT[type=hidden]");
		errortrap=true;
		$.each(objcheckboxes,function(index,value){
			var tmpocode=value.value;
			errortrap=false;
			if(value.id=="txtActionUnit[]") alert(tmpocode);
		});		
		
		}
		
		function msgAddAction(divID, historyDIV, docID)
		{
			 var msg= confirm("Add Action?"); 
			if(msg){
			 getData("showAction.php?ac=reply&ID=0"+'&div='+divID+'&historydiv='+historyDIV+'&docID='+docID+'&mode=del',divID) ;
			 } else 
			 return (false);
		}

 


		function enableOfficeCombo(oForm)
		{
			
			oForm["cmbEmpNumber"].disabled = true;
			oForm["cmbOfficeCode"].disabled = false;
			oForm["cmbAgency"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[0].checked = true;
		}
	
		function enableNameCombo(oForm)
		{
			oForm["cmbEmpNumber"].disabled = false;
			oForm["cmbOfficeCode"].disabled = true;
			oForm["cmbAgency"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[1].checked = true;
		}
		
		function enableAgencyCombo(oForm)
		{
			
			oForm["cmbAgency"].disabled = false;
			oForm["cmbEmpNumber"].disabled = true;
			oForm["cmbOfficeCode"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[2].checked = true;
		}
		
		function validate(formname)
		{
			//alert($('#txtActionUnit').val());return false;
			var oForm = document.forms[formname];
			if(oForm.elements["cmbActionTaken"].options[oForm.elements["cmbActionTaken"].selectedIndex].value == "-1")
			{
				alert("Please select Action Taken!");
				document.getElementById("cmbActionTaken").focus();
				return false
			}else if(oForm.elements["cmbActionRequired"].options[oForm.elements["cmbActionRequired"].selectedIndex].value == "-1")
			{
				alert("Please select Action Required!");
				document.getElementById("cmbActionRequired").focus();
				return false
			}
			else if(oForm.elements["actionUnitSwitch"][0].checked==false && oForm.elements["actionUnitSwitch"][1].checked==false && oForm.elements["actionUnitSwitch"][2].checked==false)
			{
				alert("Please select Action Unit!");
				//document.getElementById("actionUnitSwitch").focus();
				return false
			}
		}
		
		function validatePopup(formname)
		{
			var oForm = document.forms[formname];
			if(oForm.elements["cmbActionTaken"].options[oForm.elements["cmbActionTaken"].selectedIndex].value == "-1")
			{
				alert("Please select Action Taken!");
				document.getElementById("cmbActionTaken").focus();
				return false
			}else if(oForm.elements["cmbActionRequired"].options[oForm.elements["cmbActionRequired"].selectedIndex].value == "-1")
			{
				alert("Please select Action Required!");
				document.getElementById("cmbActionRequired").focus();
				return false
			}
		
		}

	/*
		

		function doAction(id,docid)
		{
		getData("showActionReply.php?mode=actionReply&ID="+id+'&DOCID='+docid,"dialogcontent"); 
		//alert(document.getElementById("ID").innerHTML);
		} */
</script>
					<?php
					
				// ADDING A RECORD
				//$arrFields2 = $objDocDetail->getUserFields();

						if($t_submit)
						{
						//print_r($_POST);
						//return false;	
						$res = $objDocDetail->addAction($_POST);								
						//print_r($arrFields2);
						
							if(count($res)==0)
							{
								//$intActionCodeId = $arrFields2['cmbActionRequired'];
								//$intActionUnitSwitch = $arrFields2['actionUnitSwitch'];
								//$strRemarks = $arrFields2['txtRemarks'];
							}
							else 
								{
								foreach($res as $row)
									{										
									$objDocDetail->sendEmail($row); //email recipient for Notification
									}
								}
							$msg = $objDocDetail->getValue('msg');
							/*
							echo "<script language=\"JavaScript\">
							if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
							getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
							</script>";
							*/
							$hideActionForm = TRUE;
						}
						
							
						// DELETING A RECORD
						
						if($_GET['ac']=='del')
						{
							$objDocDetail->deleteAction($_GET['ID'], $_GET['docID']);
							$msg = $objDocDetail->getValue('msg');
							echo "<script language=\"JavaScript\">
							if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
							getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
							</script>";
							$refID = "";
						}
						
						?>
						<? if($msg<>""){?>
						<div class="pane" style="position:absolute; width:50%;">
						<div class="ui-widget" style="width:40%">
							<div  class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; "> 
								<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
								<strong>Notice: </strong><? echo $msg;?></p>
							</div>
						</div>
						</div>
						<script type="text/javascript">
						setTimeout("disappear()",2000);
						</script>
						<? } ?>


<?php 
// Hide IF No Pending For Action
// Show Only IF Add Reply and AddRecipient is Click

//$rsReceivedActions = $objDocDetail->getForActions($docID);
$rsAction = $objDocDetail->showActions($docID);

//SHOW THIS if No Actoon commented by LCRM 11/14/2016 due to error
// if(count($rsAction) ==0 && $objDocDetail->isCurrUserDocOwner($docID))
	 // $hideActionForm=FALSE;

$hideActionForm = FALSE;
 

if ($hideActionForm==FALSE) 
{

 ?>
	<table width="98%"  border="0">
      <tr>
        <td>
	<!-- 	<?// echo "Ref.ID : " .$refID." " .$intActionUnitSwitch .":" .$strSenderUnit?>:<?// echo $strSenderId?> -->
		<form action="javascript:get(document.getElementById('frmUserAction<? echo $divId;?>'),'<? echo $divId;?>','showAction.php'); " name="frmUserAction<? echo $divId;?>" id="frmUserAction<? echo $divId;?>" onSubmit="return validate('frmUserAction<? echo $divId;?>')" accept-charset="utf-8">
		<table width="98%"  border="0" align="center">
          <tr>
            <td>Action Taken</td>
            <td><?
				$rsActionTaken = $objDocDetail->getActionTaken();
			?>
			<input type="hidden" name="div" value="<? echo $divId;?>">
			<input type="hidden" name="docID" value="<? echo $docID;?>">
			<input type="hidden" name="mode" value="<? echo $mode; ?>" />
			<input type="hidden" name ="historydiv" value="<? echo $historydiv;?>" />
<!-- PAO
//

 -->
			<input type="hidden" name="addSource" value="1" />
            <input type="hidden" name="txtReferenceID" value="<? echo $refID;?>">            
<!-- PAO -->
            
			
            <select name="cmbActionTaken" id="cmbActionTaken">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionTaken);$i++) 
				{ 		
					echo "<OPTION value='".$rsActionTaken[$i]['actionCodeId']."'>".$rsActionTaken[$i]["actionDesc"]."</OPTION>\n"; 
				}
			?>		  
			</select></td>
          </tr>
          <tr> 
            <td colspan="2">Action Unit</td>
          </tr> 
          <tr>
            <td colspan="2"><?php  
			//$objDocDetail->displaySourceRecipient(); 
			//include("fcbkcomplete/index.php");
			?>
			<ol>        
        <li id="facebook-list" class="input-text" style="margin-bottom:0px !important">
          <input type="text" value="" id="txtActionUnit" />
          
          
          <?php if($objDocDetail->get("blnAgencyUser")){ //prepopulated list value  for Agency User?> 
          <ul id="preadded" style="display:none">
              <li rel="2:Section:RMS" fckb="2" class="">RMS</li>
          </ul>
          <?php } ?>
          
          <?php 
/* PAO  				  
		if($_GET['ac']=='reply'){ //prepopulated list value  for Reply
	
		  ?> 
          <ul id="preadded" style="display:none">
              <li rel="<? echo $intActionUnitSwitch ?>:<? echo $strSenderUnit?>:<? echo $strSenderId?>" fckb="2" class=""><? echo  $senderName ?></li>
          </ul>
          <?php } */ ?>
          
          
          <div id="facebook-auto">
            <div class="default">Type the name of an Employee / Office / Agency</div> 
            <ul id="feed">
            </ul>
          </div>
        </li>
      </ol>
			</td>
          </tr>
          <tr>
            <td width="16%" >Action Needed</td>
            <td width="84%">
    		<?
				$rsActionRequired = $objDocDetail->getActionRequired();
			?>
			<select name="cmbActionRequired" id="cmbActionRequired">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionRequired);$i++) 
				{ 	
					if ($cmbActReq!="" && $rsActionRequired[$i]['actionCodeId'] == 12)  
				 		echo "<option value='".$rsActionRequired[$i]['actionCodeId']."' " .$cmbActReq  .">".$rsActionRequired[$i]["actionDesc"]."</option>\n"; 
					else
				  		echo "<option value='".$rsActionRequired[$i]['actionCodeId']."'>".$rsActionRequired[$i]["actionDesc"]."</option>\n"; 
					   
				}  
			?>		   
			</select>&nbsp;&nbsp;
            <input type="checkbox" name="cbReply" id="cbReply" value="1"  title="" <? echo $chkReply; ?>>&nbsp;
            <div class='ui-icon ui-icon-info' style=" margin-top:-17px; margin-left:40%"
            title='Place a tick mark on the checkbox if an action/reply is needed.&#xAIf document was received but no action/reply was made, the document will be considered "Unacted".&#xA e.g. &#32 Action Needed: &#32 &#32&#32&#32&#32&#32For Action and For Signature &#xA &#32 &#32 &#32 &#32 &#32No Action Needed:&#32&#32For Information and For PickUp'></div>
            
            </td>
		</tr>

          <tr>
            <td width="16%">Remarks</td>
            <td width="84%">
					<input type="hidden" name="txtDocumentID" value="<? echo $arrFields[0]['documentId'];?>">
					<textarea id="txtRemarks" name="txtRemarks" rows="2" cols="50"></textarea>
					<?php 
						if($_SESSION['office'] == 'RMS'){
							echo '<br><small><i>For date input, must be in <b>MM-DD-YYYY</b> format.</i></small>';
						}
					 ?>
			</td>
          </tr>
		    <tr>
            <td width="16%">Restricted</td>
            <td width="84%"><input type="checkbox" name="cbRestricted" id="cbRestricted" value="1"  <? echo $chkRestricted; ?>>
			</td>
          </tr>
		  <!--<tr> <span class='ui-icon ui-icon-info'
            title='Place a tick mark on the "Required" checkbox if an action is needed to be performed by the recipient.&#xA If document was received but actions are not made, the document will be considered "Unacted". &#xA This will reflect on the Summary of For Process Documents.'></span>
            <td width="16%">Reply expected</td>
            <td width="84%"><input type="checkbox" name="cbReply">
			</td>
          </tr>-->
		    <tr align="center">
            <td colspan="2" align="center"><input type="submit" class="btn" name="btnAdd" value="Submit"></td>
            </tr>
        </table>
	</form>
	
		</td>
      </tr>
    </table>

<?php 

}// end if ($hideActionForm) 

 ?>					

<?php 
// Test if employee/custodian has document for action
// Display Actions w/ Reply Req.
/*$rsReceivedActions = $objDocDetail->getForActions($docID);

// echo "For Action ". count($rsReceivedActions);

if(count($rsReceivedActions)!=0)
{
	$ctrAction=0;
	for($i=0;$i<count($rsReceivedActions);$i++) 
	{
	
	 $history = $objDocDetail->checkHistoryActions($rsReceivedActions[$i]['historyId']);	 	
//		echo "History ID ".$rsReceivedActions[$i]['historyId'];
//		echo "<br/>";
		if(count($history)!=0)
		{
			$ctrAction++;
		}
	}
} 
*/


$rsReceivedActions = $objDocDetail->getForActions($docID);
//echo count($rsReceivedActions) ." for Action <br />";
if(count($rsReceivedActions)==0 && $arrFields[0]['iscomplete']==0) {
	
// $divId.'&historydiv='.$historydiv.'&docID='.$docID
//	echo "<br/> Add New Action (New Action - No Ref. ID)";

		if ($objDocDetail->isCurrUserDocOwner($docID) && ($hideActionForm) )
			echo '<br/><div align="right" ><ul  id="icons" class="ui-widget ui-helper-clearfix">  
		<li class="ui-state-default ui-corner-all"  
		onClick="msgAddAction(\''.$divId.'\',\''.$historydiv.'\',\''.$docID.'\');">
		<span class="ui-icon ui-icon-plus" title="Add Action"></span></li></ul></div>';

?>	
	
<?php  }else {
	 
	// this will be used to display the table
	$ctrAction=0;

	for($i=0;$i<count($rsReceivedActions);$i++) 
	{
	  
	//checks if history ID is a reference 		
	$history = $objDocDetail->checkHistoryActions($rsReceivedActions[$i]['historyId']);
//	echo "<br/> $i " .$rsReceivedActions[$i]['historyId'] ." history = ". count($history)  . "  <br/>";
//	echo " - " . $rsReceivedActions[$i]['historyId']; 
	$history2 = $objDocDetail->checkRecipientHistoryReceived($rsReceivedActions[$i]['historyId']);
//	echo "<br/>" .$rsReceivedActions[$i]['historyId'] ." history2 = ". count($history2)  . "  <br/>";
	 
	if(count($history2)==0 && $rsReceivedActions[$i]['reply']==0)
	{  
		$ctrAction++ ;
	}
	
	if(count($history)==0 && $rsReceivedActions[$i]['reply']==1)
	{ 
		$ctrAction++ ;
//		echo $ctrAction . " + " .$rsReceivedActions[$i]['historyId'] ." <br/>";
		//checks if the reference ID found in the previous function is ALSO a reference ID
		$history2 = $objDocDetail->checkHistoryActions($history[$i]['historyId']);

//		echo "<br/>" .$history2[0]['historyId'] ." history2 = ". count($history2)  . "  <br/>"; 
		
		if (count($history2)<=0) // 
			{ 
//			echo $ctrAction . " ++ <br/>";
			}
		else {
			$ctrAction--;
//			echo $ctrAction . " -- <br/>";
			}
			
	} 
	 

	}//for($i=0;$i<count($rsReceivedActions);$i++)
 
 
	if ($ctrAction<=0 && $arrFields[0]['iscomplete']==0) // if there/s reply for that action 
	{

		//echo "<br/>	<br/> No Record \"For Action to Reply\" <br/>	<br/> ";	
		if ($objDocDetail->isCurrUserDocOwner($docID) && $hideActionForm )
		{	echo '<br/><div align="right" style="text-align:right"><ul  id="icons" class="ui-widget ui-helper-clearfix">  
		<li class="ui-state-default ui-corner-all" 
		onClick=" var msg= confirm(\'Add New Action?\'); 
			if(msg){  
			 getData(\'showAction.php?ac=reply&ID=0&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') 
			 } else  
			 return (false);"> 
		<span class="ui-icon ui-icon-plus" title="Add Action"></span></li></ul></div>';
		} 
		//else
		//	echo "<br/>No Record \"For Action to Reply\" <br/><br/> ";	
		
	}else 
	{
	?>
    <br/>
    <table width="100%" border="0" id="listRecipient">
	  <tr>
		<td height="114" style="text-align:right">
		<table width="100%"  border="1" class="listings" >
	  <tr class="listheader">
		<td colspan="4" align="left"><div align="left">FOR ACTIONS</div></td>
		</tr>
	  <tr class="listheader">
	    <td width="24%">Date</td> 
		<td width="29%">Sender</td>
		<td width="35%">Action Needed /Remarks </td>
		<td width="12%">&nbsp;</td>
	  </tr>

 <?php
		//$rsReceivedActions = $objDocDetail->getReceivedActions($docID);
	
		for($i=0;$i<count($rsReceivedActions);$i++) 
		{
			
			$history = $objDocDetail->checkHistoryActions($rsReceivedActions[$i]['historyId']);
			$history2 = $objDocDetail->checkRecipientHistoryReceived($rsReceivedActions[$i]['historyId']);
//			for($i=0;$i<count($history);$i++) 
//			{
//			echo " Reply " .$rsReceivedActions[$i]['reply'];
//			}
			 
			//count($history)!=0 && $rsReceivedActions[$i]['reply']==1
			if(count($history)==0 && $rsReceivedActions[$i]['reply']==1 )
			{
				$actionSender = $objDocDetail->displayRecipientSenderNames($rsReceivedActions[$i]['senderId'], $rsReceivedActions[$i]['senderUnit']);
				
				
				if($rsReceivedActions[$i]['restricted']){
					$arrHead=$objDocDetail->getOfficeHead($_SESSION["office"],$_SESSION["userUnit"]);
					$intHead=0;
					for($cntHead=0;$cntHead<count($arrHead);$cntHead++)
					{
						if($_SESSION["empNum"]==$arrHead[$cntHead]){
							$intHead=1;
							break;
						}
					}
					
					if(
						($rsReceivedActions[$i]['senderUnit']=="employee"&& $_SESSION["empNum"] ==$rsReceivedActions[$i]['senderId']) ||
						($rsReceivedActions[$i]['senderUnit']!="employee" && $rsReceivedActions[$i]['senderId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)) ||
						($rsReceivedActions[$i]['senderUnit']!="employee" && $rsReceivedActions[$i]['senderId']== $_SESSION["office"] && $intHead)
						){
							
							$strRemarks="/ " .$rsReceivedActions[$i]['remarks'];
						}
					elseif($rsReceivedActions[$i]['recipientUnit']=="employee" && $rsReceivedActions[$i]['recipientId']==$_SESSION["empNum"]) $strRemarks="/ " .$rsReceivedActions[$i]['remarks'];
					else if($rsReceivedActions[$i]['recipientUnit']!="employee"){
						if ($rsReceivedActions[$i]['recipientId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)){
							 $strRemarks=$rsReceivedActions[$i]['remarks'];
						}
						else{
							if($intHead && $rsReceivedActions[$i]['recipientId']== $_SESSION["office"]) $strRemarks="/ " .$rsReceivedActions[$i]['remarks'];
							else $strRemarks="";
						}
					}
					else
					$strRemarks="";
				}
				else{
					$strRemarks="/ " .$rsReceivedActions[$i]['remarks'];
				}
				
//				<td>[' .$rsReceivedActions[$i]['historyId'].'] - '.$rsReceivedActions[$i]['dateSent'].'</td>
				
			  echo '<tr>
				<td>'.$rsReceivedActions[$i]['dateSent'].'</td>
				<td>'.$actionSender.'</td>
				<td>'.$rsReceivedActions[$i]['actionDesc'].'<br>'.'
				<i>'.$strRemarks.'</i>
				<td>
				<ul id="icons" class="ui-widget ui-helper-clearfix">';

				echo '<li id="pencillink" class="ui-state-default ui-corner-all" 
			onClick=" var msg= confirm(\'Are you sure you want to add reply on this action?\',\'\'); 
			if(msg){
			 getData(\'showAction.php?ac=reply&ID='.$rsReceivedActions[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') 
			 } else 
			 return (false); ">
			<span class="ui-icon ui-icon-pencil" title="Add Reply"></span></li>';	
							  
			//	<li class="ui-state-default ui-corner-all" 
			//	onClick=\'showActionReplyDialog("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'>
			//	<span class="ui-icon ui-icon-pencil" title="Add Reply"></span></li>
				
			echo '<li class="ui-state-default ui-corner-all" onClick=\'showSetDeadlineDialog("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-clock" title="Set Own Deadline"></span></li></ul></td>';
			  echo '</tr>';
			  //<a href="javascript:getData(\'showAction.php?showForm=actionForm&ID='.$rsReceivedActions[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode='.$mode.'\',\''.$divId.'\')">Action</a></td>';
			  // <a href="#" onClick=\' $("#actionDialog").dialog("open"); doAction("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'"); \'>Action</a></td>';

			} // end if(count($history)==0 && $rsReceivedActions[$i]['reply']==1 )

// FOR ACTION to RECEIVED		
			if(count($history2)==0 && $rsReceivedActions[$i]['reply']==0 )
			{
//				echo " Please go to \"Received Documents\" and receive this document!<br/>" ;
			} 
			
		}// end for($i=0;$i<count($rsReceivedActions);$i++) 
	  ?> 
	</table>
<?php

 	} //end if ($ctrAction!=0)
 }//end if ?>



	
<?php $rsAction = $objDocDetail->showActions($docID); 

// echo "actions: ".count($rsAction);
	 
	if(count($rsAction)!=0)
	{
	?>
    <?php
	
	$ictr = 0;
	for($i=0;$i<count($rsAction);$i++) 
	{
	 
		// Display the Action if CurrUser is the Sender

/*		if 	($rsAction[$i]['referenceId']!=0 && !$rsAction[$i]['reply'] )  		
		{
		echo "<tr><td colspan=\"6\" align=\"center\"> <div align=\"center\"> History ".$rsAction[$i]['historyId']
			."  Reference ".$rsAction[$i]['referenceId'] ."   Reply?  "
			. $rsAction[$i]['reply'] ." </div> </td></tr>";
		} elseif ($objDocDetail->isCurrUserActionSender("".$rsAction[$i]['historyId'])) { 
*/
	
// display details Received (FOR ACTION )
		$checkRecipientReceivedOnly = $objDocDetail->checkRecipientHistoryReceived($rsAction[$i]["historyId"],$docID);
		// display details if there's a reply, display form otherwise	

		$checkRecipientReply = $objDocDetail->checkRecipientHistoryActions($rsAction[$i]["historyId"]);		
		$actionRecipient = $objDocDetail->displayRecipientSenderNames($checkRecipientReply[0]['recipientId'], $checkRecipientReply[0]['recipientUnit']);	
		
		
		$strReceivedBy = "";
		if(count($checkRecipientReceivedOnly)>=1)
			$strReceivedBy .="Received By:&#32;&#32; ". $checkRecipientReceivedOnly[0]['receivedBy']." (". $checkRecipientReceivedOnly[0]['dateReceived'].")";
		if ($rsAction[$i]["reply"]==1 && count($checkRecipientReply)>=1){			
			$strReceivedBy .="&#xA;Action Taken:&#32;&#32;". $checkRecipientReply[0]['actionTakenDesc']." (".$checkRecipientReply[0]['dateSent']  .")"; 
			$strReceivedBy .="&#xA;Forwarded To:&#32;&#32;". $actionRecipient ." [".$checkRecipientReply[0]['actionDesc']  ."]";  
		}
		else
		{
			
			if ($rsAction[$i]["reply"]==0)
				$strReceivedBy .="";  
			else
				$strReceivedBy .="&#xA;&#32;&#32;&#32;&#32;&#32;&#32;&#32;&#32;&#32;&#32;&#32;&#32;[ No Action Taken ] "; 
				
		}

//		echo " received ". count($checkRecipientReceivedOnly) ." ID  ".$rsAction[$i]["historyId"];
//		echo "<br/> by ". $checkRecipientReceivedOnly[$i]['receivedBy']; 		


		if ($objDocDetail->isCurrUserActionSender("".$rsAction[$i]['historyId'])) { 
			
		
		if ($ictr == 0)
		{
		?>
        <br/>
	<table width="100%"  border="1" class="listings">
  <tr class="listheader">
    <td colspan="6" align="center"><div align="left">ACTIONS MADE </div></td>
    </tr>
  <tr class="listheader">
    <td align="center" width="15%">Action Taken </td>
    <td align="center" width="10%">Date</td>
    <td align="center" width="20%">Recipient</td>
    <td align="center" width="18%">Action Required/ Remarks</td>
	 <td align="center" width="20%">Updated By </td>
    <td align="center" width="17%">&nbsp;</td>
  </tr>
       <?php
		} 
		$ictr ++;
		
		$actionRecipient = $objDocDetail->displayRecipientSenderNames($rsAction[$i]['recipientId'], $rsAction[$i]['recipientUnit']);
	  echo '<tr>
	    <td><!--'.$rsAction[$i]['historyId'].'--> ' .$rsAction[$i]['actionTakenDesc'].'</td>
		<td>'.$rsAction[$i]['dateSent'].'</td>
		<td>'.$actionRecipient.'</td>
		<td>'.$rsAction[$i]['actionDesc'].'<br><i>'.$rsAction[$i]['remarks'].'</i></td>
		<td>'.$objDocDetail->getEmployeeName($rsAction[$i]['addedById'],$objDocDetail->get("blnAgencyUser"))
		.'</td>';
//PAO	FOR Recipient Name

	
		// check if action sent has no reply yet
//		$checkRecipientReply2 = $objDocDetail->checkReplyActionSent($rsAction[$i]["historyId"]);
		
		 // check if action sent is a reply from previous action 
//		$checkReplyAction = $objDocDetail->checkSenderHistoryActions($rsAction[$i]["referenceId"]);
 

//***** ACTION MADE *****//
		
		//REPLY REQUIRED w/ Action >=1
		if($rsAction[$i]["reply"]==1 && count($checkRecipientReply)>=1  )
		{
 
			$replyStatus = '<li class="ui-state-default ui-corner-all">
			<span class="ui-icon ui-icon-check" title="'.$strReceivedBy .'"></span></li>
		<li class="ui-state-default ui-state-disabled ui-corner-all" >
		<span class="ui-icon ui-icon-trash"  title=""></span></li></ul>
		</td>';			 

			
			
		}
		
		// REPLY NOT REQUIRED BUT RECEIVED
		else
		{
			 

		
			// for($i=0;$i<count($checkRecipientReceivedOnly);$i++){
			//	 $arrReceiver[] =$checkRecipientReceivedOnly[$i]['receivedBy'].' ('.$checkRecipientReceivedOnly[$i]['dateReceived'].')';
			 //}
			// $strReceiver = implode(',',$arrReceiver);
			 
//			if count($checkRecipientReceivedOnly)==1
//				 $receivedData = "$strReceiver";
			
			
			if(count($checkRecipientReceivedOnly)>=1)
				$replyStatus = '<li class="ui-state-default ui-corner-all">
			<span class="ui-icon ui-icon-check" title="'.$strReceivedBy .'"></span></li>
		<li class="ui-state-default ui-state-disabled ui-corner-all" >
		<span class="ui-icon ui-icon-trash"  title=""></span></li></ul>
		</td>';			 
			
			
			else


			$replyStatus = '<li class="ui-state-default ui-state-disabled ui-corner-all">
			<span class="ui-icon ui-icon-check" title=""></span></li>
			<li class="ui-state-default ui-corner-all" 
		onClick=" var msg= confirm(\'Are you sure you want to delete this record?\'); 
			if(msg){
			 getData(\'showAction.php?ac=del&ID='.$rsAction[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') 
			 } else 
			 return (false);">
		<span class="ui-icon ui-icon-trash" title="Delete Action"></span></li></ul>
		</td>';		
			
		}
	// IF CurrUser not the OWNER
	if (!$objDocDetail->isCurrUserDocOwner($docID) && $arrFields[0]['iscomplete']==0)
 			
		echo '<td><ul id="icons" class="ui-widget ui-helper-clearfix">  
		<li class="ui-state-default ui-corner-all" 
		onClick=" var msg= confirm(\'','Add Recipient?'  ,'\'); 
			if(msg){  
			 getData(\'showAction.php?ac=reply&ID='.$rsAction[$i]["referenceId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') 
			 } else 
			 return (false);">
		<span class="ui-icon ui-icon-plus" title="Add Recipient" ></span></li>'.$replyStatus; 
				
		else
			echo '<td><ul id="icons" class="ui-widget ui-helper-clearfix">'.$replyStatus; 
		
/*		.' 
		<li class="ui-state-default ui-corner-all" 
		onClick=" var msg= confirm(\'','Are you sure you want to delete on this record?','\'); 
			if(msg){
			 getData(\'showAction.php?ac=del&ID='.$rsAction[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') 
			 } else 
			 return (false);">
		<span class="ui-icon ui-icon-trash" title="Undo Action"></span></li></ul>
		</td>';
*/
	  echo '</tr>';
		}//end if ($rsAction[$i]['referenceId']!=0 && !$rsAction[$i]['reply'] )  
	  
  	} //End for($i=0;$i<count($rsAction);$i++) 
	
	
  ?>
</table>	
<br/>

	<?php }?>	</td>
  </tr>
  
  
</table>


	<?php
		if($arrFields[0]['iscomplete']==1): 
			$completedby = $objGeneral->getEmpDetails($arrFields[0]['completed_by']);
			$completedby = $completedby['fname'].' '.$completedby['minital'].' '.$completedby['lname'];
	?>
	<table id="tableComplete2" width="100%"  border="1" class="listings" >
		<tr class="listheader">
			<td colspan="3" align="left"><div align="left">ACTION COMPLETED</div></td>
		</tr>
		<tr class="listheader">
			<td width="24%">Completed By</td>
			<td width="24%">Office</td>
			<td width="29%">Date Completed</td>
		</tr>
		<tr>
			<td style="text-align: center;"><?=$completedby?></td>
			<td style="text-align: center;"><?=$arrFields[0]['completed_byOffice']?></td>
			<td style="text-align: center;"><?=$arrFields[0]['datecomplete']?></td>
		</tr>
	</table>
	<?php endif; ?>

	<?php 
		$inicompletedby = $objGeneral->getEmpDetails($_SESSION['empNum']);
		$inicompletedby = $inicompletedby['fname'].' '.$inicompletedby['minital'].' '.$inicompletedby['lname'];
	 ?>
	<table id="tablecomplete" width="100%"  border="1" class="listings" >
		<tr class="listheader">
			<td colspan="3" align="left"><div align="left">ACTION COMPLETED</div></td>
		</tr>
		<tr class="listheader">
			<td width="24%">Completed By</td>
			<td width="24%">Office</td>
			<td width="29%">Date Completed</td>
		</tr>
		<tr>
			<td id="tdcompletedby" style="text-align: center;"><?=$inicompletedby?></td>
			<td style="text-align: center;"><?=$_SESSION['office']?></td>
			<td id="tdcompleteddate" style="text-align: center;"></td>
		</tr>
	</table>
	<br><br>


	<table id="tblcompleted" style="width: 100%;">
	  <tr>
	  	<td style="padding-bottom:10px">
	  		<a
	  			style="padding: 0px 5px 0px 5px;
	  			color: #050;
			    font: bold 90% 'trebuchet ms',helvetica,sans-serif;
			    background-color: #fed;
			    border: 1px solid;
			    border-color: #696 #363 #363 #696;
			    text-decoration: none;"
	  			id="complete"
	  		>Action Completed</a>
	  		<i id="completenote" style="color: red">Click this button if the process is complete.</i>
	  	</td>
	  </tr>
	</table>
<?php if($arrFields[0]['iscomplete']==0 and count($rsAction)): ?>
	<script type="text/javascript">$('#tblcompleted').show();</script>
<?php else: ?>
	<script type="text/javascript">$('#tblcompleted').hide();</script>
<?php endif; ?>


	<table id="tblundocomplete" style="width: 100%;">
	  <tr>
	  	<td style="padding-bottom:10px">
	  		<a
	  			style="padding: 0px 5px 0px 5px;
	  			color: #050;
			    font: bold 90% 'trebuchet ms',helvetica,sans-serif;
			    background-color: #fed;
			    border: 1px solid;
			    border-color: #696 #363 #363 #696;
			    text-decoration: none;"
	  			id="undocomplete"
	  		>Undo Completed Action</a>
	  		<i id="undocompletenote" style="color: red">Click this button if you want to <b>UNDO</b> the completed action.</i>
	  	</td>
	  </tr>
	</table>

<!-- The office completed the process can able to undo the the action complete -->
<?php if($arrFields[0]['iscomplete']==1 and $arrFields[0]['completed_byOffice']==$_SESSION['office']): ?>
	<script type="text/javascript">$('#tblundocomplete').show();</script>
<?php else: ?>
	<script type="text/javascript">$('#tblundocomplete').hide();</script>
<?php endif; ?>



<script type="text/javascript">
	$(document).ready(function() {
		$('#tablecomplete').hide();
		$('#complete').click(function() {
			var res = confirm('Are you sure this document is complete?')
			$('#pencillink').css('opacity','0.6');
			$('#pencillink').css('pointer-events','none');
			$(this).hide();
			$('#completenote').hide();
			var docid = '<?=$docID?>';
			if(res == true){
				console.log("showData.php?mode=complete&empid=<?=$_SESSION['empNum']?>&office=<?=$_SESSION['office']?>");
				$.ajax({
	                type: "GET",
	                url: "showData.php?mode=complete&empid=<?=$_SESSION['empNum']?>&office=<?=$_SESSION['office']?>",
	                data: "docid="+docid,
	                success: function(data) {
	                	jQuery.facebox(data);
	                	$('#complete').hide();
	                	$('#tablecomplete').show();
	                	$('#tdcompleteddate').html('<?=date("Y-m-d H:i:s")?>');
	                	$('#tblundocomplete').show();
	                	$('#undocomplete').show();	
						$('#undocompletenote').show();
	                }
	            });
			}
		});

		$('#undocomplete').click(function() {
			var res = confirm('Are you sure you want to undo the completed action?')
			$('#pencillink').css('opacity','2');
			$('#pencillink').css('pointer-events','');
			var docid = '<?=$docID?>';
			console.log("showData.php?mode=uncomplete&id="+docid);
			if(res == true){
				$.ajax({
	                type: "GET",
	                url: "showData.php?mode=uncomplete",
	                data: "docid="+docid,
	                success: function(data) {
	                	jQuery.facebox(data);
	                	$('#tblcompleted').show();
	                	$('#complete').show();
						$('#completenote').show();

	                	$('#tablecomplete').hide();
	                	$('#tableComplete2').hide();
	                	$('#undocomplete').hide();	
						$('#undocompletenote').hide();	
	                }
	            });
			}
		});

	});
</script>