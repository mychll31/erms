<? @session_start();
include_once("class/UserAccounts.class.php");
require_once("class/Incoming.class.php");
$objInc = new Incoming();
$objAccount = new UserAccounts;
$arOffices = $objAccount->getOfficeFromExeOffice();
$arAgencies = $objInc->getOriginOffice();
$c = sizeof($arOffices);
	if($_REQUEST["mode"]=="add")
	{	
		$arrFields = $objAccount->getUserFields();
		$arrFields['userAccountOffice'] = $arrFields['activeOffice']=='dost_office'?$arrFields['userAccountOffice']:"";
		$arrFields['userAgency'] = $arrFields['activeOffice']=='agency_office'?$arrFields['userAgency']:"";
		
		$result=$objAccount->addUser($arrFields);
		//print_r($arrFields);
		?>
		<script type="text/javascript" language="javascript">  
		<?
		if($result==1)
		{?>
			$("#userGrid").trigger("reloadGrid");
			$('#userClear').click();
			jAlert('Succesfully Added','Information');

		<? }
		else
		{?>
			jAlert('Error: Account Not Added',"warning");
		<? }
		?>
		</script>
	<?
	die();
	}
	if($_REQUEST["mode"]=="edit")
	{
	$arrFields = $objAccount->getUserFields();
	$arrFields['userAccountOffice'] = $arrFields['activeOffice']=='dost_office'?$arrFields['userAccountOffice']:"";
	$arrFields['userAgency'] = $arrFields['activeOffice']=='agency_office'?$arrFields['userAgency']:"";
	
	$result=$objAccount->updateUser($arrFields);
	?>
		<script type="text/javascript" language="javascript">  
		<?
		if($result==1)
		{?>
			$("#userGrid").trigger("reloadGrid");
			$('#userClear').click();
			jAlert('Succesfully updated','Information');

		<? }
		else
		{?>
			jAlert('Error: Account Not Updated',"warning");
		<? }
		?>
		</script>
	<?
	//print_r($arrFields);
	die();
	}

?>
<script type="text/javascript" language="javascript">
var validator='';
var tmpEmpNum="";
var e_active='dost_office';
var map={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.$arOffices[$i]['officeName'].'":'.$i.',';
		}
		echo '"'.$arOffices[$i]['officeName'].'":'.$i.'';
		?>
		}

var mapOffice={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'",';
		}
		echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'"';
		?>
		}


jQuery("#userRefresh").click( function(){ 
    //if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');
	jQuery('#userGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#userGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Employee Number','Surname','First Name', 'Middle Initial', 'Name Extension', 'email', 'DOST Office', 'Agency Office', 'Username','P'],
		colModel:[
			{name:'userId',index:'userId',width:65,  hidden:true},
			{name:'empNum',index:'empNum', width:65},
			{name:'surname',index:'surname', width:100},
			{name:'firstname',index:'firstname', width:100},
			{name:'middle',index:'middle', width:65},
			{name:'extension',index:'extension', width:65},
			{name:'email',index:'email', hidden:true},
			{name:'office',index:'office', width:100},
			{name:'agency',index:'agency', width:100},			
			{name:'uname',index:'uname', width:100},
			{name:'p',index:'p', hidden:true}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,"All"],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#userPager'),
		postData:{table:'tblUserAccount',searchField:'surname',searchString:"",searchOn:'false',searchOper:'ew'},
		sortname: 'surname',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"User Accounts"
	}).navGrid('#userPager',{edit:false,add:false,del:false});}); 
  });
   $('#userRefresh').click();

  
$("#userAdd").click(function(){
	$("#mode").val("add");
	$("#frmUserAccounts").submit();
});

$("#userEdit").click( function(){ 
	  var id = jQuery("#userGrid").getGridParam('selrow'); 
	  if (id) {
	   var ret = jQuery("#userGrid").getRowData(id); 
	    if(ret.office!='') $("#userAccountOffice").val(ret.office); 
	    if(ret.agency!='') $("#userAgency option:contains(" + ret.agency + ")").attr('selected', 'selected');
		
		$(ret.office == ''?'#agency_office':'#dost_office').click();
	    $("#empNumber").val(ret.empNum);
		$("#userId").val(ret.userId);
		$("#firstname").val(ret.firstname); 
		$("#mid").val(ret.middle); 
		$("#surname").val(ret.surname); 
		$("#extension").val(ret.extension);
		$("#uname").val(ret.uname);
		$("#pass").val(ret.p);
		$("#email").val(ret.email);
		$("#mode").val("edit");
	   if($("#userEdit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
   	    $("#userSave").css('display','block');
		$("#userAdd").css('display','none');
		$('#Clear_user').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 


jQuery("#userSave").click( function(){ 
     $("#mode").val("edit");
	 $("#frmUserAccounts").submit();
	});

$("#userDel").click(function(){ 
	 var id = jQuery("#userGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if(id!= ''){ 
		 jConfirm('Proceed deleting this record?', false, 'ERMS Confirmation Dialog', function(r) {
		 if(r==true){
		  $("#userGrid").setPostData({mode:"del",table:'tblUserAccount',ID:id.toString(),MODULE:'User Account'});
		  $("#userGrid").trigger("reloadGrid"); 
		  $('#userClear').click();
		  jAlert('Succesfully deleted', 'Confirmation Results'); } }); }
	  else jAlert('Please select row to delete', 'Warning');  });


$('#userClear').click( function(){ 
	   $("#userAccountOffice").attr("disabled",false);
	   $("#userAccountOffice, #userAgency").attr("selectedIndex",0);
	   $('#dost_office').click();
	   $("#userId").val("");
	   $("#empNumber").val("");
	   $("#firstname").val("");
	   $("#mid").val("");
	   $("#surname").val("");
	   $("#extension").val("");
	   $("#uname").val("");
	   $("#pass").val("");
	   $("#mode").val("");
	   $("#email").val("");
	   
	   $("#userSave").css('display','none');
	   $("#userAdd").css('display','block');
	   $('#Clear_user').text(" Clear ");
	   $('#userRefresh').click();
	   validator.resetForm();
	});	

$().ready(function() {
	 validator = $("#frmUserAccounts").validate({
		rules: { empNumber: "required",
				 firstname: "required",
				 mid:"required",
				 surname:"required",
				 uname:"required",
				 pass: "required",
				 email: {
					  required: true,
					  email: true
						}
				 },
		messages: {	
		         empNumber: "Employee Number is required",
				 firstname: "First Name is required",
				 mid:"Middle Initial is required",
				 surname:"Surname is required",
				 uname:"Username is required",
				 pass: "Password is required"
				 },
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			get(document.getElementById('frmUserAccounts'),"ajaxpagecontainer","showUserAccounts.php");		
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
	
	$('#dost_office,#agency_office').click(function() { 
	  var e_active = $(this).attr('id');
	  $('#activeOffice').val(e_active);
	  
	  $(e_active=='dost_office'?'#agency_office':'#dost_office').removeClass('ui-state-active').find('span').removeClass('ui-icon-radio-on').addClass('ui-icon-radio-off');
	  $(this).addClass('ui-state-active').find('span').removeClass('ui-icon-radio-off').addClass('ui-icon-radio-on');	
	  $('#userAccountOffice').css('display',e_active=='dost_office'?'block':'none');
	  $('#userAgency').css('display',e_active=='agency_office'?'block':'none');
	});
 });	 	    
</script>
<form id="frmUserAccounts" autocomplete="off" method="get">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">ID Number</td>
    <td class="field" style="width:30px;"><input type="text" name="empNumber" id="empNumber"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">First Name</td>
    <td class="field" style="width:30px;"><input type="text" name="firstname" id="firstname"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">Middle Initial</td>
    <td class="field" style="width:30px;"><input type="text" name="mid" id="mid"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">Surname</td>
    <td class="field" style="width:30px;"><input type="text" name="surname" id="surname"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">Name Extension</td>
    <td class="field" style="width:30px;"><input type="text" name="extension" id="extension"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">Email</td>
    <td class="field" style="width:30px;"><input type="text" name="email" id="email"></td>
	<td class="status" style="width:auto"></td>
  </tr>  
  
  <tr> 
    <td class="label">
    <div class="ui-buttonset" style="font-size:9px; width:160px;">
      <ul id='icons' class='office-source ui-widget ui-helper-clearfix'>
       <li class='ui-state-default ui-corner-all ui-state-active' id="dost_office"><span class='ui-icon ui-icon-radio-on' title='dost'></span><span style="vertical-align:middle">DOST&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li>  
       <li class='ui-state-default ui-corner-all' id="agency_office"><span class='ui-icon ui-icon-radio-off' title='agency'></span><span style="vertical-align:middle">AGENCY&nbsp;</span></li>
      </ul>
    </div>
    </td>
    <td class="field" colspan="2">
    <select id="userAccountOffice" name="userAccountOffice" style="width: 300px; height:30px;">
     <option></option>
	 <? 
     for($i=0;$i<$c;$i++)
     {
     echo "<option value=".$arOffices[$i]['officeCode'].">".$arOffices[$i]['officeName']."</option>";					
	 }
     ?>
    </select>
	<select id="userAgency" name="userAgency" style="width: 300px; height:30px; display:none; ">
	 <option></option>
	 <? 
     for($i=0;$i<count($arAgencies);$i++)
     {
     echo "<option value=".$arAgencies[$i]['originId'].">".$arAgencies[$i]['officeName']."</option>";					
	 }
     ?>
    </select>    
    </td>
  </tr>
  <tr> 
    <td class="label">Username</td>
    <td class="field" style="width:30px;"><input type="text" name="uname" id="uname"></td>
	<td class="status" style="width:auto"></td>
  </tr>
  <tr> 
    <td class="label">Password</td>
    <td class="field" style="width:30px;"><input type="password" name="pass" id="pass"></td>
	<td class="status" style="width:auto"></td>
	<input type="hidden" id="activeOffice" name="activeOffice" value="dost_office" />
	<input type="hidden" id="userId" name="userId" />
	<input type="hidden" id="mode" name="mode" />
  </tr>
 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="userEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="userDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="userAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="userSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="userClear"><span title='Clear'></span><div id="Clear_user">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="userRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
</form>			
<table id="userGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="userPager" class="scroll" style="text-align:center;"></div>
<div id="ajaxpagecontainer"></div>
