<?php 
	@session_start();
	include_once("class/Incoming.class.php");
	$objIncoming = new Incoming;
	include_once("class/DocDetail.class.php");
	$objDocdetail = new DocDetail;
	include_once("class/General.class.php");
	$objGeneral = new General;


	if($_GET['mode']=='getagencycontact')
	{
		$rs=$objIncoming->getOriginOfficeById($_GET['agencyid']);
		$arrAgency = array();
		$arrAgency['contactPerson'] = $rs[0]['contactPerson'];

		echo json_encode($arrAgency);
	}

	if($_GET['mode']=='updateunactedDoc'):
		$rs = $objIncoming->markUnactedasRead($_GET['docid']);
	endif;

	if($_GET['mode']=='complete'):
		$rs = $objDocdetail->completeDoc($_GET['docid'], $_GET['empid'], $_GET['office']);
		echo $rs;
	endif;

	if($_GET['mode']=='uncomplete'):
		$rs = $objDocdetail->uncompleteDoc($_GET['docid']);
		echo $rs;
	endif;

	if($_GET['mode']=='updateLocation_outg'):
		$rs = $objIncoming->updateLocation($_GET['docid'], $_GET['cabinet'], $_GET['drawer'], $_GET['container']);
		$fileLocation = $objGeneral->getLocation($_GET['docid']);
		$location = ucwords($fileLocation['label1'].'> '.$fileLocation['label2'].'> '.$fileLocation['label3']);
		echo $location;
	endif;

	if($_GET['mode']=='updateControlledDoc_inc'):
		$rs = $objGeneral->addUpdateControlledDoc($_GET['docid'], $_GET['controlleddoc'], $_GET['copyno'], $_GET['copyholder'], $_GET['mannerdisposal'], $_GET['revisionno'], $_GET['unitres'], $_GET['withrawal']);
		echo $rs;
	endif;

?>