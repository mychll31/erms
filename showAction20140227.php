<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
//$arrFields2 = $objDocDetail->getUserFields();

/////////////////////////////////
//filename 		:	showHstory.php
//description	:	ajaxpage for saving deleting document history
//parameters	:	mode = new for Get; mode="" for post
//					docID for document ID
//					div for the container div of this ajaxpage
//other param	:	note(this parameters for delete only)
//					ac = del for delete action
//					ID = historyid for history id to be deleted
/////////////////////////////////

if($_GET['mode']=="") $arrFields2 = $objDocDetail->getUserFields();
else $arrFields2 = $objDocDetail->getUserFields1();

$docID = $arrFields2['docID'];
$divId = $arrFields2['div'];
$arrFields = $objDocDetail->getDocDetails($docID);
$mode = $arrFields2['mode']; //  incoming ; outgoing ; intra
$historydiv = $arrFields2['historydiv'];

//print_r($arrFields2);

?>

<!-- cut here -->



<script language="JavaScript" id="historyjs">
		$(function(){
			$.facebooklist('#txtActionUnit', '#preadded', '#facebook-auto',{url:'fcbkcomplete/fetched2.php',cache:1}, 10, {userfilter:1,casesensetive:0});
		});

		function test()
		{
		objcheckboxes=$("INPUT[type=hidden]");
		errortrap=true;
		$.each(objcheckboxes,function(index,value){
			var tmpocode=value.value;
			errortrap=false;
			if(value.id=="txtActionUnit[]") alert(tmpocode);
		});		
		
		}
		
		function enableOfficeCombo(oForm)
		{
			
			oForm["cmbEmpNumber"].disabled = true;
			oForm["cmbOfficeCode"].disabled = false;
			oForm["cmbAgency"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[0].checked = true;
		}
	
		function enableNameCombo(oForm)
		{
			oForm["cmbEmpNumber"].disabled = false;
			oForm["cmbOfficeCode"].disabled = true;
			oForm["cmbAgency"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[1].checked = true;
		}
		
		function enableAgencyCombo(oForm)
		{
			
			oForm["cmbAgency"].disabled = false;
			oForm["cmbEmpNumber"].disabled = true;
			oForm["cmbOfficeCode"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[2].checked = true;
		}
		
		function validate(formname)
		{
			//alert($('#txtActionUnit').val());return false;
			var oForm = document.forms[formname];
			if(oForm.elements["cmbActionTaken"].options[oForm.elements["cmbActionTaken"].selectedIndex].value == "-1")
			{
				alert("Please select Action Taken!");
				document.getElementById("cmbActionTaken").focus();
				return false
			}else if(oForm.elements["cmbActionRequired"].options[oForm.elements["cmbActionRequired"].selectedIndex].value == "-1")
			{
				alert("Please select Action Required!");
				document.getElementById("cmbActionRequired").focus();
				return false
			}
			else if(oForm.elements["actionUnitSwitch"][0].checked==false && oForm.elements["actionUnitSwitch"][1].checked==false && oForm.elements["actionUnitSwitch"][2].checked==false)
			{
				alert("Please select Action Unit!");
				//document.getElementById("actionUnitSwitch").focus();
				return false
			}
		}
		
		function validatePopup(formname)
		{
			var oForm = document.forms[formname];
			if(oForm.elements["cmbActionTaken"].options[oForm.elements["cmbActionTaken"].selectedIndex].value == "-1")
			{
				alert("Please select Action Taken!");
				document.getElementById("cmbActionTaken").focus();
				return false
			}else if(oForm.elements["cmbActionRequired"].options[oForm.elements["cmbActionRequired"].selectedIndex].value == "-1")
			{
				alert("Please select Action Required!");
				document.getElementById("cmbActionRequired").focus();
				return false
			}
		
		}

		

	/*
		function doAction(id,docid)
		{
		getData("showActionReply.php?mode=actionReply&ID="+id+'&DOCID='+docid,"dialogcontent"); 
		//alert(document.getElementById("ID").innerHTML);
		} */
</script>
					<?php
				// ADDING A RECORD
				//$arrFields2 = $objDocDetail->getUserFields();

						if($t_submit)
						{
						//print_r($_POST);
						//return false;	
						$res = $objDocDetail->addAction($_POST);								
						//print_r($arrFields2);
						
							if(count($res)==0)
							{
								//$intActionCodeId = $arrFields2['cmbActionRequired'];
								//$intActionUnitSwitch = $arrFields2['actionUnitSwitch'];
								//$strRemarks = $arrFields2['txtRemarks'];
							}
							else 
								{
								foreach($res as $row)
									{										
									$objDocDetail->sendEmail($row); //email recipient for Notification
									}
								}
							$msg = $objDocDetail->getValue('msg');
							/*
							echo "<script language=\"JavaScript\">
							if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
							getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
							</script>";
							*/
						}
						
							
						// DELETING A RECORD
						
						if($_GET['ac']=='del')
						{
							$objDocDetail->deleteAction($_GET['ID'], $_GET['docID']);
							$msg = $objDocDetail->getValue('msg');
							echo "<script language=\"JavaScript\">
							if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
							getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
							</script>";
						}
						
						?>
						<? if($msg<>""){?>
						<div class="pane">
						<div class="ui-widget" style="width:40%">
							<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
								<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
								<strong>Notice: </strong><? echo $msg;?></p>
							</div>
						</div>
						</div>
						<script type="text/javascript">
						setTimeout("disappear()",2000);
						</script>
						<? } ?>

						
	<table width="98%"  border="0">
      <tr>
        <td>
	
		<form action="javascript:get(document.getElementById('frmUserAction<? echo $divId;?>'),'<? echo $divId;?>','showAction.php'); " name="frmUserAction<? echo $divId;?>" id="frmUserAction<? echo $divId;?>" onSubmit="return validate('frmUserAction<? echo $divId;?>')" accept-charset="utf-8">
		<table width="98%"  border="0" align="center">
          <tr>
            <td>Action Taken </td>
            <td><?
				$rsActionTaken = $objDocDetail->getActionTaken();
			?>
			<input type="hidden" name="div" value="<? echo $divId;?>">
			<input type="hidden" name="docID" value="<? echo $docID;?>">
			<input type="hidden" name="mode" value="<? echo $mode; ?>" />
			<input type="hidden" name ="historydiv" value="<? echo $historydiv;?>" />
			<input type="hidden" name="addSource" value="1" /> 
			<select name="cmbActionTaken" id="cmbActionTaken">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionTaken);$i++) 
				{ 		
					echo "<OPTION value='".$rsActionTaken[$i]['actionCodeId']."'>".$rsActionTaken[$i]["actionDesc"]."</OPTION>\n"; 
				}
			?>		  
			</select></td>
          </tr>
          <tr>
            <td colspan="2">Action Unit</td>
          </tr>
          <tr>
            <td colspan="2"><?php  
			//$objDocDetail->displaySourceRecipient(); 
			//include("fcbkcomplete/index.php");
			?>
			<ol>        
        <li id="facebook-list" class="input-text" style="margin-bottom:0px !important">
          <input type="text" value="" id="txtActionUnit" />
          <div id="facebook-auto">
            <div class="default">Type the name of an Employee / Office / Agency</div> 
            <ul id="feed">
            </ul>
          </div>
        </li>
      </ol>
			</td>
          </tr>
          <tr>
            <td width="16%">Action Required </td>
            <td width="84%">
			<?
				$rsActionRequired = $objDocDetail->getActionRequired();
			?>
			<select name="cmbActionRequired" id="cmbActionRequired">
			<option value="-1">&nbsp;</option>
			<?php
				for($i=0;$i<sizeof($rsActionRequired);$i++) 
				{ 		
					echo "<OPTION value='".$rsActionRequired[$i]['actionCodeId']."'>".$rsActionRequired[$i]["actionDesc"]."</OPTION>\n"; 
				}
			?>		  
			</select>
			</td>
          </tr>

          <tr>
            <td width="16%">Remarks</td>
            <td width="84%">
					<input type="hidden" name="txtDocumentID" value="<? echo $arrFields[0]['documentId'];?>">
					<textarea id="txtRemarks" name="txtRemarks" rows="2" cols="50"></textarea>
			</td>
          </tr>
		    <tr>
            <td width="16%">Restricted</td>
            <td width="84%"><input type="checkbox" name="cbRestricted" value="1" checked>
			</td>
          </tr>
		  <tr>
            <td width="16%">Reply expected</td>
            <td width="84%"><input type="checkbox" name="cbReply">
			</td>
          </tr>
		    <tr align="center">
            <td colspan="2" align="center"><input type="submit" class="btn" name="btnAdd" value="Submit"></td>
            </tr>
        </table>
	</form>
	

	
		</td>
      </tr>
    </table>
<? 
// Test if employee/custodian has document for action
$rsReceivedActions = $objDocDetail->getReceivedActions($docID);

if(count($rsReceivedActions)!=0)
{
	$ctrAction=0;
	for($i=0;$i<count($rsReceivedActions);$i++) 
	{

		$history = $objDocDetail->checkHistoryActions($rsReceivedActions[$i]['historyId']);
		if(count($history)==0)
		{
			$ctrAction++;
		}
	}
}

if($ctrAction!=0)
{
?>
	<table width="100%"  border="0" id="listRecipient">
	  <tr>
		<td height="114">
		<table width="100%"  border="1" class="listings" >
	  <tr class="listheader">
		<td colspan="4">FOR ACTIONS</td>
		</tr>
	  <tr class="listheader">
	    <td width="24%">Date</td>
		<td width="29%">Sender</td>
		<td width="35%">Action Required /Remarks </td>
		<td width="12%">&nbsp;</td>
	  </tr>
	  <?php
		$rsReceivedActions = $objDocDetail->getReceivedActions($docID);
	
		for($i=0;$i<count($rsReceivedActions);$i++) 
		{
			$history = $objDocDetail->checkHistoryActions($rsReceivedActions[$i]['historyId']);
			
			if(count($history)==0)
			{
				$actionSender = $objDocDetail->displayRecipientSenderNames($rsReceivedActions[$i]['senderId'], $rsReceivedActions[$i]['senderUnit']);
				
				
				if($rsReceivedActions[$i]['restricted']){
					$arrHead=$objDocDetail->getOfficeHead($_SESSION["office"],$_SESSION["userUnit"]);
					$intHead=0;
					for($cntHead=0;$cntHead<count($arrHead);$cntHead++)
					{
						if($_SESSION["empNum"]==$arrHead[$cntHead]){
							$intHead=1;
							break;
						}
					}
					
					if(
						($rsReceivedActions[$i]['senderUnit']=="employee"&& $_SESSION["empNum"] ==$rsReceivedActions[$i]['senderId']) ||
						($rsReceivedActions[$i]['senderUnit']!="employee" && $rsReceivedActions[$i]['senderId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)) ||
						($rsReceivedActions[$i]['senderUnit']!="employee" && $rsReceivedActions[$i]['senderId']== $_SESSION["office"] && $intHead)
						){
							
							$strRemarks=$rsReceivedActions[$i]['remarks'];
						}
					elseif($rsReceivedActions[$i]['recipientUnit']=="employee" && $rsReceivedActions[$i]['recipientId']==$_SESSION["empNum"]) $strRemarks=$rsReceivedActions[$i]['remarks'];
					else if($rsReceivedActions[$i]['recipientUnit']!="employee"){
						if ($rsReceivedActions[$i]['recipientId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)){
							 $strRemarks=$rsReceivedActions[$i]['remarks'];
						}
						else{
							if($intHead && $rsReceivedActions[$i]['recipientId']== $_SESSION["office"]) $strRemarks=$rsReceivedActions[$i]['remarks'];
							else $strRemarks="";
						}
					}
					else
					$strRemarks="";
				}
				else{
					$strRemarks=$rsReceivedActions[$i]['remarks'];
				}
				
				
			  echo '<tr>
				<td>'.$rsReceivedActions[$i]['dateSent'].'</td>
				<td>'.$actionSender.'</td>
				<td>'.$rsReceivedActions[$i]['actionDesc'].'<br>'.'
				<i>'.$strRemarks.'</i>
				<td>
				<ul id="icons" class="ui-widget ui-helper-clearfix">  
				<li class="ui-state-default ui-corner-all" onClick=\'showActionReplyDialog("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-pencil" title="Add Action Reply"></span></li>
				<li class="ui-state-default ui-corner-all" onClick=\'showSetDeadlineDialog("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-clock" title="Set Own Deadline"></span></li></ul></td>';
				
				
			  echo '</tr>';
			  //<a href="javascript:getData(\'showAction.php?showForm=actionForm&ID='.$rsReceivedActions[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode='.$mode.'\',\''.$divId.'\')">Action</a></td>';
			  // <a href="#" onClick=\' $("#actionDialog").dialog("open"); doAction("'.$rsReceivedActions[$i]["historyId"].'","'.$docID.'"); \'>Action</a></td>';
			}
		}
	  ?>
	</table>
<?php } ?>
	<br>
	&nbsp;
	<?php $rsAction = $objDocDetail->showActions($docID); 
	
	if(count($rsAction)!=0)
	{
	?>
	<table width="100%"  border="1" class="listings">
  <tr class="listheader">
    <td colspan="6" align="center"><div align="left">ACTIONS MADE </div></td>
    </tr>
  <tr class="listheader">
    <td align="center" width="15%">Action Taken </td>
    <td align="center" width="20%">Date</td>
    <td align="center" width="20%">Recipient</td>
    <td align="center" width="18%">Action Required </td>
	 <td align="center" width="15%">Updated By </td>
    <td align="center" width="12%">&nbsp;</td>
  </tr>
    <?php
	
	
	for($i=0;$i<count($rsAction);$i++) 
	{
		$actionRecipient = $objDocDetail->displayRecipientSenderNames($rsAction[$i]['recipientId'], $rsAction[$i]['recipientUnit']);
	  echo '<tr>
	    <td>'.$rsAction[$i]['actionTakenDesc'].'</td>
		<td>'.$rsAction[$i]['dateSent'].'</td>
		<td>'.$actionRecipient.'</td>
		<td>'.$rsAction[$i]['actionDesc'].'<br><i>'.$rsAction[$i]['remarks'].'</i></td>
		<td>'.$objDocDetail->getEmployeeName($rsAction[$i]['addedById']).'</td>';
		// display details if there's a reply, display form otherwise	
		$checkRecipientReply = $objDocDetail->checkRecipientHistoryActions($rsAction[$i]["historyId"]);
		
		// check if action sent has no reply yet
		$checkRecipientReply2 = $objDocDetail->checkReplyActionSent($rsAction[$i]["historyId"]);
		
		 // check if action sent is a reply from previous action 
		$checkReplyAction = $objDocDetail->checkSenderHistoryActions($rsAction[$i]["referenceId"]);
		
		// check
		if(count($checkRecipientReply)!=0)
		{
			$replyStatus = '<li class="ui-state-default ui-corner-all" onClick=\'showActionReplyDetails("'.$rsAction[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-check" title="View recipient reply"></span></li>';
		}	
		elseif(count($checkRecipientReply2)==0 AND count($checkReplyAction)==0)					
		{
			$replyStatus = '<li class="ui-state-default ui-corner-all" onClick=\'showActionReplyDialog2("'.$rsAction[$i]["historyId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-arrowreturnthick-1-s" title="Add Reply"></span></li>';				
		}	
		elseif(count($checkReplyAction)!=0)
		{
			$replyStatus = '<li class="ui-state-default ui-corner-all" onClick=\'showActionSourceDetails("'.$rsAction[$i]["referenceId"].'","'.$docID.'","'.$mode.'","'.$divId.'","'.$historydiv.'"); \'><span class="ui-icon ui-icon-arrowreturnthick-1-n" title="View sender action"></span></li>';
		}	
		
			
		echo '<td><ul id="icons" class="ui-widget ui-helper-clearfix">  			
		'.$replyStatus.'
		<li class="ui-state-default ui-corner-all" onClick=" var msg= confirm(\'','Are you sure you want to delete this record?','\'); if(msg){ getData(\'showAction.php?ac=del&ID='.$rsAction[$i]["historyId"].'&div='.$divId.'&historydiv='.$historydiv.'&docID='.$docID.'&mode=del\',\''.$divId.'\') } else return (false);"><span class="ui-icon ui-icon-trash" title="Undo Action"></span></li></ul>
		</td>';
		//echo '<td><a href=',$_SERVER['PHP_SELF'],'?'.'&ac=del&ID='.$rsAction[$i]["historyID"].' onClick="javascript: return confirm(\'','Are you sure you want to delete this record?','\');return (false);">Delete</a></td>';
	  echo '</tr>';
  	}
  ?>
</table>	
	<?php }?>	</td>
  </tr>
</table>




