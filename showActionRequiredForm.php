<? @session_start();?>
<script language="javascript" type="text/javascript"> 
var validator = '';
  jQuery("#actreqrefresh").click( function(){ 
    //if($("#actreqedit").attr('value') == 'Save') $("#actreqedit").attr('value','Edit');
	jQuery('#ActionReqGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#ActionReqGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Action Description','Action Code'],
		colModel:[
			{name:'actionCodeId',index:'actionCodeId', width:65},
			{name:'actionCode',index:'actionCode', width:90},
			{name:'actionDesc',index:'actionDesc', width:100}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#actreqpager'),
		postData:{table:'tblActionRequired '},
		sortname: 'actionCodeId',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"Action Required"
	}).navGrid('#actreqpager',{edit:false,add:false,del:false});}); 
  });

    $('#actreqclear').click( function(){ 
	   $("#actReqId").val(''); 	   
	   $("#actReqCode").val(''); 
   	   $("#actReqDesc").val(''); 
	   $("#actreqsave").css('display','none');
	   $("#actreqadd").css('display','block');
	   $('#Clear_actreq').text(" Clear ");
	   validator.resetForm();
	   $('#actreqrefresh').click();
    });	
//Edit & Save function	
	jQuery("#actreqedit").click( function(){ 
	  var id = jQuery("#ActionReqGrid").getGridParam('selrow'); 
	  if (id) {
	   var ret = jQuery("#ActionReqGrid").getRowData(id); 
	   if(ret.actionCodeId == '1'){ 
	    alert('you can not edit this record');
	    $('#actreqclear').click(); }
	   else{
	   $("#actReqId").val(ret.actionCodeId); 	   
	   $("#actReqCode").val(ret.actionCode); 
   	   $("#actReqDesc").val(ret.actionDesc); 

	   if($("#actreqedit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
	    $("#actreqsave").css('display','block');
		$("#actreqadd").css('display','none');
		$('#Clear_actreq').text(" Cancel ");
	   }}}
	   else { jAlert('Please select row to edit', 'Warning');} 
	 }); 

	jQuery("#actreqsave").click( function(){ 
	  var actcid = $("#actReqId").val();
 	  var code = $("#actReqCode").val();
   	  var desc = $("#actReqDesc").val();

      $("#ActionReqGrid").setPostData({mode:"save",table:'tblActionRequired',ID:actcid,CODE:code,DESC:desc,MODULE:'Action Required'});
	  $("#ActionReqGrid").trigger("reloadGrid");
 	  $('#actreqclear').click();
	});
//Add function	   
	//jQuery("#actreqadd").click( function(){ 
	function AddRec(){
	  var actcid = $("#actReqId").val();
 	  var code = $("#actReqCode").val();
   	  var desc = $("#actReqDesc").val();
	  
      $("#ActionReqGrid").setPostData({mode:"add",table:'tblActionRequired',ID:actcid,CODE:code,DESC:desc,MODULE:'Action Required'});
	  $("#ActionReqGrid").trigger("reloadGrid");
  	  $('#actreqclear').click();
	  jAlert('Succesfully Added','Informationss'); } 

	 
	 
	$("#actreqdelete").click(function(){ 
	 var id = jQuery("#ActionReqGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if(id!= ''){ 
  	  if(id == '1'){ jAlert('You can not delete this record', 'Warning');
	   $('#actreqclear').click(); }
      else{
   	   jConfirm('Proceed deleting this record?', false, 'ERMS Confirmation Dialog', function(r) {
	    if(r==true){
		  $("#ActionReqGrid").setPostData({mode:"del",table:'tblActionRequired',ID:id.toString(),MODULE:'Action Required'});
		  $("#ActionReqGrid").trigger("reloadGrid"); 
	      $('#actreqclear').click();
		  jAlert('Succesfully deleted', 'Confirmation Results');} });
	  }}
	 else jAlert('Please select row to delete', 'Warning'); }); 
	 $('#actreqadd').click(function() {
      $('#frmActReq').submit();
     });
     $('#actreqrefresh').click();
//Form validation
 	$().ready(function() {
	 validator = $("#frmActReq").validate({
		rules: {
			actReqCode: "required",
			actReqDesc: "required"
		},
		messages: {
			actReqCode: "Action Required Code is required",
			actReqDesc: "Action Required Description is required"
		},
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });		 
</script>
<form method="get" id="frmActReq" autocomplete = "off">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Action Required  Description</td>
    <td class="field"><input type="text"  name="actReqCode" id="actReqCode" value=""></td>
	<td class="status"></td>
	<input type="hidden"  name="actReqId" id="actReqId" value="">
  </tr>
  <tr>
    <td class="label">Action Required Code</td>
    <td class="field"><input type="text"  name="actReqDesc" id="actReqDesc" value=""></td>	
    <td class="status"></td>
  </tr>
  
  <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="actreqedit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="actreqdelete"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="actreqadd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="actreqsave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="actreqclear"><span title='Clear'></span><div id="Clear_actreq">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="actreqrefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
  </tr>
</form>  
</table>
<table id="ActionReqGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="actreqpager" class="scroll" style="text-align:center;"></div>
			