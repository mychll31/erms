<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Outgoing.class.php");
require_once("class/jqSajax.class.php");
include_once("class/IncomingList.class.php");
$objList = new IncomingList;
$file=new General();
$ajax=new jqSajax();
$ajax->export("Load","file->Load");
$ajax->export("_decode","file->_decode");
$ajax->export("getTargetFile","file->getTargetFile");
$ajax->export("getTargetFile","file->removeItem");
$ajax->export("checkifConfidential","file->checkifConfidential");
$ajax->export("fileExist","file->fileExist");
$ajax->export("getFileOrigin","file->getFileOrigin");
$ajax->export("checkNotification","file->checkNotification");
$ajax->processClientReq();
$group = explode('~',$objList->getOfficeCode($objList->get('userID')));
?>	
<script type="text/javascript">
$(function() {
 $("a.barcode").fancybox({
		 'frameWidth' : 620, 
		 'frameHeight': 450, 
		 'overlayShow': true, 
		 'overlayOpacity': 0.7,
		 'hideOnContentClick': false
 });
 	$('#documentDateOutgoing, #deadlineOutgoing').datepicker({
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true		
	});
});
function printBarcode(barcode,group){
 $.get('showbarcode.php',{ code: barcode,group: group }, function(data) {
//   $("a.barcode").click();
    var thePopup = window.open( $("a.barcode").attr('href'), "barcode", "menubar=0,location=0,height=700,width=700" );
    //$('#popup-content').clone().appendTo( thePopup.document.body );
    thePopup.print();
 });
}
<?php $ajax->showJs(); ?>
</script>
<?

$objOutgoing = new Outgoing;
################## added by LCM #############
if($_GET['src']=="receive")
	{
	//echo $_GET['id'];
	$objList->markRead($_GET['id'],$objOutgoing->get('userID'));
	}
##############################################

if($_GET['mode']=="") $arrFields = $objOutgoing->getUserFields();
else $arrFields = $objOutgoing->getUserFields1();




################################
#
#  modes: new - for new Outgoing document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################

if($_REQUEST["mode"]=="getGroup")
	{	
		$userType=$objOutgoing->get('userType');
		$tmpGroupCode=$_REQUEST["officeGroup"];
		
		echo '<select id="cmbGroupCodeOutgoing" name="cmbGroupCodeOutgoing">';
		
		if($userType=='1'){
			$arEmp=$objOutgoing->getOfficeGroup($_REQUEST["officeCode"]);
			echo "<option value=''> </option>";
			$c2 = sizeof($arEmp);
			for($i2=0;$i2<$c2;$i2++)
			{
			?>		
				<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
			<?
			}
		}
		else
		{
		
			$empnum=$objOutgoing->get('empNum');
			$arEmp=$objOutgoing->getOfficeGroupEmployee($empnum);
			$c2 = sizeof($arEmp);
			for($i2=0;$i2<$c2;$i2++)
			{
			?>		
				<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
			<?
			}
		}
		echo "</select>";
	die();
	}
	
	
if($arrFields['mode']=='edit' ||  $arrFields['mode']=='view')
{
$rsDocument= 		$objOutgoing->getDocument($arrFields['id']);
$t_strNewId = 		$rsDocument[0]['documentId'];
$t_strReferenceId=	$rsDocument[0]['referenceId'];
$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
$t_strSubject =    	$rsDocument[0]['subject'];
$t_intOriginId =   	$rsDocument[0]['originId'];
$t_intGroupCode= 	$rsDocument[0]['originGroupId'];
$deadlineOutgoing = $rsDocument[0]['deadline'];
$t_intContainer =   $rsDocument[0]['fileContainer'];
$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
$t_strRemarks =    	$rsDocument[0]['remarks'];
$t_intConfidential =$rsDocument[0]['confidential'] ;
$DocNum =			$rsDocument[0]['docNum'];
$t_intOfficeId=		$rsDocument[0]['officeSig'];
$addedByOfficeId =  $rsDocument[0]['addedByOfficeId'];
$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
$signatoryOutgoing=	$rsDocument[0]['sender'];
$disable_ID="disabled=\"disabled\"";
$nextMode="update";
}
elseif ($arrFields['mode']=='save')
{
//echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
//print_r($_POST);
	$result=$objOutgoing->addOutDocument($arrFields);
		if ($result==0)
		{
		$arrFields['mode']="new";
		$nextMode="save";
		}
		else
		{
		$nextMode="update";
		$disable_ID="disabled=\"disabled\"";
		}
	$msg = $objOutgoing->getValue('msg');
	// assign saved data to view
	if ($result==0)
		{
	$t_strNewId  =    $arrFields['t_strDocId'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];	
	$documentDateOutgoing =    $arrFields['documentDateOutgoing'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_intOriginId =       $arrFields['cmbOrigin'];
	$deadlineOutgoing =       $arrFields['deadlineOutgoing'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$DocNum = $arrFields['t_strDocNum'];
	$t_intOfficeId=		$arrFields['cmbOriginOutgoing'];
	$signatoryOutgoing=	$arrFields['signatoryOutgoing'];
	$addedByOfficeId = $objOutgoing->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else
	{
	$rsDocument= 		$objOutgoing->getDocument($arrFields['t_strDocId']);
	$t_strNewId = 		$rsDocument[0]['documentId'];
	$t_strReferenceId=	$rsDocument[0]['referenceId'];
	$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
	$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
	$t_strSubject =    	$rsDocument[0]['subject'];
	$t_intOriginId =   	$rsDocument[0]['originId'];
	$deadlineOutgoing = $rsDocument[0]['deadline'];
	$t_intContainer =   $rsDocument[0]['fileContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$t_strRemarks =    	$rsDocument[0]['remarks'];
	$t_intConfidential =$rsDocument[0]['confidential'];
	$DocNum =			$rsDocument[0]['docNum'];
	$t_intOfficeId=		$rsDocument[0]['officeSig'];
	$signatoryOutgoing=	$rsDocument[0]['sender'];
	$addedByOfficeId =  $rsDocument[0]['addedByOfficeId'];
	$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
	$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
	}
}
elseif($arrFields['mode']=='update')
{
	$result=$objOutgoing->updateOutDocument($arrFields);
	$msg = $objOutgoing->getValue('msg');
	$disable_ID="disabled=\"disabled\"";
	if($result==0){
	
	$arrFields['mode']="edit";
	$nextMode="update";
	$t_strNewId  =    $arrFields['t_strDocId'];
	$documentDateOutgoing =    $arrFields['documentDateOutgoing'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_intOriginId =       $arrFields['cmbOrigin'];
	$deadlineOutgoing =       $arrFields['deadlineOutgoing'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$DocNum = $arrFields['t_strDocNum'];
	$t_intOfficeId=		$arrFields['cmbOriginOutgoing'];
	$signatoryOutgoing=	$arrFields['signatoryOutgoing'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else
	{
	$rsDocument= 		$objOutgoing->getDocument($arrFields['t_strDocId']);
	$t_strNewId = 		$rsDocument[0]['documentId'];
	$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
	$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
	$t_strSubject =    	$rsDocument[0]['subject'];
	$t_intOriginId =   	$rsDocument[0]['originId'];
	$deadlineOutgoing = $rsDocument[0]['deadline'];
	$t_intContainer =   $rsDocument[0]['fileContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$t_strRemarks =    	$rsDocument[0]['remarks'];
	$t_intConfidential =$rsDocument[0]['confidential'];
	$t_strReferenceId=$rsDocument[0]['referenceId'];
	$DocNum =			$rsDocument[0]['docNum'];
	$t_intOfficeId=		$rsDocument[0]['officeSig'];
	$signatoryOutgoing=	$rsDocument[0]['sender'];
	$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
	$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
	$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
	$t_intConfidential = ($t_intConfidential == '1')?'1':'0';
	if($t_intConfidential==1) $objOutgoing->transferFile($t_strNewId,$t_intConfidential);  
	}
	

}
else //New
{
	$t_strNewId=$objOutgoing->getNewId("outg");
	$t_intDocTypeId=-1;
	$t_intOriginId=-1;
	$dateReceived = date("Y-m-d");
	$disable_ID="";
	$nextMode="save";
	if ($objOutgoing->get("userType")==2) $t_intOfficeId=$objOutgoing->get("office");  //for custodian account
}

$arDocType=$objOutgoing->getDocType("");
$arOrigin=$objOutgoing->getOriginOffice("");
$arOffices=$objOutgoing->getOfficeFromExeOffice();
$arContainer=$objOutgoing->getContainer($objOutgoing->get("office"),"");
$arManagedOffice=$objOutgoing->getManagedOffice($objOutgoing->get('empNum'));

?>
 	<? if($msg<>""){
	if(strpos($msg,"Duplicate entry")!==false){
		?>
		 <script type="text/javascript">
		 if(confirm("Document ID already existing. Get new ID?")==true){
		     $("#idcontaineroutgoing").load("showOutgoing.php?mode=new #idcontaineroutgoing",function(){
			 	alert("Please save again");
			 });
		 }		 
		 </script>
		<?
		}
		else{
		?>
			<div class="pane">
			<div class="ui-widget" style="width:40%">
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
					<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Notice: </strong><? echo $msg;?></p>
				</div>
			</div>
			</div>
			<script type="text/javascript">
				setTimeout("disappear()",2000);
				$(function(){ 
				  $.ajax({ 
					url: "manageBarcodeConf.php",
					data: "mode=select&employee_num=<? echo $objList->get('userID'); ?>",
					success: function(data){ 
					 if(data==1){ printBarcode("<? echo $t_strNewId;?>","<? echo $group[0]; ?>"); }
					}
				  });
				});
			</script>
	<? }} ?>
<? if($arrFields["mode"]=="save" || $arrFields["mode"]=="update" || $arrFields["mode"]=="view"){
	?>
	<table align="center" width="605px" class="documentContainer">
	<tr><td>
			<table align="center" width="605px" class="datawrap">
				<tr><td>
				<table class="metadata">
					<tr class="metabutton">
						<td colspan="3" class="containerlabel">Document Details</td>
						<td style="text-align:right">
						<? if($objOutgoing->isCustodian($objOutgoing->get("empNum"),$addedByOfficeId) and $arrFields["edit"]!="false"){
						?>
						<input type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showOutgoing.php?mode=edit&id=<? echo $t_strNewId;?>','outg');" class="btn"/>&nbsp;	
						<input type="button" name="addForm" value="Add New" onClick="getData('showOutgoing.php?mode=new','outg');" class="btn"/>
						<input type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />	
						</td>
						<? }?>
						
					</tr>
					  <tr>
						<th width="100px">Document ID : </th>
						<td width="200px"><?php echo $t_strNewId; ?>&nbsp;</td>
						<th width="100px">Document Type : </th>
						<td width="200px"><?php echo $rsDocTypeAbbrev[0]['documentTypeAbbrev']; ?>&nbsp;</td>
					  </tr>
					  <tr>
						<th>Deadline : </th>
						<td><? echo $deadlineOutgoing;?></td>
						<th>Document Date : </th>
						<td><?php echo $documentDateOutgoing; ?>&nbsp;</td>
					  </tr>
					  <? if($DocNum<>""){?>
		<tr><td colspan="4">&nbsp;</td></tr>
		<tr>
			<th>Doc No. : </th>
			<td><? echo $DocNum;?></td>
			<td></td>
			<td></td>		
		</tr>
		<? }?>
					  <tr>
						<th >Subject : </th>
						<td><!--DWLayoutEmptyCell-->&nbsp;</td>
						<td><!--DWLayoutEmptyCell-->&nbsp;</td>
						<td><!--DWLayoutEmptyCell-->&nbsp;</td>
					  </tr>
	
					  <tr>
						<th></th>
						<td colspan="3"><?php echo $t_strSubject; ?>&nbsp;</td>
					  </tr>
	
					  <tr>
						<th>Origin : </th>
						<td><?php echo $rsOriginDetails[0]['oName'];//$t_intOfficeId; ?>&nbsp;</td>
						<th>Signatory : </th>
						<td><? echo $signatoryOutgoing;?></td>
					  </tr>
					  <tr>
						<th>File Container : </th>
						<td><?php echo $rsContainer[0]['label']; ?>&nbsp;</td>
						<th class="head" rowspan="2">Remarks : </th>
						<td rowspan="2" valign="top"><? echo $t_strRemarks;?></td>
					  </tr>
				
					 <tr><th>Related Docs:</th><td colspan="3"></td></tr>
					 <tr><td>&nbsp;</td><td colspan="3"><div class="relatedDocs"><?
						$arrRelated=$objOutgoing->getRelatedDocs($t_strNewId);
						$cRelated=count($arrRelated);
						if($cRelated>0) {
							$intStatus = $objOutgoing->getDocumentStatus($arrRelated[0]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo "<a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[0]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[0]["documentId"]."</a>";
							}
						for($cntRelated=1;$cntRelated<$cRelated;$cntRelated++)
						{
						
						$intStatus = $objOutgoing->getDocumentStatus($arrRelated[$cntRelated]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo ", <a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[$cntRelated]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[$cntRelated]["documentId"]."</a>";
						
						    //echo ", ".$arrRelated[$cntRelated]["documentId"];
						}
						?>
						</div>
					 </td></tr>
					 <tr>
						<th>Owned by : </th>
						<td><? echo $addedByOfficeId;?></td>
						<th>&nbsp;</th>
						<td rowspan="2">&nbsp;</td>
					</tr>
					  <tr>
						<td colspan="4">&nbsp;</td>
					  </tr>
	 <? if ($t_intConfidential)
		{
		?>
						<tr>
							<td colspan="4" style="text-align:center">
							<div class="ui-widget" align="center" >
								<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; width:140px"> 
									<p align="left"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Alert:</strong> Confidential.</p>
								</div>
							</div>
							</td>
						</tr>
						 <tr>
						 <td colspan="4">&nbsp;</td>
					     </tr>
		<?
		}
		?>
		
		
					  </table><!-- end of metadata -->
				</td></tr>
			</table> <!-- end of datawrap -->
	</td></tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>
				
				
				<table class="datawrap" width="605px" >
					<tr><td>
						<table width="600px" class="metadata">
							<tr class="metabutton">
								<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="outgoingfilelinkimg" /><a onClick="javascript:toggleWindowAjax2('filebodyoutgoing','outgoingfilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyoutgoing','addOut','<? echo $addedByOfficeId; ?>');">Manage Files</a><div id="loading_widget" style="float:right; display:none;">LOADING&nbsp;<img src="css/images/ajax-loader.gif" /></div></td>
<td><div id="addOut"></div></td>
							</tr>
							<tr>
								<td></td>
								<td width="590px">
<div id="filebodyoutgoing" style="display:none">
		<!-- ############################# File Content #####################################-->

		<!-- #################################################################################-->
		
</div>
								</td>
							</tr>
						</table>
						</td>
					</tr>
	
		      </table>

			  </td>
		</tr>
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr><td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="outgoinghistorylinkimg" /><a href="javascript:getData('showAction.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=docubodyoutgoing&historydiv=actionbodyoutgoing','docubodyoutgoing');" onClick="return toggleWindowAjax('docubodyoutgoing','outgoinghistorylinkimg');" id="outgoinghistorylink">Update Action</a></td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
<!-- 	***************************		history				********************************************	-->
			<div id="docubodyoutgoing" align="center" style="visibility:hidden">
			</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr>
					<td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel">
								<img src="images/menu-bar-right-arrow.gif" id="outgoingactionlinkimg" /><a href="javascript:getData('showHistory.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=actionbodyoutgoing','actionbodyoutgoing');" onClick="return toggleWindowAjax('actionbodyoutgoing','outgoingactionlinkimg');" id="outgoingactionlink">View History</a>
							</td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
								<div id="actionbodyoutgoing" align="center" style="visibility:hidden">
								</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
</table>
	
	
<? }else //mode = new or edit
{
?>
 <script language="javascript" type="text/javascript">
	  var arOriginOutgoing = new Array(<? echo sizeof($arOffices);?>);
	  <?

	  for($i=0;$i<sizeof($arOffices);$i++)
	  echo "arOriginOutgoing[".$i."]=new Array(".$intColumnNum.");\n";

	  if ($t_intOfficeId=="") $t_intOfficeId=$arOffices[0]['officeCode']; //sets first office code for RMS account

  	  for($i=0;$i<sizeof($arOffices);$i++)
	  {
	  	echo "arOriginOutgoing[".$i."][0]='".$arOffices[$i]['officeCode']."';\n";
	  	echo "arOriginOutgoing[".$i."][1]='".$arOffices[$i]['officeName']."';\n";
	  	echo "arOriginOutgoing[".$i."][2]='".$arOffices[$i]['empNumber']."';\n";
		echo "arOriginOutgoing[".$i."][3]='".$arOffices[$i]['head']."';\n";
		echo "arOriginOutgoing[".$i."][4]='".$arOffices[$i]['title']."';\n";
		echo "arOriginOutgoing[".$i."][5]='".$arOffices[$i]['officeTag']."';\n";
		if($signatoryOutgoing=="" && $mode=="new") // sets signatory of the office for new docs
			{
				if($arOffices[$i]['officeCode']== $t_intOfficeId) $signatoryOutgoing= $arOffices[$i]['head'];
			}
	  }

	  ?>
		function getSignatory(i)
		{
		for(x=0;x<arOriginOutgoing.length;x++)
		{
		if(i==arOriginOutgoing[x][0]) 
			{
				document.getElementById("signatoryOutgoing").value=arOriginOutgoing[x][3];
				//document.getElementById("cmbOriginOutgoing").title=arOriginOutgoing[x][1];;	
			}
		}
		getData("showOutgoing.php?mode=getGroup&officeCode="+i+"&officeGroup=<? echo $t_intGroupCode;?>","outgGroupDiv");
		}
		function x () {
					var oTextbox = new AutoSuggestControl(document.forms['frmOutgoing'].t_strDocType, new RemoteStateSuggestions());        
            } 
			x();
			$("#cmbOriginOutgoing").change();
			$("#documentOwnerOutgoing").change(function(){
			var src = $(this).val();
			$("#idcontaineroutgoing").load("showOutgoing.php?getOfficeID="+src+ " #idcontaineroutgoing");
			});
	  </script>
<form action="javascript:check(document.getElementById('frmOutgoing'),'outg','showOutgoing.php');" name="frmOutgoing" id="frmOutgoing" onsubmit="return checkConfi(document.getElementById('frmOutgoing'));">
<table align="center" width="600" class="tblforms">
    <tr> 
       	<th width="97">Document ID </td>
    	<td width="175"><div id="idcontaineroutgoing"><input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required" <? echo $disable_ID;?> style="width:150px"></div>
	<input type="hidden" name="mode" value="<? echo $nextMode;?>"></td>
    	<th width="100"  class="head">Document Date</td>
   	  <td width="228"><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="documentDateOutgoing" name="documentDateOutgoing" value="<? echo $documentDateOutgoing;?>"></td>
    </tr>
    
	<tr> 
    	<th>Document Type</td>
      	<td><table>
		<tr><td>
        <div id="divDocType">
        <select name="cmbDocType" class="caption" style="width:250px">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeDesc'];?></option>
	  <?
	  }
	  ?>
        </select>
		<!--input type="text" name="t_strDocType" id="t_strDocType" value="<? echo $t_strDocType;?>"autocomplete=OFF  /-->
        </div>
        </td> 
		<td>
		<? if($objOutgoing->get('userType')==1) {?>
		<span class='ui-icon ui-icon-plus' title='Add New Document Type' onclick="displayDialog('showLibrary_popup.php?mode=doctype');"></span>
		<? }?>
		</td></tr></table>
		</td>   
		<th>Deadline</td>
    	<td><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="deadlineOutgoing" name="deadlineOutgoing" value="<? echo $deadlineOutgoing;?>"> </td>      
    </tr>
	
    <tr> 
      	<th>Reference ID</td>
      	<td><input type="text" name="t_strReferenceId" id="t_strReferenceId" value="<? echo $t_strReferenceId;?>"></td>
      	<td><!--DWLayoutEmptyCell-->&nbsp;</td>
      	<td><!--DWLayoutEmptyCell-->&nbsp; </td>
      
    </tr>
	
	<tr>
	  <th>Doc No.</td>
	  <td colspan="3"><br /><input type="text" name="t_strDocNum" id="t_strDocNum" value="<? echo $DocNum;?>" /><span class="required" style="text-transform:none">&nbsp;(i.e. AO 001)</span></td>
  </tr>
    <tr> 
      	<th>Subject</td>
	    <td colspan="3"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3" title="required"><? echo $t_strSubject; ?></textarea>

    </tr>
	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><br /><br /></td>  
	</tr>
<tr> 
    	<th>Origin</td>
      	<td>
		<select name="cmbOriginOutgoing" id="cmbOriginOutgoing" class="caption" onchange="getSignatory(this.value);" style="width:172px" title="" <? if($objOutgoing->get("userType")==2) echo "disabled";?>>
	  <?
	  for($i=0;$i<sizeof($arOffices);$i++)
	  {    
	  ?>
	  <option value="<? echo $arOffices[$i]['officeCode']; ?>" <? if($arOffices[$i]['officeCode']== $t_intOfficeId) echo "selected"; ?> title="<? echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);?>"> <? 
	  	if ($objOutgoing->get("userType")==2) echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);
		else echo $arOffices[$i]['officeName'];?></option>
	  <?
	  }
	  ?>
        </select></td>   
		<th>Signatory</td>
    	<td><input type="text" id="signatoryOutgoing" name="signatoryOutgoing" value="<? echo $signatoryOutgoing;?>"> </td>      
    </tr>	
	<tr>
		<th>Group</th>
		<td colspan="3">
			<div id="outgGroupDiv"><select id="cmbGroupCodeOutgoing" name="cmbGroupCodeOutgoing"></select></div>
		</td>
	</tr>
	<tr> 
   		<th>Remarks</td>
		<td  valign="top"><textarea cols="30" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"><? echo $t_strRemarks;?></textarea></td>
		<th valign="top">File Container</td>
		<td valign="top"><select name="t_intContainer" class="caption">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
	  <?
	  }
	  ?>
        </select></td>
   	</tr>
   
   	<tr> 
		<th valign="middle">Confidential</td>
		<td valign="top"><input type="checkbox" value="1" name="t_intConfidential" id="t_intConfidential" <? 
	  if ($t_intConfidential=='1') echo "checked=\"checked\"";
	  ?>></td>
	 	<td>&nbsp;</td>
	 	<td>&nbsp;</td>
   	</tr>
	<tr> <!--$arManagedOffice=$objIncoming->getManagedOffice();-->
      <th valign="top">Document Owner </td>
      <td valign="top"><select id="documentOwnerOutgoing" name="documentOwner" <? echo $disable_ID;?>>
	   <?
	  for($i=0;$i<sizeof($arManagedOffice);$i++)
	  {
	  $managedOffice=(trim($arManagedOffice[$i]["groupCode"])=="")?$arManagedOffice[$i]["officeCode"]:$arManagedOffice[$i]["groupCode"];
	  ?>
	  <option value="<? echo $managedOffice; ?>" <? if($managedOffice== $t_strManagedOffice) echo "selected"; ?> > <? echo $managedOffice;?></option>
	  <?
	  }
	  ?>
	  </select></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:center">
			<? if ($arrFields['mode'] == "new"){?>
				<input type="submit" value="Save" class="btn" onclick="">
			<? }else{ ?>
			  	<input type="submit" value="Update" class="btn" onclick="">
			<? }  ?>
  			<input type="reset" value="Clear" class="btn"></td>
	</tr>
	<!-- 
	 <tr> 
      <td height="24" valign="top">Action to be Taken</td>
      <td valign="top"><select name="select5" class="caption">
			<option selected>for Info</option>
          <option>for Signature</option>
          <option>for Comments</option>
        </select></td>
      <td valign="top">File Container</td>
      <td valign="top"><input name="text" type="text" class="caption"></td>
 
    </tr>
    <tr> 
      <td height="27" valign="top">Action Unit</td>
      <td valign="top"><select name="select6" multiple class="caption">
          <option selected></option>
          <option>ASTI</option>
          <option>NAST</option>
          <option>NRCP</option>
        </select></td>
      <td rowspan="2" valign="top">Mode of Delivery</td>
      <td rowspan="2" valign="top"><input name="text2" type="text" class="caption"></td>
  
    </tr>
	<td valign="top">Mode of Delivery</td>
    <td valign="top"><input name="t_strDelivery" id="t_strDelivery" type="text" value="<? echo $t_strDelivery;?>" ></td>
	-->
  </table>
</form>			

<?
} ?>
<a class='barcode' href="showbarcode.php?code=<? echo $t_strNewId;?>"></a>
