<? @session_start(); ?>
<script language="javascript" type="text/javascript"> 
var validator='';
  jQuery("#refresh").click( function(){ 
  //if($("#edit").val() == 'Save') $("#edit").val('Edit');
	jQuery('#doctypegrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#doctypegrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Abbreviation', 'Description', 'Retention Period'],
		colModel:[
			{name:'documentTypeId',index:'documentTypeId', width:65},
			{name:'documentTypeAbbrev',index:'documentTypeAbbrev', width:90},
			{name:'documentTypeDesc',index:'documentTypeDesc', width:100},
			{name:'retentionPeriod',index:'retentionPeriod', width:80, align:"center"}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#doctypepager'),
		postData:{table:'tblDocumentType'},
		sortname: 'documentTypeId',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"Document Type"
	}).navGrid('#doctypepager',{edit:false,add:false,del:false});}); 
  });
  
    $('#clear').click( function(){ 
	   $("#docTypeId").val(''); 	   
	   $("#docTypeAbbrv").val(''); 
   	   $("#docTypeDesc").val(''); 
   	   $("#retentionPeriod").val(''); 
	   $("#save").css('display','none');
	   $("#add").css('display','block');
	   $('#Clear_doctype').text(" Clear ");
	   validator.resetForm();
	   $('#refresh').click();
    });	
//Edit & Save function	
	jQuery("#edit").click( function(){ 
	  var id = jQuery("#doctypegrid").getGridParam('selrow'); 
	  if (id) {
	   var ret = jQuery("#doctypegrid").getRowData(id); 
	   $("#docTypeId").val(ret.documentTypeId); 	   
	   $("#docTypeAbbrv").val(ret.documentTypeAbbrev); 
   	   $("#docTypeDesc").val(ret.documentTypeDesc); 
   	   $("#retentionPeriod").val(ret.retentionPeriod); 
   	   //$("#save").html('<input type="button" value="Save" id="save" onclick="">');
	
	   if($("#edit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
   	    $("#save").css('display','block');
		$("#add").css('display','none');
		$('#Clear_doctype').text(" Cancel ");
		}}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 

	jQuery("#save").click( function(){ 
	  var docid = $("#docTypeId").val();
 	  var abbrv = $("#docTypeAbbrv").val();
   	  var desc = $("#docTypeDesc").val();
   	  var rPeriod = $("#retentionPeriod").val();
      $("#save").css('display','none');
      $("#add").css('display','block'); 
      $("#doctypegrid").setPostData({mode:"save",table:'tblDocumentType',ID:docid,CODE:abbrv,DESC:desc,PERIOD:rPeriod,MODULE:'Document Type'});
	  $("#doctypegrid").trigger("reloadGrid");
 	  $('#clear').click();
	});
//Add function	   
	function AddRec(){ 
	  var docid = $("#docTypeId").val();
 	  var abbrv = $("#docTypeAbbrv").val();
   	  var desc = $("#docTypeDesc").val();
   	  var rPeriod = $("#retentionPeriod").val();
	  
      $("#doctypegrid").setPostData({mode:"add",table:'tblDocumentType',ID:docid,CODE:abbrv,DESC:desc,PERIOD:rPeriod,MODULE:'Document Type'});
	  $("#doctypegrid").trigger("reloadGrid");
  	  $('#clear').click(); 
	  jAlert('Succesfully Added','Informationss');}


	 
	$("#delete").click(function(){ 
	 var id = jQuery("#doctypegrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if( id != ''){ 
	 jConfirm('Proceed deleting this record?', false,'ERMS Confirmation Dialog', function(r) {
	 if(r==true){
	  $("#doctypegrid").setPostData({mode:"del",table:'tblDocumentType',ID:id.toString(),MODULE:'Document Type'});
	  $("#doctypegrid").trigger("reloadGrid"); 
	  $('#clear').click();
	  jAlert('Succesfully deleted', 'Confirmation Results');} });
	  }
	 else jAlert('Please select row to delete', 'Warning'); }); 
     $('#refresh').click();
	 
	 $('#add').click(function() {
      $('#doctypefrm').submit();
     });
//form validation
	$().ready(function() {
	 validator = $("#doctypefrm").validate({
		rules: {
			retentionPeriod:{ required: true,
							  digits: true  },
			docTypeAbbrv: "required",
			docTypeDesc: "required"
		},
		messages: {
			docTypeAbbrv: "Document Type Code is required!",
			docTypeDesc: "Document Type description is required!",
			retentionPeriod: {
				   required:"Retention Period is required!" ,
				   digits: "Invalid Input"
				   }
		},
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	
 
 
</script>
<form id="doctypefrm" method="get" autocomplete="off">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Document Type Abbreviation</td>
    <td class="field"><input type="text" id="docTypeAbbrv" name="docTypeAbbrv"/></td>
	<td class="status"></td>
	<input type="hidden" id="docTypeId" value="">
  </tr>
  <tr>
    <td class="label">Document Type Description</td>
    <td class="field"><input type="text" id="docTypeDesc" name="docTypeDesc"/><td>
	<td class="status"></td>
  </tr>
  <tr>
    <td class="label">Retention Period</td>
    <td class="field"><input type="text" size = "5" id="retentionPeriod" name="retentionPeriod" /><td>
	<td class="status"></td>
  </tr>
  </tr>

 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="edit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="delete"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="add"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="save" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="clear"><span title='Clear'></span><div id="Clear_doctype">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="refresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>
  
</table>
</form>	
<table id="doctypegrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="doctypepager" class="scroll" style="text-align:center;"></div>
