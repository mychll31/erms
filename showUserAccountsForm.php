<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/DocumentType.class.php");

$objDocType = new DocType;
$arrFieldsvalue = $objDocType->getUserFields();
$objDocType->addDocument($arrFieldsvalue);
?>
<form action="javascript:check(document.getElementById('frmUserAccnts'),'doctype','showUserAccountForm.php');" name="frmUserAccnts" id="frmUserAccnts" >
<table width="60%" class="textbody">
  <!--DWLayoutTable-->
   <tr align = "left">
    <td width="93" valign="top">Employee name</td>
    <td width="175">
	 <select name="select6[]" multiple class="caption">
          <option selected value="0"></option>
		  <option value="1">Philip Salvador</option>
          <option value="2">Louie Mandapat</option>
          <option value="3">Edgardo Catorce</option>
		  <option value="4">Abuel, Francis</option>
		  <option value="5">Dotimas, Marilen</option>
		  <option value="6">Monroyo, George</option>
        </select>
	</td>	
  </tr>
  <tr align = "left"> 
    <td width="97">User Name</td>
    <td width="175"><input type="text" class="caption" name="userName" id="userName"><span id="t_strDocTypeAbbrev_div" class="required"></span></td>
	<input type="hidden" name="mode" value="add" />
  </tr>
  <tr align = "left">
    <td width="93">User Password</td>
    <td width="175"><input type="text" class="caption" name="userPass" id="userPass"></td>	
  </tr>
  <tr align = "left"> 
    <td height="35" valign="top" align = "left">Permission</td>
    <td width="175">
	     <select name="userPermission" multiple class="caption">
          <option selected value="0"></option>
		  <option value="1">employee</option>
          <option value="2">administrator</option>
        </select>
	</td>	
  </tr>
  <tr><td colspan="4" align="center">
   <input type="submit" name="submit" value="Add" class="caption" onclick="">
   <input type="reset" value="Clear" class="caption" onclick="">
  </td></tr>
</table>
</form>			
