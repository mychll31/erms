<?php 
class Library_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
	
	// for account names
	public function getAccountNames($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblaccountnames.act_id='".$intId."'";

		$strSQL = "SELECT * FROM tblaccountnames
		WHERE 1=1
		$whereId
		ORDER BY act_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function addAccount($arrData){
		$this->db->insert('tblaccountnames', $arrData);
		return $this->db->insert_id();
	}
	public function updateAccount($arrData, $intId)
	{
		$this->db->where('act_id',$intId);
		$this->db->update('tblaccountnames', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteAccount($intId)
	{
		$this->db->where('act_id',$intId);
		$this->db->delete('tblaccountnames');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	// end of for account names
	
	// for registered owners
	public function getRegisteredOwners($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblorigowners.orig_owner_id='".$intId."'";
/*
		$strSQL = "SELECT tblorigowners.*, tblaccountnames.*  FROM tblorigowners
		LEFT JOIN tblaccountnames ON tblorigowners.act_id = tblaccountnames.act_id
		WHERE 1=1
		$whereId
		ORDER BY orig_owner_name";
*/
	$strSQL = "SELECT tblorigowners.*  FROM tblorigowners
		WHERE 1=1
		$whereId
		ORDER BY orig_owner_name";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function addOrigOwner($arrData){
		$this->db->insert('tblorigowners', $arrData);
		return $this->db->insert_id();
	}

	public function updateOrigOwner($arrData, $intId)
	{
		$this->db->where('orig_owner_id',$intId);
		$this->db->update('tblorigowners', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteOrigOwner($intId)
	{
		$this->db->where('orig_owner_id',$intId);
		$this->db->delete('tblorigowners');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	// end for registered owners
	
	// for land class
	public function getLandClassifications($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbllandclass.lcls_id='".$intId."'";

		$strSQL = "SELECT * FROM tbllandclass
		WHERE 1=1
		$whereId
		ORDER BY lcls_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function addLandClass($arrData){
		$this->db->insert('tbllandclass', $arrData);
		return $this->db->insert_id();
	}
	public function updateLandClass($arrData, $intId)
	{
		$this->db->where('lcls_id',$intId);
		$this->db->update('tbllandclass', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteLandClass($intId)
	{
		$this->db->where('lcls_id',$intId);
		$this->db->delete('tbllandclass');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	// end of for land class
	
	// for user accounts
	public function getUserLevels($intId='')
	{
		$whereId='';
		if($whereId!='')
			$whereId .= " AND tbluserlevels.ulvl_id = '".$intId."'";
			
		$strSQL = "SELECT * FROM tbluserlevels
		WHERE 1=1
		$whereId
		";	
		
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();

	}
	
	public function getUserAccounts($intId='')
	{
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblusers.usr_id = '".$intId."'";
			
		$strSQL = "SELECT tblusers.*, tbluserlevels.ulvl_name FROM tblusers
		LEFT JOIN tbluserlevels ON tblusers.usr_level = tbluserlevels.ulvl_id
		WHERE 1=1
		$whereId
		";	
		
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();

	}
	public function addUserAccount($arrData){
		$this->db->insert('tblusers', $arrData);
		return $this->db->insert_id();
	}
	public function updateUserAccount($arrData, $intId)
	{
		$this->db->where('usr_id',$intId);
		$this->db->update('tblusers', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteUserAccount($intId)
	{
		$this->db->where('usr_id',$intId);
		$this->db->delete('tblusers');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function resetPassword($intId, $strPassword)
	{
		$this->db->set('usr_password',$strPassword);
		$this->db->where('usr_id',$intId);
		$this->db->update('tblusers');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	
	// end for user accounts
	
	
	// for regions
	public function getRegions($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblregions.rgn_id='".$intId."'";

	$strSQL = "SELECT tblregions.*  FROM tblregions
		WHERE 1=1
		$whereId
		ORDER BY rgn_display_order";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function addRegion($arrData){
		$this->db->insert('tblregions', $arrData);
		return $this->db->insert_id();
	}

	public function updateRegion($arrData, $intId)
	{
		$this->db->where('rgn_id',$intId);
		$this->db->update('tblregions', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteRegion($intId)
	{
		$this->db->where('rgn_id',$intId);
		//$this->db->delete('tblorigowners');
		$this->db->set('rgn_is_deleted', '1');
		$this->db->update('tblregions');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	// end for registered owners
	
	
}
?>