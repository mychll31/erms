<?php 
class Title_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}


	public function addTitle($arrData){
		$this->db->insert('tbltitles', $arrData);
		return $this->db->insert_id();
	}

	public function updateTitle($arrData, $intId)
	{
		$this->db->where('tct_id',$intId);
		$this->db->update('tbltitles', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}

	public function getTitles($intId='', $bnWithTitle='', $bnIfConsolidated='', $bnForValidation='', $txtTCTNo='', $bnWithLeaseContract='', $intRgnId='', $intPrvId='', $txtType='', $intAcctId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbltitles.tct_id='".$intId."'";
		if($bnWithTitle=='N')
			$whereId .= " AND tbltitles.tct_with_title='".$bnWithTitle."'";
		if($bnIfConsolidated=='FC' || $bnIfConsolidated=='Y')
			$whereId .= " AND tbltitles.tct_if_consolidated='".$bnIfConsolidated."'";	
		if($bnForValidation=='Y')
			$whereId .= " AND tbltitles.tct_for_validation='".$bnForValidation."'";		
		if($txtTCTNo!='')
			$whereId .= " AND (tbltitles.tct_orig_no='".$txtTCTNo."' OR tbltitles.tct_trc_no='".$txtTCTNo."')";
		if($bnWithLeaseContract=='Y')
			$whereId .= " AND tbltitles.tct_with_lease_cont='".$bnWithLeaseContract."'";		
		if($intRgnId!='')
			$whereId .= " AND tblregions.rgn_id='".$intRgnId."'";
		if($intPrvId!='')
			$whereId .= " AND tbltitles.prv_id='".$intPrvId."'";
		if($txtType!='')
			$whereId .= " AND tbltitles.tct_type='".$txtType."'";	
		if($intAcctId!='')
			$whereId .= " AND tblaccountnames.act_id='".$intAcctId."'";	
			
		$strSQL = "SELECT tbltitles.*, tblaccountnames.act_name, tblprovinces.prv_name, tblregions.*, tblorigowners.*, tbllandclass.lcls_name FROM tbltitles 
		LEFT JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id 
		LEFT JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id 
		LEFT JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		LEFT JOIN tblorigowners ON tbltitles.orig_owner_id = tblorigowners.orig_owner_id 
		LEFT JOIN tbllandclass ON tbltitles.tct_classification = tbllandclass.lcls_id 
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name
		";
		//echo $strSQL;

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	
	public function countTitles($intId='', $bnWithTitle='', $bnIfConsolidated='', $bnForValidation='', $txtTCTNo='', $bnWithLeaseContract='', $intRgnId='', $intPrvId='', $txtType=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbltitles.tct_id='".$intId."'";
		if($bnWithTitle=='N')
			$whereId .= " AND tbltitles.tct_with_title='".$bnWithTitle."'";
		if($bnIfConsolidated=='FC' || $bnIfConsolidated=='Y')
			$whereId .= " AND tbltitles.tct_if_consolidated='".$bnIfConsolidated."'";	
		if($bnForValidation=='Y')
			$whereId .= " AND tbltitles.tct_for_validation='".$bnForValidation."'";		
		if($txtTCTNo!='')
			$whereId .= " AND (tbltitles.tct_orig_no='".$txtTCTNo."' OR tbltitles.tct_trc_no='".$txtTCTNo."')";
		if($bnWithLeaseContract=='Y')
			$whereId .= " AND tbltitles.tct_with_lease_cont='".$bnWithLeaseContract."'";		
		if($intRgnId!='')
			$whereId .= " AND tblregions.rgn_id='".$intRgnId."'";
		if($intPrvId!='')
			$whereId .= " AND tbltitles.prv_id='".$intPrvId."'";
		if($txtType!='')
			$whereId .= " AND tbltitles.tct_type='".$txtType."'";	
				
		$strSQL = "SELECT DISTINCT (tblaccountnames.act_id) FROM tbltitles 
		LEFT JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id 
		LEFT JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id 
		LEFT JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		LEFT JOIN tblorigowners ON tbltitles.orig_owner_id = tblorigowners.orig_owner_id 
		LEFT JOIN tbllandclass ON tbltitles.tct_classification = tbllandclass.lcls_id 
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name
		";
		//echo $strSQL;

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}

	public function getAccountNames($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblaccountnames.act_id='".$intId."'";

		$strSQL = "SELECT * FROM tblaccountnames
		WHERE 1=1
		$whereId
		ORDER BY act_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}

	public function getRegisteredOwners($intId=''){ //for getting new or original owner details
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblorigowners.orig_owner_id='".$intId."'";

		$strSQL = "SELECT * FROM tblorigowners
		WHERE 1=1
		$whereId
		ORDER BY orig_owner_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}

	public function getNewRegisteredOwnerName($intOrigOwnerId='', $intNewOwnerId='')
	{
		if($intNewOwnerId=='' || $intNewOwnerId==0)
		{
			$rsOwnerName = $this->title_model->getRegisteredOwners($intOrigOwnerId);
			
			$strOwnerName = $rsOwnerName[0]['orig_owner_name'].' - '.$rsOwnerName[0]['orig_owner_details'];
		}
		else 
		{
			$rsOwnerName = $this->title_model->getRegisteredOwners($intNewOwnerId);
			$strOwnerName = $rsOwnerName[0]['orig_owner_name'];
		}
		
		return $strOwnerName;
		
	}
	public function getRegions($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblregions.rgn_id='".$intId."'";

		$strSQL = "SELECT * FROM tblregions
		WHERE 1=1
		$whereId
		ORDER BY rgn_display_order";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}

	public function getProvinces($intId='', $intRgnId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblprovinces.prv_id='".$intId."'";
		if($intRgnId!='')
			$whereId .= " AND tblprovinces.prv_rgn_id='".$intRgnId."'";

		$strSQL = "SELECT tblprovinces.*, tblregions.* FROM tblprovinces
		INNER JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id
		WHERE 1=1
		$whereId
		ORDER BY prv_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function getLandClass($intId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbllandclass.lcls_id='".$intId."'";

		$strSQL = "SELECT * FROM tbllandclass
		WHERE 1=1
		$whereId
		ORDER BY lcls_name";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	// for legal concerns
	public function getLegalConcerns($intId='', $intLgcId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbllegalconcerns.tct_id='".$intId."'";
		if($intLgcId!='')
			$whereId .= " AND tbllegalconcerns.lgc_id='".$intLgcId."'";

		$strSQL = "SELECT * FROM tbllegalconcerns
		WHERE 1=1
		$whereId
		ORDER BY lgc_date DESC";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function getTitleLegalConcerns($intId='', $intRgnId='', $intPrvId='', $txtType='', $intAcctId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbltitles.tct_id='".$intId."'";
		if($intRgnId!='')
			$whereId .= " AND tblregions.rgn_id='".$intRgnId."'";
		if($intPrvId!='')
			$whereId .= " AND tbltitles.prv_id='".$intPrvId."'";
		if($txtType!='')
			$whereId .= " AND tbltitles.tct_type='".$txtType."'";	
		if($intAcctId!='')
			$whereId .= " AND tblaccountnames.act_id='".$intAcctId."'";	
				
		$strSQL = "SELECT tbltitles.*, tblaccountnames.act_name, tbllegalconcerns.*, tblprovinces.prv_name, tblorigowners.*, tbllandclass.* FROM tbltitles 
		INNER JOIN tbllegalconcerns ON tbltitles.tct_id = tbllegalconcerns.tct_id
		LEFT JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id
		LEFT JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id
		LEFT JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		LEFT JOIN tblorigowners ON tbltitles.orig_owner_id = tblorigowners.orig_owner_id 
		LEFT JOIN tbllandclass ON tbltitles.tct_classification = tbllandclass.lcls_id
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name, tbltitles.tct_id, tbllegalconcerns.lgc_date DESC ";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function countTitleLegalConcerns($bnIsTitle='',$bnIsAccount='', $intRgnId='', $intPrvId='', $txtType=''){
		$whereId='';
		if($intRgnId!='')
			$whereId .= " AND tblregions.rgn_id='".$intRgnId."'";
		if($intPrvId!='')
			$whereId .= " AND tbltitles.prv_id='".$intPrvId."'";
		if($txtType!='')
			$whereId .= " AND tbltitles.tct_type='".$txtType."'";	
			
		$strDistinct='';
		if($bnIsTitle!='')
			$strDistinct = "DISTINCT tbltitles.tct_id";
		elseif($bnIsAccount!='')
			$strDistinct = "DISTINCT tbltitles.act_id";
		
		$strSQL = "SELECT $strDistinct FROM tbltitles 
		INNER JOIN tbllegalconcerns ON tbltitles.tct_id = tbllegalconcerns.tct_id
		LEFT JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id
		LEFT JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id
		LEFT JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		LEFT JOIN tblorigowners ON tbltitles.orig_owner_id = tblorigowners.orig_owner_id 
		LEFT JOIN tbllandclass ON tbltitles.tct_classification = tbllandclass.lcls_id
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name, tbltitles.tct_id, tbllegalconcerns.lgc_date DESC ";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function addLegal($arrData){
		$this->db->insert('tbllegalconcerns', $arrData);
		return $this->db->insert_id();
	}
	public function updateLegal($arrData, $intId)
	{
		$this->db->where('lgc_id',$intId);
		$this->db->update('tbllegalconcerns', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteLegal($intId)
	{
		$this->db->where('lgc_id',$intId);
		$this->db->delete('tbllegalconcerns');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	// end of legal concerns
	
	// for site inspection
	public function addSiteInspection($arrData){
		$this->db->insert('tblsiteinspections', $arrData);
		return $this->db->insert_id();
	}
	public function getSiteInspections($intId='', $intSteId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tblsiteinspections.tct_id='".$intId."'";
		if($intSteId!='')
			$whereId .= " AND tblsiteinspections.ste_id='".$intSteId."'";

		$strSQL = "SELECT * FROM tblsiteinspections
		WHERE 1=1
		$whereId
		ORDER BY ste_date DESC";
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function getTitleListWithSiteInspections($intId='', $intRgnId='', $intPrvId='', $txtType='', $intAcctId=''){
		$whereId='';
		if($intId!='')
			$whereId .= " AND tbltitles.tct_id='".$intId."'";
		if($intRgnId!='')
			$whereId .= " AND tblregions.rgn_id='".$intRgnId."'";
		if($intPrvId!='')
			$whereId .= " AND tbltitles.prv_id='".$intPrvId."'";
		if($txtType!='')
			$whereId .= " AND tbltitles.tct_type='".$txtType."'";
		if($intAcctId!='')
			$whereId .= " AND tblaccountnames.act_id='".$intAcctId."'";	
		
		$strSQL = "SELECT tbltitles.*, tblaccountnames.act_name, tblsiteinspections.*, tblprovinces.prv_name FROM tbltitles 
		INNER JOIN tblsiteinspections ON tbltitles.tct_id = tblsiteinspections.tct_id
		LEFT JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id
		LEFT JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id
		LEFT JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name, tbltitles.tct_id, tblsiteinspections.ste_date DESC ";
		
		/*
		$strSQL = "SELECT tbltitles.*, tblaccountnames.act_name, tblsiteinspections.*, tblprovinces.prv_name FROM tbltitles 
		INNER JOIN tblsiteinspections ON tbltitles.tct_id = tblsiteinspections.tct_id
		INNER JOIN tblaccountnames ON tbltitles.act_id = tblaccountnames.act_id
		INNER JOIN tblprovinces ON tbltitles.prv_id = tblprovinces.prv_id
		INNER JOIN tblregions ON tblprovinces.prv_rgn_id = tblregions.rgn_id 
		WHERE 1=1
		$whereId
		ORDER BY tblprovinces.prv_name, tbltitles.tct_id, tblsiteinspections.ste_date DESC ";
		*/
		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	public function updateSiteInspection($arrData, $intId)
	{
		$this->db->where('ste_id',$intId);
		$this->db->update('tblsiteinspections', $arrData);
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}
	public function deleteSiteInspection($intId)
	{
		$this->db->where('ste_id',$intId);
		$this->db->delete('tblsiteinspections');
		return $this->db->affected_rows()>0?TRUE:FALSE;
	}

}
?>