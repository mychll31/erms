<?php
class Title_profile_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		//$this->fpdf = new FPDF('P','','Legal');
		//$this->load->library('Numbertowords');
		$this->load->model(array('title_model'));
	}
	
	public function Header()
	{
	}
	
	public function footer()
	{		
		$this->fpdf->SetY(-5);   // print date and time
		$this->fpdf->SetFont('Arial','',7);	
		$this->fpdf->Cell(50,3,date('Y-m-d h:i A'),0,0,'L');
		$this->fpdf->Cell(0,3,"Page ".$this->fpdf->PageNo(),0,0,'R');					
	}
	
	function generate($arrData)
	{

		$this->fpdf->SetLeftMargin(10);
		$this->fpdf->SetRightMargin(10);
		$this->fpdf->SetTopMargin(10);
		$this->fpdf->SetAutoPageBreak("on",10);
		$this->fpdf->AddPage();

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->SetTitle($arrData['reportName']); // show report title variable later
		$this->fpdf->Cell(0,5,strtoupper($arrData['reportName']),0,1,'C');
		$this->fpdf->Cell(0,5,' AS OF '.strtoupper(date('F j, Y')),0,1,'C');
		$this->fpdf->Ln(3);
		
		$rsTitleProfile = $this->title_model->getTitles('','','','',$arrData['tctnumber']);
		$colH = 22;
		$colD = 26;
	
		$this->fpdf->SetFont('Arial','B',10);
		$widths=array($colH,$colD,$colH,$colD,$colH,$colD,$colH,$colD);
		//$border=array(0,0,0,0,0,0,0,0,0);
		$border=array('B','B','B','B','B','B','B','B');
		$align=array('L','L','L','L','L','L','L','L');
		//$style=array('B','','B','','B','','B','');
		$style=array('','','','','','','','');
		
		foreach($rsTitleProfile as $row):
		
			//get new registered owner name
			$strNewOwnerName = $this->title_model->getNewRegisteredOwnerName($row['orig_owner_id'], $row['tct_new_owner_id']);
			$text=array('TCT No.:',$row['tct_orig_no'],'Account Name:',$row['act_name'],'Original Owner:',$row['orig_owner_name'],'New Registered Owner:',$strNewOwnerName);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Type:',$row['tct_type'],'Transfered to TRC?:',$row['tct_if_transfered'],'TRC TCT No.:',$row['tct_trc_no'],'Consolidated?:',$row['tct_if_consolidated']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Region:',$row['rgn_name'],'Province:',$row['prv_name'],'Location/ Particulars:',$row['tct_location'],'Land Area (sqm):',$row['tct_land_area']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Classification:',$row['lcls_name'],'Description:',$row['tct_description'],'With Title?:',$row['tct_with_title'],'Date of Mortgage:',($row['tct_date_mortgage']!='0000-00-00'? date('d-M-y',strtotime($row['tct_date_mortgage'])) : ''));
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Acquisition Cost:',number_format($row['tct_aquisition_cost'],2),'Means of Acquisition:',$row['tct_means_aquisition'],'Loan Amount:',number_format($row['tct_loan_amount'],2),'Fund:',$row['tct_fund']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Sold to TRC?:',$row['tct_sold_to_trc'],'Selling Price:',number_format($row['tct_selling_price'],2),'Amount RPT:',number_format($row['tct_amount_rpt'],2),'Year RPT:',$row['tct_year_rpt']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Capital Gain:',number_format($row['tct_capital_gain'],2),'Contacts:',$row['tct_contacts'],'With Ocular Inspection?:',$row['tct_with_ocular'],'For Valdation?:',$row['tct_for_validation']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Date of Appraisal:',($row['tct_appraisal_date']!='0000-00-00'? date('d-M-y',strtotime($row['tct_appraisal_date'])) : ''),'Appraisal Value:',$row['tct_appraisal_value'],'Appraisal Remarks:',$row['tct_appraisal_remarks'],'','');
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('WIth Lease Contract?:',$row['tct_with_lease_cont'],'','','','','','');
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$text=array('Lessee:',$row['tct_lessee'],'Monthly Lease:',number_format($row['tct_lease_monthly'],2),'Duration of Lease:',$row['tct_lease_duration'],'Status:',$row['tct_lease_status']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
			$widths=array($colH, 60);
			$border=array(0,'B');
			$align=array('L','L');
			$style=array('','');
			$text=array('Remarks:',$row['tct_remarks']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			$this->fpdf->Ln(2);
			
		endforeach;
		
		// FOR LEGAL CONCERNS
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(40,5,'LEGAL CONCERNS','B',1,'L');
		$this->fpdf->Ln(5);
		$rsTitleLegal = $this->title_model->getLegalConcerns($row['tct_id']);
		
		$this->fpdf->SetFont('Arial','B',10);
		$widths=array(30, 60);
		$border=array('B','B');
		$align=array('C','C');
		$style=array('B','B');
		$text=array('DATE','REMARKS');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($text,$border,$align,$style);
		
		$align=array('L','L');
		$style=array('','');
		foreach($rsTitleLegal as $rowLegal):
			$text=array(($rowLegal['lgc_date']!='0000-00-00'? date('d-M-y',strtotime($rowLegal['lgc_date'])) : ''),$rowLegal['lgc_remarks']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
		
		endforeach;
		
		// FOR SITE INSPECTIONS
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(40,5,'SITE INSPECTIONS','B',1,'L');
		$this->fpdf->Ln(5);
		$rsTitleSite = $this->title_model->getSiteInspections($row['tct_id']);
		
		$this->fpdf->SetFont('Arial','B',10);
		$widths=array(30,20,60,60);
		$border=array('B','B','B','B');
		$align=array('C','C','C','C');
		$style=array('B','B','B','B');
		$text=array('DATE','GUARD PRESENT','REMARKS','RECOMMENDATIONS');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($text,$border,$align,$style);

		$align=array('L','C','L','L');
		$style=array('','','','');
		foreach($rsTitleSite as $rowLSite):
			$text=array(($rowLSite['ste_date']!='0000-00-00'? date('d-M-y',strtotime($rowLSite['ste_date'])) : ''),$rowLSite['ste_guard_present'],$rowLSite['ste_remarks'],$rowLSite['ste_recommendations']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
		
		endforeach;
		
	
	}
	
}