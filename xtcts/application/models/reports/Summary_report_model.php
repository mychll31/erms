<?php
class Summary_report_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		//$this->fpdf = new FPDF('P','','Legal');
		//$this->load->library('Numbertowords');
		$this->load->model(array('title_model'));
	}
	
	public function Header()
	{
	}
	
	public function footer()
	{		
		$this->fpdf->SetY(-5);   // print date and time
		$this->fpdf->SetFont('Arial','',7);	
		$this->fpdf->Cell(50,3,date('Y-m-d h:i A'),0,0,'L');
		$this->fpdf->Cell(0,3,"Page ".$this->fpdf->PageNo(),0,0,'R');					
	}
	
	function generate($arrData)
	{

		$this->fpdf->SetLeftMargin(15);
		$this->fpdf->SetRightMargin(15);
		$this->fpdf->SetTopMargin(10);
		$this->fpdf->SetAutoPageBreak("on",10);
		$this->fpdf->AddPage();

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->SetTitle($arrData['reportName']); // show report title variable later
		$this->fpdf->Cell(0,5,strtoupper($arrData['reportName']),0,1,'C');
		$this->fpdf->Cell(0,5,' AS OF '.strtoupper(date('F j, Y')),0,1,'C');
		$this->fpdf->Ln(10);

		//$this->fpdf->SetFont('Arial','',10);
		$txtType = $arrData['typename'];
		$intRgnId = $arrData['region'];
		$intPvnId = $arrData['province'];
		
		// NO. OF TITLES
		$rsTitleProfile = $this->title_model->getTitles('','','','','','',$intRgnId,$intPvnId,$txtType);
		$rsCountTitleProfile = $this->title_model->countTitles('','','','','','',$intRgnId,$intPvnId,$txtType);

		// NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES)
		$rsTitleNo = $this->title_model->getTitles('',$bnWithTitle='N','','','','',$intRgnId,$intPvnId,$txtType);
		$rsCountTitleNo = $this->title_model->countTitles('',$bnWithTitle='N','','','','',$intRgnId,$intPvnId,$txtType);	

		// FOR CONSOLIDATION
		$rsForConso = $this->title_model->getTitles('','',$bnIfConsolidated='FC','','','',$intRgnId,$intPvnId,$txtType);
		$rsCountForConso = $this->title_model->countTitles('','',$bnIfConsolidated='FC','','','',$intRgnId,$intPvnId,$txtType);

		// CONSOLIDATED TITLES
		$rsConsolidated = $this->title_model->getTitles('','',$bnIfConsolidated='Y','','','',$intRgnId,$intPvnId,$txtType);
		$rsCountConsolidated = $this->title_model->countTitles('','',$bnIfConsolidated='Y','','','',$intRgnId,$intPvnId,$txtType);

		// FOR VALIDATION
		$rsForValidation = $this->title_model->getTitles('','','', $bnForValidation='Y','','',$intRgnId,$intPvnId,$txtType);
		$rsCountForValidation = $this->title_model->countTitles('','','', $bnForValidation='Y','','',$intRgnId,$intPvnId,$txtType);

		// WITH LEGAL CONCERNS
		$rsTitleLegal = $this->title_model->countTitleLegalConcerns(1,'',$intRgnId,$intPvnId,$txtType);
		$rsCountTitleLegal = $this->title_model->countTitleLegalConcerns('',1,$intRgnId,$intPvnId,$txtType);
		
		if($txtType!='')
		{
			$this->fpdf->Cell(0,5,strtoupper($txtType),0,1,'C');
		}
		if($intPvnId!='')
		{
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
	/*	
		if($intPvnId!='')
		{
			//$rs = $this->title_model->getTitles('',$bnWithTitle='N','','','','','',$intPvnId);
			// NO. OF TITLES
			$rsTitleProfile = $this->title_model->getTitles('','','','','','','',$intPvnId);
			$rsCountTitleProfile = $this->title_model->countTitles('','','','','','','',$intPvnId);

			// NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES)
			$rsTitleNo = $this->title_model->getTitles('',$bnWithTitle='N','','','','','',$intPvnId);
			$rsCountTitleNo = $this->title_model->countTitles('',$bnWithTitle='N','','','','','',$intPvnId);	

			// FOR CONSOLIDATION
			$rsForConso = $this->title_model->getTitles('','',$bnIfConsolidated='FC','','','','',$intPvnId);
			$rsCountForConso = $this->title_model->countTitles('','',$bnIfConsolidated='FC','','','','',$intPvnId);

			// CONSOLIDATED TITLES
			$rsConsolidated = $this->title_model->getTitles('','',$bnIfConsolidated='Y','','','','',$intPvnId);
			$rsCountConsolidated = $this->title_model->countTitles('','',$bnIfConsolidated='Y','','','','',$intPvnId);

			// FOR VALIDATION
			$rsForValidation = $this->title_model->getTitles('','','', $bnForValidation='Y','','','',$intPvnId);
			$rsCountForValidation = $this->title_model->countTitles('','','', $bnForValidation='Y','','','',$intPvnId);

			// WITH LEGAL CONCERNS
			$rsTitleLegal = $this->title_model->countTitleLegalConcerns(1,'','',$intPvnId);
			$rsCountTitleLegal = $this->title_model->countTitleLegalConcerns('',1,'',$intPvnId);
					
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			//$rs = $this->title_model->getTitles('',$bnWithTitle='N','','','','',$intRgnId,'');
			// NO. OF TITLES
			$rsTitleProfile = $this->title_model->getTitles('','','','','','',$intRgnId,'');
			$rsCountTitleProfile = $this->title_model->countTitles('','','','','','',$intRgnId,'');
			
			// NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES)
			$rsTitleNo = $this->title_model->getTitles('',$bnWithTitle='N','','','','',$intRgnId,'');
			$rsCountTitleNo = $this->title_model->countTitles('',$bnWithTitle='N','','','','',$intRgnId,'');	

			// FOR CONSOLIDATION
			$rsForConso = $this->title_model->getTitles('','',$bnIfConsolidated='FC','','','',$intRgnId,'');
			$rsCountForConso = $this->title_model->countTitles('','',$bnIfConsolidated='FC','','','',$intRgnId,'');

			// CONSOLIDATED TITLES
			$rsConsolidated = $this->title_model->getTitles('','',$bnIfConsolidated='Y','','','',$intRgnId,'');
			$rsCountConsolidated = $this->title_model->countTitles('','',$bnIfConsolidated='Y','','','',$intRgnId,'');

			// FOR VALIDATION
			$rsForValidation = $this->title_model->getTitles('','','', $bnForValidation='Y','','',$intRgnId,'');
			$rsCountForValidation = $this->title_model->countTitles('','','', $bnForValidation='Y','','',$intRgnId,'');

			// WITH LEGAL CONCERNS
			$rsTitleLegal = $this->title_model->countTitleLegalConcerns(1,'',$intRgnId,'');
			$rsCountTitleLegal = $this->title_model->countTitleLegalConcerns('',1,$intRgnId,'');
			
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
		else
		{
			//$rs = $this->title_model->getTitles('',$bnWithTitle='N');
			// NO. OF TITLES
			$rsTitleProfile = $this->title_model->getTitles();
			$rsCountTitleProfile = $this->title_model->countTitles();	

			// NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES)
			$rsTitleNo = $this->title_model->getTitles('',$bnWithTitle='N');
			$rsCountTitleNo = $this->title_model->countTitles('',$bnWithTitle='N');	

			// FOR CONSOLIDATION
			$rsForConso = $this->title_model->getTitles('','',$bnIfConsolidated='FC');
			$rsCountForConso = $this->title_model->countTitles('','',$bnIfConsolidated='FC');

			// CONSOLIDATED TITLES
			$rsConsolidated = $this->title_model->getTitles('','',$bnIfConsolidated='Y');
			$rsCountConsolidated = $this->title_model->countTitles('','',$bnIfConsolidated='Y');

			// FOR VALIDATION
			$rsForValidation = $this->title_model->getTitles('','','', $bnForValidation='Y');
			$rsCountForValidation = $this->title_model->countTitles('','','', $bnForValidation='Y');

			// WITH LEGAL CONCERNS
			$rsTitleLegal = $this->title_model->countTitleLegalConcerns(1,'');
			$rsCountTitleLegal = $this->title_model->countTitleLegalConcerns('',1);

		}
		*/
		
		$colW1 = 105;
		$colW2 = 30;
		$colH = 7;
		// HEADER
		$this->fpdf->Cell($colW1,$colH,'','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,'TITLES','B',0,'C');
		$this->fpdf->Cell($colW2,$colH,'ACCOUNTS','B',1,'C');
		
		// NO. OF TITLES
		//$rsTitleProfile = $this->title_model->getTitles();
		//$rsCountTitleProfile = $this->title_model->countTitles();
		$this->fpdf->Cell($colW1,$colH,'NO. OF TITLES:','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsTitleProfile),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountTitleProfile),'B',1,'C');
		
		// NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES)
		//$rsTitleNo = $this->title_model->getTitles('',$bnWithTitle='N');
		//$rsCountTitleNo = $this->title_model->countTitles('',$bnWithTitle='N');
		$this->fpdf->Cell($colW1,$colH,'NO. OF TITLES (WITH PHOTOCOPIES OR NO TITLES):','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsTitleNo),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountTitleNo),'B',1,'C');
		
		// FOR CONSOLIDATION
		//$rsForConso = $this->title_model->getTitles('','',$bnIfConsolidated='FC');
		//$rsCountForConso = $this->title_model->countTitles('','',$bnIfConsolidated='FC');
		$this->fpdf->Cell($colW1,$colH,'FOR CONSOLIDATION:','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsForConso),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountForConso),'B',1,'C');
		
		// CONSOLIDATED TITLES
		//$rsConsolidated = $this->title_model->getTitles('','',$bnIfConsolidated='Y');
		//$rsCountConsolidated = $this->title_model->countTitles('','',$bnIfConsolidated='Y');
		$this->fpdf->Cell($colW1,$colH,'CONSOLIDATED TITLES:','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsConsolidated),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountConsolidated),'B',1,'C');

		// FOR VALIDATION
		//$rsForValidation = $this->title_model->getTitles('','','', $bnForValidation='Y');
		//$rsCountForValidation = $this->title_model->countTitles('','','', $bnForValidation='Y');
		$this->fpdf->Cell($colW1,$colH,'FOR VALIDATION:','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsForValidation),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountForValidation),'B',1,'C');

		// WITH LEGAL CONCERNS
		//$rsTitleLegal = $this->title_model->countTitleLegalConcerns(1,'');
		//$rsCountTitleLegal = $this->title_model->countTitleLegalConcerns('',1);
		$this->fpdf->Cell($colW1,$colH,'WITH LEGAL CONCERNS:','B',0,'L');
		$this->fpdf->Cell($colW2,$colH,count($rsTitleLegal),'B',0,'C');
		$this->fpdf->Cell($colW2,$colH,count($rsCountTitleLegal),'B',1,'C');
	
	}
	
}