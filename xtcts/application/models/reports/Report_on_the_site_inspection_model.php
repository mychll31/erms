<?php
class Report_on_the_site_inspection_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		//$this->fpdf = new FPDF('P','','Legal');
		//$this->load->library('Numbertowords');
		$this->load->model(array('title_model'));
	}
	
	public function Header()
	{
	}
	
	public function footer()
	{		
		$this->fpdf->SetY(-5);   // print date and time
		$this->fpdf->SetFont('Arial','',7);	
		$this->fpdf->Cell(50,3,date('Y-m-d h:i A'),0,0,'L');
		$this->fpdf->Cell(0,3,"Page ".$this->fpdf->PageNo(),0,0,'R');					
	}
	
	function generate($arrData)
	{

		$this->fpdf->SetLeftMargin(5);
		$this->fpdf->SetRightMargin(5);
		$this->fpdf->SetTopMargin(10);
		$this->fpdf->SetAutoPageBreak("on",10);
		$this->fpdf->AddPage();

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->SetTitle($arrData['reportName']); // show report title variable later
		$this->fpdf->Cell(0,5,strtoupper($arrData['reportName']),0,1,'C');
		$this->fpdf->Cell(0,5,'AS OF '.strtoupper(date('F j, Y')),0,1,'C');
		$this->fpdf->Ln(3);
		
		$this->fpdf->SetFont('Arial','',10);
		$txtType = $arrData['typename'];
		$intAcctId = $arrData['actnumber'];
		$intRgnId = $arrData['region'];
		$intPvnId = $arrData['province'];
		
		$rs = $this->title_model->getTitleListWithSiteInspections('',$intRgnId,$intPvnId,$txtType, $intAcctId);
		
		if($txtType!='')
		{
			$this->fpdf->Cell(0,5,strtoupper($txtType),0,1,'C');
		}
		if($intAcctId!='')
		{
			$rsAccount = $this->title_model->getAccountNames($intAcctId);
			$strAccountName = $rsAccount[0]['act_name'];	
			$this->fpdf->Cell(0,5,strtoupper($strAccountName),0,1,'L');
		}
		if($intPvnId!='')
		{
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
		
		/*
		if($intPvnId!='')
		{
			$rs = $this->title_model->getTitleListWithSiteInspections('','',$intPvnId);
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			$rs = $this->title_model->getTitleListWithSiteInspections('',$intRgnId,'');
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
		else
			$rs = $this->title_model->getTitleListWithSiteInspections();
			//$rs = $this->title_model->getTitles('',$bnWithTitle='N');
		*/	

		$this->fpdf->SetFont('Arial','',10);
		$widths=array(13,30,40,22,35,18,26,26,30,24,10,36,36);
		$border=array(1,1,1,1,1,1,1,1,1,1,1,1,1);
		$align=array('C','C','C','C','C','C','C','C','C','C','C','C','C');
		$style=array('B','B','B','B','B','B','B','B','B','B','B','B','B');
		$text=array('','Province','Account Name','TCT No.','Location/ Particulars','Land Area (sqm)','Amount RPT','Capital Gain Tax','Contact Person/s','Date','W/ Guard','Status/Remarks','Recommendations');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($text,$border,$align,$style);
		
		// DATA
		$this->fpdf->SetFont('Arial','',10);
		$align=array('C','L','L','C','L','R','R','R','L','L','C','L','L');
		$style=array('','','','','','','','','','','','','');
		
	//	$rs = $this->title_model->getTitleListWithSiteInspections();
		//$row = $this->title_model->getTitleListWithSiteInspections();
		//get titles with site inspection report
	//	print_r($row);
		$intCtr=0;
		$intTCTID = '';
		foreach($rs as $row):
		//for($intRow=0; $intRow<=count($row); $intRow++)
		//{
			$tempTCTId = $intTCTID;
			
			if(strlen($row['tct_trc_no'])!=0)
				$strTCTNo = $row['tct_trc_no'];
			else $strTCTNo = $row['tct_orig_no'];
			
			$dtSiteInspection = ($row['ste_date']!='0000-00-00'? date('j-M-Y',strtotime($row['ste_date'])) : '');
			
			if($tempTCTId!=$row['tct_id'])
				$text=array(++$intCtr, $row['prv_name'], $row['act_name'], $strTCTNo, $row['tct_location'], number_format($row['tct_land_area']), number_format($row['tct_amount_rpt'],2), number_format($row['tct_capital_gain'],2),$row['tct_contacts'], $dtSiteInspection, $row['ste_guard_present'],  $row['ste_remarks'], $row['ste_recommendations']);
			else
				$text=array('', '', '', '', '', '', '', '','', $dtSiteInspection, $row['ste_guard_present'],  $row['ste_remarks'], $row['ste_recommendations']);			
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
			
			$intTCTID = $row['tct_id'];
			/*
			
			if(isset($row[$intRow]['tct_trc_no']))
				$strTCTNo = $row[$intRow]['tct_trc_no'];
			else $strTCTNo = $row[$intRow]['tct_orig_no'];
			
			$dtSiteInspection = ($row[$intRow]['ste_date']!='0000-00-00'? date('j-M-Y',strtotime($row[$intRow]['ste_date'])) : '');
			

			$text=array(++$intCtr, $row[$intRow]['prv_name'], $row[$intRow]['act_name'], $strTCTNo, $row[$intRow]['tct_location'], number_format($row[$intRow]['tct_land_area']), number_format($row[$intRow]['tct_amount_rpt'],2), number_format($row[$intRow]['tct_capital_gain'],2),$row[$intRow]['tct_contacts'], $dtSiteInspection, $row[$intRow]['ste_guard_present'],  $row[$intRow]['ste_remarks'], $row[$intRow]['ste_recommendations']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
		} */
		endforeach; 

	}

	
}