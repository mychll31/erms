<?php
class List_of_lease_contracts_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		//$this->fpdf = new FPDF('P','','Legal');
		//$this->load->library('Numbertowords');
		$this->load->model(array('title_model'));
	}
	
	public function Header()
	{
	}
	
	public function footer()
	{		
		$this->fpdf->SetY(-5);   // print date and time
		$this->fpdf->SetFont('Arial','',7);	
		$this->fpdf->Cell(50,3,date('Y-m-d h:i A'),0,0,'L');
		$this->fpdf->Cell(0,3,"Page ".$this->fpdf->PageNo(),0,0,'R');					
	}
	
	function generate($arrData)
	{

		$this->fpdf->SetLeftMargin(10);
		$this->fpdf->SetRightMargin(10);
		$this->fpdf->SetTopMargin(10);
		$this->fpdf->SetAutoPageBreak("on",10);
		$this->fpdf->AddPage();

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->SetTitle($arrData['reportName']); // show report title variable later
		$this->fpdf->Cell(0,5,strtoupper($arrData['reportName']),0,1,'C');
		$this->fpdf->Cell(0,5,'AS OF '.strtoupper(date('F j, Y')),0,1,'C');
		$this->fpdf->Ln(3);

		$this->fpdf->SetFont('Arial','',10);
		$txtType = $arrData['typename'];
		$intAcctId = $arrData['actnumber'];
		$intRgnId = $arrData['region'];
		$intPvnId = $arrData['province'];
		
		$rs = $this->title_model->getTitles('','','', '','',$bnWithLeaseConctract='Y',$intRgnId,$intPvnId,$txtType,$intAcctId);
		
		if($txtType!='')
		{
			$this->fpdf->Cell(0,5,strtoupper($txtType),0,1,'C');
		}
		if($intAcctId!='')
		{
			$rsAccount = $this->title_model->getAccountNames($intAcctId);
			$strAccountName = $rsAccount[0]['act_name'];	
			$this->fpdf->Cell(0,5,strtoupper($strAccountName),0,1,'L');
		}
		if($intPvnId!='')
		{
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
		
		/*
		if($intPvnId!='')
		{
			$rs = $this->title_model->getTitles('','','', '','',$bnWithLeaseConctract='Y','',$intPvnId);
			$rsProvince = $this->title_model->getProvinces($intPvnId);
			$strProvinceName = $rsProvince[0]['rgn_name'].' - '.$rsProvince[0]['prv_name'];
			$this->fpdf->Cell(0,5,strtoupper($strProvinceName),0,1,'L');
		}
		elseif($intRgnId!='' AND $intPvnId=='')
		{
			$rs = $this->title_model->getTitles('','','', '','',$bnWithLeaseConctract='Y',$intRgnId,'');
			$rsRegionName = $this->title_model->getRegions($intRgnId);
			$strRegionName = $rsRegionName[0]['rgn_name'];
			$this->fpdf->Cell(0,5,strtoupper($strRegionName),0,1,'L');
		
		}
		else
			$rs = $this->title_model->getTitles('','','', '','',$bnWithLeaseConctract='Y');
		*/
		$this->fpdf->SetFont('Arial','',10);
		$widths=array(13,22,40,30,40,30,26,40,40);
		$border=array(1,1,1,1,1,1,1,1,1);
		$align=array('C','C','C','C','C','C','C','C','C');
		$style=array('B','B','B','B','B','B','B','B','B');
		$text=array('','TCT No.', 'Account Name', 'Lessee', 'Location', 'Description', 'Montly Lease', 'Duration of Lease','Status of Payments/Remarks');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($text,$border,$align,$style);
		
		// DATA
		$this->fpdf->SetFont('Arial','',10);
		$align=array('C','C','L','L','L','L','R','L','L');
		$style=array('','','','','','','','','');
		
		//$rs = $this->title_model->getTitles('','','', '','',$bnWithLeaseConctract='Y');
		
		$intCtr=0;
		
		foreach($rs as $row):
		
			if(strlen($row['tct_trc_no'])!=0)
				$strTCTNo = $row['tct_trc_no'];
			else $strTCTNo = $row['tct_orig_no'];
			
			//$dtMortgage = date_format($row['tct_date_mortgage'],'j-M-y');	
			//$dtMortgage = $row['tct_date_mortgage'];	
			
			$text=array(++$intCtr, $strTCTNo, $row['act_name'], $row['tct_lessee'], $row['tct_location'], $row['lcls_name'].' - '.$row['tct_description'], number_format($row['tct_lease_monthly'],2), $row['tct_lease_duration'], $row['tct_lease_status']);
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($text,$border,$align,$style);
		
		endforeach; 

	}

	
}
