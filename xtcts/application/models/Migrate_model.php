<?php 
class Migrate_model extends CI_Model {

	public function __construct(){
		parent:: __construct();
	}
	
	public function getTitleData($intId=''){
		
		$strSQL = "SELECT * FROM tblmigration";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
	}
	

	public function addTitleData($arrData){
		$this->db->insert('temptbltitles', $arrData);
		return $this->db->insert_id();
	}

	
}
?>