<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migrate extends CI_Controller {

	var $arrTemplateData;

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('migrate_model');
	}

	public function migratedata()
	{
		$ctr = 0;
		$rsTitleData =$this->migrate_model->getTitleData();
		foreach ($rsTitleData as $row) {
			$ctr++;
			
			$arrData = array(
				  'tct_id' => $row['ID'],
				  'act_id' => $row['accountid'],
				  'tct_new_owner_id' => $row['newregisteredownerid'],
				  'orig_owner_id' => $row['registeredownerid'],
				  'tct_orig_no' => $row['tctno'],
				  'tct_if_transfered' => $row['transferredtotrc'],
				  'tct_trc_no' => $row['trctctno'],
				  'tct_location' => $row['locationparticulars'],
				  'rgn_id' => '',
				  'prv_id' => $row['provinceid'],
				  'tct_land_area' => $row['landarea'],
				  'tct_classification' => $row['classificationid'],
				  'tct_description' => $row['landdescription'],
				  'tct_aquisition_cost' => $row['acquisitioncost'],
				  'tct_with_title' => $row['withtitle'],
				  'tct_date_mortgage' => $row['dateofmortgage'],
				  'tct_loan_amount' => $row['loanamount'],
				  'tct_fund' => $row['fund'],
				  'tct_sold_to_trc' => $row['soldtotrc'],
				  'tct_selling_price' => $row['sellingprice'],
				  'tct_amount_rpt' => $row['amountrpt'],
				  'tct_year_rpt' => $row['yearrpt'],
				  'tct_capital_gain' => $row['capitalgain'],
				  'tct_contacts' => $row['contacts'],
				  'tct_with_ocular' => $row['withocularinspection'],
				  'tct_with_lease_cont' => $row['withleasecontract'],
				  'tct_lessee' => $row['lessee'],
				  'tct_lease_monthly' => $row['monthlylease'],
				  'tct_lease_duration' => $row['durationoflease'],
				  'tct_lease_status' => $row['statusofpayments'],
				  'tct_means_aquisition' => $row['meansofacquisition'],
				  'tct_for_validation' => $row['forvalidation'],
				  'tct_if_consolidated' => $row['consolidated'],
				  'tct_appraisal_date' => $row['appraisaldate'],
				  'tct_appraisal_value' => $row['appraisalvalue'],
				  'tct_appraisal_remarks' => $row['appraisalremarks'],
				  'tct_remarks' => $row['remarks']
				);
		
			$id = $this->migrate_model->addTitleData($arrData);
			
		}
		
		
		echo $ctr.' records inserted';
		
	}

	

}

?>