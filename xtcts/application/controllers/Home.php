<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

	var $arrTemplateData;

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('title_model');
	}
	public function testlogin(){
		$this->load->view('login_view2', $this->arrTemplateData);
	}
	public function index(){
	//	$this->load->view('index');
		//$this->template->load('template_view','display_owner_view');	
		//$this->listtitle();
		$this->arrTemplateData['msg']='';
		if(!$this->session->userdata('sessBoolLoggedIn'))
			$this->load->view('login_view2', $this->arrTemplateData);
		else	
			redirect('home/listtitle');
	}
	
	public function authenticate(){
		$arrPost = $this->input->post();
		$txtUsername = $arrPost['txtUsername'];
		$txtPassword = $arrPost['txtPassword'];
		
		$this->load->model('login_model');
		if(isset($txtUsername))
		{
			$result = $this->login_model->authenticate($txtUsername, $txtPassword);
			
			if(count($result))
			{
				$arrAccess = $result[0]['usr_access'];
				
				$sessData = array(
					'sessUserId'	=> $result[0]['usr_id'],
					'sessAccessLevel'	=> $result[0]['usr_level'],
					'sessUsername'	=> $result[0]['usr_username'],
					'sessUserPasword'	=> $result[0]['usr_password'],
					'sessUserAccess'	=> $arrAccess,
					'sessName'	=> $result[0]['usr_name'],
					'sessPosition'	=> $result[0]['usr_position'],
					'sessOffice'	=> $result[0]['usr_office']
				);	
				
				$this->session->set_userdata($sessData);
				redirect('home/listtitle');
			}else
			{
				$this->session->set_flashdata('strNotification', 'Invalid username/password');
				$this->arrTemplateData['msg'] = "Invalid username/password";
				$this->load->view('login_view2', $this->arrTemplateData);
			}
		}
	}

	public function listtitle(){
		$rsRegOwner=$this->title_model->getRegisteredOwners();
		$arrRegOwners['']='';
		foreach($rsRegOwner as $row):
			$arrRegOwners[$row['orig_owner_id']]=$row['orig_owner_name'];
		endforeach;
		$this->arrTemplateData['arrRegOwners']=$arrRegOwners;	

		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;	
		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces();
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		$rsLandClass=$this->title_model->getLandClass();
		$arrLandClass['']='';
		foreach($rsLandClass as $row):
			$arrLandClass[$row['lcls_id']]=$row['lcls_name'];
		endforeach;
		$this->arrTemplateData['arrLandClass']=$arrLandClass;
		$this->arrTemplateData['arrListData']=$this->title_model->getTitles();
		//$this->load->view('list_title_view', $this->arrTemplateData);
		$this->template->load('template_view','list_title_view', $this->arrTemplateData);	
	}	

	public function addTitle(){


		$rsRegOwner=$this->title_model->getRegisteredOwners();
		$arrRegOwners['']='';
		foreach($rsRegOwner as $row):
			$arrRegOwners[$row['orig_owner_id']]=$row['orig_owner_name'];
		endforeach;
		$this->arrTemplateData['arrRegOwners']=$arrRegOwners;	
		
		// for registered new owner
		$rsRegNewOwner=$this->title_model->getRegisteredOwners();
		$arrRegNewOwners['']='';
		foreach($rsRegNewOwner as $row):
			$arrRegNewOwners[$row['orig_owner_id']]=$row['orig_owner_name']; //discrepancies in label though. should have been new owner id/name
		endforeach;
		$this->arrTemplateData['arrRegNewOwners']=$arrRegNewOwners;	

		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;	
		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces();
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		$rsLandClass=$this->title_model->getLandClass();
		$arrLandClass['']='';
		foreach($rsLandClass as $row):
			$arrLandClass[$row['lcls_id']]=$row['lcls_name'];
		endforeach;
		$this->arrTemplateData['arrLandClass']=$arrLandClass;

		$this->arrTemplateData['arrStatus'] = 'add';
		//$this->load->view('save_title_view', $this->arrTemplateData);
		
		$this->template->load('template_view','save_title_view', $this->arrTemplateData);
	}

	
	public function savetitle(){
		//$this->load->view('title_view');
		$arrPost=$this->input->post();
		print_r($arrPost);
		if(isset($arrPost['btnSave']) || isset($arrPost['btnUpdate'])):
			$arrData = array(
				'act_id' => $arrPost['intAcctId'],
				'tct_new_owner_id' => $arrPost['intNewOwnerId'],
				'orig_owner_id' => $arrPost['intOrigOwnerId'],
				'tct_orig_no' => $arrPost['intOrigNo'],
				//'tct_if_transfered' => $arrPost['bnIfTransfered'],
				'tct_if_consolidated' => $arrPost['bnIfConsolidated'],
				'tct_trc_no' => $arrPost['txtTRCNo'],
				'tct_location' => $arrPost['txtLocation'],
				//'rgn_id' => $arrPost['intRgnId'],
				'prv_id' => $arrPost['intPrvId'],
				'tct_land_area' => $arrPost['intLandArea'],
				'tct_classification' => $arrPost['intClass'],
				'tct_description' => $arrPost['txtDescription'],
				'tct_type' => $arrPost['txtType'],
				'tct_aquisition_cost' => $arrPost['intAcquisition'],
				'tct_with_title' => $arrPost['bnWithTitle'],
				'tct_date_mortgage'	=> $arrPost['dtMortgage'],
				'tct_loan_amount' => $arrPost['intLoanAmount'],
				'tct_fund' => $arrPost['txtFund'],
				//'tct_sold_to_trc'	=> $arrPost['bnSoldtoTRC'],
				'tct_selling_price'	=> $arrPost['intSellingPrice'],
				'tct_amount_rpt' => $arrPost['intAmountRpt'],
				'tct_year_rpt'	=> $arrPost['intYearRpt'],
				'tct_capital_gain' => $arrPost['intCapitalGain'],
				'tct_contacts' => $arrPost['txtContacts'],
				'tct_with_ocular' => $arrPost['bnWithOcular'],
				'tct_with_lease_cont' => $arrPost['bnWithLeaseCont'],
				'tct_lessee' => $arrPost['txtLessee'],
				'tct_lease_monthly'	=> $arrPost['intLeaseMonthly'],
				'tct_lease_duration' => $arrPost['txtLeaseDuration'],
				'tct_lease_status' => $arrPost['txtLeaseStatus'],
				'tct_means_aquisition' => $arrPost['txtMeansAquisition'],
				'tct_for_validation' => $arrPost['bnForValidation'],
				'tct_appraisal_date' => $arrPost['dtAppraisal'],
				'tct_appraisal_value' => $arrPost['intAppraisalValue'],
				'tct_appraisal_remarks' => $arrPost['txtAppraisalRemarks'],
				'tct_remarks' => $arrPost['txtRemarks']
				);

			
			if(isset($arrPost['btnSave'])){
				$id = $this->title_model->addTitle($arrData);
				redirect('home/listtitle/');
				//$this->load->view('list_title_view', $this->arrTemplateData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$id = $this->title_model->updateTitle($arrData, $arrPost['intTCTId']);	
				//$this->arrTemplateData['arrTCTId']=$arrPost['intTCTId'];
				//$this->load->view('display_title_view', $this->arrTemplateData);
				// call viewtitle of Home controller to call display_title view
				redirect('home/viewtitle/'.$arrPost['intTCTId']);
			} 
				
			//$this->load->view('display_title_view', $this->arrTemplateData);		

		endif;	
		
	/*
		$intTitleId = $this->uri->segment(3);	
		$this->arrTemplateData['arrShowData'] = $this->getarraytitle($intTitleId);			


		$rsRegOwner=$this->title_model->getRegisteredOwners();
		$arrRegOwners['']='';
		foreach($rsRegOwner as $row):
			$arrRegOwners[$row['orig_owner_id']]=$row['orig_owner_name'];
		endforeach;
		$this->arrTemplateData['arrRegOwners']=$arrRegOwners;	

		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;	
		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces();
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		$rsLandClass=$this->title_model->getLandClass();
		$arrLandClass['']='';
		foreach($rsLandClass as $row):
			$arrLandClass[$row['lcls_id']]=$row['lcls_name'];
		endforeach;
		$this->arrTemplateData['arrLandClass']=$arrLandClass;
		//$this->arrTemplateData['arrListData']=$this->title_model->getTitles();
	*/
		//$this->load->view('list_title_view', $this->arrTemplateData);
	}

/*
	public function addtitle(){
		//$this->load->view('title_view');
		$arrPost=$this->input->post();
		if(isset($arrPost['btnAdd'])):
			$arrData = array(
				'act_id' => $arrPost['intAcctId'],
				'orig_owner_id' => $arrPost['intOrigOwnerId'],
				'tct_orig_no' => $arrPost['intOrigNo'],
				'tct_if_transfered' => $arrPost['bnIfTransfered'],
				'tct_trc_no' => $arrPost['txtTRCNo'],
				'tct_location' => $arrPost['txtLocation'],
				//'rgn_id' => $arrPost['intRgnId'],
				'prv_id' => $arrPost['intPrvId'],
				'tct_land_area' => $arrPost['intLandArea'],
				'tct_classification' => $arrPost['intClass'],
				'tct_description' => $arrPost['txtDescription'],
				'tct_aquisition_cost' => $arrPost['intAcquisition'],
				'tct_with_title' => $arrPost['bnWithTitle'],
				'tct_date_mortgage'	=> $arrPost['dtMortgage'],
				'tct_loan_amount' => $arrPost['intLoanAmount'],
				'tct_fund' => $arrPost['txtFund'],
				'tct_selling_price'	=> $arrPost['intSellingPrice'],
				'tct_amount_rpt' => $arrPost['intAmountRpt'],
				'tct_year_rpt'	=> $arrPost['intYearRpt'],
				'tct_capital_gain' => $arrPost['intCapitalGain'],
				'tct_contacts' => $arrPost['txtContacts'],
				'tct_with_ocular' => $arrPost['bnWithOcular'],
				'tct_with_lease_cont' => $arrPost['bnWithLeaseCont'],
				'tct_lessee' => $arrPost['txtLessee'],
				'tct_lease_monthly'	=> $arrPost['intLeaseMonthly'],
				'tct_lease_duration' => $arrPost['txtLeaseDuration'],
				'tct_lease_status' => $arrPost['txtLeaseStatus'],
				'tct_means_aquisition' => $arrPost['txtMeansAquisition'],
				'tct_for_validation' => $arrPost['bnForValidation'],
				'tct_remarks' => $arrPost['txtRemarks']
				);

		$id = $this->title_model->addTitle($arrData);
		endif;	
		
		$this->arrTemplateData['arrListData']=$this->title_model->getLegalConcerns();
		$this->load->view('title_view', $this->arrTemplateData);
	}
*/
	public function getarraytitle($intTitleId)
	{
		$rsTitle = $this->title_model->getTitles($intTitleId);
		foreach ($rsTitle as $row) {
				$arrShowData['intTCTId'] = $row['tct_id'];
				$arrShowData['intAcctId'] = $row['act_id'];
				$arrShowData['txtAcctName'] = $row['act_name'];
				$arrShowData['intNewOwnerId'] = $row['tct_new_owner_id']; // new owner
				$rsNewOwnerName = $this->title_model->getRegisteredOwners($row['tct_new_owner_id']);
				if(count($rsNewOwnerName)==0)
					$strNewOwnerName = '';
				else $strNewOwnerName = $rsNewOwnerName[0]['orig_owner_name'];
				$arrShowData['txtNewOwnerName'] = $strNewOwnerName; // ***** call a function
				$arrShowData['intOrigOwnerId'] = $row['orig_owner_id']; 
				$arrShowData['txtOrigOwnerName'] = $row['orig_owner_name']; 
				$arrShowData['intOrigNo'] = $row['tct_orig_no']; 
				//$arrShowData['bnIfTransfered'] = $row['tct_if_transfered']; 
				$arrShowData['bnIfConsolidated'] = $row['tct_if_consolidated']; 
				$arrShowData['txtTRCNo'] = $row['tct_trc_no']; 
				$arrShowData['txtLocation'] = $row['tct_location']; 
				$arrShowData['intRgnId'] = $row['rgn_id']; 
				$arrShowData['txtRgnName'] = $row['rgn_name']; 
				$arrShowData['intPrvId'] = $row['prv_id']; 
				$arrShowData['txtPrvName'] = $row['prv_name']; 
				$arrShowData['intLandArea'] = $row['tct_land_area']; 
				$arrShowData['intClass'] = $row['tct_classification']; 
				$arrShowData['txtClass'] = $row['lcls_name']; 
				$arrShowData['txtDescription'] = $row['tct_description']; 
				$arrShowData['txtType'] = $row['tct_type']; 
				$arrShowData['intAcquisition'] = $row['tct_aquisition_cost']; 
				$arrShowData['bnWithTitle'] = $row['tct_with_title']; 
				$arrShowData['dtMortgage'] = $row['tct_date_mortgage'];
				$arrShowData['intLoanAmount'] = $row['tct_loan_amount']; 
				$arrShowData['txtFund'] = $row['tct_fund']; 
				//$arrShowData['bnSoldtoTRC'] = $row['tct_sold_to_trc'];
				$arrShowData['intSellingPrice'] = $row['tct_selling_price'];
				$arrShowData['intAmountRpt'] = $row['tct_amount_rpt']; 
				$arrShowData['intYearRpt'] = $row['tct_year_rpt'];
				$arrShowData['intCapitalGain'] = $row['tct_capital_gain']; 
				$arrShowData['txtContacts'] = $row['tct_contacts']; 
				$arrShowData['bnWithOcular'] = $row['tct_with_ocular']; 
				$arrShowData['bnWithLeaseCont'] = $row['tct_with_lease_cont']; 
				$arrShowData['txtLessee'] = $row['tct_lessee']; 
				$arrShowData['intLeaseMonthly'] = $row['tct_lease_monthly'];
				$arrShowData['txtLeaseDuration'] = $row['tct_lease_duration']; 
				$arrShowData['txtLeaseStatus'] = $row['tct_lease_status']; 
				$arrShowData['txtMeansAquisition'] = $row['tct_means_aquisition']; 
				$arrShowData['bnForValidation'] = $row['tct_for_validation']; 
				$arrShowData['dtAppraisal'] = $row['tct_appraisal_date']; 
				$arrShowData['intAppraisalValue'] = $row['tct_appraisal_value'];
				$arrShowData['txtAppraisalRemarks'] = $row['tct_appraisal_remarks']; 
				$arrShowData['txtRemarks'] = $row['tct_remarks'];
			}	

			//$this->arrTemplateData['arrShowData'] = $arrShowData;	
			
			return $arrShowData;	

	}
	
	public function edittitle(){

		$intTitleId = $this->uri->segment(3);	

		$this->arrTemplateData['arrShowData'] = $this->getarraytitle($intTitleId);			
	
		$rsRegOwner=$this->title_model->getRegisteredOwners();
		$arrRegOwners['']='';
		foreach($rsRegOwner as $row):
			$arrRegOwners[$row['orig_owner_id']]=$row['orig_owner_name'];
		endforeach;
		$this->arrTemplateData['arrRegOwners']=$arrRegOwners;	
		
		$rsRegNewOwner=$this->title_model->getRegisteredOwners();
		$arrRegNewOwners['']='';
		foreach($rsRegNewOwner as $row):
			$arrRegNewOwners[$row['orig_owner_id']]=$row['orig_owner_name']; // also for new owner
		endforeach;
		$this->arrTemplateData['arrRegNewOwners']=$arrRegNewOwners;	

		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;	
		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces('',$this->arrTemplateData['arrShowData']['intRgnId']);
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		$rsLandClass=$this->title_model->getLandClass();
		$arrLandClass['']='';
		foreach($rsLandClass as $row):
			$arrLandClass[$row['lcls_id']]=$row['lcls_name'];
		endforeach;
		$this->arrTemplateData['arrLandClass']=$arrLandClass;

		//$this->load->view('save_title_view', $this->arrTemplateData);
		$this->template->load('template_view','save_title_view', $this->arrTemplateData);	
	}

	public function viewtitle(){
		
		// here is the possible error
		$intTitleId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(5);
			
		
		$this->arrTemplateData['arrShowData'] = $this->getarraytitle($intTitleId);	
		$this->arrTemplateData['arrListDataLegal'] = $this->title_model->getLegalConcerns($intTitleId);
		$this->arrTemplateData['arrListDataSte'] = $this->title_model->getSiteInspections($intTitleId);

		if($txtMode=='editlegal' || $txtMode=='deletelegal')
		{
			$intLgcId = $this->uri->segment(4);	
			$this->arrTemplateData['arrModeLegal'] = $txtMode;				
			$this->arrTemplateData['arrShowDataLegal'] = $this->getarraylegalconcern($intTitleId,$intLgcId);			
		}elseif($txtMode=='editste' || $txtMode=='deleteste')
		{
			$intSteId = $this->uri->segment(4);	
			$this->arrTemplateData['arrModeSte'] = $txtMode;				
			$this->arrTemplateData['arrShowDataSte'] = $this->getarraysiteinspect($intTitleId,$intSteId);			
		}

//		if(isset($intLgcId)) // if edit legal concerns
	//		$this->arrTemplateData['arrShowDataLegal'] = $this->getarraylegalconcern($intTitleId,$intLgcId);			
		
		
		//$this->load->view('display_title_view', $this->arrTemplateData);
		$this->template->load('template_view','display_title_view', $this->arrTemplateData);
	}		

	public function getarraylegalconcern($intTitleId='', $intLgcId='')
	{
		$rsLegalConcern = $this->title_model->getLegalConcerns($intTitleId, $intLgcId);
		foreach ($rsLegalConcern as $row) {
				$arrShowDataLegal['intLegalId'] = $row['lgc_id'];
				$arrShowDataLegal['intTCTId'] = $row['tct_id'];
				$arrShowDataLegal['txtLegalDate'] = $row['lgc_date'];
				$arrShowDataLegal['txtRemarks'] = $row['lgc_remarks'];
			}	
			return $arrShowDataLegal;	
	}

	public function getarraysiteinspect($intTitleId='', $intSteId='')
	{
		$rsSiteInspect = $this->title_model->getSiteInspections($intTitleId, $intSteId);
		foreach ($rsSiteInspect as $row) {
				$arrShowDataSte['intSteId'] = $row['ste_id'];
				$arrShowDataSte['intTCTId'] = $row['tct_id'];
				$arrShowDataSte['txtSteDate'] = $row['ste_date'];
				$arrShowDataSte['bnSteGuardPresent'] = $row['ste_guard_present'];
				$arrShowDataSte['txtSteRemarks'] = $row['ste_remarks'];
				$arrShowDataSte['txtSteRecommendations'] = $row['ste_recommendations'];
			}	
			return $arrShowDataSte;	
	}
/*
	public function viewlegal(){
		$intTitleId = $this->uri->segment(3);	
		
		$this->arrTemplateData['arrShowData'] = $this->getarraytitle($intTitleId);	

		$this->arrTemplateData['arrListDataLegal'] = $this->title_model->getLegalConcerns($intTitleId);

		$this->load->view('display_title_view', $this->arrTemplateData);
	}
*/	
	public function savelegal(){

		$arrPost=$this->input->post();
		
		//if(isset($arrPost['btnSaveLegal']) || isset($arrPost['btnUpdateLegal'])):
			$arrData = array(
				'tct_id' => $arrPost['intTCTId'],
				'lgc_date' => $arrPost['txtLegalDate'],
				'lgc_remarks' => $arrPost['txtRemarks'],
				
				);
			if(isset($arrPost['btnSaveLegal'])){
				$id = $this->title_model->addLegal($arrData);
				redirect('home/viewtitle/'.$arrPost['intTCTId']);
			}
			elseif (isset($arrPost['btnUpdateLegal'])){
				$intLgcId = $arrPost['intLegalId'];
				$intTCTId = $arrPost['intTCTId'];
				$id = $this->title_model->updateLegal($arrData, $intLgcId);	
				redirect('home/viewtitle/'.$intTCTId);
			}elseif (isset($arrPost['btnDeleteLegal'])){
				$intLgcId = $arrPost['intLegalId'];
				$intTCTId = $arrPost['intTCTId'];
				print_r($arrPost);
				$id = $this->title_model->deleteLegal($intLgcId);	
				redirect('home/viewtitle/'.$intTCTId);
			} 
				
	//	endif;	

		
	}
	/*
	public function deletelegal(){

		$arrPost=$this->input->post();
		print_r($arrPost);
		if(isset($arrPost['btnDeleteLegal'])):
			$arrData = array(
				'tct_id' => $arrPost['intTCTId'],
				'lgc_date' => $arrPost['txtLegalDate'],
				'lgc_remarks' => $arrPost['txtRemarks'],
				
				);

				$intLgcId = $arrPost['intLegalId'];
				$intTCTId = $arrPost['intTCTId'];
				$id = $this->title_model->deleteLegal($intLgcId);	
				redirect('home/viewtitle/'.$intTCTId);
		endif;		
	}
	*/
	
	public function savesiteinspect(){
		$arrPost=$this->input->post();
		
			$arrData = array(
				'tct_id' => $arrPost['intTCTId'],
				'ste_date' => $arrPost['txtSteDate'],
				'ste_guard_present' => $arrPost['bnSteGuardPresent'],
				'ste_remarks' => $arrPost['txtSteRemarks'],
				'ste_recommendations' => $arrPost['txtSteRecommendations'],
				);
			if(isset($arrPost['btnSaveSte'])){
				$id = $this->title_model->addSiteInspection($arrData);
				redirect('home/viewtitle/'.$arrPost['intTCTId']);
			}
			elseif (isset($arrPost['btnUpdateSte'])){
				print_r($arrPost);
				$intSteId = $arrPost['intSteId'];
				$intTCTId = $arrPost['intTCTId'];
				$id = $this->title_model->updateSiteInspection($arrData, $intSteId);	
				redirect('home/viewtitle/'.$intTCTId);
			}elseif (isset($arrPost['btnDeleteSte'])){
				$intSteId = $arrPost['intSteId'];
				$intTCTId = $arrPost['intTCTId'];
				print_r($arrPost);
				$id = $this->title_model->deleteSiteInspection($intSteId);	
				redirect('home/viewtitle/'.$intTCTId);
			} 
		
	}
	
	// for logout
	public function logout(){
		$this->session->sess_destroy();
		redirect('home/index');
	}
	// end of for logout
	
	/*
	public function editlegal(){

		$intTitleId = $this->uri->segment(3);
		$intLgcId = $this->uri->segment(4);	

		$this->arrTemplateData['arrShowData'] = $this->getarraytitle($intTitleId);			
	
		$rsRegOwner=$this->title_model->getRegisteredOwners();
		$arrRegOwners['']='';
		foreach($rsRegOwner as $row):
			$arrRegOwners[$row['orig_owner_id']]=$row['orig_owner_name'];
		endforeach;
		$this->arrTemplateData['arrRegOwners']=$arrRegOwners;	

		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;	
		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces();
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		$rsLandClass=$this->title_model->getLandClass();
		$arrLandClass['']='';
		foreach($rsLandClass as $row):
			$arrLandClass[$row['lcls_id']]=$row['lcls_name'];
		endforeach;
		$this->arrTemplateData['arrLandClass']=$arrLandClass;

		//$this->arrTemplateData['arrListData']=$this->title_model->getTitles();
		$this->load->view('save_title_view', $this->arrTemplateData);
	}
	*/
	/*
	public function showtemplate(){
		$this->template->load('template_view','display_owner_view');	
	}*/

}

?>