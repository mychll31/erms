<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Library extends CI_Controller {

	var $arrTemplateData;

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('library_model');
	}

	public function index(){
		$this->load->view('index');
	}

	public function listaccounts(){	
		$intActId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
		
		if($txtMode=='edit' || $txtMode=='delete')
		{	
			$this->arrTemplateData['arrShowData'] = $this->getarrayaccountname($intActId);
		}
		$this->arrTemplateData['arrListData']=$this->library_model->getAccountNames();

		//$this->load->view('display_account_view', $this->arrTemplateData);
		$this->template->load('template_view','display_account_view', $this->arrTemplateData);	
	}	

	public function saveaccount(){

		$arrPost=$this->input->post();
			$arrData = array(
				'act_id' => $arrPost['intActId'],
				'act_name' => $arrPost['txtAccountName']
				);
						print_r($arrData);

			if(isset($arrPost['btnSave'])){
				$id = $this->library_model->addAccount($arrData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$id = $this->library_model->updateAccount($arrData, $arrPost['intActId']);	
			}elseif (isset($arrPost['btnDelete'])){
				$id = $this->library_model->deleteAccount($arrPost['intActId']);	
			} 
			
			redirect('library/listaccounts');

	}
	
	public function getarrayaccountname($intActId='')
	{
		$rsAccountName = $this->library_model->getAccountNames($intActId);
		foreach ($rsAccountName as $row) {
				$arrShowData['intActId'] = $row['act_id'];
				$arrShowData['txtAccountName'] = $row['act_name'];
			}	
			return $arrShowData;	
	}
	
	// end of for account names
	
	// for registered owners
	public function listregisteredowners(){	
		$intOrigOwnerId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
	/*	$rsAccountName=$this->library_model->getAccountNames();
		$arrAccountNames['']='';
		
		foreach($rsAccountName as $row):
			$arrAccountNames[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccountNames']=$arrAccountNames;	
	*/
		if($txtMode=='edit' || $txtMode=='delete')
		{	
			$this->arrTemplateData['arrShowData'] = $this->getarrayregisteredowner($intOrigOwnerId);
		}
		$this->arrTemplateData['arrListData']=$this->library_model->getRegisteredOwners();

	//	$this->load->view('display_owner_view', $this->arrTemplateData);
		$this->template->load('template_view','display_owner_view', $this->arrTemplateData);	
	}	
	
	public function getarrayregisteredowner($intOrigOwnerId='')
	{
		$rsRegisteredOwner = $this->library_model->getRegisteredOwners($intOrigOwnerId);
		foreach ($rsRegisteredOwner as $row) {
				$arrShowData['intOrigOwnerId'] = $row['orig_owner_id'];
				//$arrShowData['intActId'] = $row['act_id'];
				$arrShowData['txtOrigOwnerName'] = $row['orig_owner_name'];
				$arrShowData['txtOrigOwnerDetails'] = $row['orig_owner_details'];
			}	
			return $arrShowData;	
	}

	public function saveregisteredowner(){

		$arrPost=$this->input->post();
			$arrData = array(
				'orig_owner_id' => $arrPost['intOrigOwnerId'],
				//'act_id' => $arrPost['intActId'],
				'orig_owner_name' => $arrPost['txtOrigOwnerName'],
				'orig_owner_details' => $arrPost['txtOrigOwnerDetails']
				);
						print_r($arrData);

			if(isset($arrPost['btnSave'])){
				$id = $this->library_model->addOrigOwner($arrData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$id = $this->library_model->updateOrigOwner($arrData, $arrPost['intOrigOwnerId']);	
			}elseif (isset($arrPost['btnDelete'])){
				$id = $this->library_model->deleteOrigOwner($arrPost['intOrigOwnerId']);	
			} 
			
			redirect('library/listregisteredowners');

	}
	// end of for registered owners
	
	// for land classifications
	public function listlandclass(){	
		$intActId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
		
		if($txtMode=='edit' || $txtMode=='delete')
		{	
			$this->arrTemplateData['arrShowData'] = $this->getarraylandclass($intActId);
		}
		$this->arrTemplateData['arrListData']=$this->library_model->getLandClassifications();

		//$this->load->view('display_account_view', $this->arrTemplateData);
		$this->template->load('template_view','display_landclass_view', $this->arrTemplateData);	
	}	

	public function savelandclass(){

		$arrPost=$this->input->post();
			$arrData = array(
				'lcls_id' => $arrPost['intLandClassId'],
				'lcls_name' => $arrPost['txtLandClassName']
				);

			if(isset($arrPost['btnSave'])){
				$id = $this->library_model->addLandClass($arrData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$id = $this->library_model->updateLandClass($arrData, $arrPost['intLandClassId']);	
			}elseif (isset($arrPost['btnDelete'])){
				$id = $this->library_model->deleteLandClass($arrPost['intLandClassId']);	
			} 
			
			redirect('library/listlandclass');

	}
	
	public function getarraylandclass($intLandClassId='')
	{
		$rsLandClass = $this->library_model->getLandClassifications($intLandClassId);
		foreach ($rsLandClass as $row) {
				$arrShowData['intLandClassId'] = $row['lcls_id'];
				$arrShowData['txtLandClassName'] = $row['lcls_name'];
			}	
			return $arrShowData;	
	}
	
	// for user accounts
	public function listuseraccounts(){	
		$intUserId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
		$rsUserLevel=$this->library_model->getUserLevels();
		$arrUserLevels['']='';
		
		foreach($rsUserLevel as $row):
			$arrUserLevels[$row['ulvl_id']]=$row['ulvl_name'];
		endforeach;
		$this->arrTemplateData['arrUserLevels']=$arrUserLevels;	

		$arrUserStatus = array(1=>'Active', 2=>'Inactive');
		$this->arrTemplateData['arrUserStatus']=$arrUserStatus;

		if($txtMode=='edit' || $txtMode=='delete')
		{	
			$this->arrTemplateData['arrShowData'] = $this->getarrayuseraccount($intUserId);
		}
		$this->arrTemplateData['arrListData']=$this->library_model->getUserAccounts();

	//	$this->load->view('display_owner_view', $this->arrTemplateData);
		$this->template->load('template_view','display_user_view', $this->arrTemplateData);	
	}	
	
	public function getarrayuseraccount($intUserId='')
	{
		$rsUserAccount = $this->library_model->getUserAccounts($intUserId);
		foreach ($rsUserAccount as $row) {
				$arrShowData['intUserId'] = $row['usr_id'];
				$arrShowData['txtName'] = $row['usr_name'];
				$arrShowData['txtUserPosition'] = $row['usr_position'];
				$arrShowData['txtUserOffice'] = $row['usr_office'];
				$arrShowData['txtUserName'] = $row['usr_username'];
				$arrShowData['txtPassword'] = $row['usr_password'];
				$arrShowData['intLevel'] = $row['usr_level'];
				$arrShowData['txtLevelName'] = $row['ulvl_name'];
				$arrShowData['intActive'] = $row['usr_active'];
			}	
			return $arrShowData;	
	}

	public function saveuseraccount(){

		$arrPost=$this->input->post();
			$arrData = array(
				'usr_id' => $arrPost['intUserId'],
				'usr_name' => $arrPost['txtName'],
				'usr_position' => $arrPost['txtUserPosition'],
				'usr_office' => $arrPost['txtUserOffice'],
				'usr_username' => $arrPost['txtUserName'],
				'usr_level' => $arrPost['intLevel'],
				'usr_active' => $arrPost['intActive']
				);
			
				

			if(isset($arrPost['btnSave'])){
				$arrData['usr_password'] = md5($arrPost['txtPassword']); //for adding new account only
				$id = $this->library_model->addUserAccount($arrData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$arrData['usr_password'] = $arrPost['txtPassword']; //for editing existing account only. password as is.
				$id = $this->library_model->updateUserAccount($arrData, $arrPost['intUserId']);	
			}elseif (isset($arrPost['btnDelete'])){
				$id = $this->library_model->deleteUserAccount($arrPost['intUserId']);	
			} 
			
			redirect('library/listuseraccounts');

	}
	// end of for registered owners
	
	// for reset password
	public function resetpassword(){	
		$intUserId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
		
		$arrUserLevels['']='';
		
		$this->arrTemplateData['arrShowData'] = $this->getarrayuseraccount($intUserId);

		$this->template->load('template_view','display_password_view', $this->arrTemplateData);	
	}	

	public function savepassword(){

		$arrPost=$this->input->post();
		if($arrPost['txtPassword']==$arrPost['txtPassword2'])
		{
			$id = $this->library_model->resetPassword($arrPost['intUserId'], md5($arrPost['txtPassword']));
			redirect('library/listuseraccounts');
		}
		else
			redirect('library/resetpassword');

	}
	// end of for reset password
	
	// for dynamic dropdown of region/province
	public function getcmbprovinces()
	{
		$intRgnId = $this->uri->segment(3);
		
		$this->load->model('title_model');
		
		$rs = $this->title_model->getProvinces('', $intRgnId);	
		
		echo "<option value=''></option>";
		foreach($rs as $row):
		
			echo "<option value='".$row['prv_id']."'>".$row['prv_name']."</option>";
		
		endforeach;
	}

	// for regions
	public function listregions(){	
		$intRegionId = $this->uri->segment(3);
		$txtMode = $this->uri->segment(4);
		$this->arrTemplateData['arrMode'] = $txtMode;
		if($txtMode=='edit' || $txtMode=='delete')
		{	
			$this->arrTemplateData['arrShowData'] = $this->getarrayregion($intRegionId);
		}
		$this->arrTemplateData['arrListData']=$this->library_model->getRegions();

	//	$this->load->view('display_owner_view', $this->arrTemplateData);
		$this->template->load('template_view','display_region_view', $this->arrTemplateData);	
	}	
	
	public function getarrayregion($intId='')
	{
		$rsRegion = $this->library_model->getRegions($intId);
		foreach ($rsRegion as $row) {
				$arrShowData['intRegionId'] = $row['rgn_id'];
				$arrShowData['txtRegionCode'] = $row['rgn_code'];
				$arrShowData['txtRegionLabel'] = $row['rgn_label'];
				$arrShowData['txtRegionName'] = $row['rgn_name'];
				$arrShowData['intRegionDisplayOrder'] = $row['rgn_display_order'];
				$arrShowData['intRegionIsDeleted'] = $row['rgn_is_deleted'];
			}	
			return $arrShowData;	
	}

	public function saveregion(){

		$arrPost=$this->input->post();
			$arrData = array(
				'rgn_id' => $arrPost['intRegionId'],
				'rgn_code' => $arrPost['txtRegionCode'],
				'rgn_label' => $arrPost['txtRegionLabel'],
				'rgn_name' => $arrPost['txtRegionName'],
				'rgn_display_order' => $arrPost['intRegionDisplayOrder']
				);

			if(isset($arrPost['btnSave'])){
				$id = $this->library_model->addRegion($arrData);
			}
			elseif (isset($arrPost['btnUpdate'])){
				$id = $this->library_model->updateRegion($arrData, $arrPost['intRegionId']);	
			}elseif (isset($arrPost['btnDelete'])){
				$id = $this->library_model->deleteRegion($arrPost['intRegionId']);	
			} 
			
			redirect('library/listregions');

	}
	// end of for region
	

}

?>