<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends CI_Controller {

	var $arrTemplateData;

	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('report_model','title_model'));
	}

	public function displayreports(){
		$rsReport=$this->report_model->getReports();
		$arrReports['']='';
		foreach($rsReport as $row):
			$arrReports[$row['rpt_id']]=$row['rpt_name'];
		endforeach;
		$this->arrTemplateData['arrReports']=$arrReports;
		
		$rsAccount=$this->title_model->getAccountNames();
		$arrAccounts['']='';
		foreach($rsAccount as $row):
			$arrAccounts[$row['act_id']]=$row['act_name'];
		endforeach;
		$this->arrTemplateData['arrAccounts']=$arrAccounts;

		$rsRegion=$this->title_model->getRegions();
		$arrRegions['']='';
		foreach($rsRegion as $row):
			$arrRegions[$row['rgn_id']]=$row['rgn_name'];
		endforeach;
		$this->arrTemplateData['arrRegions']=$arrRegions;
		$rsProvince=$this->title_model->getProvinces();
		$arrProvinces['']='';
		foreach($rsProvince as $row):
			$arrProvinces[$row['prv_id']]=$row['prv_name'];
		endforeach;
		$this->arrTemplateData['arrProvinces']=$arrProvinces;
		
		$this->template->load('template_view','display_report_view', $this->arrTemplateData);
	}
	
	
	public function show()
	{
		$this->load->library('fpdf_gen');
		
		$arrGet=$this->input->get();
		// report type
		$rpt=$arrGet['rpt'];
		// get report type name
		$rsReport = $this->report_model->getReports($rpt);
		$rptName = $rsReport[0]['rpt_name'];
		// filter variables
		$actnumber=$arrGet['actnumber'];
		$tctnumber=$arrGet['tctnumber'];
		$title=$arrGet['title'];
		$typename=$arrGet['typename'];
		$region=$arrGet['region'];
		$province=$arrGet['province'];
		switch($rpt)
		{
			// set report title later
			
			case 1: //List of Transfer Certificates of Titles 
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				
				$this->load->model('reports/list_of_titles_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
					//'title'=>$title
				);
				$this->list_of_titles_model->generate($arrData);
			break;
			case 2: //List of Transfer Certificates of Titles (Photo copies and with no titles)
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/list_of_titles_with_no_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->list_of_titles_with_no_model->generate($arrData);
			break;
			case 3: //Report on the Site Inspection
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/report_on_the_site_inspection_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->report_on_the_site_inspection_model->generate($arrData);
			break;
			case 4: //List of for Consolidation Titles
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/List_of_titles_for_consolidation_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->List_of_titles_for_consolidation_model->generate($arrData);
			break;
			case 5: //List of Consolidated Titles to TRC
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/List_of_titles_consolidated_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->List_of_titles_consolidated_model->generate($arrData);
			break;
			case 6: //List of Titles with Legal Concerns
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/List_of_titles_with_legal_concerns_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->List_of_titles_with_legal_concerns_model->generate($arrData);
			break;
			case 7: //List of Titles for Validation
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/List_of_titles_for_validation_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->List_of_titles_for_validation_model->generate($arrData);
			break;
			case 8: //List of Lease Contracts
				$this->fpdf = new FPDF('L','mm','Legal');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/List_of_lease_contracts_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->List_of_lease_contracts_model->generate($arrData);
			break;
			case 9: //Summary Report
				$this->fpdf = new FPDF('P','mm','A4');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/Summary_report_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'actnumber'=>$actnumber,
					'typename'=>$typename,
					'region'=>$region,
					'province'=>$province
				);
				$this->Summary_report_model->generate($arrData);
			break;
			case 10: //Title Profile
				$this->fpdf = new FPDF('P','mm','A4');
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$this->load->model('reports/Title_profile_model');
				// filter variables
				$arrData=array(
					'reportName'=>$rptName,
					'tctnumber'=>$tctnumber
				);
				$this->Title_profile_model->generate($arrData);
			break;
		}

		echo $this->fpdf->Output();
	}


}

?>