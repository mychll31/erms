<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Title Details</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 


<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Original TCT No. </label>
			<?php echo $arrShowData['intOrigNo']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Account Name </label>
			<?php echo $arrShowData['txtAcctName']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Original Owner </label>
			<?php echo $arrShowData['txtOrigOwnerName']; ?>
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>New Registered Owner </label>
			<?php echo $arrShowData['txtNewOwnerName']; ?>
		</div>
	</div>
	
</div>
<div class="row">
	<div class="col-sm-3">
		<label>Consolidated?</label>
		<?php echo $arrShowData['bnIfConsolidated']; ?>
	</div>
	<div class="col-sm-3">
		<label>TRC TCT No.</label>
		<?php echo $arrShowData['txtTRCNo']; ?>
	</div>
    <div class="col-sm-3">
		<label>Type</label>
		<?php echo $arrShowData['txtType']; ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Region </label>
			<?php echo $arrShowData['txtRgnName']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Province </label>
			<?php echo $arrShowData['txtPrvName']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Location/Particulars</label>
			<?php echo $arrShowData['txtLocation']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Land Area (sq.m.) </label>
			<?php echo number_format($arrShowData['intLandArea']); ?>
		</div>
	</div>
	
</div>	
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Classification </label>
			<?php echo $arrShowData['txtClass']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Description</label>
			<?php echo $arrShowData['txtDescription']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>With Title? </label>
			<?php echo $arrShowData['bnWithTitle']; ?>
		</div>
	</div>
    
	<div class="col-sm-3">
		<div class="form-group">
			<label>Date of Mortage</label>
			<?php echo ($arrShowData['dtMortgage']!='0000-00-00'? date('d-M-y',strtotime($arrShowData['dtMortgage'])) : ''); ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Acquisition Cost</label>
				<?php echo number_format($arrShowData['intAcquisition'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Means of Acquisition/Date/Value</label>
				<?php echo $arrShowData['txtMeansAquisition']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Loan Amount</label>
				<?php echo number_format($arrShowData['intLoanAmount'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Fund</label>
				<?php echo $arrShowData['txtFund']; ?>
		</div>
	</div>
</div>

<div class="row">
	
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Selling Price</label>
				<?php echo number_format($arrShowData['intSellingPrice'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Amount RPT</label>
				<?php echo number_format($arrShowData['intAmountRpt'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Year RPT</label>
				<?php echo $arrShowData['intYearRpt']; ?>
		</div>
	</div>
	
</div>

<div class="row">
    <div class="col-sm-3">
		<div class="form-group">	
				<label>Capital Gain</label>
				<?php echo number_format($arrShowData['intCapitalGain'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Contacts</label>
				<?php echo $arrShowData['txtContacts']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Ocular Inspection? </label>
				<?php echo $arrShowData['bnWithOcular']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>For Validation? </label>
				<?php echo $arrShowData['bnForValidation']; ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Date of Appraisal</label>
				<?php echo ($arrShowData['dtAppraisal']!='0000-00-00'? date('d-M-y',strtotime($arrShowData['dtAppraisal'])) : ''); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Appraisal Value</label>
				<?php echo number_format($arrShowData['intAppraisalValue'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Appraisal Remarks</label>
				<?php echo $arrShowData['txtAppraisalRemarks']; ?>
		</div>
	</div>
	
	
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Lease Contract? </label>
				<?php echo $arrShowData['bnWithLeaseCont']; ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Lessee</label>
				<?php echo $arrShowData['txtLessee']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Monthly Lease</label>
				<?php echo number_format($arrShowData['intLeaseMonthly'],2); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Duration of Lease</label>
				<?php echo $arrShowData['txtLeaseDuration']; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Status</label>
				<?php echo $arrShowData['txtLeaseStatus']; ?>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-sm-6">
          <div class="form-group">	
                  <label>Remarks</label>
                  <?php echo $arrShowData['txtRemarks']; ?>
          </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
			<a href="<?= base_url('home/edittitle/'.$arrShowData['intTCTId'])?>"><button type="button" class="btn btn-success">Edit</button></a>  
			<a href="<?= base_url('home/deletetitle/'.$arrShowData['intTCTId'])?>"><button type="button" class="btn btn-danger">Delete</button></a>
		</div>
	</div>        
</div>

<br />
<!-- START OF ACCORDION -->
<div class="col-sm-12"> 
<div class="panel-group" id="accordion-test"> 

<div class="panel panel-default"> 
<div class="panel-heading"> <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion-test" href="tabs-accordions.html#collapseOne">
Legal Concerns
</a> </h4> 
</div> 
<div id="collapseOne" class="panel-collapse collapse in"> <div class="panel-body">

<!-- LEGAL CONCERNS -->
<form method="post" action='<?= base_url();?>home/savelegal'>

<div class="row">
	<div class="col-sm-4">
    	<label>Date *</label>
		<div class="input-group">	
			<input type="text" class="form-control has-datepicker" placeholder="YYYY-MM-DD" name="txtLegalDate"  id="txtLegalDate"  value="<?= isset($arrShowDataLegal['txtLegalDate']) ? $arrShowDataLegal['txtLegalDate'] : '' ?>" required>
            <div class="input-group-addon"> <a href="javascript:void(0);" onclick="$('#txtLegalDate').focus()"><i class="entypo-calendar"></i></a> </div>
            
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label>Remarks *</label>
            
            <textarea id="txtRemarks" name="txtRemarks" class="form-control autogrow"  placeholder="Remarks" required><?= isset($arrShowDataLegal['txtRemarks'])?trim($arrShowDataLegal['txtRemarks']):'' ?></textarea>
            
		</div>
	</div>

</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intTCTId" id="intTCTId" value="<?=isset($arrShowDataLegal['intTCTId']) ? $arrShowDataLegal['intTCTId'] : $arrShowData['intTCTId'] ?>">	
		<input type="hidden" name="intLegalId" id="intLegalId" value="<?=isset($arrShowDataLegal['intLegalId']) ? $arrShowDataLegal['intLegalId'] : '' ?>">	

		<?php 
		if(isset($arrModeLegal)) { 
			if($arrModeLegal=='editlegal') {
		?>
			<!-- <button type="submit" class="btn btn-success" name="btnUpdateLegal">Update</button>-->
            <button type="submit" class="btn btn-success" name="btnUpdateLegal">Update</button>
            
		<?php }
		   if($arrModeLegal=='deletelegal') { ?>
           <!--<button type="submit" class="btn btn-red btn-icon" name="btnDeleteLegal">Delete<i class="entypo-cancel"></i> </button>-->
			<button type="submit" class="btn btn-danger" name="btnDeleteLegal">Delete</button>
            
		<?php 
			}
		} else { ?>
			<!-- <button type="submit" class="btn btn-success" name="btnSaveLegal">Add</button>-->
            <button type="submit" class="btn btn-grey" name="btnSaveLegal">Add</button>
		<?php } ?>
		</div>
	</div>        
</div>

</form>

<table class="table table-hover">
	<thead>	
		<th>Date</th>
		<th>Remarks</th>
	</thead>
	<tbody>

		<?php foreach($arrListDataLegal as $row): ?>
		<tr>
			<td><?=($row['lgc_date']!='0000-00-00'? date('d-M-y',strtotime($row['lgc_date'])) : '')?></td>
			<td><?=$row['lgc_remarks']?></td>
			<td>
				 
                 <a href='<?=base_url('home/viewtitle/'.$row['tct_id'].'/'.$row['lgc_id'].'/editlegal')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
                <a href='<?=base_url('home/viewtitle/'.$row['tct_id'].'/'.$row['lgc_id'].'/deletelegal')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
				
			</td>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<!-- END OF LEGAL CONCERNS -->
</div> </div> </div> 

<div class="panel panel-default"> 
<div class="panel-heading"> <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion-test" href="tabs-accordions.html#collapseTwo">
Site Inspections
</a> </h4> 
</div> 
<div id="collapseTwo" class="panel-collapse collapse"> <div class="panel-body">

<!-- FOR SITE INSPECTION REPORTS -->

<form method="post" action='<?= base_url();?>home/savesiteinspect'>

<div class="row">
	<div class="col-sm-3">
		<label>Date *</label>
		<div class="input-group">
			<input type="text" class="form-control has-datepicker" placeholder="YYYY-MM-DD" name="txtSteDate"  id="txtSteDate" value="<?= isset($arrShowDataSte['txtSteDate']) ? $arrShowDataSte['txtSteDate'] : '' ?>" required>   
            <div class="input-group-addon"> <a href="javascript:void(0);" onclick="$('#txtSteDate').focus()"><i class="entypo-calendar"></i></a> </div>
            
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>Is Guard Present? *</label>
		<?php echo form_dropdown("bnSteGuardPresent", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrShowDataSte['bnSteGuardPresent']) ? $arrShowDataSte['bnSteGuardPresent'] : '','class="form-control" id="bnSteGuardPresent" required=""'); ?>
        </div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Remarks *</label>
            
             <textarea id="txtSteRemarks" name="txtSteRemarks" class="form-control autogrow" placeholder="Remarks" required><?= isset($arrShowDataSte['txtSteRemarks'])?trim($arrShowDataSte['txtSteRemarks']):''?></textarea>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Recommendations *</label>
			
             <textarea id="txtSteRecommendations" name="txtSteRecommendations" class="form-control autogrow"  placeholder="Remarks" required><?= isset($arrShowDataSte['txtSteRecommendations'])?trim($arrShowDataSte['txtSteRecommendations']):''?></textarea>
             
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
			<input type="hidden" name="intTCTId" id="intTCTId" value="<?=isset($arrShowDataSte['intTCTId']) ? $arrShowDataSte['intTCTId'] : $arrShowData['intTCTId'] ?>">	
		<input type="hidden" name="intSteId" id="intSteId" value="<?=isset($arrShowDataSte['intSteId']) ? $arrShowDataSte['intSteId'] : '' ?>">	

		<?php 
		if(isset($arrModeSte)) { 
			if($arrModeSte=='editste') {
		?>
			<!-- <button type="submit" class="btn btn-success" name="btnUpdateSte">Update</button> -->
            <button type="submit" class="btn btn-success" name="btnUpdateSte">Update</button>
		<?php }
		   if($arrModeSte=='deleteste') { ?>
			<!-- <button type="submit" class="btn btn-success" name="btnDeleteSte">Delete</button> -->
            <button type="submit" class="btn btn-danger" name="btnDeleteSte">Delete</button>
		<?php 
			}
		} else { ?>
			<!-- <button type="submit" class="btn btn-success" name="btnSaveSte">Add</button> -->
            <button type="submit" class="btn btn-grey" name="btnSaveSte">Add</button>
		<?php } ?>
		</div>
	</div>        
</div>

</form>

<table class="table table-hover">
	<thead>	
		<th>Date</th>
		<th>Guard Present</th>
        <th>Remarks</th>
        <th>Recommendations</th>
	</thead>
	<tbody>
		<?php foreach($arrListDataSte as $row): ?>
		<tr>
            
			<td><?=($row['ste_date']!='0000-00-00'? date('d-M-y',strtotime($row['ste_date'])) : '')?></td>
            <td><?=$row['ste_guard_present']?></td>
            <td><?=$row['ste_remarks']?></td>
			<td><?=$row['ste_recommendations']?></td>
			<td>			
           		<a href='<?=base_url('home/viewtitle/'.$row['tct_id'].'/'.$row['ste_id'].'/editste')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
                <a href='<?=base_url('home/viewtitle/'.$row['tct_id'].'/'.$row['ste_id'].'/deleteste')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
            
            </td>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<!-- END OF SITE INSPECTION REPORTS -->

</div> </div> </div> 

</div> </div> 
<!-- END OF ACCORDION -->

<!-- end panel body -->
</div>
</div> 
</div>
</div> 


<script src="<?=base_url('assets/js/bootstrap-datepicker.js')?>"></script>
<script>
$(document).ready(function(e) {
    $('.has-datepicker').datepicker( { format:'yyyy-mm-dd' });
});
</script>

