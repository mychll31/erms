
<!DOCTYPE html>
<html lang="en">
<head>
		<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
		<script src="<?=base_url('assets/js/jquery.js')?>"> </script>
		<script src="<?=base_url('assets/js/bootstrap.min.js')?>"> </script>
</head>

<body>

<form method="post" action='<?= base_url();?>home/addtitle'>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label>Account Name *</label>
			<!-- <input type="text" class="form-control" name="intAcctId"  id="intAcctId" placeholder="Account Name"> -->
			<?=isset($intAcctId) ? $intAcctId : ''?>
			<?php echo form_dropdown("intAcctId", $arrAccounts,isset($arrEditData['intAcctId']) ? $arrEditData['intAcctId'] : '','class="form-control" id="intAcctId" required=""'); ?>

		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label>Registered Owner *</label>
			<!--<input type="text" class="form-control" name="intOrigOwnerId"  id="intOrigOwnerId" placeholder="Registered Owner">-->
			<?php echo form_dropdown("intOrigOwnerId", $arrRegOwners,isset($arrEditData['intOrigOwnerId']) ? $arrEditData['intOrigOwnerId'] : '','class="form-control" id="intOrigOwnerId" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label>TCT No. *</label>
			<input type="text" class="form-control" name="intOrigNo"  id="intOrigNo" placeholder="TCT No." value="<?= isset($arrEditData['intOrigNo']) ? $arrEditData['intOrigNo'] : '' ?>" required="">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label>Transferred to TRC? *</label>
		<?php echo form_dropdown("bnIfTransfered", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrEditData['bnIfTransfered']) ? $arrEditData['bnIfTransfered'] : '','class="form-control" id="bnIfTransfered" required=""'); ?>
		<!-- <input type="text" class="form-control" name="bnIfTransfered"  id="bnIfTransfered"> -->
	</div>
	<div class="col-sm-4">
		<label>TRC TCT No.</label>
		<input type="text" class="form-control" name="txtTRCNo"  id="txtTRCNo" placeholder="TRC TCT No." value="<?= isset($arrEditData['txtTRCNo']) ? $arrEditData['txtTRCNo'] : '' ?>">
	</div>
</div>
<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
			<label>Region *</label>
			<!-- <input type="text" class="form-control" name="intRgnId"  id="intRgnId" placeholder="Region"> -->
			
			<?php echo form_dropdown("intRgnId", $arrRegions,isset($arrEditData['intRgnId']) ? $arrEditData['intRgnId'] : '','class="form-control" id="intRgnId" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Province *</label>
			<!-- <input type="text" class="form-control" name="intPrvId"  id="intPrvId" placeholder="Province"> -->
			<?php echo form_dropdown("intPrvId", $arrProvinces,isset($arrEditData['intPrvId']) ? $arrEditData['intPrvId'] : '','class="form-control" id="intPrvId" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Location/Particulars</label>
			<input type="text" class="form-control" name="txtLocation"  id="txtLocation" placeholder="Location/Particulars" value="<?= isset($arrEditData['txtLocation']) ? $arrEditData['txtLocation'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label>Land Area (sq.m.) *</label>
			<input type="text" class="form-control" name="intLandArea"  id="intLandArea" placeholder="Land Area (sq.m.)" value="<?= isset($arrEditData['intLandArea']) ? $arrEditData['intLandArea'] : '' ?>" required="">
		</div>
	</div>
	
</div>	
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Classification *</label>
			<!--<input type="text" class="form-control" name="intClass"  id="intClass" placeholder="Classification">-->
			<?php echo form_dropdown("intClass", $arrLandClass,isset($arrEditData['intClass']) ? $arrEditData['intClass'] : '','class="form-control" id="intClass" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" name="txtDescription"  id="txtDescription" placeholder="Description" value="<?= isset($arrEditData['txtDescription']) ? $arrEditData['txtDescription'] : '' ?>" >
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>With Title? *</label>
			<!--<input type="text" class="form-control" name="bnWithTitle" id="bnWithTitle">-->
			<?php echo form_dropdown("bnWithTitle", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrEditData['bnWithTitle']) ? $arrEditData['bnWithTitle'] : '','class="form-control" id="bnWithTitle" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Date of Mortage</label>
			<input type="text" class="form-control" name="dtMortgage" id="dtMortgage" placeholder="Date of Mortage" value="<?= isset($arrEditData['dtMortgage']) ? $arrEditData['dtMortgage'] : '' ?>">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Acquisition Cost</label>
				<input type="text" class="form-control" name="intAcquisition" id="intAcquisition" placeholder="Acquisition Cost" value="<?= isset($arrEditData['intAcquisition']) ? $arrEditData['intAcquisition'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Means of Acquisition</label>
				<input type="text" class="form-control" name="txtMeansAquisition" id="txtMeansAquisition" placeholder="Means of Acquisition" value="<?= isset($arrEditData['txtMeansAquisition']) ? $arrEditData['txtMeansAquisition'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Loan Amount</label>
				<input type="text" class="form-control" name="intLoanAmount" id="intLoanAmount" placeholder="Loan Amount" value="<?= isset($arrEditData['intLoanAmount']) ? $arrEditData['intLoanAmount'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Fund</label>
				<input type="text" class="form-control" name="txtFund" id="txtFund" placeholder="Fund" value="<?= isset($arrEditData['txtFund']) ? $arrEditData['txtFund'] : '' ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Selling Price</label>
				<input type="text" class="form-control" name="intSellingPrice" id="intSellingPrice" placeholder="Selling Price" value="<?= isset($arrEditData['intSellingPrice']) ? $arrEditData['intSellingPrice'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Amount RPT</label>
				<input type="text" class="form-control" name="intAmountRpt" id="intAmountRpt" placeholder="Amount RPT" value="<?= isset($arrEditData['intAmountRpt']) ? $arrEditData['intAmountRpt'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Year RPT</label>
				<input type="text" class="form-control" name="intYearRpt" id="intYearRpt" placeholder="Year RPT" value="<?= isset($arrEditData['intYearRpt']) ? $arrEditData['intYearRpt'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Capital Gain</label>
				<input type="text" class="form-control" name="intCapitalGain" id="intCapitalGain" placeholder="Capital Gain" value="<?= isset($arrEditData['intCapitalGain']) ? $arrEditData['intCapitalGain'] : '' ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Contacts</label>
				<input type="text" class="form-control" name="txtContacts" id="txtContacts" placeholder="Contacts" value="<?= isset($arrEditData['txtContacts']) ? $arrEditData['txtContacts'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Ocular Inspection? *</label>
				<!--<input type="text" class="form-control" name="bnWithOcular" id="bnWithOcular">-->
				<?php echo form_dropdown("bnWithOcular", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrEditData['bnWithOcular']) ? $arrEditData['bnWithOcular'] : '','class="form-control" id="bnWithOcular" required=""'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>For Validation? *</label>
				<!--<input type="text" class="form-control" name="bnForValidation" id="bnForValidation">-->
				<?php echo form_dropdown("bnForValidation", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrEditData['bnForValidation']) ? $arrEditData['bnForValidation'] : '','class="form-control" id="bnForValidation" required=""'); ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Lease Contract? *</label>
				<!--<input type="text" class="form-control" name="bnWithLeaseCont" id="bnWithLeaseCont">-->
				<?php echo form_dropdown("bnWithLeaseCont", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrEditData['bnWithLeaseCont']) ? $arrEditData['bnWithLeaseCont'] : '','class="form-control" id="bnWithLeaseCont" required=""'); ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Lessee</label>
				<input type="text" class="form-control" name="txtLessee" id="txtLessee" placeholder="Lesse" value="<?= isset($arrEditData['txtLessee']) ? $arrEditData['txtLessee'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Monthly Lease</label>
				<input type="text" class="form-control" name="intLeaseMonthly" id="intLeaseMonthly" placeholder="Monthly Lease" value="<?= isset($arrEditData['intLeaseMonthly']) ? $arrEditData['intLeaseMonthly'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Duration of Lease</label>
				<input type="text" class="form-control" name="txtLeaseDuration" id="txtLeaseDuration" placeholder="Duration of Lease" value="<?= isset($arrEditData['txtLeaseDuration']) ? $arrEditData['txtLeaseDuration'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Status</label>
				<input type="text" class="form-control" name="txtLeaseStatus" id="txtLeaseStatus" placeholder="Status" value="<?= isset($arrEditData['txtLeaseStatus']) ? $arrEditData['txtLeaseStatus'] : '' ?>">
		</div>
	</div>
</div>

<div class="row">
		<div class="form-group">	
				<label>Remarks</label>
				<input type="text" class="form-control" name="txtRemarks" id="txtRemarks" placeholder="Remarks" value="<?= isset($arrEditData['txtRemarks']) ? $arrEditData['txtRemarks'] : '' ?>">
		</div>

</div>

<div class="row">
		<div class="form-group">	
				<button type="submit" class="btn btn-success" name="btnAdd">Save</button>
		</div>
</div>

</form>
<a href="<?= base_url('home/addtitle')?>"> <button class="btn btn-primary" type="button">Add</button></a>
<table class="table table-bordered">
	<thead>	
		<th>Province</th>
		<th>Account Name</th>
		<th>TCT No.</th>
		<th>Location</th>
		<th>Land Area</th>
		<th>Classification</th>
		<th>Registered Owner</th>
		<th>Action</th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
			<td><?=$row['prv_name']?></td>
			<td><?=$row['acct_id']?></td>
			<td><?=$row['tct_orig_no']?></td>
			<td><?=$row['tct_location']?></td>
			<td><?=$row['tct_land_area']?></td>
			<td><?=$row['tct_classification']?></td>
			<td><?=$row['orig_owner_name']?></td>
			<td>
				<a href='<?=base_url('home/edittitle/'.$row['tct_id'])?>'>EDIT</a> |  
				<a href='<?=base_url('home/deletetitle/'.$row['tct_id'])?>'>DELETE</a>
			</td>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>



</body>
</head>
