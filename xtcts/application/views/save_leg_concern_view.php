
<!DOCTYPE html>
<html lang="en">
<head>
		<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
		<script src="<?=base_url('assets/js/jquery.js')?>"> </script>
		<script src="<?=base_url('assets/js/bootstrap.min.js')?>"> </script>
</head>

<body>

<form method="post" action='<?= base_url();?>home/savelegal'>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label>Date</label>
			<input type="text" class="form-control" name="intOrigNo"  id="intOrigNo" placeholder="TCT No." value="<?= isset($arrShowData['intOrigNo']) ? $arrShowData['intOrigNo'] : '' ?>" required="">
		</div>
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<label>Recommendations *</label>
			<input type="text" class="form-control" name="intAcctId"  id="intAcctId" placeholder="Account Name"> 
		</div>
	</div>

</div>

<div class="row">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
			<?php //if ($this->uri->segment(3)=='add') { 
					if (isset($arrStatus)=='add') { 
				?>	
				<button type="submit" class="btn btn-success" name="btnSave">Save</button>
			<?php } else  { ?>		
				<input type="hidden" name="intTCTId" id="intTCTId" value="<?=$arrShowData['intTCTId']?>">
				<button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
			 <?php } ?>	
		</div>
</div>

</form>

<table class="table table-bordered">
	<thead>	
		<th>Date</th>
		<th>Recommendations</th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
			<td><?=$row['lgc_date']?></td>
			<td><?=$row['lgc_recommendations']?></td>
			<td>
				<a href='<?=base_url('home/edittitle/'.$row['lgc_id'])?>'>EDIT</a> |  
				<a href='<?=base_url('home/deletetitle/'.$row['lgc_id'])?>'>DELETE</a>
			</td>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

</body>
</head>
