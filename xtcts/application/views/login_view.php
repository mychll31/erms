<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  
  <link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
  <script src="<?=base_url('assets/js/jquery.js')?>"> </script>
  <script src="<?=base_url('assets/js/bootstrap.min.js')?>"> </script>
  
  <link rel="stylesheet" href="<?= base_url('assets/template/login2/css/normalize.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/template/login2/css/style.css')?>">

  
</head>

<body>
  <div class="login">
  <h2>Log In</h2>
  <form method="post" action='<?= base_url();?>home/authenticate'>
    <div class="row">
   		<div class="form-group">
        <?= isset($msg)? $msg : ''; ?>
        <div>
    </div>
    <div class="row">
    	<div class="form-group">
        <label>Username: </label>
        <input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="Username" required>
        </div>
    </div>
    <div class="row">
    	<div class="form-group">
        <label>Password: </label>
        <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Password" required>
        </div>
    </div>
    
    <div class="row">
    	<div class="form-group">
        <button type="submit" class="btn btn-default" name="btnLogin">Login</button>
        </div>
    </div>
    
	</form>
 <!--
  <div class="utilities">
    <a href="#">Forgot Password?</a>
    <a href="#">Sign Up &rarr;</a>
  </div>
  -->
</div>
  
  
</body>
</html>
