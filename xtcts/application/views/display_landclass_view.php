<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Land Classifications</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 

<form method="post" action='<?= base_url();?>library/savelandclass'>

<div class="row">
	
	<div class="col-sm-4">
		<div class="form-group">
			<label>Land Classification Name *</label>
			<input type="text" class="form-control" name="txtLandClassName"  id="txtLandClassName" placeholder="Land Classification Name" value="<?= isset($arrShowData['txtLandClassName']) ? $arrShowData['txtLandClassName'] : '' ?>" required>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intLandClassId" id="intLandClassId" value="<?=isset($arrShowData['intLandClassId']) ? $arrShowData['intLandClassId'] : '' ?>">	
		
		<?php 
		if(isset($arrMode)) { 
			if($arrMode =='edit') {
		?>
			<!-- <button type="submit" class="btn btn-success" name="btnUpdate">Update</button>-->
            <button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
		<?php }
		   if($arrMode=='delete') { ?>
			<!--<button type="submit" class="btn btn-success" name="btnDelete">Delete</button>-->
            <button type="submit" class="btn btn-danger" name="btnDelete">Delete</button>
		<?php 
			}
		} else { ?>
			<!-- <button type="submit" class="btn btn-success" name="btnSave">Add</button>-->
            <button type="submit" class="btn btn-grey" name="btnSave">Add</button>
		<?php } ?>
		</div>
	</div>
</div>

</form>

<table class="table table-hover">
	<thead>	
		<th>Land Classification Names</th>
        <th></th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
			<td><?=$row['lcls_name']?></td>
			<td>

                <a href='<?=base_url('library/listlandclass/'.$row['lcls_id'].'/edit')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
				<a href='<?=base_url('library/listlandclass/'.$row['lcls_id'].'/delete')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<!-- end panel body -->
</div>
</div> 
</div>
</div> 
