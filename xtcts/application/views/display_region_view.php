<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Regions</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 


<form method="post" action='<?= base_url();?>library/saveregion'>

<div class="row">
	
	<div class="col-sm-3">
		<div class="form-group">
			<label>Region Code </label>
			<input type="text" class="form-control" name="txtRegionCode"  id="txtRegionCode" placeholder="Region Code" value="<?= isset($arrShowData['txtRegionCode']) ? $arrShowData['txtRegionCode'] : '' ?>">
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>Region Label </label>
			<input type="text" class="form-control" name="txtRegionLabel"  id="txtRegionLabel" placeholder="Region Label" value="<?= isset($arrShowData['txtRegionLabel']) ? $arrShowData['txtRegionLabel'] : '' ?>" required>
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>Region Name </label>
			<input type="text" class="form-control" name="txtRegionName"  id="txtRegionName" placeholder="Region Name" value="<?= isset($arrShowData['txtRegionName']) ? $arrShowData['txtRegionName'] : '' ?>" required>
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>Display Order </label>
			<input type="text" class="form-control" name="intRegionDisplayOrder"  id="intRegionDisplayOrder" placeholder="Display Order" value="<?= isset($arrShowData['intRegionDisplayOrder']) ? $arrShowData['intRegionDisplayOrder'] : '' ?>" required>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intRegionId" id="intRegionId" value="<?=isset($arrShowData['intRegionId']) ? $arrShowData['intRegionId'] : '' ?>">	
		
		<?php 
		if(isset($arrMode)) { 
			if($arrMode =='edit') {
		?>
			<button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
		<?php }
		   if($arrMode=='delete') { ?>
			<button type="submit" class="btn btn-danger" name="btnDelete">Delete</button>
		<?php 
			}
		} else { ?>
			<button type="submit" class="btn btn-grey" name="btnSave">Add</button>
		<?php } ?>
		</div>
	</div>
</div>

</form>

<table class="table table-hover">
	<thead>	
  		<th>Region Code</th>
        <th>Region Label</th>
        <th>Region Name</th>
        <th>Display Order</th>
	    <th></th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
       	 	 <td><?=$row['rgn_code']?></td>
              <td><?=$row['rgn_label']?></td>
              <td><?=$row['rgn_name']?></td>
              <td><?=$row['rgn_display_order']?></td>
			<td>
 
                <a href='<?=base_url('library/listregions/'.$row['rgn_id'].'/edit')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
				<a href='<?=base_url('library/listregions/'.$row['rgn_id'].'/delete')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<!-- end panel body -->
</div>
</div> 
</div>
</div> 
