<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Account Names</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 


<form method="post" action='<?= base_url();?>library/saveaccount'>

<div class="row">
	
	<div class="col-sm-4">
		<div class="form-group">
			<label>Account Name *</label>
			<input type="text" class="form-control" name="txtAccountName"  id="txtAccountName" placeholder="Account Name" value="<?= isset($arrShowData['txtAccountName']) ? $arrShowData['txtAccountName'] : '' ?>" required>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intActId" id="intActId" value="<?=isset($arrShowData['intActId']) ? $arrShowData['intActId'] : '' ?>">	
		
		<?php 
		if(isset($arrMode)) { 
			if($arrMode =='edit') {
		?>
			<!-- <button type="submit" class="btn btn-success" name="btnUpdate">Update</button>-->
            <button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
		<?php }
		   if($arrMode=='delete') { ?>
			<!--<button type="submit" class="btn btn-success" name="btnDelete">Delete</button>-->
            <button type="submit" class="btn btn-danger" name="btnDelete">Delete</button>
		<?php 
			}
		} else { ?>
			<!-- <button type="submit" class="btn btn-success" name="btnSave">Add</button>-->
            <button type="submit" class="btn btn-grey" name="btnSave">Add</button>
		<?php } ?>
		</div>
	</div>        
</div>

</form>

<table class="table table-hover">
	<thead>	
		<th>Account Names</th>
        <th></th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
			<td><?=$row['act_name']?></td>
			<td>

                <a href='<?=base_url('library/listaccounts/'.$row['act_id'].'/edit')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
				<a href='<?=base_url('library/listaccounts/'.$row['act_id'].'/delete')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<!-- end panel body -->
</div>
</div> 
</div>
</div> 
