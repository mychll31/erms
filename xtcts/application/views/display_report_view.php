<!-- For Panel -->

<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Reports</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 

<form method="post" id="frmReport">

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label>Report Name *</label>
			<?php echo form_dropdown("intRptId", $arrReports,isset($arrReports['intRptId']) ? $arrReports['intRptId'] : '','class="form-control" id="intRptId" required=""'); ?>
            
		</div>
	</div>
   
</div>

<div class="row">
 <div class="col-sm-4">
    <div class="form-group filter-tctnumber">
          <label>TCT NO.</label>
          <input type="text" name="txtTCTNo" class="form-control">
     </div>
</div>
</div>

<div class="row">
 <div class="col-sm-4">
    <div class="form-group filter-title">
          <label>Title Name (for report 1)</label>
          <input type="text" name="txtTitleName" class="form-control">
     </div>
</div>
 </div>
 
<div class="row">
  <div class="col-sm-4">
    <div class="form-group filter-type">
          <label>Type</label>
         <?php echo form_dropdown("txtType", array(''=>'','Acquired'=>'Acquired', 'Corporate'=>'Corporate'),'','class="form-control" id="txtType" required=""'); ?>
     </div>
</div>
  <div class="col-sm-4">
    <div class="form-group filter-account">
          <label>Account Name</label>
         <?php echo form_dropdown("intAcctId", $arrAccounts,'','class="form-control" id="intAcctId" required=""'); ?>
     </div>
</div>
</div>

<div class="row">
 <div class="col-sm-4">
    <div class="form-group filter-region">
          <label>Region Name</label>
         <?php echo form_dropdown("txtRegionName", $arrRegions,'','class="form-control" id="txtRegionName" required=""'); ?>
     </div>
</div>
<div class="col-sm-4">
    <div class="form-group filter-region">
          <label>Province Name</label>
          <?php echo form_dropdown("txtProvinceName", $arrProvinces,'','class="form-control" id="txtProvinceName" required=""'); ?>
	
     </div>
</div>
 </div>
 
  <div class="row">
   <div class="col-sm-4">
   		<div class="form-group">
          <button class="btn btn-primary" name="btnPrint" type="submit">Print</button>
        </div>
   </div>     
  </div>      
</form>


<!-- end panel body -->
</div>
</div> 
</div>
</div> 

<script>
$(document).ready(function(){
	$('.filter-tctnumber,.filter-title,.filter-account,.filter-type,.filter-region,.filter-province').hide(); // initial hide of filter divs
	
	$('select[name="intRptId"]').change(function(){
    	var rpt=$(this).val();
          clearInput();
		  if(rpt==1)
          {
            //$('select, input').removeAttr('required'); // include textarea if there's any input type
            //showFilter('filter-title');
            //$('input[name="txtTitleName"]').prop ('required','required');
			       $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');
          }
          else if(rpt==2)
          {
             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');
          }
		   else if(rpt==3)
          {
            // $('select, input').removeAttr('required');
            //showFilter('filter-region,filter-province');
            //$('input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required');
             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');

          }
		  else if(rpt==4)
          {
             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');
          }
		  else if(rpt==5)
          {
             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');
          }
		   else if(rpt==6)
          {
			            $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');
             }
		   else if(rpt==7)
          {
 		            $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');		  }
		   else if(rpt==8)
          {
		             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-account,filter-region,filter-province');
            $('input[name="txtType"],input[name="intAcctId"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required','required');	  }
		  else if(rpt==9)
          {
		             $('select, select, select, select').removeAttr('required');
            showFilter('filter-type,filter-region,filter-province');
            $('input[name="txtType"],input[name="txtRegionName"],input[name="txtProvinceName"]').prop ('required','required','required');	  }
		  else if(rpt==10)
          {
             $('select, input').removeAttr('required');
            showFilter('filter-tctnumber');
            $('input[name="txtTCTNo"]').prop ('required');
          }
	});
	
	// checking and setting of values to be submitted to controllers
	 $("#frmReport").submit(function(event) {

          event.preventDefault(); // to prevent form submit
		  // set report type	
		  var rpt=$('select[name="intRptId"]').val();
		  // set filter variables
		  var tctnumber=$('input[name="txtTCTNo"]').val();
      var actnumber=$('select[name="intAcctId"]').val();
		  var title=$('input[name="txtTitleName"]').val();
		  var typename=$('select[name="txtType"]').val();
		  var region=$('select[name="txtRegionName"]').val();
		  var province=$('select[name="txtProvinceName"]').val();
		  //var sig=$('select[name="txtSignatory"]').val();
		  //var year=$('select[name="txtYear"]').val();
		  //var status=$('select[name="txtStatus"]').val();
		 //console.log(title);
		  if(rpt!='')
		 	 window.open('<?=base_url('report/show/')?>?rpt='+rpt+'&tctnumber='+tctnumber+'&title='+title+'&typename='+typename+'&actnumber='+actnumber+'&region='+region+'&province='+province,'_blank','directories=0,titlebar=0,toolbar=0,menubar=0,addressbar=0');
		 
        });
		
		//for region/province dropdown
		 $('#txtRegionName').change( function() { 
	// alert($('#intRgnId').val());
	 	$.ajax({
			url:'<?=base_url('library/getcmbprovinces/')?>'+$('#txtRegionName').val(),
			cache:false,
			success: function(data) {
				//alert(data);
				$('#txtProvinceName').html('');
				$('#txtProvinceName').html(data);
				}
			});
	 
	 
	 });

});

  function showFilter(str) // to hide/show input form based on selected report type
  {
      var val=str.split(',');
      $('.filter-tctnumber,.filter-title,.filter-type,.filter-account,.filter-region,.filter-province').hide();
      $.each(val,function(i){
        if(val[i]=='filter-tctnumber')
          $('.filter-tctnumber').show();
		if(val[i]=='filter-title')
          $('.filter-title').show();
      if(val[i]=='filter-account')
          $('.filter-account').show();   
		  if(val[i]=='filter-type')
          $('.filter-type').show();
        if(val[i]=='filter-region')
          $('.filter-region').show();
		if(val[i]=='filter-province')
          $('.filter-province').show();
      });
  }

  function clearInput() // clear input form when changing report type
  {
   // $('input[name="txtDate"]').val(''); // for input area
    //$('select[name="txtProfile"]').val(''); // dropdown
    $('input[name="txtTCTNo"]').val('');
	$('input[name="txtTitleName"]').val('');
  $('input[name="intAcctId"]').val('');
	 $('select[name="txtType"]').val('');
    $('select[name="txtRegionName"]').val('');
	 $('select[name="txtProvinceName"]').val('');
  }
  
</script>

