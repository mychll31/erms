

<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Title List</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">	
	<a href="<?= base_url('home/addtitle/add')?>"> <button type="button" class="btn btn-default btn-icon icon-left">Add Title
	<i class="entypo-doc-text"></i> </button></a>
    	</div>
    </div>
</div>
<table class="table table-hover" id="title-list">
	<thead>	
		<th>Province</th>
		<th>Account Name</th>
		<th>Orig TCT No.</th>
		<th>TRC TCT No.</th>
		<th>Location</th>
		<th>Land Area</th>
		<th>Classification</th>
		<th>Registered Owner</th>
		<th>For Validation?</th>
		<th>Consolidated?</th>
		<th>Action</th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
			<td><?=$row['prv_name']?></td>
			<td><?=$row['act_name']?></td>
			<td><?=$row['tct_orig_no']?></td>
			<td><?=$row['tct_trc_no']?></td>
			<td><?=$row['tct_location']?></td>
			<td><?=number_format($row['tct_land_area'])?></td>
			<td><?=$row['lcls_name']?></td>
			<td><?=$row['orig_owner_name']?></td>
			<td><?=$row['tct_for_validation']?></td>
			<td><?=$row['tct_if_consolidated']?></td>
			<td>
				
                <a href='<?=base_url('home/viewtitle/'.$row['tct_id'])?>' class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
View
</a>
			</td>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<!-- end panel body -->
</div>
</div> 
</div>
</div> 

<link rel="stylesheet" href="<?= base_url('assets/plugins/datatables/datatables.css')?>" id="style-resource-1">

<script src="<?= base_url('assets/plugins/datatables/datatables.js')?>" id="script-resource-8"></script>

<script>

$(document).ready(function(e) {
    
	var $table1 = jQuery( '#title-list' );
	// Initialize DataTable
	$table1.DataTable( {
	"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	"bStateSave": true
	});
	// Initalize Select Dropdown after DataTables is created
	/*$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
	minimumResultsForSearch: -1
	}); */
	

});

</script>