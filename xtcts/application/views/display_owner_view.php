<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Registered Owners</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 


<form method="post" action='<?= base_url();?>library/saveregisteredowner'>

<div class="row">
	
	<div class="col-sm-4">
		<div class="form-group">
			<label>Registered Owner Name *</label>
			<input type="text" class="form-control" name="txtOrigOwnerName"  id="txtOrigOwnerName" placeholder="Registered Owner Name" value="<?= isset($arrShowData['txtOrigOwnerName']) ? $arrShowData['txtOrigOwnerName'] : '' ?>" required>
		</div>
	</div>
    <div class="col-sm-4">
		<div class="form-group">
			<label>Owner Details</label>
			<input type="text" class="form-control" name="txtOrigOwnerDetails"  id="txtOrigOwnerDetails" placeholder="Registered Owner Details" value="<?= isset($arrShowData['txtOrigOwnerDetails']) ? $arrShowData['txtOrigOwnerDetails'] : '' ?>">
		</div>
	</div>
    
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intOrigOwnerId" id="intOrigOwnerId" value="<?=isset($arrShowData['intOrigOwnerId']) ? $arrShowData['intOrigOwnerId'] : '' ?>">	
		
		<?php 
		if(isset($arrMode)) { 
			if($arrMode =='edit') {
		?>
			<button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
		<?php }
		   if($arrMode=='delete') { ?>
			<button type="submit" class="btn btn-danger" name="btnDelete">Delete</button>
		<?php 
			}
		} else { ?>
			<button type="submit" class="btn btn-grey" name="btnSave">Add</button>
		<?php } ?>
		</div>
	</div>
</div>

</form>

<table class="table table-hover">
	<thead>	
  		<th>Registered Owner Names</th>
        <th>Details</th>
	    <th></th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
       	 	 <td><?=$row['orig_owner_name']?></td>
              <td><?=$row['orig_owner_details']?></td>
			<td>
 
                <a href='<?=base_url('library/listregisteredowners/'.$row['orig_owner_id'].'/edit')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
				<a href='<?=base_url('library/listregisteredowners/'.$row['orig_owner_id'].'/delete')?>' class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a>  
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<!-- end panel body -->
</div>
</div> 
</div>
</div> 
