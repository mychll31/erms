<!DOCTYPE html> <html lang="en"> <head> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1" /> <meta name="description" content="Neon Admin Panel" /> <meta name="author" content="Laborator.co" /> <link rel="icon" href="assets/images/favicon.ico"> <title>Neon</title> 

<link rel="stylesheet" href="<?= base_url('assets/template/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')?>" id="style-resource-1"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/font-icons/entypo/css/entypo.css')?>" id="style-resource-2"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/fonts.css')?>" id="style-resource-3"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/bootstrap.css')?>" id="style-resource-4"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/neon-core.css')?>" id="style-resource-5"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/neon-theme.css')?>" id="style-resource-6"> 

<link rel="stylesheet" href="<?= base_url('assets/template/css/neon-forms.css')?>" id="style-resource-7"> 
<link rel="stylesheet" href="<?= base_url('assets/template/css/skins/green.css')?>" id="style-resource-9"> 
<link rel="stylesheet" href="<?= base_url('assets/template/css/custom.css')?>" id="style-resource-8"> 



<script src="<?= base_url('assets/template/js/jquery-1.11.3.min.js')?>"></script> <!--[if lt IE 9]><script src="http://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]--> <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]--> <!-- TS1485940551: Neon - Responsive Admin Template created by Laborator --> </head> <body class="page-body page-fade skin-green" data-url="http://demo.neontheme.com"> <!-- TS148594055119156: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <div class="page-container"> <!-- TS14859405517764: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <div class="sidebar-menu"> <div class="sidebar-menu-inner"> <header class="logo-env"> <!-- logo --> <div class="logo"> <a href="dashboard/main/main.html"> <img src="assets/images/logo@2x.png" width="120" alt="" /> </a> </div> <!-- logo collapse icon --> <div class="sidebar-collapse"> <a href="demo_neontheme_default_5.html#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> <i class="entypo-menu"></i> </a> </div> <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) --> <div class="sidebar-mobile-menu visible-xs"> <a href="demo_neontheme_default_5.html#" class="with-animation"><!-- add class "with-animation" to support animation --> <i class="entypo-menu"></i> </a> </div> </header> 

<ul id="main-menu" class="main-menu"> 

<li class="has-sub"> <a href='<?=base_url('home/listtitle')?>'><i class="entypo-newspaper"></i><span class="title">Titles</span></a> 
</li> 

<li class="has-sub"> <a href='<?=base_url('report/displayreports')?>'><i class="entypo-docs"></i><span class="title">Reports</span></a> 
</li> 

<li class="has-sub"> <a href="layouts/layout-api/layout-api.html"><i class="entypo-layout"></i><span class="title">Libraries</span></a> 
<ul> 
<li> <a href='<?=base_url('library/listaccounts')?>'><span class="title">Account Names</span></a> </li> 
<li> <a href='<?=base_url('library/listregisteredowners')?>'><span class="title">Registered Owners</span></a> </li>
<li> <a href='<?=base_url('library/listlandclass')?>'><span class="title">Land Classifications</span></a> </li>
<li> <a href='<?=base_url('library/listregions')?>'><span class="title">Regions</span></a> </li>
<?php if($this->session->userdata('sessAccessLevel')==1)
	{ ?>
<li> <a href='<?=base_url('library/listuseraccounts')?>'><span class="title">User Accounts</span></a> </li> 
<?php
	}
?>
</ul> 
</li> 

 </ul> </div> </div> 
 
 <div class="main-content"> <!-- TS14859405519348: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
 <div class="row"> <!-- Profile Info and Notifications --> 
 <div class="col-md-6 col-sm-4 clearfix"> 
 <ul class="user-info pull-left pull-none-xsm"> <!-- Profile Info --><br>
 
   <li class="profile-info dropdown">
     Login as:
     <?= $this->session->userdata('sessName');?>
     </a> 
     <ul class="dropdown-menu"> 
       <!-- Reverse Caret --> 
       
       <li class="caret"></li> 
       <!-- Profile sub-links --> 
     </ul> 
     
   </li> 
 </ul>
</div> 
 <!-- col-md ...-->

 <!-- Raw Links --> 

<div class="col-md-6 col-sm-4 clearfix hidden-xs"> 
<ul class="list-inline links-list pull-right"> 



<li> 
					<!-- for date -->
					<div id="pst-time"></div>
            <script type="text/javascript" id="gwt-pst">
		            (function(d, eId) {
		              var js, gjs = d.getElementById(eId);
		              js = d.createElement('script'); js.id = 'gwt-pst-jsdk';
		              js.src = "//gwhs.i.gov.ph/pst/gwtpst.js?"+new Date().getTime();
		              gjs.parentNode.insertBefore(js, gjs);
		            }(document, 'gwt-pst'));

		            var gwtpstReady = function(){
		              new gwtpstTime('pst-time');
		            }
		            </script>
                    
</li> 




<li class="sep"></li>



 <li> <a href='<?=base_url('home/logout')?>'>
Log Out <i class="entypo-logout right"></i> </a> </li> </ul> </div> </div> <hr /> <!-- TS14859405513853: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <script type="text/javascript">
jQuery(document).ready(function($)
{
// Sample Toastr Notification
setTimeout(function()
{
var opts = {
"closeButton": true,
"debug": false,
"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
"toastClass": "black",
"onclick": null,
"showDuration": "300",
"hideDuration": "1000",
"timeOut": "5000",
"extendedTimeOut": "1000",
"showEasing": "swing",
"hideEasing": "linear",
"showMethod": "fadeIn",
"hideMethod": "fadeOut"
};
});

});

</script>

<div class="row">
<div class="col-sm-12">
<?= $contents ?>
</div>

</div>

 <!-- TS148594055114601: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <!-- Footer --> <footer class="main"> <div class="pull-right">  </div>
&copy; 2017 <strong>DOST Central Office</strong> Information Technology Division  </footer></div> <!-- TS14859405514983: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25"> <div class="chat-inner"> <h2 class="chat-header"> <a href="demo_neontheme_default_5.html#" class="chat-close"><i class="entypo-cancel"></i></a> <i class="entypo-users"></i>
Chat
<span class="badge badge-success is-hidden">0</span> </h2> <div class="chat-group" id="group-1"> <strong>Favorites</strong> <a href="demo_neontheme_default_5.html#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a> </div> <div class="chat-group" id="group-2"> <strong>Work</strong> <a href="demo_neontheme_default_5.html#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a> <a href="demo_neontheme_default_5.html#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a> </div> <div class="chat-group" id="group-3"> <strong>Social</strong> <a href="demo_neontheme_default_5.html#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a> <a href="demo_neontheme_default_5.html#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a> </div> </div> <!-- conversation template --> <div class="chat-conversation"> <div class="conversation-header"> <a href="demo_neontheme_default_5.html#" class="conversation-close"><i class="entypo-cancel"></i></a> <span class="user-status"></span> <span class="display-name"></span> <small></small> </div> <ul class="conversation-body"> </ul> <div class="chat-textarea"> <textarea class="form-control autogrow" placeholder="Type your message"></textarea> </div> </div> </div> <!-- Chat Histories --> <ul class="chat-history" id="sample_history"> <li> <span class="user">Art Ramadani</span> <p>Are you here?</p> <span class="time">09:00</span> </li> <li class="opponent"> <span class="user">Catherine J. Watkins</span> <p>This message is pre-queued.</p> <span class="time">09:25</span> </li> <li class="opponent"> <span class="user">Catherine J. Watkins</span> <p>Whohoo!</p> <span class="time">09:26</span> </li> <li class="opponent unread"> <span class="user">Catherine J. Watkins</span> <p>Do you like it?</p> <span class="time">09:27</span> </li> </ul> <!-- Chat Histories --> <ul class="chat-history" id="sample_history_2"> <li class="opponent unread"> <span class="user">Daniel A. Pena</span> <p>I am going out.</p> <span class="time">08:21</span> </li> <li class="opponent unread"> <span class="user">Daniel A. Pena</span> <p>Call me when you see this message.</p> <span class="time">08:27</span> </li> </ul> </div> <!-- TS14859405519773: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <!-- Sample Modal (Default skin) --> <div class="modal fade" id="sample-modal-dialog-1"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Widget Options - Default Modal</h4> </div> <div class="modal-body"> <p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button> </div> </div> </div> </div> <!-- Sample Modal (Skin inverted) --> <div class="modal invert fade" id="sample-modal-dialog-2"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Widget Options - Inverted Skin Modal</h4> </div> <div class="modal-body"> <p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button> </div> </div> </div> </div> <!-- Sample Modal (Skin gray) --> <div class="modal gray fade" id="sample-modal-dialog-3"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Widget Options - Gray Skin Modal</h4> </div> <div class="modal-body"> <p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button> </div> </div> </div> </div> <!-- Imported styles on this page --> 

<link rel="stylesheet" href="<?= base_url('assets/template/js/jvectormap/jquery-jvectormap-1.2.2.css')?>" id="style-resource-1"> 

<link rel="stylesheet" href="<?= base_url('assets/template/js/rickshaw/rickshaw.min.css')?>" id="style-resource-2"> 

<script src="<?= base_url('assets/template/js/gsap/TweenMax.min.js')?>" id="script-resource-1"></script> 

<script src="<?= base_url('assets/template/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')?>" id="script-resource-2"></script> 

<script src="<?= base_url('assets/template/js/bootstrap.js')?>" id="script-resource-3"></script> 

<script src="<?= base_url('assets/template/js/joinable.js')?>" id="script-resource-4"></script> 

<script src="<?= base_url('assets/template/js/resizeable.js')?>" id="script-resource-5"></script> 

<script src="<?= base_url('assets/template/js/neon-api.js')?>" id="script-resource-6"></script> 

<script src="<?= base_url('assets/template/js/cookies.min.js')?>" id="script-resource-7"></script> 

<script src="<?= base_url('assets/template/js/jvectormap/jquery-jvectormap-1.2.2.min.js')?>" id="script-resource-8"></script> 

<script src="<?= base_url('assets/template/js/jvectormap/jquery-jvectormap-europe-merc-en.js')?>" id="script-resource-9"></script> 

<script src="<?= base_url('assets/template/js/jquery.sparkline.min.js')?>" id="script-resource-10"></script> 

<script src="<?= base_url('assets/template/js/rickshaw/vendor/d3.v3.js')?>" id="script-resource-11"></script> 

<script src="<?= base_url('assets/template/js/rickshaw/rickshaw.min.js')?>" id="script-resource-12"></script> 

<script src="<?= base_url('assets/template/js/raphael-min.js')?>" id="script-resource-13"></script> 



<script src="<?= base_url('assets/template/js/toastr.js')?>" id="script-resource-15"></script> 

<script src="<?= base_url('assets/template/js/neon-chat.js')?>" id="script-resource-16"></script> <!-- JavaScripts initializations and stuff --> 
<script src="<?= base_url('assets/template/js/neon-custom.js')?>" id="script-resource-17"></script> <!-- Demo Settings --> <

script src="<?= base_url('assets/template/js/neon-demo.js')?>" id="script-resource-18"></script> 

<script src="<?= base_url('assets/template/js/neon-skins.js')?>" id="script-resource-19"></script> 

<script type="text/javascript">
jQuery(document).ready(function($)
{
Cookies.set('current-skin', 'green', {domain: 'demo.neontheme.com', expires: 3600});
});
</script> 

 </body> </html>