<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">Edit Title Details</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 

<form method="post" action='<?= base_url();?>home/savetitle'>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Original TCT No.</label>
			<input type="text" class="form-control" name="intOrigNo"  id="intOrigNo" placeholder="TCT No." value="<?= isset($arrShowData['intOrigNo']) ? $arrShowData['intOrigNo'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Account Name</label>
			<!-- <input type="text" class="form-control" name="intAcctId"  id="intAcctId" placeholder="Account Name"> -->
			<?=isset($intAcctId) ? $intAcctId : ''?>
			<?php echo form_dropdown("intAcctId", $arrAccounts,isset($arrShowData['intAcctId']) ? $arrShowData['intAcctId'] : '','class="form-control" id="intAcctId"'); ?>

		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Original Owner</label>
			<!--<input type="text" class="form-control" name="intOrigOwnerId"  id="intOrigOwnerId" placeholder="Registered Owner">-->
			<?php echo form_dropdown("intOrigOwnerId", $arrRegOwners,isset($arrShowData['intOrigOwnerId']) ? $arrShowData['intOrigOwnerId'] : '','class="form-control" id="intOrigOwnerId"'); ?>
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>New Registered Owner</label>
			<!--<input type="text" class="form-control" name="intOrigOwnerId"  id="intOrigOwnerId" placeholder="Registered Owner">-->
			<?php echo form_dropdown("intNewOwnerId", $arrRegNewOwners,isset($arrShowData['intNewOwnerId']) ? $arrShowData['intNewOwnerId'] : '','class="form-control" id="intNewOwnerId"'); ?>
		</div>
	</div>
	
</div>
<div class="row">
	 <div class="col-sm-3">
		<div class="form-group">
			<label>Consolidated? </label>
			<!--<input type="text" class="form-control" name="bnWithTitle" id="bnWithTitle">-->
			<?php echo form_dropdown("bnIfConsolidated", array(''=>'','Y'=>'Yes', 'FC'=>'For Consolidation'),isset($arrShowData['bnIfConsolidated']) ? $arrShowData['bnIfConsolidated'] : '','class="form-control" id="bnIfConsolidated"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>TRC TCT No.</label>
			<input type="text" class="form-control" name="txtTRCNo"  id="txtTRCNo" placeholder="TRC TCT No." value="<?= isset($arrShowData['txtTRCNo']) ? $arrShowData['txtTRCNo'] : '' ?>">
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">
			<label>Type</label>
			<?php echo form_dropdown("txtType", array(''=>'','Acquired'=>'Acquired', 'Corporate'=>'Corporate'),isset($arrShowData['txtType']) ? $arrShowData['txtType'] : '','class="form-control" id="txtType"'); ?>
		</div>
		</div>
	</div>
   
</div>
<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
			<label>Region </label>
			<!-- <input type="text" class="form-control" name="intRgnId"  id="intRgnId" placeholder="Region"> -->
			
			<?php echo form_dropdown("intRgnId", $arrRegions,isset($arrShowData['intRgnId']) ? $arrShowData['intRgnId'] : '','class="form-control" id="intRgnId"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Province </label>
			<!-- <input type="text" class="form-control" name="intPrvId"  id="intPrvId" placeholder="Province"> -->
			<?php echo form_dropdown("intPrvId", $arrProvinces,isset($arrShowData['intPrvId']) ? $arrShowData['intPrvId'] : '','class="form-control" id="intPrvId"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Location/Particulars</label>
			<input type="text" class="form-control" name="txtLocation"  id="txtLocation" placeholder="Location/Particulars" value="<?= isset($arrShowData['txtLocation']) ? $arrShowData['txtLocation'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label>Land Area (sq.m.) </label>
			<input type="text" class="form-control" name="intLandArea"  id="intLandArea" placeholder="Land Area (sq.m.)" value="<?= isset($arrShowData['intLandArea']) ? $arrShowData['intLandArea'] : '' ?>">
		</div>
	</div>
	
</div>	
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label>Classification</label>
			<!--<input type="text" class="form-control" name="intClass"  id="intClass" placeholder="Classification">-->
			<?php echo form_dropdown("intClass", $arrLandClass,isset($arrShowData['intClass']) ? $arrShowData['intClass'] : '','class="form-control" id="intClass"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" name="txtDescription"  id="txtDescription" placeholder="Description" value="<?= isset($arrShowData['txtDescription']) ? $arrShowData['txtDescription'] : '' ?>" >
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<label>With Title?</label>
			<!--<input type="text" class="form-control" name="bnWithTitle" id="bnWithTitle">-->
			<?php echo form_dropdown("bnWithTitle", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrShowData['bnWithTitle']) ? $arrShowData['bnWithTitle'] : '','class="form-control" id="bnWithTitle"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
    
			<label>Date of Mortage</label>
		<div class="input-group">
			<input type="text" class="form-control has-datepicker" placeholder="YYYY-MM-DD" name="dtMortgage" id="dtMortgage"  value="<?= isset($arrShowData['dtMortgage']) ? $arrShowData['dtMortgage'] : '' ?>">
            
            <div class="input-group-addon"> <a href="javascript:void(0);" onclick="$('#dtMortgage').focus()"><i class="entypo-calendar"></i></a> </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Acquisition Cost</label>
				<input type="text" class="form-control" name="intAcquisition" id="intAcquisition" placeholder="Acquisition Cost" value="<?= isset($arrShowData['intAcquisition']) ? $arrShowData['intAcquisition'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Means of Acquisition/Date/Value</label>
				<input type="text" class="form-control" name="txtMeansAquisition" id="txtMeansAquisition" placeholder="Means of Acquisition" value="<?= isset($arrShowData['txtMeansAquisition']) ? $arrShowData['txtMeansAquisition'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Loan Amount</label>
				<input type="text" class="form-control" name="intLoanAmount" id="intLoanAmount" placeholder="Loan Amount" value="<?= isset($arrShowData['intLoanAmount']) ? $arrShowData['intLoanAmount'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Fund</label>
				<input type="text" class="form-control" name="txtFund" id="txtFund" placeholder="Fund" value="<?= isset($arrShowData['txtFund']) ? $arrShowData['txtFund'] : '' ?>">
		</div>
	</div>
</div>

<div class="row">
	
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Selling Price</label>
				<input type="text" class="form-control" name="intSellingPrice" id="intSellingPrice" placeholder="Selling Price" value="<?= isset($arrShowData['intSellingPrice']) ? $arrShowData['intSellingPrice'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Amount RPT</label>
				<input type="text" class="form-control" name="intAmountRpt" id="intAmountRpt" placeholder="Amount RPT" value="<?= isset($arrShowData['intAmountRpt']) ? $arrShowData['intAmountRpt'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Year RPT</label>
				<input type="text" class="form-control" name="intYearRpt" id="intYearRpt" placeholder="Year RPT" value="<?= isset($arrShowData['intYearRpt']) ? $arrShowData['intYearRpt'] : '' ?>">
		</div>
	</div>
	
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Capital Gain</label>
				<input type="text" class="form-control" name="intCapitalGain" id="intCapitalGain" placeholder="Capital Gain" value="<?= isset($arrShowData['intCapitalGain']) ? $arrShowData['intCapitalGain'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Contacts</label>
				<input type="text" class="form-control" name="txtContacts" id="txtContacts" placeholder="Contacts" value="<?= isset($arrShowData['txtContacts']) ? $arrShowData['txtContacts'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Ocular Inspection? </label>
				<!--<input type="text" class="form-control" name="bnWithOcular" id="bnWithOcular">-->
				<?php echo form_dropdown("bnWithOcular", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrShowData['bnWithOcular']) ? $arrShowData['bnWithOcular'] : '','class="form-control" id="bnWithOcular"'); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>For Validation? </label>
				<!--<input type="text" class="form-control" name="bnForValidation" id="bnForValidation">-->
				<?php echo form_dropdown("bnForValidation", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrShowData['bnForValidation']) ? $arrShowData['bnForValidation'] : '','class="form-control" id="bnForValidation"'); ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
			<label>Appraisal Date</label>
		<div class="input-group">
			<input type="text" class="form-control has-datepicker" placeholder="YYYY-MM-DD" name="dtAppraisal" id="dtAppraisal"  value="<?= isset($arrShowData['dtAppraisal']) ? $arrShowData['dtAppraisal'] : '' ?>">
            
            <div class="input-group-addon"> <a href="javascript:void(0);" onclick="$('#dtAppraisal').focus()"><i class="entypo-calendar"></i></a> </div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Appraisal value</label>
				<input type="text" class="form-control" name="intAppraisalValue" id="intAppraisalValue" placeholder="Appraisal Value" value="<?= isset($arrShowData['intAppraisalValue']) ? $arrShowData['intAppraisalValue'] : '' ?>">
		</div>
	</div>
    <div class="col-sm-3">
		<div class="form-group">	
				<label>Appraisal Remarks</label>
				<input type="text" class="form-control" name="txtAppraisalRemarks" id="txtAppraisalRemarks" placeholder="Appraisal Remarks" value="<?= isset($arrShowData['txtAppraisalRemarks']) ? $arrShowData['txtAppraisalRemarks'] : '' ?>">
		</div>
	</div>
	
</div>


<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>With Lease Contract? </label>
				<!--<input type="text" class="form-control" name="bnWithLeaseCont" id="bnWithLeaseCont">-->
				<?php echo form_dropdown("bnWithLeaseCont", array(''=>'','Y'=>'Yes', 'N'=>'No'),isset($arrShowData['bnWithLeaseCont']) ? $arrShowData['bnWithLeaseCont'] : '','class="form-control" id="bnWithLeaseCont"'); ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Lessee</label>
				<input type="text" class="form-control" name="txtLessee" id="txtLessee" placeholder="Lesse" value="<?= isset($arrShowData['txtLessee']) ? $arrShowData['txtLessee'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Monthly Lease</label>
				<input type="text" class="form-control" name="intLeaseMonthly" id="intLeaseMonthly" placeholder="Monthly Lease" value="<?= isset($arrShowData['intLeaseMonthly']) ? $arrShowData['intLeaseMonthly'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Duration of Lease</label>
				<input type="text" class="form-control" name="txtLeaseDuration" id="txtLeaseDuration" placeholder="Duration of Lease" value="<?= isset($arrShowData['txtLeaseDuration']) ? $arrShowData['txtLeaseDuration'] : '' ?>">
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">	
				<label>Status</label>
				<input type="text" class="form-control" name="txtLeaseStatus" id="txtLeaseStatus" placeholder="Status" value="<?= isset($arrShowData['txtLeaseStatus']) ? $arrShowData['txtLeaseStatus'] : '' ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">	
				<label>Remarks</label>
                
                <textarea id="txtRemarks" name="txtRemarks" class="form-control autogrow"><?=isset($arrShowData['txtRemarks'])?trim($arrShowData['txtRemarks']):''?></textarea>
                
				<!--<input type="text" class="form-control" name="txtRemarks" id="txtRemarks" placeholder="Remarks" value="<?//= //isset($arrShowData['txtRemarks']) ? $arrShowData['txtRemarks'] : '' ?>"> -->
                
                
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
			<?php //if ($this->uri->segment(3)=='add') { 
					if (isset($arrStatus)=='add') { 
				?>	
				<button type="submit" class="btn btn-success" name="btnSave">Save</button>
			<?php } else  { ?>		
				<input type="hidden" name="intTCTId" id="intTCTId" value="<?=$arrShowData['intTCTId']?>">
				<button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
			 <?php } ?>	
		</div>
	</div>        
</div>
</form>

<!-- end panel body -->
</div>
</div> 
</div>
</div> 

<script src="<?=base_url('assets/js/bootstrap-datepicker.js')?>"></script>
<script>
$(document).ready(function(e) {
    $('.has-datepicker').datepicker( { format:'yyyy-mm-dd' });
	 $('#intRgnId').change( function() { 
	// alert($('#intRgnId').val());
	 	$.ajax({
			url:'<?=base_url('library/getcmbprovinces/')?>'+$('#intRgnId').val(),
			cache:false,
			success: function(data) {
				//alert(data);
				$('#intPrvId').html('');
				$('#intPrvId').html(data);
				}
			});
	 
	 
	 });
	
});
</script>