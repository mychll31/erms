<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>

  <link rel="stylesheet" href="<?= base_url('assets/template/login2/css/normalize.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/template/login2/css/style.css')?>">

<style type="text/css">


	div {
		padding: 20px;	
	}
	
	.res {
		font-size: 28px;
		height:80px;
		background-color:#137F4F;
		border-bottom:1px solid black;
		text-align:center;
		color: #ffffff;
	
	}
	
	.res img {
	
		margin-top:8.5px;
		margin-left:6px;
		display:inline
	}
	
	.res span {
	
		display:block;
	}
/*
	.footer {
	  font-size: 12px;
	  position: absolute;
	  right: 0;
	  bottom: 0;
	  left: 0;
	  padding: .5rem;
	  background-color: #efefef;
	  text-align: center;
	}
*/	
	.footer{ 
	   font-size: 14px;
	   color: #FFFFFF;
       position: fixed;     
       text-align: center;    
       bottom: 0px; 
       width: 100%;
   }  
    </style>

  
</head>

<body>

<div class="res">
<span>
<img src='<?=base_url('assets/images/logo.png')?>' height="40" width="40">
</span>
<span>
 DOST-TRC Transfer of Certificates of Titles System
</span>

</div>

</br>
  <div class="login">
  <h2>Log In</h2>
 <form method="post" action='<?= base_url();?>home/authenticate'>
 <fieldset>
   <input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="Username" required />

   <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Password" required />
 </fieldset>
   
   <input type="submit" class="btn btn-default" name="btnLogin" value="Login" />

	</form>
<?php if(isset($msg))
	{ ?>   
  <div class="utilities">
    <a href="#"><?= $msg ?></a>
    <!--<a href="#">Sign Up &rarr;</a> -->
  </div>
  <?php  
  	}?>
</div>
  
  <div class="footer">  
&copy; 2017 <strong>DOST Central Office</strong> Information Technology Division  
  </div>
</body>
</html>
