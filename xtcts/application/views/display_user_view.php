
<div class="row"> 
<div class="form-group">
<div class="panel panel-success" data-collapsed="0"> 
<!-- panel head --> 
<div class="panel-heading"> 
<div class="panel-title">User Accounts</div> 
</div> 
<!-- panel body --> 
<div class="panel-body"> 


<form method="post" action='<?= base_url();?>library/saveuseraccount'>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label>Name *</label>
			<input type="text" class="form-control" name="txtName"  id="txtName" placeholder="Name" value="<?= isset($arrShowData['txtName']) ? $arrShowData['txtName'] : '' ?>" required>
		</div>
	</div>
    <div class="col-sm-4">
		<div class="form-group">
			<label>Position *</label>
			<input type="text" class="form-control" name="txtUserPosition"  id="txtUserPosition" placeholder="Position" value="<?= isset($arrShowData['txtUserPosition']) ? $arrShowData['txtUserPosition'] : '' ?>" required>
		</div>
	</div>
    <div class="col-sm-4">
		<div class="form-group">
			<label>Office *</label>
			<input type="text" class="form-control" name="txtUserOffice"  id="txtUserOffice" placeholder="Office" value="<?= isset($arrShowData['txtUserOffice']) ? $arrShowData['txtUserOffice'] : '' ?>" required>
		</div>
	</div>
</div>   

<div class="row">      
	<div class="col-sm-4">
		<div class="form-group">
			<label>Username *</label>
			<input type="text" class="form-control" name="txtUserName"  id="txtUserName" placeholder="Username" value="<?= isset($arrShowData['txtUserName']) ? $arrShowData['txtUserName'] : '' ?>" required>
		</div>    
    </div>
    <?php if(!isset($arrShowData['intUserId'])) 
	{?>
	<div class="col-sm-4">
		<div class="form-group">
			<label>Password *</label>
			<input type="text" class="form-control" name="txtPassword"  id="txtPassword" placeholder="Password" value="<?= isset($arrShowData['txtPassword']) ? $arrShowData['txtPassword'] : '' ?>" required>
		</div>    
    </div>    
<?php } ?>
   
    <div class="col-sm-4">
		<div class="form-group">
			<label>Level *</label>
			<?php echo form_dropdown("intLevel", $arrUserLevels,isset($arrShowData['intLevel']) ? $arrShowData['intLevel'] : '','class="form-control" id="intLevel" required=""'); ?>
		</div>
	</div>
    <div class="col-sm-4">
		<div class="form-group">
			<label>Status *</label>
			<?php echo form_dropdown("intActive", $arrUserStatus,isset($arrShowData['intActive']) ? $arrShowData['intActive'] : '','class="form-control" id="intActive" required=""'); ?>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">	
	<!-- if add butto from list view  else save view -->
		<input type="hidden" name="intUserId" id="intUserId" value="<?=isset($arrShowData['intUserId']) ? $arrShowData['intUserId'] : '' ?>">	
		
		<?php 
		if(isset($arrMode)) { 
			if($arrMode =='edit') {
		?>
	        <input type="hidden" class="form-control" name="txtPassword"  id="txtPassword" placeholder="Password" value="<?= isset($arrShowData['txtPassword']) ? $arrShowData['txtPassword'] : '' ?>" required>

			<button type="submit" class="btn btn-success" name="btnUpdate">Update</button>
		<?php }
		  
		  // if($arrMode=='delete') { ?>
			<!-- <button type="submit" class="btn btn-danger" name="btnDelete">Delete</button> -->
		<?php 
			//}
		} 
		
		else { ?>
			<button type="submit" class="btn btn-grey" name="btnSave">Add</button>
		<?php } ?>
		</div>
	</div>
</div>

</form>

<table class="table table-hover">
	<thead>	
  		<th>Names</th>
        <th>Office</th>
        <th>Position</th>
        <th>Username</th>
		<th>Level</th>
        <th>Status</th>
        
        <th></th>
	</thead>
	<tbody>
		<?php foreach($arrListData as $row): ?>
		<tr>
       	 	 <td><?=$row['usr_name']?></td>
             <td><?=$row['usr_office']?></td>
			<td><?=$row['usr_position']?></td> 
            <td><?=$row['usr_username']?></td> 
            <td><?=$row['ulvl_name']?></td>  
            <td><?= ($row['usr_active']==1) ? 'Active' : 'Inactive' ?></td>
			<td>
 
                <a href='<?=base_url('library/listuseraccounts/'.$row['usr_id'].'/edit')?>' class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a>
				<a href='<?=base_url('library/resetpassword/'.$row['usr_id'].'/reset')?>' class="btn btn-gold btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Reset
</a>  

			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<!-- end panel body -->
</div>
</div> 
</div>
</div> 
