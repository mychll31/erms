<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login </title>
  
  <link rel="stylesheet" href="<?= base_url('assets/template/login1/css/reset.min.css')?>">

  
      <link rel="stylesheet" href="<?= base_url('assets/template/login1/css/style.css')?>">

  
</head>

<body>
  <div class="wrap">
		<div class="avatar">
      <img src="<?= base_url('assets/images/dostlogo.png')?>" width="50" height="50">
		</div>
		<input type="text" placeholder="username" required>
		<div class="bar">
			<i></i>
		</div>
		<input type="password" placeholder="password" required>
		<a href="" class="forgot_link">forgot ?</a>
		<button>Sign in</button>
</div>
  
    <script src="<?= base_url('assets/template/login1/js/index.js')?>"></script>

</body>
</html>
