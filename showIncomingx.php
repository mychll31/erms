<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");
require_once("class/jqSajax.class.php");
require_once("class/DocDetail.class.php");
include_once("class/IncomingList.class.php");
$objList = new IncomingList;
$objIncoming = new Incoming;
$objDocDetail = new DocDetail;
$file=new General();
$ajax=new jqSajax();
$ajax->export("Load","file->Load");
$ajax->export("_decode","file->_decode");
$ajax->export("getTargetFile","file->getTargetFile");
$ajax->export("getTargetFile","file->removeItem");
$ajax->export("checkifConfidential","file->checkifConfidential");
$ajax->export("getFileOrigin","file->getFileOrigin");
$ajax->export("fileExist","file->fileExist");
$ajax->processClientReq();
$group = explode('~',$objList->getOfficeCode($objList->get('userID')));
if($_REQUEST['mode']=='update' && $_REQUEST['blnDocNoOnly'])
{
	 echo $objIncoming->updateDocNo($_REQUEST);
}

?>	
<script src="facebox/facebox.js" type="text/javascript"></script>
<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(function() {
	$('#dateReceived').datetimepicker({
		timeFormat: "hh:mm tt",
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true
	});
	$('#documentDate').datepicker({
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true		
	});
	$('a[rel*=facebox]').facebox({
	 loading_image : '/facebox/loading.gif',
	 close_image   : '/facebox/closelabel.gif'
	});
$("a.barcode").fancybox({
		 'frameWidth' : 620, 
		 'frameHeight': 450, 
		 'overlayShow': true, 
		 'overlayOpacity': 0.7,
		 'hideOnContentClick': false
});
$('#intDocNum').live('focusin',function(){
 $('#btnEditDocNo').val("Save");
 //alert($(this).val());
})
});

	function editDocNo(docID,docNO,hashID){  
	  if($('#btnEditDocNo').val() == 'Save'){ 
		$.ajax({ 
		  url: "showIncoming.php",
		  type: "POST",
		  data: "mode=update&blnDocNoOnly=true&docID="+docID+"&docNO="+$('#intDocNum').val(),
		  success: function(data){ 
		   getData('showIncoming.php?mode=view&id='+docID,'doclist');
		  }
		});		  
		return false;
	  }
	  var newInput = "<input type='text' name='intDocNum' id='intDocNum' value='"+docNO+"' />";
	  $('#docNoViewMode').html(newInput);
	}



function printBarcode(barcode,group){
 $.get('showbarcode.php',{ code: barcode,group: group }, function(data) {
//   $("a.barcode").click();
    var thePopup = window.open( $("a.barcode").attr('href'), "barcode", "menubar=0,location=0,height=700,width=700" );
    //$('#popup-content').clone().appendTo( thePopup.document.body );
    thePopup.print();
 });
}


function displayDialog(lnk)
	{
	var url = lnk;
	url = url;
	jQuery.facebox({ ajax: url });	
	}
<?php $ajax->showJs(); ?>
</script>
<?php 			 
  if($_GET['mode']=="showAgency")
  {
  displayAgency($_GET['origunit'],$_GET['origid']);
  exit(1);
  }		 
 function displayAgency($t_intOriginUnit,$t_intOriginId)
 {
 //echo "origin".$t_intOriginId;
 $objInc = new Incoming;
 $arOrigin=$objInc->getOriginOffice("");
  ?>
  <script type="text/javascript">
 var arOrigin = new Array(<? echo sizeof($arOrigin);?>);
	  <?
  	  $intColumnNum=sizeof($arOrigin[0])/2;
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  echo "arOrigin[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {
	  	  for($x=0;$x<$intColumnNum;$x++)
		  {
	  		echo "arOrigin[".$i."][".$x."]='".$arOrigin[$i][$x]."';\n";
		  }
	  }?>  
  </script>
 <select name="cmbAgency" id="cmbAgency" onFocus="toggleAgency(this.form);" onchange="getPerson(this.value)" <? if($t_intOriginUnit=="2") echo "disabled=\"disabled\"";?> >
	  <option value="-1">&nbsp;</option>
	<?
				for($i=0;$i<sizeof($arOrigin);$i++) 
				{ 	?>	
					
					<OPTION value="<? echo $arOrigin[$i]['originId'];?>" <? if($arOrigin[$i]['originId']== $t_intOriginId) echo "selected"; ?>><? echo $arOrigin[$i]['officeName'];?></OPTION> 
			<?	}?>
		
</select><?
			 }
   ?>
<?

################## added by LCM #############
if($_GET['src']=="receive")
	{
	$objList->markRead($_GET['id'],$objIncoming->get('userID'));
	}
$hashId = ($_GET['src'] == 'receive')?'doclist':'inc';
    
##############################################

if($_GET['mode']=="") $arrFields = $objIncoming->getUserFields();
else $arrFields = $objIncoming->getUserFields1();

################################
#
#  modes: new - for new incoming document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################

if($_REQUEST["mode"]=="getGroup")
	{	
		$tmpGroupCode=$_REQUEST["officeGroup"];
		$arEmp=$objIncoming->getOfficeGroup($_REQUEST["officeCode"]);
		echo '<select id="cmbGroupCode" name="cmbGroupCode">';
		echo "<option value=''> </option>";
		$c2 = sizeof($arEmp);
		for($i2=0;$i2<$c2;$i2++)
		{
		?>		
			<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
		<?
		}
		echo "</select>";
	die();
	}


if($arrFields['mode']=='edit' || $arrFields['mode']=='view')
{
$rsDocument= $objIncoming->getDocument($arrFields['id']);
$t_strNewId = $rsDocument[0]['documentId'];
$dateReceived =    $rsDocument[0]['dateReceived'];
$documentDate =    $rsDocument[0]['documentDate'];
$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
$t_strSubject =    $rsDocument[0]['subject'];
$t_intOriginId =   $rsDocument[0]['originId'];
$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
$t_intGroupCode= $rsDocument[0]['originGroupId'];
$oTag=$rsDocument[0]['originUnit'];
//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
//else 
if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
else $t_intOriginUnit=2;

$t_strSender =     $rsDocument[0]['sender'];
$deadline =         $rsDocument[0]['deadline'];
$t_intContainer =   $rsDocument[0]['fileContainer'];
$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
$t_strRemarks =    $rsDocument[0]['remarks'];
$t_intConfidential =   $rsDocument[0]['confidential'];
$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
$DocNum = $rsDocument[0]['docNum'];
$t_strReferenceId=$rsDocument[0]['referenceId'];
$disable_ID="disabled=\"disabled\"";
$nextMode="update";
//$rsFile = $objIncoming->getFiles($t_strNewId);
}

elseif ($arrFields['mode']=='save')
{
//echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
//print_r($_POST);

	$result=$objIncoming->addDocument($arrFields);
		if ($result==0)
		{
		$arrFields['mode']="new";
		$nextMode="save";
		}
		else
		{
		$nextMode="update";
		$disable_ID="disabled=\"disabled\"";
		}
	$msg = $objIncoming->getValue('msg');
	// assign saved data to view
	if($result==0) //not saved
	{
	$t_strNewId  =    $arrFields['t_strDocId'];
	$dateReceived =    $arrFields['dateReceived'];
	$documentDate =    $arrFields['documentDate'];
	$DocNum = stripslashes($arrFields['t_strDocNum']);
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    stripslashes($arrFields['t_strSubject']);
	$t_intOriginUnit = $arrFields['actionUnitSwitch'];
	if($t_intOriginUnit=="2") $t_intOriginId =  $arrFields['cmbOfficeCode'];
	else 	if($t_intOriginUnit=="3") $t_intOriginId = $arrFields['cmbAgency'];
	$oTag=	$arrFields['oTag'];
	$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
	$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
	$t_strSender =     stripslashes($arrFields['t_strSender']);
	$deadline =       $arrFields['deadline'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
	$t_strRemarks =  stripslashes($arrFields['t_strRemarks']);
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else//saved
	{
		$rsDocument= $objIncoming->getDocument($arrFields['t_strDocId']);
		$t_strNewId = $rsDocument[0]['documentId'];
		$dateReceived =    $rsDocument[0]['dateReceived'];
		$documentDate =    $rsDocument[0]['documentDate'];
		$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
		$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
		$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
		$t_strSubject =    $rsDocument[0]['subject'];
		$t_intOriginId =   $rsDocument[0]['originId'];
		$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
		$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
		$oTag=$rsDocument[0]['originUnit'];
		//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
		//else 
		if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
		else $t_intOriginUnit=2;
		
		$t_strSender =     $rsDocument[0]['sender'];
		$deadline =         $rsDocument[0]['deadline'];
		$t_intContainer =   $rsDocument[0]['fileContainer'];
		$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
		$t_strRemarks =    $rsDocument[0]['remarks'];
		$t_intConfidential =   $rsDocument[0]['confidential'];
		$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
		$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
		$DocNum = $rsDocument[0]['docNum'];
	}
}

elseif($arrFields['mode']=='update')
{
	$result=$objIncoming->updateDocument($arrFields);
	$msg = $objIncoming->getValue('msg');
	$disable_ID="disabled=\"disabled\"";
	
	if($result==0)
	{
	$arrFields['mode']="edit";
	$nextMode="update";
	$t_strNewId  =    $arrFields['t_strDocId'];
	$DocNum = stripslashes($arrFields['t_strDocNum']);
	$dateReceived =    stripslashes($arrFields['dateReceived']);
	$documentDate =    $arrFields['documentDate'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    stripslashes($arrFields['t_strSubject']);
	$t_intOriginUnit = $arrFields['actionUnitSwitch'];
	
	if($t_intOriginUnit=="2") $t_intOriginId =  $arrFields['cmbOfficeCode'];
	else 	if($t_intOriginUnit=="3") $t_intOriginId = $arrFields['cmbAgency'];
	$oTag=	$arrFields['oTag'];

	$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
	$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
	
	$t_strSender =     stripslashes($arrFields['t_strSender']);
	$deadline =       $arrFields['deadline'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
	$t_strRemarks =   stripslashes($arrFields['t_strRemarks']);
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else
	{
	$nextMode="edit";
	
	$rsDocument= $objIncoming->getDocument($arrFields['t_strDocId']);
		$t_strNewId = $rsDocument[0]['documentId'];
		$dateReceived =    $rsDocument[0]['dateReceived'];
		$documentDate =    $rsDocument[0]['documentDate'];
		$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
		$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
		$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
		$t_strSubject =    $rsDocument[0]['subject'];
		$t_intOriginId =   $rsDocument[0]['originId'];
		$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
		$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
		$oTag=$rsDocument[0]['originUnit'];
		//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
		//else 
		if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
		else $t_intOriginUnit=2;
		$t_strSender =     $rsDocument[0]['sender'];
		$deadline =         $rsDocument[0]['deadline'];
		$t_intContainer =   $rsDocument[0]['fileContainer'];
		$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
		$t_strRemarks =    $rsDocument[0]['remarks'];
		$t_intConfidential =   $rsDocument[0]['confidential'];
		$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
		$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
		$DocNum = $rsDocument[0]['docNum'];
	$t_intConfidential = ($t_intConfidential == '1')?'1':'0';
	if($t_intConfidential==1) $objOutgoing->transferFile($t_strNewId,$t_intConfidential);  
	}

}
else //New
{
	$t_strNewId=$objIncoming->getNewId();
	$t_intDocTypeId=-1;
	$t_intOriginId=-1;
	$dateReceived = date("Y-m-d");
	$disable_ID="";
	$nextMode="save";
}

$arDocType=$objIncoming->getDocType("");
$arContainer=$objIncoming->getContainer($objIncoming->get("office"),"");
$arOrigin=$objIncoming->getOriginOffice("");
$arOffices=$objIncoming->getOfficeFromExeOffice();
$arManagedOffice=$objIncoming->getManagedOffice($objIncoming->get('empNum'));

?>
 	<? if($msg<>""){
		//echo "messge=".$msg."<br>".strpos($msg,"DDuplicate entry");
		if(strpos($msg,"Duplicate entry")!==false){
		?>
		 <script type="text/javascript">
		 var currentID = $('#t_strDocId').val().split('-');
		 currentID[2] = pad(5, (parseInt(currentID[2])+1), '0');
		 var newID = currentID.join('-');
		 $('#t_strDocId').val(newID);
		 $("form#frmIncoming").submit();
		 //if(confirm("Document ID already existing. Get new ID?")==true){
		 //    $("#idcontainerincoming").load("showIncoming.php?mode=new #incidcontainerincoming");
		 //}	
		 function pad(w, str, pad) { 
			return (w <= str.length) ? str : pad(w, pad + str, pad )
		 }	 
		 </script>
		<?
		}
		else{
	?>
		<div class="pane">
			<div class="ui-widget" style="width:40%">
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
					<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Notice: </strong><? echo $msg;?></p>
				</div>
			</div>
			</div>
		<script type="text/javascript">
			setTimeout("disappear()",2000);
			$(function(){
			  $.ajax({ 
				url: "manageBarcodeConf.php",
				data: "mode=select&employee_num=<? echo $objList->get('userID'); ?>",
				success: function(data){ 
				 if(data==1){ printBarcode("<? echo $t_strNewId;?>","<? echo $group[0]; ?>"); }
				}
			  });
			});
		</script>
	<? }} ?>
	<? if($arrFields["mode"]=="save" || $arrFields["mode"]=="update"|| $arrFields["mode"]=="view"){	?>
	<table align="center" width="605px" class="documentContainer">
	<tr><td>
	<table align="center" width="605px" class="datawrap">
	
	<tr><td>
	<table align="center" width="600px" class="metadata">
	<tr class="metabutton">
	  <td colspan="2" class="containerlabel">Document Details</td>
	  <td style="text-align:right" colspan="2">
    <? if($objIncoming->isCustodian($objIncoming->get("empNum"),$addedByOfficeId) && $arrFields['edit']!="false"){ ?>
	<input style="cursor:pointer" type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showIncoming.php?mode=edit&id=<? echo $t_strNewId;?>&src=<?php echo $arrFields['src']; ?>','<?=$hashId; ?>');" class="btn" />&nbsp;	
	<? //checkNodeStatus('showIncoming.php?mode=edit&id=<? echo $t_strNewId;','inc',' echo HTTP.constant($_SESSION['office']).'/'; ','1'); ?> 
	<input style="cursor:pointer" type="button" name="addForm" value="Add New" onClick="getData('showIncoming.php?mode=new','<?=$hashId; ?>');" class="btn" />
	<input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />	
	<?php } 
     else if($objIncoming->get('office') === 'RMS' && $objIncoming->get('userType')===1){ ?>
    <input style="cursor:pointer" type="button" id="btnEditDocNo" value="Edit Doc No." onClick="editDocNo('<?=$t_strNewId;?>','<?=$DocNum; ?>','<?=$hashId ?>');" class="btn"/>&nbsp;	
    <?php } ?>
    
	</td></tr>
	<tr>
		<th width="100px">Document ID :</th>
		<td width="200px"><? echo $t_strNewId;?></td>
		<th width="120px">Document Type :</th>
		<td width="180px"><? echo $rsDocTypeAbbrev[0]['documentTypeAbbrev'];?></td> 
	</tr>
	<tr>
		<th width="97">Document Date :</th>
		<td><? echo $documentDate;?></td>
		<th>Date Received :</th>
		<td><? echo $dateReceived;?></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<th>Doc No. : </th>
		<td id="docNoViewMode"><? echo $DocNum;?></td>
		<td></td>
		<td></td>		
	</tr>

	<tr>
		<th>Subject : </th>
		<td></td>
		<td></td>
		<td></td>		
	</tr>
	
	<tr>
	<th></th>
	<td colspan="2"><? echo $t_strSubject;?></td>
	<td></td>
	</tr>
	
	<tr>
		<th>Origin : </th>
		<td><? if($t_intOriginUnit=="2") echo $rsOriginDetails[0]['oName'];
				else echo $rsOrigin[0]["officeName"];
				?></td>
		<th width="97">Sender :</th>
		<td><? echo $t_strSender;?></td>
	</tr>
	
	<tr>
		<th>Deadline : </th>
		<td><? echo $deadline;?></td>
		<th>Remarks :</th>
		<td rowspan="2"><? echo $t_strRemarks;?></td>
	</tr>
	
	<tr>
		<th>File Container : </th>
		<td><? echo $rsContainer[0]['label'];?></td>
		<td></td>
	</tr>
	 <tr><th>Related Docs:</th><td colspan="3"></td></tr>
					 <tr><td>&nbsp;</td><td colspan="3"><div class="relatedDocs"><?
						$arrRelated=$objIncoming->getRelatedDocs($t_strNewId);
						$cRelated=count($arrRelated);
						if($cRelated>0) {
							$intStatus = $objIncoming->getDocumentStatus($arrRelated[0]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo "<a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[0]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[0]["documentId"]."</a>";
							}
						for($cntRelated=1;$cntRelated<$cRelated;$cntRelated++)
						{
						
						$intStatus = $objIncoming->getDocumentStatus($arrRelated[$cntRelated]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo ", <a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[$cntRelated]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[$cntRelated]["documentId"]."</a>";
						
						    //echo ", ".$arrRelated[$cntRelated]["documentId"];
						}
						?>
						</div>
					 </td></tr>
		<tr>
			<th>Owned by : </th>
			<td><? echo $addedByOfficeId;?></td>
			<th>&nbsp;</th>
			<td rowspan="2">&nbsp;</td>
		</tr>
	 <tr>
						<td colspan="4">&nbsp;</td>
					  </tr>
	<? if ($t_intConfidential)
	{
	?>
	<tr>
		<td colspan="4" style="text-align:center"><div class="ui-widget" align="center">
				<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; width:140px;"> 
					<p align="left"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
					<strong>Alert:</strong> Confidential.</p>
				</div>
			</div></td>
	</tr>
	<tr>
	<td colspan="4">&nbsp;</td>
	</tr>
	<?
	}
	?>
	
	</table>  <!-- end of metadata -->
	
	</td></tr></table> <!-- end of datawrap -->
	
	</td></tr>
	<tr><td><br /></td></tr>
	<tr><td>
	
<table class="datawrap" width="605px" >
	<tr><td>
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="incomingfilelinkimg" /><a onClick="javascript:toggleWindowAjax2('filebodyincoming','incomingfilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyincoming','addInc','<? echo $addedByOfficeId; ?>');">Manage Files</a><div id="loading_widget" style="float:right; display:none;">LOADING&nbsp;<img src="css/images/ajax-loader.gif" /></div></td>
		<td><div id="addInc"></div></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
		<div id="filebodyincoming" style="display:none">
		<!-- ############################# File Content #####################################-->

		<!-- #################################################################################-->
		</div>		
		</td></tr>
	    </table></td></tr>

	</table>

	</td></tr>
	<tr><td>	<table class="datawrap" width="605px" >
	<tr><td>
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="incominghistorylinkimg" /><a href="javascript:getData('showAction.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=docubodyincoming&historydiv=actionbodyincoming','docubodyincoming');" onClick="return toggleWindowAjax('docubodyincoming','incominghistorylinkimg');" id="incominghistorylink">Update Action</a></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
			<!-- 	***************************		history				********************************************	-->
			<div id="docubodyincoming" align="center" style="visibility:hidden">
		
			</div>
			<!-- 	***************************		upload image	    ********************************************	-->	
		</td>
	</tr>
	</table>
		</td>
	</tr>
	</table>
	</td></tr>
	<tr><td>
	<table class="datawrap" width="605px" >
	<tr><td>
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="incomingactionlinkimg" /><a href="javascript:getData('showHistory.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=actionbodyincoming','actionbodyincoming');" onClick="return toggleWindowAjax('actionbodyincoming','incomingactionlinkimg');" id="incomingactionlink">View History</a></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
			<div id="actionbodyincoming" align="center" style="visibility:hidden">
			</div>
		</td>
	</tr>
	</table>
		</td>
	</tr>
	</table>
	
	</td></td>
	
	</table>	
	<?
	}
	else   // MODE = new or edit
	{
	/*if($objIncoming->get('office')=='ITD'){
	print_r($arOrigin);
	}*/
	?>

	 <script language="javascript" type="text/javascript">
	  var arOrigin = new Array(<? echo sizeof($arOrigin);?>);
	  <?
  	  $intColumnNum=sizeof($arOrigin[0])/2;
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  echo "arOrigin[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {
	  	  for($x=0;$x<$intColumnNum;$x++)
		  {
	  		echo "arOrigin[".$i."][".$x."]='".$arOrigin[$i][$x]."';\n";
		  }
	  }
      //echo "alert(arOrigin[0][0]);";
	  ?>
		function getPerson(i)
		{
		if(i==-1)  document.getElementById("t_strSender").value="";
		for(x=0;x<arOrigin.length;x++)
		{
		if(i==arOrigin[x][0]) 
			{
				document.getElementById("t_strSender").value=arOrigin[x][4];
				document.getElementById("oTag").value="agency";
			}
		}
		
		}
		
		 var arOriginOutgoing = new Array(<? echo sizeof($arOffices);?>);
	  <?

	  for($i=0;$i<sizeof($arOffices);$i++)
	  echo "arOriginOutgoing[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOffices);$i++)
	  {
	  	echo "arOriginOutgoing[".$i."][0]='".$arOffices[$i]['officeCode']."';\n";
	  	echo "arOriginOutgoing[".$i."][1]='".$arOffices[$i]['officeName']."';\n";
	  	echo "arOriginOutgoing[".$i."][2]='".$arOffices[$i]['empNumber']."';\n";
		echo "arOriginOutgoing[".$i."][3]='".$arOffices[$i]['head']."';\n";
		echo "arOriginOutgoing[".$i."][4]='".$arOffices[$i]['title']."';\n";
		echo "arOriginOutgoing[".$i."][5]='".$arOffices[$i]['officeTag']."';\n";
	  }

	  ?>
		function getPerson2(i)
		{
		if(i==-1)  document.getElementById("t_strSender").value="";
		for(x=0;x<arOriginOutgoing.length;x++)
		{
		if(i==arOriginOutgoing[x][0]) 
			{
				document.getElementById("t_strSender").value=arOriginOutgoing[x][3];
				document.getElementById("oTag").value=arOriginOutgoing[x][5];
				//document.getElementById("cmbOriginOutgoing").title=arOriginOutgoing[x][1];;	
			}
		}
		getData("showIncoming.php?mode=getGroup&officeCode="+i+"&officeGroup=<? echo $t_intGroupCode;?>","incGroupDiv");
		}
		    function x () {
                var oTextbox = new AutoSuggestControl(document.forms['frmIncoming'].t_strDocType, new RemoteStateSuggestions());        
            } 
			//x();
			/*
			<? if($t_intOriginUnit=="2"){?>
			$("#cmbOfficeCode").change();
			<? }
			else {?>
			$("#cmbAgency").change();
			<? }?>
			*/
			$("#documentOwner").change(function(){
			var src = $(this).val();
			$("#idcontainerincoming").load("showIncoming.php?getOfficeID="+src+ " #idcontainerincoming",function(){
			 	alert("Please save again");
			 });
			});
	  </script>
<form action="javascript:check(document.getElementById('frmIncoming'),'<?=$hashId; ?>','showIncoming.php');" name="frmIncoming" id="frmIncoming" onsubmit="return checkConfi(document.getElementById('frmIncoming'));">

<table align="center" width="700px" class="tblforms" >	

  <tr> 
    <th width="97">Document ID </td>
    <td width="180">
		<div id="idcontainerincoming">
		<input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required" <? echo $disable_ID;?> title="required" style="width:150px" ></div>
		<input type="hidden" name="mode" value="<? echo $nextMode;?>">
	</td>
    <th width="150">Date Received</td>
      <td width="150">
	  	<input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d no-animation ?>" id="dateReceived" name="dateReceived" value="<? echo $dateReceived;?>"></td>
  </tr>
  <tr> 
    <th>Document Type</td>
    <td><table><tr>
		<td><div id="divDocType"><select name="cmbDocType" id="t_strDocType" class="caption" style="width:250px">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeDesc'];?></option>
	  <?
	  }
	  ?>
        </select>
		<!--input type="text" name="t_strDocType" id="t_strDocType" value="<? echo $t_strDocType;?>" autocomplete=OFF /-->
        </div>
        </td>
		<td>
		<? if($objIncoming->get('userType')==1) {?>
		<span class='ui-icon ui-icon-plus' title='Add New Document Type' onclick="displayDialog('showLibrary_popup.php?mode=doctype');"></span><? }?>
		</td></tr></table>
		<!--
		<INPUT onblur="clearSuggest()"; 
onkeydown="return tabfix(this.value,event,this);" id=s 
onkeyup="suggest(this.value,event);" type=text> </P>
<P class=nodisplay><LABEL>kIndex</LABEL> <INPUT id=keyIndex class=nodisplayd 
type=text> </P>
<P class=nodisplay><LABEL>rev</LABEL> <INPUT id=sortIndex class=nodisplayd 
type=text> </P>
<DIV id=results></DIV>
		--></td>   
    <th>Document Date</td>
    <td><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="documentDate" name="documentDate" value="<? echo $documentDate;?>"></td>
  </tr>
   <tr> 
      	<th>Reference ID</td>
      	<td><input type="text" name="t_strReferenceId" id="t_strReferenceId" value="<? echo $t_strReferenceId;?>"></td>
      	<td><!--DWLayoutEmptyCell-->&nbsp;</td>
      	<td><!--DWLayoutEmptyCell-->&nbsp; </td>
      
    </tr>
  <tr>
  <th>Doc No.</td>
  <td colspan="2" ><input type="text" name="t_strDocNum" id="t_strDocNum" value="<? echo $DocNum;?>" maxlength="15" /><span class="required" style="text-transform:none">&nbsp;(i.e. AO 001)</span></td>
  <td><span class="required" >&nbsp;</span></td>
  </tr>
  <tr> 
    <th>Subject</td>
    <td colspan="3"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3" title="required"><? echo $t_strSubject; ?></textarea></td>
  </tr>
    <tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><br /><br /></td>  
  </tr>
  <tr> 
   <th valign="top">Origin</th>
   <td colspan="3"></td>
   </tr>
   <tr>
   	<td></td>
	<td colspan="3"><table width="100%"  border="0">
			  <tr>
				<td width="21%" height="23" align="left">
				<input type="hidden" name="oTag" id="oTag" value="<? echo $oTag;?>"/>
				<input name="actionUnitSwitch" type="radio" value="2" onClick="toggleOffice(this.form);" <? if($t_intOriginUnit=="2") echo "checked=\"checked\"";?>> Office:</td>
				<td width="79%"> <SELECT name="cmbOfficeCode" id="cmbOfficeCode" onClick="toggleOffice(this.form);" onchange="getPerson2(this.value)" style="width:320px" <? if($t_intOriginUnit=="3") echo "disabled=\"disabled\"";?>>
	<OPTION value="-1">&nbsp;</OPTION>
	 <? $sOffice=sizeof($arOffices);
		  for($i=0;$i< $sOffice;$i++)
		  {    
		  ?>
	  <option value="<? echo $arOffices[$i]['officeCode']; ?>" <? if($arOffices[$i]['officeCode']== $t_intOriginId) echo "selected"; ?> title="<? echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);?>"> <? echo $arOffices[$i]['officeName'];?></option>
	  <?
	  }
	  ?>
		</SELECT>
		</td>
			  </tr>
			  <tr>
			  	<td style="padding-left:2em;">Group</td>
				<td><div id="incGroupDiv"><select id="cmbGroupCode" name="cmbGroupCode"></select></div></td>
			  </tr>
			</table>
		<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="3"  onClick="toggleAgency(this.form);" <? if($t_intOriginUnit=="3") echo "checked=\"checked\"";?>>
				  Agency Name: </td>
				<td width="79%">
				<? 
function multi_implode($glue, $pieces)
{
    $string='';
    
    if(is_array($pieces))
    {
        reset($pieces);
        while(list($key,$value)=each($pieces))
        {
            $string.=$glue.multi_implode($glue, $value);
        }
    }
    else
    {
        return $pieces;
    }
    
    return trim($string, $glue);
}
				
				//print_r($arOrigin);
				//$tmp = multi_implode(':',$arOrigin);
				//$tmp = serialize($arOrigin);
				//print_r( $arOrigin);
				for($i=0;$i<sizeof($arOrigin);$i++)
					$tmp = $tmp.$arOrigin[$i]['originId']."|".$arOrigin[$i]['officeName']."|".$arOrigin[$i]['contact']."|".$arOrigin[$i]['address']."|".$arOrigin[$i]['contactPerson']."|".$arOrigin[$i]['ownerOffice'].",";
				//echo $tmp;
				?>
			
			<table><tr>
		<td><div id="divAgency"> <? displayAgency($t_intOriginUnit,$t_intOriginId);?> </div></td>
		<td><span class='ui-icon ui-icon-plus' title='Add New Agency' onclick="displayDialog('showLibrary_popup.php?origunit=<? echo $t_intOriginUnit;?>&arorig=<? echo $tmp;?>&origid=<? echo $t_intOriginId;?>&mode=agency');"></span></td></tr></table>
			
			</td>
			  </tr>
			</table>
	</td>
	
	</tr>

	<tr>
    <th valign="top">Sender</td>
    <td valign="top"><input type="text" size="30" class="caption" name="t_strSender" id="t_strSender" value="<? echo $t_strSender;?>"></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
 	<th valign="top">Deadline</td>
    <td valign="top"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="deadline" name="deadline" value="<? echo $deadline;?>"> </td>
	<th rowspan="2">Remarks</td>
    <td rowspan="2" valign="top"><textarea cols="25" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"><? echo $t_strRemarks;?></textarea></td>
  </tr>
  <tr>
      <th valign="top">File Container</td>
      <td valign="top">
	  <select name="t_intContainer" class="caption">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
	  <?
	  }
	  ?>
        </select>
	  </td>
  </tr>
  <tr> 
      <th valign="top">Confidential</td>
      <td valign="top"><input type="checkbox" value="1" name="t_intConfidential" id="t_intConfidential" <? 
	  if ($t_intConfidential=='1') echo "checked=\"checked\"";
	  ?>></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
  </tr>
  <tr> <!--$arManagedOffice=$objIncoming->getManagedOffice();-->
      <th valign="top">Document Owner</td>
      <td valign="top"><select id="documentOwner" name="documentOwner" <? echo $disable_ID;?>>
	   <?
	  for($i=0;$i<sizeof($arManagedOffice);$i++)
	  {
	  $managedOffice=(trim($arManagedOffice[$i]["groupCode"])=="")?$arManagedOffice[$i]["officeCode"]:$arManagedOffice[$i]["groupCode"];
	  ?>
	  <option value="<? echo $managedOffice; ?>" <? if($managedOffice== $t_strManagedOffice) echo "selected"; ?> > <? echo $managedOffice;?></option>
	  <?
	  }
	  ?>
	  </select></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
  </tr>
  
  <!--
  <tr> 
  <td height="24">Action to be Taken</td>
      <td><select name="select5" class="caption">
          <option selected value="0">for Info</option>
		  <option  value="1">for Signature</option>
          <option value="2">for Comments</option>
        </select></td>
      <td height="24">Action Unit</td>
    <td><select name="select6[]" multiple class="caption">
          <option selected value="0"></option>
		  <option value="1">OASEC-FALA</option>
          <option value="2">OSEC</option>
          <option value="3">OASECST</option>
		  <option value="4">Abuel, Francis</option>
		  <option value="5">Dotimas, Marilen</option>
		  <option value="6">Monroyo, George</option>
        </select></td>
  </tr> -->
  
  <tr><td colspan="4" style="text-align:center">
  <? if ($arrFields['mode'] == "new"){
  ?><input type="submit" value="Save" class="btn" onclick="">
  <?
  }
  else{
  ?><input type="submit" value="Update" class="btn" onclick="">
  <?
  }
  /*
  elseif ($arrFields['mode'] == "save"){
  ?><input type="submit" value="Update" class="caption" onclick="">
  <?
  }
  */
  ?>
  <input type="button" value="Cancel" class="btn" onclick="getData('showIncoming.php?mode=view&id=<? echo $t_strNewId;?>','inc');" />
  <input type="reset" value="Clear" class="btn"></td></tr>
</table>
</form>

<? } ?>
<a class='barcode' href="showbarcode.php?code=<? echo $t_strNewId;?>"></a>
