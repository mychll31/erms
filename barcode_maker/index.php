<style type="text/css">
form#two {background:#dee; width:470px; padding:10px; border:1px solid #000; margin:0 auto;}
form#two fieldset {width:450px; display:block; border:1px solid #fff; padding:5px; font-family:verdana, sans-serif; margin-bottom:0.5em; line-height:1.5em;}
form#two fieldset#personal {background:transparent url(cssplay/crutch.gif) no-repeat 400px 10px;}
form#two fieldset#medical {background:transparent url(cssplay/sling.gif) no-repeat 400px 10px;}
form#two fieldset#current {background:transparent url(cssplay/cold.gif) no-repeat 390px 10px;}
* html form#two fieldset#personal, * html form#two fieldset#medical {background-position:400px 20px;}
* html form#two fieldset#current {background-position:390px 20px;}
form#two fieldset#opt {background:#dee;}
form#two legend {font-family:georgia, sans-serif; font-size:1.1em; font-weight:bold; border:3px solid #fff; margin-bottom:5px; padding:3px; width:254px; background:#fff url(cssplay/form.gif) repeat-x center left;}
form#two label {clear:left; display:block; float:left; width:200px; text-align:right; padding-right:10px; color:#888; margin-bottom:0.5em;}
form#two input {border:1px solid #fff; background:#fff url(cssplay/form.gif) repeat-x top left; padding-left:0.5em; margin-bottom:0.6em;}
form#two select {margin-left:0.5em;}
form#two textarea {width:410px; height:15em; border:1px solid #fff; padding:0.5em; overflow:auto; background:#fff url(cssplay/form.gif) repeat-x bottom left;}
form#two option {background:#788; color:#fff;}
form#two optgroup {background:#abb; color:#000; font-family:georgia, serif;}
form#two optgroup option {background:#9aa; color:#fff;}
form#two #button1, form#two #button2 {color:#c00; padding-right:0.5em; cursor:pointer; width:205px; margin-left:8px;}
form#two #button1:hover, form#two #button2:hover {background-position:center left; color:#000;}
</style>
<form id="two" name="_frm_barcode" action="_show_barcode.php" method="post">
  <fieldset id="personal">
    <legend>BARCODE INFORMATION</legend>
	<label for="barcode_title">Barcode Type</label>   	 
	 <select name="barcode_title">
	 <optgroup label="Group 1">	 
	  <option value="PO">Purchase Order</option>
	  <option value="PR">Purchase Request</option>
	  <option value="RR">Request for Repair</option>  
	  <option value="WO">Work Order</option>  
	  <option value="AC">Agency Control</option>
	  <option value="CN">CONTRACT</option> 
	  <option value="APP">APP</option>    
	  <option value="PPMP">PPMP</option>    
	  <option value="APR">APR</option>       
      </optgroup>
	  <optgroup label="Group 2">
	  <option value="DV">Disbursement Voucher</option>    
	  </optgroup>	  
	  <optgroup label="Group 3">
	  <option value="FC">Fleef Card</option>    
  	  <option value="JS">Janitorial Services</option>    	  
  	  <option value="SS">Security Services</option>    	  	  
	  </optgroup>
	  <optgroup label="Group 4">
	  <option value="TT">Trip Ticket</option>    
	  </optgroup>	  
	 </select><br /><br />

     <label for="month">Month: </label> 
	 <select name="month">
	  <option value="01">January</option>	  
  	  <option value="02">February</option>
	  <option value="03">March</option>	  
  	  <option value="04">April</option>
	  <option value="05">May</option>	  
  	  <option value="06">June</option>
	  <option value="07">July</option>	  
  	  <option value="08">August</option>
	  <option value="09">September</option>	  
  	  <option value="10">October</option>
	  <option value="11">November</option>	  
  	  <option value="12">Decemeber</option>
	 </select><br /><br />
	 	 
    <label for="qty">quantity: </label> 
    <input name="qty" id="qty" type="text" tabindex="1" />
    <br />
    <label for="l_num">last number: </label>
    <input name="l_num" id="l_num" type="text" tabindex="2" />
 
</fieldset>
  <p>
  <button onClick="printBarcode();">Print Barcode</button>
  </p>
</form>
