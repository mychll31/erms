<?php
require('../class/barcode/code128.php');

$title = $_REQUEST['barcode_title'];
$last_num=$_REQUEST['l_num'];
$qty = $_REQUEST['qty'];
$month = $_REQUEST['month'];
//$title=($title=='RFR')?'RR':($title=='CONTRACT')?'CN':$title;
$year = date('y');
//$month=date('m');

$header='Department of Science and Technology';
$header2='Central Office';
$header3=($title=='FC' || $title=='JS' || $title=='SS'?'General Service Section (GSD)':
           ($title == 'TT'?'Motorpool Section (GSD)':'Procurement Section (GSD)'));
$pdf=new PDF_Code128();
$pdf->SetAutoPageBreak(true,1);


for($i=0;$i<$qty;$i++){
    $pad = $title=='TT'?4:3;
    $ctr = str_pad($last_num++, $pad, "0", STR_PAD_LEFT);
	$mm = $title=='TT' || $title=='DV'?"":"{$month}-";
	$str_barcode = "$title-{$year}-$mm{$ctr}";
	
	$pdf->AddPage('L','Legal');
	$pdf->SetFont('Arial','',50);
	//$pdf->Image('../images/logo.jpg',4,10,20);
	$pdf->SetXY(25,20);
	$pdf->Write(0,$header);
	$pdf->SetXY(70,38);
	$pdf->Write(0,$header3);
//	$pdf->SetXY(70,55);
//	$pdf->Write(0,$header3);
	
	$pdf->Code128(2,50,$str_barcode,354,95);
	$pdf->SetXY(88,127);
	$pdf->SetFont('Arial','',90);
	$pdf->Write(85,$str_barcode);
}
$pdf->Output();
?>