<?php
session_start();
include_once("class/Employee.class.php");
$objEmployee = new Employee;
?>
<script type="text/javascript" src="js/multiple_function_emp.js"></script>
<script type="text/javascript">
 <?php 
  echo ($objEmployee->get("blnAgencyUser") && $_REQUEST['menu'] == 'docs')?
      "getEmpDocList('showEmpDocList.php?mode=show&DOCID=".$_REQUEST['DOCID']."&ACTION=".$_REQUEST['ACTION']."&menu=docs','doclist');":
        "getEmpDocList('showEmpIncomingList.php?mode=show&DOCID=".$_REQUEST['DOCID']."&ACTION=".$_REQUEST['ACTION']."&menu=docs','doclist');";
 ?>
 $(function(){
  var tabs = $('#tabs'),
  tab_a_selector = 'ul.ui-tabs-nav a';
  tabs.tabs({ event: 'change' });
  
  tabs.find( tab_a_selector ).click(function(){ 
   var state = {},
   id = $(this).closest( '#tabs' ).attr( 'id' ),
   idx = $(this).parent().prevAll().length;
   state[ id ] = idx;
   $.bbq.pushState( state ); 
   $selectfromid=false;
  });
  
 $(window).bind( 'hashchange', function(e) { 
   tabs.each(function(){  
    var idx = $.bbq.getState( this.id, true ) || 0; 
	idx = ($selectfromid)?$selectedtab:idx; 
    $(this).find( tab_a_selector ).eq( idx ).triggerHandler( 'change' );
	$selectfromid=false;
   });
 })
 
$(window).trigger( 'hashchange' );
});
function doAgencytDelete(ID)
{
	var docID = ID;
	var msg = confirm("Are you sure you want to delete this Document with Document Code : "+docID);
	if(msg)
		getData('showEmpDocList.php?mode=delete_true&id='+docID,'doclist');	
}


</script>
    <td width="764" valign="top">
		<div id="tabs">
			<ul>
                <?php if($_REQUEST['menu']=='rdocs') { ?>            
				<li><a href="#doclist" onClick="getEmpDocList('showEmpIncomingList.php?mode=show','doclist');">Received Documents</a></li>
                <?php } ?>
                <?php if($objEmployee->get("blnAgencyUser") && $_REQUEST['menu']=='docs') { ?>
				<li><a href="#doclist" onClick="getEmpDocList('showEmpDocList.php?mode=show','doclist');">Documents</a></li>				
		  <li><a href="#outg" onClick="getEmpDocList('showOutgoing.php?mode=new','outg');">Outgoing</a></li>
		  <li><a href="#reports" onClick="getPageData('showReports.php?mode=show','reports');">Reports</a></li>
                <?php } ?>
			</ul>
			<div id="doclist" align="center"></div>
            <?php if($objEmployee->get("blnAgencyUser") && $_REQUEST['menu']=='docs') { ?>            
			<div id="outg" align="center"></div>
			<div id="reports" align="center"></div>	
            <?php } ?>            
		</div>	
	</td>
