<?php
session_start();

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=MasterListofMaintainedDocuments.xls");
// header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

// include_once("class/Records.class.php");
include_once("class/General.class.php");
include_once("class/DocDetail.class.php");
include_once("class/Report.class.php");

// $objRecordDetail = new Records;
$objDocDetail = new DocDetail;
$objGeneral = new General;
$objReport = new Report;

$officename = $objGeneral->getOfficeNameBYAbbv($_SESSION['office']);
$empDetails = $objGeneral->getEmpDetails($_SESSION['empNum']);

$fullname = sprintf("%s %s. %s %s", $empDetails['fname'], $empDetails['minitial'], $empDetails['lname'], $empDetails['nameExtension']);
$docs = $objReport->getReportDocumentMLMD($_GET["dateFrom"], $_GET["dateTo"]);
?>

<style type="text/css">
    th {
        background-color: #ccc;
    }
</style>

<br><br>
<center>
    <b><u>MASTERLIST OF MAINTAINED DOCUMENTS</u></b><br>
    Department of Science and Technology - Central Office<br>
    <b><?=$officename?></b><br>
    As of<br><?="&nbsp;".date("m/d/Y", strtotime($_GET["dateFrom"]))." to ".date("m/d/Y", strtotime($_GET["dateTo"]))."&nbsp;"?>
</center>
<br>

<table border=1>
    <thead>
        <tr>
            <th>ITEM NO.</th>
            <th>DOC CODE</th>
            <th>DOCUMENT TITLE</th>
            <th>COPY NO.</th>
            <th>DATE OF ISSUANCE</th>
            <th>COPY HOLDER/S</th>
            <th>LOCATION</th>
            <th>PERSON/ UNIT RESPONSIBLE</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($docs as $doc): ?>
        <tr>
            <td><?=$no++?></td>
            <td><?=$doc["documentId"]?></td>
            <td><?=$doc["subject"]?></td>
            <td><?=$doc["copyno"]?></td>
            <td><?=date('Y-m-d', strtotime($doc["dateofissue"]))?></td>
            <td><?=$doc["copyholder"]?></td>
            <td><?=$objReport->getDrawerLabels($doc["cabinetId"],$doc["drawerId"],$doc["containerId"])?></td>
            <td><?=$doc["personUnitResponsible"]?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr> 
            <td colspan="4" valign="top">
                PREPARED BY: <br><br>
                <center>
                    <u><b><?=strtoupper($fullname)?></b></u><br>
                    <?=$empDetails['positionDesc']?>
                </center>
            </td>
            <td colspan="4" valign="top">
                NOTED BY: <br><br>
                <center>
                    <u><b><?=strtoupper($_GET["nname"])?></b></u><br>
                    <?=$_GET['npos']?>
                </center>
            </td>
        </tr>
    </tfoot>
</table>