<?php
@session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/ManageContainer.class.php");

$list = new ManageContainer;
if(intval($_GET['cabinetid']) > 0){
	if(isset($_GET['drawerid']))
		$selectedcont = $_GET['drawerid'];

	if($_GET['getFile']=='drawer'){
		$drawers = $list->getDrawer($_GET['cabinetid']);
			echo '<option value=-1> </option>';
		foreach($drawers as $drawer){
			if($selectedcont==$drawer['container2Id'])
				echo '<option selected value="'.$drawer['container2Id'].'">'.$drawer['label'].'</option>';
			else
				echo '<option value="'.$drawer['container2Id'].'">'.$drawer['label'].'</option>';
		}
	}
}

if(intval($_GET['drawerid']) > 0){
	$selectedcont = '';
	if(isset($_GET['contid']))
		$selectedcont = $_GET['contid'];

	if($_GET['getFile']=='container'){
		$containers = $list->getContainer($_GET['drawerid']);
		echo '<option value=-1> </option>';
		foreach($containers as $container){
			if($selectedcont==$container['container3Id'])
				echo '<option selected value="'.$container['container3Id'].'">'.$container['label'].'</option>';
			else
				echo '<option value="'.$container['container3Id'].'">'.$container['label'].'</option>';
		}
	}
}
?>