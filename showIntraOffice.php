<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Intra.class.php");
require_once("class/jqSajax.class.php");


$file=new General();
$ajax=new jqSajax();
$ajax->export("Load","file->Load");
$ajax->export("_decode","file->_decode");
$ajax->export("getTargetFile","file->getTargetFile");
$ajax->export("getTargetFile","file->removeItem");
$ajax->export("checkifConfidential","file->checkifConfidential");
$ajax->export("fileExist","file->fileExist");
$ajax->export("checkNotification","file->checkNotification");
$ajax->processClientReq();
?>	
<script type="text/javascript">
<?php $ajax->showJs(); ?>
</script>
<?
$objIntra = new Intra;
if($_GET['mode']=="") $arrFields = $objIntra->getUserFields();
else $arrFields = $objIntra->getUserFields1();

################################
#
#  modes: new - for new IntraOffice document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################

if($arrFields['mode']=='edit' ||  $arrFields['mode']=='view')
{
$rsDocument= 		$objIntra->getDocument($arrFields['id']);
$t_strNewId = 		$rsDocument[0]['documentId'];
$t_strReferenceId=	$rsDocument[0]['referenceId'];
$documentDateIntra =    	$rsDocument[0]['documentDate'];
$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
$rsDocTypeAbbrev = $objIntra->getDocType($t_intDocTypeId);
$t_strSubject =    	$rsDocument[0]['subject'];
$t_strOrigin =   	$rsDocument[0]['officeSig'];
$t_strSender = 		$rsDocument[0]['sender'];
$deadlineIntra = $rsDocument[0]['deadline'];
$t_intContainer =   $rsDocument[0]['fileContainer'];
$rsContainer = $objIntra->getContainer($objIntra->get("office"),$t_intContainer);
$t_strRemarks =    	$rsDocument[0]['remarks'];
$t_intConfidential =$rsDocument[0]['confidential'];
$DocNum = 			$rsDocument[0]['docNum'];
$disable_ID="disabled=\"disabled\"";
$nextMode="update";
$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
}
elseif ($arrFields['mode']=='save')
{
//echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
//print_r($_POST);
	$result=$objIntra->addIntraDocument($arrFields);
		if ($result==0)
		{
		$arrFields['mode']="new";
		$nextMode="save";
		}
		else
		{
		$nextMode="update";
		$disable_ID="disabled=\"disabled\"";
		}
	$msg = $objIntra->getValue('msg');
	// assign saved data to view
	$t_strNewId  =    $arrFields['t_strDocId'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];	
	$dateReceived =    $arrFields['dateReceived'];
	$documentDateIntra =    $arrFields['documentDateIntra'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIntra->getDocType($t_intDocTypeId);
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_strOrigin =       $arrFields['t_strOrigin'];
	$t_strSender =     $arrFields['t_strSender'];
	$deadlineIntra =       $arrFields['deadlineIntra'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objIntra->getContainer($objIntra->get("office"),$t_intContainer);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$addedByOfficeId = $objIntra->get("office");
	$DocNum = $arrFields['t_strDocNum'];
}
elseif($arrFields['mode']=='update')
{
	$result=$objIntra->updateIntraDocument($arrFields);
	$msg = $objIntra->getValue('msg');
	$disable_ID="disabled=\"disabled\"";
	$nextMode="update";
	$t_strNewId  =    $arrFields['t_strDocId'];
	$dateReceived =    $arrFields['dateReceived'];
	$documentDateIntra =    $arrFields['documentDateIntra'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIntra->getDocType($t_intDocTypeId);
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_strOrigin =       $arrFields['t_strOrigin'];
	$t_strSender =     $arrFields['t_strSender'];
	$deadlineIntra =       $arrFields['deadlineIntra'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objIntra->getContainer($objIntra->get("office"),$t_intContainer);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$DocNum = $arrFields['t_strDocNum'];
	$addedByOfficeId = $objIntra->get("office");
	
	if($result==1){
	 $t_intConfidential = ($t_intConfidential == '1')?'1':'0';
	 $objIntra->transferFile($t_strNewId,$t_intConfidential); }   
}
else //New
{
	$t_strNewId=$objIntra->getNewId("intra");
	$t_intDocTypeId=-1;
	$t_intOriginId=-1;
	$dateReceived = date("Y-m-d");
	$disable_ID="";
	$nextMode="save";
	$t_strOrigin=$objIntra->get("office");
	$t_strSender=$objIntra->getOfficeHead($t_strOrigin);
}

$arDocType=$objIntra->getDocType("");
$arContainer=$objIntra->getContainer($objIntra->get("office"),"");
//$arOrigin=$objIntra->getOriginOffice("");
?>
 	<? if($msg<>""){?>
			<div class="ui-widget" style="width:40%">
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
					<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Notice: </strong><? echo $msg;?></p>
				</div>
			</div>
	<? } ?>
<? if($arrFields["mode"]=="save" || $arrFields["mode"]=="update" || $arrFields["mode"]=="view"){
	?>
	<table align="center" width="605px" class="documentContainer">
	<tr><td>
		<table align="center" width="605px" class="datawrap">
			<tr><td>
				<table class="metadata">
				<tr class="metabutton">
					<td colspan="2" class="containerlabel">Document Details</td>
					<td colspan="2" style="text-align:right">
					<? if($addedByOfficeId == $objIntra->get("office")){
	?>
					
					<input type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showIntraOffice.php?mode=edit&id=<? echo $t_strNewId;?>','intra');" class="btn"/>&nbsp;	
					<? if ($objIntra->get("office")=="RMS") 
			{
		?> 
			<input type="button" name="show201" id="show201" value="update 201" onClick="show201Dialog('<? echo $t_strNewId;?>')" class="btn" />
		<? 
			} 
		?> 
					
					<input type="button" name="addForm" value="Add New" onClick="getData('showIntraOffice.php?mode=new','intra');" class="btn"/><? }?></td>
				</tr>
				  <tr>
					<th width="100px">Document ID : </th>
					<td width="200px"><?php echo $t_strNewId; ?>&nbsp;</td>
					<th width="100px">Document Type : </th>
					<td width="200px"><?php echo $rsDocTypeAbbrev[0]['documentTypeAbbrev']; ?>&nbsp;</td>
				  </tr>
				  <tr>
					<th>Deadline : </th>
					<td><? echo $deadlineIntra;?></td>
					<th>Document Date : </td>
					<td><?php echo $documentDateIntra; ?>&nbsp;</td>
				  </tr>
				  <tr>
					<th >Subject : </th>
					<td><!--DWLayoutEmptyCell-->&nbsp;</td>
					<td><!--DWLayoutEmptyCell-->&nbsp;</td>
					<td><!--DWLayoutEmptyCell-->&nbsp;</td>
				  </tr>

				  <tr>
					<th></th>
					<td colspan="3"><?php echo $t_strSubject; ?>&nbsp;</td>
				  </tr>
				  <tr>
				  	<th>Origin : </th>
					<td><?php echo $t_strOrigin;?></td>
					<th>Sender :</th>
					<td><?php echo $t_strSender;?></td>
				  </tr>
				  <tr>
					<th>File Container : </th>
					<td><?php echo $rsContainer[0]['label']; ?>&nbsp;</td>
					<th class="head" rowspan="2">Remarks : </th>
					<td rowspan="2" valign="top"><? echo $t_strRemarks;?></td>
				  </tr>
				  <tr>
					<td></td>
					<td></td>
				  </tr>
 <? if ($t_intConfidential)
	{
	?>
					<tr>
						<td colspan="4" style="text-align:center" >
							<div class="ui-widget" align="center">
								<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; width:140px"> 
									<p align="left"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Alert:</strong> Confidential.</p>
								</div>
							</div>
						</td>
					</tr>
	<?
	}
	?>
				  </table><!-- end of metadata -->
				</td></tr>
			</table> <!-- end of datawrap -->
			</td></tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>
				
				
				<table class="datawrap" width="605px" >
					<tr><td>
						<table width="600px" class="metadata">
							<tr class="metabutton">
								<td width="10px"></td>
								<td class="containerlabel">
<img src="images/menu-bar-right-arrow.gif" id="intrafilelinkimg" />
<a href="#filebodyintra" onClick="javascript:toggleWindowAjax2('filebodyintra','intrafilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyintra','addIntra');">Manage Files</a></td>
<td><div id="addIntra"></div></td>
							</tr>
							<tr>
								<td></td>
								<td width="590px">
<div id="filebodyintra" style="display:none">
		<!-- ############################# File Content #####################################-->
		
		<!-- #################################################################################-->
</div>
								</td>
							</tr>
						</table>
						</td>
					</tr>
	
		      </table>

			  </td>
		</tr>
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr><td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="intrahistorylinkimg" /><a href="javascript:getData('showAction.php?mode=intra&docID=<? echo $t_strNewId;?>&div=docubodyintra&historydiv=actionbodyintra','docubodyintra');" onClick="return toggleWindowAjax('docubodyintra','intrahistorylinkimg');" id="intrahistorylink">Update Action</a></td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
<!-- 	***************************		history				********************************************	-->
			<div id="docubodyintra" align="center" style="visibility:hidden">
			</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr>
					<td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel">
								<img src="images/menu-bar-right-arrow.gif" id="intraactionlinkimg" /><a href="javascript:getData('showHistory.php?mode=intra&docID=<? echo $t_strNewId;?>&div=actionbodyintra','actionbodyintra');" onClick="return toggleWindowAjax('actionbodyintra','intraactionlinkimg');" id="intraactionlink">View History</a>
							</td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
								<div id="actionbodyintra" align="center" style="visibility:hidden">
								</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
</table>
	
	
<? }else //mode = new or edit
{
?>
<form action="javascript:check(document.getElementById('frmIntra'),'intra','showIntraOffice.php');" name="frmIntra" id="frmIntra" onsubmit="return checkConfi(document.getElementById('frmIntra'));">
<table align="center" width="600" class="tblforms">
    <tr> 
       	<th width="97">Document ID </td>
    	<td width="175"><input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required" <? echo $disable_ID;?> style="width:150px">
	<input type="hidden" name="mode" value="<? echo $nextMode;?>"></td>
    	<th width="100">Document Date</td>
      	<td width="228"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="documentDateIntra" name="documentDateIntra" value="<? echo $documentDateIntra;?>"></td>
    </tr>
    
	<tr> 
    	<th>Document Type</td>
      	<td><select name="cmbDocType" class="caption">
		<option value="-1">  </option>
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeAbbrev'];?></option>
	  <?
	  }
	  ?>
        </select></td>   
		<th>Deadline</td>
    	<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="deadlineIntra" name="deadlineIntra" value="<? echo $deadlineIntra;?>"> </td>      
    </tr>
	
    <tr> 
      	<th>Reference ID</td>
      	<td valign="top"><input type="text" name="t_strReferenceId" id="t_strReferenceId" value="<? echo $t_strReferenceId;?>"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>	
	</tr>
	<tr>
	  <th>Doc No.</td>
	  <td colspan="3"><br /><input type="text" name="t_strDocNum" id="t_strDocNum" value="<? echo $DocNum;?>" /><span class="required" style="text-transform:none">&nbsp;(optional: AO no.,SO no. etc.)</span></td>
  	</tr>

    <tr> 
      	<th>Subject</td>
	    <td colspan="3"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3" title="required"><? echo $t_strSubject; ?></textarea>
    </tr>
	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><br /><br /></td>  
	</tr>
	
	<tr>
		<th>Origin</th>
		<td><input type="text" name="t_strOrigin" id="t_strOrigin" value="<? echo $t_strOrigin;?>" disabled="disabled"/></td>
		<th>Sender</th>
		<td><input type="text" name="t_strSender" id="t_strSender" value="<? echo $t_strSender;?>" /></td>
	</tr>
	<tr> 
   		<th>Remarks</td>
		<td  valign="top"><textarea cols="30" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"><? echo $t_strRemarks;?></textarea></td>
		<th valign="top">File Container</td>
		<td valign="top"><select name="t_intContainer" class="caption">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
	  <?
	  }
	  ?>
        </select></td>
   	</tr>
   
   	<tr> 
		<th>Confidential</td>
		<td valign="top"><input type="checkbox" value="1" name="t_intConfidential" id="t_intConfidential" <? 
	  if ($t_intConfidential=='1') echo "checked=\"checked\"";
	  ?>></td>
	 	<td></td>
	 	<td></td>
   	</tr>

	<tr>
		<td colspan="4" style="text-align:center">
			<? if ($arrFields['mode'] == "new"){?>
				<input type="submit" value="Save" class="btn" onclick="">
			<? }else{ ?>
			  	<input type="submit" value="Update" class="btn" onclick="">
			<? }  ?>
  			<input type="reset" value="Clear" class="btn"></td>
	</tr>
	<!-- 
	 <tr> 
      <td height="24" valign="top">Action to be Taken</td>
      <td valign="top"><select name="select5" class="caption">
			<option selected>for Info</option>
          <option>for Signature</option>
          <option>for Comments</option>
        </select></td>
      <td valign="top">File Container</td>
      <td valign="top"><input name="text" type="text" class="caption"></td>
 
    </tr>
    <tr> 
      <td height="27" valign="top">Action Unit</td>
      <td valign="top"><select name="select6" multiple class="caption">
          <option selected></option>
          <option>ASTI</option>
          <option>NAST</option>
          <option>NRCP</option>
        </select></td>
      <td rowspan="2" valign="top">Mode of Delivery</td>
      <td rowspan="2" valign="top"><input name="text2" type="text" class="caption"></td>
  
    </tr>
	<td valign="top">Mode of Delivery</td>
    <td valign="top"><input name="t_strDelivery" id="t_strDelivery" type="text" value="<? echo $t_strDelivery;?>" ></td>
	-->
  </table>
</form>			

<?
} ?>
