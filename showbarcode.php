<?php
@session_start();
require('class/barcode/code128.php');
include_once('class/General.class.php');
$code = $_REQUEST['code'];
$group = $_REQUEST['group'];
$user = "".$_SESSION["userOffice"]; 

$gen = new General;
//$code='RMS-12-00009';

//if($group=='RMS'){
// $barcode = explode('-',$code);
// $barcode_cntr =str_pad(ltrim($barcode[4], "0"), 6, "0", STR_PAD_LEFT);  
// $strbarcode = "{$barcode[1]}-{$barcode[3]}-".$barcode_cntr;

//}
//size of Legal paper = 216x356
$x = 356/2;
$strbarcode=$code;
$code_split = explode('-',$code);
// $arOffices = $gen->getOfficeFromExeOffice();
// echo '<pre>';
// 		  print_r($arOffices);
// 		  die();

$arOffices = array(
	array('officeCode' => 'OSEC','officeName' => ' Office of the Secretary'),
	array('officeCode' => 'IAS','officeName' => 'Internal Audit Service'),
	array('officeCode' => 'OMAD','officeName' => 'Operations and Management Audit Division'),
	array('officeCode' => 'COA','officeName' => 'Commission on Audit'),
	array('officeCode' => 'OASECFALA','officeName' => 'Office of the Assistant Secretary Financial Administration and Legal Affairs'),
	array('officeCode' => 'FMS','officeName' => 'Financial and Management Service'),
	array('officeCode' => 'AD','officeName' => 'Accounting Division'),
	array('officeCode' => 'BD','officeName' => 'Budget Division'),
	array('officeCode' => 'MD','officeName' => 'Management Division'),
	array('officeCode' => 'DLLO','officeName' => 'Department Legislative Liaison Office'),
	array('officeCode' => 'OASECA','officeName' => 'Office of the Assistant Secretary for Administration'),
	array('officeCode' => 'ALS','officeName' => 'Administrative and Legal Service'),
	array('officeCode' => 'GSD','officeName' => 'General Service Division'),
	array('officeCode' => 'BAC','officeName' => 'Bids and Awards Committee'),
	array('officeCode' => 'BGMU','officeName' => 'Buildings and Grounds Maintenance Unit'),
	array('officeCode' => 'CASHIER','officeName' => 'Collection and Disbursement Section'),
	array('officeCode' => 'GSS','officeName' => 'General Service Section'),
	array('officeCode' => 'MOTORPOOL','officeName' => 'Motorpool Unit'),
	array('officeCode' => 'PROCUREMENT','officeName' => 'Procurement Section'),
	array('officeCode' => 'PSS','officeName' => 'Property and Supply Section'),
	array('officeCode' => 'RMS','officeName' => 'Records Management Section'),
	array('officeCode' => 'TELECOM','officeName' => 'Telecommunications Unit'),
	array('officeCode' => 'LD','officeName' => 'Legal Division'),
	array('officeCode' => 'PD','officeName' => 'Personnel Division'),
	array('officeCode' => 'DDCC','officeName' => 'DOST Day Care Center'),
	array('officeCode' => 'OASECIC','officeName' => 'Office of the Assistant Secretary for International Cooperation'),
	array('officeCode' => 'OUSECDRRCC','officeName' => 'Office of the Undersecretary for Disaster Risk Reduction and Climate Change'),
	array('officeCode' => 'OUSECROS','officeName' => 'Office of the Undersecretary for Regional Operations'),
	array('officeCode' => 'OUSECRD','officeName' => 'Office of the Undersecretary for Research and Development'),
	array('officeCode' => 'SPD','officeName' => 'Special Projects Division'),
	array('officeCode' => 'OUSECSTS','officeName' => 'Office of the Undersecretary for Science and Technology Services'),
	array('officeCode' => 'PES','officeName' => 'Planning and Evaluation Service'),
	array('officeCode' => 'ITD','officeName' => 'Information Technology Division'),
	array('officeCode' => 'PDPD','officeName' => 'Policy Development and Planning DIvision'),
	array('officeCode' => 'PCMD','officeName' => 'Program Coordination and Monitoring Division'),
	array('officeCode' => 'STRAED','officeName' => 'Science and Technology Resource Assessment and Evaluation Division'),
	array('officeCode' => 'ITCU','officeName' => 'International Technology Cooperation Unit'),
	array('officeCode' => SFU, 'officeName' => 'Science and Technology Foundation Unit')
);
$header='Department of Science and Technology' ;

if ($user == 'OASECFALA')
	$header2='Finance, Administrative & Legal Affairs (OASECFALA)'; 
else	
	$header2='Records Management Section (GSD)'; 


//$docDateAdded = $gen->getDocDateAdded($code);

for($i=0;$i<sizeof($arOffices);$i++)
 if($arOffices[$i]['officeCode']==$code_split[0]){
  $accro = $code_split[0]=='RMS'?"GSD":$arOffices[$i]['officeCode'];
//  $accro = $code_split[0]=='OASECFALA'?"OASECFALA":$arOffices[$i]['officeCode'];
  
  $header2 = preg_replace("/&#?[a-z0-9]{2,8};/i","",$arOffices[$i]['officeName'])."($accro)";
	if ($accro=='OUSECSTS')
		$header2='Office of the Undersecretary for S&T Services (OUSECSTS)';
	if ($accro=='OASECFALA' OR $user == 'OASECFALA')
		$header2='Finance, Administrative & Legal Affairs (OASECFALA)';
	if ($accro=='OUSECRD' OR $user == 'OUSECRD')
		$header2='Office of the Undersecretary for Research and Development';
  //$docDateAdded = $gen->getDocDateReceived($code); 
 					
 }
 
if(is_numeric($_SESSION['userOffice'])){
	$_SESSION['userOffice'] = $gen->getOfficeName($_SESSION['userOffice'],'','','agency');
}

if ($_SESSION['grp'] == 'NCBP'):
	$header2='National Committee on Biosafety of the Philippines';
endif;

// if ($gen->isDocFromAgency($code_split[0])==''){
// 	$docDateAdded = $gen->getDocDateReceived($code);
// 	//echo"UP - $docDateAdded - $code_split[0] -  $gen->isDocFromAgency($code_split[0])";
// }else	{
// 	$docDateAdded = $gen->getDocDateAdded($code);
// 	//echo"DOWN - $docDateAdded - $code_split[0] - $gen->isDocFromAgency($code_split[0]) ";
// }
if(($code_split[0] == $_SESSION['office']) or ($code_split[0] == $_SESSION['userOffice'])){
	$docDateAdded = $gen->getDocDateAdded($code);
}else{
	$docDateAdded = $gen->getDocFirstDateReceived($code, $_SESSION['office']);
}

$pdf=new PDF_Code128();
$pdf->SetAutoPageBreak(true, 0.2);//$_REQUEST['bmargin']);
$pdf->AddPage('L','Legal');
$pdf->SetFont('Arial','',50);
$pdf->Image('images/logo.jpg',4,8,17);
$pdf->SetXY($x-($pdf->GetStringWidth($header) / 2),17);
$pdf->Write(0,$header);
$pdf->SetFont('Arial','',35);
$pdf->SetXY($x-($pdf->GetStringWidth($header2) / 2),35);
$pdf->Write(0,$header2);
$pdf->SetFont('Arial','',50);

//print the barcode
$bheight = $accro=='OSEC'?70:95;
$pdf->Code128(2,48,$strbarcode,354,$bheight);
$pdf->SetFont('Arial','',65);
$bheight = $accro=='OSEC'?135:160;
$pdf->SetXY($x-($pdf->GetStringWidth($strbarcode) / 2),$bheight);
$pdf->Write(0,$strbarcode);
$pdf->SetFont('Arial','',50);
$bheight = $accro=='OSEC'?157:182;

if ($accro!='OSEC'){
// $docDateAdded = $gen->getDocDateAdded($code);
// $date = date_create(date('Y-m-d H:i:s')); //date_create($docDateAdded);
// $date = date_create(date('Y-m-d H:i:s')); //date_create($docDateAdded);

// $pdf->SetXY($x-($pdf->GetStringWidth(date('j F Y g:h:i a', strtotime($docDateAdded))) / 2),$bheight);

//$pdf->SetXY($x-($pdf->GetStringWidth(date_format($date,'j F Y g:h:i a')) / 2),$bheight);
// $pdf->Write(0,date_format($date,'j F Y g:h:i a'));
// $pdf->Write(0,date('j F Y g:h:i a', strtotime($docDateAdded)));

// $pdf->SetXY($x-($pdf->GetStringWidth(date('j F Y g:i:s A', strtotime($docDateAdded))) / 2),$bheight);
// $pdf->Write(0,date('j F Y g:i:s A', strtotime($docDateAdded)));
}

// if(date('j F Y g:i:s A', strtotime($docDateAdded)) == '')

$pdf->SetXY($x-($pdf->GetStringWidth(date('j F Y g:i:s A', strtotime($docDateAdded))) / 2),$bheight);
// $pdf->Write(0,date('j F Y g:i:s A', strtotime($docDateAdded)));
$thisYear = date('Y', strtotime($docDateAdded));
if($thisYear == '1970'):
	$pdf->Write(0,date('j F Y g:i:s A'));
else:
	$pdf->Write(0,date('j F Y g:i:s A', strtotime($docDateAdded)));
endif;

$pdf->Output();
?>

<!-- 1 January 1970 -->