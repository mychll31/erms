<?php
$filename = $_GET['filename'];
$mode = $_GET['mode']; 
$office = $_GET['office'];

function getFileContent($srvr,$tfile){
    $url = 'http://'.$srvr.'/'.$tfile;
	$options = array( 'http' => array(
		'user_agent'    => 'hack',        // who am i
		'max_redirects' => 20,              // stop after 10 redirects
		'timeout'       => 120,             // timeout on response
	) );
	$context = stream_context_create( $options );
	$page    = @file_get_contents( $url, false, $context );
	$result  = array( );
	if ( $page != false )
		$result['content'] = $page;
	else if ( !isset( $http_response_header ) )
		return null;    // Bad url, timeout

	// Save the header
	$result['header'] = $http_response_header;

	// Get the *last* HTTP status code
	$nLines = count( $http_response_header );
	for ( $i = $nLines-1; $i >= 0; $i-- )
	{
		$line = $http_response_header[$i];
		if ( strncasecmp( "HTTP", $line, 4 ) == 0 )
		{
			$response = explode( ' ', $line );
			$result['http_code'] = $response[1];
			break;
		}
	}
	return $result;
}

function mkdr($office)
{
	 $old_umask = umask(0);
	 $ERMS_DIR = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')?'':DIRECTORY_SEPARATOR.'ERMS';
	 $dir = getcwd().$ERMS_DIR.DIRECTORY_SEPARATOR.$office.DIRECTORY_SEPARATOR.$_GET['dateY'].DIRECTORY_SEPARATOR.$_GET['dateF'].$_GET['dateY'].DIRECTORY_SEPARATOR.$_GET['dated'];  
	if(!file_exists($dir)){
		mkdir( $dir, 01777, true);
		umask($old_umask);
	} 
		return  $dir;
}

if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)){
	$ip = $_SERVER['HTTP_CLIENT_IP'];
}elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){
	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
}else{
	$ip = $_SERVER['REMOTE_ADDR'];
}

switch($mode)
{
 case 'create':
 					$content = getFileContent($_GET['s'],$_GET['tfile']);
			   		$f = fopen(mkdr($office).'/'.$filename, "wb");
				    fwrite($f, $content['content']);
				    fclose($f);
				    break;

 case 'edit':		$content = getFileContent($_GET['s'],$_GET['tfile']);
 					$dir = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')?str_replace('ERMS/','',$_GET['tfile']):'ERMS'.'/'.$_GET['tfile'];
			   		$f = fopen($dir, "wb");
				    fwrite($f, $content['content']);
				    fclose($f);
				    break;					
 case 'destroy':	$filename = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')?str_replace('/','\\',$filename):$filename;
 					unlink(getcwd().'/'.$filename);
 				    break;		   
}

?>
