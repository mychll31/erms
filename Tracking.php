<?php 
	include("class/Tracking.class.php");

	$tracking = new Tracking;
	$document = $tracking->GetDocStatus($_GET['id']);
	
	if($document):
		$document = $document[0];
		
		$recipient = '';
		if($document['recipientUnit'] != 'employee'){
			$recipient = $tracking->GetOfficeName($document['recipientId']);
			$recipient = $recipient[0]['groupname'];
		}else{
			$recipient = $tracking->GetEmpName($document['recipientId']);
			$recipient = $recipient[0]['firstname'].' '.$recipient[0]['surname'];
		}

		$sender = '';
		if($document['senderUnit'] != 'employee'){
			$sender = $tracking->GetOfficeName($document['senderId']);
			$sender = $sender[0]['groupname'];
		}else{
			$sender = $tracking->GetEmpName($document['senderId']);
			$sender = $sender[0]['firstname'].' '.$sender[0]['surname'];
		}
		
		$document['recipient'] = $recipient;
		$document['sender'] = $sender;

		echo json_encode($document);
	else:
		echo 'confidential';
	endif;
 ?>