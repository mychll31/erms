<? 
@session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Report.class.php");
include_once("class/DocumentType.class.php");
$objGen = new General;
$objReport = new Report;
$objDocuType = new DocType;

?>
<script language="JavaScript">
	function openPrint()
	{
		
	//var glideNo = "<? echo $_GET['GID'] ?>";
	//var reportType = document.all.dtmReportType.value;
	var reportType = document.getElementById("dtmReportType").options[document.getElementById("dtmReportType").selectedIndex].value;
	//strPage = "ReportDocumentDeadlines.php?RID="+regionCode+'&GID='+glideNo+'&_=<? echo time();?>';
	//alert(regionCode);
	var GID="test";
	
	var showWindow = 1;
	var notedbyname = '';
	var notedbyposition = '';

	//alert(reportType);
	//if(reportType=="LRD" || reportType=="LIR" || reportType=="LOR" || reportType=="LIOR" || reportType=="NDP" || reportType=="RP" || reportType=="UD")
	if(reportType=="IAO" || reportType=="LRD" || reportType=="LIR" || reportType=="MLMD" || reportType=="MLF" || reportType=="MLER" ||  reportType=="MLOD" ||  reportType=="MLIR" || reportType=="MLRD" || reportType=="LOR" || reportType=="LIOR" || reportType=="RP" || reportType=="UD" || reportType=="SRD" || reportType=="UDSD" || reportType=="BRCD")
	{
		dateFrom = document.getElementById("dateFrom").value;
		dateTo = document.getElementById("dateTo").value;
		if(reportType=="IAO" || reportType=="LIR" || reportType=="LOR")
			seldoctype = document.getElementById("seldoctype").value;
		
		if(dateFrom.length == 0)
		{
			alert("Please select Date From!");
			document.getElementById("dateFrom").focus();
			showWindow = 0;		
		}
		else if(dateTo.length == 0) 
		{
			alert("Please select Date To!");
			document.getElementById("dateTo").focus();
			showWindow = 0;		
		} 
		else if(dateFrom>dateTo)
		{
			alert("Please change date range!");
			document.getElementById("dateFrom").focus();
			showWindow = 0;		
		}
		
	}	
	else if(reportType=="RA")
	{
		documentID = document.getElementById("documentID").value;
		if(documentID.length == 0)
		{
			alert("Please input Document ID!");
			document.getElementById("documentID").focus();
			showWindow = 0;		
		}
	}	 
	
	else if(reportType=="NDP" || reportType=="RD" || reportType=="AR")
	{
		//documentID = document.getElementById("documentID").value;
		dateYear = document.getElementById("cboLtrYear").value;
		dateMonth = document.getElementById("cboLtrMonth").value;
		if(dateYear.length == 0)
		{
			alert("Please select Year!");
			document.getElementById("cboLtrYear").focus();
			showWindow = 0;		
		}
		 
		if(dateMonth.length == 0)
		{
			alert("Please select Month!");
			document.getElementById("cboLtrMonth").focus();
			showWindow = 0;		
		}
	}	
	if(reportType=="BRCD")
	{
		doctypebarcode = document.getElementById("seldoctype").value;
		if(doctypebarcode=="")
		{
			alert("Please select document type!");
			document.getElementById("seldoctype").focus();
			showWindow = 0;		
		}
	}
	if(reportType=="MLMD" || reportType=="MLRD" || reportType=="MLF" || reportType=="MLER" || reportType=="MLIR" || reportType == "MLOD")
	{
		notedbyname = document.getElementById("txtnotedbyname").value;
		notedbyposition = document.getElementById("txtnotedbyposition").value;
	}
	if(reportType!="")
		if(reportType=="LRD")
			strPage = "reportDocumentDeadlines.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLMD")
			strPage = "reportDocumentMLMDExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLRD")
			strPage = "reportDocumentMLRDExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLF")
			strPage = "reportDocumentMLFExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLER")
			strPage = "reportDocumentMLERExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLIR")
			strPage = "reportDocumentMLIRExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="MLOD")
			strPage = "reportDocumentMLODExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&nname='+notedbyname+'&npos='+notedbyposition+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="LIR")
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentIncomingExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';	
			}
			else
			{ // added by emma END
			strPage = "reportDocumentIncoming.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		else if(reportType=="LOR")
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentOutgoingExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
			else
			{ // added by emma END
			strPage = "reportDocumentOutgoing.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		else if(reportType=="IAO")
			//// added by emma for excel format of report
			if(document.getElementById("excelType").checked)
			{
			strPage = "reportDocumentIncOutExcel.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';		
			}
			else
			{ // added by emma END
			strPage = "reportDocumentIncOut.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&doctype='+seldoctype+'&_=<? echo time();?>';
			}
		else if(reportType=="LIOR")
			strPage = "reportDocumentIntra.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		//else if(reportType=="NDP")
		//	strPage = "reportDocumentEncoded.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="RA")
			strPage = "reportDocumentHistory.php?reportType="+reportType+'&documentID='+documentID+'&_=<? echo time();?>';
		else if(reportType=="RP")
			strPage = "reportDocumentProfile.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="UD")
			strPage = "reportDocumentUnacted.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';	
		else if(reportType=="UDSD")
			strPage = "reportDocumentUnacted2.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
		else if(reportType=="NDP")
			strPage = "reportDocumentEncoded.php?reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';
		else if(reportType=="RD")
			strPage = "reportDocumentReceived.php?reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';	
		else if(reportType=="AR")
			strPage = "reportDocumentActionsRecorded.php?reportType="+reportType+'&dateMonth='+dateMonth+'&dateYear='+dateYear+'&_=<? echo time();?>';	
		else if(reportType=="SRD") //pao
			strPage = "reportDocumentReceivedSummary.php?reportType="+reportType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';	
		else if(reportType=="BRCD")
			strPage = "reportDocumentToSummary.php?reportType="+reportType+"&docType="+doctypebarcode+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&_=<? echo time();?>';
	else
	{
		showWindow = 0;
		alert("Please select Report Type!");
		document.getElementById("dtmReportType").focus();
	}	
	/*
	if(reportType!='')
		window.open(strPage, '_blank','toolbar=no,location=no,directories=no,status=0,menubar=0,scrollbars=1,resizable=1,width=780,height=528');
	else
		alert('Error:\nPlease select a Region');
	*/	
		if(showWindow)
			window.open(strPage, '_blank','toolbar=no,location=0,directories=no,status=0,menubar=0,scrollbars=1,resizable=1,width=780,height=528');
	}
	
	function changeReportType()
	{
	var value = document.getElementById("dtmReportType").value;
		getData('showReports.php?mode=show&reportType='+value,'reports');
	}
</script>
<?
//javascript:check(document.getElementById('frmReport'),'reports','showReports.php');
?>

<form name="frmReport" id="frmReport">
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="textbody" align="center">
  <!--DWLayoutTable-->
  <tr>
    <td width="110" height="1"></td>
    <td width="157"></td>
    <td width="41"></td>
    <td width="172"></td>
    <td width="129"></td>
    <td width="104"></td>
  </tr>
  <tr>
    <td height="22"></td>
    <td valign="top"><div align="right"><strong>Report Type &nbsp;&nbsp; </strong></div></td>
    <td colspan="2" valign="top">
	<?php 
	
		$rsReport = $objReport->getReportTypes();
		?>
	<select name="dtmReportType" id="dtmReportType" class="textbody" onchange="changeReportType();">
	<option value="-1">&nbsp;</option>
	<?php
		for($i=0; $i<count($rsReport); $i++){
			if($_GET['reportType']==$rsReport[$i]['reportCode'])
				echo "<OPTION value='".$rsReport[$i]['reportCode']."' SELECTED>".$rsReport[$i]["reportName"]."</OPTION>\n";
			else
				echo "<OPTION value='".$rsReport[$i]['reportCode']."'>".$rsReport[$i]["reportName"]."</OPTION>\n";
		}
	?>
    </select></td>
    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td></td>
  </tr>
  <!-- Begin Document Type -->
  <?php 
  	if($_GET['reportType']=="IAO" || $_GET['reportType']=="LRD" || $_GET['reportType']=="MLMD" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLOD" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="LIOR" || $_GET['reportType']=="RP" || $_GET['reportType']=="UD" || $_GET['reportType']=="SRD"  || $_GET['reportType']=="UDSD" || $_GET['reportType']=="BRCD")
			if($_GET['reportType']=="IAO" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="BRCD") 
			{
				echo '
					  <tr>
					    <td height="22"></td>
					    <td valign="top"><div align="right"><strong>Document Type &nbsp;&nbsp; </strong></div></td>
					    <td colspan="2" valign="top">';
					    	$rsDoctypes = $objDocuType->getDocType();
				echo '
							<select style="width: 231px;" name="seldoctype" id="seldoctype">';
								if($_GET['reportType']=="BRCD")
									echo '<option value=""></option>';
								else
									echo '<option value="all">ALL</option>';
					    		foreach($rsDoctypes as $doctype):
					    			if($_GET['reportType']=="BRCD"){
					    				if(in_array($doctype['documentTypeId'], array('177','257','161','263')))
					    					echo '<option value='.$doctype['documentTypeId'].'>'.$doctype['documentTypeAbbrev'].'</option>';
					    			}
					    			else
					    				echo '<option value='.$doctype['documentTypeId'].'>'.$doctype['documentTypeAbbrev'].'</option>';
					    		endforeach;
				echo '
							</select>
						</td>
					    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
					    <td></td>
					  </tr>
				';
			}
   ?>
   <!-- End Document Type -->
  <tr>
    <td height="5"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> 
  <tr>
    <td height="22"></td>
    <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td colspan="4" valign="top">
	
	<table width="82%"  border="0">
	<?php 
 	if($_GET['reportType']=="IAO" || $_GET['reportType']=="LRD" || $_GET['reportType']=="MLMD" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLOD" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR" || $_GET['reportType']=="LIOR" || $_GET['reportType']=="RP" || $_GET['reportType']=="UD" || $_GET['reportType']=="SRD"  || $_GET['reportType']=="UDSD" || $_GET['reportType']=="BRCD")
		 echo '
			  <tr>
				<td width="27%">From</td>
				<td width="73%"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateFrom" name="dateFrom"></td>
			  </tr>
			  <tr>
				<td>To</td>
				<td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateTo" name="dateTo"></td>
			  </tr>';
		  //// added by emma for excel format of report
			if($_GET['reportType']=="IAO" || $_GET['reportType']=="LIR" || $_GET['reportType']=="LOR") 
			{
				echo '<tr>
						<td>Excel Format</td>
						<td><input type="checkbox" id="excelType" name="excelType" value="1"></td>
			  		  </tr>';
			}
			
			/// added by emma end
	elseif($_GET['reportType']=="RA") 
		  echo ' 
			  <tr>
				<td>Document ID</td>
				<td><input type="text" id="documentID" name="documentID"></td>
			  </tr>
			 '; 
	elseif($_GET['reportType']=="NDP" || $_GET['reportType']=="RD" || $_GET['reportType']=="AR")
	{
		  echo ' 
			  <tr>
				<td>Month</td>
				<td>
					<select name="cboLtrYear" id="cboLtrYear">
					 ';
					 $objReport->comboYear($cboYear);
                     
					echo '
					</select>
				</td>
			  </tr>
  			  <tr>
				<td>Year</td>
				<td>
					<select name="cboLtrMonth" id="cboLtrMonth">
					';
					$objReport->comboMonth($cboMonth); 
                    
                    echo '
					</select>
				</td>
			  </tr>
			 '; 	
	}
	if($_GET['reportType']=="MLMD" || $_GET['reportType']=="MLRD" || $_GET['reportType']=="MLF" || $_GET['reportType']=="MLER" || $_GET['reportType']=="MLIR" || $_GET['reportType']=="MLOD"){
			echo '
				<tr>
					<td><b>Noted BY</b></td>
					<td><input type="text" name="txtnotedbyname" id="txtnotedbyname"></td>
				</tr>
				<tr>
					<td><b>Position</b></td>
					<td><input type="text" name="txtnotedbyposition" id="txtnotedbyposition"></td>
				</tr>
			';
	}
	?>		 
	   <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
 
  
  <tr>
    <td height="5"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="24"></td>
    <td colspan="4" align="center" valign="top" style="text-align:center;"><input type="button" value="Generate" name="Generate" class="btn" onClick="openPrint();"></td>
    <td></td>
  </tr>
  </table>

</form>
