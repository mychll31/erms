<? @session_start();?>
<script language="javascript" type="text/javascript"> 
var validator='';
  jQuery("#acttakenrefresh").click( function(){ 
   //if($("#acttakenedit").val() == 'Save') $("#acttakenedit").val('Edit');
	jQuery('#ActionTakenGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#ActionTakenGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID', 'Action Description','Action Code'],
		colModel:[
			{name:'actionCodeId',index:'actionCodeId', width:65},
			{name:'actionCode',index:'actionCode', width:90},
			{name:'actionDesc',index:'actionDesc', width:100}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#pager'),
		postData:{table:'tblActionTaken'},
		sortname: 'actionCodeId',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"Action Taken"
	}).navGrid('#pager',{edit:false,add:false,del:false});}); 
  });
    $('#acttakenclear').click( function(){ 
	   $("#actTakenId").val(''); 	   
	   $("#actTakenCode").val(''); 
   	   $("#actTakenDesc").val(''); 
	   $("#acttakensave").css('display','none');
	   $("#acttakenadd").css('display','block');
	   $('#Clear_acttaken').text(" Clear ");
	   validator.resetForm();
	   $('#acttakenrefresh').click();
    });	
	
//Edit & Save function	
	jQuery("#acttakenedit").click( function(){ 
	  var id = jQuery("#ActionTakenGrid").getGridParam('selrow'); 
	  if (id) {
	   var ret = jQuery("#ActionTakenGrid").getRowData(id); 
	   $("#actTakenId").val(ret.actionCodeId); 	   
	   $("#actTakenCode").val(ret.actionCode); 
   	   $("#actTakenDesc").val(ret.actionDesc); 

	   if($("#acttakenedit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
	    $("#acttakensave").css('display','block');
		$("#acttakenadd").css('display','none');
		$('#Clear_acttaken').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 

	jQuery("#acttakensave").click( function(){ 
	  var actcid = $("#actTakenId").val();
 	  var code = $("#actTakenCode").val();
   	  var desc = $("#actTakenDesc").val();
	    $("#ActionTakenGrid").setPostData({mode:"save",table:'tblActionTaken',ID:actcid,CODE:code,DESC:desc,MODULE:'Action Taken'});
	    $("#ActionTakenGrid").trigger("reloadGrid");
 	    $('#acttakenclear').click();
	});
//Add function	   
	//jQuery("#acttakenadd").click( function(){ 
	function AddRec(){
	  var actcid = $("#actTakenId").val();
 	  var code = $("#actTakenCode").val();
   	  var desc = $("#actTakenDesc").val();
	  
      $("#ActionTakenGrid").setPostData({mode:"add",table:'tblActionTaken',ID:actcid,CODE:code,DESC:desc,MODULE:'Action Taken'});
	  $("#ActionTakenGrid").trigger("reloadGrid");
  	  $('#acttakenclear').click();
	  jAlert('Succesfully Added','Informationss'); }
	 
	$("#acttakendelete").click(function(){ 
	 var id = jQuery("#ActionTakenGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if( id != ''){ 
   	   jConfirm('Proceed deleting this record?', false,'ERMS Confirmation Dialog', function(r) {
	    if(r==true){
		  $("#ActionTakenGrid").setPostData({mode:"del",table:'tblActionTaken',ID:id.toString(),MODULE:'Action Taken'});
	      $("#ActionTakenGrid").trigger("reloadGrid"); 
		   $('#acttakenclear').click();
		  jAlert('Succesfully deleted', 'Confirmation Results');} });
	  }
	 else jAlert('Please select row to delete', 'Warning'); }); 
     $('#acttakenrefresh').click();
 	 $('#acttakenadd').click(function() {
      $('#frmActTaken').submit();
     });

//Form validation
 	$().ready(function() {
	 validator = $("#frmActTaken").validate({
		rules: {
			actTakenCode: "required",
			actTakenDesc: "required"
		},
		messages: {
			actTakenCode: "Action Taken Code is required",
			actTakenDesc: "Action Taken Description is required"
		},
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	 
</script>
<form  id="frmActTaken" autocomplete="off">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Action Taken  Code</td>
    <td class="field"><input type="text" name="actTakenCode" id="actTakenCode"></td>
	<td class="status"></td>
	<input type="hidden" name="actTakenId" id="actTakenId">
  </tr>
  <tr>
    <td class="label">Action Taken Description</td>
    <td class="field"><input type="text" name="actTakenDesc" id="actTakenDesc"></td>	
    <td class="status"></td>
  </tr>

 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="acttakenedit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="acttakendelete"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="acttakenadd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="acttakensave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="acttakenclear"><span title='Clear'></span><div id="Clear_acttaken">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="acttakenrefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>
</form>			  
</table>
<table id="ActionTakenGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="pager" class="scroll" style="text-align:center;"></div>
