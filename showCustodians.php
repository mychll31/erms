<?php
@session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ListOfCustodians.xlsx");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 

include_once("class/ManageCustodian.class.php");
$objCustodian = new ManageCustodian;

$employee = $objCustodian->getCustodians();
$count = 1;
echo "<font face='Gotham, Helvetica Neue, Helvetica, Arial, sans-serif'><table border='1' cellpadding='0' cellspacing='0'>";

echo "<tr bgcolor='#CCCCCC'>";
echo 	"<td width='35'>&nbsp;</td>";
echo 	"<th width='150' align='center'>EMPLOYEE NUMBER</th>";
echo 	"<th width='200' align='center'>SURNAME</th>";	
echo 	"<th width='200' align='center'>FIRSTNAME</th>";	
echo 	"<th width='35' align='center'>MIDDLE INITIAL</th>";	
echo 	"<th width='35' align='center'>OFFICE</th>";	
echo "</tr>";


for($a=0;$a<count($employee);$a++)
{
	$office = $objCustodian->getCustodianOffice($employee[$a]['empNumber']);
	//$office = $employee[$a]['officeCode']==""?$employee[$a]['divisionCode']:$employee[$a]['officeCode'];
	echo "<tr>";
	echo 	"<td width='35'>&nbsp;".$count."</td>";
	echo 	"<td width='150'>&nbsp;".$employee[$a]['empNumber']."</td>";
	echo 	"<td width='200'>&nbsp;".$employee[$a]['surname']."</td>";	
	echo 	"<td width='200'>&nbsp;".$employee[$a]['firstname']."</td>";	
	echo 	"<td width='35'>&nbsp;".$employee[$a]['middleInitial']."</td>";	
	echo 	"<td width='35'>&nbsp;".$office[0]['officeCode']."</td>";		
	echo "</tr>";
	$count++;
}
echo "</table></font>";
?>