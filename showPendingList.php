<?php
session_start();
// error_reporting(-1);

$_SESSION['userID'] = $_GET['userID'];
$_SESSION['office'] = isset($_GET['userOffice']) ? $_GET['userOffice'] : '';
$_SESSION['grp'] = isset($_GET['grp']) ? $_GET['grp'] : '';
$rec_office = isset($_GET['recoffice']) ? $_GET['recoffice'] : 'all';

// include_once("class/General.class.php");
include_once("class/IncomingList.class.php");
function cmp($a, $b)
{
    return strcmp($a["officeName"], $b["officeName"]);
}

// $objGeneral = new General;
$objIncoming = new IncomingList;
if($_GET['mode']=='officeList'){
	$arrOffice = $objIncoming->getOfficeList2($_SESSION['office']);  # OTHER OFFICES
	$arrOffice2 = $objIncoming->getOfficeList($_SESSION['office']);  # AGENCY
	$arrOffice3 = $objIncoming->getOfficeList3($_SESSION['office']); # ID NUMBER
	$arrOffice = array_merge($arrOffice2, $arrOffice);
	usort($arrOffice, "cmp");

	$arrOffice = array_merge($arrOffice, $arrOffice3);
	echo json_encode($arrOffice);
}else if($_GET['mode']=='receive'){
	foreach($_POST['docids'] as $key=>$docid):
		$docreceived = $objIncoming->receiveDocument($docid,$_POST['receivedby'],$_POST['historyid'][$key]);
	endforeach;
}else if($_GET['mode']=='saveImage'){
	// save signature
	define('UPLOAD_DIR', '/var/www/erms/signature/');
    $filePath = UPLOAD_DIR . uniqid().date('U').'.png';
    $imgData = str_replace(' ','+',$_POST['img']);
	$imgData =  substr($imgData,strpos($imgData,",")+1);
	$imgData = base64_decode($imgData);
	$file = fopen($filePath, 'w');
	fwrite($file, $imgData);
	fclose($file);

	// insert signature
	$saveSignature = $objIncoming->saveSignature($_POST['docid'], $_POST['historyid'], $filePath, $_POST['receivedby']);
}else{
	$arrPending = array();
	$arrPending = $objIncoming->getPendingList($_SESSION['office'], $rec_office);
	echo json_encode($arrPending);
}
?>