<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");
require_once("class/DocDetail.class.php");
include_once("class/IncomingList.class.php");
$objList = new IncomingList;
$objIncoming = new Incoming;
$objDocDetail = new DocDetail;
$file=new General();
$group = explode('~',$objList->getOfficeCode($objList->get('userID')));
// ini_set('display_errors', 1);

if($_REQUEST['mode']=='update' && $_REQUEST['blnMinDocInfo'])
{
	 echo $objIncoming->updateDocNo($_REQUEST);
}
if($_REQUEST['mode']!='new'){
	echo "<style>.ui-datepicker-trigger{ display:none; }</style>";
}

  if($_GET['mode']=="showAgency")
  {
  displayAgency($_GET['origunit'],$_GET['origid']);
  exit(1);
  }		 
 function displayAgency($t_intOriginUnit,$t_intOriginId)
 {
 //echo "origin".$t_intOriginId;
 $objInc = new Incoming;
 $arOrigin=$objInc->getOriginOffice("");
  ?>
  <script type="text/javascript">
 var arOrigin = new Array(<? echo sizeof($arOrigin);?>);
	  <?
  	  $intColumnNum=sizeof($arOrigin[0])/2;
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  echo "arOrigin[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {
	  	  for($x=0;$x<$intColumnNum;$x++)
		  {
	  		echo "arOrigin[".$i."][".$x."]='".$arOrigin[$i][$x]."';\n";
		  }
	  }?>  
  </script>
 <select name="cmbAgency" id="cmbAgency" onFocus="toggleAgency(this.form);" onchange="getPerson(this.value)" <? if($t_intOriginUnit=="2") echo "disabled=\"disabled\"";?> >
	  <option value="-1">&nbsp;</option>
	<?
				for($i=0;$i<sizeof($arOrigin);$i++) 
				{ 	?>	
					
					<OPTION value="<? echo $arOrigin[$i]['originId'];?>" <? if($arOrigin[$i]['originId']== $t_intOriginId) echo "selected"; ?>><? echo $arOrigin[$i]['officeName'];?></OPTION> 
			<?	}?>
		
</select><?
			 }
   ?>
<?

################## added by LCM #############
if($_GET['src']=="receive")
	{
	$objList->markRead($_GET['id'],$objIncoming->get('userID'));
	}
$hashId = ($_GET['src'] == 'receive')?'doclist':'inc';
    
##############################################

if($_GET['mode']=="") $arrFields = $objIncoming->getUserFields();
else $arrFields = $objIncoming->getUserFields1();

################################
#
#  modes: new - for new incoming document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################

if($_REQUEST["mode"]=="getGroup")
	{	
		$tmpGroupCode=$_REQUEST["officeGroup"];
		$arEmp=$objIncoming->getOfficeGroup($_REQUEST["officeCode"]);
		echo '<select id="cmbGroupCode" name="cmbGroupCode">';
		echo "<option value=''> </option>";
		$c2 = sizeof($arEmp);
		for($i2=0;$i2<$c2;$i2++)
		{
		?>		
			<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
		<?
		}
		echo "</select>";
	die();
	}


if($arrFields['mode']=='edit' || $arrFields['mode']=='view')
{
$rsDocument= $objIncoming->getDocument($arrFields['id']);
$t_strNewId = $rsDocument[0]['documentId'];
$dateReceived =    $rsDocument[0]['dateReceived'];
$documentDate =    $rsDocument[0]['documentDate'];
$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
$t_strSubject =    $rsDocument[0]['subject'];
$t_intOriginId =   $rsDocument[0]['originId'];
$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
$t_intGroupCode= $rsDocument[0]['originGroupId'];
$oTag=$rsDocument[0]['originUnit'];
$t_contdoc = $rsDocument[0]['controlleddoc'];
$t_cd_copyno = $rsDocument[0]['contdoc_copyno'];
$t_cd_copyholder = $rsDocument[0]['contdoc_copyHolder'];
$t_cd_mannerdisposal = $rsDocument[0]['contdoc_mannerOfDisposal'];
$t_cd_revisionno = $rsDocument[0]['contdoc_revisionno'];
$t_cd_personres = $rsDocument[0]['contdoc_personUnitResposible'];
$t_cd_withrawalno = $ctrlcontdocs['withrawalno'];
//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
//else 
if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
else $t_intOriginUnit=2;

$t_strSender =     $rsDocument[0]['sender'];
$deadline =         $rsDocument[0]['deadline'];
$fileLocation = $file->getLocation($t_strNewId);
$t_intContainer =  $fileLocation['containerId'];
$t_intContainer2 = $fileLocation['container2Id'];
$t_intContainer3 = $fileLocation['container3Id'];
$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
$rsContainer2 = $objIncoming->getContainerTwo($t_intContainer, $t_intContainer2);
$rsContainer3 = $objIncoming->getContainerThree($t_intContainer2, $t_intContainer3);
$t_strRemarks =    $rsDocument[0]['remarks'];
$t_intConfidential =   $rsDocument[0]['confidential'];
$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
$DocNum = $rsDocument[0]['docNum'];
$t_strReferenceId=$rsDocument[0]['referenceId'];
$disable_ID="disabled=\"disabled\"";
$nextMode="update";
//$rsFile = $objIncoming->getFiles($t_strNewId);
}

elseif ($arrFields['mode']=='save')
{
//echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
//print_r($_POST);

	$result=$objIncoming->addDocument($arrFields);
		if ($result==0)
		{
		$arrFields['mode']="new";
		$nextMode="save";
		}
		else
		{
		$nextMode="update";
		$disable_ID="disabled=\"disabled\"";
		}
	$msg = $objIncoming->getValue('msg');
	// assign saved data to view
	if($result==0) //not saved
	{
	$t_strNewId  =    $arrFields['t_strDocId'];
	$dateReceived =    $arrFields['dateReceived'];
	$documentDate =    $arrFields['documentDate'];
	$DocNum = stripslashes($arrFields['t_strDocNum']);
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    stripslashes($arrFields['t_strSubject']);
	$t_intOriginUnit = $arrFields['actionUnitSwitch'];
	if($t_intOriginUnit=="2") $t_intOriginId =  $arrFields['cmbOfficeCode'];
	else 	if($t_intOriginUnit=="3") $t_intOriginId = $arrFields['cmbAgency'];
	$oTag=	$arrFields['oTag'];
	$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
	$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
	$t_strSender =     stripslashes($arrFields['t_strSender']);
	$deadline =       $arrFields['deadline'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$t_intContainer2 =  $arrFields['t_intContainer2'];
	$t_intContainer3 =  $arrFields['t_intContainer3'];
	$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
	$rsContainer2 = $objIncoming->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objIncoming->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =  stripslashes($arrFields['t_strRemarks']);
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	$t_contdoc =  $arrFields['t_contdoc'];
	}
	else//saved
	{
		$rsDocument= $objIncoming->getDocument($arrFields['t_strDocId']);
		$t_strNewId = $rsDocument[0]['documentId'];
		$dateReceived =    $rsDocument[0]['dateReceived'];
		$documentDate =    $rsDocument[0]['documentDate'];
		$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
		$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
		$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
		$t_strSubject =    $rsDocument[0]['subject'];
		$t_intOriginId =   $rsDocument[0]['originId'];
		$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
		$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
		$oTag=$rsDocument[0]['originUnit'];
		//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
		//else 
		if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
		else $t_intOriginUnit=2;
		
		$t_strSender =     $rsDocument[0]['sender'];
		$deadline =         $rsDocument[0]['deadline'];
		$t_intContainer =  $fileLocation['containerId'];
		$t_intContainer2 = $fileLocation['container2Id'];
		$t_intContainer3 = $fileLocation['container3Id'];
		$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
		$rsContainer2 = $objIncoming->getContainerTwo($t_intContainer, $t_intContainer2);
		$rsContainer3 = $objIncoming->getContainerThree($t_intContainer2, $t_intContainer3);
		$t_strRemarks =    $rsDocument[0]['remarks'];
		$t_intConfidential =   $rsDocument[0]['confidential'];
		$t_contdoc =   $rsDocument[0]['controlleddoc'];
		$t_cd_copyno = $rsDocument[0]['contdoc_copyno'];
		$t_cd_copyholder = $rsDocument[0]['contdoc_copyHolder'];
		$t_cd_mannerdisposal = $rsDocument[0]['contdoc_mannerOfDisposal'];
		$t_cd_revisionno = $rsDocument[0]['contdoc_revisionno'];
		$t_cd_personres = $rsDocument[0]['contdoc_personUnitResposible'];
		$t_cd_withrawalno = $ctrlcontdocs['withrawalno'];
		$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
		$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
		$DocNum = $rsDocument[0]['docNum'];
	}
}

elseif($arrFields['mode']=='update')
{
	$result=$objIncoming->updateDocument($arrFields);
	$msg = $objIncoming->getValue('msg');
	$disable_ID="disabled=\"disabled\"";
	
	if($result==0)
	{
	$arrFields['mode']="edit";
	$nextMode="update";
	$t_strNewId  =    $arrFields['t_strDocId'];
	$DocNum = stripslashes($arrFields['t_strDocNum']);
	$dateReceived =    stripslashes($arrFields['dateReceived']);
	$documentDate =    $arrFields['documentDate'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    stripslashes($arrFields['t_strSubject']);
	$t_intOriginUnit = $arrFields['actionUnitSwitch'];
	
	if($t_intOriginUnit=="2") $t_intOriginId =  $arrFields['cmbOfficeCode'];
	else 	if($t_intOriginUnit=="3") $t_intOriginId = $arrFields['cmbAgency'];
	$oTag=	$arrFields['oTag'];

	$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
	$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
	
	$t_strSender =     stripslashes($arrFields['t_strSender']);
	$deadline =       $arrFields['deadline'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
	$rsContainer2 = $objIncoming->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objIncoming->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =   stripslashes($arrFields['t_strRemarks']);
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_contdoc =  $arrFields['t_contdoc'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else
	{
	$nextMode="edit";
	
	$rsDocument= $objIncoming->getDocument($arrFields['t_strDocId']);
		$t_strNewId = $rsDocument[0]['documentId'];
		$dateReceived =    $rsDocument[0]['dateReceived'];
		$documentDate =    $rsDocument[0]['documentDate'];
		$t_intDocTypeId =  $rsDocument[0]['documentTypeId'];
		$rsDocTypeAbbrev = $objIncoming->getDocType($t_intDocTypeId);
		$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
		$t_strSubject =    $rsDocument[0]['subject'];
		$t_intOriginId =   $rsDocument[0]['originId'];
		$rsOrigin = $objIncoming->getOriginOffice($t_intOriginId);
		$rsOriginDetails = $objIncoming->getOfficeDetails($t_intOriginId);
		$oTag=$rsDocument[0]['originUnit'];
		//if($rsDocument[0]['originUnit']=="office") $t_intOriginUnit=2;
		//else 
		if($rsDocument[0]['originUnit']=="agency") $t_intOriginUnit=3;
		else $t_intOriginUnit=2;
		$t_strSender =     $rsDocument[0]['sender'];
		$deadline =         $rsDocument[0]['deadline'];
		$t_intContainer =   $rsDocument[0]['fileContainer'];
		$t_intContainer2 =   $rsDocument[0]['fileContainer2'];
		$t_intContainer3 =   $rsDocument[0]['fileContainer3'];
		$rsContainer = $objIncoming->getContainer($objIncoming->get("office"),$t_intContainer);
		$rsContainer2 = $objIncoming->getContainerTwo($t_intContainer, $t_intContainer2);
		$rsContainer3 = $objIncoming->getContainerThree($t_intContainer2, $t_intContainer3);
		$t_strRemarks =    $rsDocument[0]['remarks'];
		$t_intConfidential =   $rsDocument[0]['confidential'];
		$t_contdoc =   $rsDocument[0]['controlleddoc'];
		$t_cd_copyno = $rsDocument[0]['contdoc_copyno'];
		$t_cd_copyholder = $rsDocument[0]['contdoc_copyHolder'];
		$t_cd_mannerdisposal = $rsDocument[0]['contdoc_mannerOfDisposal'];
		$t_cd_revisionno = $rsDocument[0]['contdoc_revisionno'];
		$t_cd_personres = $rsDocument[0]['contdoc_personUnitResposible'];
		$t_cd_withrawalno = $ctrlcontdocs['withrawalno'];
		$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
		$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
		$DocNum = $rsDocument[0]['docNum'];
	$t_intConfidential = ($t_intConfidential == '1')?'1':'0';
	if($t_intConfidential==1) $objOutgoing->transferFile($t_strNewId,$t_intConfidential);  
	}

}
else //New
{
	$t_strNewId=$objIncoming->getNewId();
	$t_intDocTypeId=-1;
	$t_intOriginId=-1;
	$dateReceived = date("Y-m-d");
	$disable_ID="";
	$nextMode="save";
}

$ctrlcontdocs = $file->getControlledDocsById($t_strNewId);
$t_contdoc = $ctrlcontdocs['controlleddoc'];
$t_cd_copyno = $ctrlcontdocs['copyno'];
$t_cd_copyholder = $ctrlcontdocs['copyholder'];
$t_cd_mannerdisposal = $ctrlcontdocs['mannerOfDisposal'];
$t_cd_revisionno = $ctrlcontdocs['revisionno'];
$t_cd_personres = $ctrlcontdocs['personUnitResponsible'];
$t_cd_withrawalno = $ctrlcontdocs['withrawalno'];

$arDocType=$objIncoming->getDocType("");
$arContainer=$objIncoming->getContainer($objIncoming->get("office"),"");
$arOrigin=$objIncoming->getOriginOffice("");
$arOffices=$objIncoming->getOfficeFromExeOffice();
$arManagedOffice=$objIncoming->getManagedOffice($objIncoming->get('empNum'));
if($_GET['mode']=='filecontainer')
{
	?><select name="t_intContainer" class="caption">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
	  <?
	  }
	  ?>
        </select><?	
		exit(1);
}
?>
 	<? if($msg<>""){
		//echo "messge=".$msg."<br>".strpos($msg,"DDuplicate entry");
		if(strpos($msg,"Duplicate entry")!==false){
		?>
		 <script type="text/javascript">
		 var currentID = $('#t_strDocId').val().split('-');
		 currentID[2] = pad(5, (parseInt(currentID[2])+1), '0');
		 var newID = currentID.join('-');
		 $('#t_strDocId').val(newID);
		 $("form#frmIncoming").submit();
		 //if(confirm("Document ID already existing. Get new ID?")==true){
		 //    $("#idcontainerincoming").load("showIncoming.php?mode=new #incidcontainerincoming");
		 //}	
		 function pad(w, str, pad) { 
			return (w <= str.length) ? str : pad(w, pad + str, pad )
		 }	 
		 </script>
		<?
		}
		else{
	?>
		<div class="pane">
			<div class="ui-widget" style="width:40%">
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
					<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Notice: </strong><? echo $msg;?></p>
				</div>
			</div>
			</div>
		<script type="text/javascript">
			$(".pane").fadeOut(3000,function(){
				 $(function(){ 
				  $.ajax({ 
					url: "manageBarcodeConf.php",
					data: "mode=select&employee_num=<? echo $objList->get('userID'); ?>",
					success: function(data){ 
					 if(data==1){ printBarcode("<? echo $t_strNewId;?>","<? echo $group[0]; ?>"); }
					}
				  });
				});					
			})
		</script>
	<? }} ?>
	<? if($arrFields["mode"]=="save" || $arrFields["mode"]=="update"|| $arrFields["mode"]=="view"){	?>
	<table align="center" width="605px" class="documentContainer">
	<tr><td>
	<table align="center" width="605px" class="datawrap">
	<style type="text/css">
		.tdpads{
			padding: 2px 5px;
			white-space: nowrap;
			text-align: right !important;
		}
		.tdbor{
			border-right: 1px solid #6699cc;
		}
		.td4{
			vertical-align: top;
			padding-left: 7px;
		}
	</style>
	<tr><td>
	<table align="center" width="600px" class="metadata" >
		<tr class="metabutton">
			<td colspan="2" class="containerlabel">Document Details</td>
			<td style="text-align:right" colspan="2">
				<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
				<? if($objIncoming->isCustodian($objIncoming->get("empNum"),$addedByOfficeId) && $arrFields['edit']!="false"){ ?>
					<input style="cursor:pointer" type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showIncoming.php?mode=edit&id=<? echo $t_strNewId;?>&src=<?php echo $arrFields['src']; ?>','<?=$hashId; ?>');" class="btn" />&nbsp;
					<input style="cursor:pointer" type="button" name="addForm" value="Add New" onClick="getData('showIncoming.php?mode=new','<?=$hashId; ?>');" class="btn" />
					<input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />	
				<?php } else if($objIncoming->get('office') === 'RMS' && $objIncoming->get('userType')===1){ ?>
					<input style="cursor:pointer" type="button" id="btnEditDocNo" value="Edit Info" onClick="editMinDocDetails('<?=$t_strNewId;?>','<?=$DocNum; ?>','<?=$hashId ?>');" class="btn"/>&nbsp;	
				<?php } else if($objIncoming->get('office') == 'OASECFALA' ){ ?> 
					<input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />&nbsp;
				<?php } ?>
				<?php endif; ?> <!-- endif action = 54 -->

				<?php if($objIncoming->get('office') == 'RMS' ): ?> 
					<input style="cursor:pointer" type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showIncoming.php?mode=edit&id=<? echo $t_strNewId;?>&src=<?php echo $arrFields['src']; ?>','<?=$hashId; ?>');" class="btn" />&nbsp;
					<input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />&nbsp;
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<th class="tdpads" width="100px">Document ID :</th>
			<td width="200px"><? echo $t_strNewId;?></td>
			<th class="tdpads" width="120px">Document Type :</th>
			<td class="tdbor" width="180px"><? echo $rsDocTypeAbbrev[0]['documentTypeAbbrev'];?></td> 
	</tr>
	<tr>
		<th  class="tdpads" width="97">Document Date :</th>
		<td id="docDateViewMode">
			<span><? echo $documentDate;?></span>
			<input type="text" id="documentDateClone" value="<? echo $documentDate;?>" style="display:none">
		</td>
		<th class="tdpads" >Date Received :</th>
		<td class="tdbor" ><? echo date('j F Y g:h:i a',strtotime($dateReceived));?></td>
	</tr>
	<tr>
		<th class="tdpads">Doc No. : </th>
		<td class="tdpads" id="docNoViewMode"><? echo $DocNum;?></td>
		<td></td>
		<td class="tdbor"></td>		
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td></td>
		<td></td>
		<td class="tdbor"></td>		
	</tr>
	<tr>
		<th class="tdpads">Subject : </th>
		<td class="tdbor" style="vertical-align: top;" colspan="3" rowspan="2" id="docSubjViewMode"><? echo $t_strSubject;?></td>	
	</tr>
	<tr>
	<th>&nbsp;</th>
	</tr>
	
	<tr>
		<th class="tdpads">Origin : </th>
		<td><? if($t_intOriginUnit=="2") echo $rsOriginDetails[0]['oName'];
				else echo $rsOrigin[0]["officeName"];
				?></td>
		<th class="tdpads" width="97">Sender :</th>
		<td class="tdbor"><? echo $t_strSender;?></td>
	</tr>
	
	<tr>
		<th class="tdpads">Deadline : </th>
		<td><? echo $deadline;?></td>
		<th class="tdpads">Remarks :</th>
		<td class="tdbor" rowspan="2"><? echo $t_strRemarks;?></td>
	</tr>
	
	<?php if($addedByOfficeId==$_SESSION['office']): ?>
	<!-- Location.. Removed when condition is true -->
	<tr>
		<th class="tdpads">File Container : </th>
		<td colspan="2">&nbsp;
			<? echo ucwords($fileLocation['label1'].'> '.$fileLocation['label2'].'> '.$fileLocation['label3']);?>
			<?php $empdetails = $file->getEmpDetails($fileLocation['updatedby']);?>
			<?php if($fileLocation['updatedby'] != null): ?>
				<br><small><i>
					Last Modified By: <?=$empdetails['fname'].' '.$empdetails['lname']?> (<?=date('Y-m-d', strtotime($fileLocation['updateddate']))?>)
				</i></small>
			<?php endif; ?>
		</td>
	</tr>

	<!-- Begin Controlled Document ..Removed when condition is true -->
	<tr>
		<td nowrap class="td4" style="text-align: right;"><b>Controlled Document :</b> <br><?=$ctrlcontdocs['description']?></td>
			<td id="td1" style="padding-left: 7px;"></td>
			<td id="td2" style="padding-left: 7px;"></td>
			<td id="td3" style="padding-left: 7px;" class="tdbor"></td>
			<?php 
				if($ctrlcontdocs['controlleddoc']==1){
					echo '<td class="td4"><b>Copy no: </b><br>'.$ctrlcontdocs['copyno'].'</td>';
					echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
					echo '<td class="td4 tdbor"><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
				}else if($ctrlcontdocs['controlleddoc']==2){
					echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
					echo '<td class="td4"><b>Manner of Disposal: </b><br>'.$ctrlcontdocs['mannerOfDisposal'].'</td>';
					echo '<td class="td4 tdbor"></td>';
				}else if($ctrlcontdocs['controlleddoc']==3){
					echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
					echo '<td class="td4"><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
					echo '<td class="td4 tdbor"></td>';
				}else if($ctrlcontdocs['controlleddoc']==4 || $ctrlcontdocs['controlleddoc']==5){
					echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
					echo '<td class="td4"></td>';
					echo '<td class="td4 tdbor"></td>';
				}else if($ctrlcontdocs['controlleddoc']==6){
					echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
					echo '<td class="td4"><b>Issue/ Withrawal Control no.: </b><br>'.$ctrlcontdocs['withrawalno'].'</td>';
					echo '<td class="td4 tdbor"></td>';
				}else{
					echo '<td class="td4"></td><td class="td4"></td><td class="td4 tdbor"></td>';
				}
			 ?>
	</tr>
	<?php endif; ?>
	<!-- End Controlled Document -->

	<!-- Begin Related Docs Notification -->
	<style>
	.tooltip {
	    position: relative;
	    display: inline-block;
	    border-bottom: 1px dotted black;
	}

	.tooltip .tooltiptext {
	    visibility: hidden;
	    width: 120px;
	    background-color: #fff;
	    color: #fff;
	    text-align: center;
	    border-radius: 6px;
	    padding: 5px 0;

	    
	    position: absolute;
	    z-index: 1;
	}

	.tooltip:hover .tooltiptext {
	    visibility: visible;
	}
	</style>
	<!-- End Related Docs Notification -->
	 <tr><th class="tdpads">Related Docs:</th><td colspan="3" class="tdbor"></td></tr>
					 <tr><td>&nbsp;</td><td colspan="3" class="tdbor"><div class="relatedDocs"><?
					 	$arrRelated=null;
					 	if($t_strReferenceId==''){
					 		$t_strReferenceId = $objIncoming->getRelatedDocsByParentDoc($t_strNewId);
					 		if(count($t_strReferenceId)){
					 			$t_strReferenceId = $t_strReferenceId["documentId"].','.$t_strReferenceId["referenceId"];
					 			$t_strReferenceId = str_replace($t_strNewId.',', '', $t_strReferenceId);
					 		}
					 	}
					 	if (strpos($t_strReferenceId, ',') !== false) {
					 		$t_strReferenceId = rtrim(str_replace(",", "','", str_replace(' ', '', $t_strReferenceId)), ",'");
					 		$t_strReferenceId = "'".$t_strReferenceId."'";
					 		$arrRelated=$objIncoming->getRelatedDocsByRefId($t_strReferenceId);
					 	}else{
					 		$arrRelated=$objIncoming->getRelatedDocs($t_strNewId);
							$arrRelated1=$objIncoming->getOriginRelatedRecord($t_strNewId);
							if(count($arrRelated) < 1){
								$arrRelated = $arrRelated1;
							}
					 	}
						
						$cRelated=count($arrRelated);
						if($cRelated>0) {
							$intStatus = $objIncoming->getDocumentStatus($arrRelated[0]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							//COMMENTED:: echo "<a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[0]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[0]["documentId"]."</a>";
							
							// Begin Related Document Notification
							echo "<div class=\"tooltip\"><a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[0]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[0]["documentId"]."</a>";
							echo "<div style='overflow:auto; width:400px;height:320px;background-color: rgba(255, 255, 255, 0)' class='tooltiptext'></div></div>";
							echo "<script id='tooltipdiv' type='text/javascript'>
									$('.tooltiptext').load('showHistory.php?mode=incoming&docID=".$t_strNewId."&div=actionbodyincoming');
								</script>";
							// End Related Document Notification
							}
						for($cntRelated=1;$cntRelated<$cRelated;$cntRelated++)
						{
						
						$intStatus = $objIncoming->getDocumentStatus($arrRelated[$cntRelated]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							// COMMENTED:: echo ", <a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[$cntRelated]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[$cntRelated]["documentId"]."</a>";
							
							// begin Related Document Notification
							echo ", <div class=\"tooltip\"><a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[$cntRelated]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[$cntRelated]["documentId"]."</a>";
							echo "<div id='tooltipdiv' style='overflow:auto; width:400px;height:320px;background-color: rgba(255, 255, 255, 0)' class='tooltiptext'></div></div>";
							echo "<script type='text/javascript'>
									$('.tooltiptext').load('showHistory.php?mode=incoming&docID=".$t_strNewId."&div=actionbodyincoming');
								</script>";
							// End Related Document Notification
						    
						    ////echo ", ".$arrRelated[$cntRelated]["documentId"];
						}
						?>
						</div>
					 </td></tr>
		<tr>
			<th class="tdpads">Owned by : </th>
			<td><? echo $addedByOfficeId;?></td>
			<th>&nbsp;</th>
			<td class="tdbor">&nbsp;</td>
		</tr>
		<?php if($addedByOfficeId!=$_SESSION['office']): ?>
		<!-- Begin editable Controlled Document -->
		<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
		<tr>
			<th style="vertical-align: top;" nowrap class="tdpads"><br>Controlled Document:</th>
			<td style="vertical-align: top;">&nbsp;<br>
				<?php $ctrlDocs = $file->getControlledDocDesc(); ?>
		      	<select name="t_contdoc_editmode" class="caption" id="sel_contdoc_editmode">
				  	<option value="0"></option>
				  	<?php foreach($ctrlDocs as $contdoc): ?>
				  	<option value="<?=$contdoc['id']?>" <?=($t_contdoc==$contdoc['id']) ? 'selected' : ''?>><?=$contdoc['description']?></option>
				  	<?php endforeach; ?>
				</select>
				<br>
				<input type="button" id="tblControlleddoc_inc" style="background-color: #6699cc !important;color: #fff !important;border: 1px solid #000;" value="Update Controlled Document">
			</td>
			<td style="vertical-align: top;" class="tdbor" colspan="3">
				<div id="conta_editmode" <?=($t_contdoc==1)? '' : 'style="display: none;"'?> >
	      			<b>Copy No.</b>&nbsp;&nbsp;
	      			<br>
	      			<input type="text" id="txt_conta_copyno_editmode" value="<?=$t_cd_copyno?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
	      		<div id="contb_editmode" <?=($t_contdoc==1 || $t_contdoc==2 || $t_contdoc==4 || $t_contdoc==5)? '' : 'style="display: none;"'?> >
	      			<b>Copy Holder/s</b>&nbsp;&nbsp;
	      			<input type="text" id="txt_contb_copyholder_editmode" value="<?=$t_cd_copyholder?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
	      		<div id="contc_editmode" style="display: grid; <?=($t_contdoc==2)? '' : 'display: none;' ?>" >
	      			<b>Manner of Disposal</b>
	      			<input type="text" id="txt_contc_mannerdisposal_editmode" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
	      		<div id="contd_editmode" <?=($t_contdoc==3)? '' : 'style="display: none;"'?> >
	      			<b>Revision no.</b>&nbsp;&nbsp;
	      			<br>
	      			<input type="text" id="txt_contd_revisionno_editmode" value="<?=$t_cd_revisionno?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
	      		<div id="conte_editmode" style="display: grid; <?=($t_contdoc==3 || $t_contdoc==1)? '' : 'display: none;' ?>" >
	      			<b>Person/Unit Responsible</b>	
	      			<input type="text" id="txt_conte_unitres_editmode" value="<?=$t_cd_personres?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
	      		<div id="contf_editmode" style="display: grid; <?=($t_contdoc==6)? '' : 'display: none;' ?>" >
	      			<b>Issue/ Withrawal Control #</b>	
	      			<input type="text" id="txt_withrawal_editmode" value="<?=$t_cd_withrawalno?>" style="width: 180px !important;height: 12px !important;">
	      		</div>
			</td>
		</tr>
		<?php endif; ?> <!-- endif action = 54 -->
		<!-- End editable Controlled Document -->
		<tr><td colspan="4" class="tdbor">&nbsp;</td></tr>
		<!-- Begin editable Table -->
		<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
		<tr>
			<th class="tdpads">Cabinet:</th>
			<td colspan="3" class="tdbor">
				<select id="selcabinet_edit_inc" class="caption">
					<option value="-1"> </option>
					<?php for($i=0;$i<sizeof($arContainer);$i++) { ?>
					<option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th class="tdpads">Drawer:</th>
			<td colspan="3" class="tdbor">
				<select id="seldrawer_edit_inc" class="caption">
					<option value="-1"> </option>
					<?php for($i=0;$i<sizeof($arContainer2);$i++) { ?>
					<option value="<? echo $arContainer2[$i]['container2Id']; ?>" <? if($arContainer2[$i]['container2Id']== $t_intContainer2) echo "selected"; ?> > <? echo $arContainer2[$i]['label'];?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th class="tdpads">Container:</th>
			<td colspan="3" class="tdbor">
				<select id="selcontainer_edit_inc" class="caption">
					<option value="-1"> </option>
					<?php for($i=0;$i<sizeof($arContainer3);$i++) { ?>
					<option value="<? echo $arContainer3[$i]['container3Id']; ?>" <? if($arContainer3[$i]['container3Id']== $t_intContainer3) echo "selected"; ?> > <? echo $arContainer3[$i]['label'];?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr><td></td><td class="tdbor" colspan="3"><input type="button" id="tblupdateLocation_inc" style="background-color: #6699cc !important;color: #fff !important;border: 1px solid #000;" value="Update File Location"></td></tr>
		<!-- End editable Table -->
		<?php endif; ?> <!-- endif action = 54 -->
		<?php endif; ?>

	 <tr><td colspan="4" class="tdbor">&nbsp;</td></tr>
	<? if ($t_intConfidential)
	{
	?>
	<tr>
		<td colspan="4" style="text-align:center"><div class="ui-widget" align="center">
				<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; width:140px;"> 
					<p align="left"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
					<strong>Alert:</strong> Confidential.</p>
				</div>
			</div></td>
	</tr>
	<tr>
	<td colspan="4">&nbsp;</td>
	</tr>
	<?
	}
	?>
	
	</table>  <!-- end of metadata -->
	
	</td></tr></table> <!-- end of datawrap -->
	
	</td></tr>
	<tr><td><br /></td></tr>
	<!--COMMENTED:: <tr><td>
	
<table class="datawrap" width="605px" >
	<tr><td>
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="incomingfilelinkimg" /><a onClick="javascript:toggleWindowAjax2('filebodyincoming','incomingfilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyincoming','addInc','<? echo $addedByOfficeId; ?>');">Manage Files</a><div id="loading_widget" style="float:right; display:none;">LOADING&nbsp;<img src="css/images/ajax-loader.gif" /></div></td>
		<td><div id="addInc"></div></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
		<div id="filebodyincoming" style="display:none">
		</div>		
		</td></tr>
	    </table></td></tr>

	</table>

	</td></tr> -->

	<!-- begin manage files -->
	<tr><td>
		<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
		<table class="datawrap" width="605px" >
			<tr>
				<td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel">
								<img src="images/menu_strip_down_arrow.gif" id="incomingfilelinkimg" />
								<a onClick="javascript:toggleWindowAjax2('filebodyincoming','incomingfilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyincoming','addInc','<? echo $addedByOfficeId; ?>');"> Manage Files</a>
								<div id="loading_widget" style="float:right; display:none;"><!-- LOADING&nbsp;<img src="css/images/ajax-loader.gif" /> --></div>
							</td>
							<td>
								<div id="addInc" style="text-align: right;"></div>
							</td>
						</tr>
						<tr>
							<td width="590px" colspan="3">
								<div id="filebodyincoming"><!-- ############################# File Content #####################################--></div>
							</td>
						</tr>
	    			</table>
	    		</td>
	    	</tr>
		</table>
		<?php endif; ?> <!-- endif action = 54 -->
	</td></tr>
	<style type="text/css"> #addInc Input[type="submit"] { display: initial !important;} </style>
	<!-- end manage files -->

	<?php if($cRelated>0): ?>
	<!-- begin related documents -->
	<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
	<style type="text/css"> .scrollable {height: 128px !important; }</style>
	<tr><td>
	
		<table class="datawrap" width="605px" >
			<tr>
				<td>
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel">
								<img src="images/menu_strip_down_arrow.gif" id="incomingfilelinkimgreldocs" />
								<a onClick="javascript:toggleWindowAjax002('filebodydoc','incomingfilelinkimgreldocs');"> Related Document Files</a>
								<!-- <div id="loading_widget2" style="float:right; display:none;">LOADING&nbsp;<img src="css/images/ajax-loader.gif" /></div> -->
								<span style="font-size: 10px;background-color: rgb(251, 9, 21);padding: 3px;">New</span>
							</td>
							<td>
								<div id="addIncreldocs" style="text-align: right;">&nbsp;</div>
							</td>
						</tr>
						<?php for($cntRelated=0;$cntRelated<$cRelated;$cntRelated++){ ?>
						<tr>
							<td width="590px" colspan="3">
								<div class="filebodydoc" id="filebodyincomingreldocs1"><!-- #### File Content ####--></div>
								<script type="text/javascript">
									showManageFilesReldocs('<? echo $arrRelated[$cntRelated]["documentId"]; ?>','filebodyincomingreldocs1','addIncreldocs','<? echo $addedByOfficeId; ?>');
								</script>

							</td>
						</tr>
						<?php } ?>
	    			</table>
	    		</td>
	    	</tr>
		</table>

	</td></tr>
	<?php endif; ?> <!-- endif action = 54 -->
	<!-- end related documents -->
	<?php endif; ?>

	<tr><td>	<table class="datawrap" width="605px" >
	<tr><td>
	<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="incominghistorylinkimg" /><a href="javascript:getData('showAction.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=docubodyincoming&historydiv=actionbodyincoming','docubodyincoming');" onClick="return toggleWindowAjax('docubodyincoming','incominghistorylinkimg');" id="incominghistorylink">Update Action</a></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
			<!-- 	***************************		history				********************************************	-->
			<div id="docubodyincoming" align="center">
		
			</div>
			<!-- 	***************************		upload image	    ********************************************	-->	
		</td>
	</tr>
	</table>
	<?php endif; ?> <!-- endif action = 54 -->
		</td>
	</tr>
	</table>
	</td></tr>
	<tr><td>
	<table class="datawrap" width="605px" >
	<tr><td>
	<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
	<table width="600px" class="metadata">
	<tr class="metabutton">
		<td width="10px"></td>
		<td class="containerlabel"><img src="images/menu_strip_down_arrow.gif" id="incomingactionlinkimg" /><a href="javascript:getData('showHistory.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=actionbodyincoming','actionbodyincoming');" onClick="return toggleWindowAjax('actionbodyincoming','incomingactionlinkimg');" id="incomingactionlink">View History</a></td>
	</tr>
	<tr>
		<td></td>
		<td width="590px">
			<!--COMMENTED <div id="actionbodyincoming" align="center" style="visibility:hidden"> -->
			<!-- begin collapse view history -->
			<div id="actionbodyincoming" align="center"><div id="bodyincoming"></div>
			<!-- end collapse view history -->
			</div>
		</td>
	</tr>
	</table>
	<?php endif; ?> <!-- endif action = 54 -->
		</td>
	</tr>
	</table>
	
	</td></td>
	
	</table>	
	<?
	}
	else   // MODE = new or edit
	{
	/*if($objIncoming->get('office')=='ITD'){
	print_r($arOrigin);
	}*/
	?>

	 <script language="javascript" type="text/javascript">
	  var arOrigin = new Array(<? echo sizeof($arOrigin);?>);
	  <?
  	  $intColumnNum=sizeof($arOrigin[0])/2;
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  echo "arOrigin[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {
	  	  for($x=0;$x<$intColumnNum;$x++)
		  {	  		
			echo "arOrigin[".$i."][".$x."]='".$arOrigin[$i][$x]."';\n";
		  }
	  }
      //echo "alert(arOrigin[0][0]);";
	  ?>
		function getPerson(i)
		{
			$.ajax({ 
			  url: "showData.php",
			  type: "GET",
			  data: "mode=getagencycontact&agencyid="+i,
			  dataType: "json",
			  success: function(data){ 
			  	//alert(data);
				//$('#t_strSender').val(data);
				$('#t_strSender').val(data['contactPerson']);
			   //getData('showIncoming.php?mode=view&id='+docID,'doclist');
			  }
			});		  
			//return false;
			/*
			if(i==-1)  document.getElementById("t_strSender").value="";
			for(x=0;x<arOrigin.length;x++)
			{
				
			if(i==arOrigin[x][0]) 
				{
					//alert(i+'=='+arOrigin[x][0]+'='+arOrigin[x][4]);
					document.getElementById("t_strSender").value=arOrigin[x][4];
					document.getElementById("oTag").value="agency";
				}
			}
		*/
		}
		
		 var arOriginOutgoing = new Array(<? echo sizeof($arOffices);?>);
	  <?

	  for($i=0;$i<sizeof($arOffices);$i++)
	  echo "arOriginOutgoing[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOffices);$i++)
	  {
	  	echo "arOriginOutgoing[".$i."][0]='".$arOffices[$i]['officeCode']."';\n";
	  	echo "arOriginOutgoing[".$i."][1]='".$arOffices[$i]['officeName']."';\n";
	  	echo "arOriginOutgoing[".$i."][2]='".$arOffices[$i]['empNumber']."';\n";
		echo "arOriginOutgoing[".$i."][3]='".$arOffices[$i]['head']."';\n";
		echo "arOriginOutgoing[".$i."][4]='".$arOffices[$i]['title']."';\n";
		echo "arOriginOutgoing[".$i."][5]='".$arOffices[$i]['officeTag']."';\n";
	  }

	  ?>
		function getPerson2(i)
		{
		if(i==-1)  document.getElementById("t_strSender").value="";
		for(x=0;x<arOriginOutgoing.length;x++)
		{
		if(i==arOriginOutgoing[x][0]) 
			{
				document.getElementById("t_strSender").value=arOriginOutgoing[x][3];
				document.getElementById("oTag").value=arOriginOutgoing[x][5];
				//document.getElementById("cmbOriginOutgoing").title=arOriginOutgoing[x][1];;	
			}
		}
		getData("showIncoming.php?mode=getGroup&officeCode="+i+"&officeGroup=<? echo $t_intGroupCode;?>","incGroupDiv");
		}
		    function x () {
                var oTextbox = new AutoSuggestControl(document.forms['frmIncoming'].t_strDocType, new RemoteStateSuggestions());        
            } 
			//x();
			/*
			<? if($t_intOriginUnit=="2"){?>
			$("#cmbOfficeCode").change();
			<? }
			else {?>
			$("#cmbAgency").change();
			<? }?>
			*/
			$("#documentOwner").change(function(){
			var src = $(this).val();
			$("#idcontainerincoming").load("showIncoming.php?getOfficeID="+src+ " #idcontainerincoming",function(){
			 	alert("Please save again");
			 });
			});
	  </script>
<form action="javascript:check(document.getElementById('frmIncoming'),'<?=$hashId; ?>','showIncoming.php');" name="frmIncoming" id="frmIncoming" onsubmit="return checkConfi(document.getElementById('frmIncoming'));">

<table align="center" width="700px" class="tblforms">
	<tr>
		<th width="97">Document ID </td>
		<td width="180">
			<div id="idcontainerincoming">
				<input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required" <? echo $disable_ID;?> title="required" style="width:150px" ></div>
				<input type="hidden" name="mode" value="<? echo $nextMode;?>">
		</td>
		<th width="150">Date Received</td>
		<td width="150">
			<input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d no-animation ?>" id="dateReceived" name="dateReceived" value="<? echo $dateReceived;?>">
		</td>
  </tr>

  <tr> 
    <th>Document Type</td>
    <td><table><tr>
		<td><div id="divDocType"><select name="cmbDocType" id="t_strDocType" class="caption" style="width:250px">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeDesc'];?></option>
	  <?
	  }
	  ?>
        </select>
		<!--input type="text" name="t_strDocType" id="t_strDocType" value="<? echo $t_strDocType;?>" autocomplete=OFF /-->
        </div>
        </td>
		<td>
		<? if($objIncoming->get('userType')==1) {?>
		<span class='ui-icon ui-icon-plus' title='Add New Document Type' onclick="displayDialog('showLibrary_popup.php?mode=doctype');"></span><? }?>
		</td></tr></table>
		<!--
		<INPUT onblur="clearSuggest()"; 
onkeydown="return tabfix(this.value,event,this);" id=s 
onkeyup="suggest(this.value,event);" type=text> </P>
<P class=nodisplay><LABEL>kIndex</LABEL> <INPUT id=keyIndex class=nodisplayd 
type=text> </P>
<P class=nodisplay><LABEL>rev</LABEL> <INPUT id=sortIndex class=nodisplayd 
type=text> </P>
<DIV id=results></DIV>
		--></td>   
    <th>Document Date</td>
    <td><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="documentDate" name="documentDate" value="<? echo $documentDate;?>"></td>
  </tr>
   <tr> 
      	<th valign="top">Reference ID</td>
      	<td><input type="text" name="t_strReferenceId" id="t_strReferenceId" value="<? echo $t_strReferenceId;?>">
      		<br><span class="required" style="text-transform:none">&nbsp;To add more than one Reference Id, use comma to separate<br>(i.e. SMPL-17-00000, SMPL-17-00001, SMPL-17-00002)</span><br><br></td>
      	<td><!--DWLayoutEmptyCell-->&nbsp;</td>
      	<td><!--DWLayoutEmptyCell-->&nbsp; </td>
      
    </tr>
  
  <tr>
  <th>Doc No.</td>
  <td colspan="2" ><input type="text" name="t_strDocNum" id="t_strDocNum" value="<? echo $DocNum;?>" maxlength="15" /><span class="required" style="text-transform:none">&nbsp;(i.e. AO 001)</span></td>
  <td><span class="required" >&nbsp;</span></td>
  </tr>
  <tr> 
    <th>Subject</td>
    <td colspan="3"><div id="subjBlock"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3" title="required"><? echo $t_strSubject; ?></textarea></div></td>
  </tr>
    <tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><br /><br /></td>  
  </tr>
  <tr> 
   <th valign="top">Origin</th>
   <td colspan="3"></td>
   </tr>
   <tr>
   	<td></td>
	<td colspan="3"><table width="100%"  border="0">
			  <tr>
				<td width="21%" height="23" align="left">
				<input type="hidden" name="oTag" id="oTag" value="<? echo $oTag;?>"/>
				<input name="actionUnitSwitch" type="radio" value="2" onClick="toggleOffice(this.form);" <? if($t_intOriginUnit=="2") echo "checked=\"checked\"";?>> Office:</td>
				<td width="79%"> <SELECT name="cmbOfficeCode" id="cmbOfficeCode" onClick="toggleOffice(this.form);" onchange="getPerson2(this.value)" style="width:320px" <? if($t_intOriginUnit=="3") echo "disabled=\"disabled\"";?>>
	<OPTION value="-1">&nbsp;</OPTION>
	 <? $sOffice=sizeof($arOffices);
		  for($i=0;$i< $sOffice;$i++)
		  {    
		  ?>
	  <option value="<? echo $arOffices[$i]['officeCode']; ?>" <? if($arOffices[$i]['officeCode']== $t_intOriginId) echo "selected"; ?> title="<? echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);?>"> <? echo $arOffices[$i]['officeName'];?></option>
	  <?
	  }
	  ?>
		</SELECT>
		</td>
			  </tr>
			  <tr>
			  	<td style="padding-left:2em;">Group</td>
				<td><div id="incGroupDiv"><select id="cmbGroupCode" name="cmbGroupCode"></select></div></td>
			  </tr>
			</table>
		<table width="100%"  border="0">
			  <tr>
				<td width="21%" align="left"><input name="actionUnitSwitch" type="radio" value="3"  onClick="toggleAgency(this.form);" <? if($t_intOriginUnit=="3") echo "checked=\"checked\"";?>>
				  Agency Name: </td>
				<td width="79%">
				<? 
function multi_implode($glue, $pieces)
{
    $string='';
    
    if(is_array($pieces))
    {
        reset($pieces);
        while(list($key,$value)=each($pieces))
        {
            $string.=$glue.multi_implode($glue, $value);
        }
    }
    else
    {
        return $pieces;
    }
    
    return trim($string, $glue);
}
				
				//print_r($arOrigin);
				//$tmp = multi_implode(':',$arOrigin);
				//$tmp = serialize($arOrigin);
				//print_r( $arOrigin);
				for($i=0;$i<sizeof($arOrigin);$i++)
					$tmp = $tmp.$arOrigin[$i]['originId']."|".$arOrigin[$i]['officeName']."|".$arOrigin[$i]['contact']."|".$arOrigin[$i]['address']."|".$arOrigin[$i]['contactPerson']."|".$arOrigin[$i]['ownerOffice'].",";
				//echo $tmp;
				?>
			
			<table><tr>
		<td><div id="divAgency"> <? displayAgency($t_intOriginUnit,$t_intOriginId);?> </div></td>
		<td><span class='ui-icon ui-icon-plus' title='Add New Agency' onclick="displayDialog('showLibrary_popup.php?origunit=<? echo $t_intOriginUnit;?>&arorig=<? echo $tmp;?>&origid=<? echo $t_intOriginId;?>&mode=agency');"></span></td></tr></table>
			
			</td>
			  </tr>
			</table>
	</td>
	
	</tr>

	<tr>
    <th valign="top">Sender</td>
    <td valign="top"><input type="text" size="30" class="caption" name="t_strSender" id="t_strSender" value="<? echo $t_strSender;?>"></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
 	<th valign="top">Deadline</td>
    <td valign="top"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="deadline" name="deadline" value="<? echo $deadline;?>"> </td>
	<th rowspan="2">Remarks</td>
    <td rowspan="2" valign="top"><textarea cols="25" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"><? echo $t_strRemarks;?></textarea></td>
  </tr>
  <tr>
      <th valign="top">Cabinet</td>
      <td valign="top">
      <table><tr><td>
      <div id="divContainer">
	  <select name="t_intContainer" id="selcabinet" class="caption">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
	  <?
	  }
	  ?>
        </select></div></td><td><!--span class='ui-icon ui-icon-plus' title='Add File Container' onclick="displayDialog('showLibrary_popup.php?origunit=<? echo $t_intOriginUnit;?>&arorig=<? echo $tmp;?>&origid=<? echo $t_intOriginId;?>&mode=filecontainer');"></span--></td></tr></table>
	  </td>
  </tr>

  <tr>
      <th valign="top">Drawer</td>
      <td valign="top">
      <table><tr><td>
      <div id="divContainer">
	  <select name="t_intContainer2" id="seldrawer" class="caption">
	  	<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer2);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer2[$i]['container2Id']; ?>" <? if($arContainer2[$i]['container2Id']== $t_intContainer2) echo "selected"; ?> > <? echo $arContainer2[$i]['label'];?></option>
	  <?
	  }
	  ?>
	  </select></div></td><td><!--span class='ui-icon ui-icon-plus' title='Add File Container' onclick="displayDialog('showLibrary_popup.php?origunit=<? echo $t_intOriginUnit;?>&arorig=<? echo $tmp;?>&origid=<? echo $t_intOriginId;?>&mode=filecontainer');"></span--></td></tr></table>
	  </td>
  </tr>
  <tr>
      <th valign="top">Container</td>
      <td valign="top">
      <table><tr><td>
      <div id="divContainer">
	  <select name="t_intContainer3" id="selcontainer" class="caption">
	  	<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arContainer3);$i++)
	  {    
	  ?>
	  <option value="<? echo $arContainer3[$i]['container3Id']; ?>" <? if($arContainer3[$i]['container3Id']== $t_intContainer3) echo "selected"; ?> > <? echo $arContainer3[$i]['label'];?></option>
	  <?
	  }
	  ?>
	  </select></div></td><td><!--span class='ui-icon ui-icon-plus' title='Add File Container' onclick="displayDialog('showLibrary_popup.php?origunit=<? echo $t_intOriginUnit;?>&arorig=<? echo $tmp;?>&origid=<? echo $t_intOriginId;?>&mode=filecontainer');"></span--></td></tr></table>
	  </td>
  </tr>

  <tr> 
      <td valign="top" colspan=2><b>Controlled Document</b>&nbsp;&nbsp;
      	<?php $ctrlDocs = $file->getControlledDocDesc(); ?>
      	<select name="t_contdoc" class="caption" id="sel_contdoc">
		  	<option value="0"></option>
		  	<?php foreach($ctrlDocs as $contdoc): ?>
		  	<option value="<?=$contdoc['id']?>" <?=($t_contdoc==$contdoc['id']) ? 'selected' : ''?>><?=$contdoc['description']?></option>
		  	<?php endforeach; ?>
		</select>
      </td>
      <!-- Begin Controlled Saving Document -->
      <td colspan=2>
      		<div id="conta" <?=($t_contdoc==1)? '' : 'style="display: none;"'?> >
      			<b>Copy No.</b>&nbsp;&nbsp;
      			<br>
      			<input type="text" name="txt_conta_copyno" value="<?=$t_cd_copyno?>" style="width: 180px !important">
      		</div>
      		<div id="contb" <?=($t_contdoc==1 || $t_contdoc==2 || $t_contdoc==4 || $t_contdoc==5)? '' : 'style="display: none;"'?> >
      			<b>Copy Holder/s</b>&nbsp;&nbsp;
      			<br>
      			<input type="text" name="txt_contb_copyholder" value="<?=$t_cd_copyholder?>" style="width: 180px !important">
      		</div>
      		<div id="contc" style="display: grid; <?=($t_contdoc==2)? '' : 'display: none;' ?>" >
      			<b>Manner of Disposal</b>
      			<br>
      			<input type="text" name="txt_contc_mannerdisposal" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important">
      		</div>
      		<div id="contd" <?=($t_contdoc==3)? '' : 'style="display: none;"'?> >
      			<b>Revision no.</b>&nbsp;&nbsp;
      			<br>
      			<input type="text" name="txt_contd_revisionno" value="<?=$t_cd_revisionno?>" style="width: 180px !important">
      		</div>
      		<div id="conte" style="display: grid; <?=($t_contdoc==3 || $t_contdoc==1)? '' : 'display: none;' ?>" >
      			<b>Person/Unit Responsible</b>	
      			<input type="text" name="txt_conte_unitres" value="<?=$t_cd_personres?>" style="width: 180px !important">
      		</div>
      		<div id="contf" style="display: grid; <?=($t_contdoc==6)? '' : 'display: none;' ?>" >
      			<b>Issue/ Withrawal Control #</b>	
      			<input type="text" name="txt_withrawal" value="<?=$t_cd_withrawalno?>" style="width: 180px !important">
      		</div>
      </td>
      <!-- End Controlled Saving Document -->
  </tr>

  <tr> 
      <th valign="top">Confidential</td>
      <td valign="top"><input type="checkbox" value="1" name="t_intConfidential" id="t_intConfidential" <? 
	  if ($t_intConfidential=='1') echo "checked=\"checked\"";
	  ?>></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
  </tr>
  <tr> <!--$arManagedOffice=$objIncoming->getManagedOffice();-->
      <th valign="top">Document Owner</td>
      <td valign="top"><select id="documentOwner" name="documentOwner" <? echo $disable_ID;?>>
	   <?
	  for($i=0;$i<sizeof($arManagedOffice);$i++)
	  {
	  $managedOffice=(trim($arManagedOffice[$i]["groupCode"])=="")?$arManagedOffice[$i]["officeCode"]:$arManagedOffice[$i]["groupCode"];
	  ?>
	  <option value="<? echo $managedOffice; ?>" <? if($managedOffice== $t_strManagedOffice) echo "selected"; ?> > <? echo $managedOffice;?></option>
	  <?
	  }
	  ?>
	  </select></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
  </tr>

  <tr>
  	<td colspan=4>
  		<center>
  			<?php if($arrFields['mode'] == "new"): ?>
  				<input type="submit" value="Save" class="btn" onclick="">
  			<?php else: ?>
  				<input type="submit" value="Update" class="btn" onclick="">
  				<input type="button" value="Cancel" class="btn" onclick="getData('showIncoming.php?mode=view&id=<? echo $t_strNewId;?>','inc');" />
  			<?php endif; ?>
  			
  			<input type="reset" value="Clear" class="btn">
  		</center>
  	</td>
  </tr>
</table>
</form>

<? } ?>
<!-- <a class='barcode' href="showbarcode.php?code=<? #echo $t_strNewId;?>"></a> -->
<a class='barcode'></a>
<script src="facebox/facebox.js" type="text/javascript"></script>
<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$('#bodyincoming').load('showHistory.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=actionbodyincoming');

$('#docubodyincoming').load('showAction.php?mode=incoming&docID=<? echo $t_strNewId;?>&div=docubodyincoming&historydiv=actionbodyincoming');
showManageFiles('<? echo $t_strNewId; ?>','filebodyincoming','addInc','<? echo $addedByOfficeId; ?>');
$(function() {
	$('#dateReceived').datetimepicker({
		timeFormat: "hh:mm tt",
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true
	});
	
	$('#documentDate,#documentDateClone').datepicker({
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true		
	});

	$('a[rel*=facebox]').facebox({
	 loading_image : '/facebox/loading.gif',
	 close_image   : '/facebox/closelabel.gif'
	});
$("a.barcode").fancybox({
		 'frameWidth' : 620, 
		 'frameHeight': 450, 
		 'overlayShow': true, 
		 'overlayOpacity': 0.7,
		 'hideOnContentClick': false
});

});

	function editMinDocDetails(docID,docNO,hashID){  
	  if($('#btnEditDocNo').val() == 'Save'){ 
		$.ajax({ 
		  url: "showIncoming.php",
		  type: "POST",
		  data: "mode=update&blnMinDocInfo=true&docID="+docID+"&docNO="+$('#intDocNum').val()+'&docDate='+$('#documentDateClone').val()+'&docSubj='+$('#t_strSubject').val(),
		  success: function(data){ 
		   getData('showIncoming.php?mode=view&id='+docID,'doclist');
		  }
		});		  
		return false;
	  }
	  var subjOrigVal = $('#docSubjViewMode').html(); //get val before load manipulation
  	  $("#docSubjViewMode").load("showIncoming.php?mode=new #subjBlock",function(){
	  	var newDocNoInput = "<input type='text' name='intDocNum' id='intDocNum' value='"+docNO+"' />";
		$('#btnEditDocNo').val("Save");		
		$('#docNoViewMode').html(newDocNoInput);
		$('#docDateViewMode span').hide();
		$('#documentDateClone,.ui-datepicker-trigger').css('display','inline-block');		  
		$('#t_strSubject').val(subjOrigVal);
	  });
	}



function printBarcode(barcode,group){
 $.get('showbarcode.php',{ code: barcode,group: group }, function(data) {
//   $("a.barcode").click();
    // var thePopup = window.open( $("a.barcode").attr('href'), "barcode", "menubar=0,location=0,height=700,width=700" );
    var thePopup = window.open( 'showbarcode.php?code=<? echo $t_strNewId;?>', "barcode", "menubar=0,location=0,height=700,width=700" );
    //$('#popup-content').clone().appendTo( thePopup.document.body );
    //thePopup.print();
 });
}


function displayDialog(lnk)
	{
	var url = lnk;
	url = url;
	jQuery.facebox({ ajax: url });	
	}
</script>

  <script type="text/javascript">
  	$(document).ready(function(){
  		$("#selcabinet").change(function(){
  			var id=$(this).val();

  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+id+"&drawerid=",
  				cache: false,
  				success: function(html){
  					$("#seldrawer").html(html);
  				} 
  			});
  		});

  		$("#seldrawer").change(function(){
  			var id=$(this).val();

  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid="+id,
  				cache: false,
  				success: function(html){
  					$("#selcontainer").html(html);
  				} 
  			});
  		});

  		// begin Edit mode File Location
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+$("#selcabinet_edit_inc").val()+"&drawerid=<?=$t_intContainer2?>",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_edit_inc").html(html);
  				} 
  			});
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid=<?=$t_intContainer2?>&contid=<?=$t_intContainer3?>",
  				cache: false,
  				success: function(html){
  					$("#selcontainer_edit_inc").html(html);
  				} 
  			});
  		$("#selcabinet_edit_inc").change(function(){
  			var id=$(this).val();
  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+id+"&drawerid=",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_edit_inc").html(html);
  				} 
  			});
  		});

  		$("#seldrawer_edit_inc").change(function(){
  			var id=$(this).val();

  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid="+id,
  				cache: false,
  				success: function(html){
  					$("#selcontainer_edit_inc").html(html);
  				} 
  			});
  		});
  		// End Edit mode File Location

  		// Begin editable cabinet
  		$('#tblupdateLocation_inc').click(function(){
  			cab = $('#selcabinet_edit_inc').val();
  			draw = $('#seldrawer_edit_inc').val();
  			cont = $('#selcontainer_edit_inc').val();

  			$.ajax ({
  				type: "GET",
  				url: "showData.php?",
  				data: "mode=updateLocation_outg&docid=<?=$t_strNewId?>&cabinet="+cab+"&drawer="+draw+"&container="+cont,
  				cache: false,
  				success: function(data){
  					alert('Location has been updated');
  				} 
  			});
  		});
  		// End editable cabinet

  		// begin editable controlled document
  		$('#td1, #td2, #td3').hide();
  		$('#tblControlleddoc_inc').click(function(){
  			controlleddoc = $('#sel_contdoc_editmode').val();
  			copyno = $('#txt_conta_copyno_editmode').val();
			copyholder = $('#txt_contb_copyholder_editmode').val();
			mannerdisposal = $('#txt_contc_mannerdisposal_editmode').val();
			revisionno = $('#txt_contd_revisionno_editmode').val();
			unitres = $('#txt_conte_unitres_editmode').val();
			withrawal = $('#txt_withrawal_editmode').val();

			$.ajax ({
  				type: "GET",
  				url: "showData.php?",
  				data: "mode=updateControlledDoc_inc&docid=<?=$t_strNewId?>&controlleddoc="+controlleddoc+"&copyno="+copyno+"&copyholder="+copyholder+"&mannerdisposal="+mannerdisposal+"&revisionno="+revisionno+"&unitres="+unitres+"&withrawal="+withrawal,
  				cache: false,
  				success: function(data){
  					alert('Controlled Document has been updated.');
  					$('#td1, #td2, #td3').show();
  					$('.td4').hide();
  				} 
  			});
			
  		});

  		$('#sel_contdoc_editmode').change(function() {
  			contdoc_edit = $(this).val();
  			if(contdoc_edit==1){
  				$('#conta_editmode').show();
		  		$('#contb_editmode').show();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').hide();
		  		$('#conte_editmode').show();
		  		$('#contf_editmode').hide();
  			}else if(contdoc_edit==2){
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').show();
		  		$('#contc_editmode').show();
		  		$('#contd_editmode').hide();
		  		$('#conte_editmode').hide();
		  		$('#contf_editmode').hide();
		  	}else if(contdoc_edit==3){
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').hide();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').show();
		  		$('#conte_editmode').show();
		  		$('#contf_editmode').hide();
		  	}else if(contdoc_edit==4){
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').show();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').hide();
		  		$('#conte_editmode').hide();
		  		$('#contf_editmode').hide();
		  	}else if(contdoc_edit==5){
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').show();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').hide();
		  		$('#conte_editmode').hide();
		  		$('#contf_editmode').hide();
		  	}else if(contdoc_edit==6){
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').hide();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').show();
		  		$('#conte_editmode').hide();
		  		$('#contf_editmode').show();
  			}else{
  				$('#conta_editmode').hide();
		  		$('#contb_editmode').hide();
		  		$('#contc_editmode').hide();
		  		$('#contd_editmode').hide();
		  		$('#conte_editmode').hide();
		  		$('#contf_editmode').hide();
  			}
  		});
  		// end editable controlled document

  		/*begin select Controlled docments*/
  		$('#sel_contdoc').change(function() {
  			contdoc = $(this).val();
  			if(contdoc==1){
  				$('#conta').show();
		  		$('#contb').show();
		  		$('#contc').hide();
		  		$('#contd').hide();
		  		$('#conte').show();
		  		$('#contf').hide();
  			}else if(contdoc==2){
  				$('#conta').hide();
		  		$('#contb').show();
		  		$('#contc').show();
		  		$('#contd').hide();
		  		$('#conte').hide();
		  		$('#contf').hide();
		  	}else if(contdoc==3){
  				$('#conta').hide();
		  		$('#contb').hide();
		  		$('#contc').hide();
		  		$('#contd').show();
		  		$('#conte').show();
		  		$('#contf').hide();
		  	}else if(contdoc==4){
  				$('#conta').hide();
		  		$('#contb').show();
		  		$('#contc').hide();
		  		$('#contd').hide();
		  		$('#conte').hide();
		  		$('#contf').hide();
		  	}else if(contdoc==5){
  				$('#conta').hide();
		  		$('#contb').show();
		  		$('#contc').hide();
		  		$('#contd').hide();
		  		$('#conte').hide();
		  		$('#contf').hide();
		  	}else if(contdoc==6){
  				$('#conta').hide();
		  		$('#contb').hide();
		  		$('#contc').hide();
		  		$('#contd').show();
		  		$('#conte').hide();
		  		$('#contf').show();
  			}else{
  				$('#conta').hide();
		  		$('#contb').hide();
		  		$('#contc').hide();
		  		$('#contd').hide();
		  		$('#conte').hide();
		  		$('#contf').hide();
  			}
  		});
  		/*end select Controlled docments*/
  		
  	});
  </script>