new Vue({
    el: '#recieved',

    data: {
        recdata: {},
    },

    ready: function() {
        this.getrecievedDocs();
    },

    methods: {
        getrecievedDocs: function() {
            this.$http.get('layout/getdocrecieved.php', function(recdata) {
                this.$set('recdata', recdata);
            });
        },
    }
});