    $(document).ready(function() {
        $('#divalert').hide();
        $( "#btnlogin" ).click(function() {
            var PUserId = $('#t_strUsername').val();
            var PassWord = $('#t_strPassword').val();
            $.ajax({
                type: "GET",
                url: "layout/getdocrecieved.php?mode=getdiffUserLogin&uname="+PUserId+"&pword="+PassWord,
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if(data==false){
                        $('#divalert').show();
                    }else{
                        $('#divalert').hide();
                        $('#loginModal').modal('hide');
                        $('#txtempid').val(data.empnumber);
                        $('#spanoffice').html(data.office);
                        $('#spanempname').html(data.empname);
                        $('#txtempname').val(data.empname);
                    }
                }
            });
        });

        $('.bootstrap-switch-handle-on').html('RECIEVE');
        $('#tblrec').hide();
        setTimeout(function() { $("#tblrec").DataTable({
            "language": {
                       "info": "Showing _START_ to _END_ of first _TOTAL_ entries"
                     }
        }); }, 2000);
        
            $.ajax({
                type: "GET",
                url: "layout/getdocrecieved.php?mode=getRecievedDocs",
                dataType: "json",
                success: function(response){
                    $.each(response, function(i, obj) {
                        // console.log(obj.documentId);
                        var stat = '';
                        if(obj.status == 0)
                            stat = 'Inc';
                        else
                            stat = 'Outg';
                        $('#tbodyrec').append('<tr class="odd gradeX '+obj.docstat+'"><td><input type="hidden" id="txtprio" value="0"/><input type="hidden" id="txtflag" value="0"/><input type="checkbox" class="'+obj.docstat+'" name="chkdoc" value="'+obj.documentId+'" class="group-checkable" data-set="#tblrec .checkboxes" /><input type="hidden" id="txtchkbox" value="'+obj.historyId+'" /> </th><td nowrap id="tdaction"> <i id="iflag"></i> &nbsp; <i id="iprio"></i> </td><td>'+obj.senderId+'</td><td nowrap>'+obj.documentId+'</td><td nowrap>'+obj.docNum+'</td><td>'+obj.doctype+'</td><td>'+obj.status+'</td><td>'+obj.dateSent+'</td><td style="display: none">'+obj.docstat+'</td><td style="display: none">'+obj.dateSentTime+'</td></tr>');
                        // $('#tbodyrec').append('<tr class="odd gradeX "><td><input type="checkbox" class="" name="chkdoc" value="" class="group-checkable" data-set="#tblrec .checkboxes" /><input type="hidden" id="txtchkbox" value="'+obj.documentId+'"> </th><td> &nbsp; </td><td>'+obj.senderId+'</td><td nowrap>'+obj.documentId+'</td><td nowrap>'+obj.docNum+'</td><td>'+stat+'</td><td>'+obj.dateSent+'</td><td style="display: none"></td></tr>');

                    });
                // $('#tbodyrec').append('<tr class="odd gradeX"><td><input type="checkbox" class="group-checkable" data-set="#tblrec .checkboxes" /> </th><td> &nbsp; </td><td></td><td></td><td></td><td></td><td></td><td></td><td style="display: none"></td><td style="display: none"></td></tr>');
                }
            });
        $('#tblrec').show();

        //RECIEVED BY CHECKBOXES
        var arrElem = '';
        var arrHist = '';
        $('#linkReceive').click(function () {
            $('#txtempid').val($('#txtsessempid').val());
            $('#spanoffice').html($('#txtsessoffc').val());
            $('#spanempname').html($('#txtsessempname').val());
            $('#txtempname').val($('#txtsessempname').val());
            $('#seldocid').empty();
            arrElem = '';
            arrHist = '';
            var oTable = $('#tblrec').dataTable();
            var rowcollection =  oTable.$("input[name=chkdoc]:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value = $(elem).val();
                    if($(elem).attr("class")=='unread'){
                        $('#seldocid').append('<span class="label label-sm label-default">'+checkbox_value+'</span>&nbsp;');
                        arrElem = arrElem + ',' + $(elem).val();
                        arrHist = arrHist + ',' + $(this).closest('tr').find('#txtchkbox').val();
                    }else{
                        $(elem).attr('checked',false);
                    }
                });
        });

        //FLAG
        $('#linkFlag').click(function () {
            var oTable = $('#tblrec').dataTable();
            var rowcollection =  oTable.$("input[name=chkdoc]:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value = $(elem).val();
                    fvalue = $(this).closest('tr').find('#txtflag').val();
                    hisid = $(this).closest('tr').find('#txtchkbox').val();
                    console.log("layout/getdocrecieved.php?mode=updateAction&table=tblHistory&field=flag&confield=historyId&historyid"+hisid);
                    $.ajax({
                        type: "GET",
                        url: "layout/getdocrecieved.php?mode=updateAction&table=tblHistory&field=flag&confield=historyId&historyid"+hisid,
                        dataType: "json",
                        success: function(response){
                            if(fvalue==0){
                                $(this).closest('tr').find('#txtflag').val(1);
                                $(this).closest('tr').find('#iflag').addClass('fa fa-flag');
                            }else{
                                $(this).closest('tr').find('#txtflag').val(0);
                                $(this).closest('tr').find('#iflag').removeClass('fa fa-flag');
                            }
                        }
                    });
                });
        });

        //PRIORITY
        $('#linkPriority').click(function () {
            var oTable = $('#tblrec').dataTable();
            var rowcollection =  oTable.$("input[name=chkdoc]:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value = $(elem).val();
                    fvalue = $(this).closest('tr').find('#txtprio').val();
                    if(fvalue==0){
                        $(this).closest('tr').find('#txtprio').val(1);
                        $(this).closest('tr').find('#iprio').addClass('fa fa-exclamation');
                    }else{
                        $(this).closest('tr').find('#txtprio').val(0);
                        $(this).closest('tr').find('#iprio').removeClass('fa fa-exclamation');
                    }
                });
        });

        $('#receive_continue').click(function () {
            $.ajax({
                type: "GET",
                url: "layout/getdocrecieved.php?mode=receiveDocument&receivedid="+arrElem+"&historyid="+arrHist+"&empid="+$('#txtempid').val()+"&empname="+$('#txtempname').val(),
                dataType: "json",
                success: function(response){
                    $.each(response, function(i, item) {
                        $('#tbodyrec :input[value="'+item+'"]').closest('tr').attr('class', 'gradeX read odd');
                    });
                }
            });
        });

        $('#switch-change').on('switchChange.bootstrapSwitch', function (event, state) {
            var oTable = $('#tblrec').DataTable();
            if(state){
                oTable.order([ 8, 'desc' ]).draw();
                oTable.$('.read').show();
            }else{
                oTable.order([ 7, 'desc' ]).draw();
                oTable.$('.read').hide();
            }
        });
    });