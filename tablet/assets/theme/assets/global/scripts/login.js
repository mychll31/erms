new Vue({

    el: '#login',

    data: {
        uname: '',
        pass: '',
        error: '',

        checkUser: []
    },

    computed: {
    },

    methods: {
        checkLogin: function(e) {
            e.preventDefault();
            this.$http.get('database/database.php?mode=checkUser&uname=' + this.uname + '&pass=' + this.pass, function(checkUser) {
                this.$set('checkUser', checkUser);
            });
        },
    }

});