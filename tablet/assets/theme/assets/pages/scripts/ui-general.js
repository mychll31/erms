var UIGeneral = function () {

    var handlePulsate = function () {
        if (!jQuery().pulsate) {
            return;
        }

        if (App.isIE8() == true) {
            return; // pulsate plugin does not support IE8 and below
        }

        if (jQuery().pulsate) {
            jQuery('#pulsate-regular').pulsate({
                color: "#bf1c56"
            });

            jQuery('#pulsate-once').click(function () {
                $('#pulsate-once-target').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-crazy').click(function () {
                $('#pulsate-crazy-target').pulsate({
                    color: "#fdbe41",
                    reach: 50,
                    repeat: 10,
                    speed: 100,
                    glow: true
                });
            });
        }
    }
    var totalpageno = 10;
    var handleDynamicPagination = function() {
        $('#dynamic_pager_demo1').bootpag({
            paginationClass: 'pagination',
            next: '<i class="fa fa-angle-right"></i>',
            prev: '<i class="fa fa-angle-left"></i>',
            total: 6,
            page: 1,
        }).on("page", function(event, num){
            $("#dynamic_pager_content1").html("Page " + num + " content here"); // or some ajax content loading...
        });
        // console.log('totalpageno = ' + totalpageno);
        $('#dynamic_pager_demo2').bootpag({
            paginationClass: 'pagination pagination-sm',
            next: '<i class="fa fa-angle-right"></i>',
            prev: '<i class="fa fa-angle-left"></i>',
            total: totalpageno,
            page: 1,
            maxVisible: 6 
        }).on('page', function(event, num){
            // console.log('on page totalpageno = ' + totalpageno);
            var $input = $('#txtpageno').val(num);
            var e = document.createEvent('HTMLEvents');
            e.initEvent('input', true, true);
            $input[0].dispatchEvent(e);
        });
    }

    return {
        //main function to initiate the module
        init: function (total_url) {
            handlePulsate();
            handleDynamicPagination();
            // console.log('total_url= '+total_url);
            // var totalpageno;
            // $.ajax({
            //     url: total_url,
            //     async: false,
            //     success: function (response) {
            //         totalpageno = response;
            //     },
            //     error: function(jqXHR, textStatus, errorThrown) {
            //        console.log(textStatus, errorThrown);
            //     }
            // });
            // handleDynamicPagination(total_url);
        }

    };

}();

jQuery(document).ready(function() { 
    var total_url = 'http://erms.dost.gov.ph/showPendingList?search=&page=0&limit=10&mode=0&userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val();
    UIGeneral.init(total_url);
    $('#btnfilter').click(function(){
        // console.log('filter ='+$('#txtsearch').val());
        var total_url = 'http://erms.dost.gov.ph/showPendingList?search='+$('#txtsearch').val()+'&page=0&limit=10&mode=1&userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val();  
        UIGeneral.init(total_url);
    });
    $('#btnclear').click(function(){
        // console.log('clear');
        var total_url = 'http://erms.dost.gov.ph/showPendingList?search=&page=0&limit=10&mode=0&userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val();
        UIGeneral.init(total_url);
    });
});