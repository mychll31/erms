var vmUser = new Vue({
    el: '#login',

    data: {
        uname: '',
        pass: '',
        error: false,
        message: '',
    },

    methods: {
        doLogin: function(e) {
            e.preventDefault();
            if(this.uname == '' || this.pass == ''){
                this.error = true;
                this.message = 'Username/Password must not be blank.';
            }else{
                axios.get('checkLogin/'+this.uname+'/'+this.pass).then(function (response) {
                    console.log(response.data);
                    if(response.data.login_status=='success'){
                        this.error = false;
                        window.location = "received-documents";
                    }else{
                        this.error = true;
                    }
                    this.message = response.data.login_status;
                }.bind(this));
            }
        }
    }
});

