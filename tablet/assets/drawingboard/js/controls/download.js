DrawingBoard.Control.Download = DrawingBoard.Control.extend({

	name: 'download',

	initialize: function() {
		this.$el.append('<button class="drawing-board-control-download-button"></button>');
		this.$el.on('click', '.drawing-board-control-download-button', $.proxy(function(e) {
			img = this.board.downloadImg();
			var docids = [];
			var historyIds = [];
            $("input:checkbox[id=chckreceived]:checked").each(function () {
                docids.push($(this).val());
                historyIds.push($(this).data('id'));
            });
			$.ajax({
                    type: "POST",
                    data: {docid: docids, historyid: historyIds, receivedby: $('#txtreceivedby').val(), img: img},
                    url: '../showPendingList?userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val()+'&recoffice='+$('#seloffice').val()+'&mode=saveImage',
                    success: function(data){
                        console.log(data);
                        $('#modalSignature').modal('hide');
                        $('#modalRec').modal('show');
                    }});
			e.preventDefault();
		}, this));
	}

});
