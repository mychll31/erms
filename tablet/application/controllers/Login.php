<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	var $arrTemplateData;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
    }

	public function index()
	{
		$this->load->view('users/login', $this->arrTemplateData);
	}

	public function authenticate($strUsername, $strPassword)
	{
		if(isset($strUsername)):
			$hrmisUser=getHRMISAccounts(); //HRMIS USERS
			$ermsUser=$this->Login_model->getEmployeeAccounts(); //ERMS USERS

			if(in_array($strUsername,array_column($hrmisUser,'username')) ):
				if($hrmisUser[$strUsername]['password']==md5($strPassword)):
					$result = $hrmisUser[$strUsername];
					$this->set_session_login_data($result['name'],$result['empNumber'],$result['username'],$result['office'], 'hrmis');
					$uaccess = $this->Login_model->userAccess($result['empNumber']);
					if(count($uaccess) > 0):
						$this->session->set_userdata(array('isadmin' => $uaccess['isadmin'], 'iscustodian' => $uaccess['iscustodian'], 'office' => $uaccess['officeCode'], 'grp' => $uaccess['groupCode']));
					else:
						$this->session->set_userdata(array('isadmin' => '0', 'iscustodian' => '0', 'office' => $result['office'], 'grp' => ''));
					endif;
					echo json_encode( array('login_status'=>'success') );
				else:
					echo json_encode( array('login_status'=>'Wrong Password') );
				endif;
			elseif(in_array($strUsername,array_column($ermsUser,'username')) ):
				$result = searchForId($strUsername, $ermsUser, 'username');
				if($result['password']==md5($strPassword)):
					$this->set_session_login_data($result['name'],$result['empNumber'],$result['username'],$result['office'], 'erms');
					$uaccess = $this->Login_model->userAccess($result['empNumber']);
					if(count($uaccess) > 0):
						$this->session->set_userdata(array('isadmin' => $uaccess['isadmin'], 'iscustodian' => $uaccess['iscustodian'], 'office' => $uaccess['officeCode'], 'grp' => $uaccess['groupCode']));
					else:
						$this->session->set_userdata(array('isadmin' => '0', 'iscustodian' => '0', 'office' => $result['office'], 'grp' => ''));
					endif;
					echo json_encode( array('login_status'=>'success') );
				else:
					echo json_encode( array('login_status'=>'Wrong Password') );
				endif;
			else:
				echo json_encode( array('login_status'=>'Invalid User') );
			endif;
		endif;
	}
	
	function set_session_login_data($name,$empno,$username,$loginby)
	{
		$sessData = array(
			 'sessname'  			=> $name,
			 'sessempNumber'		=> $empno,
			 'sessusername'  		=> $username,
			 'sessloginby'  		=> $loginby,
			 'sessBoolLoggedIn' 	=> TRUE,
			);
		$this->session->set_userdata($sessData);
	}

	public function Logout()
	{
		$this->session->sess_destroy();
		session_destroy();			
		redirect('http://erms.dost.gov.ph/tablet/login');
	}

}
