<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Received extends CI_Controller {

	var $arrTemplateData;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Received_model');
		is_logged_in();
    }

	public function index()
	{
		$this->arrTemplateData = array();
		$this->template->load('template/main_layout', 'receives/index', $this->arrTemplateData);
	}

	public function jsonData($slimit, $yr, $month)
	{
		$month = sprintf("%02d", $month);
		$pendings = $this->Received_model->getPendingDocs($_SESSION['sessoffice'], $_SESSION['sessempNumber'], $yr, $month, $slimit);
		foreach($pendings as $key=>$pending):
			// TODO:: check tblHistory.dateReceived
			$doc = $this->Received_model->ifReceived($pending['historyId']);
			$pendings[$key]['isReceived'] = $doc;
		endforeach;
		echo json_encode($pendings);
	}

	public function getTotalPageno($yr, $month)
	{
		$month = sprintf("%02d", $month);
		$totalPending = $this->Received_model->totalPending($_SESSION['sessoffice'], $_SESSION['sessempNumber'], $yr, $month);
		// echo $totalPending;
		$totalPending = floor($totalPending / 10) + 1;
		echo json_encode($totalPending);
	}

	public function isReceived($historyId)
	{
		$doc = $this->Received_model->ifReceived($historyId);
		echo json_encode($doc);
	}

	public function test()
	{
		$this->load->view('test');
	}

	public function getFilterDates()
	{
		echo json_encode($this->Received_model->getFilterDate());
	}

	public function add()
	{
		$arrData = json_decode($_GET['data']);
		foreach($arrData as $data):
			$recData = array(
				'historyId' => $data[0],
				'empNumber' => $_SESSION['sessempNumber'],
				'documentId' => $data[1],
				'dateReceived' => date('Y-m-d H:i:s'),
				'receivedBy' => $_SESSION['sessname'],
				'empID' => 0
			);
			$err = $this->Received_model->addDocReceived($recData);
			$HistoryData = array(
				'dateReceived' => date('Y-m-d H:i:s'),
				'receivedBy' => $_SESSION['sessname']
			);
			$err = $this->Received_model->updateHistory($data[0], $HistoryData);
		endforeach;
	}


}
