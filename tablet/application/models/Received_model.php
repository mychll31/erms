<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Received_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}

	public function addDocReceived($arrData)
	{
		$this->db->insert('tblDocumentReceived', $arrData); 	
		return $this->db->insert_id();
	}

	public function updateHistory($historyId, $HistoryData)
	{
		$this->db->where('historyId', $historyId);  
		$this->db->update('tblHistory', $HistoryData);
		return $this->db->affected_rows(); 	
	}
	
	public function getPendingDocs($officename, $empnumber, $year, $month, $slimit)
	{
		$sql = "SELECT 
					tblHistory.historyId,
					tblHistory.senderId,
					tblHistory.documentId,
					tblDocument.docNum,
					tblDocument.status,
					tblHistory.dateAdded,
					tblDocumentType.documentTypeAbbrev
				FROM `tblHistory`
				LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
				LEFT JOIN tblDocumentType on tblDocumentType.documentTypeId = tblDocument.documentTypeId
				WHERE (recipientId LIKE '".$officename."' OR recipientId LIKE '".$empnumber."') and tblHistory.dateAdded LIKE '".$year."-".$month."%'
				GROUP BY tblHistory.documentId ORDER BY tblHistory.historyId DESC LIMIT ".$slimit.",10";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function totalPending($officename, $empnumber, $year, $month)
	{
		$sql = "SELECT *  FROM `tblHistory`
				LEFT JOIN tblDocument on tblDocument.documentId = tblHistory.documentId
				WHERE (recipientId LIKE '".$officename."' OR recipientId LIKE '".$empnumber."') and tblHistory.dateAdded LIKE '".$year."-".$month."%'
				GROUP BY tblHistory.documentId ORDER BY tblHistory.historyId";
				// return $sql;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function ifReceived($historyId)
	{
		$sql = "SELECT count(historyId) as ctr FROM `tblDocumentReceived` WHERE `historyId` = ".$historyId." LIMIT 1";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res[0]['ctr'];
	}

	public function getFilterDate()
	{
		$sql = "SELECT min(dateAdded) as mindate, max(dateAdded) as maxdate FROM tblHistory";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		$res = $res[0];
		$arrFilter = array();
		$arrYear = array();
		$arrMonth = array();
		$yrselected = date('Y');
		$monselected = date('m');
		for($yr = date('Y', strtotime($res['mindate'])); $yr <= date('Y', strtotime($res['maxdate'])); $yr++):
			$arrYear[] = array('year' => $yr);
		endfor;
		
		for($mon = 1; $mon<= 12; $mon++):
			$arrMonth[] = array('month' => sprintf("%02d", $mon), 'name' => date('F', mktime(0, 0, 0, $mon, 10)));
		endfor;
		
		return array('arrYear' => $arrYear, 'arrMonth' => $arrMonth, 'defYear' => $yrselected, 'defMonth' => $monselected, 'asdf' => 'asdf');
	}

}
