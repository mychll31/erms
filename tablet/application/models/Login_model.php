<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}
	
	public function getEmployeeAccounts()
	{
		$sql = "SELECT
					CONCAT(surname, ', ', firstname, ' ', middleInitial, '.') as name,
					userPassword as password,
					agencyCode as empNumber,
					userName as username,
					email,
					officeCode as office
				FROM tblUserAccount";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function userAccess($empNumber)
	{
		$sql = "SELECT officeCode, groupCode, admin FROM `tblCustodian` WHERE `empNumber` LIKE '".$empNumber."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		if(count($res) > 0){
			return array('isadmin' => $res[0]['admin'], 'iscustodian' => 1, 'officeCode'=> $res[0]['officeCode'], 'groupCode' => $res[0]['groupCode']);
		}else{
			return null;
		}
	}

}
