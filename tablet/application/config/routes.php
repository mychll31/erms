<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Login/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['checkLogin/(:any)/(:any)'] = 'Login/authenticate/$1/$2';
$route['logout'] = 'Login/logout';

$route['received-documents'] = 'Received/index';
$route['jsonData/(:any)/(:any)/(:any)'] = 'Received/jsonData/$1/$2/$3';
$route['isReceived/(:any)'] = 'Received/isReceived/$1';
$route['totalpageno/(:any)/(:any)'] = 'Received/getTotalPageno/$1/$2';
$route['getFilterDates'] = 'Received/getFilterDates';

#historyid, documentid
$route['receive'] = 'Received/add';
