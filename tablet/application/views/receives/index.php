<style type="text/css">
    select { font-size: 12px !important; }
    select#fixed { -webkit-appearance: none; }
    a#advsearch { text-decoration: none !important; }
    td, th{ text-align: center; }
</style>

<!-- <div id="recieved"> -->
<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>

<div class="col-md-12" id="div_rec" style="display: none;">
    <a class="btn btn-success" id="btnReceived"><i class="fa fa-envelope-o"></i> <b>RECEIVE</b> </a>
    <div class="btn-group pull-right">
        <b>Recipient: </b>
        <select class="form-control" id="seloffice">
            <option value="all" selected></option>
            <optgroup label="OFFICE">
        </select>
    </div>
    <div class="clear-fix"></div>
    <br><br>
    <div class="row">
        <input type="hidden" id="txtuserid" value="<?=$_SESSION['sessempNumber']?>">
        <input type="hidden" id="txtoffice" value="<?=$_SESSION['office']?>">
        <input type="hidden" id="txtgrp" value="<?=$_SESSION['grp']?>">
        <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblReceived">
                <thead>
                    <tr>
                        <th style="background: none !important; padding-right: 0px !important;"> &nbsp; </th>
                        <th width="10%"> Sender </th>
                        <th> DocID </th>
                        <th> docNum </th>
                        <th> DocType </th>
                        <th> Date </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <br><br><br><br><br><br><br>
</div>

<div id="modalSignature" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <?php include('signature.php'); ?>
                <br><br>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN SEARCH MODAL -->
<div id="modalRec" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Received Document</h4>
            </div>
            <div class="modal-body">
                <b>Document Id :</b>  <span id="span-docid"></span>
                <br><b>Recipient :</b> <span id="span-recipient"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" id="txtrecipient">
                <br><b>Received By :</b> &nbsp;&nbsp;
                    <input type="text" id="txtreceivedby"> <a id="btnSignature" class="btn btn-xs btn-primary">Click Here for Signature</a>
                <br>
                <small>Note: Check the "Update Action" tab for documents that require action/reply. If document was received but action is not made, the document will be considered "Unacted"</small>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="btnokay">Okay</button> <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- END SEARCH MODAL -->

<script>
    $(window).resize(function() {
      $('.page-content').css('height',$(window).height()-80);
    });
    $(document).ready(function() {
        $('.page-content').css('height',$(window).height()-80);
        // begin table body
        $('button.drawing-board-control-download-button').val('okay');
        $.get('../showPendingList?userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val()).then(
          function(data) {
                data = JSON.parse(data);
                $.each(data, function(i, item) {
                    doctype = '';
                    if(item.documentTypeAbbrev != null){
                        doctype = item.documentTypeAbbrev;
                    }
                    $('tbody').append('<tr class="odd gradeX"><td align="center"><input type="checkbox" id="chckreceived" value="'+item.documentId+'" data-id="'+item.historyId+'">'+
                                        '<td>'+item.senderId+'</td>'+
                                        '<td>'+item.documentId+'</td>'+
                                        '<td>'+item.docNum+'</td>'+
                                        '<td>'+doctype+'</td>'+
                                        '<td>'+item.dateSent+'</td>'+
                                      '</tr>');
                });
                $("#tblReceived").DataTable();
                $("div#tblReceived_info").html('Showing 1 to 10 of First 100 entries');
                $("#div_rec").show();
                $('.loading-image').hide();
          },
          function(jqXHR) {
                console.log('Error occurred: '+ jqXHR.statusText + ' ' + jqXHR.status);
          }
        ); 
        // end table body

        // begin office list
        $.get('../showPendingList?userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val()+'&mode=officeList').then(
          function(data) {
                data = JSON.parse(data);
                var isuser = 0;
                $.each(data, function(i, item) {
                    // console.log(item);
                    if(item.recipientId == 'user'){
                        if(isuser == 0){
                            $('#seloffice').append('<optgroup label="USER">');
                        } isuser = 1;
                        $('#seloffice').append('<option value="'+item.empNumber+'">'+item.officeName+'</option>');
                    }else{
                        $('#seloffice').append('<option value="'+item.recipientId+'">'+item.officeName+'</option>');
                    }
                });
          }, function(jqXHR) { console.log('Error occurred: '+ jqXHR.statusText + ' ' + jqXHR.status);}
        ); 
        // end office list

        $('#btnReceived').click(function(){
            var docids = '';
            var historyIds = '';
            $("input:checkbox[id=chckreceived]:checked").each(function () {
                docids = docids + ', ' + $(this).val();
                historyIds = historyIds + ', ' + $(this).data('id');
                console.log(" Value: " + $(this).val());
                console.log(" historyid: " + $(this).data('id'));
            });
            if(docids.length < 1){
                alert('Please select Document to receive.');
            }else{
                $('#span-docid').html(docids);
                $('#modalRec').modal('show');
            }
        });

        $('#seloffice').change(function(){
            $('#span-docid').html('');
            $('#txtrecipient').hide();
            $('#span-recipient').html($('#seloffice option:selected').text());
            $('.loading-image').show();
            $("#div_rec").hide();
            // begin table based in office
            $.get('../showPendingList?userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val()+'&recoffice='+$(this).val()).then(
              function(data) {
                    $("#tblReceived").DataTable().destroy();
                    $('tbody').html('');
                    data = JSON.parse(data);
                    $.each(data, function(i, item) {
                        doctype = '';
                        if(item.documentTypeAbbrev != null){
                            doctype = item.documentTypeAbbrev;
                        }
                        $('tbody').append('<tr class="odd gradeX"><td align="center"><input type="checkbox" id="chckreceived" value="'+item.documentId+'" data-id="'+item.historyId+'">'+
                                            '<td>'+item.senderId+'</td>'+
                                            '<td>'+item.documentId+'</td>'+
                                            '<td>'+item.docNum+'</td>'+
                                            '<td>'+doctype+'</td>'+
                                            '<td>'+item.dateSent+'</td>'+
                                          '</tr>');
                    });

                    table = $('#tblReceived').DataTable( {
                        paging: false
                    } );
                     
                    table.destroy();
                     
                    table = $('#tblReceived').DataTable( {
                        searching: false
                    } );
                    if($('#seloffice').val() == 'all'){
                        $("div#tblReceived_info").html('Showing 1 to 10 of First 100 entries');
                        $('#txtrecipient').show();
                    }
                    $("#div_rec").show();
                    $('.loading-image').hide();
              },
              function(jqXHR) {
                    console.log('Error occurred: '+ jqXHR.statusText + ' ' + jqXHR.status);
              }
            );
            // end table based in office
        });

        $('#btnokay').click(function(e){
            e.preventDefault();
            var docids = [];
            var historyIds = [];
            $("input:checkbox[id=chckreceived]:checked").each(function () {
                docids.push($(this).val());
                historyIds.push($(this).data('id'));
            });
            console.log(historyIds);
            if($('#txtreceivedby').val() == ''){
                alert("'Received By' must not be empty.");
            }else{
                $.ajax({
                    type: "POST",
                    data: {docids: docids, historyid: historyIds, receivedby: $('#txtreceivedby').val()},
                    url: '../showPendingList?userID='+$('#txtuserid').val()+'&userOffice='+$('#txtoffice').val()+'&grp='+$('#txtgrp').val()+'&recoffice='+$('#seloffice').val()+'&mode=receive',
                    success: function(data){
                        console.log(data);
                        $('#modalRec').modal('hide');
                        //begin toastr
                        Command: toastr['success']("Document Successfully Received.", "Success")

                        toastr.options = {
                          "closeButton": true,
                          "debug": false,
                          "positionClass": "toast-top-right",
                          "onclick": null,
                          "showDuration": "1000",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                        // end toastr
                    }});    
            }
            
        });

        $('#btnSignature').click(function(e){
            e.preventDefault();
            $('#modalRec').modal('hide');
            $('#modalSignature').modal('show');
        });

    });
</script>