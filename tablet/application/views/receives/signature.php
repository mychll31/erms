<noscript>JavaScript is required :(</noscript>
<link rel="stylesheet" href="<?=base_url('assets/drawingboard/dist/drawingboard.css')?>">
<style>
	/*
	* drawingboards styles: set the board dimensions you want with CSS
	*/
	.board {
		margin: 0 auto;
		width: 400px;
		height: 400px;
	}
	pre	{ display: none; }
</style>

<style data-example="2">
	#custom-board {
		width: 400px;
		height: 400px;
	}
</style>

<style data-example="4">
	#simple-board {
		width: 400px;
		height: 400px;
	}
</style>

<style data-example="1">
	#default-board {
		width: 700px;
		height: 400px;
	}
</style>

<style data-example="3">
	#custom-board-2 {
		width: 550px;
		height: 300px;
	}

	#custom-board-2 canvas {
		transform: scale(0.95);
	}
</style>

<style data-example="5">
	#title-board {
		width: 600px;
		height: 270px;
	}
	#title-board .drawing-board-canvas-wrapper {
		border: none;
		margin: 0;
	}
</style>

<style data-example="6">
	#transparent-board {
		width: 600px;
		height: 270px;
	}

	#transparent-board .drawing-board-canvas-wrapper {
		border: 1px solid gray;
	}
</style>

<div id="container">
	<div class="example" data-example="3">
		<br><br>
		<div class="board" id="custom-board-2"></div>
		<br>
	</div>
</div>

<!-- jquery is required - zepto might do the trick too -->
<!-- <script src="<?=base_url('assets/drawingboard/bower_components/jquery/dist/jquery.min.js')?>"></script> -->
<script src="<?=base_url('assets/drawingboard/bower_components/simple-undo/lib/simple-undo.js')?>"></script>

<!-- in a production environment, just include the minified script. It contains the board and the default controls (size, nav, colors, download): -->
<!--<script src="../dist/drawingboard.min.js"></script>-->

<script src="<?=base_url('assets/drawingboard/js/utils.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/board.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/control.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/color.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/drawingmode.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/navigation.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/size.js')?>"></script>
<script src="<?=base_url('assets/drawingboard/js/controls/download.js')?>"></script>

<script data-example="1">
	//create the drawingboard by passing it the #id of the wanted container
	var defaultBoard = new DrawingBoard.Board('default-board');
</script>

<script src="<?=base_url('assets/drawingboard/yepnope.js')?>"></script>
<script>
	var iHasRangeInput = function() {
		var inputElem  = document.createElement('input'),
			smile = ':)',
			docElement = document.documentElement,
			inputElemType = 'range',
			available;
		inputElem.setAttribute('type', inputElemType);
		available = inputElem.type !== 'text';
		inputElem.value         = smile;
		inputElem.style.cssText = 'position:absolute;visibility:hidden;';
		if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {
			docElement.appendChild(inputElem);
			defaultView = document.defaultView;
			available = defaultView.getComputedStyle &&
				defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
				(inputElem.offsetHeight !== 0);
			docElement.removeChild(inputElem);
		}
		return !!available;
	};

	yepnope({
		test : iHasRangeInput(),
		nope : ['fd-slider.min.css', 'fd-slider.min.js'],
		callback: function(id, testResult) {
			if("fdSlider" in window && typeof (fdSlider.onDomReady) != "undefined") {
				try { fdSlider.onDomReady(); } catch(err) {}
			}
		}
	});
</script>

<script data-example="2">
	//pass options and add custom controls to a board
	var customBoard = new DrawingBoard.Board('custom-board', {
		background: "#ff7ffe",
		color: "#ff0",
		size: 30,
		fillTolerance: 150,
		controls: [
			{ Size: { type: "range", min: 12, max: 42 } },
			{ Navigation: { back: false, forward: false } },
			'DrawingMode'
		],
		webStorage: 'local',
		droppable: true //try dropping an image on the canvas!
	});

	//There are multiple ways to add a control to a board after its initialization:
	// customBoard.addControl('Download'); //if the DrawingBoard.Control.Download class exists

	//or...
	//var downloadControl = new DrawingBoard.Control.Download(customBoard).addToBoard();

	//or...
	//var downloadControl = new DrawingBoard.Control.Download(customBoard);
	//customBoard.addControl(downloadControl);
</script>

<script data-example="3">
	var customBoard2 = new DrawingBoard.Board('custom-board-2', {
		controls: [
			'Color',
			{ Size: { type: 'dropdown' } },
			{ DrawingMode: { filler: false } },
			'Navigation',
			'Download'
		],
		size: 1,
		webStorage: 'session',
		enlargeYourContainer: true,
		droppable: true, //try dropping an image on the canvas!
		stretchImg: true //the dropped image can be automatically ugly resized to to take the canvas size
	});
</script>

<script data-example="4">
	var simpleBoard = new DrawingBoard.Board('simple-board', {
		controls: false,
		webStorage: false
	});
</script>

<script data-example="5">
	var imageBoard = new DrawingBoard.Board('title-board', {
		controls: false,
		background: 'drawingboardjs.png',
		color: '#ff0',
		webStorage: false
	});
</script>

<script data-example="6">
	//the "filler" mode currently doesn't work with transparent boards...
	//keeping default controls, replacing the DrawingMode one with a filler-less version
	var transparentBoardControls = DrawingBoard.Board.defaultOpts.controls.slice();
	transparentBoardControls.splice(DrawingBoard.Board.defaultOpts.controls.indexOf('DrawingMode'), 1, { DrawingMode: { filler: false } });
	var transparentBoard = new DrawingBoard.Board('transparent-board', {
		background: false,
		controls: transparentBoardControls
	});
</script>

<!-- totally not drawingboard-related code -->
<!-- <script src="prism.js"></script> -->
<script src="<?=base_url('assets/drawingboard/script.js')?>"></script>