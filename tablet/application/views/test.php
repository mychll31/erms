<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>
<body>
<br>
<table id="example" class="display" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>First name</th>
      <th>Last name</th>
      <th>Position</th>
      <th>Office</th>
      <th>Start date</th>
      <th>Salary</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th>First name</th>
      <th>Last name</th>
      <th>Position</th>
      <th>Office</th>
      <th>Start date</th>
      <th>Salary</th>
    </tr>
  </tfoot>
</table>
<script>
  $(document).ready(function() {

  var url = 'http://www.json-generator.com/api/json/get/bXZCNjxHCa?indent=2';

  var table = $('#example').DataTable({
    'processing': true,
    'serverSide': true,
    "pageLength": 10,
    scrollY: 200,
    scroller: {
      loadingIndicator: true
    },
    'ajax': {
      type: 'POST',
      'url': url,
      'data': function(d) {
        console.log(d.order);
        return JSON.stringify(d);
      }
    }

  });
  //

  //    
});

</script>
</body>
</html>