<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>ERMS - DOST |
            <?php if(isset($_GET['agency'])){ if($_GET['agency']==1){ echo 'Agency Documents'; }else{ echo 'Received Documents'; }} ?>
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="<?=base_url('assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" /> -->
        <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
        <!-- <link href="<?=base_url('assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')?>" rel="stylesheet" type="text/css" /> -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css">
        <link href="<?=base_url('assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/plugins/uniform/css/uniform.default.css')?>" rel="stylesheet" type="text/css" />
        <!-- <link href="<?=base_url('assets/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')?>" rel="stylesheet" type="text/css" /> -->
        <link href="<?=base_url('assets/theme/assets/global/plugins/datatables/datatables.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/css/components-rounded.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/css/plugins.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/layouts/layout/css/layout.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet')?>" type="text/css" id="style_color" />
        <link href="<?=base_url('assets/theme/assets/layouts/layout/css/custom.css')?>" rel="stylesheet" type="text/css" />
        <script src="<?=base_url('assets/theme/assets/global/plugins/jquery.min.js')?>" type="text/javascript"></script>
        <!-- <script src="<?=base_url('assets/vuejs/axios.min.js')?>"></script> -->
        <!-- <script src="<?=base_url('assets/vuejs/vue.js')?>"></script> -->
        <!-- <script src="https://unpkg.com/vue@2.5.13/dist/vue.js"></script> -->
        <!-- END PAGE LEVEL PLUGINS -->
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white ">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                        <!-- <img src="theme/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> -->
                        <span class="logo-default" id="logo-dost">DOST</span>
                        <span id="logo-erms" class="logo-default">ERMS</span>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile">
                                    Signed in as <?=checkAccess($_SESSION['sessloginby'], $_SESSION['isadmin'], $_SESSION['iscustodian'])?>: <?=$_SESSION['sessname']?>
                                    <?=$_SESSION['office'] != '' ? '('.$_SESSION['office'].')' : '';?>
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="<?=base_url('logout')?>">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <?php if(is_mobile()): ?>
            <div class="clearfix" style="color: #fff;line-height: 45px;padding-left: 20px;">
                <?=$_SESSION['sessname']?> <?=$_SESSION['office'] != '' ? '('.$_SESSION['office'].')' : '';?></div>
            </div>
        <?php else: ?>
            <div class="clearfix"></div>
        <?php endif; ?>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <li class="nav-item start <?=($this->uri->segment(1) == 'received-documents' ? 'active open' : '')?>">
                            <a href="http://erms.dost.gov.ph/tablet/received-documents" class="nav-link nav-toggle">
                                <i class="fa fa-file-text-o"></i>
                                <span class="title">Received Documents</span>
                            </a>
                        </li>
                        <!-- <li class="nav-item start "> -->
                        <!-- <li class="nav-item start <?=(isset($_GET['agency'])? ($_GET['agency']==1) ? 'active open' : '' : '')?>">
                            <a href="index.php?agency=1" class="nav-link nav-toggle">
                                <i class="fa fa-building-o"></i>
                                <span class="title">Agency Documents</span>
                            </a>
                        </li> -->
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="#">Home</a>
                                <i class="fa fa-circle"></i>
                                <a href="#" class="active">Documents</a>
                            </li>
                            <li>
                                <?php 
                                    if(isset($_GET['agency'])){
                                        if($_GET['agency']==1){
                                            echo '<a href="#" id="portlet-agency">Agency Documents</a>';
                                        }else{
                                            echo '<a href="#" id="portlet-received">Received Documents</a>';
                                        }
                                    }
                                 ?>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <div class="row">
                        <br>
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?=$contents?>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> <?php echo date("Y"); ?> &nbsp; <i class="fa fa-circle"></i> &nbsp; Department of Science and Technology
                <!-- <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a> -->
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="theme/assets/global/plugins/respond.min.js"></script>
<script src="theme/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- <script src="<?=base_url('assets/vuejs/custom-vue.js')?>" type="text/javascript"></script> -->
        <script src="<?=base_url('assets/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/js.cookie.min.js')?>" type="text/javascript"></script>
        <!-- <script src="<?=base_url('assets/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/global/plugins/jquery.blockui.min.js')?>" type="text/javascript"></script> -->
        <script src="<?=base_url('assets/theme/assets/global/plugins/uniform/jquery.uniform.min.js')?>" type="text/javascript"></script>
        <!-- <script src="<?=base_url('assets/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js')?>" type="text/javascript"></script> -->
        <script src="<?=base_url('assets/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/scripts/datatable.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/datatables/datatables.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')?>" type="text/javascript"></script>

        <script src="<?=base_url('assets/theme/assets/global/scripts/app.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/pages/scripts/ui-toastr.min.js')?>" type="text/javascript"></script>
        <!-- <script src="<?=base_url('assets/theme/assets/pages/scripts/ui-general.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/layouts/layout/scripts/layout.min.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/layouts/layout/scripts/demo.min.js')?>" type="text/javascript"></script> -->
        <!-- <script src="<?=base_url('assets/theme/assets/layouts/global/scripts/quick-sidebar.min.js')?>" type="text/javascript"></script> -->

        <script>
            $(document).ready(function() {
                // $("#tblReceived").DataTable();
                // setTimeout(function() { $("#tblReceived").DataTable(); }, 1000);
                console.log('datatables');
            });
        </script>

    </body>

</html>