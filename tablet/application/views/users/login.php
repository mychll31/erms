<?php 
// include("../class/Login.class.php");
// $login = new Login;
// $msg = $login->validateAccount();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>ERMS - DOST | User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="<?=base_url('assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" /> -->
        <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
        <link href="<?=base_url('assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/plugins/uniform/css/uniform.default.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/global/css/components.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/pages/css/login.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/theme/assets/layouts/layout/css/custom.css')?>" rel="stylesheet" type="text/css" />
        <script src="<?=base_url('assets/theme/assets/global/plugins/jquery.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/vuejs/axios.min.js')?>"></script>
        <script src="<?=base_url('assets/vuejs/vue.js')?>"></script>
        <link rel="shortcut icon" href="favicon.ico" /> </head>

    <body class=" login">
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
            <span class="big logo-default" id="logo-dost">DOST</span>
            <span id="logo-erms" class="big logo-default">ERMS</span>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <div id="login">
                <form class="login-form" method="post">
                    <input type="hidden" name="t_tablet" value="tablet">
                    <h3 class="form-title font-green">Sign In</h3>
                    <div class="alert alert-danger" v-if="error">
                        <button class="close" data-close="alert"></button>
                        <span>{{ message }}</span>
                    </div>
                    <div class="form-group">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <label class="control-label visible-ie8 visible-ie9">Username</label>
                        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="t_strUsername" v-model="uname" /> </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Password</label>
                        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="t_strPassword" v-model="pass" /> </div>
                    <div class="form-actions">
                        <button class="btn green uppercase" id="btnlogin" v-on:click="doLogin">Login</button>
                    </div>
                </form>
            </div>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright">  <?php echo date("Y"); ?> &nbsp; <i class="fa fa-circle"></i> &nbsp; Department of Science and Technology </div>
        <!--[if lt IE 9]>
<script src="../theme/assets/global/plugins/respond.min.js"></script>
<script src="../theme/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url('assets/vuejs/custom-vue-login.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/js.cookie.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/uniform/jquery.uniform.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/global/scripts/app.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/theme/assets/pages/scripts/login.min.js')?>" type="text/javascript"></script>
    </body>

</html>