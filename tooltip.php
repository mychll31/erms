
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<style>
body {
	padding:150px 50px;
	font-family:"Lucida Grande","Lucida Sans Unicode","bitstream vera sans","trebuchet ms",verdana;
}

a:active {
  outline:none;
}

:focus {
  -moz-outline-style:none;
}
</style>


<? include('includes.php'); ?>




<!-- without body tag IE may have unprodictable behaviours --> 
<body>


<!-- here we use separate stylesheets. you should definitely combine yours into a single file! -->

<!-- scrollable styling -->
<link rel="stylesheet" type="text/css" href="css/scrollable-horizontal.css" />
<link rel="stylesheet" type="text/css" href="css/scrollable-buttons.css" />

<!-- overlay styling -->
<link rel="stylesheet" type="text/css" href="css/overlay-basic.css"/>

<!-- gallery styling -->
<link rel="stylesheet" type="text/css" href="css/overlay-gallery.css"/>

<!-- tooltip styling -->


<style>


/*** override styling in external stylesheets ***/



/* remove margins from the image */
.items img {
	margin:0;
}

/* make A tags our floating scrollable items */
.items a {
	display:block;
	float:left;
	margin:20px 15px;
} 


/* tooltip styling */
#tooltip {
	display:none;
	background:url(img/black_arrow.png);
	font-size:12px;
	height:70px;
	width:160px;
	padding:25px;
	color:#fff;
}



/* scrollable should not disable gallery navigation */
#gallery .disabled {
	visibility:visible !important;
}

#gallery .inactive {
	visibility:hidden !important;
}
</style>

<!-- "previous page" action -->
<a class="prevPage browse left"></a>

<!-- root element for scrollable -->

<div class="scrollable">

	<!-- root element for the items -->
	<div class="items">

		<a href="http://farm1.static.flickr.com/143/321464099_a7cfcb95cf.jpg" title="Back view">
			<img src="http://farm1.static.flickr.com/143/321464099_a7cfcb95cf_t.jpg" /></a>

		<a href='<iframe src="img/thumb/Manual.pdf" width="675" height="595" />'>
			<img src="http://farm4.static.flickr.com/3275/2765571978_43771e81eb_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/79/244441862_08ec9b6b49.jpg" title="View from the pool">
			<img src="http://farm1.static.flickr.com/79/244441862_08ec9b6b49_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/28/66523124_b468cf4978.jpg" title="Granite stones">
			<img src="http://farm1.static.flickr.com/28/66523124_b468cf4978_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/164/399223606_b875ddf797.jpg" title="The bronze statue">
			<img src="http://farm1.static.flickr.com/164/399223606_b875ddf797_t.jpg" /></a>

		<a href="http://farm2.static.flickr.com/1083/1118933512_5ea1fe8f41.jpg" title="Marble walls">

			<img src="http://farm2.static.flickr.com/1083/1118933512_5ea1fe8f41_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/135/321464104_c010dbf34c.jpg" title="Statue again">
			<img src="http://farm1.static.flickr.com/135/321464104_c010dbf34c_t.jpg" /></a>

		<a href="http://farm4.static.flickr.com/3651/3445879840_7ca4b491e9.jpg" title="Monumental walls">
			<img src="http://farm4.static.flickr.com/3651/3445879840_7ca4b491e9_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/153/399232237_6928a527c1.jpg" title="Water view">
			<img src="http://farm1.static.flickr.com/153/399232237_6928a527c1_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/50/117346182_1fded507fa.jpg" title="Famous chairs">
			<img src="http://farm1.static.flickr.com/50/117346182_1fded507fa_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/3629/3323896446_3b87a8bf75.jpg" title="A lightning effect">
			<img src="http://farm1.static.flickr.com/3629/3323896446_3b87a8bf75_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/3023/3323897466_e61624f6de.jpg" title="Concrete corridors">
			<img src="http://farm1.static.flickr.com/3023/3323897466_e61624f6de_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/3650/3323058611_d35c894fab.jpg" title="Heavy red curtain">

			<img src="http://farm1.static.flickr.com/3650/3323058611_d35c894fab_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/3635/3323893254_3183671257.jpg" title="Large tropical trees">
			<img src="http://farm1.static.flickr.com/3635/3323893254_3183671257_t.jpg" /></a>

		<a href="http://farm1.static.flickr.com/3624/3323893148_8318838fbd.jpg" title="Lot's of tourists">
			<img src="http://farm1.static.flickr.com/3624/3323893148_8318838fbd_t.jpg" /></a>

	</div>

</div>

<div id="tooltip"></div>


<!-- "next page" action -->
<a class="nextPage browse right"></a>

<br clear="all" />



<!-- overlay element -->
<div class="simple_overlay" id="gallery">

	<!-- "previous image" action -->
	<a class="prev">prev</a>

	<!-- "next image" action -->
	<a class="next">next</a>

	<!-- image information -->
	<div class="info"></div>

	<!-- load indicator (animated gif) -->
	<img class="progress" src="img/thumb/loading.gif" />
</div>

<!-- javascript coding -->
<script>


$(document).ready(function() {
$(".scrollable").scrollable().find("a").tooltip({
	tip: '#tooltip'
}).overlay({
	target: '#gallery',
	expose: '#111',
	closeOnClick: false
}).gallery({
	disabledClass: 'inactive'
});
});


</script>







<div class="scrollable"><div class="items"><a id="93" href="pdfReader.php?filename=UPLOAD/RMS/2012/November2012/28/techniques-and-tools-for-the-design-and-implementation-of-enterprise-information-systems978159904826033132.pdf" rel="image" name="0" alt="techniques-and-tools-for-the-design-and-implementation-of-enterprise-information-systems978159904826033132.pdf" title="title: &lt;b&gt;&lt;i&gt;techniques-and-tools-for-the-design-and-implementation-of-enterprise-information-systems978159904826033132.pdf&lt;/i&gt;&lt;/b&gt;&lt;br&gt; upload date: 2012-11-28 00:52:36&lt;br&gt;&lt;br&gt;&lt;a title='Delete' onclick='removeItem();'&gt;&lt;img src='images/cancel-12.png' align='buttom'/&gt;&lt;/a&gt;" onmouseover="window.status='ERMS'; return true;"><img src="images/pdf.jpg" class="reflected" style="border: 0px none ; display: block;"></a><a id="94" onclick="" href="UPLOAD/RMS/2012/November2012/28/3D-graphics__002231_7.jpg" rel="image" title="title: &lt;b&gt;&lt;i&gt;3D-graphics__002231_7.jpg&lt;/i&gt;&lt;/b&gt;&lt;br&gt; upload date: 2012-11-28 01:01:48&lt;br&gt;&lt;br&gt;&lt;a title='Delete' onclick='removeItem();'&gt;&lt;img src='images/cancel-12.png' align='buttom'/&gt;&lt;/a&gt;" name="1" onmouseover="window.status='ERMS'; return true;"><img src="UPLOAD/RMS/2012/November2012/28/3D-graphics__002231_7.jpg" class="reflected" style="border: 0px none ; display: block;"></a></div></div>
<div style="position: absolute; top: -110px; left: -2px; display: none; text-align:left" id="tooltip"></div>