<?php 
//header("Content-Type: application/x-javascript; charset=UTF-8"); 
include_once("../class/General.class.php");
$objGen = new General;
if(isset($_GET['ret_val']) && $_GET['ret_val']!=''){
	$strRes = $objGen->checkNotification($_GET['ret_val']);	
	echo $strRes;
}
else{
	$strRes = $objGen->checkNotification('count');
?>
<!-- link type="text/css" href="realtime-notification/dist/themes/holler.css" rel="stylesheet"/-->
<!--script type='text/javascript'>
	var hollerConfig = {
	host: "http://erms.dost.gov.ph",
	port: "1337"
}
</script>
<script type="text/javascript" src="realtime-notification/dist/holler-client.min.js"></script-->	

<!-- script data-main="realtime-notification/js/main" src="realtime-notification/js/require.js"></script -->
<script type="text/javascript"> 
$(function(){
  var alertCount = <?php echo $strRes ?>;
  $(window).focus(function(){ if(alertCount>0 && getCookie('sysNotification') == 0) $("#footnot").css("display","block"); });
  $('#btnNotify').click(function(e){ 
      disableAlert();
	  $.ajax({ type: "GET",
		  url: "templates/footer.php", 
		  data: { ret_val:'data' },
		  success: function(res) {  $('#footpanel .content').html(res); } 
	  });  
  })
})
  
</script>
<div id="footpanel">
	<ul id="mainpanel">    	
      <li><a href="#" class="home" id="copyright">&copy; 2009 Department of Science and Technology.</a></li>
      <li id="alertpanel">
	   <div id="footnot"><?= $strRes; ?></div><a href="#" class="alerts" id="btnNotify"><small>Notification</small></a>
       <div style="height: auto; display: none;" class="subpanel">
<h3><span>&#8211;</span>Notifications</h3><ul style='height:500px;'><li class='view'><a onclick='javascript:getData(\'Notifications.php?mode=show\',\'rightPane_content\')'>View All Notification</a></li>
		<div class="content"></div>
	</ul>		   
	   </div>
      </li>
      <li id="chatpanel"><a href="#" onClick="parent.location='<? echo $HRMISlink;?>'" class="hrmis">HRMIS<small>hrmis link</small></a></li>
	</ul>
</div>
<?php } ?>