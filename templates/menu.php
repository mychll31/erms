<?
session_start();
?>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='css/images/plus.gif' class='statusicon' />", "<img src='css/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})


</script>
<style type="text/css">

.glossymenu{
margin: 5px 0;
padding: 0;
width: 100px; /*width of menu*/
border: 1px solid #0066FF;
border-bottom-width: 0;
}

.glossymenu a.menuitem{
background: black url(css/images/ui-bg_glass_45_0078ae_1x400.png) 50% 50% repeat-x;
font: bold 14px "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, sans-serif;
color: white;
display: block;
position: relative; /*To help in the anchoring of the ".statusicon" icon image*/
width: auto;
padding: 4px 0;
padding-left: 10px;
text-decoration: none;
}


.glossymenu a.menuitem:visited, .glossymenu .menuitem:active{
color: white;
}

.glossymenu a.menuitem .statusicon{ /*CSS for icon image that gets dynamically added to headers*/
position: absolute;
top: 5px;
right: 5px;
border: none;
}

.glossymenu a.menuitem:hover{
background-image: url(css/images/ui-bg_glass_75_79c9ec_1x400.png);
}

.glossymenu div.submenu{ /*DIV that contains each sub menu*/
background: white;
}

.glossymenu div.submenu ul{ /*UL of each sub menu*/
list-style-type: none;
margin: 0;
padding: 0;
}

.glossymenu div.submenu ul li{
border-bottom: 1px solid 0066FF;
}

.glossymenu div.submenu ul li a{
display: block;
font: normal 13px "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, sans-serif;
color: 0066FF;
text-decoration: none;
padding: 2px 0;
padding-left: 10px;
}

.glossymenu div.submenu ul li a:hover{
background: #DFDCCB;
colorz: white;
}

</style>
<? 
include_once("class/General.class.php");
include_once("class/Folder.class.php");
include_once("class/MySQLHandler.class.php");
$objGen = new General;
$objFolder = new Folder;

		echo '<table align="center" class="menu" width="100%" cellpadding=0 cellspacing=0 border=0 style="border-collapse:">';
		//echo '<tr><td>Folders</td></tr>';
		########### folder management ##########
		/*
		########### personal folders ##########
		$sql = "SELECT * FROM `tblFolders` WHERE userID = '".$objGen->get("userID")."'";
		$sql1= new MySQLHandler();
		$sql1->init();
		$rs=$sql1->Select($sql);
		if(count($rs))
		{
		for($t=0;$t<count($rs);$t++) 
			{
			//echo "<tr><td>";
			if($_GET['fid']==$rs[$t]['folderID'])
				//echo "<img src='images/folderOpen.jpg' width='18' height='14'>";
				echo '<ul id="icons" class="ui-widget ui-helper-clearfix"><li class="ui-state-default ui-corner-all" onClick=\'location.href="customFolder.php?mode=show&uid='.$objGen->get("userID").'&fid='.$rs[$t]['folderID'].'"\'><span class="ui-icon ui-icon-folder-open" title="'.$rs[$t]['folderName'].'"></span><span class="textbody">'.fillSpace($rs[$t]['folderName']).'&nbsp;</span></li></ul>';
			else
				//echo "<img src='images/folder.jpg' width='18' height='14'>";
				echo '<ul id="icons" class="ui-widget ui-helper-clearfix"><li class="ui-state-default ui-corner-all" onClick=\'location.href="customFolder.php?mode=show&uid='.$objGen->get("userID").'&fid='.$rs[$t]['folderID'].'"\'><span class="ui-icon ui-icon-folder-collapsed" title="'.$rs[$t]['folderName'].'"></span><span class="textbody">'.fillSpace($rs[$t]['folderName']).'&nbsp;</span></li></ul>';
			//echo '<a href="customFolder.php?mode=show&uid='.$objGen->get("userID").'&fid='.$rs[$t]['folderID'].'" onMouseOver="statusBar(); return true;" onClick="statusBar();" onMouseUp="statusBar()" onFocus="statusBar()" onKeyPress="statusBar();" onKeyDown="statusBar();">'.$rs[$t]['folderName'].'</a>';
			//echo '</td></tr>';
			}
		}
		*/	
		echo '<tr><td><div class="glossymenu">';
		########### documents ##########
		if($objGen->get('userType')==1 || $objGen->get('userType')==2)
		{
			echo '<a class="menuitem" href="#" onClick=\'javascript:getData("'.$objGen->getUserPageLink($objGen->get('userType')).'","rightPane_content");\'>Documents</a>';		
		}
		
		########### incoming - for admin & custodian ##########
		if($objGen->get('userType')==1 || $objGen->get('userType')==2)
		{
			echo '<a class="menuitem submenuheader" href="#" >Received Documents</a>
			<div class="submenu">
				<ul>
				<li><a href="#" onClick=\'javascript:getData("incomingList.php?mode=show","rightPane_content"); getData("showIncomingList.php?mode=show","doclist");\'>Received Docs</a></li>';
				################  personal folders ##################
				$rsFolders = $objFolder->getSqlEmpFolders($objGen->get("userID"));
				if(count($rsFolders))
					for($t=0;$t<count($rsFolders);$t++) 					
						echo '<li><a href="#" onClick=\'javascript:getData("customFolder.php?mode=show&uid='.$objGen->get("userID").'&fid='.$rsFolders[$t]['folderID'].'","rightPane_content");\'>'.$rsFolders[$t]['folderName'].'</a></li>';
				echo '</ul>
			</div>';
		}
		else //for employee
		{
			echo '<a class="menuitem submenuheader" href="#">Received Documents</a>';
			echo '<div class="submenu">
				<ul>
				<li><a href="#" onClick=\'javascript:getData("'.$objGen->getUserPageLink($objGen->get('userType')).'","rightPane_content");\'>Received Docs</a></li>';
				################  personal folders ##################
				$rsFolders = $objFolder->getSqlEmpFolders($objGen->get("userID"));
				if(count($rsFolders))
					for($t=0;$t<count($rsFolders);$t++) 					
						echo '<li><a href="#" onClick=\'javascript:getData("customFolder.php?mode=show&uid='.$objGen->get("userID").'&fid='.$rsFolders[$t]['folderID'].'","rightPane_content");\'>'.$rsFolders[$t]['folderName'].'</a></li>';
				echo '</ul>
			</div>';			
		}		


		########### libraries ##########	
		if($objGen->get('userType')==1 || $objGen->get('userType')==2)
		{
			echo '<a class="menuitem" href="#" onClick=\'javascript:getData("libraries.php?mode=accounts","rightPane_content");\'>Libraries</a>';
		}
		
		########### disposals ##########	
		if($objGen->get('userType')==1 || $objGen->get('userType')==2)
		{
			echo '<a class="menuitem" href="#" onClick=\'javascript:getData("disposals.php?mode=disposals&uid='.$objGen->get("userID").'","rightPane_content");\'>Disposals</a>';
		}
		########### trash ##########
		echo '<a class="menuitem" href="#" onClick=\'javascript:getData("trash.php?mode=trash&uid='.$objGen->get("userID").'","rightPane_content");\'>Trash</a>
		</td></tr>
</table>';
		########### switch ##########
		echo "<br />";
		switch($objGen->get('userType'))
		{
		case 3:
			if($_SESSION["byPass"]=="y")
			{
				if($_SESSION["tmpUserType"]==1) 
				{ 
					$mod="records module";
					$plink="records.php?mode=show&sec=revert&fid=";
				}
				else if ($_SESSION["tmpUserType"]==2) 	
				{	
					$mod="custodian module";
					$plink="custodian.php?mode=show&sec=revert&fid=";
				}
				echo '<a href="#" onClick=\'javascript:getData("'.$plink.'","rightPane_content");\'>'.$mod.'</a>';
			}
		break;
		default:
			echo '<a href="#" onClick=\'javascript:getData("employee.php?mode=show&sec=bp&fid=","rightPane_content");\'>employee module</a>';		
		}

function fillSpace($t_strText)
{
$n = strlen($t_strText);
$tmp = trim($t_strText);
for($t=$n;$t<14;$t++)
	$tmp = $tmp . "&nbsp;";
return $tmp;
}
?>