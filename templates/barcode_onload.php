<?php
@session_start();

include_once("../class/General.class.php");
include_once("../class/IncomingList.class.php");
$objGen = new General;
$objList = new IncomingList;

$t_strReceivedBy = $objGen->getEmpName($objGen->get('userID'));
$res = $objGen->getDocStatusByID($_GET['docid']);
$t_strHistoryID = $objGen->getDocHistoryId($_GET['docid']);
$t_strClass = $objGen->chkdocrec($t_strHistoryID['historyId']);
$ifExist = $objList->checkExistsById($_GET['docid']);

$return_data['insert'] = 0;
$return_data['exist'] = $ifExist;
if($ifExist){
	if($t_strClass < 1){
		$inserted = $objList->receiveDocument($_GET['docid'],$t_strReceivedBy,$t_strHistoryID['historyId']);
		$return_data['insert'] = $inserted;
	}
	if ($res==1){
	   $return_data['status'] = 'showOutgoing';
	} else {
	    $return_data['status'] = 'showIncoming';
	}
}

echo json_encode($return_data);
?>