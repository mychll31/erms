<script language="javascript">
//getData('templates/menu2.php?showfolders='+document.getElementById('tmpcol').value,'folderdiv');
function showHideFolder()
    {
        var divstyle = new String();
        divstyle = document.getElementById("folderdiv").style.visibility;
        if(divstyle.toLowerCase()=="visible" || divstyle == "")
        {
            document.getElementById("folderdiv").style.visibility = "hidden";
			document.getElementById("arrowdiv").innerHTML = "<span class='ui-icon ui-icon-triangle-1-e'></span>";
			
        }
        else
        {
            document.getElementById("folderdiv").style.visibility = "visible";
			document.getElementById("arrowdiv").innerHTML = "<span class='ui-icon ui-icon-triangle-1-s'></span>";
        }
    }
/*
if(col=="collapse")
	document.getElementById('folderdiv').style.visibility='visible';
else
	document.getElementById('folderdiv').style.visibility='hidden';	
	*/
getData('showFolder.php?mode=refreshfolder','folderdiv');
</script>
<? 
	include_once("class/General.class.php");
	include_once("class/Folder.class.php");
	include_once("class/MySQLHandler.class.php");
	// BEGIN CLASS FOR P-D-D (Pending, Duedata, Deadlines)
	include_once("class/IncomingList.class.php");
	$objList = new IncomingList;
	// END CLASS FOR P-D-D
	$objGen = new General;
	$objFolder = new Folder;

	include_once("templates/menu_include.php");

	// if($objGen->get('office')=='ITD'){
		$totalUnacted = $objList->getUnactedDocuments(0,'total');
		$totalOverdue = $objList->getOverDue(0,'total','overdue');
		$totalPending = $objList->getOverDue(0,'total','pending');
		$totalDeadline = $objList->getOverDue(0,'total','deadlines');
	// }

	if($_GET['showicon']=="collapse")
		{
		echo "<script type='text/javascript'>document.getElementById('tmpcol').value='uncollapse';</script>";
		echo "<span class='ui-icon ui-icon-triangle-1-e'></span>";
		exit(1);
		}
		
	if($_GET['showicon']=="uncollapse")
		{
		echo "<script type='text/javascript'>document.getElementById('tmpcol').value='collapse';</script>";
		echo "<span class='ui-icon ui-icon-triangle-1-s'></span>";
		exit(1);
		}
	

?>
<!-- <div id="menuborder" class="ui-tabs ui-widget ui-widget-content ui-corner-all">-->
	<!-- <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#">&nbsp;Menu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
</ul>-->

<!-- begin style for menu -->
<style type="text/css">
	.divtool-pending, .divtool-overdue, .divtool-unacted, .divtool-deadline {
	    background-color: #fff;
	    position: fixed;
	    margin-left: 40px;
	    border: 1px solid #ccc;
	    padding: 3px;
	    z-index: 1;
	}
</style>
<!-- end style for menu -->

<table border="0" width="<? echo $menusize;?>" align="center">
	<?php
		if($objGen->get('userType')==1 || $objGen->get('userType')==2 || ($objGen->get('userType')==3 && $objGen->get("blnAgencyUser"))):
			$menuClicked = ($objGen->get('userType')==3)?"&menu=docs":""; ?>

		<tr <?=$style;?> onClick="getData('<? echo $objGen->getUserPageLink($objGen->get('userType')).$menuClicked?>','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-document' title='Inbox' style="float:left;"></span>
				<? echo setTextFont("Documents");?>
			</td>
		</tr>

	<?php endif; ?>

	<!-- DATA ENCONDING MENU -->
	<? if($objGen->get('userType')==1 || $objGen->get('office')== RECORDS_ADMIN_VALUE || $objGen->get('office')== "PROCUREMENT"):?>
	
		<tr <? echo $style;?> onClick="getData('recordOld.php?mode=show','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-document' title='Inbox' style="float:left;"></span>
				<? echo setTextFont("Old Documents");?><br>
			</td>
		</tr>

	<?php endif; ?>

	<? if($objGen->get('userType')==1 || $objGen->get('userType')==2):?>

		<tr <? echo $style;?> onClick="getData('incomingList.php?mode=show','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-mail-closed' title='Received Documents' style="float:left;"></span>
				<? echo setTextFont("Received Documents");?>
			</td>
		</tr>

		<?php #if($objGen->get('office')=='ITD'): ?>
			<tr id="recpull-pending" <?=$style;?> onClick="getData('pendingList.php?mode=show','rightPane_content')">
				<td style="padding-left: 21px;" width="<?=$iconwidth;?>" align="right" height="<?=$height;?>">
					<span class='ui-icon ui-icon-mail-closed' title='Pending Documents' style="float:left;"></span>
					<span>Pending&nbsp;(<span style='color: red;'><?=$totalPending[0]['_total']?></span>)</span>
					<div class="divtool-pending" style="display: none;">Displays list of documents<br>that has been sent to user’s<br>office within the three<br>(3) days period</div>
				</td>
			</tr>
			<tr id="recpull-overdue" <?=$style;?> onClick="getData('overDueList.php?mode=show','rightPane_content')">
				<td style="padding-left: 21px;" width="<?=$iconwidth;?>" align="right" height="<?=$height;?>">
					<span class='ui-icon ui-icon-mail-closed' title='Unacted Documents' style="float:left;"></span>
					<span id="spanoverdue">Over Due&nbsp;(<span style='color: red;'><?=$totalOverdue[0]['_total']?></span>)</span>
					<div class="divtool-overdue" style="display: none;">Displays list of documents<br>that has been sent to user’s<br>office that exceeds the three<br>(3) days process period</div>
				</td>
			</tr>
			<tr id="recpull-unacted" <?=$style;?> onClick="getData('unactedList.php?mode=show','rightPane_content')">
				<td style="padding-left: 21px;" width="<?=$iconwidth;?>" align="right" height="<?=$height;?>">
					<span class='ui-icon ui-icon-mail-closed' title='Unacted Documents' style="float:left;"></span>
					<span>Unacted Docu...&nbsp;(<span style='color: red;'><?=$totalUnacted[0]['_total']?></span>)</span>
					<div class="divtool-unacted" style="display: none;">Displays list of unprocessed<br>documents that exceeds the 15<br>days process period</div>
				</td>
			</tr>
			<tr id="recpull-deadlines" <?=$style;?> onClick="getData('deadlinesList.php?mode=show','rightPane_content')">
				<td style="padding-left: 21px;" width="<?=$iconwidth;?>" align="right" height="<?=$height;?>">
					<span class='ui-icon ui-icon-mail-closed' title='Unacted Documents' style="float:left;"></span>
					<span>Deadlines&nbsp;(<span style='color: red;'><?=$totalDeadline[0]['_total']?></span>)</span>
					<div class="divtool-deadline" style="display: none;">Displays list of documents<br>that has a specified<br>deadline</div>
				</td>
			</tr>
		<?php #endif; ?>

		<? if(MAIN_OFFICE==1 && $objGen->get('office')== "RMS"):?>

		<tr <? echo $style;?> onClick="getData('records2.php?mode=show&blnAgencyDocsOnly=true','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-mail-closed' title='Agency Documents' style="float:left;"></span>
				<? echo setTextFont("Agency Documents");?>
			</td>
		</tr>

		<?php endif; ?>
	<?php else: ?>

		<tr <? echo $style;?> onClick="getData('<? echo $objGen->getUserPageLink($objGen->get('userType'));?>&menu=rdocs','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-mail-closed' title='Received Documents' style="float:left;"></span>
				<? echo setTextFont("Received Documents");?>
			</td>
		</tr>
	
	<?php endif; ?>

	<? if($objGen->get('userType')==1 || $objGen->get('userType')==2): ?>
		<tr <? echo $style;?> onClick="getData('libraries.php?mode=accounts','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-wrench' title='Libraries' style="float:left;"></span>
				<? echo setTextFont("Libraries");?>
			</td>
		</tr>
	<?php endif; ?>

	<? if($objGen->get('userType')==1 || $objGen->get('userType')==2): ?>
		<tr <? echo $style;?> onClick="getData('disposals.php?mode=disposals&uid=<? echo $objGen->get("userID")?>','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-note' title='Disposals' style="float:left;"></span>
				<? echo setTextFont("For Disposals");?>
			</td>
		</tr>
	<?php endif; ?>

	<tr <? echo $style;?> onClick="getData('trash.php?mode=trash&uid=<? echo $objGen->get("userID");?>','rightPane_content');">
			<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
				<span class='ui-icon ui-icon-trash' title='Trash' style="float:left;"></span>
				<? echo setTextFont("Trash");?>
			</td>
		</tr>

</table>

<? if($objGen->get('userType')==1 || $objGen->get('userType')==2) {?>
<hr width="<? echo $menusize;?>" align="center">
<table border="0" width="<? echo $menusize;?>" align="center">
<? 
	switch($objGen->get('userType'))
	{
	case 3:
		if($_SESSION["byPass"]=="y")
		{
			if($_SESSION["tmpUserType"]==1) 
			{ 
				$mod="Records Module";
				$plink="records.php?mode=show&sec=revert&fid=";
			}
			else if ($_SESSION["tmpUserType"]==2) 	
			{	
				$mod="Custodian Module";
				$plink="custodian.php?mode=show&sec=revert&fid=";
			}
			echo '<tr '.$style.' onClick=\'getData("'.$plink.'","rightPane_content");\'><td width="'.$iconwidth.'" align="right" height="'.$height.'"><span class="ui-icon ui-icon-shuffle" title="Switch to '.$mod.'" style="float:left;"></span>'.setTextFont($mod).'</td></tr>';
		}
	break;
	default:
		echo '<tr '.$style.' onClick=\'getData("employee.php?mode=show&sec=bp&fid=","rightPane_content");\'><td width="'.$iconwidth.'" align="right" height="'.$height.'"><span class="ui-icon ui-icon-shuffle" title="Switch to Employee Module"></span></td><td>'.setTextFont("Employee Module").'</td></tr>';	 break;
	}
?>
</table>
<? }?>
<hr width="<? echo $menusize;?>" align="center">
<table border="0" width="<? echo $menusize;?>" align="center">
<tr <? echo $style;?> onClick="showHideFolder();">
	<td width="<? echo $iconwidth;?>" align="right" height="<? echo $height;?>">
	<input type="hidden" id="tmpcol" value="uncollapse">
	<div id="arrowdiv">
	<span class='ui-icon ui-icon-triangle-1-s'></span>
	</div>
	</td>
	<td><? echo setTextFont("Folders");?></td></tr></table>
	
	<div id="folderdiv">
	
	</div><img src="images/FAQ.png" id="imgfaq" width="60px" height="25px" style="margin-top:10px; margin-left:5px; cursor:pointer" />

	<br><br>
	<div>
		<b style="color: #e20000;">New!!!</b>
		<span style="color: #e20000;">Generating Custom Report</span>&nbsp;&nbsp;<a class="linklearnmore" target="_"  href="UserManual/RecordsOfficer/GeneratingCustomReport.php">Learn more</a>
	</div>
	<br>	
	<div>
		<span style="color: #e20000;">Unblocking of Pop-up in Printing Barcode</span>&nbsp;&nbsp;<a class="linklearnmore" target="_"  href="UserManual/RecordsOfficer/UnblockPopupinPrintingBarcode.php">Learn more</a>
	</div>
	<br>	
	<div>
		<span style="color: #e20000;">Monitoring of Compliance with Satutory and Regulatory Reports</span>&nbsp;&nbsp;<a class="linklearnmore" target="_"  href="UserManual/RecordsOfficer/UpdatePublicationofDocument.php">Learn more</a>
	</div>
	<br>
	<div>
		<span style="color: #e20000;">Viewing of Document Status without Logging In</span>&nbsp;&nbsp;<a class="linklearnmore" target="_"  href="UserManual/RecordsOfficer/ViewDocumentStatus.php">Learn more...</a>
	</div>
	<br>
	<div>
		<span style="color: #e20000;">Trouble in Printing Barcode</span>&nbsp;&nbsp;<a class="linklearnmore" target="_"  href="UserManual/RecordsOfficer/TroubleinPrintingBarcode.php">Learn more...</a>
	</div>
	<br>	

	<style type="text/css">
		.linklearnmore {
			text-decoration: none !important;
			color: blue !important;
		}

		a.linklearnmore:hover {
			text-decoration: none;
			color: blue;
			font-weight: bolder;
		}
	</style>
<!--</div>-->

<script type="text/javascript">
	$('#spane').hide();
	$('#recpull').click( function(){
		if($('#recpull-unacted').is(':visible')){
			$('#recpull-unacted').hide();
			$('#recpull-overdue').hide();
			$('#recpull-pending').hide();
			$('#recpull-deadlines').hide();
			$('#spane').show();
			$('#spans').hide();
		}else{
			$('#recpull-unacted').show();
			$('#recpull-overdue').show();
			$('#recpull-pending').show();
			$('#recpull-deadlines').show();
			$('#spane').hide();
			$('#spans').show();
		}
	});
	
	$("#recpull-pending")
		.mouseover(function() {
			$('.divtool-pending').show();
		})
		.mouseout(function() {
			$('.divtool-pending').hide();
		});

	$("#recpull-overdue")
		.mouseover(function() {
			$('.divtool-overdue').show();
		})
		.mouseout(function() {
			$('.divtool-overdue').hide();
		});

	$("#recpull-unacted")
		.mouseover(function() {
			$('.divtool-unacted').show();
		})
		.mouseout(function() {
			$('.divtool-unacted').hide();
		});

	$("#recpull-deadlines")
		.mouseover(function() {
			$('.divtool-deadline').show();
		})
		.mouseout(function() {
			$('.divtool-deadline').hide();
		});
</script>