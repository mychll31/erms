<? 
require('class/ReportDocumentOutgoing.class.php');
$objRpt = new ReportDocumentOutgoing('L','mm','A4');
$objRpt->SetLeftMargin(15);
$objRpt->SetRightMargin(15);
$objRpt->SetTopMargin(15);
$objRpt->SetAutoPageBreak("on",30);
$objRpt->AliasNbPages();
$objRpt->Open();
$objRpt->AddPage();
$objRpt->generateReport();
$objRpt->Output();
?>