<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Incoming.class.php");

$objIncoming = new Incoming;
if($_GET['mode']=="") $arrFields = $objIncoming->getUserFields();
else $arrFields = $objIncoming->getUserFields1();

################################
#
#  modes: new - for new incoming document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################
if($arrFields['mode']=='edit')
{
echo "station1";
}
elseif ($arrFields['mode']=='save')
{
echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
print_r($_POST);
$objIncoming->addDocument($arrFields);
}
else
{
echo "station3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo $arrFields['t_strDocId']."&nbsp;&nbsp;&nbsp;&nbsp;";
echo $_SERVER["REQUEST_URI"];
$t_strNewId=$objIncoming->getNewId();
$t_intDocTypeId=-1;
$t_intOriginId=-1;
$dateReceived = date("Y-m-d");
}

$arDocType=$objIncoming->getDocType();
$arOrigin=$objIncoming->getOriginOffice();
?>
 <script language="javascript" type="text/javascript">
 function check(obj,div,phppage)
 {
 intEmptyInput=0;
 for (i=0; i<obj.getElementsByTagName("input").length; i++) 
 {
 var sel = obj.getElementsByTagName("input")[i];
    if (sel.type == "text" && sel.getAttribute('alt')=="required") {
	if (sel.value==""){
	var divname=sel.name+"_div";
	intEmptyInput++;
	document.getElementById(divname).innerHTML="&nbsp;*required";
	}
	}
 }
 if(intEmptyInput==0) get(obj,div,phppage);
 }
 </script>
 
<form action="javascript:check(document.getElementById('frmIncoming'),'inc','showIncoming.php');" name="frmIncoming" id="frmIncoming">
<table align="center" width="600px" class="textbody">
  <!--DWLayoutTable-->
  <tr> 
    <td width="97">Document ID </td>
    <td width="175"><input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required"><span id="t_strDocId_div" class="required"></span>
	<input type="hidden" name="mode" value="<? 
	if($arrFields['mode']=='new')
	{
	echo "save";
	}
	elseif($arrFields['mode']=='edit')
	{
	echo "update";
	}
	elseif($arrFields['mode']=='save')
	{
	echo "update";
	}
	elseif($arrFields['mode']=='update')
	{
	echo "update";
	}
	?>">
	</td>
    <td width="93">Date Received</td>
      <td width="217"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateReceived" name="dateReceived" value="<? echo $dateReceived;?>"></td>
  </tr>
  <tr> 
    <td>Document Type</td>
      <td><select name="cmbDocType" class="caption">
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeAbbrev'];?></option>
	  <?
	  }
	  ?>
        </select></td>   
    <td>Document Date</td>
      <td><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="documentDate" name="documentDate"></td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><br /><br /></td>  
  </tr>
  <tr> 
    <td>Subject</td>
    <td colspan="3"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3"></textarea></td>
  </tr>
    <tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><br /><br /></td>  
  </tr>
  <tr> 
   <td height="30" valign="top">Origin</td>
      <td valign="top">
	  <script language="javascript" type="text/javascript">
	  var arOrigin = new Array(<? echo sizeof($arOrigin);?>);
	  <?
  	  $intColumnNum=sizeof($arOrigin[0])/2;
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  echo "arOrigin[".$i."]=new Array(".$intColumnNum.");\n";
  	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {
	  for($x=0;$x<$intColumnNum;$x++)
	  {
	  	echo "arOrigin[".$i."][".$x."]='".$arOrigin[$i][$x]."';\n";
	  }
	  }

	  ?>
		function getPerson(i)
		{
		for(x=0;x<arOrigin.length;x++)
		{
		if(i==arOrigin[x][0]) document.getElementById("t_strSender").value=arOrigin[x][4];
		}		
		}
	  </script>
	  
        <select class="caption" name="cmbOrigin" onchange="getPerson(this.value);" id="cmbOrigin">
		  <?
	  for($i=0;$i<sizeof($arOrigin);$i++)
	  {    
	  ?>
	  <option value="<? echo $arOrigin[$i]['originId']; ?>" <? if($arOrigin[$i]['originId']== $t_intOriginId) echo "selected"; ?> > <? echo $arOrigin[$i]['officeName'];?></option>
	  <?
	  }
	  ?>
		</select></td>
    <td height="42" valign="top">Sender</td>
    <td valign="top"><input type="text" size="30" class="caption" name="t_strSender" id="t_strSender"></td>
  </tr>
  <tr>
 	<td valign="top">Deadline</td>
    <td valign="top"><input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="deadline" name="deadline"></td>
	<td rowspan="2">Remarks</td>
    <td rowspan="2" valign="top"><textarea cols="30" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"></textarea></td>
  </tr>
  <tr>
      <td height="39" valign="top">File Container</td>
      <td valign="top"><input type="text" class="caption" name="t_strContainer" id="t_strContainer"></td>
  </tr>
  <tr> 
      <td valign="top">Confidential</td>
      <td valign="top"><input type="checkbox" value="checkbox" name="t_intConfidential" id="t_intConfidential"></td>
  </tr>
  <tr> 
  <td height="24">Action to be Taken</td>
      <td><select name="select5" class="caption">
          <option selected value="0">for Info</option>
		  <option  value="1">for Signature</option>
          <option value="2">for Comments</option>
        </select></td>
      <td height="24">Action Unit</td>
    <td><select name="select6[]" multiple class="caption">
          <option selected value="0"></option>
		  <option value="1">OASEC-FALA</option>
          <option value="2">OSEC</option>
          <option value="3">OASECST</option>
		  <option value="4">Abuel, Francis</option>
		  <option value="5">Dotimas, Marilen</option>
		  <option value="6">Monroyo, George</option>
        </select></td>
  </tr>
  
  <tr><td colspan="4" style="text-align:center">
  <? if ($arrFields['mode'] == "new"){
  ?><input type="submit" value="Save" class="caption" onclick="">
  <?
  }
  elseif ($arrFields['mode'] == "edit"){
  ?><input type="submit" value="Update" class="caption" onclick="">
  <?
  }
  elseif ($arrFields['mode'] == "save"){
  ?><input type="submit" value="Update" class="caption" onclick="">
  <?
  }
  ?>
  
  <input type="reset" value="Clear" class="caption"></td></tr>
</table>
</form>			

