<?php
/* 
Name: xmlparser2.php
Author: Mongi
Decription: Custom made from xmlparser.php only for use with manage custodian.
			uses two tables (tblEmpPersonal and tblCustodian).
			Dynamically Converts Mysql data into xml to 
            be use in Grid from 4 tables 
            (tblActionRequired,tblActionTaken,tblDocumentType,tblOOffice)
*/
// connect to the database
include_once("class/General.class.php");
include_once("class/MySQLHandler.class.php");
$sql = new MySQLHandler2();
$sql->init();	


$examp = $_REQUEST["q"]; // query number
$page = $_REQUEST['page']; // get the requested page
$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
$sord = $_REQUEST['sord']; // get the direction
if(!$sidx) $sidx =1;

$mode = Strip($_REQUEST["mode"]); //mode - add,edit,del,save

$modes = array("add"=>"INSERT","save"=>"UPDATE","del"=>"DELETE"); //use for log section 
$module = Strip($_REQUEST["MODULE"]); // use for log

$table = "tblCustodian";//table name
$table1 = "tblEmpPersonal";
$id = explode(',',Strip($_REQUEST["ID"])); //parameter1
$code = Strip($_REQUEST["CODE"]);//parameter2
$empNumber = Strip($_REQUEST["EMPNUMBER"]);//parameter3
$groupCode = Strip($_REQUEST["GROUP"]);//parameter4
$admin = Strip($_REQUEST["ADMIN"]);
$email = Strip($_REQUEST["EMAIL"]);

//Get Field List
$values = "'$code','$desc'"; 
$fields_query = mysql_query("show fields from ".$table);
while($fields = mysql_fetch_array($fields_query)){
 if(strchr(strtoupper($fields['Field']),'CONTACT') != ''){ $values = "'$name','$contact','$add','$person','$owner'";  break; }
 if(strchr(strtoupper($fields['Field']),'RETENTION')!=''){ $values =  "'$code','$desc','$period'"; break; }
 if(strchr(strtoupper($fields['Field']),'EMAIL')!=''){ $values =  "'$timeInternal','$ftp','$email','$fname','$timectr','$location','$time_last_run'"; break; }}

$fields_query = mysql_query("show fields from ".$table);
while($fields = mysql_fetch_array($fields_query)){
 if(strchr(strtoupper($fields['Field']),'ID')=='ID') $field_ID = $fields['Field'];
 else $field_list[] = $fields['Field'];}
 
switch($mode)
{
 case "add": $SQL = "INSERT INTO " . $table . " (officeCode,empNumber,groupCode,admin,email) VALUES ('".$code."','".$empNumber."','".$groupCode."',$admin,'$email')";
 			 //mysql_query($SQL);
			 $sql3= new MySQLHandler();
			 $sql3->init();
			 $sql3->Insert($SQL);
			 
		     $sql->changelog($SQL,$modes[$mode],$module,"Added new ".$module." information");
 			 break;
 case "del": 
 			  $sql3= new MySQLHandler();
			  $sql3->init();
 			  foreach($id as $ID){ //for multiple row deletion(not workin)
 			  $SQL = "DELETE from ".$table." where $field_ID = '".$ID."'";
			  //mysql_query($SQL);
			  $sql3->Delete($SQL);
			  }
			  $sql->changelog($SQL,$modes[$mode],$module,"Deleted ".$module." information");
 			  break;
 case "save": $SQL = "UPDATE $table SET 
			  				officeCode='$code',
							empNumber='$empNumber',
							groupCode='$groupCode',
							admin=$admin,
							email='$email'
						   WHERE custodianId = '".$id[0]."'";
				$sql3= new MySQLHandler();
			    $sql3->init();
			    $sql3->Update($SQL);
				//mysql_query($SQL);
				$sql->changelog($SQL,$modes[$mode],$module,"Edited ".$module." information");
			 break;			 
// default :			 
}

//search is disabled as of sept. 26, 2010
$wh = "";
$searchOn = Strip($_REQUEST['_search']);
$search_On = Strip($_REQUEST['searchOn']);
if($searchOn=='true' || $search_On == 'true') {
	$fld = Strip($_REQUEST['searchField']);
	if ($fld=="empName") $fld="$table1.surname";
	else $fld = "$table.$fld";
	//if( $fld=='documentTypeId' || $fld =='documentTypeAbbrev' || $fld=='documentTypeDesc' || $fld=='retentionPeriod' ) {
		$fldata = Strip($_REQUEST['searchString']);
		$foper = Strip($_REQUEST['searchOper']);
		// costruct where
		$wh .= " where ".$fld;
		switch ($foper) {
			case "cn":
				$wh .= " LIKE '%".$fldata."%'";
				if($fld=="tblEmpPersonal.surname")
					{

					$wh .= " OR $table1.firstname LIKE '%".$fldata."%'";
					}
				break;
			default :
				$wh = "";
		//}
	}
}
//$sqlll="SELECT COUNT(*) AS count FROM $table LEFT JOIN $table1 on $table.empNumber = $table1.empNumber ";
//echo $sqlll;
//$result = mysql_query("SELECT COUNT(*) AS count FROM $table LEFT JOIN $table1 on $table.empNumber = $table1.empNumber ".$wh);
//$row = mysql_fetch_array($result,MYSQL_ASSOC);
$SQL="SELECT COUNT(*) AS count FROM $table";
$sql3= new MySQLHandler();
$sql3->init();
$row=$sql3->Select($SQL);
$count = $row[0]['count'];

if( $count >0 ) {
	$total_pages = ceil($count/$limit);
} else {
	$total_pages = 0;
}
if ($page > $total_pages) $page=$total_pages;
$start = $limit*$page - $limit; // do not put $limit*($page - 1)
//$SQL = "SELECT *,$table1.surname, $table1.firstname FROM $table LEFT JOIN $table1 on $table.empNumber = $table1.empNumber ".$wh." ORDER BY ".$sidx." ". $sord." LIMIT ".$start." , ".$limit; 
$LIMIT=($limit == 'All')?"":" LIMIT ".$start." , ".$limit;
$SQL = "SELECT * FROM $table ".$wh." ORDER BY ".$sidx." ". $sord.$LIMIT; 
//$SQL = "SELECT * FROM $table  ORDER BY ".$sidx." ". $sord." LIMIT ".$start." , ".$limit; 
//echo $SQL;
//$result = mysql_query( $SQL ) or die("Couldn't execute query.".mysql_error());
$sql3= new MySQLHandler();
$sql3->init();
$row=$sql3->Select($SQL);
$rCount=count($row);
if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
header("Content-type: application/xhtml+xml;charset=utf-8"); } else {
header("Content-type: text/xml;charset=utf-8");
}
$et = ">";

echo "<?xml version='1.0' encoding='utf-8'?$et\n";
echo "<rows>";
echo "<page>".$page."</page>";
echo "<total>".$total_pages."</total>";
echo "<records>".$count."</records>";
// be sure to put text data in CDATA
/*while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
	echo "<row id='". $row[$field_ID]."'>";			
	echo "<cell>". $row[$field_ID]."</cell>";
	echo "<cell><![CDATA[". $row[$field_list[0]]."]]></cell>";
	echo "<cell><![CDATA[". $row[$field_list[1]]."]]></cell>";
	echo "<cell>".$row['surname'].", ".$row['firstname']."</cell>";
	echo "</row>";
}*/

for($i=0;$i<$rCount;$i++)
{
	echo "<row id='". $row[$i]['custodianId']."'>";			
	echo "<cell>". $row[$i]['custodianId']."</cell>";
	echo "<cell><![CDATA[". $row[$i]['officeCode']."]]></cell>";
	echo "<cell><![CDATA[". $row[$i]['groupCode']."]]></cell>";
	echo "<cell><![CDATA[". $row[$i]['empNumber']."]]></cell>";
	echo "<cell>".getName($row[$i]['empNumber'])."</cell>";
	echo "<cell><![CDATA[". $row[$i]['email']."]]></cell>";	
	if($row[$i]['admin']=='1') $retAdmin="Yes";
	else 	$retAdmin="No";
	echo "<cell><![CDATA[". $retAdmin."]]></cell>";
	echo "</row>";
}
echo "</rows>";		


mysql_close($db);



function getName($empNum)
{
$SQL="SELECT surname, firstname, nameExtension FROM tblEmpPersonal WHERE empNumber='$empNum'";
$sql2 = new MySQLHandler2();
$sql2->init();
$row=$sql2->Select($SQL);
if(count($row)==0){
$SQL="SELECT surname, firstname, nameExtension FROM tblUserAccount WHERE empNumber='$empNum'";
$sql1 = new MySQLHandler();
$sql1->init();
$row=$sql1->Select($SQL);
}
if(count($row)==0) return "(unknown)";
return $row[0]['surname'].", ".$row[0]['firstname']." ".$row[0]['nameExtension'];
}


function Strip($value)
{
	if(get_magic_quotes_gpc() != 0)
  	{
    	if(is_array($value))  
			if ( array_is_associative($value) )
			{
				foreach( $value as $k=>$v)
					$tmp_val[$k] = stripslashes($v);
				$value = $tmp_val; 
			}				
			else  
				for($j = 0; $j < sizeof($value); $j++)
        			$value[$j] = stripslashes($value[$j]);
		else
			$value = stripslashes($value);
	}
	return $value;
}
function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}
?>

