<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ListOfIncomingandOutgoing.xls");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

include_once("class/Records.class.php");
$objRecordDetail = new Records;
		
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
		
$rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);

echo "DOST CENTRAL OFFICE<br>";
echo $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit'])."<br>";

echo "<strong><center>LIST OF INCOMING AND OUTGOING RECORDS</center></strong><br>";
echo "<strong><center>(".date('d M Y',strtotime($_GET['dateFrom']))." TO ".date('d M Y',strtotime($_GET['dateTo'])).")</center></strong><br>";

?>


<table border="1">
    <strong>
    <thead>
    <tr bgcolor="#CCCCCC">
        <td ></td>
        <td width="100">DOC ID</td>
        <td width="150">DOC TYPE</td>
        <td>STATUS</td>
        <td>ORIGIN</td>
        <td>SUBJECT</td>
        <td>DATE ENCODED</td>
      
    </tr>
    </thead>
    </strong>
    <?php
	$ctr=0;	
    for($i=0;$i<count($rsRecordDetail);$i++) 
	{	
	//echo strtotime($rsRecordDetail[$i]['dateAdded']).">=".strtotime($_GET['dateFrom']) ."&&". strtotime($rsRecordDetail[$i]['dateAdded'])."<=".strtotime($_GET['dateTo']) ."&&". $rsRecordDetail[$i]['status']."==0<br>";
    	if($_GET['doctype']=='all'){
            if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo']){
                $ctr++;
                ?>
                    <tr>
                        <td><?php echo $ctr;?></td>
                        <td><?php echo $rsRecordDetail[$i]['documentId'];?></td>
                        <td><?php echo $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']);?></td>
                        <td><?php echo $rsRecordDetail[$i]['status']==0?'inc':'outg';?></td>
                        <td><?php echo $objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']);?></td>
                        <td><?php echo $rsRecordDetail[$i]['subject'];?></td>
                        <td><?php echo $rsRecordDetail[$i]['dateAdded'];?></td>
                                     
                    </tr>
                <?php
                }
        }else{
            if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['documentTypeId']==$_GET['doctype']){
                $ctr++;
                ?>
                <tr>
                    <td><?php echo $ctr;?></td>
                    <td><?php echo $rsRecordDetail[$i]['documentId'];?></td>
                    <td><?php echo $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']);?></td>
                    <td><?php echo $rsRecordDetail[$i]['status']==0?'inc':'outg';?></td>
                    <td><?php echo $objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']);?></td>
                    <td><?php echo $rsRecordDetail[$i]['subject'];?></td>
                    <td><?php echo $rsRecordDetail[$i]['dateAdded'];?></td>
                                 
                </tr>
            <?php
            }
        }
	}
	?>
</table>