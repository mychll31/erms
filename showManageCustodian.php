<? @session_start();
include_once("class/ManageCustodian.class.php");
$objCustodian = new ManageCustodian;
$arOffices = $objCustodian->getOfficeFromExeOffice();
$c = sizeof($arOffices);
	
	if($_REQUEST["officeTag"]!="")
	{	
		$tmpEmpNumber=$_REQUEST["empNumber"];
		$arEmp=$objCustodian->getEmployeeList($_REQUEST["officeTag"],$_REQUEST["officeCode"],$tmpEmpNumber);
		
		echo '<select id="custName" name="custName">';
		$c2 = sizeof($arEmp);
		for($i2=0;$i2<$c2;$i2++)
		{
		?>		
			<option value="<? echo $arEmp[$i2]["empNumber"];?>" <? if($tmpEmpNumber==$arEmp[$i2]["empNumber"]) echo "selected"; ?>><? echo $arEmp[$i2]["surname"].", ".$arEmp[$i2]["firstname"];?></option>";
		<?
		}
		echo "</select>";
	die();
	}
	
	if($_REQUEST["limit"]=="groupRequest")
	{	
		$tmpGroupCode=$_REQUEST["officeGroup"];
		$arEmp=$objCustodian->getOfficeGroup($_REQUEST["officeCode"]);
		echo '<select id="custGroup" name="custGroup">';
		echo "<option value=''> </option>";
		$c2 = sizeof($arEmp);
		for($i2=0;$i2<$c2;$i2++)
		{
		?>		
			<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
		<?
		}
		echo "</select>";
	die();
	}
	

?>
<script language="javascript" type="text/javascript"> 
var validator='';
var tmpEmpNum="";
var tmpOffGroup="";
var map={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.$arOffices[$i]['officeCode'].'":'.$i.',';
		}
		echo '"'.$arOffices[$i]['officeCode'].'":'.$i.'';
		?>
		}

var mapOffice={<? 
		for($i=0;$i<$c-1;$i++)
		{
			echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'",';
		}
		echo '"'.$arOffices[$i]['officeCode'].'":"'.$arOffices[$i]['officeTag'].'"';
		?>
		}

		
 
    //if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');

	
	$('#custRefresh').click(function(){
	jQuery("#custGrid").GridUnload();
	jQuery("#custGrid").jqGrid({
		url:'xmlparser2.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['ID','Office Code','Office Group','ID Number','Name','Email','Admin'],
		colModel:[
			{name:'custodianId',index:'custodianId', width:65, hidden:true, search:false},
			{name:'officeCode',index:'officeCode', width:65,sortable:true},
			{name:'officeGroup',index:'officeGroup', width:65,sortable:true},
			{name:'empNumber',index:'empNumber', width:100,searchoptions:{ odata:['contains'],
			sopt:['cn'],sortable:false}},
			{name:'empName',index:'empName', width:100,sortable:true,searchoptions:{ odata:['contains'],
			sopt:['cn']}},
			{name:'email',index:'email', width:100,sortable:true,searchoptions:{ odata:['contains'],
			sopt:['cn']}},			
			{name:'admin',index:'admin', width:25,sortable:true,searchoptions:{ odata:['contains'],
			sopt:['cn']}}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#custPager'),
//		postData:{searchField:'officeCode',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'false',searchOper:'ew'},
		sortname: 'officeCode',
		viewrecords: true,
//		loadonce: true, 
//        sortable: true, 		
		sortorder: "asc", //desc
		caption:"Manage Custodian"
	}).navGrid('#custPager',{edit:false,add:false,del:false,search:false});}); 
	
	$('#custRefresh').click();
  
  
    $('#custClear').click( function(){ 
	   $("#custOffice").attr("disabled",false);
	   $("#custOffice").attr("selectedIndex",0);
	   $("#custOffice").attr("selectedIndex",0);
	   $("#custSave").css('display','none');
	   $("#custAdd").css('display','block');
	   $('#Clear_custodian').text(" Clear ");
	   $('#custRefresh').click();
	   tmpEmpNum="";
	   tmpOffGroup="";
	    $("#custOffice").change();
		
	   //validator.resetForm();
    });	
	
//Edit & Save function	

	jQuery("#custEdit").click( function(){ 
	  var id = jQuery("#custGrid").getGridParam('selrow'); 

	  if (id) {
	   var ret = jQuery("#custGrid").getRowData(id); 
	   $("#custOffice").attr("selectedIndex",map[ret.officeCode]);
	   $("#custOffice").attr("disabled","disabled");
	   $("#custId").val(ret.custodianId);
	   $("#custEmail").val(ret.email);
	   //alert( $("#custId").val());
	   tmpEmpNum=ret.empNumber;
	   tmpOffGroup=ret.officeGroup;
	   if(ret.admin.toLowerCase()=="no")
	   {
	   $("#admin").removeAttr("checked");
	   }
	   else
	   {
	   $("#admin").attr("checked",true);
	   }
	   $("#custOffice").change();
//	   $("#cabLabel").val(ret.label); 
		
	   if($("#custEdit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
   	    $("#custSave").css('display','block');
		$("#custAdd").css('display','none');
		$('#Clear_custodian').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 
	
	$("#custOffice").change(function(){
	var otag=mapOffice[$("#custOffice").val()];
	var oval=$("#custOffice").val();
	  getData("showManageCustodian.php?officeCode="+oval+"&officeGroup="+tmpOffGroup+"&limit=groupRequest","custGroupDiv");
	  getData("showManageCustodian.php?officeTag="+otag+"&officeCode="+oval+"&empNumber="+tmpEmpNum,"custNameDiv");
	  
	if(oval=="<? echo RECORDS_ADMIN_VALUE;?>")
	{
	$("#admin").removeAttr('disabled');
	}
	else {
		$("#admin").removeAttr("checked");
		$("#admin").attr("disabled","disabled");
		}
	});
	
	$().ready(function(){
	$("#custOffice").change();
	})

	jQuery("#custSave").click( function(){ 
	  var custId = $("#custId").val();
 	  var officeCode = $("#custOffice").val();
	  var officeGroup = $("#custGroup").val();
	  var empNumber = $("#custName").val();
	  var email = $("#custEmail").val();
	  var admin =document.getElementById("admin").checked;
	    $("#custGrid").setPostData({mode:"save",ID:custId,CODE:officeCode,GROUP:officeGroup,EMPNUMBER:empNumber,ADMIN:admin,EMAIL:email,MODULE:'Manage Custodian'});
	   
	    $("#custGrid").trigger("reloadGrid");
 	    $('#custClear').click();
	});
	
//Add function	   
	$("#custAdd").click( function(){ 
	
	//function AddRec(){
	  var officeCode = $("#custOffice").val();
  	  var officeGroup = $("#custGroup").val();
	  var empNumber = $("#custName").val();
	  //alert(officeCode + empNumber);
		var admin = document.getElementById("admin").checked;
	  var email = $("#custEmail").val(); 
      $("#custGrid").setPostData({mode:"add",CODE:officeCode,GROUP:officeGroup,EMPNUMBER:empNumber,ADMIN:admin,EMAIL:email,MODULE:'Manage Custodian'});$("#custGrid").trigger("reloadGrid");
  	  $('#custClear').click();
	  jAlert('Succesfully Added','Information'); });
	 
	 
	
	$("#custDel").click(function(){ 
	 var id = jQuery("#custGrid").getGridParam('selarrrow'); //selrow - for 1 row
	 if(id!= ''){ 
		 jConfirm('Proceed deleting this record?', false, 'ERMS Confirmation Dialog', function(r) {
		 if(r==true){
		  $("#custGrid").setPostData({mode:"del",ID:id.toString(),MODULE:'Manage Custodian'});
		  $("#custGrid").trigger("reloadGrid"); 
		  $('#custClear').click();
		  jAlert('Succesfully deleted', 'Confirmation Results');
		 }
		}); 
	  }
	  else jAlert('Please select row to delete', 'Warning');  });
/*
     $('#cabRefresh').click();
 	 $('#cabAdd').click(function() {
      $('#frmCabinet').submit();
     });*/

//Form validation
/*
 	$().ready(function() {
	 validator = $("#frmCabinet").validate({
		rules: { cabLabel: "required" },
		messages: {	cabLabel: "Cabinet Label is required" },
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	 */	 
 
</script>
<form id="frmManageCustodian" autocomplete="off" method="get">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label">Office</td>
    <td class="field"><select id="custOffice" name="custOffice">
					<? 
					for($i=0;$i<$c;$i++)
					{
					echo "<option value=".$arOffices[$i]['officeCode'].">".$arOffices[$i]['officeName']."</option>";					}
					?>
					</select></td>
	<td class="status"></td>
  </tr>
  <tr> 
    <td class="label">Office Groups</td>
    <td class="field"><div id="custGroupDiv"><select id="custGroup" name="custGroup">
					</select></div></td>
	<td class="status"></td>
	<input type="hidden" id="custId" value="">
  </tr>
  <tr> 
    <td class="label">Custodian</td>
    <td class="field"><div id="custNameDiv"><select id="custName" name="custName">
					</select></div></td>
	<td class="status"></td>
  </tr>
  <tr> 
    <td class="label">Email</td>
    <td class="field"><input type="text" id="custEmail" name="custEmail"></td>
  </tr>  
  <tr> 
    <td class="label">Admin</td>
    <td class="field"><input type="checkbox" id="admin" name="admin" /></td>
	<td class="status"></td>
  </tr>
 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="custEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="custDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="custAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="custSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="custClear"><span title='Clear'></span><div id="Clear_custodian">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="custRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
<table id="custGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="custPager" class="scroll" style="text-align:center;"></div>
</form>			