<?php 
session_start();
include_once("class/General.class.php");
include_once("class/Records.class.php");
$objGen = new General;
$objRecords = new Records;

include_once("class/IncomingList.class.php");
$objList = new IncomingList;
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;

$years = $objGen->getSearchYearOpt();

if($_GET['mode']=="show")
{
?>
<br />
<form name="frmSearch" action="" method="post">
<table width="519"  border="0"  align="center" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
    <td width="108" height="22" valign="top"><select name="searchBy" class="textbody">
      <option value="subject">Subject</option>
      <option value="documentId">DocumentID</option>
      <option value="originId">Origin</option>
      <option value="sender">Sender</option>
      <option value="recipientId">Destination</option>
      <option value="remarks">Remarks</option>
    </select></td>
    <td colspan="3" valign="top" align="left"><input name="t_strText" type="text" size="35" /></td>
  </tr>  
  <tr>
    <td height="20" colspan="4" valign="top"><? $objGen->listDocumentType('type');?></td>
  </tr>
  <tr>
    <td height="22" colspan="4" valign="top">
	  <? $objGen->listDocumentStatus('status');?></td>
  </tr>
  <tr>
    <td height="22" colspan="4" valign="top">
    	<select name="searchyear">
    		<option value="">Year Added</option>
    		<?php foreach($years as $year): ?>
    			<option value="<?=$year['yearadded']?>"><?=$year['yearadded']?></option>
    		<?php endforeach; ?>
    	</select>
    </td>
  </tr>
  <tr>

    <td height="22" colspan="2" valign="top">
    <select name="dtmDate" class="textbody">
    <?php if(!$objGen->get("blnAgencyUser")) { ?>        
      <option value="deadline">Deadline</option>
    <?php } ?>        
      <option value="documentDate">Document Date</option>
      <option value="dateReceived">Date Received</option>
    </select>
    </td>

    <td width="193" valign="top">From : 
      <input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateFrom" name="dateFrom"></td>
    <td width="201" valign="top">To : 
      <input type="text" class="w12em dateformat-Y-ds-m-ds-d" id="dateTo" name="dateTo"></td>
    </tr>
  <tr>
    <td height="24" colspan="4" align="center" valign="top" style="text-align:center;"><input name="submit" type="button" value="Search" class="btn" onclick="clearDiv(); getMenu('showSearch.php?mode=doSearch&t_strText='+this.form.t_strText.value+'&searchBy='+this.form.searchBy.options[this.form.searchBy.selectedIndex].value+'&searchyear='+this.form.searchyear.options[this.form.searchyear.selectedIndex].value+'&type='+this.form.type.options[this.form.type.selectedIndex].value+'&status='+this.form.status.options[this.form.status.selectedIndex].value+'&dtmDate='+this.form.dtmDate.options[this.form.dtmDate.selectedIndex].value+'&dateFrom='+this.form.dateFrom.value+'&dateTo='+this.form.dateTo.value+'','searchResults');" /></td>
    </tr>
  <tr>
    <td height="1"></td>
    <td width="17"></td>
    <td></td>
    <td></td>
    </tr>
</table>

</form>
<? }?>


<? 
if($_GET['mode']=="doSearch")
{
	if($_GET['t_strText']<>"")
	{
	include_once("class/Search.class.php");
	$objSearch = new Search;
	//$rsCount = $objSearch->doSearch($_GET['t_strText']); 
	$rsCount = $objSearch->doSearch($_GET['t_strText'],$_GET['searchBy'],$_GET['searchyear'],$_GET['type'],$_GET['status'], $_GET['dtmDate'],$_GET['dateFrom'],$_GET['dateTo'],$page, $limit, $_GET['order'], $ascdesc);
	$strGroupOffice= $_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']; 
	
	$strGroupOffice= $_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office'];

	//$rsCount = $objCust->getCustDocList($objCust->get("office"));
	$addURLString = '#';
	$limit = 10;
	if($page == "") 
		$page = 0;
	else if($page<>"")
		$page = $_GET['page'];
	if($_GET['ascdesc']=="ASC")
		$ascdesc = "DESC";
	else
		$ascdesc = "ASC";



	if(count($rsCount))
		$total_pages = count($rsCount);
	//$rs = $objCust->getCustDocList($objCust->get("office"),$page, $limit, $_GET['order'], $ascdesc);
	$rs = $objSearch->doSearch($_GET['t_strText'],$_GET['searchBy'],$_GET['searchyear'],$_GET['type'],$_GET['status'], $_GET['dtmDate'],$_GET['dateFrom'],$_GET['dateTo'],$page, $limit, $_GET['order'], $ascdesc);
	if(count($rs))
	{
	echo '<br><form name="frmDocList" method="post"><table cellpadding="0" cellspacing="0" align="center" class="listings" width="99%" border="1" bordercolor="#4297d7">
		<tr class="listheader">
		<td align="center" width=140><a href="#" onClick=\'getData("showSearch.php?mode=doSearch&t_strText='.$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo'].'&page='.$_GET['page'].'&order=documentId&ascdesc='.$ascdesc.'","searchResults");\'>DOCID</a></td>
		
		<td align="center"><a href="#" onClick=\'getData("showSearch.php?mode=doSearch&t_strText='.$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo'].'&page='.$_GET['page'].'&order=subject&ascdesc='.$ascdesc.'","searchResults");\'>';
//		if ($_GET['searchBy'] == 'remarks')
//			echo 'Remarks';
//		else
			echo 'Subject';		
	echo '</a></td>
		
		<td align="center" width=30><a href="#" onClick=\'getData("showSearch.php?mode=doSearch&t_strText='.$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo'].'&page='.$_GET['page'].'&order=status&ascdesc='.$ascdesc.'","searchResults");\'>STATUS</a></td>
		
		<td align="center" width=60><a href="#" onClick=\'getData("showSearch.php?mode=doSearch&t_strText='.$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo'].'&page='.$_GET['page'].'&order=documentTypeId&ascdesc='.$ascdesc.'","searchResults");\'>DOC TYPE</a></td>
		
		<td align="center" width=60><a href="#" onClick=\'getData("showSearch.php?mode=doSearch&t_strText='.$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo'].'&page='.$_GET['page'].'&order=documentDate&ascdesc='.$ascdesc.'","searchResults");\'>DOC DATE</a></td>
		
		<td align="center" width=50>ORIGIN</td>';
		if($objGen->get("userType")==1)
			echo '<td align="center" colspan=2></td>';
		echo '</tr>';
	for($t=0;$t<count($rs);$t++)
		{

		switch($objGen->getDocumentStatus($rs[$t]['status']))
		{
			case "inc": $tab = "1"; $url = "showIncoming.php"; $div = "inc"; break;
			case "outg": $tab = "2"; $url = "showOutgoing.php"; $div = "outg"; break;
			case "intra": $tab = "3"; $url = "showIntraOffice.php"; $div = "intra"; break;
			default: $tab="0"; break;
		}
		$strOrigin=$objGen->getOfficeName($rs[$t]['originId'],$rs[$t]['officeSig'],$rs[$t]['status'],$rs[$t]['originUnit']);
		$t_dtmDate = $rs[$t]['documentDate']=="0000-00-00"?"-":$rs[$t]['documentDate'];

		$arrDocIDdetails = explode('-',$rs[$t]['documentId']);		
		if ($strGroupOffice==$arrDocIDdetails[0])
			$blnEditable = 1;  
		else
			$blnEditable = 0;  


		echo '<tr>';
//echo '<td align="center"><a href="#" onClick="getData(\''.$url.'?mode=view&id='.$rs[$t]["documentId"].'\',\''.$div.'\');">&nbsp;'.str_replace($t_strText,"<strong><font color=red>".$t_strText."</font></strong>",$rs[$t]['documentId']).'</a></td>';

		$intStatus = $objRecords->getDocumentStatus($rs[$t]['status']);		

//'onClick="getData(\''.$strPageLink.'?mode=view&id='.$arrRes['data'][$t]["documentId"].'&src=receive&edit='.$blnEditable.'\',\'doclist\');"':

$rsReceivedActions = $objDocDetail->getForActions($rs[$t]['documentId']);
// print_r($rsReceivedActions);
//$onclick2="";

if(count($rsReceivedActions)<=0) 
 {
	$onclick = 'onClick=\'getPageData("'.$url.'?mode=view&id='.$rs[$t]['documentId'].'&stat='.$rs[$t]['status'].'&sender='.($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']).'","'.$intStatus.'");  $selecttab("#'.$intStatus.'");\'';
 } 
 else
 {	 	 
	for($i=0;$i<count($rsReceivedActions);$i++) 
	{
	$onclick2="";
	$history2 = $objDocDetail->checkRecipientHistoryReceived($rsReceivedActions[$i]['historyId']);


	$dateSent = explode(' ',$rsReceivedActions[$i]['dateSent']);
	if  (strtotime('2015-03-01')-strtotime($dateSent[0])>0)
	{	
	$onclick = 'onClick=\'getPageData("'.$url.'?mode=view&id='.$rs[$t]['documentId'].'&stat='.$rs[$t]['status'].'&sender='.($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']).'","'.$intStatus.'");  $selecttab("#'.$intStatus.'");\'';
	//echo " PAO PAO 2";
	} else { 

		 
		if 	 (count($history2)<=0) 
		{	//echo " PAO PAO 3";
			$onclick = 'onClick=\'doReceiveDocumentSearch("'.$rs[$t]['documentId'].'","'.$rsReceivedActions[$i]['historyId'].'","'.$rsReceivedActions[$i]['recipientId'].'","'.$blnEditable.'");\'';
			$onclick2 = $onclick;

		} else{
			if ($onclick2!="")
			{//echo " PAO PAO 4";
				$onclick=$onclick2;
			}
			else 
			{ //echo " PAO PAO 5";
			$onclick = 'onClick=\'getPageData("'.$url.'?mode=view&id='.$rs[$t]['documentId'].'&stat='.$rs[$t]['status'].'&sender='.($_SESSION['grp']!=""?$_SESSION['grp']:$_SESSION['office']).'","'.$intStatus.'");  $selecttab("#'.$intStatus.'");\'';
			}
		}
		
	}
//	echo "<br/>".$onclick."<br>";
	} //end for
 
} // end if 


		if(strpos($onclick,"doReceive") !== FALSE) 
		{			
			

			echo '<td align="center"><strong><a href="#"' .$onclick .'>&nbsp;'.str_replace($t_strText,"<strong><font color=red>".$t_strText."</font></strong>",$rs[$t]['documentId']).'</a></strong></td>';
		}
		else
		{
			echo '<td align="center"><a href="#"' .$onclick .'>&nbsp;'.str_replace($t_strText,"<strong><font color=red>".$t_strText."</font></strong>",$rs[$t]['documentId']).'</a></td>';
		}
		
		if ($_GET['searchBy'] == 'remarks')
			echo '<td align="center">&nbsp;'.strtolower($rs[$t]['subject']).'</td>';
		else
			echo '<td align="center">&nbsp;'.str_replace(strtolower($t_strText),"<font color=red><strong>".strtolower($t_strText)."</strong></font>",strtolower($rs[$t]['subject'])).'</td>';
		
		
		echo '<td style="text-align:center;">&nbsp;'.$objGen->getDocumentStatus($rs[$t]['status']).'</td>
				<td align="center">&nbsp;'.$objGen->getDocumentType($rs[$t]['documentTypeId']).'</td>
				<td style="text-align:center;">&nbsp;'.$t_dtmDate.'</td>
				<td align="center">&nbsp;'.$strOrigin.'</td>';
				//str_replace($t_strText,"<strong><font color=red>".$t_strText."</font></strong>",$objGen->getOfficeName($rs[$t]['originId'],$rs[$t]['officeSig'],$rs[$t]['status']))

		if($objGen->get("userType")=='1')
		{
		echo "<td align='center' width=1>
			<span class='ui-icon ui-icon-circle-arrow-n' title='Upload Document' onClick=\"showUploadWindow('".$rs[$t]['documentId']."')\"></span></td>
			<td align='center' width=1>
			<span class='ui-icon ui-icon-trash' title='Delete' onClick=\"doDelete('".$rs[$t]['documentId']."'); getData('showSearch.php?mode=doSearch&t_strText=".$_GET['t_strText']."&searchBy=".$_GET['searchBy']."&searchyear=".$_GET['searchyear']."&type=".$_GET['type']."&status=".$_GET['status']."&dtmDate=".$_GET['dtmDate']."&dateFrom=".$_GET['dateFrom']."&dateTo=".$_GET['dateTo']."','searchResults');\"></span></td>";
		}



		echo "</tr>";	  		
		}//end loop
	echo '</table></form><br>';		
//	echo $objGen->showPagination($addURLString, $page, $total_pages, $limit,"searchResults","showSearch.php?mode=doSearch&t_strText=".$_GET['t_strText']);	
	echo $objGen->showPagination($addURLString, $page, $total_pages, $limit,"searchResults","showSearch.php?mode=doSearch&t_strText=".$_GET['t_strText']
	."&searchBy=".$_GET['searchBy']
	."&searchyear=".$_GET['searchyear']
	."&type=".$_GET['type']
	."&status=".$_GET['status']
	."&dtmDate=".$_GET['dtmDate']
	."&dateFrom=".$_GET['dateFrom']
	."&dateTo=".$_GET['dateTo']);	



}//end if
	else
		echo "<br>No records found<br><br>";
	}
	else
		echo "<br>Please input a text<br><br>";	

}
if($_GET['mode']=="docsearch")
{
	echo "<table align=center><tr><td><input type='textbox'><input type='button' value='Search'></td></tr></table>";
}
?>

<script type="text/javascript">

function doReceiveDocumentSearch(docid,hid,rid,blnEditable,stat)
	{
		
	var url = 'showReceiveDialog.php?mode=receive&docid='+docid+'&hid='+hid+'&rid='+rid+'&stat'+stat+'&edit='+blnEditable+'&src=search';
	jQuery.facebox({ ajax: url });	
	} 
	 

</script>	
