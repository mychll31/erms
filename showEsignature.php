<?php
session_start();
include_once("class/IncomingList.class.php");

$objIncgList = new IncomingList;

$docid = $_GET['docid'];
$historyid = $_GET['historyid'];
$img = $objIncgList->getImgLocation($docid,$historyid);
if(count($img) > 0):
	$imgLoc = substr($img[0]['signature_location'],14);
else:
	echo '<font color="red">NO E-SIGNATURE DATA AVAILABLE</font>';
	die();
endif;
?>

<img src="<?=$imgLoc?>" alt="">
<div>
	Received By: <?=$_GET['recdata']?></div>
