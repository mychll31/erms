<? @session_start();
include_once("class/MySQLHandler.class.php");
include_once("class/General.class.php");
include_once("class/Outgoing.class.php");
include_once("class/IncomingList.class.php");
$objList = new IncomingList;
$objOutgoing = new Outgoing;
$file=new General();
$group = explode('~',$objList->getOfficeCode($objList->get('userID')));
if($_REQUEST['mode']=='update' && $_REQUEST['blnMinDocInfo'])
{
	 echo $objOutgoing->updateDocNo($_REQUEST,'outg');
}
if($_REQUEST['mode']=='view' || $_REQUEST['mode']=='update' || $_REQUEST['mode'] == 'save'){
	echo "<style>.ui-datepicker-trigger{ display:none; }</style>";
}


################## added by LCM #############
if($_GET['src']=="receive")
	{
	//echo $_GET['id'];
	$objList->markRead($_GET['id'],$objOutgoing->get('userID'));
	}
$hashId = ($_GET['src'] == 'receive')?'doclist':'outg';
	
##############################################

if($_GET['mode']=="") $arrFields = $objOutgoing->getUserFields();
else $arrFields = $objOutgoing->getUserFields1();


################################
#
#  modes: new - for new Outgoing document
#	 	  edit - editing old document
#		  save - saving & viewing the new document
#		  update - updates the edited document
#
################################

if($_REQUEST["mode"]=="getGroup")
	{	
		$userType=$objOutgoing->get('userType');
		$tmpGroupCode=$_REQUEST["officeGroup"];
		
		echo '<select id="cmbGroupCodeOutgoing" name="cmbGroupCodeOutgoing">';
		
		if($userType=='1'){
			$arEmp=$objOutgoing->getOfficeGroup($_REQUEST["officeCode"]);
			echo "<option value=''> </option>";
			$c2 = sizeof($arEmp);
			for($i2=0;$i2<$c2;$i2++)
			{
			?>		
				<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
			<?
			}
		}
		else
		{
		
			$empnum=$objOutgoing->get('empNum');
			$arEmp=$objOutgoing->getOfficeGroupEmployee($empnum);
			$c2 = sizeof($arEmp);
			for($i2=0;$i2<$c2;$i2++)
			{
			?>		
				<option value="<? echo $arEmp[$i2]["groupCode"];?>" <? if($tmpGroupCode==$arEmp[$i2]["groupCode"]) echo "selected"; ?>><? echo $arEmp[$i2]["groupName"];?></option>";
			<?
			}
		}
		echo "</select>";
	die();
	}
	
	
if($arrFields['mode']=='edit' ||  $arrFields['mode']=='view')
{
$rsDocument= 		$objOutgoing->getDocument($arrFields['id']);
$t_strNewId = 		$rsDocument[0]['documentId'];
$t_strReferenceId=	$rsDocument[0]['referenceId'];
$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
$t_strSubject =    	$rsDocument[0]['subject'];
$t_intOriginId =   	$rsDocument[0]['originId'];
$t_intGroupCode= 	$rsDocument[0]['originGroupId'];
$deadlineOutgoing = $rsDocument[0]['deadline'];
$t_contdoc =   $rsDocument[0]['controlleddoc'];
$fileLocation = $file->getLocation($t_strNewId);
$t_intContainer = $fileLocation['containerId'];
$t_intContainer2 = $fileLocation['container2Id'];
$t_intContainer3 =  $fileLocation['container3Id'];
$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
$rsContainer2 = $objOutgoing->getContainerTwo($t_intContainer, $t_intContainer2);
$rsContainer3 = $objOutgoing->getContainerThree($t_intContainer2, $t_intContainer3);
$t_strRemarks =    	$rsDocument[0]['remarks'];
$t_intConfidential =$rsDocument[0]['confidential'] ;
$DocNum =			$rsDocument[0]['docNum'];
$t_intOfficeId=		$rsDocument[0]['officeSig'];
$addedByOfficeId =  $rsDocument[0]['addedByOfficeId'];
$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
$signatoryOutgoing=	$rsDocument[0]['sender'];
$disable_ID="disabled=\"disabled\"";
$nextMode="update";
}
elseif ($arrFields['mode']=='save')
{
//echo "station2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//echo $_SERVER["REQUEST_URI"];
//print_r($_POST);
	$result=$objOutgoing->addOutDocument($arrFields);
		if ($result==0)
		{
		$arrFields['mode']="new";
		$nextMode="save";
		}
		else
		{
		$nextMode="update";
		$disable_ID="disabled=\"disabled\"";
		}
	$msg = $objOutgoing->getValue('msg');
	// assign saved data to view
	if ($result==0)
		{
	$t_strNewId  =    $arrFields['t_strDocId'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];	
	$documentDateOutgoing =    $arrFields['documentDateOutgoing'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_intOriginId =       $arrFields['cmbOrigin'];
	$deadlineOutgoing =       $arrFields['deadlineOutgoing'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$rsContainer2 = $objOutgoing->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objOutgoing->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_contdoc =  $arrFields['t_contdoc'];
	$DocNum = $arrFields['t_strDocNum'];
	$t_intOfficeId=		$arrFields['cmbOriginOutgoing'];
	$signatoryOutgoing=	$arrFields['signatoryOutgoing'];
	$addedByOfficeId = $objOutgoing->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	}
	else
	{
	$rsDocument= 		$objOutgoing->getDocument($arrFields['t_strDocId']);
	$t_strNewId = 		$rsDocument[0]['documentId'];
	$t_strReferenceId=	$rsDocument[0]['referenceId'];
	$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
	$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
	$t_strSubject =    	$rsDocument[0]['subject'];
	$t_intOriginId =   	$rsDocument[0]['originId'];
	$deadlineOutgoing = $rsDocument[0]['deadline'];
	$fileLocation = $file->getLocation($t_strNewId);
	$t_intContainer = $fileLocation['containerId'];
	$t_intContainer2 = $fileLocation['container2Id'];
	$t_intContainer3 =  $fileLocation['container3Id'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$rsContainer2 = $objOutgoing->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objOutgoing->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =    	$rsDocument[0]['remarks'];
	$t_intConfidential =$rsDocument[0]['confidential'];
	$DocNum =			$rsDocument[0]['docNum'];
	$t_intOfficeId=		$rsDocument[0]['officeSig'];
	$signatoryOutgoing=	$rsDocument[0]['sender'];
	$addedByOfficeId =  $rsDocument[0]['addedByOfficeId'];
	$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
	$t_contdoc = $rsDocument[0]['controlleddoc'];
	$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
	}
}
elseif($arrFields['mode']=='update')
{
	$result=$objOutgoing->updateOutDocument($arrFields);
	$msg = $objOutgoing->getValue('msg');
	$disable_ID="disabled=\"disabled\"";
	if($result==0){
	
	$arrFields['mode']="edit";
	$nextMode="update";
	$t_strNewId  =    $arrFields['t_strDocId'];
	$documentDateOutgoing =    $arrFields['documentDateOutgoing'];
	$t_intDocTypeId =      $arrFields['cmbDocType'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $arrFields['t_strDocType'];
	$t_strSubject =    $arrFields['t_strSubject'];
	$t_intOriginId =       $arrFields['cmbOrigin'];
	$deadlineOutgoing =       $arrFields['deadlineOutgoing'];
	$t_intContainer =  $arrFields['t_intContainer'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$rsContainer2 = $objOutgoing->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objOutgoing->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =   $arrFields['t_strRemarks'];
	$t_intConfidential =  $arrFields['t_intConfidential'];
	$t_strReferenceId=$arrFields['t_strReferenceId'];
	$DocNum = $arrFields['t_strDocNum'];
	$t_intOfficeId=		$arrFields['cmbOriginOutgoing'];
	$signatoryOutgoing=	$arrFields['signatoryOutgoing'];
	$addedByOfficeId = $objIncoming->get("office");
	$t_strManagedOffice = $arrFields['documentOwner'];
	$t_contdoc = $arrFields['controlleddoc'];
	}
	else
	{
	$rsDocument= 		$objOutgoing->getDocument($arrFields['t_strDocId']);
	$t_strNewId = 		$rsDocument[0]['documentId'];
	$documentDateOutgoing =    	$rsDocument[0]['documentDate'];
	$t_intDocTypeId =  	$rsDocument[0]['documentTypeId'];
	$rsDocTypeAbbrev = $objOutgoing->getDocType($t_intDocTypeId);
	$t_strDocType = $rsDocTypeAbbrev[0]['documentTypeAbbrev'];
	$t_strSubject =    	$rsDocument[0]['subject'];
	$t_intOriginId =   	$rsDocument[0]['originId'];
	$deadlineOutgoing = $rsDocument[0]['deadline'];
	$fileLocation = $file->getLocation($t_strNewId);
	$t_intContainer = $fileLocation['containerId'];
	$t_intContainer2 = $fileLocation['container2Id'];
	$t_intContainer3 =  $fileLocation['container3Id'];
	$rsContainer = $objOutgoing->getContainer($objOutgoing->get("office"),$t_intContainer);
	$rsContainer2 = $objOutgoing->getContainerTwo($t_intContainer, $t_intContainer2);
	$rsContainer3 = $objOutgoing->getContainerThree($t_intContainer2, $t_intContainer3);
	$t_strRemarks =    	$rsDocument[0]['remarks'];
	$t_intConfidential =$rsDocument[0]['confidential'];
	$t_strReferenceId=$rsDocument[0]['referenceId'];
	$DocNum =			$rsDocument[0]['docNum'];
	$t_intOfficeId=		$rsDocument[0]['officeSig'];
	$signatoryOutgoing=	$rsDocument[0]['sender'];
	$rsOriginDetails = $objOutgoing->getOfficeDetails($t_intOfficeId);
	$addedByOfficeId = $rsDocument[0]['addedByOfficeId'];
	$t_strManagedOffice = $rsDocument[0]['addedByOfficeId'];
	$t_contdoc = $rsDocument[0]['controlleddoc'];
	$t_intConfidential = ($t_intConfidential == '1')?'1':'0';
	if($t_intConfidential==1) $objOutgoing->transferFile($t_strNewId,$t_intConfidential);  
	}
	

}
else //New
{
	$t_strNewId=$objOutgoing->getNewId("outg");
	$t_intDocTypeId=-1;
	$t_intOriginId=-1;
	$dateReceived = date("Y-m-d");
	$disable_ID="";
	$nextMode="save";
	if ($objOutgoing->get("userType")==2) $t_intOfficeId=$objOutgoing->get("office");  //for custodian account
}

$ctrlcontdocs = $file->getControlledDocsById($t_strNewId);
$t_contdoc = $ctrlcontdocs['controlleddoc'];
$t_cd_copyno = $ctrlcontdocs['copyno'];
$t_cd_copyholder = $ctrlcontdocs['copyholder'];
$t_cd_mannerdisposal = $ctrlcontdocs['mannerOfDisposal'];
$t_cd_revisionno = $ctrlcontdocs['revisionno'];
$t_cd_personres = $ctrlcontdocs['personUnitResponsible'];
$t_cd_withrawalno = $ctrlcontdocs['withrawalno'];

$arDocType=$objOutgoing->getDocType("");
$arOrigin=$objOutgoing->getOriginOffice("");
$arOffices=$objOutgoing->getOfficeFromExeOffice();
$arContainer=$objOutgoing->getContainer($objOutgoing->get("office"),"");
$arManagedOffice=$objOutgoing->getManagedOffice($objOutgoing->get('empNum'));

?>
 	<? if($msg<>""){
	if(strpos($msg,"Duplicate entry")!==false){
		?>
		 <script type="text/javascript">
		 if(confirm("Document ID already existing. Get new ID?")==true){
		     $("#idcontaineroutgoing").load("showOutgoing.php?mode=new #idcontaineroutgoing",function(){
			 	alert("Please save again");
			 });
		 }		 
		 </script>
		<?
		}
		else{
		?>
			<div class="pane">
			<div class="ui-widget" style="width:40%">
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
					<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Notice: </strong><? echo $msg;?></p>
				</div>
			</div>
			</div>
			<script type="text/javascript">
				//setTimeout("disappear()",2000);
				$(".pane").fadeOut(3000,function(){
				 $(function(){ 
				  $.ajax({ 
					url: "manageBarcodeConf.php",
					data: "mode=select&employee_num=<? echo $objList->get('userID'); ?>",
					success: function(data){ 
					 if(data==1){ printBarcode("<? echo $t_strNewId;?>","<? echo $group[0]; ?>"); }
					}
				  });
				});					
				})
				
			</script>
	<? }} ?>
<? if($arrFields["mode"]=="save" || $arrFields["mode"]=="update" || $arrFields["mode"]=="view"){
	?>
	<style type="text/css">
		.tdpads{
			padding: 2px 5px;
			white-space: nowrap;
			text-align: right !important;
		}
		.tdbor{
			border-right: 1px solid #6699cc;
		}
	</style>
	<table align="center" width="605px" class="documentContainer">
	<tr><td>
			
			<table align="center" width="605px" class="datawrap">

				<tr><td>
				<table class="metadata">
					<tr class="metabutton">
						<td colspan="2" class="containerlabel">Document Details</td>
						<td style="text-align:right" colspan="2" class="tdbor">
						<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
						<?php if($objOutgoing->isCustodian($objOutgoing->get("empNum"),$addedByOfficeId) && $arrFields["edit"]!="false"){ ?>
						<input style="cursor:pointer" type="button" name="editForm" id="editForm" value="Edit Info" onClick="getData('showOutgoing.php?mode=edit&id=<? echo $t_strNewId; ?>&src=<?php echo $arrFields['src']; ?>','<?=$hashId ?>');" class="btn"/>&nbsp;	
						<input style="cursor:pointer" type="button" name="addForm" value="Add New" onClick="getData('showOutgoing.php?mode=new','<?=$hashId ?>');" class="btn"/>
						<!-- <input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />	 -->
						<?php } 
						 else if($objOutgoing->get('office') === 'RMS' && $objOutgoing->get('userType')===1){ ?>
						<input style="cursor:pointer" type="button" id="btnEditDocNo" value="Edit Info" onClick="editMinDocDetails('<? echo $t_strNewId;?>','<?=$DocNum; ?>','<?=$hashId ?>');" class="btn"/>&nbsp;	
     						<!-- <input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />  -->
    						<?php } 
						 else if($objOutgoing->get('office') == 'OASECFALA' ){ ?> 
						<!-- <input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />&nbsp; -->
						<?php } ?>
						<!-- SET PRINT BARCODE -->
						<input style="cursor:pointer" type="button" name="printbarcode" value="Print Barcode" onclick="printBarcode('<? echo $t_strNewId;?>')" class="btn" />&nbsp;
						<?php endif; ?> <!-- endif action = 54 -->
						</td>
					</tr>
					  <tr>
						<th width="100px" class="tdpads">Document ID:</th>
						<td width="200px">&nbsp;<?php echo $t_strNewId; ?></td>
						<th width="100px" class="tdpads">Document Type:</th>
						<td width="200px" class="tdbor">&nbsp;<?php echo $rsDocTypeAbbrev[0]['documentTypeAbbrev']; ?>&nbsp;</td>
					  </tr>
					  <tr>
                      <?php if(!$objOutgoing->get("blnAgencyUser")){ ?>
						<th class="tdpads">Deadline:</th>
						<td>&nbsp;<? echo $deadlineOutgoing;?></td>
                      <?php } ?>
						<th class="tdpads">Document Date:</th>
                        <td id="docDateViewMode" class="tdbor">&nbsp;
                         <span><? echo $documentDateOutgoing;?></span>
                         <input type="text" id="documentDateClone" value="<? echo $documentDateOutgoing;?>" style="display:none">
                        </td>
                        
					  </tr>
					<tr><td colspan="4" class="tdbor">&nbsp;</td></tr>
					<tr>
						<th class="tdpads">Doc No.:</th>
						<td id="docNoViewMode">&nbsp;<? echo $DocNum;?></td>
						<td></td>
						<td class="tdbor"></td>		
					</tr>
					  <tr>
						<th class="tdpads">Subject:</th>
						<td colspan="3" class="tdbor" rowspan="2" style="vertical-align: top;" id="docSubjViewMode">&nbsp;<?php echo $t_strSubject; ?></td>
					  </tr>
					<tr><td>&nbsp;</td></tr>
					  <tr>
						<th class="tdpads">Origin:</th>
                        <td>&nbsp;
                        <?php if($objOutgoing->get("blnAgencyUser")){ ?> 
                        <label><?php echo $objOutgoing->getOfficeCode2($objOutgoing->get('userID')); ?></label>
                        <input type="hidden" name="cmbOriginOutgoing" value="<?php echo $objOutgoing->get('agencyCode'); ?>"  />
                        <?php } else { ?>
						<?php echo $rsOriginDetails[0]['oName'];//$t_intOfficeId; ?>&nbsp;
                        <?php } ?>
                        </td>
                        <?php if(!$objOutgoing->get("blnAgencyUser")){ ?>
						<th class="tdpads">Signatory:</th>
						<td class="tdbor" id="docSignatoryViewMode">&nbsp;<? echo $signatoryOutgoing;?></td>
                        <?php } ?>
					  </tr>
					  <?php if($addedByOfficeId==$_SESSION['office']): ?>
					  <tr>
						<th class="tdpads">File Container:</th>
						<td>&nbsp;
							<? echo $rsContainer[0]['label'].'> '.$rsContainer2[0]['label'].'> '.$rsContainer3[0]['label'];?>
							<?php $empdetails = $file->getEmpDetails($fileLocation['updatedby']);?>
							<?php if($fileLocation['updatedby'] != null): ?>
								<br><small><i>
								Last Modified By: <?=$empdetails['fname'].' '.$empdetails['lname']?> (<?=date('Y-m-d', strtotime($fileLocation['updateddate']))?>)
								</i></small>
							<?php endif; ?>
						</td>
						<th class="head tdpads" style="vertical-align: top;" rowspan="2">Remarks:</th>
						<td class="tdbor" rowspan="2" valign="top">&nbsp;<? echo $t_strRemarks;?></td>
					  </tr>
					 <?php endif; ?>
				 	 <?php if(!$objOutgoing->get("blnAgencyUser")){ ?> 
					
					<!-- End Related Document Notification History -->
				 	 <style>
						.tooltip {
						    position: relative;
						    display: inline-block;
						    border-bottom: 1px dotted black;
						}

						.tooltip .tooltiptext {
						    visibility: hidden;
						    width: 120px;
						    background-color: #fff;
						    color: #fff;
						    text-align: center;
						    border-radius: 6px;
						    padding: 5px 0;

						    
						    position: absolute;
						    z-index: 1;
						}

						.tooltip:hover .tooltiptext {
						    visibility: visible;
						}
						.td4{
							vertical-align: top;
							padding-left: 7px;
						}
					 </style>
					 <!-- Begin Related Document Notification History -->

					 <tr><th class="tdpads">Related Docs:</th><td colspan="3" class="tdbor"></td></tr>
					 <tr><td>&nbsp;</td><td class="tdbor" colspan="3"><div class="relatedDocs"><?
						$arrRelated=null;
					 	if($t_strReferenceId==''){
					 		$t_strReferenceId = $objOutgoing->getRelatedDocsByParentDoc($t_strNewId);
					 		if(count($t_strReferenceId)){
					 			$t_strReferenceId = $t_strReferenceId["documentId"].','.$t_strReferenceId["referenceId"];
					 			$t_strReferenceId = str_replace($t_strNewId, '', $t_strReferenceId);
					 		}
					 	}
					 	if (strpos($t_strReferenceId, ',') !== false) {
					 		$t_strReferenceId = rtrim(str_replace(",", "','", str_replace(' ', '', $t_strReferenceId)), ",'");
					 		$t_strReferenceId = "'".$t_strReferenceId."'";
					 		$arrRelated=$objOutgoing->getRelatedDocsByRefId($t_strReferenceId);
					 	}else{
					 		$arrRelated=$objOutgoing->getRelatedDocs($t_strNewId);
							//Additional for OriginRelatedRecord
							$arrRelated1=$objOutgoing->getOriginRelatedRecord($t_strNewId);
							if(count($arrRelated) < 1){
								$arrRelated = $arrRelated1;
							}
					 	}
						$cRelated=count($arrRelated);
						$rscRelated=count($arrRelated);
						if($cRelated>0) {
							$intStatus = $objOutgoing->getDocumentStatus($arrRelated[0]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo "<div class=\"tooltip\"><a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[0]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[0]["documentId"]."</a>";
							echo "<div style='overflow:auto; width:400px;height:320px;background-color: rgba(255, 255, 255, 0)' class='tooltiptext'></div></div>";
							echo "<script id='tooltipdiv' type='text/javascript'>
									$('.tooltiptext').load('showHistory.php?mode=incoming&docID=".$t_strNewId."&div=actionbodyincoming');
								</script>";
							}
						for($cntRelated=1;$cntRelated<$cRelated;$cntRelated++)
						{
						
						$intStatus = $objOutgoing->getDocumentStatus($arrRelated[$cntRelated]["status"]);
							if($intStatus=="inc") // Incoming
								{$strPageLink= "showIncoming.php";}
							elseif($intStatus=="outg")//Outgoing
								{$strPageLink= "showOutgoing.php";}
							else //Intra
								{$strPageLink= "showIntraOffice.php";}
							echo "<div class=\"tooltip\">, <a href='#' onclick=\"getData('".$strPageLink."?mode=view&id=".$arrRelated[$cntRelated]["documentId"]."','".$intStatus."');selecttab('#".$intStatus."');\">".$arrRelated[$cntRelated]["documentId"]."</a>";
							echo "<div style='overflow:auto; width:400px;height:320px;background-color: rgba(255, 255, 255, 0)' class='tooltiptext'></div></div>";
							echo "<script id='tooltipdiv' type='text/javascript'>
									$('.tooltiptext').load('showHistory.php?mode=incoming&docID=".$t_strNewId."&div=actionbodyincoming');
								</script>";
						    //echo ", ".$arrRelated[$cntRelated]["documentId"];
						}
						?>
						</div>
					 </td></tr>
                     <?php } ?>
					 
					<!-- Begin Controlled Document Condition to be removed -->
					<?php if($addedByOfficeId==$_SESSION['office']): ?>
					<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
					<tr>
						<td nowrap class="td4" style="text-align: right;"><b>Controlled Document:</b> <br><?=$ctrlcontdocs['description']?></td>
							<?php 
								if($ctrlcontdocs['controlleddoc']==1){
									echo '<td class="td4"><b>Copy no: </b><br>&nbsp;&nbsp;&nbsp;'.$ctrlcontdocs['copyno'].'</td>';
									echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
									echo '<td class="td4 tdbor"><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
								}else if($ctrlcontdocs['controlleddoc']==2){
									echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
									echo '<td class="td4"><b>Manner of Disposal: </b><br>'.$ctrlcontdocs['mannerOfDisposal'].'</td>';
									if($_SESSION['office']=='ITD') {
										echo '<td class="td4"><b>Doc Code: </b><br>'.$ctrlcontdocs['mannerOfDisposal'].'</td>';
									}else{
										echo '<td class="td4 tdbor"></td>';
									}
								}else if($ctrlcontdocs['controlleddoc']==3){
									echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
									echo '<td class="td4 tdbor" colspan=2><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
								}else if($ctrlcontdocs['controlleddoc']==4 || $ctrlcontdocs['controlleddoc']==5){
									echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
									echo '<td class="td4"></td>';
									echo '<td class="td4 tdbor"></td>';
								}else if($ctrlcontdocs['controlleddoc']==6){
									echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
									echo '<td class="td4 tdbor" colspan=2><b>Issue/ Withrawal Control no.: </b><br>'.$ctrlcontdocs['withrawalno'].'</td>';
								}else{
									echo '<td class="td4"></td><td class="td4"></td><td class="td4 tdbor"></td>';
								}
							 ?>
					</tr>
					<?php else: ?>
						<!-- Begin Editable Controlled Document. -->
						<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
						<?php if($_SESSION['userType'] == 3): ?>
							<tr>
								<td nowrap class="td4" style="text-align: right;"><b>Controlled Document:</b> <br><?=$ctrlcontdocs['description']?></td>
									<?php 
										if($ctrlcontdocs['controlleddoc']==1){
											echo '<td class="td4"><b>Copy no: </b><br>'.$ctrlcontdocs['copyno'].'1</td>';
											echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
											echo '<td class="td4 tdbor"><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
										}else if($ctrlcontdocs['controlleddoc']==2){
											echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
											echo '<td class="td4"><b>Manner of Disposal: </b><br>'.$ctrlcontdocs['mannerOfDisposal'].'</td>';
											if($_SESSION['office']=='ITD') {
												echo '<td class="td4"><b>Doc Code: </b><br>'.$ctrlcontdocs['mannerOfDisposal'].'</td>';
											}else{
												echo '<td class="td4 tdbor"></td>';
											}
										}else if($ctrlcontdocs['controlleddoc']==3){
											echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
											echo '<td class="td4 tdbor" colspan=2><b>Person/Unit Responsible: </b><br>'.$ctrlcontdocs['personUnitResponsible'].'</td>';
										}else if($ctrlcontdocs['controlleddoc']==4 || $ctrlcontdocs['controlleddoc']==5){
											echo '<td class="td4"><b>Copy Holder/s: </b><br>'.$ctrlcontdocs['copyholder'].'</td>';
											echo '<td class="td4"></td>';
											echo '<td class="td4 tdbor"></td>';
										}else if($ctrlcontdocs['controlleddoc']==6){
											echo '<td class="td4"><b>Revision no.: </b><br>'.$ctrlcontdocs['revisionno'].'</td>';
											echo '<td class="td4 tdbor" colspan=2><b>Issue/ Withrawal Control no.: </b><br>'.$ctrlcontdocs['withrawalno'].'</td>';
										}else{
											echo '<td class="td4"></td><td class="td4"></td><td class="td4 tdbor"></td>';
										}
									 ?>
							</tr>
						<?php else: ?>
					   	<tr>
					   		<td colspan=2 class="tdpads" valign="top"><br>
					   			<b>Controlled Document: </b>
					   			<?php $ctrlDocs = $file->getControlledDocDesc(); ?>
						      	<select name="t_contdoc" class="caption" id="sel_contdoc_outg">
								  	<option value="0"></option>
								  	<?php foreach($ctrlDocs as $contdoc): ?>
								  	<option value="<?=$contdoc['id']?>" <?=($t_contdoc==$contdoc['id']) ? 'selected' : ''?>><?=$contdoc['description']?></option>
								  	<?php endforeach; ?>
								</select>
								<br><input type="button" id="tblControlleddoc_outg" style="background-color: #6699cc !important;color: #fff !important;border: 1px solid #000;" value="Update Controlled Document">
				   	  		</td>
				   	  		<td colspan=2 class="tdbor">
				   	  			<div id="conta_outg" <?=($t_contdoc==1)? '' : 'style="display: none;"'?> >
				   	  				<b>Copy No.</b>&nbsp;&nbsp;<br>
						      		<input type="text" id="txt_conta_copyno" name="txt_conta_copyno" value="<?=$t_cd_copyno?>" style="width: 180px !important;height: 12px !important;">
						      	</div>
						      	<div id="contb_outg" <?=($t_contdoc==1 || $t_contdoc==2 || $t_contdoc==4 || $t_contdoc==5)? '' : 'style="display: none;"'?> >
						      		<b>Copy Holder/s</b>&nbsp;&nbsp;<br>
						      		<input type="text" id="txt_contb_copyholder" name="txt_contb_copyholder" value="<?=$t_cd_copyholder?>" style="width: 180px !important;height: 12px !important;">
						      	</div>
						      	<div id="contc_outg" <?=($t_contdoc==2)? '' : 'style="display: none;"' ?> >
						      		<b>Manner of Disposal</b>
						      		<input type="text" id="txt_contc_mannerdisposal" name="txt_contc_mannerdisposal" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important;height: 12px !important;">
						      		<?php if($_SESSION['office']=='ITD'): ?>
							      		<br>
							      		<b>Doc Code</b><br>
							      		<input type="text" id="txt_contc_docCode" name="txt_contc_docCode" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important;height: 12px !important;">
						      		<?php endif; ?>
						      	</div>
						      	<div id="contd_outg" <?=($t_contdoc==3)? '' : 'style="display: none;"'?> >
						      		<b>Revision no.</b>&nbsp;&nbsp;<br>
						      		<input type="text" id="txt_contd_revisionno" name="txt_contd_revisionno" value="<?=$t_cd_revisionno?>" style="width: 180px !important;height: 12px !important;">
						      	</div>
						      	<div id="conte_outg" <?=($t_contdoc==3 || $t_contdoc==1)? '' : 'style="display: none;"' ?> >
						      		<b>Person/Unit Responsible</b>
						      		<input type="text" id="txt_conte_unitres" name="txt_conte_unitres" value="<?=$t_cd_personres?>" style="width: 180px !important;height: 12px !important;">
						      	</div>
						      	<div id="contf_outg" <?=($t_contdoc==6)? '' : 'style="display: none;"' ?> >
						      		<b>Issue/ Withrawal Control #</b>	
						      		<input type="text" id="txt_withrawal" name="txt_withrawal" value="<?=$t_cd_withrawalno?>" style="width: 180px !important;height: 12px !important;">
						      	</div>
						    </td>
						    <?php endif; ?> <!-- endif action = 54 -->
					  	</tr>
					  	<?php endif; ?>
				  	<!-- End Editable Controlled Document -->
				  	<?php endif; ?> <!-- endif action = 54 -->
					<!-- End Controlled Document -->
					<tr><td class="tdbor" colspan="4">&nbsp;</td></tr>

					<!-- Begin Edit Cabinet. Hide when Condition is true -->
					<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
					<?php if($_SESSION['userType']==3): ?>
						<tr>
							<th class="tdpads">File Container:</th>
							<td>&nbsp;<? echo $rsContainer[0]['label'].'> '.$rsContainer2[0]['label'].'> '.$rsContainer3[0]['label'];?>
								<?php $empdetails = $file->getEmpDetails($fileLocation['updatedby']);?>
								<?php if($fileLocation['updatedby'] != null): ?>
									<br><small><i>
									Last Modified By: <?=$empdetails['fname'].' '.$empdetails['lname']?> (<?=date('Y-m-d', strtotime($fileLocation['updateddate']))?>)
									</i></small>
								<?php endif; ?>
							</td>
					  </tr>
					<?php else: ?>
					<tr>
						<th class="tdpads">Cabinet:</th>
						<td class="tdbor" colspan="4">
							<select name="t_intContainer" id="selcabinet_outg_edit_mode" class="caption">
								<option value="-1"> </option>
								<?php for($i=0;$i<sizeof($arContainer);$i++){ ?>
								<option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<th class="tdpads">Drawer:</th>
						<td class="tdbor" colspan="4">
							<select name="t_intContainer2" id="seldrawer_outg_edit_mode" class="caption">
								<option value="-1"> </option>
								<?php for($i=0;$i<sizeof($arContainer2);$i++) { ?>
								<option value="<? echo $arContainer2[$i]['container2Id']; ?>" <? if($arContainer2[$i]['container2Id']== $t_intContainer2) echo "selected"; ?> > <? echo $arContainer2[$i]['label'];?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<th class="tdpads">Container:</th>
						<td class="tdbor" colspan="4">
								<select name="t_intContainer3" id="selcontainer_outg_edit_mode" class="caption">
									<option value="-1"> </option>
									<?php for($i=0;$i<sizeof($arContainer3);$i++) { ?>
									<option value="<? echo $arContainer3[$i]['container3Id']; ?>" <? if($arContainer3[$i]['container3Id']== $t_intContainer3) echo "selected"; ?> > <? echo $arContainer3[$i]['label'];?></option>
									<?php } ?>
								</select>
							</td>
					</tr>
					<?php endif; ?>
					<?php endif; ?> <!-- endif action = 54 -->
					<tr>
						<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
						<td></td>
						<td colspan="4" class="tdbor"><input type="button" id="tblupdateLocation_outg" style="background-color: #6699cc !important;color: #fff !important;border: 1px solid #000;" value="Update File Location">
							<?php $empdetails = $file->getEmpDetails($fileLocation['updatedby']);?>
							<?php if($fileLocation['updatedby'] != null): ?>
								<br><small><i>
								Last Modified By: <?=$empdetails['fname'].' '.$empdetails['lname']?> (<?=date('Y-m-d', strtotime($fileLocation['updateddate']))?>)
								</i></small>
							<?php endif; ?>
						</td>
						<?php endif; ?> <!-- endif action = 54 -->
					</tr>
					<tr><td class="tdbor" colspan="4">&nbsp;</td></tr>
					<?php endif; ?>

					 <tr>
						<th class="tdpads">Owned by:</th>
						<td>&nbsp;<? echo $addedByOfficeId;?></td>
						<th>&nbsp;</th>
						<td rowspan="2" class="tdbor">&nbsp;</td>
					</tr>
					  <tr>
						<td colspan="4" class="tdbor">&nbsp;</td>
					  </tr>

	 <? if ($t_intConfidential)
		{
		?>
						<tr>
							<td colspan="4" style="text-align:center">
							<div class="ui-widget" align="center" >
								<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; width:140px"> 
									<p align="left"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Alert:</strong> Confidential.</p>
								</div>
							</div>
							</td>
						</tr>
						 <tr>
						 <td class="tdbor" colspan="4">&nbsp;</td>
					     </tr>
		<?
		}
		?>
		
		
					  </table><!-- end of metadata -->
				</td></tr>
			</table> <!-- end of datawrap -->
	</td></tr>
			<tr><td><br /><br /></td></tr>

		<!-- Begin Manage Files -->
		<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
		<tr>
			<td>
				<table class="datawrap" width="605px" >
					<tr>
						<td>
							<table width="600px" class="metadata">
								<tr class="metabutton">
									<td width="10px"></td>
									<td class="containerlabel">
										<img src="images/menu_strip_down_arrow.gif" id="outgoingfilelinkimg" />
										<a onClick="javascript:toggleWindowAjax2('filebodyoutgoing','outgoingfilelinkimg'); showManageFiles('<? echo $t_strNewId; ?>','filebodyoutgoing','addOut','<? echo $addedByOfficeId; ?>');">Manage Files</a>
										<div id="loading_widget" style="float:right; display:none;"><!-- LOADING&nbsp;<img src="css/images/ajax-loader.gif" /> --></div>
									</td>
									<td><div id="addOut" style="text-align: right;"></div></td>
								</tr>
								<tr>
									<td width="590px" colspan="3">
										<div id="filebodyoutgoing"><!-- ############################# File Content #####################################--></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<style type="text/css"> #addOut Input[type="submit"] { display: initial !important;} </style>
		<?php endif; ?> <!-- endif action = 54 -->
		<!-- End Manage Files -->
		
		<?php if($rscRelated>0): ?>
		<!-- begin related documents -->
		<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
		<style type="text/css"> .scrollable { height: 128px !important; }</style>
		<tr><td>
		
			<table class="datawrap" width="605px" >
				<tr>
					<td>
						<table width="600px" class="metadata">
							<tr class="metabutton">
								<td width="10px"></td>
								<td class="containerlabel">
									<img src="images/menu_strip_down_arrow.gif" id="incomingfilelinkimgreldocs" />
									<a onClick="javascript:toggleWindowAjax002('filebodydoc','incomingfilelinkimgreldocs');"> Related Document Files</a> <span style="font-size: 10px;background-color: rgb(251, 9, 21);padding: 3px;">New</span>
									<!-- <div id="loading_widget2" style="float:right; display:none;">LOADING&nbsp;<img src="css/images/ajax-loader.gif" /></div> -->
								</td>
								<td>
									<div id="addIncreldocs" style="text-align: right;">&nbsp;</div>
								</td>
							</tr>
							<?php for($cntRelated=0;$cntRelated<$rscRelated;$cntRelated++){ ?>
							<tr>
								<td width="590px" colspan="3"><b>Document ID:</b> <i><?=$arrRelated[$cntRelated]["documentId"]?></i>
									<div class="filebodydoc" id="filebodyincomingreldocs<?=$cntRelated?>"><!-- #### File Content ####--></div>
									

								</td>
							</tr>
							<script type="text/javascript">
								showManageFilesReldocs('<?=$arrRelated[$cntRelated]["documentId"]?>','filebodyincomingreldocs<?=$cntRelated?>','addIncreldocs','<? echo $addedByOfficeId; ?>');
							</script>
							<?php } ?>
		    			</table>
		    		</td>
		    	</tr>
			</table>

		</td></tr>
		<?php endif; ?> <!-- endif action = 54 -->
		<!-- end related documents -->
		<?php endif; ?>
		
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr><td>
					<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel"><img src="images/menu-bar-right-arrow.gif" id="outgoinghistorylinkimg" /><a href="javascript:getData('showAction.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=docubodyoutgoing&historydiv=actionbodyoutgoing','docubodyoutgoing');" onClick="return toggleWindowAjax('docubodyoutgoing','outgoinghistorylinkimg');" id="outgoinghistorylink">Update Action</a></td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
<!-- 	***************************		history				********************************************	-->
			<div id="docubodyoutgoing" align="center">
			</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<?php endif; ?> <!-- endif action = 54 -->
			</td>
		</tr>
		<tr>
			<td>
			<table class="datawrap" width="605px" >
				<tr>
					<td>
					<?php if($_GET['actioncode'] != 54): ?> <!-- begin action = 54 -->
					<table width="600px" class="metadata">
						<tr class="metabutton">
							<td width="10px"></td>
							<td class="containerlabel">
								<img src="images/menu-bar-right-arrow.gif" id="outgoingactionlinkimg" /><a href="javascript:getData('showHistory.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=actionbodyoutgoing','actionbodyoutgoing');" onClick="return toggleWindowAjax('actionbodyoutgoing','outgoingactionlinkimg');" id="outgoingactionlink">View History</a>
							</td>
						</tr>
						<tr>
							<td></td>
							<td width="590px">
								<div id="actionbodyoutgoing" align="center"><div id="bodyoutgoing"></div>
								</div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<?php endif; ?> <!-- endif action = 54 -->
			</td>
		</tr>
</table>
	
	
<? }else { //mode = new or edit ?>
 <script language="javascript" type="text/javascript">
	  var arOriginOutgoing = new Array(<? echo sizeof($arOffices);?>);
	  <?
	  for($i=0;$i<sizeof($arOffices);$i++)
	  echo "arOriginOutgoing[".$i."]=new Array(".$intColumnNum.");\n";

	  if ($t_intOfficeId=="") $t_intOfficeId=$arOffices[0]['officeCode']; //sets first office code for RMS account

  	  for($i=0;$i<sizeof($arOffices);$i++)
	  {
	  	echo "arOriginOutgoing[".$i."][0]='".$arOffices[$i]['officeCode']."';\n";
	  	echo "arOriginOutgoing[".$i."][1]='".$arOffices[$i]['officeName']."';\n";
	  	echo "arOriginOutgoing[".$i."][2]='".$arOffices[$i]['empNumber']."';\n";
		echo "arOriginOutgoing[".$i."][3]='".$arOffices[$i]['head']."';\n";
		echo "arOriginOutgoing[".$i."][4]='".$arOffices[$i]['title']."';\n";
		echo "arOriginOutgoing[".$i."][5]='".$arOffices[$i]['officeTag']."';\n";
		if($signatoryOutgoing=="" && $mode=="new") // sets signatory of the office for new docs
		{
		  if($arOffices[$i]['officeCode']== $t_intOfficeId) $signatoryOutgoing= $arOffices[$i]['head'];
		}
	  }

	  ?>
		function getSignatory(i)
		{
		for(x=0;x<arOriginOutgoing.length;x++)
		{
		if(i==arOriginOutgoing[x][0]) 
			{
				document.getElementById("signatoryOutgoing").value=arOriginOutgoing[x][3];
				//document.getElementById("cmbOriginOutgoing").title=arOriginOutgoing[x][1];;	
			}
		}
		getData("showOutgoing.php?mode=getGroup&officeCode="+i+"&officeGroup=<? echo $t_intGroupCode;?>","outgGroupDiv");
		}
		function x () {
					var oTextbox = new AutoSuggestControl(document.forms['frmOutgoing'].t_strDocType, new RemoteStateSuggestions());        
            } 
			//x();
			$("#cmbOriginOutgoing").change();
			$("#documentOwnerOutgoing").change(function(){
			var src = $(this).val();
			$("#idcontaineroutgoing").load("showOutgoing.php?getOfficeID="+src+ " #idcontaineroutgoing");
			});
	  </script>
      <form action="javascript:check(document.getElementById('frmOutgoing'),'<?=$hashId; ?>','showOutgoing.php');" name="frmOutgoing" id="frmOutgoing" onsubmit="return checkConfi(document.getElementById('frmOutgoing'));">
<table align="center" width="600" class="tblforms">
    <tr> 
       	<th width="97">Document ID </td>
    	<td width="175"><div id="idcontaineroutgoing"><input type="text" class="caption" value="<? echo $t_strNewId;?>" name="t_strDocId" id="t_strDocId"  alt="required" <? echo $disable_ID;?> style="width:150px"></div>
	<input type="hidden" name="mode" value="<? echo $nextMode;?>"></td>
    	<th width="100"  class="head">Document Date</td>
   	  <td width="228"><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="documentDateOutgoing" name="documentDateOutgoing" value="<? echo $documentDateOutgoing;?>"></td>
    </tr>
    
	<tr> 
    	<th>Document Type</td>
      	<td><table>
		<tr><td>
        <div id="divDocType">
        <select name="cmbDocType" id="t_strDocType" class="caption" style="width:250px">
		<option value="-1"> </option>
	  <?
	  for($i=0;$i<sizeof($arDocType);$i++)
	  {    
	  ?>
	  <option value="<? echo $arDocType[$i]['documentTypeId']; ?>" <? if($arDocType[$i]['documentTypeId']== $t_intDocTypeId) echo "selected"; ?> > <? echo $arDocType[$i]['documentTypeDesc'];?></option>
	  <?
	  }
	  ?>
        </select>
		<!--input type="text" name="t_strDocType" id="t_strDocType" value="<? echo $t_strDocType;?>"autocomplete=OFF  /-->
        </div>
        </td> 
		<td>
		<? if($objOutgoing->get('userType')==1) {?>
		<span class='ui-icon ui-icon-plus' title='Add New Document Type' onclick="displayDialog('showLibrary_popup.php?mode=doctype');"></span>
		<? }?>
		</td></tr></table>
		</td>   
		<?php if(!$objOutgoing->get("blnAgencyUser")){ ?>
        <th>Deadline</td>
    	<td><input type="text" class="<?php //w12em dateformat-Y-ds-m-ds-d ?>" id="deadlineOutgoing" name="deadlineOutgoing" value="<? echo $deadlineOutgoing;?>"> </td>      
		<?php } ?>
    </tr>
	
    <tr> 
      	<th valign="top">Reference ID</td>
      	<td><input type="text" name="t_strReferenceId" id="t_strReferenceId" value="<? echo $t_strReferenceId;?>">
      		<br><span class="required" style="text-transform:none">&nbsp;To add more than one Reference Id, use comma to separate<br>(i.e. SMPL-17-00000, SMPL-17-00001, SMPL-17-00002)</span></td>
      	<td><!--DWLayoutEmptyCell-->&nbsp;</td>
      	<td><!--DWLayoutEmptyCell-->&nbsp; </td>
      
    </tr>
	
	<tr>
	  <th>Doc No.</td>
	  <td colspan="3"><br /><input type="text" name="t_strDocNum" id="t_strDocNum" value="<? echo $DocNum;?>" /><span class="required" style="text-transform:none">&nbsp;(i.e. AO 001)</span></td>
  </tr>
    <tr> 
      	<th>Subject</td>
	    <td colspan="3"><div id="subjBlock"><textarea cols="60" name="t_strSubject" id="t_strSubject" rows="3" title="required"><? echo $t_strSubject; ?></textarea></div></td>


    </tr>
	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><br /><br /></td>  
	</tr>
<tr> 
    	<th>Origin:</td>
      	<td>
        <?php if($objOutgoing->get("blnAgencyUser")){ ?> 
        <label><?php echo $objOutgoing->getOfficeCode2($objOutgoing->get('userID')); ?></label>
        <input type="hidden" name="cmbOriginOutgoing" value="<?php echo $objOutgoing->get('agencyCode'); ?>"  />
        <?php } else { ?>

		<select name="cmbOriginOutgoing" id="cmbOriginOutgoing" class="caption" onchange="getSignatory(this.value);" style="width:172px" title="" <? if($objOutgoing->get("userType")==2 && $objOutgoing->get("blnAgencyUser")) echo "disabled";?>>
	  <?php for($i=0;$i<sizeof($arOffices);$i++) { ?>
	  <option value="<? echo $arOffices[$i]['officeCode']; ?>" <? if($arOffices[$i]['officeCode']== $t_intOfficeId) echo "selected"; ?> title="<? echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);?>"> <? 
	  	if ($objOutgoing->get("userType")==2 && $objOutgoing->get("blnAgencyUser")) echo str_replace("&nbsp;","",$arOffices[$i]['officeName']);
		else echo $arOffices[$i]['officeName'];?></option>
	  <?php } ?>
        </select>

        <?php } ?>
        </td>  
        <?php if(!$objOutgoing->get("blnAgencyUser")){ ?> 
		<th>Signatory</td>
    	<td><div id="signatoryBlock"><input type="text" id="signatoryOutgoing" name="signatoryOutgoing" value="<? echo $signatoryOutgoing;?>">        </div></td>      
        <?php } ?>
    </tr>	
    <?php if(!$objOutgoing->get("blnAgencyUser")){ ?>
	<tr>
		<th>Group</th>
		<td colspan="3">
			<div id="outgGroupDiv"><select id="cmbGroupCodeOutgoing" name="cmbGroupCodeOutgoing"></select></div>
		</td>
	</tr>
    <?php }  ?>
	<tr> 
   		<th rowspan="3" valign="top">Remarks</td>
		<td rowspan="3" valign="top"><textarea cols="30" class="caption" name="t_strRemarks" id="t_strRemarks" rows="3"><? echo $t_strRemarks;?></textarea></td>
		<td></td><td></td>
   	</tr>
   	<tr>
        <th valign="top"></td>
		<td valign="top"></td>
   	</tr>

   	<tr>
   		<td colspan="4">&nbsp;</td>
   	</tr>
   	<!-- Begin Controlled Document. -->
	   	<tr>
	   		<td colspan=2 valign="top"><br>
	   			<b>Controlled Document: </b>
	   			<?php $ctrlDocs = $file->getControlledDocDesc(); ?>
		      	<select name="t_contdoc" class="caption" id="sel_contdoc_outg">
				  	<option value="0"></option>
				  	<?php foreach($ctrlDocs as $contdoc): ?>
				  	<option value="<?=$contdoc['id']?>" <?=($t_contdoc==$contdoc['id']) ? 'selected' : ''?>><?=$contdoc['description']?></option>
				  	<?php endforeach; ?>
				</select>
   	  		</td>
   	  		<td colspan=2>
   	  			<div id="conta_outg" <?=($t_contdoc==1)? '' : 'style="display: none;"'?> >
   	  				<b>Copy No.</b>&nbsp;&nbsp;<br>
		      			<input type="text" name="txt_conta_copyno" value="<?=$t_cd_copyno?>" style="width: 180px !important;height: 12px !important;">
		      	</div>
		      	<div id="contb_outg" <?=($t_contdoc==1 || $t_contdoc==2 || $t_contdoc==4 || $t_contdoc==5)? '' : 'style="display: none;"'?> >
		      		<b>Copy Holder/s</b>&nbsp;&nbsp;
		      		<input type="text" name="txt_contb_copyholder" value="<?=$t_cd_copyholder?>" style="width: 180px !important;height: 12px !important;">
		      	</div>
		      	<div id="contc_outg" <?=($t_contdoc==2)? '' : 'style="display: none;"' ?> >
		      		<b>Manner of Disposal</b>
		      		<input type="text" name="txt_contc_mannerdisposal" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important;height: 12px !important;">
		      		<?php if($_SESSION['office']=='ITD'): ?>
			      		<br>
			      		<b>Doc Code</b><br>
			      		<input type="text" name="txt_contc_docCode" value="<?=$t_cd_mannerdisposal?>" style="width: 180px !important;height: 12px !important;">
			      	<?php endif; ?>
		      	</div>
		      	<div id="contd_outg" <?=($t_contdoc==3)? '' : 'style="display: none;"'?> >
		      		<b>Revision no.</b>&nbsp;&nbsp;
		      		<input type="text" name="txt_contd_revisionno" value="<?=$t_cd_revisionno?>" style="width: 180px !important;height: 12px !important;">
		      	</div>
		      	<div id="conte_outg" <?=($t_contdoc==3 || $t_contdoc==1)? '' : 'style="display: none;"' ?> >
		      		<b>Person/Unit Responsible</b>
		      		<input type="text" name="txt_conte_unitres" value="<?=$t_cd_personres?>" style="width: 180px !important;height: 12px !important;">
		      	</div>
		      	<div id="contf_outg" <?=($t_contdoc==6)? '' : 'style="display: none;"' ?> >
		      		<b>Issue/ Withrawal Control #</b>	
		      		<input type="text" name="txt_withrawal" value="<?=$t_cd_withrawalno?>" style="width: 180px !important">
		      	</div>
		    </td>
	  </tr>
  	<!-- End Controlled Document -->

  	<tr>
   		<td colspan="4">&nbsp;</td>
   	</tr>
   	<!-- Begin Cabinet. Hide when Condition is true -->
	<tr>
		<th class="tdpads">Cabinet:</th>
		<td class="tdbor" colspan="4">
			<select name="t_intContainer" id="selcabinet_outg" class="caption">
				<option value="-1"> </option>
				<?php for($i=0;$i<sizeof($arContainer);$i++){ ?>
				<option value="<? echo $arContainer[$i]['containerId']; ?>" <? if($arContainer[$i]['containerId']== $t_intContainer) echo "selected"; ?> > <? echo $arContainer[$i]['label'];?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th class="tdpads">Drawer:</th>
		<td class="tdbor" colspan="4">
			<select name="t_intContainer2" id="seldrawer_outg" class="caption">
				<option value="-1"> </option>
				<?php for($i=0;$i<sizeof($arContainer2);$i++) { ?>
				<option value="<? echo $arContainer2[$i]['container2Id']; ?>" <? if($arContainer2[$i]['container2Id']== $t_intContainer2) echo "selected"; ?> > <? echo $arContainer2[$i]['label'];?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th class="tdpads">Container:</th>
		<td class="tdbor" colspan="4">
				<select name="t_intContainer3" id="selcontainer_outg" class="caption">
					<option value="-1"> </option>
					<?php for($i=0;$i<sizeof($arContainer3);$i++) { ?>
					<option value="<? echo $arContainer3[$i]['container3Id']; ?>" <? if($arContainer3[$i]['container3Id']== $t_intContainer3) echo "selected"; ?> > <? echo $arContainer3[$i]['label'];?></option>
					<?php } ?>
				</select>
			</td>
	</tr>

	<tr>
	<td colspan="4" class="tdbor">&nbsp;</td>
	</tr>
   	<!-- End Cabinet -->
    <?php if(!$objOutgoing->get("blnAgencyUser")){ ?>
   	<tr> 
		<th valign="middle">Confidential</td>
		<td valign="top"><input type="checkbox" value="1" name="t_intConfidential" id="t_intConfidential" <? 
	  if ($t_intConfidential=='1') echo "checked=\"checked\"";
	  ?>></td>
	 	<td>&nbsp;</td>
	 	<td>&nbsp;</td>
   	</tr>
    <tr> <!--$arManagedOffice=$objIncoming->getManagedOffice();-->
      <th valign="top">Document Owner </td>
      <td valign="top"><select id="documentOwnerOutgoing" name="documentOwner" <? echo $disable_ID;?>>
	   <?
	  for($i=0;$i<sizeof($arManagedOffice);$i++)
	  {
	  $managedOffice=(trim($arManagedOffice[$i]["groupCode"])=="")?$arManagedOffice[$i]["officeCode"]:$arManagedOffice[$i]["groupCode"];
	  ?>
	  <option value="<? echo $managedOffice; ?>" <? if($managedOffice== $t_strManagedOffice) echo "selected"; ?> > <? echo $managedOffice;?></option>
	  <?
	  }
	  ?>
	  </select></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	
    <?php } ?>
	<tr>
		<td colspan="4" style="text-align:center">
			<? if ($arrFields['mode'] == "new"){?>
				<input type="submit" value="Save" class="btn" onclick="">
			<? }else{ ?>
			  	<input type="submit" value="Update" class="btn" onclick="">
			<? }  ?>
  			<input type="reset" value="Clear" class="btn"></td>
	</tr>

  </table>
</form>			

<?
} ?>
<a class='barcode'></a>
<script type="text/javascript">
$(function() {
 $("a.barcode").fancybox({
		 'frameWidth' : 620, 
		 'frameHeight': 450, 
		 'overlayShow': true, 
		 'overlayOpacity': 0.7,
		 'hideOnContentClick': false
 });
 	$('#documentDateOutgoing, #deadlineOutgoing,#documentDateClone').datepicker({
		dateFormat: "yy-mm-dd",
		showOn: "button",
        buttonImage: "images/calendar/icon_minicalendar.gif",
        buttonImageOnly: true		
	});

});

	function editMinDocDetails(docID,docNO,hashID){  
	  if($('#btnEditDocNo').val() == 'Save'){ 
		$.ajax({ 
		  url: "showOutgoing.php",
		  type: "POST",
		  data: "mode=update&blnMinDocInfo=true&docID="+docID+"&docNO="+$('#intDocNum').val()+'&docDate='+$('#documentDateClone').val()+'&signatory='+$('#signatoryOutgoing').val()+'&docSubj='+$('#t_strSubject').val(),
		  success: function(data){ 
		   getData('showOutgoing.php?mode=view&id='+docID,'doclist');
		  }
		});		  
		return false;
	  }

	  var subjOrigVal = $('#docSubjViewMode').text(); //get val before load manipulation
	  var signOrigVal = $('#docSignatoryViewMode').text();
  	  $("#docSubjViewMode").load("showOutgoing.php?mode=new #subjBlock",function(){
	  $('#btnEditDocNo').val("Save");
		var newDocNoInput = "<input type='text' name='intDocNum' id='intDocNum' value='"+docNO+"' />";
		//var newDocDateInput = "<input type='text' id='documentDate' name='documentDate' value='"+$('#docDateViewMode').text()+"' />";
		$('#docNoViewMode').html(newDocNoInput);
		$('#docDateViewMode span').hide();
		$('#documentDateClone,.ui-datepicker-trigger').css('display','inline-block');
		$('#t_strSubject').val(subjOrigVal);
		$("#docSignatoryViewMode").load("showOutgoing.php #signatoryBlock",function(){
			$('#signatoryOutgoing').val(signOrigVal);
		});
	  });
	  
	}

function printBarcode(barcode,group){
 $.get('showbarcode.php',{ code: barcode,group: group }, function(data) {
//   $("a.barcode").click();
    // var thePopup = window.open( $("a.barcode").attr('href'), "barcode", "menubar=0,location=0,height=700,width=700" );
    var thePopup = window.open( 'showbarcode.php?code=<? echo $t_strNewId;?>', "barcode", "menubar=0,location=0,height=700,width=700" );
    //$('#popup-content').clone().appendTo( thePopup.document.body );
    //thePopup.print();
 });
}
</script>

  <script type="text/javascript">
  	$(document).ready(function(){
  		showManageFiles('<? echo $t_strNewId; ?>','filebodyoutgoing','addOut','<? echo $addedByOfficeId; ?>');
  		$('#bodyoutgoing').load('showHistory.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=actionbodyoutgoing');

  		$('#docubodyoutgoing').load('showAction.php?mode=outgoing&docID=<? echo $t_strNewId;?>&div=docubodyoutgoing&historydiv=actionbodyoutgoing');
  		$("#selcabinet_outg").change(function(){
  			var id=$(this).val();
  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+id+"&drawerid=",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_outg").html(html);
  				} 
  			});
  		});

  		// begin add in document ready
  		
  		//GET DRAWER
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+$("#selcabinet_outg").val()+"&drawerid=<?=$t_intContainer2?>",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_outg").html(html);
  				} 
  			});

  		//GET CONTAINER
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid=<?=$t_intContainer2?>&contid=<?=$t_intContainer3?>",
  				cache: false,
  				success: function(html){
  					$("#selcontainer_outg").html(html);
  				} 
  			});


  		$("#seldrawer_outg").change(function(){
  			var id=$(this).val();

  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid="+id,
  				cache: false,
  				success: function(html){
  					$("#selcontainer_outg").html(html);
  				} 
  			});
  		});

  		// begin Edit mode

  		//GET DRAWER
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+$("#selcabinet_outg_edit_mode").val()+"&drawerid=<?=$t_intContainer2?>",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_outg_edit_mode").html(html);
  				} 
  			});

  		//GET CONTAINER
  		$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid=<?=$t_intContainer2?>&contid=<?=$t_intContainer3?>",
  				cache: false,
  				success: function(html){
  					$("#selcontainer_outg_edit_mode").html(html);
  				} 
  			});

  		$("#selcabinet_outg_edit_mode").change(function(){
  			var id=$(this).val();
  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=drawer&cabinetid="+id+"&drawerid=",
  				cache: false,
  				success: function(html){
  					$("#seldrawer_outg_edit_mode").html(html);
  				} 
  			});
  		});
  		$("#seldrawer_outg_edit_mode").change(function(){
  			var id=$(this).val();
  			$.ajax ({
  				type: "GET",
  				url: "ajaxContainer.php?",
  				data: "getFile=container&cabinetid=&drawerid="+id,
  				cache: false,
  				success: function(html){
  					$("#selcontainer_outg_edit_mode").html(html);
  				} 
  			});
  		});
  		// End Edit mode

  		$('#btnLocation').click(function(){
  			$.ajax ({
  				type: "GET",
  				url: "showData.php?",
  				data: "mode=updateLocation_outg&docid=<?=$t_strNewId?>&cabinet="+$('#selcabinet_editmode').val()+"&drawer="+$('#seldrawer_editmode').val()+"&container="+$('#selcontainer_editmode').val(),
  				cache: false,
  				success: function(data){
  					alert('Folder location has been updated.');
  					$('#locationtd').html(data);
  					console.log(data);
  				} 
  			});
  		});

  		// Begin editable cabinet
  		$('#tblupdateLocation_outg').click(function(){
  			conf = confirm('Are you sure you want to update the location of this file?');
  			if(conf){
  				cab = $('#selcabinet_outg_edit_mode').val();
	  			draw = $('#seldrawer_outg_edit_mode').val();
	  			cont = $('#selcontainer_outg_edit_mode').val();

	  			$.ajax ({
	  				type: "GET",
	  				url: "showData.php?",
	  				data: "mode=updateLocation_outg&docid=<?=$t_strNewId?>&cabinet="+cab+"&drawer="+draw+"&container="+cont,
	  				cache: false,
	  				success: function(data){
	  					alert('Location has been updated');
	  				} 
	  			});
  			}
  		});
  		// End editable cabinet

  		// begin editable controlled document
  		$('#tblControlleddoc_outg').click(function(){
  			conf = confirm("Are you sure you want to update the controlled document field of this document?");
  			if(conf){
	  			controlleddoc = $('#sel_contdoc_outg').val();
	  			copyno = $('#txt_conta_copyno').val();
				copyholder = $('#txt_contb_copyholder').val();
				mannerdisposal = $('#txt_contc_mannerdisposal').val();
				revisionno = $('#txt_contd_revisionno').val();
				unitres = $('#txt_conte_unitres').val();
				withrawal = $('#txt_withrawal').val();

				$.ajax ({
	  				type: "GET",
	  				url: "showData.php?",
	  				data: "mode=updateControlledDoc_inc&docid=<?=$t_strNewId?>&controlleddoc="+controlleddoc+"&copyno="+copyno+"&copyholder="+copyholder+"&mannerdisposal="+mannerdisposal+"&revisionno="+revisionno+"&unitres="+unitres+"&withrawal="+withrawal,
	  				cache: false,
	  				success: function(data){
	  					alert('Controlled Document has been updated.');
	  				} 
	  			});
	  		}
			
  		});

  		/*begin select Controlled docments*/
  		$('#sel_contdoc_outg').change(function() {
  			contdoc = $(this).val();
  			if(contdoc==1){
  				$('#conta_outg').show();
		  		$('#contb_outg').show();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').hide();
		  		$('#conte_outg').show();
		  		$('#contf_outg').hide();
  			}else if(contdoc==2){
  				$('#conta_outg').hide();
		  		$('#contb_outg').show();
		  		$('#contc_outg').show();
		  		$('#contd_outg').hide();
		  		$('#conte_outg').hide();
		  		$('#contf_outg').hide();
		  	}else if(contdoc==3){
  				$('#conta_outg').hide();
		  		$('#contb_outg').hide();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').show();
		  		$('#conte_outg').show();
		  		$('#contf_outg').hide();
		  	}else if(contdoc==4){
  				$('#conta_outg').hide();
		  		$('#contb_outg').show();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').hide();
		  		$('#conte_outg').hide();
		  		$('#contf_outg').hide();
		  	}else if(contdoc==5){
  				$('#conta_outg').hide();
		  		$('#contb_outg').show();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').hide();
		  		$('#conte_outg').hide();
		  		$('#contf_outg').hide();
		  	}else if(contdoc==6){
  				$('#conta_outg').hide();
		  		$('#contb_outg').hide();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').show();
		  		$('#conte_outg').hide();
		  		$('#contf_outg').show();
  			}else{
  				$('#conta_outg').hide();
		  		$('#contb_outg').hide();
		  		$('#contc_outg').hide();
		  		$('#contd_outg').hide();
		  		$('#conte_outg').hide();
		  		$('#contf_outg').hide();
  			}
  		});
  		/*end select Controlled docments*/
  	});
  </script>