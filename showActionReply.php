<? @session_start();
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
?>
<script language="JavaScript">
	$(document).ready(function(e) {

        $('#popupContactClose').click(function(){
				
				$('#popupContact').css('display','none');
				$('#backgroundPopup').css('display','none');
			});

    });
	
	/*
	function validate(){
	
		if(document.getElementById("cmbActionTaken").options[document.getElementById("cmbActionTaken").selectedIndex].value == "-1")
		{
			alert("Please select Action Taken!");
			document.getElementById("cmbActionTaken").focus();
			return false
		}else if(document.getElementById("cmbActionRequired").options[document.getElementById("cmbActionRequired").selectedIndex].value == "-1")
		{
			alert("Please select Action Required!");
			document.getElementById("cmbActionRequired").focus();
			return false
		}
		
	}*/
	//alert('submitted');
	//$('#popupContactClose').trigger('click');		
</script>


<?

if($t_submit==1)
{
?>

<?	
	// process form values
	$arrFields2 = $objDocDetail->getUserFields();
	$docID = $arrFields2['txtDocumentID'];
	
	$divId = $arrFields2['divid'];
	//$arrFields = $objDocDetail->getDocDetails($docID);
	$mode = $arrFields2['mode']; //  incoming ; outgoing ; intra
	$historydiv = $arrFields2['historydiv'];
	// addSource = 1 = action reply is added by recipient, 2 = action reply is added by sender also
	//echo $arrFields2['actionUnitSwitch']."/".$arrFields2['txtActionUnit'];	
	$res = $objDocDetail->addAction($arrFields2);
	//echo "=>".count($res);
	if($res!=0)
	{
		//echo '<br>';
		$msg = $objDocDetail->getValue('msg');
		
		// Display submitted data
		$rsActionTakenSubmitted = $objDocDetail->getActionTakenDetails($arrFields2['cmbActionTaken']);
		$rsActionRequiredSubmitted = $objDocDetail->getActionRequiredDetails($arrFields2['cmbActionRequired']);
			
			if($arrFields2['actionUnitSwitch']==2) // if office
			{
				list($strRecipientUnit, $strRecipientId) = explode(":", $arrFields2['cmbOfficeCode']);
				$recipientUnitLabel = 'Office';
		
			}elseif($arrFields2['actionUnitSwitch']==1) // if employee
			{
				$strRecipientUnit="employee";
				$strRecipientId = $arrFields2['cmbEmpNumber'];
				$recipientUnitLabel = 'Employee';
			}
			
			$recipientName = $objDocDetail->displayRecipientSenderNames($strRecipientId, $strRecipientUnit);
			
		
		//$actionRecipient = $objDocDetail->displayRecipientSenderNames($arrFields2[$ctr]['recipientId'], $arrFields2[$ctr]['recipientUnit']);
/*
		if($arrFields2['cbRestricted']==1) 
		$restricted = 'Yes';
			else
		$restricted = 'No'; 
*/
			 			if($msg<>""){?>
						<div class="pane">
						<div class="ui-widget" style="width:75%">
							<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
								<p align="left"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
								<strong>Notice: </strong><? echo $msg;?></p>
							</div>
						</div>
						</div>
						<script type="text/javascript">
						setTimeout("disappear()",2000);
						</script>
				<? } 
		
		echo '<table width="100%"  border="0">
		  <tr>
			<td width="30%">Action Taken </td>
			<td width="70%">'.$rsActionTakenSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Required </td>
			<td>'.$rsActionRequiredSubmitted[0]['actionDesc'].'</td>
		  </tr>
		  <tr>
			<td>Action Unit </td>
			<td></td>
		  </tr>
		  <tr>
			<td>'.$recipientUnitLabel.' </td>
			<td>'.$recipientName.'</td>
		  </tr>
		  <tr>
			<td>Remarks</td>
			<td>'.$arrFields2['txtRemarks'].'</td>
		  </tr>
		  <tr>
			<td>Restricted</td>
			<td>'.$restricted.'</td>
		  </tr>
		</table>';
		/*
		echo "<script language=\"JavaScript\">
		getData('showAction.php?mode=$mode&docID=$docID&div=$divId&historydiv=$historydiv','$divId');
		if(document.getElementById('$historydiv').style.visibility!=\"hidden\")
		getData('showHistory.php?mode=$mode&docID=$docID&div=$historydiv','$historydiv');
		</script>";
		*/
		$objDocDetail->sendEmail($res); //send email to sender (Hack111709)
	}

	echo '<br>';
	

	/*
echo 'submitted'; 
echo "<script language=\"JavaScript\">$('#popupContactClose').trigger('click');		
</script>";*/

}

if($_GET['mode']=='actionReply')
{
		$rsAction = $objDocDetail->selectAction($_GET['ID']);
		//echo $rsAction[0]['senderId'], $rsAction[0]['senderUnit'].$_GET['mode'];
		$strRecipientName = $objDocDetail->displayRecipientSenderNames($rsAction[0]['recipientId'], $rsAction[0]['recipientUnit']);
		
?>
				<form action="javascript:get(document.getElementById('frmActionReply'),'ActionReplyDialog','showActionReply.php');" id="frmActionReply" name="frmActionReply" onSubmit="return validatePopup('frmActionReply')">
					<table width="98%"  border="0" align="center">
						  <tr>
							<td>Action Taken </td>
							<td><?
								$rsActionTaken = $objDocDetail->getActionTaken();
							?>
							
							<input type="hidden" name="txtReferenceID" value="<? echo $_GET['ID'];?>">
							<input type="hidden" name="mode" value="<? echo $_GET['MODE'];?>">
							<input type="hidden" name="divid" value="<? echo $_GET['DIVID'];?>">
							<input type="hidden" name="historyid" value="<? echo $_GET['HISTORYID'];?>">
							<input type="hidden" name="historydiv" value="<? echo $_GET['HISTORYDIV'];?>" />
							<input type="hidden" name="addSource" value="<? echo $_GET['ADDSOURCE'];?>" />
							<select name="cmbActionTaken" id="cmbActionTaken">
							<option value="-1">&nbsp;</option>
							<?php
								for($i=0;$i<sizeof($rsActionTaken);$i++) 
								{ 		
									echo "<OPTION value='".$rsActionTaken[$i]['actionCodeId']."'>".$rsActionTaken[$i]["actionDesc"]."</OPTION>\n"; 
								}
							?>		  
							</select></td>
						  </tr>
						  <tr>
							<td width="16%">Action Required </td>
							<td width="84%">
							<?
								$rsActionRequired = $objDocDetail->getActionRequired();
							?>
							<select name="cmbActionRequired" id="cmbActionRequired">
							<option value="-1">&nbsp;</option>
							<?php
								for($i=0;$i<sizeof($rsActionRequired);$i++) 
								{ 		
									echo "<OPTION value='".$rsActionRequired[$i]['actionCodeId']."'>".$rsActionRequired[$i]["actionDesc"]."</OPTION>\n"; 
								}
							?>		  
							</select>
							</td>
						  </tr>
						  <tr>
							<td colspan="2">Action Unit</td>
						  </tr>
						  <tr>
							<td colspan="2">
				

				<table width="100%"  border="0">
				  <tr>
				<? if($rsAction[0]['senderUnit']=='employee') 
			  	{
				
				  
				  echo '<input type="hidden" name="actionUnitSwitch" value="1">
				   <input type="hidden" name="cmbEmpNumber" value="'.$rsAction[0]['senderId'].'">
				  <td width="21%" align="left">
					  Employee Name: </td>
					<td width="79%">'.$strRecipientName.'</td>';
					?>

					<input type="hidden" name="senderUnit" value="<? echo 'employee';?>" />
					<input type="hidden" name="sender" value="<? echo $rsAction[0]['recipientId'];?>" />
					
					<?
				}else
				{				 
 				  echo '<input type="hidden" name="actionUnitSwitch" value="2">
				   <input type="hidden" name="cmbOfficeCode" value="'.$rsAction[0]['senderUnit'].':'.$rsAction[0]['senderId'].'">
				  <td width="21%" align="left">
					  Office: </td>
					<td width="79%">'.$strRecipientName.'</td>';
					?>

					<input type="hidden" name="senderUnit" value="<? echo $rsAction[0]['recipientUnit'];?>" />
					<input type="hidden" name="sender" value="<? echo $rsAction[0]['recipientId'];?>" />
					
					<?
				 
				}
				?>	
				  </tr>
				</table>
							</td>
						  </tr>
						  <tr>
							<td width="16%">Remarks</td>
							<td width="84%"><input name="txtRemarks" type="text" size="40"><input type="hidden" name="txtDocumentID" value="<? echo $_GET['DOCID'];?>"><input name="cbRestricted" value="1" type="hidden">
							</td>
						  </tr>
						   <tr>
							<td width="16%">Reply expected</td>
							<td width="84%"><input type="checkbox" id="cbReply" name="cbReply" value="0">
							</td>
						  </tr>
						  <!--
						    <tr>
							<td width="16%">Restricted</td>
							<td width="84%"><input type="checkbox" name="cbRestricted" value="1" <?php // if($rsAction[0]['restricted']==1) echo "checked"; ?>>
							</td>
						  </tr>
						  -->
							<tr align="center">
							<td colspan="2" align="center"><input type="submit" class="btn" name="btnAdd" value="Submit"></td>
							</tr>
						</table>
			</form>			
						
<?
	//echo $_GET['ID'];
	
}
//print_r($_GET);
?>