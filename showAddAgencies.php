<? 
include("class/Agencies.class.php");
$objAgencies = new  Agencies;
$strButtonText="Add";

	echo "<div id='loader'></div>";

if($mode=="add"){
	$objAgencies->addAgencies($_POST);
	exit();
	}
elseif($mode=="delete"){
	$arrOfficeDetails=$objAgencies->deleteAgencies($_GET["id"]);
	$officeName="";
	$contact="";
	//$email="";
	$id="";
	$mode="add";
}
elseif($mode=="edit"){
	$arrOfficeDetails=$objAgencies->getAgencies($_GET["id"]);
	$officeName=$arrOfficeDetails[0]["officeName"];
	$contact=$arrOfficeDetails[0]["contactPerson"];
	//$email=$arrOfficeDetails[0]["email"];
	$mode="update";
	$strButtonText="Update";
}
elseif($mode=="update"){
	$arrOfficeDetails=$objAgencies->updateAgencies($_POST);
	$officeCode="";
	$officeName="";
	$contact="";
	//$email="";
	$id="";
	$mode="add";
	exit();
}
elseif($mode=="") $mode="add";
$arrAgencies = $objAgencies->getAgencies();
//print_r($arrAgencies);
?>
<script type="text/javascript">
$(".deletelink").click(function(e) {
	var href=$(this).attr("href");
	x=confirm("Are you sure you want to delete?");
	if(x){
		$.facebox({ajax:href});
		//$("#loader").load(href,function(data){
		//	$.facebox({ajax:"showAddAgencies.php"});
		//});
	}
    return false;
});

$("#frmAgencies").submit(function(e) {
	e.preventDefault();
    $.post("showAddAgencies.php",$("#frmAgencies").serialize(),function(data){
		$.facebox({ajax:"showAddAgencies.php"});
		//alert(data);
		//$("#loader").html(data);
	});

});

$(".editlink").click(function(e) {
    	var href=$(this).attr("href");
		$.facebox({ajax:href});
		return false;
});
</script>
<form action="showAddAgencies.php" method="post" id="frmAgencies">
<input type="hidden" value="<? echo $mode?>" name="mode" id="mode">
<input type="hidden" value="<? echo $_GET["id"];?>" name="id" id="id">
<table width="400px" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="label">Agenyc Name</td>
    <td class="field"><input type="text" id="officeName" name="officeName" value="<? echo $officeName;?>"></td>
    <td class="status"></td>
</tr>
<tr>
	<td class="label">Agency Head</td>
    <td class="field"><input type="text" id="contact" name="contact" value="<? echo $contact;?>"></td>
    <td class="status"></td>
</tr>
<!--tr>
	<td class="label">Email</td>
    <td class="field"><input type="text" id="email" name="email" value="<? echo $email;?>"></td>
    <td class="status"></td>
</tr-->
<tr>
	<td colspan='3' align="center"><input type="submit" value=" <? echo $strButtonText; ?> "> <input type="submit" value=" Reset "></td>
</tr>

</table>

</form>

<table width="600px" cellpadding="0" cellspacing="0" align="center" class="borderedtable">
<thead>
<tr>
    <th>Agency Name</th>
    <th>Agency Head</th>
    <!--th>Email</th-->
    <th></th>
</tr>
</thead>
<?
for($i=0;$i<count($arrAgencies);$i++){
	echo "<tr><td>".$arrAgencies[$i]["officeName"]."</td><td>".$arrAgencies[$i]["contactPerson"]."</td>";//<td>".$arrAgencies[$i]["email"]."</td>
	echo "<td>";
	echo "<a href='showAddAgencies.php?mode=edit&id=".$arrAgencies[$i]["originId"]."' class='editlink'>Edit</a> | ";
	echo "<a href='showAddAgencies.php?mode=delete&id=".$arrAgencies[$i]["originId"]."' class='deletelink'>Delete</a></td></tr>";
}
?>
</table>
