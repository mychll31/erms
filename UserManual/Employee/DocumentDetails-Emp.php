<div id="sectiontitle">Document Details</div>
<div id="sectionbody">
<p>This section shows the necessary information regarding a document.</p>

<ol>
<li>To view document details, click the <strong>DOCID</strong> corresponding to the document to view (See Figure 3-4).</li>

<p align="center"><img src="images/Figure3-4DocumentDetails.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-5 Document Details</p></div>
</ol>
</div>