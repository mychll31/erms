<div id="sectiontitle">Default Screen</div>
<div id="sectionbody">
<p>Once logged in to the system you will be automatically directed to the Received Documents Section (See Figure 2).</p>

<p align="center"><img src="images/Figure2DefaultScreen.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 2 Default Screen</p></div>

<p>These are the components of the Default Screen and a brief description of what they are:</p>

<ul>
	<li><strong>Received Documents Section</strong> - Provides links to Received Document List and Manage Folder sub-sections</li>
	<li><strong>Trash Section</strong> - Provides link to Trash sub-section</li>
	<li><strong>Folders Section</strong> - Provides links to Folders created by the user</li>
    <li><strong>Advanced Search Section</strong> - Provides link to the Advanced Search option</li>
     <li><strong>Notification Section</strong> - Provides link to Notification sent to the user</li>
      <li><strong>Switch to HRMIS Section</strong> - Provides link to HRMIS</li>
      </ul>
      </div>
  