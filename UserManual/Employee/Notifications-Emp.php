<div id="sectiontitle">Notifications</div>
<div id="sectionbody">
<p>This section contains notification alerts received by the user that contain information regarding the status and location of a specific document as well as other important details. To view notifications, click the <strong>Notification</strong> icon at the lower right side of the screen (See Figure 3-22).</p>

<p align="center"><img src="images/Figure3-21Notification.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-22 Notification</p></div>

</div>