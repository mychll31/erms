<div id="sectiontitle">Automatic Barcode Printing</div>
<div id="sectionbody">
<p>This section allows user to choose automatic barcode printing options.</p>

<ol>
<li>Once logged in the system will show a confirmation dialog regarding automatic barcode printing option (See Figure 3-2).</li>

<p align="center"><img src="images/Figure3-2AutomaticBarcodePrintingdialogbox.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-2 Automatic Barcode Printing dialog box</p></div>

<li>Choose the preferred option, click <strong>Ok</strong> button  <img src="images/OKButton.jpg" height="20"> to automatically print barcode, <strong>Cancel</strong> button  <img src="images/CancelButton.jpg" height="20"> to cancel operation or <strong>Never for this account</strong> button <img src="images/NeverforthisAccountButton.jpg" height="20">  to disable automatic barcode printing option. A confirmation message will be displayed if the action is successful (See Figure 3-3).</li>

<p align="center"><img src="images/Figure3-3AutomaticBarcodePrintingnotification.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-3 Automatic Barcode Printing notification</p></div>

</ol>
</div>