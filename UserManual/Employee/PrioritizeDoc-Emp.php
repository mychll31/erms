<div id="sectiontitle">How to Mark a Received Document as Priority</div>
<div id="sectionbody">
<ol>
<li>To mark received document/s as priority, place a tick mark on the checkbox corresponding to the document/s to mark in the received document list in <a href="javascript:void(0)" onclick="loadpopup('../Employee/images/Figure3-1ReceivedDocumentspage.jpg', 'Figure 3-1 Received Document page');"> Figure 3-1</a>.</li>
<li>When done, click the <strong>Priority</strong> icon <img src="images/PriorityIcon.jpg" height="20"> located at the upper-left side of the received document list.</li>
<li>An exclamation point image shall suffice at the left side of the document chosen, beside the checkbox (See Figure 3-14).</li>

<p align="center"><img src="images/Figure3-13SampleDocumentMarkedasPriority.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-14 Sample Document Marked as Priority</p></div>

</ol>
</div>