<div id="sectiontitle">Switch from DOST-CO ERMS to DOST-CO HRMIS</div>
<div id="sectionbody">
<p>This section allows user to switch from DOST-CO ERMS to DOST-CO HRMIS. To access HRMIS, click the <strong>HRMIS</strong> link at the bottom right side of the screen. The user will be directed to the HRMIS main page (See Figure 3-21).</p>

<p align="center"><img src="images/Figure3-20HRMISlink.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-21 HRMIS link</p></div>

</div>