<div id="sectiontitle">View History</div>
<div id="sectionbody">
<p>This section contains the information on document movement.</p>

<ol>
<li>To view history of documents, click the <strong>View History</strong> tab (See Figure 3-12).</li>

<p align="center"><img src="images/Figure3-11ViewHistory.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-12 View History</p></div>
</ol>
</div>