<div id="sectiontitle">Employee Module</div>
<div id="sectionbody">
<p>The Employee Module is accessible to the employees of DOST-CO. They can only access records intended for them.</p>

<p>The Employee Module allows users to:</p>

<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Received Documents Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
     <li>View and Manage Received Documents</li>
    <li>Manage Folders</li>
    </ul></td>
    </tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Trash Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View and Manage Documents Deleted by the User</li>
    </ul></td>
    </tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Folders Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Manage Folders</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Advanced Search Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Search Documents using Metadata</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Notification Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View Notifications</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Switch to HRMIS Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Switch to HRMIS</li>
    </ul></td>
    </tr>
      </table>
</table>
</div>