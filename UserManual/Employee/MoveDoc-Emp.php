<div id="sectiontitle">How to Move Received Documents to Other Folder</div>
<div id="sectionbody">
<ol>
<li>To move received document/s to other folder, place a tick mark on the checkbox corresponding to the document/s to move in the received document list in <a href="javascript:void(0)" onclick="loadpopup('../Employee/images/Figure3-1ReceivedDocumentspage.jpg', 'Figure 3-1 Received Document page');"> Figure 3-1</a>.</li>
<li>When done, click the <strong>Move Document</strong> icon  <img src="images/MoveToIcon.jpg" height="20">  located at the upper-left side of the received document list.</li>
<li>Specify the <strong>Name of Folder</strong> from the drop-down list in Figure 3-15</li>

<p align="center"><img src="images/Figure3-14MoveFolderwindow.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-15 Move Folder window</p></div>
<li>When done, click <strong>Move</strong> button <img src="images/MoveButton.jpg" height="20"> .</li>
</ol>
</div>

