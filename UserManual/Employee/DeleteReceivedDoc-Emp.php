<div id="sectiontitle">How to Delete Received Document</div>
<div id="sectionbody">
<ol>
<li>To delete received document/s, place a tick mark on the checkbox corresponding to the document/s to delete in the received document list in <a href="javascript:void(0)" onclick="loadpopup('../Employee/images/Figure3-1ReceivedDocumentspage.jpg', 'Figure 3-1 Received Document page');"> Figure 3-1</a>.</li>
<li>When done, click the <strong>Delete</strong> icon <img src="images/DeleteIcon.jpg" height="20">  located at the upper-left side of the received document list.</li>
<li>A message prompt will appear confirming the received document deletion. Click the <strong>OK</strong> button to proceed or <strong>Cancel</strong> button to cancel operation. A confirmation message will be displayed if the action is successful.</li>
<li>The deleted documents will be moved to the <strong>Trash</strong> Folder.</li>
</ol>
</div>