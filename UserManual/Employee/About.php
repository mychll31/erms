<div id="sectiontitle" style="border-bottom: ridge">DOST-CO Electronic Records Management System</div>
<div id="sectionbody">
<p>DOST-CO Electronic Records Management System (ERMS) is a web-based system that automates the management of documents from records creation to document disposition. It provides a facility for records officers and custodians to view, print and archive documents. The system also allows the user to monitor the movement of the document including its current location and status. It is integrated with the DOST-CO HRMIS.</p>
</div>

<div id="sectiontitle" style="border-bottom: ridge">Credits</div>
<div id="sectionbody">
<p><strong>Information Technology Division</strong></p>
<p><strong>IT Chief:</strong> Donna Ruth Montalban</p>
<p><strong>Lead Developer:</strong> Edgardo Catorce, Jr</p>
<p><strong>Programmer:</strong> Francis Abuel, Louie Carl Mandapat, George Monroyo</p>
<p><strong>System Analyst:</strong> Jennifer Ramirez</p>
<p><strong>Document Analyst:</strong> Remy Monica Oma&#241;a</p>
<p><strong>Quality Assurance Engineer:</strong> Dunn Alfredo Celestial, Angelo Evangelista</p>
<p><strong>Graphic Designer: </strong> Angelo Evangelista</p>
</div>