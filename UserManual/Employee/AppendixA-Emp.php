<div id="sectiontitle">Appendix A: Document Restrictions</div>
<div id="sectionbody">
<p>Document Restriction feature allows the user to limit which office, agency or specific employee(s) can view and/or make necessary updates to a record. The Restricted checkbox is located at the lower portion of the Update Action tab (See Appendix A Figure 1-1).</p>

<p align="center"><img src="images/AppendixAFigure1-1UpdateActionform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-1 Update Action form</p></div>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>Example 1:</strong> Action Unit: Office, Restriction: Unchecked</p>

<p align="center"><img src="images/AppendixAFigure1-2Scenario1.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-2 Scenario 1</p></div>

<p>In this example, <strong>ALL Employees</strong> under Planning and Evaluation Services and <strong>ALL Divisions</strong> &#40;and its <strong>Employees</strong>&#41; under PES will be able to receive the document.</p>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>Example 2: </strong>Action Unit: Office, Restriction: Checked</p>

<p align="center"><img src="images/AppendixAFigure1-3Scenario2.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-3 Scenario 2</p></div>

<p>In this example, <strong>ONLY</strong> the <strong>Head of Office</strong>,<strong> Records Custodian</strong> and <strong>Employees</strong> &#40;who do not belong in any division but are directly under PES&#41; will be able to receive the document.</p>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>Example 3:</strong> Action Unit: Employee &#91;Head of Office&#93;, Restriction: Unchecked</p>

<p align="center"><img src="images/AppendixAFigure1-4Scenario3.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-4 Scenario 3</p></div>

<p>In this example, <strong>ONLY</strong> the <strong>Head of Office</strong> and the <strong>Office&#39;s Records Custodian</strong> will be able to receive the document.</p>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>Example 4</strong> Action Unit: Employee &#91;Head of Office&#93;, Restriction: Checked</p>

<p align="center"><img src="images/AppendixAFigure1-5Scenario4.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-5 Scenario 4</p></div>

<p>In this example, <strong>ONLY</strong> the <strong>Head of Office</strong> will be able to receive the document.</p>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>Example 5:</strong> Action Unit: Employee, Restriction: Unchecked</p>

<p align="center"><img src="images/AppendixAFigure1-6Scenario5.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix A Figure 1-6 Scenario 5</p></div>

<p>In this example, <strong>ONLY</strong> the <strong>Employee</strong> will be able to receive the document.</p>
</div>