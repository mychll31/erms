<div id="sectiontitle">How to View Confidential Document's Digitized File/s</div>
<div id="sectionbody">

<p>It is required to have XAMPP installed on user's workstation to be able to access the confidential document's digitized file/s.</p>
<p>For a guide on how to install XAMPP, see <i>APPENDIX C: XAMPP Installation and Configuration for Windows</i> on ERMS Online Manual.</p>

	<p align="center"><img src="images/ERMS online manual window.jpg" width="500" border="1"></p>

<ol>	
	<li>After installing the XAMPP, create <b>"UPLOADLOCAL"</b> folder on <i>C:\xampp\htdocs\</i> directory.</li>

	<li>Copy the <b><a href="../../UPLOADLOCAL/getRemoteFile.txt" download>getRemoteFile.php</a></b> (it will be provided by the ITD staff) into the <i>C:\xampp\htdocs\UPLOADLOCAL\</i> folder.</li>

	<li>When done, uploaded file/s under the confidential document will be unrestricted to other users who have an access on the document.</li>

	<li>Should you have any questions, please contact an ITD staff at local <b>2008</b>.</li>
</ol>

</div>