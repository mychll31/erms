<div id="sectiontitle">Documents</div>
<div id="sectionbody">
<p>This section allows user to perform Document Management functionalities such as view and manage incoming and outgoing documents, upload digitized copies of documents, search documents and generate reports among others. To access the Documents section, click the <strong>Documents</strong> link on the left side of the screen (See Figure 3-1).</p>

<p align="center"><img src="images/Figure3-1Documentspage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-1 Documents page</p></div>

<p><div id="notebox"><strong>Note: </strong><em>By default, Document List sub-section is shown on the Documents page. To view other sub-sections under Documents, refer to Section 3.1.2-Section 3.1.4.</em></div></p>

</div>