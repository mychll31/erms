<div id="sectiontitle">How to Upload Digitized Files</div>
<div id="sectionbody">
<ol>
<li>To upload digitized documents, click <strong>Add New</strong> button  <img src="images/AddNewButton.jpg" height="20">  in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-10ManageFiles.jpg', 'Figure 3-10 Manage Files');"> Figure 3-10</a> (See Figure 3-11).</li>

<p align="center"><img src="images/Figure3-11AttachFileswindow.jpg" width="300"></p>
<div id="figurelabel"><p>Figure 3-11 Attach Files Window</p></div>

<li>To select files to upload, click <strong>Browse</strong> button  <img src="images/BrowseButton.jpg" height="20"> in Figure 3-11.  </li>
<li>When done, click <strong>Start Upload</strong> button  <img src="images/StartUploadButton.jpg" height="17"> to start uploading selected files or <strong>Cancel</strong> button  <img src="images/CancelButton_white.jpg" height="17"> to cancel operation (See Figure 3-12). </li>

<p align="center"><img src="images/Figure3-12Uploadwindow.jpg" width="300" /></p>
<div id="figurelabel"><p>Figure 3-12 Upload window</p></div>

</ol>
</div>