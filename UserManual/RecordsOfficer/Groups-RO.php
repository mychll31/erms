<div id="sectiontitle">Groups</div>
<div id="sectionbody">
<ol>
<li>To access the Groups section, click the <strong>Groups</strong> tab (See Figure 3-58).</li>

<p align="center"><img src="images/Figure3-58Groupspage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-58 Groups page</p></div>
</ol>
</div>