<div id="sectiontitle">How to Delete Existing Cabinet Details</div>
<div id="sectionbody">
<ol>
<li>To delete existing Cabinet details, select a row corresponding to the Cabinet details to delete then click the <strong>Delete</strong> icon <img src="images/DeleteIcon.jpg" height="20"> .</li>
<li>A message prompt will appear confirming the deletion. Click the <strong>OK</strong> button to proceed or <strong>Cancel</strong> button to cancel operation. A confirmation message will be displayed if the action is successful.
</li>
</ol>
</div>