<div id="sectiontitle">Records Officer Module</div>
<div id="sectionbody">
<p>The Records Officer Module allows user to access all records and is authorized to process Incoming and Outgoing Documents, as well as other functionalities and features of the system, such management of system Libraries and Documents for Disposal, among others.</p>
<p>The Records Officer Module allows users to:</p>

<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Documents Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View and Manage Incoming and Outgoing Documents</li>
    <li>View and Edit Document Details</li>
    <li>Upload Digitized Copies of Documents</li>
    <li>Update Action Taken</li>
    <li>View Document History</li>
    <li>Search Documents</li>
    <li>Print Barcodes for Documents</li>
    <li>Generate and Print Reports</li>
    </ul></td>
  </tr>
  <tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Received Documents Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View and Manage Received Documents</li>
    <li>Manage Folders</li>
    </ul></td>
    </tr>
    <tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Libraries Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View and Manage Office</li>
    <li>View and Manage Cabinet</li>
     <li>View and Manage Document Type</li>
      <li>View and Manage Action Required</li>
       <li>View and Manage Action Taken</li>
        <li>View and Manage System Backup</li>
         <li>View and Manage Custodian</li>
          <li>View and Manage Groups</li>
           <li>View and Manage User Account</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Disposal Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Dispose Documents</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Trash Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View and Manage Documents Deleted by the User</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Switch to Employee Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Switch to Employee Access Level</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Folders Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Manage Folders</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Advanced Search Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Search Documents using Metadata</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Notification Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>View Notifications</li>
    </ul></td>
    </tr>
<tr>
    <td width="125" valign="top"><p style="margin-left:20px" >Switch to HRMIS Section</p></td>
    <td width="495" valign="top">
	<ul id="list">
    <li>Switch to HRMIS</li>
    </ul></td>
    </tr>
      </table>
</table>
</div>