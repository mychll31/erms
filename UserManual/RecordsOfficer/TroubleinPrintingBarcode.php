<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">Trouble in Printing Barcode</div>
	<div id="sectionbody">
		<p>
			<div id="notebox"><strong>Browser: </strong>Google Chrome</div>
		</p>
		<p>
			<div id="notebox"><strong>Reason: </strong>The pop-ups are blocked on the page.</div>
		</p>

	<ol>
		<li>Click the <b>Print Barcode</b> button.
		</li>
			<p align="center"><img src="images/1.jpg" width="550" border="1">
			</p>
		<li>Click the <b>(pop-up blocked)</b> icon at the upper right corner of your browser (encircled with red).
		</li>
			<p align="center"><img src="images/2.jpg" width="550" border="1"></p>
			<p>
		<li>Select <b>Always allow pop-ups</b> and click <b>Done</b>.
		</li>	
			<p align="center"><img src="images/3.jpg" width="550" border="1"></p>
			<p>
		<li>Refresh the page and check if the (pop-up blocked) icon is gone.
		</li>
	</ol>
		<br>
		<p>
			<div id="notebox"><strong>Browser: </strong>Mozilla Firefox</div>
		</p>
	<ol>
		<li>Click the <b>Print Barcode</b> button.
		</li>
			<p align="center"><img src="images/1.jpg" width="550" border="1">
			</p>
		<li>Click the <b>Options</b> button.
		</li>
			<p align="center"><img src="images/2-2.jpg" width="550" border="1">
			</p>
		<li>Click <b>Allow pop-ups form erms.dost.gov.ph.</b>
		</li>
			<p align="center"><img src="images/3-2.jpg" width="550" border="1">
			</p>
		<li>Refresh the page.</b>
		</li>	
	</ol>
	</div>

</body>
</html>