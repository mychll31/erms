<div id="sectiontitle">How to Update Action of a Document</div>
<div id="sectionbody">
<ol>
<li>To update action of a document, fill-up the entry boxes that constitute the parameters for updating action in Figure 3-15.</li>
<li>Specify the <strong>Action Taken</strong> from the drop-down list</a>.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency </strong>on the <strong>Action Unit</strong> entry box or specify it from the drop-down list.</li>
<li>Specify the <strong>Action Needed</strong> from the drop-down list.</li>
<p><div id="notebox"><strong>Note: </strong><em>Place a tick mark on the checkbox if an action/reply is required.</em></div></p>

<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<p><div id="notebox"><strong>Note: </strong><em>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted. </em></div></p>
<p><div><em>For restrictions on documents, see  <a id= "link" onClick="getData('AppendixA-RO.php','maincontent');">APPENDIX A: Document Restrictions</a>.</em></div></p>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitBtn.jpg" height="20">. Action made to the document will be displayed in the Update Action tab.</li>
<li>If the action has been completed, click the <b>Action Completed</b> button <img src="images/ActionCompletedButton.jpg" height="17">.</li>
<li>A message prompt will appear confirming the document completion. Click <b>Ok</b> button <img src="images/OKButton.jpg" height="20">.</li> to proceed or <b>Cancel</b> button <img src="images/CancelButton2.jpg" height="20">.</li> to cancel operation. A confirmation message will be displayed if the action is successful.</li>
<p><div id="notebox"><strong>Note: </strong><em>Once completed, user will not be able to send the document to other offices.</em></div></p>

<!-- <p align="center"><img src="images/Figure3-14UpdateDocumentAction.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-14 Update Document Action</p></div> -->
</ol>
</div>
