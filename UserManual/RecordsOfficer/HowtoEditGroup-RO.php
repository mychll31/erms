<div id="sectiontitle">How to Edit Existing Group Details</div>
<div id="sectionbody">
<ol>
<li>To edit existing Group details, select a row corresponding to the Group details to edit then click the <strong>Edit</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20"> (See Figure 3-60).</li>

<p align="center"><img src="images/Figure3-60EditGroupform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-60 Edit Group form</p></div>

<li>Information that can be updated includes <strong>Group Code</strong> and <strong>Group Description</strong>.</li>

<li>When done, click <strong>Save</strong> button<img src="images/SaveButton.jpg" height="20">    to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton_blue.jpg" height="20">  to cancel operation.
</li>
</ol>
</div>