<div id="sectiontitle">Advanced Search</div>
<div id="sectionbody">
<p>This section provides more comprehensive searching of records that matched the search criteria. To access the Advanced Search section, click the <strong>Advanced Search</strong> link at the upper right side of the screen (See Figure 3-72).</p>

<p align="center"><img src="images/Figure3-72AdvancedSearchpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-72 Acvanced Search page</p></div>

</div>