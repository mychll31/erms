<div id="sectiontitle">How to Open Document Using Barcode Scanner</div>
<div id="sectionbody">

<ol>
<li>To open a document using barcode scanner, scan the document barcode using the device - place the red light directly over the middle of the barcode, then wait for the document details to display.</li>

<p align="center"><img src="images/barcode scanner 2.jpg" width="385" height="335" border="1"></p>

<p><div id="notebox"><strong>Note: </strong><em>Once read, user will be able to view and edit the details of the scanned document barcode.</em></div></p>
</ol>
</div>
