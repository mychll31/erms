<div id="sectiontitle">How to Set Own Deadline</div>
<div id="sectionbody">
<ol>
<li>To set own deadline, click the <strong>Update Action</strong> tab then click the <strong>Set Own Deadline</strong> icon <img src="images/SetOwnDeadlineIcon.jpg" height="20">   in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-27AddActionReplywindow.jpg', 'Figure 3-27 Add Action Reply window');"> Figure 3-27</a>.</li>
<li>Fill-up the <strong>Deadline</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitButton.jpg" height="19"> .</li>
</ol>
</div>