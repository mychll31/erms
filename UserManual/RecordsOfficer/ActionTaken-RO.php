<div id="sectiontitle">Action Taken</div>
<div id="sectionbody">
<ol>
<li>To access the Action Taken section, click the <strong>Action Taken</strong> tab (See Figure 3-50).</li>

<p align="center"><img src="images/Figure3-50ActionTakenpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-50 Action Taken page</p></div>

</ol>
</div>