<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to Update Controlled Document Type</div>
	<div id="sectionbody">

	<ol>
		<p align="center"><img src="images/Update Controlled Document tab.jpg" width="525" border="1"></p>

		<li>Specify the <b>Controlled Document Type</b> (Maintained, Retained, Form, External Reference, Internal Reference, and Obsolete Document) from the drop-down list. Depending on the controlled document type chosen, supply the parameter(s) it may require using the entry box(es) provided. </li>


		<li>When done, click the <b>Update Controlled Document</b> button <img src="images/UpdateControlledDocument button.jpg" height="20"> to save entry.</li>

		<li>A confirmation message will be displayed if the action is successful.</li>

		<p><div id="notebox"><strong>Note: </strong><em>Updated controlled document type will not be visible to other offices that have an access on the document.</em></div></p>

	</ol>
	</div>

</body>
</html>