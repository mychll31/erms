<div id="sectiontitle">How to End the Processing of Document</div>
<div id="sectionbody">
<ol>
<p><div id="notebox"><strong>Note: </strong><em>This process is applicable only if the document does not require any action to follow.</em></div></p>

<p align="center"><img src="images/ActionCompletewindow.jpg" width="525"></p>
<li>To end the processing of document, click the <b>Action Completed</b> button <img src="images/ActionCompletedButton.jpg" height="20"> from the Update Action tab.</li>

<p><div id="notebox"><strong>Note: </strong><em>Once clicked, user will not be able to add any action/reply on the document.</em></div></p>

</ol>
</div>