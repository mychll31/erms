<div id="sectiontitle">Scheduled Backup</div>
<div id="sectionbody">
<ol>
<li>To modify schedule of automated backup, click the <strong>Scheduled</strong> link in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-53Backuppage.jpg', 'Figure 3-53 Backup page');"> Figure 3-53</a>. You will be directed to the Configure Database Backup page (See Figure 3-54).</li>

<p align="center"><img src="images/Figure3-54ConfigDbaseBackup.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-54 Configure Database Backup Schedule page</p></div>

<li>Change the frequency of database backup to desired schedule.</li>
<li>Optional configuration can be updated depending upon the user&#39;s preferences.</li>
</ol>
</div>
