<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to Undo Completed Action</div>
	<div id="sectionbody">

	<ol>
		<p align="center"><img src="images/UndoActiontab.jpg" width="525" border="1"></p>
		<p>
			<div id="notebox"><strong>Note: </strong><em>User's office that completed the processing of document (as shown in the Update Action tab) has the overall responsibility to undo the action.</em>
			</div>
		</p>

		<li>To undo completed action, click the <b>Undo Completed Action</b> button <img src='images/UndoCompletedActionBtn.jpg' height="18" /> from the Update Action tab.
		</li>

		<li>A confirmation message will be displayed if the action is successful.</li>
		<p>
			<div id="notebox"><strong>Note: </strong><em>Once clicked, users who have an access on the document will be able to add action/reply.</em>
			</div>
		</p>	

	</ol>
	</div>

</body>
</html>