<div id="sectiontitle">How to Edit Existing Cabinet Details</div>
<div id="sectionbody">
<ol>
<li>To edit existing Cabinet details, select a row corresponding to the Cabinet details to edit then click the <strong>Edit</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20"> (See Figure 3-43).</li>

<p align="center"><img src="images/Figure3-43EditCabinetform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-43 Edit Cabinet form</p></div>

<li>Information that can be updated includes <strong>Cabinet Label</strong>.</li>
<li>When done, click <strong>Save</strong> button<img src="images/SaveButton.jpg" height="20">    to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton_blue.jpg" height="20">  to cancel operation.
</li>
</ol>
</div>