<div id="sectiontitle">Document List</div>
<div id="sectionbody">
<p>This section displays the list of all documents associated with the user arranged according to Document ID (DOCID). This also includes the document details such as Subject, Status, Document Type (DOC TYPE), Document Date (DOC DATE) and Origin. To access the Document List section, click the <strong>Document List</strong> tab (See Figure 3-2).</p>

<p align="center"><img src="images/Figure3-2DocumentListpage.jpg" width="525"></p>
<div id="figurelabel"><p>Figure 3-2 Document List page</p></div>

<p><div id="notebox"><strong>Note: </strong><em>To sort the data in ascending/descending order, click its header with the icon. Clicking the numbered link(s) below the screen would lead to the next or previous list of documents.</em></div></p>
</div>

