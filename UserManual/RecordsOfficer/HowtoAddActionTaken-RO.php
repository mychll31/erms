<div id="sectiontitle">How to Add New Action Taken Details</div>
<div id="sectionbody">
<ol>
<li>To add new Action Taken details, fill-up the entry boxes that constitutes to the parameters for adding new Action Taken details in Figure 3-51.</li>

<p align="center"><img src="images/Figure3-51AddActionTakenform.jpg" width="317" border="1"></p>
<div id="figurelabel"><p>Figure 3-51 Add Action Taken form</p></div>

<li>Type in the <strong>Action Taken Code</strong> and <strong>Action Taken Description</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>