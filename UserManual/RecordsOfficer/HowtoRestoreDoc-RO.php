<div id="sectiontitle">How to Restore Document</div>
<div id="sectionbody">
<ol>
<li>To restore a document record, click the <strong>Restore to Documents</strong> icon <img src="images/RestoretoDocsIcon.jpg" height="20">  in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-66Trashpage.jpg', 'Figure 3-66 Trash page');"> Figure 3-66</a> .</li>
<li>A confirmation message will be displayed if the action is successful (See Figure 3-67). 
</li>

<p align="center"><img src="images/Figure3-67DocumentRestoreConfirmation.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-67 Document Restore Confirmation</p></div>

</ol>
</div>