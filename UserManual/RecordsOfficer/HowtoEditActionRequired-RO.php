<div id="sectiontitle">How to Edit Existing Action Required Details</div>
<div id="sectionbody">
<ol>
<li>To edit existing Action Required details, select a row corresponding to the Action Required details to edit then click the <strong>Edit</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20">  (See Figure 3-49).</li>

<p align="center"><img src="images/Figure3-49EditActionRequiredform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-49 Edit Action Required form</p></div>

<li>Information that can be updated includes <strong>Action Required Code</strong> and <strong>Action Required Description</strong>.</li>

<li>When done, click <strong>Save</strong> button<img src="images/SaveButton.jpg" height="20">    to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton_blue.jpg" height="20">  to cancel operation.
</li>
</ol>
</div>