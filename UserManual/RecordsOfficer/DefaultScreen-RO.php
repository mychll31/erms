<div id="sectiontitle">Default Screen</div>
<div id="sectionbody">
<p>Once logged in to the system you will be automatically directed to the Documents Section (See Figure 2).</p>

<p align="center"><img src="images/Figure2DefaultScreen.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 2 Default Screen</p></div>

<p>These are the components of the Default Screen and a brief description of what they are:</p>

<ul>
  <li><strong>User</strong> - Displays the username of the logged-in user</li>
    <li><strong>Document Section</strong> - Provides links to Document List, Incoming, Outgoing and Reports sub-sections</li>
    <li><strong>Received Documents Section</strong> - Provides links to Received Document List and Manage Folder sub-sections</li>
    <li><strong>Libraries Section</strong> - Provides links to Office, Cabinet, Document Type, Action Required, Action Taken, Backup, Manage Custodian, Groups and User Account sub-sections</li>
     <li><strong>Disposal Section</strong> - Provides links to For Disposal, Disposed Documents, Documents Transferred to Records Center and Documents Transferred to National Archives sub-section</li>
     <li><strong>Trash Section</strong> - Provides link to Trash sub-section</li>
     <li><strong>Switch to Employee Section</strong> - Provides  link to the user’s account in the Employee Module </li>
     <li><strong>Folders Section</strong> - Provides links to Folders created by the user</li>
     <li><strong>Advanced Search Section</strong> - Provides link to the Advanced Search option</li>
     <li><strong>Notification Section</strong> - Provides link to Notification sent to the user</li>
      <li><strong>Switch to HRMIS Section</strong> - Provides link to HRMIS</li>
      </ul>
      </div>
  