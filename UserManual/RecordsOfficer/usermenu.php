<? //$dir="help/RecordsOfficer/";?>
<div id="usermenu">

<s1 onClick='getData("Login-RO.php","maincontent");'>Login</s1><br>
	<s2 onClick='getData("HowtoLogin-RO.php","maincontent");'>How to Login</s2><br>
<s1 onClick='getData("DefaultScreen-RO.php","maincontent");'>Default Screen</s1><br> 
<s1 onClick='getData("RecordsOfficerModule-RO.php","maincontent");'>Records Officer Module</s1><br>
	<s2 onClick='getData("Documents-RO.php","maincontent");'>Documents</s2><br>
    	<s3 onClick='getData("DocumentList-RO.php","maincontent");'>Document List</s3><br>
    		<s4 onClick='getData("HowtoOpenDoc-RO.php","maincontent");'>How to Open Document using Barcode Scanner</s4><br>
        	<s4 onClick='getData("HowtoSearchDoc-RO.php","maincontent");'>How to Search Document</s4><br>
            <s4 onClick='getData("HowtoSortDoc-RO.php","maincontent");'>How to Sort Document</s4><br>
            <s4 onClick='getData("DocumentDetails-RO.php","maincontent");'>Document Details</s4><br>
				<s5 onClick='getData("HowtoEditDocInfo-RO.php","maincontent");'>How to Edit Document Information</s5><br>
				<s5 onClick='getData("HowtoAddDoc-RO.php","maincontent");'>How to Add New Document</s5><br>
				<s5 onClick='getData("HowtoPrintBarcode-RO.php","maincontent");'>How to Print Document Barcode</s5><br>
			<s4 onClick='getData("ManageFiles-RO.php","maincontent");'>Manage Files</s4><br>
            	<s5 onClick='getData("HowtoUploadDigitizedFiles-RO.php","maincontent");'>How to Upload Digitized Files</s5><br>
            	<s5 onClick='getData("HowtoOpenConfidentialDoc-RO.php","maincontent");'>How to View Confidential Document's Digitized File/s</s5><br>
			<s4 onClick='getData("UpdateAction-RO.php","maincontent");'>Update Action</s4><br>
        	 	<s5 onClick='getData("HowtoUpdateAction-RO.php","maincontent");'>How to Update Action of a Document</s5><br>
				<s5 onClick='getData("HowtoUndoAction-RO.php","maincontent");'>How to Undo an Action Taken</s5><br>
			<s4 onClick='getData("ViewHistory-RO.php","maincontent");'>View History</s4><br>
        	<s4 onClick='getData("UploadDigitizedFiles-RO.php","maincontent");'>Upload Digitized File to an Existing Document</s4><br>
        	<s4 onClick='getData("DeleteDoc-RO.php","maincontent");'>Delete Document</s4><br>
		<s3 onClick='getData("Incoming-RO.php","maincontent");'>Incoming</s3><br>
    	 	<s4 onClick='getData("HowtoAddIncomingDoc-RO.php","maincontent");'>How to Add Incoming Documents</s4><br>
		<s3 onClick='getData("Outgoing-RO.php","maincontent");'>Outgoing</s3><br>
			<s4 onClick='getData("HowtoAddOutgoingDoc-RO.php","maincontent");'>How to Add Outgoing Documents</s4><br>
		<s3 onClick='getData("Reports-RO.php","maincontent");'>Reports</s3><br>
			<s4 onClick='getData("HowtoGenerateReport-RO.php","maincontent");'>How to Generate Report</s4><br>
            <s4 onClick='getData("HowtoSaveReport-RO.php","maincontent");'>How to Save Generated Report</s4><br>
			<s4 onClick='getData("HowtoPrintReport-RO.php","maincontent");'>How to Print Generated Report</s4><br>
	<s2 onClick='getData("ReceivedDocs-RO.php","maincontent");'>Received Documents</s2><br>
		<s3 onClick='getData("HowtoReceiveDocsUsingBarcodeScanner.php","maincontent");'>How to Receive Documents Using Barcode Scanner</s3><br>
    	<s3 onClick='getData("HowtoReceiveDocs-RO.php","maincontent");'>How to Receive Documents</s3><br>
        <s3 onClick='getData("HowtoAddActionReply-RO.php","maincontent");'>How to Add Action Reply</s3><br>
        <s3 onClick='getData("HowtoSetDeadline-RO.php","maincontent");'>How to Set Own Deadline</s3><br>
        <s3 onClick='getData("HowtoFlagDoc-RO.php","maincontent");'>How to Flag Document</s3><br>
        <s3 onClick='getData("HowtoPrioritizeDoc-RO.php","maincontent");'>How to Mark a Received Document as Priority</s3><br>
        <s3 onClick='getData("HowtoDeleteReceivedDoc-RO.php","maincontent");'>How to Delete Received Document</s3><br>
        <s3 onClick='getData("HowtoMoveDoc-RO.php","maincontent");'>How to Move Received Documents to Other Folder</s3><br>
         <s3 onClick='getData("HowtoManageFolders-RO.php","maincontent");'>How to Manage Folders</s3><br>
 	<s2 onClick='getData("Libraries-RO.php","maincontent");'>Libraries</s2><br>
    	<s3 onClick='getData("Office-RO.php","maincontent");'>Office</s3><br>
			<s4 onClick='getData("HowtoAddOffice-RO.php","maincontent");'>How to Add Office Details</s4><br>
            <s4 onClick='getData("HowtoEditOffice-RO.php","maincontent");'>How to Edit Existing Office Details</s4><br>
			<s4 onClick='getData("HowtoDeleteOffice-RO.php","maincontent");'>How to Delete Existing Office Details</s4><br>
			<s4 onClick='getData("HowtoFindRecords-RO.php","maincontent");'>How to Find Records</s4><br>
		<s3 onClick='getData("Cabinet-RO.php","maincontent");'>Cabinet</s3><br> 
			<s4 onClick='getData("HowtoAddCabinet-RO.php","maincontent");'>How to Add Cabinet Details</s4><br>
			<s4 onClick='getData("HowtoEditCabinet-RO.php","maincontent");'>How to Edit Existing Cabinet Details</s4><br>
			<s4 onClick='getData("HowtoDeleteCabinet-RO.php","maincontent");'>How to Delete Existing Cabinet Details</s4><br>
			<s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br>
		<s3 onClick='getData("DocumentType-RO.php","maincontent");'>Document Type</s3><br>   
			<s4 onClick='getData("HowtoAddDocumentType-RO.php","maincontent");'>How to Add Document Type Details</s4><br>          
			<s4 onClick='getData("HowtoEditDocumentType-RO.php","maincontent");'>How to Edit Existing Document Type Details</s4><br>
			<s4 onClick='getData("HowtoDeleteDocumentType-RO.php","maincontent");'>How to Delete Existing Document Type Details</s4><br>
 			<s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br>
		<s3 onClick='getData("ActionRequired-RO.php","maincontent");'>Action Required</s3><br>       
        	<s4 onClick='getData("HowtoAddActionRequired-RO.php","maincontent");'>How to Add Action Required Details</s4><br>  
            <s4 onClick='getData("HowtoEditActionRequired-RO.php","maincontent");'>How to Edit Existing Action Required Details</s4><br>   
            <s4 onClick='getData("HowtoDeleteActionRequired-RO.php","maincontent");'>How to Delete Existing Action Required Details</s4><br>
			<s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br> 
		<s3 onClick='getData("ActionTaken-RO.php","maincontent");'>Action Taken</s3><br>       
        	<s4 onClick='getData("HowtoAddActionTaken-RO.php","maincontent");'>How to Add Action Taken Details</s4><br>	
            <s4 onClick='getData("HowtoEditActionTaken-RO.php","maincontent");'>How to Edit Existing Action Taken Details</s4><br>  
            <s4 onClick='getData("HowtoDeleteActionTaken-RO.php","maincontent");'>How to Delete Existing Action Taken Details</s4><br>
			<s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br>						
		<s3 onClick='getData("Backup-RO.php","maincontent");'>Backup</s3><br>   
        	<s4 onClick='getData("ScheduledBackup-RO.php","maincontent");'>Scheduled Backup</s4><br>  
            <s4 onClick='getData("ManualBackup-RO.php","maincontent");'>Manual Backup</s4><br>    
			<s4 onClick='getData("HowtoDownloadBackupFile-RO.php","maincontent");'>How to Download Backup File</s4><br>
			<s4 onClick='getData("HowtoDeleteBackupFile-RO.php","maincontent");'>How to Delete Backup File</s4><br>  
			<s4 onClick='getData("HowtoFindBackupFile-RO.php","maincontent");'>How to Find Backup File</s4><br>  
		<s3 onClick='getData("ManageCustodian-RO.php","maincontent");'>Manage Custodian</s3><br> 
			<s4 onClick='getData("HowtoAddCustodian-RO.php","maincontent");'>How to Add Custodian Details</s4><br>	
            <s4 onClick='getData("HowtoEditCustodian-RO.php","maincontent");'>How to Edit Existing Custodian Details</s4><br>  
            <s4 onClick='getData("HowtoDeleteCustodian-RO.php","maincontent");'>How to Delete Existing Custodian Details</s4><br>	
		<s3 onClick='getData("Groups-RO.php","maincontent");'>Groups</s3><br>	
			<s4 onClick='getData("HowtoAddGroup-RO.php","maincontent");'>How to Add Group Details</s4><br>	
            <s4 onClick='getData("HowtoEditGroup-RO.php","maincontent");'>How to Edit Existing Group Details</s4><br>
            <s4 onClick='getData("HowtoDeleteGroup-RO.php","maincontent");'>How to Delete Existing Group Details</s4><br>	  
            <s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br>
		<s3 onClick='getData("UserAccount-RO.php","maincontent");'>User Account</s3><br>	
			<s4 onClick='getData("HowtoAddUserAccount-RO.php","maincontent");'>How to Add User Account</s4><br>	
			<s4 onClick='getData("HowtoEditUserAccount-RO.php","maincontent");'>How to Edit Existing User Account</s4><br>            
			<s4 onClick='getData("HowtoDeleteUserAccount-RO.php","maincontent");'>How to Delete Existing User Account</s4><br>  
			<s4 onClick='getData("HowtoFindRecords1-RO.php","maincontent");'>How to Find Records</s4><br>  
	<s2 onClick='getData("Disposals-RO.php","maincontent");'>Disposals</s2><br> 
    	<s3 onClick='getData("HowtoDisposeDoc-RO.php","maincontent");'>How to Dispose Documents</s3><br>
	<s2 onClick='getData("Trash-RO.php","maincontent");'>Trash</s2><br>  
    	<s3 onClick='getData("HowtoRestoreDoc-RO.php","maincontent");'>How to Restore Documents</s3><br>        
        <s3 onClick='getData("HowtoPermanentlyDeleteDoc-RO.php","maincontent");'>How to Permanently Delete Documents</s3><br>   
	<s2 onClick='getData("RecordsOfficertoEmployee-RO.php","maincontent");'>Switch from Records Officer to Employee</s2><br> 
    	<s3 onClick='getData("HowtoManageEmployeeModule-RO.php","maincontent");'>How to Manage Employee Module</s3><br>   
	<s2 onClick='getData("ERMStoHRMIS-RO.php","maincontent");'>Switch from DOST-CO ERMS to DOST-CO HRMIS</s2><br>   
    <s2 onClick='getData("Notifications-RO.php","maincontent");'>Notifications</s2><br>  
    	<s3 onClick='getData("HowtoViewNotification-RO.php","maincontent");'>How to View Notification</s3><br>
	<s2 onClick='getData("AdvancedSearch-RO.php","maincontent");'>Advanced Search</s2><br>      
		<s3 onClick='getData("HowtoUseMetadataSearch-RO.php","maincontent");'>How to Use Metadata Search</s3><br>
<appendix onClick='getData("AppendixA-RO.php","maincontent");'>A Document Restrictions</appendix><br> 
<appendix onClick='getData("AppendixB-RO.php","maincontent");'>B ERMS Reports</appendix><br>  
	<appendix1 onClick='getData("AppendixB-RO.php","maincontent");'>1 List of Incoming Records</appendix1><br>  
    <appendix1 onClick='getData("AppendixB2-RO.php","maincontent");'>2 List of Outgoing Records</appendix1><br>  
	<appendix1 onClick='getData("AppendixB3-RO.php","maincontent");'>3 List of Records with Deadline</appendix1><br> 
	<appendix1 onClick='getData("AppendixB4-RO.php","maincontent");'>4 Number of Documents Processed</appendix1><br> 
	<appendix1 onClick='getData("AppendixB5-RO.php","maincontent");'>5 Records Action</appendix1><br>
	<appendix1 onClick='getData("AppendixB6-RO.php","maincontent");'>6 Records Profile</appendix1><br>
	<appendix1 onClick='getData("AppendixB7-RO.php","maincontent");'>7 Unacted Documents</appendix1><br>
<appendix onClick='getData("AppendixC-RO.php","maincontent");'>C XAMPP Installation and Configuration for Windows</appendix><br> 
<about onClick='getData("About.php","maincontent");'>About DOST-CO ERMS</about><br>
</div>