<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to Generate Custom Report</div>
	<div id="sectionbody">

	<ol>
		<li>
			To generate custom report, select the <b>Custom Report 1</b> under Reports tab (See figure below).
		</li>
				<p align="center"><img src="images/Report Selection.jpg" width="650" border="1">
				</p>
		<li>
			From the options provided, select the <b>Type of Report</b> you want to generate.
		
				<p align="center"><img src="images/Custom Report 1 form.jpg" width="650" border="1">
				</p>

				<ul style="list-style-type:circle">
  					<li><b>Date Received by Office</b> - all documents received by your office from other office/s based on the period given</li>
  					<li><b>Date Sent by Office</b> � all documents released by your office to other office/s on the period given</li>
  					<li><b>Date Created by Office</b> � all documents encoded by your office (including those which has no action taken) on the period given</li>
				</ul>
		</li>
		<br>	
		<li>
			Please take note also of the following type of documents:
				<ul style="list-style-type:circle">
  					<li><b>Incoming</b> � Documents received by your office (DOST-CO) from other agencies</li>
  					<li><b>Outgoing</b> - Documents that originated from your office (DOST-CO) to other agencies/offices</li>
  					<li><b>Summary</b> � Lists all incoming and outgoing documents passed through your office</li>
				</ul>
		</li>	
		<br>		
		<li>	
			When done, click the <b>Generate</b> button <img src="images/GenerateButton.jpg" height="22">. 
		</li>
		<li>	
			The report will be automatically downloaded to your local in excel format. 
		</li>

	</ol>
	
	</div>
</body>
</html>