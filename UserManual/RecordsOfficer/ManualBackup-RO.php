<div id="sectiontitle">Manual Backup</div>
<div id="sectionbody">
<ol>
<li>To manually backup, click the <strong>Backup now</strong> link in  <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-53Backuppage.jpg', 'Figure 3-53 Backup page');"> Figure 3-53</a>.</li>
<li>The backup file will instantly be listed on the grid.</li>
</ol>
</div>