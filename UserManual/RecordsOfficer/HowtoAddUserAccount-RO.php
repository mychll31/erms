<div id="sectiontitle">How to Add New User Account</div>
<div id="sectionbody">
<ol>
<li>To add new User Account, fill-up the entry boxes that constitute to the parameters for adding new User Account in Figure 3-62.</li>

<p align="center"><img src="images/Figure3-62AddUserAccountform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-62 Add User Account form</p></div>

<li>Type in the <strong>ID Number</strong>, <strong>First Name</strong>,<strong> Middle Initial</strong>, <strong>Surname</strong> and <strong>Name Extension</strong> on the entry boxes provided.</li>
<li>Specify the <strong>Office</strong> from the drop-down list.</li>
<li>Type in the <strong>Username</strong> and <strong>Password</strong> on the entry boxes provided.
</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>