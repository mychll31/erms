<div id="sectiontitle">Manage Custodian</div>
<div id="sectionbody">
<ol>
<li>To access the Manage Custodian section, click the <strong>Manage Custodian</strong> tab (See Figure 3-55).</li>

<p align="center"><img src="images/Figure3-55ManageCustodianpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-55 Manage Custodian page</p></div>

</ol>
</div>