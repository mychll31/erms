<div id="sectiontitle">How to Update Location of Received Document/s</div>
<div id="sectionbody">
<ol>
<p align="center"><img src="images/UpdateLocationofDocs.jpg" width="525"></p>
<li>To update location of received document/s, specify the <b>File Container</b> (Cabinet, Drawer, and Container) from the drop-down lists.</li>

<li>When done, click the <strong>Update Location</strong> button <img src="images/UpdateLocationButton.jpg" height="20"> .</li>

<li>A confirmation message will be displayed if the action is successful.</li>

<p><div id="notebox"><strong>Note: </strong><em>Updated file location will not be visible to other offices that have an access on the document.</em></div></p>

</ol>
</div>