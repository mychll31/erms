<div id="sectiontitle">How to Permanently Delete Document</div>
<div id="sectionbody">
<ol>
<li>To permanently delete a document, click the <strong>Delete</strong> icon  <img src="images/DeleteIcon_blue.jpg" height="20">  in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-66Trashpage.jpg', 'Figure 3-66 Trash page');"> Figure 3-66</a>.</li>
<li>A message prompt will appear confirming the action deletion. Click the <strong>OK</strong> button to proceed or <strong>Cancel</strong> button to cancel operation. A confirmation message will be displayed if the action is successful.</li>
</ol>
</div>