<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to View Document Status without Logging In</div>
	<div id="sectionbody">

	<ol>
		<li>
			To view document status without logging in, fill-in the entry box that constitutes the parameter for searching document (See figure below).
		</li>
				<p align="center"><img src="images/Search Doc Status.jpg" width="550" border="1">
				</p>
		<li>
			Type in the <b>Document No.</b> on the entry box provided.
		</li>
		<li>
			When done, click <b>Search</b> button <img src="images/Search Button 2.jpg" height="20">.
		</li>	
		<li>
			Once clicked, recent document status will immediately be displayed below the entry box (See figure below).
		</li>
				<p align="center"><img src="images/Doc Status.jpg" width="350" border="1">
				</p>
	</ol>
	
	</div>
</body>
</html>