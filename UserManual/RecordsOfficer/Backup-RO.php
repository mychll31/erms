<div id="sectiontitle">Backup</div>
<div id="sectionbody">
<ol>
<li>To access the Backup section, click the <strong>Backup</strong> tab (See Figure 3-53).</li>

<p align="center"><img src="images/Figure3-53Backuppage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-53 Backup page</p></div>

<p><div id="notebox"><strong>Note: </strong><em>By default, ERMS is scheduled to backup data once a week. But the user may change the frequency of the automated backup or may manually backup the database anytime.</em></div></p>
</ol>
</div>