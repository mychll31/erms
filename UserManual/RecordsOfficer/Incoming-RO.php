<div id="sectiontitle">Incoming</div>
<div id="sectionbody">
<p>This section contains documents received by the office (DOST-CO) from other agencies. To access the Incoming section, click the <strong>Incoming</strong> tab (See Figure 3-17).</p>

<p align="center"><img src="images/Figure3-17Incomingpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-17 Incoming page</p></div>

</div>
