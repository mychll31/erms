<div id="sectiontitle">How to Generate Report</div>
<div id="sectionbody">
<ol>
<li>	Depending on the type of report chosen, supply the parameter(s) it may require using the entry box(es) and drop-down calendar provided. It may be the date or document ID.</li>
<li>	When done, click <strong>Generate</strong> button <img src="images/GenerateButton.jpg" height="20"> .
</li>
</ol>
</div>