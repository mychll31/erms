<div id="sectiontitle">View History</div>
<div id="sectionbody">
<p>This section contains the information on document movement.</p>

<ol>
<li>To view history of documents, click the <strong>View History</strong> tab (See Figure 3-16).</li>

<p align="center"><img src="images/FIgure3-16ViewHistory.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Figure 3-16 View History</p></div>
</ol>
</div>