<div id="sectiontitle">Appendix C: XAMPP Installation and Configuration for Windows</div>
<div id="sectionbody">
<ol>
	<li>Make sure to have a XAMPP setup file. An installer is included in the ERMS Installation CD or you may download a more up-to-date version from the ApacheFriends website: <a href="https://www.apachefriends.org/index.html">https://www.apachefriends.org/index.html</a>. Run the XAMPP installer xampp-win32-X.exe → where X is the version number of your XAMPP installer.</li>

<br>
<p style="border-width:thin;
border-color:#003300;
border-style:dashed;"><strong>e.g.</strong> xampp-win32-1.4.12.exe</p>

<li>Choose the destination folder where you want XAMPP to be installed on. The default destination folder is C:\, but you may opt to install XAMPP in a directory of your choice.) You may also choose to install the Apache and MySQL servers as services, which will make them start automatically every time you start Windows. If you do not choose this option, you will need to use the XAMPP Control Panel application to start these servers individually each time you need them.</li>

<li>Upon completion of installation, the XAMPP Control Panel will open. (If not, click Start → All Programs → XAMPP for Windows →XAMPP Control Panel.) This allows you to start and stop the various servers installed as part of XAMPP.</li>

<p align="center"><img src="images/XAMPP Control Panel.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix C Figure 1-1 XAMPP Control Panel</p></div>

<li>Start Apache and MySQL by clicking the <b>Start</b> button <img src="images/Start button.jpg" height="18"> next to each item. You should be able to see the <b>highlighted module</b> adjacent to the services you started. If prompted by Windows Firewall, click the <b>Unblock</b> button.</li>

<p align="center"><img src="images/Starting Apache and MySQL from XAMPP Control Panel.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix C Figure 1-2 Starting Apache and MySQL from XAMPP Control Panel</p></div>

<li>Open your web browser (e.g. Internet Explorer, Mozilla Firefox). On the web browser’s address box, type <a href="http://localhost">http://localhost</a> or <a href="http://127.0.0.1">http://127.0.0.1</a>.</li>

<p align="center"><img src="images/Mozilla Firefox web browser.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix C Figure 1-3 Mozilla Firefox web browser</p></div>

<li>If your installation was successful, you will be directed to a page with the <b>XAMPP for Windows</b> title.</li>

<p align="center"><img src="images/XAMPP Splash Screen.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Appendix C Figure 1-4 XAMPP Splash Screen</p></div>

<li>To UNINSTALL XAMPP: Stop all running services (from XAMPP Control Panel), then simply delete the <b>xampp</b> folder created upon installation.</li>


</div>