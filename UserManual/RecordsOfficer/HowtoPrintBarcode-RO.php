<div id="sectiontitle">How to Print Document Barcode</div>
<div id="sectionbody">
<ol>
<li>To print document barcode, click <strong>Print Barcode</strong> button  <img src="images/PrintBarcodeButton.jpg" height="20"> in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-4DocumentDetails.jpg', 'Figure 3-4 Document Details');"> Figure 3-4</a>.</li>

<p align="center"><img src="images/Figure3-9SampleBarcode.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-9 Sample Barcode</p></div>

</ol>
<ul>
<li>Look for the <strong>Print</strong> icon <img src="images/PrintIcon.jpg" height="20">   on the application and click it.</li>
<li>Specify printing options will be prompted. Printing options vary depending on printer.</li>
<li>Click <strong>Ok</strong> to proceed.</li>
</ul>
</div>
