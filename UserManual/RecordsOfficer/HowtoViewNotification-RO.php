<div id="sectiontitle">How to View Notification</div>
<div id="sectionbody">

<ol>
<li>To view a notification, click the <strong>Notification</strong> icon   in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-70Notificationlink.jpg', 'Figure 3-70 Notification link');"> Figure 3-70</a> .</li>
<li>To view a particular notification, click the <strong>Document Subject</strong> link of the document to view (See Figure 3-71). The user will be directed to the folder that contains the desired document.

<p align="center"><img src="images/Figure3-71NotificationList.jpg" width="200" border="1"></p>
<div id="figurelabel"><p>Figure 3-71 Notification list</p></div>

<p><div id="notebox"><strong>Note: </strong><em>The number above the notification icon indicates the number of notifications received that are not viewed.</em></div></p>
<br />
<li>To view all notifications, click the<strong> View All Notification</strong> link.</li>
</ol>
</div>
