<div id="sectiontitle">How to Print Generated Report</div>
<div id="sectionbody">

<ol>

<li>Look for the <strong>Print</strong> icon <img src="images/PrintIcon.jpg" height="20">  on the application and click it. </li>
<li>You will be prompted to specify your printing options. Printing options vary depending on your printer.</li>
<li>Click <strong>Ok</strong> to proceed.</li>
</ol>
</div>