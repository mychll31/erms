<div id="sectiontitle">User Account</div>
<div id="sectionbody">
<ol>
<li>To access the User Account section, click the User Account tab (See Figure 3-61).</li>

<p align="center"><img src="images/Figure3-61UserAccountpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-61 User Account page</p></div>
</ol>
</div>