<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to Update Publication of Document</div>
	<div id="sectionbody">

	<ol>
		<li>
			To update publication of document, click the <b>Update Action</b> tab (See Figure below).
		</li>
				<p align="center"><img src="images/Update Action tab.jpg" width="550" border="1">
				</p>
		<li>
			Fill-in the entry boxes that constitute the parameters for updating action.
		</li>
		<li>
			Specify the <b>Action Taken</b> from the drop-down list.
		</li>	
		<li>
			Type in the <b>Publication Unit</b> (General Circulation, Bulletin Board, HOR-LLAM, NAP, Official Gazzette, UP Law Center) where the document will be published on the entry box or specify it from the drop-down list.
		</li>
		<li>
			Specify the <b>Action Needed</b> from the drop-down list.
		</li>
				<p>
					<div id="notebox"><strong>Note: </strong><em>Publication Unit for Action Taken and Action Needed must be <b>identical</b> (See Figure below).</em>
					</div>
				</p>
				<p align="center"><img src="images/Publication of Document.jpg" width="550" border="1">
				</p>
				<p>
					<div id="notebox"><strong>Note: </strong><em>Place a tick mark on the checkbox if an action/reply is required.</em>
					</div>
				</p>
		<li>
			Fill-in the <b>Date of Publication</b> by typing the date on the <b>Remarks</b> entry box provided (in the following format: MM-DD-YYYY).
		</li>
		<li>
			Place a tick mark on the <b>Restricted</b> checkbox if the action is restricted.
		</li>
		<li>
			When done, click <b>Submit</b> button <img src="images/SubmitBtn.jpg" height="20">.
		</li>

	</ol>
	
	</div>
</body>
</html>