<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to View and Print Acknowledgement Receipt</div>
	<div id="sectionbody">

	<p align="center"><img src="images/View History tab.jpg" width="620" height="335" border="1"></p>

	<ol>
		<li>
			To view acknowledgement receipt of the action taken, click the <b>Print</b> icon <img src='images/Print icon 1.jpg' width="20" height="20" /> shown on the View History tab. A new window will open showing the printable file.
			
			<p align="center"><img src="images/Acknowledgement Receipt.jpg" width="430" height="600" border="1"></p>
		</li>

		<li>
			To print the acknowledgement receipt, look for the <b>Print</b> icon <img src='images/Print icon.jpg' width="20" height="20" /> on the application and click it.
				<ol type="a">
					<li>Specify printing options that will be prompted. Printing options vary depending on the printer.</li>
					<li>Click <b>Ok</b> to proceed.</li>
				</ol>
		</li>

	</ol>
	</div>

</body>
</html>