<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button class="btnclose" onclick="window.close()">Close</button>
	<br>
	<style type="text/css">
		div#sectiontitle 
		{
			font-weight: bolder;
		}
		body 
		{
			padding: 43px;
		}
		.btnclose 
		{
		    padding: 5px 20px;
		    font-weight: bolder;
		    float: right;
		    position: absolute;
		    right: 1px;
		    top: 1px;
		}
	</style>

	<div id="sectiontitle">How to Unblock Pop-up in Printing Barcode</div>
	<div id="sectionbody">

	<ol>
		<li>
			To unblock pop-up in printing barcode, click the <b>Print Barcode</b> button <img src="images/PrintBarcodeButton.jpg" height="22"> shown on Document Details window first (See Figure below).
		</li>
				<p align="center"><img src="images/Document Details.png" width="650" border="1">
				</p>
		<li>
			Once clicked, a message prompt will appear stating that pop-ups were blocked on the current page.
		</li>
				<p align="center"><img src="images/Address bar.png" width="800" border="1">
				</p>
		<li>
			Click the <b>Pop-up</b> button shown on the address bar (See figure below).
		</li>	
				<p align="center"><img src="images/Message prompt.png" width="350" border="1">
				</p>
		<li>
			Place a tick mark on the ''<b>Always allow pop-ups from http://erms.dost.gov.ph</b>'' radio button to unblock pop-up in printing barcode.
		</li>
				<p align="center"><img src="images/Always allow pop-ups.png" width="450" border="1">
				</p>
		<li>
			Otherwise, ERMS will continue to block pop-ups appearing on the page.
		</li>
		<li>	
			When done, click <b>Done</b> button <img src="images/Done button.png" height="22">. 
		</li>
		<li>	
			You can also manage page settings by clicking <b>Manage</b> button <img src="images/Manage button.png" height="22">. 
		</li>
		<li>	
			This will open a new window showing allowed and blocked pop-ups (See figure below).
		</li>
				<p align="center"><img src="images/Settings page.png" width="650" height="300" border="1">
				</p>
				<p>
					<div id="notebox"><strong>Note: </strong><em>Lists below are the site/s allowing and blocking display of pop-ups.</em>
					</div>
				</p>
		<li>	
			Specify using a slider whether to allow or block pop-ups in appearing.
		</li>
				<p align="center"><img src="images/Slider.png" width="450" border="1">
				</p>

		<li>	
			When done, you will be able to print barcode without the interruption of pop-up.
		</li>

	</ol>
	
	</div>
</body>
</html>