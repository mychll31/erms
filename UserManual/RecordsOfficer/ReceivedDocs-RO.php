<div id="sectiontitle">Received Document</div>
<div id="sectionbody">
<p>This section allows the user to access functionalities on Documents Management regarding receiving of documents. To access the Received Documents section, click the <strong>Received Documents</strong> link on the left side of the screen (See Figure 3-25).</p>

<p align="center"><img src="images/Figure3-25ReceivedDocumentspage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-25 Received Documents page</p></div>

<p><div id="notebox"><strong>Note: </strong><em>The incoming documents are displayed with bold font style for emphasis. This means that these documents are yet to be formally received. The user has to click the document ID to receive the document properly in order to apply necessary actions to it.</em></div></p>
</div>
