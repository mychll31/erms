<div id="sectiontitle">Office</div>
<div id="sectionbody">
<ol>
<li>To access the Office section, click the <strong>Office</strong> tab (See Figure 3-37).</li>

<p align="center"><img src="images/Figure3-37Officepage.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-37 Office page</p></div>
</ol>
</div>