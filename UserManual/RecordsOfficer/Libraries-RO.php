<div id="sectiontitle">Libraries</div>
<div id="sectionbody">
<p>This section serves as storage of information which can be used as options for users when using ERMS, especially when adding incoming and outgoing documents. To access the Libraries section, click the <strong>Libraries</strong> link on the left side of the screen (See Figure 3-36). </p>

<p align="center"><img src="images/Figure3-36Librariespage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-36 Libraries page</p></div>

<p><div id="notebox"><strong>Note: </strong><em>By default, Office sub-section is shown on the Libraries page. To view other sub-sections under Libraries, refer to Section 3.3.2-Section 3.3.9.</em></div></p>
</div>