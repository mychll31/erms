<div id="sectiontitle">How to Sort Documents</div>
<div id="sectionbody">
<ol>
<li>To sort documents, specify the <strong>Office</strong> and/or <strong>Status</strong> from the drop-down lists in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-2DocumentListpage.jpg', 'Figure 3-2 Document List page');"> Figure 3-2</a>.</li>
</ol>
</div>