<div id="sectiontitle">How to Add New Action Required Details</div>
<div id="sectionbody">

<ol>
<li>To add new Action Required details, fill-up the entry boxes that constitutes to the parameters for adding new <strong>Action Required</strong> details in Figure 3-48.</li>

<p align="center"><img src="images/Figure3-48AddNewActionRequired.jpg" width="337" border="1"></p>
<div id="figurelabel"><p>Figure 3-48 Add New Action Required</p></div>

<li>Type in the <strong>Action Required Code</strong> and <strong>Action Required</strong> Description on the entry boxes provided.</li>

<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>