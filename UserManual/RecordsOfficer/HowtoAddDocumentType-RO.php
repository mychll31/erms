<div id="sectiontitle">How to Add New Document Type Details</div>
<div id="sectionbody">

<ol>
<li>To add new Document Type details, fill-up the entry boxes that constitutes to the parameters for adding new Document Type details in Figure 3-45.</li>

<p align="center"><img src="images/Figure3-45AddDocumentTypeform.jpg" width="340" border="1"></p>
<div id="figurelabel"><p>Figure 3-45 Add Document Type form</p></div>

<li>Type in the <strong>Document Type Abbreviation</strong>, <strong>Document Type Description</strong> and <strong>Retention Period</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>