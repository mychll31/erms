<div id="sectiontitle">How to Login</div>
<div id="sectionbody">

<p><div id="notebox"><strong>Note: </strong><em>User accounts from DOST-CO HRMIS shall be used to log into the system.</em></div></p>

<ol>
<li>Enter <strong>Username</strong> and <strong>Password</strong> on the entry boxes labeled respectively. Tick the <strong>Remember Me</strong> check box to remember username next time you login. Then click <strong>Login</strong> button  <img src="images/LoginButton.jpg" height="20">  (See Figure 1-2).</li>

<p align="center"><img src="images/Figure1-2Loginform.jpg" width="227"></p>
<div id="figurelabel"><p>Figure 1-2 Login Form</p></div>

<li>If login is successful, you will be directed to the <a id= "link" onClick="getData('DefaultScreen-RO.php','maincontent');"> Default Screen</a>.</li>

</ol>
</div>