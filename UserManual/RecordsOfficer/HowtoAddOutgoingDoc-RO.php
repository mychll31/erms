<div id="sectiontitle">How to Add Outgoing Document</div>
<div id="sectionbody">
<ol>
<li>To add outgoing documents, fill-up the parameters that constitute in adding outgoing documents in Figure 3-22.</li>

<p align="center"><img src="images/Figure3-22AddOutgoingDocumentform.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Figure 3-22 Add Outgoing Document form</p></div>

<li>Type in the Document Type, Reference ID, Doc No. and Subject on the entry boxes provided.</li>

<li>If the desired Document Type is not listed, click the <strong>Add</strong> icon  <img src="images/AddIcon.jpg" height="20"> fill-up the entry boxes that constitute the parameters for adding new Document Type in Figure 3-23.</li>

<p align="center"><img src="images/Figure3-7AddNewDocTypewindow.jpg" width="350" border="1"></p>
<div id="figurelabel"><p>Figure 3-23 Add New DocType window</p></div>

<li>Type in the <strong>Document Type Abbreviation</strong>, <strong>Document Type Description</strong> and <strong>Retention Period</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button   <img src="images/AddButton.jpg" height="20"> to save entry or <strong>Clear</strong> button   <img src="images/ClearButton_Blue.jpg" height="20"> to reset parameters.</li>
<br /><br />
<li>Fill-up the <strong>Document Date</strong> and <strong>Deadline</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>

<li>Specify the <strong>Origin</strong> and <strong>Group</strong> from the drop-down lists.</li>
<li>Type in the <strong>Name of Signatory</strong> and <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>Place a tick mark on the <strong>Confidentia</strong>l checkbox if the document is confidential.</li>
<li>Specify the <strong>File Container</strong> and <strong>Document Owner</strong> from the drop-down lists.</li>
<li>When done, click <strong>Save</strong> button  <img src="images/SaveButton1.jpg" height="20"> to save entry or <strong>Clear</strong> button  <img src="images/ClearButton.jpg" height="20"> to reset parameters.</li>
</ol>
</div>
