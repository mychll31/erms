<div id="sectiontitle">How to Receive Document Using Barcode Scanner</div>
<div id="sectionbody">
<ol>
	<li>To receive document using barcode scanner, scan the document barcode using the device - place the red light directly over the middle of the barcode. </li>
		<p align="center"><img src="images/barcode scanner 2.jpg" width="385" height="335" border="1"></p>
	<li>Once scanned, a confirmation message will be displayed if the action is successful. User will be directed to the document details.</li>

</ol>
</div>
