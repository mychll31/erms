<div id="sectiontitle">How to Delete Backup File</div>
<div id="sectionbody">
<ol>
<li>To delete backup file, click the <strong>Delete</strong> icon  corresponding the backup file to delete.</li>
<li>A message prompt will appear confirming the deletion. Click the <strong>OK</strong> button to proceed or <strong>Cancel</strong> button to cancel operation. A confirmation message will be displayed if the action is successful.</li>
</ol>
</div>
