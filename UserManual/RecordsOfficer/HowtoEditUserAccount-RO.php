<div id="sectiontitle">How to Edit Existing User Account</div>
<div id="sectionbody">
<ol>
<li>To edit existing User Account, select a row corresponding to the User Account to edit then click the <strong>Edit</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20"> (See Figure 3-63).</li>

<p align="center"><img src="images/Figure3-63EditUserAccountform.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-63 Edit User Account form</p></div>

<li>Information that can be updated includes <strong>ID Numbe</strong>r, <strong>First Name</strong>, <strong>Middle Initial</strong>, <strong>Surname</strong>,<strong> Name Extension</strong>,<strong> Office</strong>, <strong>Username</strong> and <strong>Password</strong>.</li>

<li>When done, click <strong>Save</strong> button<img src="images/SaveButton.jpg" height="20">    to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton_blue.jpg" height="20">  to cancel operation.
</li>
</ol>
</div>