<div id="sectiontitle">How to Add New Group Details</div>
<div id="sectionbody">
<ol>
<li>To add new Group details, fill-up the entry boxes that constitute to the parameters for adding new Group details in Figure 3-59.</li>

<p align="center"><img src="images/Figure3-59AddGroupform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-59 Add Group form</p></div>

<li>Specify the <strong>Office</strong> from the drop-down list.</li>
<li>Type in the <strong>Group Code</strong> and <strong>Group Description</strong> on the entry boxes provided.
</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>