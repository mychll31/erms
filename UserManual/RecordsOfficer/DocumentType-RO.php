<div id="sectiontitle">Document Type</div>
<div id="sectionbody">
<ol>
<li>To access the Document Type section, click the <strong>Document Type</strong> tab (See Figure 3-44).</li>

<p align="center"><img src="images/Figure3-44DocumentTypepage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-44 Document Type page</p></div>

</ol>
</div>