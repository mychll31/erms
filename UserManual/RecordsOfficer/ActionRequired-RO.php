<div id="sectiontitle">Action Required</div>
<div id="sectionbody">
<ol>
<li>To access the Action Required section, click the <strong>Action Required</strong> tab (See Figure 3-47).</li>

<p align="center"><img src="images/Figure3-47ActionRequiredpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-47 Action Required page</p></div>

</ol>
</div>