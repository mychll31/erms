<div id="sectiontitle">How to Add Custodian Details</div>
<div id="sectionbody">
<ol>
<li>To add new Custodian details, fill-up the entry boxes that constitutes to the parameters for adding new Custodian details in Figure 3-56.</li>

<p align="center"><img src="images/Figure3-56AddCustodianform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-56 Add Custodian Details form</p></div>

<li>Specify the <strong>Office</strong>, <strong>Office Groups</strong> and <strong>Custodian Name</strong> from the drop down lists.</li>
<li>Type in the<strong> Email Address</strong> on the entry box provided.</li>
<li>Place a tick mark on the <strong>Admin </strong>checkbox if assigning employee as System Administrator.</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>