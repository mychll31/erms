<div id="sectiontitle">Delete Document</div>
<div id="sectionbody">
<ol>
<li>To delete a document, click the <strong>Delete</strong> icon <img src="images/DeleteIcon.jpg" height="20">   in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-2DocumentListpage.jpg', 'Figure 3-2 Document List page');"> Figure 3-2</a>.</li>
<li>The deleted documents will be moved to the <strong>Trash</strong> Folder.</li>
</ol>
</div>