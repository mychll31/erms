<div id="sectiontitle">How to Save Generated Report</div>
<div id="sectionbody">
<ol>
<li>Press Ctrl+Shift+S or look for the <strong>Save</strong> icon   <img src="images/SaveIcon.jpg" height="20"> on the application and click it.</li>
<li>You will be prompted to enter the name of the file. Specify directory path and type desired name for the file.</li>
<li>Click <strong>Save</strong> to proceed.</li>
</ol>
</div>