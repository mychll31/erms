<div id="sectiontitle">How to Edit Existing Office Details</div>
<div id="sectionbody">
<ol>
<li>To edit existing Office details, select a row corresponding to the Office details to edit then click the <strong>Edit</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20">  (See Figure 3-36).</li>

<p align="center"><img src="images/Figure3-36EditOfficeform.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-36 Edit Office form</p></div>

<li>Information that can be updated includes <strong>Office Name</strong>, <strong>Contact Number</strong>, <strong>Address</strong> and <strong>Contact Person</strong>.</li>
<li>When done, click <strong>Save</strong> button<img src="images/SaveButton.jpg" height="20">    to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton_blue.jpg" height="20">  to cancel operation.
</li>
</ol>
</div>