<div id="sectiontitle">How to Undo an Action Taken</div>
<div id="sectionbody">
<ol>
<li>To undo an action taken to a document, click the <strong>Undo Action</strong> icon <img src="images/UndoActionIcon.jpg" height="20">  .</li>
<li>A message prompt will appear confirming the action deletion. Click the <strong>OK</strong> button to proceed or <strong>Cancel</strong> button to cancel operation. A confirmation message will be displayed if the action is successful.</li>

<p><div id="notebox"><strong>Note: </strong><em>The system disables the "Undo Action" icon once a reply was made.</em></div></p>

</ol>
</div>