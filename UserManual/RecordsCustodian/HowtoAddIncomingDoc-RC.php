<div id="sectiontitle">How to Add Incoming Document</div>
<div id="sectionbody">

<ol>
<li>To add incoming documents, fill-up the parameters that constitute in adding incoming documents in Figure 3-17.</li>

<p align="center"><img src="images/Figure3-17AddIncomingDocumentform.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Figure 3-17 Add Incoming Document form</p></div>

<li>Type in the <strong>Document Type</strong>, <strong>Reference ID</strong>,<strong> Doc No.</strong> and <strong>Subject</strong> on the entry boxes provided.</li>

<li>Fill-up the <strong>Date Received</strong> and <strong>Document Date</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>
<li>To specify origin, choose between <strong>Office</strong> and <strong>Agency</strong> then supply other details by specifying it from the drop-down lists. </li>

<li>If the desired Agency is not listed on the drop-down list, click the <strong>Add</strong> icon    <img src="images/AddIcon.jpg" height="20">  fill-up the entry boxes that constitute the parameters for adding new Agency in Figure 3-18.</li>

<p align="center"><img src="images/Figure3-7AddNewAgencywindow.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-18 Add New Agency window</p></div>

<li>Type in the <strong>Office Name</strong>, <strong>Contact Number</strong>, <strong>Address</strong> and <strong>Contact Person</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button  <img src="images/AddButton.jpg" height="20">   to save entry or <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20"> to reset parameters.</li>
<br>
<br>
<li>Type in the <strong>Sender Name</strong> on the entry box provided.</li>
<li>Fill-up the <strong>Deadline</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>
<li>Specify the <strong>File Container</strong> and <strong>Document Owner</strong> from the drop-down lists.</li>
<li>Place a tick mark on the <strong>Confidential</strong> checkbox if adding a confidential document.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>When done, click <strong>Save</strong> button  <img src="images/SaveButton1.jpg" height="20"> to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton.jpg" height="20"> to cancel operation or <strong>Clear</strong> button  <img src="images/ClearButton.jpg" height="20"> to reset parameters.</li>
<li>A confirmation message will be displayed if the action is successful. 
</li>

</ol>
</div>
