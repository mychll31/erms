<div id="sectiontitle">How to Manage Folders</div>
<div id="sectionbody">
<ol>
<li>To manage folders, click the <strong>Manage Folders</strong> link in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-22ReceivedDocumentspage.jpg', 'Figure 3-22 Received Document page');"> Figure 3-22</a> (See Figure 3-33).</li>

<p align="center"><img src="images/Figure3-30ManageFolderpage.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-30 Manage Folders page</p></div>

<li>To create new folder, type in the <strong>Folder Name</strong> on the entry box provided.</li>
<li>When done, click <strong>Create</strong> button <img src="images/CreateButton.jpg" height="19"> .</li>
<br><br>
<li>To rename existing folder, click the <strong>Rename</strong> link opposite the corresponding folder to rename (See Figure 3-31).</li>

<p align="center"><img src="images/Figure3-31FolderRename.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-31 Rename Folder form</p></div>

<li>Type in the <strong>New Folder Name</strong> on the entry box provided.</li>
<li>When done, click <strong>Rename</strong> button <img src="images/RenameButton.jpg" height="17">  to rename folder or the <strong>Cancel</strong> button  <img src="images/CancelButton_white.jpg" height="17"> to cancel operation.</li>
<br><br>
<li>To delete existing folder, click the <strong>Delete</strong> link opposite the corresponding folder to delete (See Figure 3-32).</li>

<p align="center"><img src="images/Figure3-32DeleteFolder.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-32 Delete Folder form</p></div>
<li>Click the <strong>Delete</strong> button <img src="images/DeleteButton.jpg"> to delete folder or <strong>Cancel</strong> button <img src="images/CancelButton_white.jpg" height="17"> to cancel operation.</li>
</ol>
</div>