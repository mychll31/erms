<div id="sectiontitle">How to Search a Document</div>
<div id="sectionbody">
<ol>
<li>To search for a particular document, type in the <strong>DOCID</strong> or <strong>Document Subject</strong> on the search box located at the upper right side of the document list in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-2DocumentListpage.jpg', 'Figure 3-2 Document List page');"> Figure 3-2</a>.</li>
<li>When done, click <strong>Search</strong> button <img src="images/SearchButton.jpg" height="20"> .</li>

<p align="center"><img src="images/Figure3-3SampleSearchResults.jpg" width="525"></p>
<div id="figurelabel"><p>Figure 3-3 Sample Search Result</p></div>

<p><div id="notebox"><strong>Note: </strong><em>Words that matched the search keywords are highlighted.</em></div></p>

</ol>
</div>