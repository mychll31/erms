<div id="sectiontitle">Outgoing</div>
<div id="sectionbody">
<p>This section contains documents that originated from the office (DOST-CO) to other agencies. To access the Outgoing section, click the <strong>Outgoing</strong> tab (See Figure 3-19).</p>

<p align="center"><img src="images/Figure3-19Outgoingpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-19 Outgoing page</p></div>

</div>