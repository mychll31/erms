<div id="sectiontitle">How to Upload Digitized Files</div>
<div id="sectionbody">
<ol>
<li>To upload digitized documents, click <strong>Add New</strong> button  <img src="images/AddNewButton.jpg" height="20">  in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-9ManageFiles.jpg', 'Figure 3-9 Manage Files');"> Figure 3-9</a> (See Figure 3-10).</li>

<p align="center"><img src="images/Figure3-10AttachFileswindow.jpg" width="300"></p>
<div id="figurelabel"><p>Figure 3-10 Attach Files Window</p></div>

<li>To select files to upload, click <strong>Browse</strong> button  <img src="images/BrowseButton.jpg" height="20"> in Figure 3-10.  </li>
<li>When done, click <strong>Start Upload</strong> button  <img src="images/StartUploadButton.jpg" height="17"> to start uploading selected files or <strong>Cancel</strong> button  <img src="images/CancelButton_white.jpg" height="17"> to cancel operation (See Figure 3-11). </li>

<p align="center"><img src="images/Figure3-11Uploadwindow.jpg" width="300" /></p>
<div id="figurelabel"><p>Figure 3-11 Upload window</p></div>

</ol>
</div>