<div id="sectiontitle">Cabinet</div>
<div id="sectionbody">
<ol>
<li>To access the Cabinet section, click the <strong>Cabinet</strong> tab (See Figure 3-38).</li>

<p align="center"><img src="images/Figure3-38Cabinetpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-38 Cabinet page</p></div>
</ol>
</div>