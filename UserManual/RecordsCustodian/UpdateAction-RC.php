<div id="sectiontitle">Update Action</div>
<div id="sectionbody">
<p>This section allows user to update the document movement by adding actions to the document.</p>

<ol>
<li>To update action of documents, click the <strong>Update Action</strong> tab (See Figure 3-12).</li>

<p align="center"><img src="images/Figure3-12UpdateAction.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-12 Update Action</p></div>

</ol>
</div>