<div id="sectiontitle">Libraries</div>
<div id="sectionbody">
<p>This section serves as storage of information which can be used as options for users when using ERMS, especially when adding incoming and outgoing documents. To access the Libraries section, click the <strong>Libraries</strong> link on the left side of the screen (See Figure 3-33). </p>

<p align="center"><img src="images/Figure3-33Librariespage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-33 Libraries page</p></div>

</div>