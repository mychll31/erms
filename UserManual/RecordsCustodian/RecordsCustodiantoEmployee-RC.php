<div id="sectiontitle">Switch from Records Custodian to Employee</div>
<div id="sectionbody">
<p>This section allows the Records Custodian to switch to the Employee Module of his account. To access the Employee Module section, click the <strong>Employee Module</strong> link on the left side of the screen (See Figure 3-45).</p>

<p align="center"><img src="images/Figure3-45EmployeeModulepage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-45 Employee Module page</p></div>

</div>