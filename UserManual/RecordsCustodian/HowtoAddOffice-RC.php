<div id="sectiontitle">How to Add New Office Details</div>
<div id="sectionbody">
<ol>
<li>To add new Office details, fill-up the entry boxes that constitutes to the parameters for adding new Office details in Figure 3-35.</li>

<p align="center"><img src="images/Figure3-35AddOfficeform.jpg" width="296" border="1"></p>
<div id="figurelabel"><p>Figure 3-35 Add Office form</p></div>

<li>Type in the <strong>Office Name</strong>, <strong>Contact Number</strong>,<strong> Address</strong> and <strong>Contact Person</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>