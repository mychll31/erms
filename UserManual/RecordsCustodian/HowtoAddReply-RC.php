<div id="sectiontitle">How to Add Reply to an Action Taken</div>
<div id="sectionbody">
<ol>
<li>To add reply to action/s taken to a document, click the <strong>Add Reply</strong> icon <img src="images/AddActionReplyIcon.jpg" height="20">   corresponding to action taken (See Figure 3-14).</li>

<p align="center"><img src="images/Figure3-14AddActionReply.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-14 Add Action Reply</p></div>


<li>Specify the <strong>Action Taken</strong> from the drop-down list.</a>.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency </strong>on the <strong>Action Unit</strong> entry box provided.</li>
<li>Specify the <strong>Action Needed</strong> from the drop-down list.</li>
<li>Place a tick mark on the <strong>Required</strong> checkbox if reply/action from recipient is expected.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted. For restrictions on documents, see  <a id= "link" onClick="getData('AppendixA-RC.php','maincontent');">APPENDIX A: Document Restrictions</a>.</li>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitBtn.jpg" height="20"> .</li>
</ol>
</div>
