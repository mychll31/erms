<div id="sectiontitle">How to Dispose Documents</div>
<div id="sectionbody">
<ol>
<li>To dispose documents, click the <strong>Dispose Document</strong> icon  <img src="images/DisposeDocumentIcon.jpg" height="20"> opposite the document or place a tick mark on the checkbox corresponding to the document/s to dispose then click the Dispose Document icon (See Figure 3-42).</li>

<p align="center"><img src="images/Figure3-42DisposeDocumentwindow.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-42 Dispose Document window</p></div>

<li>Specify the <strong>Dispose Remark</strong> from the drop-down list.</li>
<li>When done, click <strong>Dispose</strong> button <img src="images/DisposeButton.jpg"> .</li>
</ol>
</div>