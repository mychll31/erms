<div id="sectiontitle">How to Flag a Received Document</div>
<div id="sectionbody">
<ol>
<li>To flag document/s, place a tick mark on the checkbox corresponding to the document/s to flag in the received document list in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-22ReceivedDocumentspage.jpg', 'Figure 3-22 Received Document page');"> Figure 3-22</a>.</li>
<li>When done, click the <strong>Flag</strong> icon   <img src="images/FlagIcon.jpg" height="20"> located at the upper-left side of the received document list.</li>
<li>A flag image shall suffice at the left side of the document chosen, beside the checkbox (See Figure 3-27).</li>

<p align="center"><img src="images/figure3-27sampleflaggeddocument.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-27 Sample Flagged Document</p></div>

</ol>
</div>