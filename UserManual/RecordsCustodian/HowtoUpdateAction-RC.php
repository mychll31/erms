<div id="sectiontitle">How to Update Action of a Document</div>
<div id="sectionbody">
<ol>
<li>To update action of a document, specify the <strong>Action Taken</strong> from the drop-down list in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-12UpdateAction.jpg', 'Figure 3-12 Update Action');"> Figure 3-12</a>.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency </strong>on the <strong>Action Unit</strong> entry box provided.</li>
<li>Specify the <strong>Action Needed</strong> from the drop-down list.</li>
<li>Place a tick mark on the <strong>Required</strong> checkbox if reply/action from recipient is expected.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted. For restrictions on documents, see  <a id= "link" onClick="getData('AppendixA-RC.php','maincontent');">APPENDIX A: Document Restrictions</a>.</li>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitBtn.jpg" height="20"> . Action made to the document will be displayed in the Update Action tab.</li>

<p align="center"><img src="images/Figure3-13UpdateDocumentAction.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Figure 3-13 Update Document Action</p></div>
</ol>
</div>