<div id="sectiontitle">How to Add New Document</div>
<div id="sectionbody">
<ol>
<li>To add new document, click Add New button   in  <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-4DocumentDetails.jpg', 'Figure 3-4 Document Details');"> Figure 3-4</a>  (See Figure 3-6).</li>

<p align="center"><img src="images/Figure3-6AddNewInformationform.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-6 Documents page</p></div>

<li>Fill-up the entry boxes that constitute to the parameters for adding new document.</li>

<p><div id="notebox"><strong>Note: </strong><em>The system generates auto-incrementing Doc ID.</em></div></p>

<li>Type in the <strong>Document Type</strong>,<strong> Reference ID</strong>,<strong> Doc No.</strong> and <strong>Subject</strong> on the entry boxes provided.
</li><li>Fill-up the <strong>Date Received</strong> and <strong>Document Date</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>
<li>To specify origin, choose between <strong>Office</strong> and <strong>Agency</strong> then supply other details by specifying it from the drop-down lists. </li>
<li>If the desired Agency is not listed on the drop-down list, click the <strong>Add</strong> icon    <img src="images/AddIcon.jpg" height="20">  fill-up the entry boxes that constitute the parameters for adding new Agency in Figure 3-7.</li>

<p align="center"><img src="images/Figure3-7AddNewAgencywindow.jpg" width="350" border="1"></p>
<div id="figurelabel"><p>Figure 3-7 Add New Agency window</p></div>

<li>Type in the <strong>Office Name</strong>, <strong>Contact Number</strong>, <strong>Address</strong> and <strong>Contact Person</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Add</strong> button  <img src="images/AddButton.jpg" height="20">   to save entry or <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20"> to reset parameters.</li>

<br /><br />
<li>Type in the <strong>Sender Name</strong> on the entry box provided.</li>
<li>Fill-up the <strong>Deadline</strong> by typing the date on the textbox (in the following format: YYYY-MM-DD) or by clicking on the drop-down calendar.</li>
<li>Specify the <strong>File Container</strong> and <strong>Document Owner</strong> from the drop-down lists.</li>
<li>Place a tick mark on the <strong>Confidential</strong> checkbox if adding a confidential document.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>When done, click <strong>Save</strong> button  <img src="images/SaveButton1.jpg" height="20"> to save entry or <strong>Cancel</strong> button  <img src="images/CancelButton.jpg" height="20"> to cancel operation or <strong>Clear</strong> button  <img src="images/ClearButton.jpg" height="20"> to reset parameters.</li>
<li>A confirmation message will be displayed if the action is successful. 
</li>

</ol>
</div>