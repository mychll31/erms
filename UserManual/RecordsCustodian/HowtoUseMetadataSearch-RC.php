<div id="sectiontitle">How to Use Metadata Search</div>
<div id="sectionbody">
<ol>
<li>To use metadata search, specify the <strong>Metadata</strong> (e.g. Subject, Document Type, Status, Deadline) from the drop-down lists in <a href="javascript:void(0)" onclick="loadpopup('../RecordsOfficer/images/Figure3-49AdvancedSearchpage.jpg', 'Figure 3-49 Advanced Search page');"> Figure 3-49</a>.</li>
<li>Type the search <strong>Keyword</strong>/<strong>s</strong> on the entry boxes provided.</li>
<li>When done, click <strong>Search</strong> button  <img src="images/SearchButton_white.jpg" height="20"> (See Figure 3-50).</li>

<p align="center"><img src="images/Figure3-50SampleSearchResults.jpg" width="500" border="1"></p>
<div id="figurelabel"><p>Figure 3-50 Sample Search Results</p></div>

</ol>
</div>