<div id="sectiontitle">Disposals</div>
<div id="sectionbody">
<p>This section allows user to access functionalities regarding disposal of documents that reached the end of their retention period. To access the Disposals section, click the <strong>Disposals</strong> link on the left side of the screen (See Figure 3-41).</p>

<p align="center"><img src="images/Figure3-41Disposalspage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-41 Disposals page</p></div>

</div>