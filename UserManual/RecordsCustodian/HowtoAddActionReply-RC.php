<div id="sectiontitle">How to Add Action Reply</div>
<div id="sectionbody">
<ol>
<li>To add action reply, click the <strong>Update Action</strong> tab then click the <strong>Add Action Reply</strong> icon  <img src="images/AddActionReplyIcon.jpg" height="20"> (See Figure 3-24).</li>

<p><div id="notebox"><strong>Note: </strong><em>The "For Actions" table lists the actions needed to be performed by the recipient to the document.</em></div></p>

<p><div id="notebox"><strong>Note: </strong><em>The "Actions Made" table lists the actions performed by the recipient to the document.</em></div></p>

<p align="center"><img src="images/Figure3-14AddActionReply.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-24 Add Action Reply</p></div>


<li>Specify the <strong>Action Taken</strong> from the drop-down list.</a>.</li>
<li>Type in the <strong>Employee</strong>, <strong>Office</strong> or <strong>Agency </strong>on the <strong>Action Unit</strong> entry box provided.</li>
<li>Specify the <strong>Action Needed</strong> from the drop-down list.</li>
<li>Place a tick mark on the <strong>Required</strong> checkbox if reply/action from recipient is expected.</li>
<li>Type in <strong>Remarks</strong> (if there is any) on the entry box provided.</li>
<li>Place a tick mark on the <strong>Restricted</strong> checkbox if the action is restricted. For restrictions on documents, see  <a id= "link" onClick="getData('AppendixA-RC.php','maincontent');">APPENDIX A: Document Restrictions</a>.</li>
<li>When done, click <strong>Submit</strong> button <img src="images/SubmitBtn.jpg" height="20"> .</li>

<p><div id="notebox"><strong>Note: </strong><em>If an action is not made to a document that requires a reply, the document will be considered "Unacted".</em></div></p>

</ol>
</div>
