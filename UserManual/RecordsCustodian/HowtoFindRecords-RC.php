<div id="sectiontitle">How to Find Records</div>
<div id="sectionbody">
<ol>
<li>To find records, click the <strong>Find Records</strong> icon  <img src="images/FindRecords.jpg" height="20"> below the grid (See Figure 3-37).</li>

<p align="center"><img src="images/Figure3-37Searchwindow.jpg" width="400" border="1"></p>
<div id="figurelabel"><p>Figure 3-37 Search window</p></div>

<li>Specify the <strong>Category to Search</strong> and the <strong>Parameter</strong> in the drop-down list.</li>
<li>Type in the <strong>Keyword</strong> in the Search textbox.</li>
<li>When done, click <strong>Find</strong> button <img src="images/FindButton.jpg" height="20">  to start searching or <strong>Reset</strong> button <img src="images/ResetButton.jpg" height="20"> to reset parameters.</li>
</ol>
</div>
