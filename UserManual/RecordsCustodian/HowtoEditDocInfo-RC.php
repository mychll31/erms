<div id="sectiontitle">How to Edit Document Information</div>
<div id="sectionbody">
<ol>
<li>To edit document information, click <strong>Edit Info</strong> button  <img src="images/EditInfoButton.jpg" height="20"> in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-4DocumentDetails.jpg', 'Figure 3-4 Document Details');"> Figure 3-4</a> (See Figure 3-5).</li>

<p align="center"><img src="images/Figure3-5EditInformationform.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-5 Edit Information form</p></div>

<p><div id="notebox"><strong>Note: </strong><em>Editing depends on the status of the document (Incoming or Outgoing). Some information may not be subject to modification.</em></div></p>

<li>Information that can be updated includes <strong>Document Type</strong>,<strong> Reference ID</strong>, <strong>Doc No.</strong> and <strong>Subject</strong> among others.</li>
<li>When done, click <strong>Update</strong> button  <img src="images/UpdateButton.jpg" height="20"> to save entry or <strong>Cancel</strong> button <img src="images/CancelButton.jpg" height="20"> to cancel operation or <strong>Clear </strong>button  <img src="images/ClearButton.jpg" height="20"> to reset parameters.</li>
<li>A confirmation message will be displayed if the action is successful. </li>
</ol>
</div>