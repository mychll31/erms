<div id="sectiontitle">How to Add New Cabinet Details</div>
<div id="sectionbody">
<ol>
<li>To add new Cabinet details, fill-up the entry boxes that constitutes to the parameters for adding new Cabinet details in Figure 3-39.</li>

<p align="center"><img src="images/Figure3-39AddCabinetform.jpg" width="290" border="1"></p>
<div id="figurelabel"><p>Figure 3-39 Add Cabinet form</p></div>

<li>Type in the <strong>Cabinet Label</strong> on the entry box provided.</li>
<li>When done, click <strong>Add</strong> button <img src="images/AddButton_blue.jpg" height="20">  to save entry or the <strong>Clear</strong> button  <img src="images/ClearButton_Blue.jpg" height="20">to reset parameters.
</li>
</ol>
</div>