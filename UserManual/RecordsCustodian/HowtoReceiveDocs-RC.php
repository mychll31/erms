<div id="sectiontitle">How to Receive Documents</div>
<div id="sectionbody">
<ol>
<li>To receive a document, click on the <strong>Document ID</strong> (<strong>DOCID</strong>) link of the specific document. </li>

<p><div id="notebox"><strong>Note: </strong><em>Only those documents displayed with bold font style can be received.</em></div></p>

<li>Type in the <strong>Receiver&#39;s Name</strong> on the <strong>Received by:</strong> entry box on the window that will open (See Figure 3-23).</li>

<p align="center"><img src="images/Figure3-23ReceiveDocumentwindow.jpg" width="300" border="1"></p>
<div id="figurelabel"><p>Figure 3-23 Receive Document window</p></div>

<li>When done, click <strong>Receive</strong> button   <img src="images/ReceiveButton.jpg" height="20"> or <strong>Close</strong> button   <img src="images/CloseButton.jpg" height="21">  to cancel operation.</li>
<li>A confirmation message will be displayed if the action is successful. You will be directed to the document details.</li>
</ol>
</div>