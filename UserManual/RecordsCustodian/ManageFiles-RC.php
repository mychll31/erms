<div id="sectiontitle">Manage Files</div>
<div id="sectionbody">
<p>This section contains the uploaded digitized copy of the document.</p>
<ol>
<li>To manage files associated with a document, click the <strong>Manage Files</strong> tab (See Figure 3-9).</li>

<p align="center"><img src="images/Figure3-9ManageFiles.jpg" width="450" border="1"></p>
<div id="figurelabel"><p>Figure 3-9 Manage Files</p></div>
</ol>
</div>