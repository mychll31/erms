<div id="sectiontitle">Trash</div>
<div id="sectionbody">
<p>This section contains a list of documents records that for some reason were deleted or removed from the document list. To access the Trash section, click the <strong>Trash</strong> link on the left side of the screen (See Figure 3-43).</p>

<p align="center"><img src="images/Figure3-43Trashpage.jpg" width="525" border="1"></p>
<div id="figurelabel"><p>Figure 3-43 Trash page</p></div>
</div>