<div id="sectiontitle">Upload Digitized File to an Existing Document</div>
<div id="sectionbody">
<ol>
<li>To upload digitized file to an existing document, click the <strong>Upload Document</strong> icon <img src="images/UploadIcon.jpg" height="20">  in <a href="javascript:void(0)" onclick="loadpopup('../RecordsCustodian/images/Figure3-2DocumentListpage.jpg', 'Figure 3-2 Document List page');"> Figure 3-2</a>.</li>
<li>Refer to <a id= "link" onClick="getData('HowtoUploadDigitizedFiles-RC.php','maincontent');"> Section 3.1.1.4.1 How to Upload Digital Files</a> of this manual.</li>
</ol>
</div>
