<div id="sectiontitle">Login</div>
<div id="sectionbody">
<p>To login to the system, the user is required to enter a valid username and password on the Login Form at the center of the Main Page (See Figure 1-1).</p>

<p align="center"><img src="../RecordsCustodian/images/Figure1-1DOST-COERMSMainPage.jpg" width="525"></p>
<div id="figurelabel"><p>Figure 1-1 DOST-CO ERMS MainPage</p></div>
</div>
