<p class="big"><strong>Mark Document as Priority</strong></p>

<p>
<ul>
	<li>From the Received Documents screen, select the document you want to mark as priority by ticking the checkbox (located at the left part of the document record). Then click the Priority icon <img src="images/MarkDocument1.png" /> (located at the top of the table/grid).<br>
	<p align="center"><img src="images/MarkDocument2.png" width="400" /></p>
	</li>
	<li>An exclamation point image shall suffice at the left side of the document chosen, beside the checkbox.<br>
	<p align="center"><img src="images/MarkDocument3.png" width="350" /></p>
	</li>
</ul>
</p>



