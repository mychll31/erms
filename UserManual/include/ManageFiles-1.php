<p class="big"><strong>Manage Files</strong></p>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="boxyellow">
<tr>
	<td width="50%" align="center" style="border-right:thin #FFFF00 solid"><a href="#UDF">Uploading Digitized Files</a></td>
	<td width="50%" align="center"><a href="#VUDF">Viewing Uploaded Digitized Files</a></td>
</tr>
</table>
<p>Manage Files contains the uploaded digitized copy of the document. To manage files associated with a document, click the <strong>Manage Files</strong> tab.</p>

<p align="center"><img src="images/ManageFilesX.png" width= "400" /></p>

<a name="UDF"></a>
<p><strong>Uploading Digitized Files</strong></p>

<p>
<ul>
	<li>To upload a digitized document, click the <strong>Add New button</strong> <img src="images/UploadDigitizedFiles1.png" /> (at the upper right area of the tab) from the Document Details screen.<br>
	<p align="center"><img src="images/UploadDigitizedFilesX2.png" width="400" /></p>
	</li>
	<li>To select a file to upload, click on <strong>Browse</strong> button <img src="images/UploadDigitizedFilesA1.png" width="60" class="box" /><br>
	<p align="center"><img src="images/UploadDigitizedFilesA.png" width="250" /></p>
	</li>
	<li>When done selecting file to upload, click <strong>Start Upload</strong> button <img src="images/UploadDigitizedFilesB1.png" /> to begin process. Click Cancel button  otherwise.<br>
	<p align="center"><img src="images/UploadDigitizedFilesB.png" width="250" /></p>
	</li>
	<li>When done uploading, click the <strong>X</strong> button <img src="images/UploadDigitizedFilesB2.png" width="20" /> to revert to the Manage Files tab.</li><br>
</ul>
</p>

<p align="right"><a href="manageFiles-Records-Officer.php">&uarr; back To Top</a></p>

<a name="VUDF"></a>
<p><strong>Viewing Uploaded Digitized Files</strong></p>

<p>
<ul>
	<li>From Manage Files, click on the attached file you want to view.<br>
	<p align="center"><img src="images/ViewUploadedDigitizedFilesA.png" width="350" /></p>
	</li>
	<li>Wait for the file to load.<br>
	<p align="center"><img src="images/ViewUploadedDigitizedFilesB.png" width="250" /></p>
	</li>
	<li>To stop viewing the file, click the <strong>X</strong> button <img src="images/UploadDigitizedFilesB2.png" width="20" /> to revert to the Manage Files tab.</li><br>
</ul>
</p>

<p align="right"><a href="manageFiles-Records-Officer.php">&uarr; back To Top</a></p>