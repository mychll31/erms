<p class="xbig"><strong>NOTIFICATIONS</strong></p>

<p>Users will receive notifications through alerts that contain information regarding the status and location of a specific document as well as other important details. Clicking on the icon/graphic located at the lower-right corner of the screen will show the notifications.</p>

<p align="center"><img src="images/Notifications.png" width= "500" /></p>

<p align="center" class="notebox"><strong>NOTE:</strong> <em>User accounts from DOST � CO HRMIS shall be used to log into the system.</em></p>
