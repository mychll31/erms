<p class="xbig"><strong>RECEIVED DOCS</strong></p>

<p>The <strong>Received Docs</strong> folder contains the documents received by an office/employee. The incoming documents are displayed with bold font style for emphasis. This means that these documents are yet to be formally received. The user has to receive the document properly in order to apply necessary actions to it.</p>

