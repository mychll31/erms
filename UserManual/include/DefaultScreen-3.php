<p class="xbig"><strong>DEFAULT SCREEN</strong></p>

<p>Below is the Default Screen for Employee User Account. This page is the same as the Document List page. This is where you are automatically directed to after successfully logging into the system.</p>

<p align="center"><img src="images/DefaultScreen3.png" width= "500"/></p>

<p class="big"><strong>Components of the Default Screen</strong></p>

<p>These are the components of the Default Screen and a brief description of what they are:</p>

<p>
<ul>
	<li><strong>User</strong> � displays the name of the logged-in user. You may also opt to logout of the system by clicking the <em>Logout</em> link.</li><br>
	<li><strong>Folders</strong> � functions as the �menu� containing the different functionalities of the system.</li><br>
	<li><strong>Advanced Search</strong> � allows the user to find records from the database. When link is clicked, user will be redirected to <em>Advanced Search</em> page, where the user can specify the search criteria.</li><br>
	<li><strong>Document List</strong> � shows the list of documents associated with the user. When a <em>DOCID</em> link is clicked, user will be directed to a page that displays the complete details of that document.</li><br>
	<li><strong>Switch to DOST�CO HRMIS</strong> � allows the user to access his/her DOST � CO HRMIS account without having to logout of ERMS and login again to HRMIS.</li><br>
	<li><strong>Notifications</strong> � users receive notifications depending on the system or recipient responses and actions made to the documents. Clicking on the computer icon will show the notification messages.</li>
</ul>
</p>

<p align="right"><a href="defaultScreen-Employee.php">&uarr; Back To Top</a></p>