<p class="xbig"><strong>TRASH</strong></p>

<p align="center"><img src="images/TrashA.png" width="450" /></p>

<p>Clicking on the <strong>Trash</strong> option from the <em>Folders</em> menu which contains a list of document records that, for some reason, were deleted or removed from the Document List.</p>

<p align="center"><img src="images/TrashA1.png" width="400" /></p>

<p>
<ul>
	<li>To restore a record, click the <strong>Restore</strong> icon <img src="images/TrashA2.png" /> across a specific document record. </li>
	<li>To permanently delete a record, click the Delete icon <img src="images/TrashA3.png" /> across a specific document record. A confirmation message will be displayed prompting you if you want to delete the document from the Trash folder, click Yes button to proceed; No button otherwise. This document record will also be deleted from the database.</li>
</ul>
</p>

