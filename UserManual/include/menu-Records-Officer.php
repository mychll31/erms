<div align="left">
<a href="login-Records-Officer.php">LOGIN</a><br>
<hr>

<a href="defaultScreen-Records-Officer.php">DEFAULT SCREEN</a><br>
<hr>

<a href="folders-Records-Officer.php">FOLDERS</a><br>
- <a href="documents-Records-Officer.php">Documents</a><br>
	&nbsp; &bull; <a href="documentList-Records-Officer.php">Document List</a><br>
		&nbsp; &nbsp; &loz; <a href="viewDocumentInfo-Records-Officer.php">View Document Info</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="documentDetails-Records-Officer.php">Document Details</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="manageFiles-Records-Officer.php">Manage Files</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="updateAction-Records-Officer.php">Update Action</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="viewHistory-Records-Officer.php">View History</a><br>
		&nbsp; &nbsp; &loz; <a href="uploadDigitizedFile-Records-Officer.php">Upload Digitized File</a><br>
		&nbsp; &nbsp; &loz; <a href="deleteDocumentRecord-Records-Officer.php">Delete Document</a><br>
	&nbsp; &bull; <a href="incoming-Records-Officer.php">Incoming</a><br>
	&nbsp; &bull; <a href="outgoing-Records-Officer.php">Outgoing</a><br>
	&nbsp; &bull; <a href="reports-Records-Officer.php">Reports</a><br>
- <a href="receivedDocuments-Records-Officer.php">Received Documents</a><br>
	&nbsp; &bull; <a href="receivedDocs-Records-Officer.php">Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="receiveDocument-Records-Officer.php">Receive a Document</a><br>
		&nbsp; &nbsp; &loz; <a href="addActionReply-Records-Officer.php">Add Action Reply</a><br>
		&nbsp; &nbsp; &loz; <a href="flagReceivedDocument-Records-Officer.php">Flag Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="markAsPriority-Records-Officer.php">Mark as Priority</a><br>
		&nbsp; &nbsp; &loz; <a href="deleteReceivedDocument-Records-Officer.php">Delete Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="moveReceivedDocument-Records-Officer.php">Move Received Docs</a><br>

	&nbsp; &bull; <a href="manageFolders-Records-Officer.php">Manage Folders</a><br>
- <a href="libraries-Records-Officer.php">Libraries</a><br>
	&nbsp; &bull; <a href="office-Records-Officer.php">Office</a><br>
	&nbsp; &bull; <a href="cabinet-Records-Officer.php">Cabinet</a><br>
	&nbsp; &bull; <a href="documentType-Records-Officer.php">Document Type</a><br>
	&nbsp; &bull; <a href="actionRequired-Records-Officer.php">Action Required</a><br>
	&nbsp; &bull; <a href="actionTaken-Records-Officer.php">Action Taken</a><br>
	&nbsp; &bull; <a href="backUp-Records-Officer.php">Back Up</a><br>
	&nbsp; &bull; <a href="manageCustodian-Records-Officer.php">Manage Custodian</a><br>
	&nbsp; &bull; <a href="groups-Records-Officer.php">Groups</a><br>
	&nbsp; &bull; <a href="userAccount-Records-Officer.php">User Account</a><br>
- <a href="disposals-Records-Officer.php">Disposals</a><br>
- <a href="trash-Records-Officer.php">Trash</a><br>
<hr>

<a href="notifications-Records-Officer.php">NOTIFICATIONS</a><br>
<hr>

<a href="search-Records-Officer.php">SEARCH</a><br>
<hr>

<a href="switch-Records-Officer.php">SWITCH</a><br>
</div>