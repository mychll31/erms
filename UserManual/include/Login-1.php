<p class="xbig"><strong>LOGIN</strong></p>

<p>Below is the Login Form. Users of the system are required to enter a valid username and password in order to gain access to the system.</p>

<p align="center"><img src="images/001a.png" width="200" /></p>

<p class="big"><strong>How to Login</strong></p>

<p align="center" class="notebox"><strong>NOTE:</strong> <em>User accounts from DOST � CO HRMIS shall be used to log into the system.</em></p>

<p>
<ul>
	<li>Type your Username and Password in the specified textboxes. You may also tick the Remember Login check box if you want your login information to be saved. Then click Login button <img src="images/001b.png">.</li><br>
	<li>If login is successful, you will be directed to the Default Screen (depending on your user access level.</li>
</ul>
</p>

