<p class="big"><strong>Delete Received Document</strong></p>

<p>
<ul>
	<li>From the Received Documents screen, select the document you want to delete by ticking the checkbox (located at the left part of the document record). Then click the Delete icon <img src="images/DeleteReceivedDocument1.png" /> (located at the top of the table/grid).<br>
	<p align="center"><img src="images/DeleteReceivedDocsA1.png" width="400" /></p>
	</li>
	<li>An alert window will appear. To confirm delete of action done to a document, click the <strong>OK</strong> button. Click <strong>Cancel</strong> button otherwise.
	</li>
</ul>
</p>



