<div align="left">
<a href="login-Records-Custodian.php">LOGIN</a><br>
<hr>

<a href="defaultScreen-Records-Custodian.php">DEFAULT SCREEN</a><br>
<hr>

<a href="folders-Records-Custodian.php">FOLDERS</a><br>
- <a href="documents-Records-Custodian.php">Documents</a><br>
	&nbsp; &bull; <a href="documentList-Records-Custodian.php">Document List</a><br>
		&nbsp; &nbsp; &loz; <a href="viewDocumentInfo-Records-Custodian.php">View Document Info</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="documentDetails-Records-Custodian.php">Document Details</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="manageFiles-Records-Custodian.php">Manage Files</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="updateAction-Records-Custodian.php">Update Action</a><br>
			&nbsp; &nbsp; &nbsp; &times; <a href="viewHistory-Records-Custodian.php">View History</a><br>
		&nbsp; &nbsp; &loz; <a href="deleteDocument-Records-Custodian.php">Delete Document</a><br>
	&nbsp; &bull; <a href="incoming-Records-Custodian.php">Incoming</a><br>
	&nbsp; &bull; <a href="outgoing-Records-Custodian.php">Outgoing</a><br>
	&nbsp; &bull; <a href="reports-Records-Custodian.php">Reports</a><br>
- <a href="receivedDocuments-Records-Custodian.php">Received Documents</a><br>
	&nbsp; &bull; <a href="receivedDocs-Records-Custodian.php">Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="receiveDocument-Records-Custodian.php">Receive a Document</a><br>
		&nbsp; &nbsp; &loz; <a href="addActionReply-Records-Custodian.php">Add Action Reply</a><br>
		&nbsp; &nbsp; &loz; <a href="flagReceivedDocs-Records-Custodian.php">Flag Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="markAsPriority-Records-Custodian.php">Mark as Priority</a><br>
		&nbsp; &nbsp; &loz; <a href="deleteReceivedDocs-Records-Custodian.php">Delete Received Docs</a><br>
		&nbsp; &nbsp; &loz; <a href="moveReceivedDocs-Records-Custodian.php">Move Received Docs</a><br>
	&nbsp; &bull; <a href="manageFolders-Records-Custodian.php">Manage Folders</a><br>
- <a href="libraries-Records-Custodian.php">Libraries</a><br>
	&nbsp; &bull; <a href="office-Records-Custodian.php">Office</a><br>
	&nbsp; &bull; <a href="cabinet-Records-Custodian.php">Cabinet</a><br>
- <a href="disposals-Records-Custodian.php">Disposals</a><br>
- <a href="trash-Records-Custodian.php">Trash</a><br>
<hr>

<a href="notifications-Records-Custodian.php">NOTIFICATIONS</a><br>
<hr>

<a href="search-Records-Custodian.php">SEARCH</a><br>
<hr>

<a href="switch-Records-Custodian.php">SWITCH</a><br>
</div>