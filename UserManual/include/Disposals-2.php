<p class="xbig"><strong>DISPOSALS</strong></p>

<p align="center"><img src="images/DisposalsB.png" width= "450"/></p>

<p>Clicking on the <strong>Disposals</strong> option from the <strong>Folders</strong> menu allows the user to access functionalities on Documents Disposal. Documents subject for disposal are the ones which have reached the end of their retention period. It contains the following sections:</p>

<p>
<ul>
	<li><strong>For Disposals</strong> � contains the list of documents which have reached the end of their retention period.</li><br>
	<li><strong>Disposed Docs</strong> � contains the list of documents which have been disposed either through destruction or selling.</li><br>
	<li><strong>Docs Transferred to Records Center</strong> � contains the list of documents which have been transferred to the Records Center.</li><br>
	<li><strong>Docs Transferred to National Archives</strong> � contains the list of documents which have been transferred to the National Archives.</li><br>
</ul>
</p>

<p class="big"><strong>Dispose a Document</strong></p>

<p>
<ul>
	<li>From the Disposals screen, click <strong>For Disposals</strong> tab. Select the document you want to dispose from the list by ticking the checkbox (located at the left part of the document record). Then click the Dispose Document icon (located at the top of the table/grid).<br>
	<p align="center"><img src="images/ForDisposals.png" width="400" /></p>
	</li>
	<li>The following window will appear:<br>
	<p align="center"><img src="images/ForDisposals1.png" width="250" /></p>
	</li>
	<li>Select <strong>Document Remarks</strong> from the drop-down list. Then click <strong>Dispose</strong> button to view the document record. A message will receive indicating whether the document was transferred, sold or destroyed or not.</li>
</ul>
</p>

<p align="right"><a href="disposals-Records-Custodian.php">&uarr; Back To Top</a></p>