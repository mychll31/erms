<p class="big"><strong>Update Action</strong></p>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="boxyellow">
<tr>
	<td width="33%" align="center" style="border-right:thin #FFFF00 solid"><a href="#UAD">Updating Action to a Document</a></td>
	<td width="34%" align="center" style="border-right:thin #FFFF00 solid"><a href="#ARATD">Adding a Reply to an Action Taken to a Document</a></td>
	<td width="33%" align="center"><a href="#UAMD">Undoing Action Made to a Document</a></td>
</tr>
</table>

<p>Update Action is where the Employee can update the document movement by adding actions to the document.</p>

<p align="center"><img src="images/UpdateActionX.png" width="400" /></p>

<p>The following information is needed:
<ul>
	<li><strong>Action Taken</strong> � applied action.</li><br>
	<li><strong>Action Required</strong> � required action for the document recipient.</li><br>
	<li><strong>Action Unit</strong> � employee, office or agency responsible for undertaking the action required.</li><br>
	<li><strong>Remarks</strong> � additional notes or comments regarding action done to document.</li><br>
	<li><strong>Restricted</strong> � checkbox which indicates the restrictions of the document (when checked).</li><br>
	<li><strong>Reply Expected</strong> � checkbox which indicates whether a reply is expected from the recipient (when checked).</li><br>
</ul>
</p>

<p align="right"><a href="updateAction-Records-Officer.php">&uarr; back To Top</a></p>

<a name="UAD"></a>
<p><strong>Updating Action to a Document</strong></p>

<p>
<ul>
	<li>Select an <strong>Action Taken</strong> from the drop-down list.<br>
	<p align="center"><img src="images/UpdateAction1.png" width="350" /></p>
	</li><br>
	<li>Select an <strong>Action Required</strong> from the document recipient from the drop-down list.<br>
	<p align="center"><img src="images/UpdateAction2.png" width="350" /></p>
	</li><br>
	<li>Type the <strong>Action Unit</strong> (recipient of document). It can either be an Agency, Office or Employee.<br>
	<p align="center"><img src="images/UpdateAction3.png" width="350" /></p>
	</li><br>
	<li>Type in your <strong>Remarks</strong>, if any.
	</li><br>
	<li>If the document is <strong>Restricted</strong>, tick the checkbox as an indication. For restrictions on documents, see <a href="index-Restrictions.php">Document Restrictions</a>.
	</li><br>
	<li>If a reply is expected from the recipient, tick the <strong>Reply expected</strong> checkbox as an indication.<br>
	<p align="center"><img src="images/UpdateAction4.png" width="350" /></p>
	</li><br>
	<li>Click Submit button <img src="images/UpdateAction5.png" /> when done.</li><br>
	<li>Action made to the document will be displayed in the Update Action tab.<br>
	<p align="center"><img src="images/UpdateAction6.png" width="350" /></p>
	</li><br>
</ul>
</p>

<p align="right"><a href="updateAction-Records-Officer.php">&uarr; back To Top</a></p>

<a name="ARATD"></a>
<p><strong>Adding a Reply to an Action Taken to a Document</strong></p>

<p>
<ul>
	<li>On the Actions Made table, click on the <strong>Add Reply</strong> icon across a specific action made.<br>
	<p align="center"><img src="images/ReplyAction1.png" width="350" /></p>
	</li><br>
	<li>The following window will appear:<br>
	<p align="center"><img src="images/ReplyAction2.png" width="250" /></p>
	</li><br>
	<li>Select an <strong>Action Taken</strong> from the drop-down list.</li><br>
	<li>Select an <strong>Action Required</strong> from the document recipient from the drop-down list.</li><br>
	<li>Type in your <strong>Remarks</strong>, if any.</li><br>
	<li>If a reply is expected from the recipient, tick the <strong>Reply expected</strong> checkbox as an indication.</li><br>
	<li>Click Submit button <img src="images/ReplyAction3.png" /> when done.</li><br>
	<li>You may also click the <strong>X</strong> button to revert to the previous page.</li><br>
</ul>
</p>

<p align="right"><a href="updateAction-Records-Officer.php">&uarr; back To Top</a></p>

<a name="UAMD"></a>
<p><strong>Undoing Action Made to a Document</strong></p>

<p>
<ul>
	<li>On the Actions Made table, click on the <strong>Undo Action</strong> icon across a specific action made.<br>
	<p align="center"><img src="images/UndoAction.png" width="350" /></p>
	</li><br>
	<li>An alert window will appear. To confirm delete of action done to a document, click the <strong>OK</strong> button. Click <strong>Cancel</strong> button otherwise.</li><br>
</ul>
</p>

<p align="right"><a href="updateAction-Records-Officer.php">&uarr; back To Top</a></p>

