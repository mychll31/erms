<p class="xbig"><strong>SWITCH</strong></p>

<p class="big"><strong>Switch from DOST-CO ERMS to DOST-CO HRMIS</strong></p>

<p>The DOST-CO ERMS is integrated with DOST-HRMIS. As such, users are allowed to switch from one system to another and access it based on the given level.</p>

<p>
<ul>
	<li>To switch from the DOST-CO ERMS to the DOST-HRMIS, click on the <strong>HRMIS</strong> link located at the lower-right corner of the screen. The logged in user will be directed to the DOST�CO HRMIS and will therefore be logged on to the system with the same user account.</li>
</ul>
</p>

<p align="center"><img src="images/toHRMIS2.png" width= "500"/></p>

<p align="right"><a href="switch-Records-Custodian.php">&uarr;  Back To Top</a></p>

<p class="big"><strong>Switch from Records Custodian Module to Employee Module</strong></p>

<p>Aside from having this account, the Records Custodian, can also access his/her Employee Account without logging out of the system.</p>

<p>
<ul>
	<li>To switch from the Records Custodian Module to the Employee Module, click on the <strong>Employee Module</strong> link located at the left side of the screen. The logged in user will be directed to his/her employee account.</li><br>
	<li>To switch from the Employee Module to the Records Custodian Module, click on the <strong>Records Custodian</strong> link.</li>
</ul>
</p>

<p align="center"><img src="images/toEmployee2.png" width= "500"/></p>

<p align="right"><a href="switch-Records-Custodian.php">&uarr;  Back To Top</a></p>
