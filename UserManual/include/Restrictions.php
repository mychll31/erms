<p class="xbig"><strong>DOCUMENT RESTRICTIONS</strong></p>

<p>Document Restriction feature allows the user to limit which office, agency or specific employee(s) can view and/or make necessary updates to a record. The <strong>Restricted</strong> checkbox is located at the lower portion of the Update Action tab.</p>

<p align="center"><img src="images/Restrictions.png" width= "350" /></p>

<p class="big"><strong>Sample Scenarios</strong></p>

<p>
<ul>
	<li>
		<p>Action Unit: <strong>Office</strong><br>Restriction: <strong>Unchecked</strong></p>
		<p align="center"><img src="images/RestrictionsA.png" width= "350" /></p>
		<p>In this example, <strong>ALL employees</strong> under Planning and Evaluation Services (PES) <strong>AND ALL divisions (and its employees)</strong> under PES will be able to receive the document.</p>
		<p align="right"><a href="index-Restrictions.php">&uarr;  Back To Top</a></p>
	</li>
	<li>
		<p>Action Unit: <strong>Office</strong><br>Restriction: <strong>Checked</strong></p>
		<p align="center"><img src="images/RestrictionsB.png" width= "350" /></p>
		<p>In this example, <strong>ONLY the Head of Office, Records Custodian and employees (who do not belong in any division)</strong> but are directly under PES will be able to receive the document.</p>
		<p align="right"><a href="index-Restrictions.php">&uarr;  Back To Top</a></p>
	</li>
	<li>
		<p>Action Unit: <strong>Employee (Head of Office)</strong><br>Restriction: <strong>Unchecked</strong></p>
		<p align="center"><img src="images/RestrictionsC.png" width= "350" /></p>
		<p>-In this example, <strong>ONLY ITD Chief Donna Ruth Montalban AND her Records Custodian</strong> will be able to receive the document.</p>
		<p align="right"><a href="index-Restrictions.php">&uarr;  Back To Top</a></p>
	</li>
	<li>
		<p>Action Unit: <strong>Employee (Head of Office)</strong><br>Restriction: <strong>Checked</strong></p>
		<p align="center"><img src="images/RestrictionsD.png" width= "350" /></p>
		<p>In this example, <strong>ONLY ITD Chief Donna Ruth Montalban</strong> will be able to receive the document.</p>
		<p align="right"><a href="index-Restrictions.php">&uarr;  Back To Top</a></p>
	</li>
	<li>
		<p>Action Unit: <strong>Employee (Regular)</strong><br>Restriction: <strong>Unchecked</strong></p>
		<p align="center"><img src="images/RestrictionsE.png" width= "350" /></p>
		<p>In this example, <strong>ONLY Employee Dunn Alfredo Celestial</strong> will be able to receive the document.</p>
		<p align="right"><a href="index-Restrictions.php">&uarr;  Back To Top</a></p>
	</li>
</ul>
</p>
 
