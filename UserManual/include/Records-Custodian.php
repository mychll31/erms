<table align="center" border="0" cellpadding="0" cellspacing="0" width="800">
	<tr>
		<td width="200" valign="top" style="padding:10px"><?php include("menu-Records-Custodian.php"); ?></td>
		<td width="600" valign="top" style="padding:10px">
		The Records Custodian Module is accessible ONLY to Records Custodians. The Records Custodian is any DOST-CO employee assigned to manage documents. They can only access records intended for their respective offices.
		</td>
	</tr>
</table>
