<p class="xbig"><strong>SEARCH</strong></p>

<p>The system also has a Search feature, which allows searching of records in two (2) options: <a href="#SimpleSearch">Simple Search</a> and <a href="#AdvancedSearch">Advanced Search.</a></p>

<p align="center"><img src="images/Search2.png" width= "500"/></p>

<a name="SimpleSearch"></a>
<p class="big"><strong>Simple Search</strong></p>

<p>
<ul>
	<li>To search for a document, type a keyword in the <strong>Simple Search</strong> textbox (e.g. �for�). Then click Search button<img src="images/SearchSimple1.png"/>.</li><br>
	<li>The system will display all the records that matched the keyword. Notice that the Subjects column displayed the matching keywords in <font color="#FF0000">red</font>.</li>
</ul>
</p>

<p align="center"><img src="images/SearchSimple.png" width="400"/></p>

<p align="right"><a href="search-Records-Custodian.php">&uarr;  Back To Top</a></p>

<a name="AdvancedSearch"></a>
<p class="big"><strong>Advanced Search</strong></p>

<p>Advanced search provides a more comprehensive searching of records that matched the search criteria. Click on the <strong>Advanced Search</strong> link located at the upper-right side of the screen. You will be directed to the Advanced Search form.</p>

<p align="center"><img src="images/SearchAdvanced.png" width="400"/></p>

<p>
<ul>
	<li>Select the specific metadata from the drop-down lists: <em>Subject</em>, <em>Document Type</em>, <em>Status</em>, and <em>Deadline</em>. Type the search keyword on the textbox provided. Click the Search button <img src="images/SearchAdvanced1.png"/> when done.</li>
</ul>
</p>

<p align="right"><a href="search-Records-Custodian.php">&uarr;  Back To Top</a></p>
