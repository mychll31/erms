<p class="xbig"><strong>RECEIVED DOCUMENTS</strong></p>

<p>Clicking on the <strong>Received Documents</strong> option from the <strong>Folders</strong> menu allows the user to access functionalities on Documents Management regarding receiving of documents.</p>
<p align="center"><img src="images/ReceivedDocumentsX.png" width="450" /></p>



