function statusBar()
{
	var str = "Electronic Record Management System";
	//alert(str);
	window.status=str;
}

function checkedAll (frm,chkbox) {
	var checked = chkbox.checked;
	if(document.forms[frm].chk.length == undefined)
		document.forms[frm].chk.checked = checked;
	else
	{
		for (var i =0; i < document.forms[frm].chk.length; i++) 
			 document.forms[frm].chk[i].checked = checked;
	}
}


function toggleWindowAjax(divId,imgId)
{
if(document.getElementById(divId).style.visibility=="hidden")
{
	document.getElementById(divId).style.visibility="visible";
	document.getElementById(imgId).src="images/menu_strip_down_arrow.gif";
	
}
else
{
	document.getElementById(divId).style.visibility="hidden";
	document.getElementById(divId).innerHTML="";
	document.getElementById(imgId).src="images/menu-bar-right-arrow.gif"
	return false;
}

}

function toggleWindowAjax2(divId,imgId)
{
if($('#'+divId).css('display')=="block"){
 $('#'+divId).css('display','none');
 $('#'+imgId).attr('src','images/menu-bar-right-arrow.gif');}
else{
 $('#'+divId).css('display','block');
 $('#'+imgId).attr('src','images/menu_strip_down_arrow.gif');}
}

function toggleWindowAjax002(divId,imgId)
{
if($('.'+divId).css('display')=="block"){
 $('.'+divId).css('display','none');
 $('#'+imgId).attr('src','images/menu-bar-right-arrow.gif');}
else{
 $('.'+divId).css('display','block');
 $('#'+imgId).attr('src','images/menu_strip_down_arrow.gif');}
}


function setCookie(c_name,value,expiredays)
{
 var exdate=new Date();
 exdate.setDate(exdate.getDate()+expiredays);
 document.cookie=c_name+ "=" +escape(value)+
 ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}


		function toggleOffice(oForm)
		{
			oForm["cmbOfficeCode"].disabled = false;
			if(oForm["cmbGroupCode"])
			{
			oForm["cmbGroupCode"].disabled=false;
			}
			oForm["cmbAgency"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[0].checked = true;
		}
		function toggleAgency(oForm)
		{
			oForm["cmbAgency"].disabled = false;
			if(oForm["cmbGroupCode"])
			{
			oForm["cmbGroupCode"].disabled=true;
			}
			oForm["cmbOfficeCode"].disabled = true;
			radiovar = oForm["actionUnitSwitch"];
			radiovar[1].checked = true;
		}
		function checkConfi(obj)
{
//x=trim(obj.t_strDocType.value);
x=1;
if(x=="")
{
	if(confirm("Proceed with unknown document type?")==true)
	{
	 for (var n=0; n < obj.elements.length; n++) 
	 {
		 if(obj.elements[n].getAttribute('title')=="required")
		 {
		  obj.elements[n].setAttribute('title',"");
		 }
	 }
	 return true;
	}
	else
	return false;
}

}



function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
	
}

function checkNodeStatus(url,div,f,c)
{
 if($.x_file_fileExist(f,c)){
  getData(url,div);
 return true; }
 else {
  alert('Remote server is down! You can not edit this document');
  return false; }
}

function disappear(){
	$(".pane").animate({ opacity: 'hide' }, 3000);
}