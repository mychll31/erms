

// FOR LOCATION LIBRARIES

// Roshan's Ajax dropdown code with php
// This notice must stay intact for legal use
// Copyright reserved to Roshan Bhattarai - nepaliboy007@yahoo.com
// If you have any problem contact me at http://roshanbh.com.np
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	
	
	function resetMuni()
	{
		if (document.getElementById('citymunidiv'))
		{
			document.getElementById('citymunidiv').innerHTML="<select name='txtCityMuniCode'><option>Select Province First</option> </select>";
		}
		
	
	}
	
	

var max=0;
		function showChar(obj, name)
		{
			var obj3=document.getElementById("char");
			if(max==0)
			{
				max=obj3.firstChild.nodeValue*1;
			}
			len=obj.value.length;
			var cur=max*1;
			cur=cur-len;
			if(cur<0)
			{
				var obj2=document.forms[0].elements[name];
				var str=obj2.value.substring(0,max*1);
				obj2.value=str;
				alert('Input limit has been reached.');
				showChar(obj2,max);					
				return false;
			}
			else
			{
				var obj2=document.getElementById('char');
				var str=document.createTextNode(cur);
				obj2.replaceChild(str,obj2.firstChild);
				return true;
			}
		}	
		
//////////////

	

////////////

function getServices(txtOfficeCode,txtDiv) {
		var randomnumber=Math.floor(Math.random()*11)
		var strURL="findServices.php?code="+txtOfficeCode+"&rnd="+randomnumber;
	
		var req = getXMLHTTP();
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById(txtDiv).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
		

function getDivisions(txtOfficeCode,txtServiceCode,txtDiv) {
		var randomnumber=Math.floor(Math.random()*11)
		var strURL="findDivisions.php?code="+txtOfficeCode+"&servcode="+txtServiceCode+"&rnd="+randomnumber;
	
		var req2 = getXMLHTTP();
		if (req2) {
			
			req2.onreadystatechange = function() {
				if (req2.readyState == 4) {
					// only if "OK"
					if (req2.status == 200) {						
						document.getElementById(txtDiv).innerHTML=req2.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req2.statusText);
					}
				}				
			}			
			req2.open("GET", strURL, true);
			req2.send(null);
		}		
	}
	
	