$().ready(function() {
	 var validator = $("#entryfrm").validate({
		rules: {
			retentionPeriod:{ required: true,
							  digits: true  },
			docTypeAbbrv: "required",
			docTypeDesc: "required",
			
			officeName: "required",
			contact:{ required: true,
					  digits: true  },
			address: "required",
			contactperson: "required"
		},
		messages: {
			docTypeAbbrv: "Document Type Code is required!",
			docTypeDesc: "Document Type description is required!",
			retentionPeriod: { required:"Retention Period is required!" ,
						       digits: "Invalid Input"
				   			 },
			officeName: "Office Name is required",
			contact: { required: "Contact is required",
				       digits: "Invalid Input"
				     },
		    address: "Address is required",
			contactperson: "Contact person is required"
				   
		},
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		// specifying a submitHandler prevents the default submit, good for the demo
		submitHandler: function() {
		 $("#add").click();
		},
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	
