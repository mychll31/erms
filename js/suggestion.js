function RemoteStateSuggestions() {

    if (typeof XMLHttpRequest != "undefined") {
        this.http = new XMLHttpRequest();
    } else if (typeof ActiveXObject != "undefined") {
        this.http = new ActiveXObject("MSXML2.XmlHttp");
    } else {
        alert("No XMLHttpRequest object available. This functionality will not work.");
    }

}

RemoteStateSuggestions.prototype.requestSuggestions = function (oAutoSuggestControl,
bTypeAhead) {

    var oHttp = this.http;

    if (oHttp.readyState != 0) {
        oHttp.abort();
    }

    var sURL = "suggest.php?t="+fetch_unix_timestamp()+"&userInput=" + encodeURIComponent(oAutoSuggestControl.textbox.value);

    oHttp.open("get", sURL , true);
    oHttp.onreadystatechange = function () {
        if (oHttp.readyState == 4) {
            var aSuggestions = eval(oHttp.responseText);
            oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
        }
    };
    oHttp.send(null);

};