// JavaScript Document
function AJAX(){ 
try{xmlHttp=new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari 
return xmlHttp; } 
catch (e){ 
try{ xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer 
return xmlHttp; } 
catch (e){ 
try{ xmlHttp=new ActiveXObject("Microsoft.XMLHTTP"); 
return xmlHttp; } 
catch (e){ alert("Your browser does not support AJAX."); 
return false; } } } }  
 
// Timestamp for preventing IE caching the GET request (common function) 
function fetch_unix_timestamp() { 
	return parseInt(new Date().getTime().toString().substring(0, 10)) 
} 


//doc list
function getDocList(url,divid){ 
var xmlHttp_list = AJAX(); 
var timestamp = fetch_unix_timestamp(); 
var nocacheurl = url+"&t="+timestamp; 
var seconds = 90; 
xmlHttp_list.onreadystatechange=function(){ 
	if(xmlHttp_list.readyState==4){ 
		//document.getElementById(divid).innerHTML=xmlHttp_list.responseText;
		sethtml(divid,xmlHttp_list.responseText);
		//setTimeout('getDocList(url,divid)',seconds*1000); 
	} 
} 

xmlHttp_list.open("GET",nocacheurl,true); 
xmlHttp_list.send(null); 
}

//get menu function
function getMenu(url,divid){ 
var xmlHttp_menu = AJAX(); 
var timestamp = fetch_unix_timestamp(); 
var nocacheurl = url+"&t="+timestamp;
xmlHttp_menu.onreadystatechange=function(){ 
	if(xmlHttp_menu.readyState==4){ 
		document.getElementById(divid).innerHTML=xmlHttp_menu.responseText; 
	} 
} 

xmlHttp_menu.open("GET",nocacheurl,true); 
xmlHttp_menu.send(null); 
}




// main function
function getData(url,divid){ 
var xmlHttp_data = AJAX(); 
var timestamp = fetch_unix_timestamp(); 
var nocacheurl = url+"&t="+timestamp;
xmlHttp_data.onreadystatechange=function(){ 
	if(xmlHttp_data.readyState==4){  
		//document.getElementById(divid).innerHTML=xmlHttp_data.responseText; 
		sethtml(divid,xmlHttp_data.responseText);
		datePickerController.create();
	} 
} 
xmlHttp_data.open("GET",nocacheurl,true); 
xmlHttp_data.send(null); 
}

function postData(url,divid,phppage){ 
var xmlHttp_data = AJAX(); 
var timestamp = fetch_unix_timestamp(); 
var nocacheurl = url+"&t="+timestamp;
xmlHttp_data.onreadystatechange=function(){ 
	if(xmlHttp_data.readyState==4){ 
		//document.getElementById(divid).innerHTML=xmlHttp_data.responseText; 
		sethtml(divid,xmlHttp_data.responseText);
		datePickerController.create();
	} 
} 
xmlHttp_data.open("POST",phppage,true); 
xmlHttp_data.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlHttp_data.setRequestHeader("Content-length", nocacheurl.length);
xmlHttp_data.setRequestHeader("Connection", "close");
xmlHttp_data.send(nocacheurl);
}



function sethtml(div,content) 
{ 
    var search = content; 
    var script; 
	var oDiv=document.getElementById(div);
	if (oDiv)
	{
    document.getElementById(div).innerHTML=content;       
    while( script = search.match(/(<script[^>]+javascript[^>]+>\s*(<!--)?)/i)) 
    { 
      search = search.substr(search.indexOf(RegExp.$1) + RegExp.$1.length); 
       
      if (!(endscript = search.match(/((-->)?\s*<\/script>)/))) break; 
       
      block = search.substr(0, search.indexOf(RegExp.$1)); 
      search = search.substring(block.length + RegExp.$1.length); 
       
      var oScript = document.createElement('script'); 
      oScript.text = block; 
      document.getElementsByTagName("head").item(0).appendChild(oScript); 
    } 
	}
}





//////////////////////////////////////////
//Function:    get
//Parameters:  form Object, div name, form target page
//Return Type: N/A
//Description: posts the ajax form to the server
//Usage:	   <form action="javascript:get(document.getElementById('frmName'),'divname','phppage.php');"  
//				name="frmName"id="frmName">
//Note:		   if any form elements here have been forgotten... 
//			   kindly add the code here to append the data to getstr
///////////////////////////////////////////
 function get(obj,div,phppage) {
  var getstr = "";
  for (i=0; i<obj.getElementsByTagName("input").length; i++) {
	  //alert(obj.getElementsByTagName("input")[i].type+":"+obj.getElementsByTagName("input")[i].name+":"+obj.getElementsByTagName("input")[i].value);
        if (obj.getElementsByTagName("input")[i].type == "text") {
        idy=obj.getElementsByTagName("input")[i].name;
		/*getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   document.getElementById(idy).value + "&";*/
           getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
		}
        if (obj.getElementsByTagName("input")[i].type == "checkbox") {
           if (obj.getElementsByTagName("input")[i].checked) {
              getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
           } else {
              getstr += obj.getElementsByTagName("input")[i].name + "=&";
           }
        }
        if (obj.getElementsByTagName("input")[i].type == "radio") {
           if (obj.getElementsByTagName("input")[i].checked) {
              getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
           }
     }  
     
	  if (obj.getElementsByTagName("input")[i].type == "hidden") {
           getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
        }
  }
  
    for (i=0; i<obj.getElementsByTagName("select").length; i++) {	
    if(obj.getElementsByTagName("select")[i].length!=0)
	{
	  if (obj.getElementsByTagName("select")[i].multiple == true) {
	  var sel = obj.getElementsByTagName("select")[i];
			  for(z=0;z<sel.options.length;z++)
				  {
				  if(sel.options[z].selected)
				  {
				  getstr += sel.name + "=" + sel.options[z].value + "&";
				  }
				  
				  }
	  
	  }
	  else{
		  var sel = obj.getElementsByTagName("select")[i];
		  getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
	  }
	}
  }
   for (i=0; i<obj.getElementsByTagName("textarea").length; i++) {
        var sel = obj.getElementsByTagName("textarea")[i];
		selectvalue=sel.value;
		selectvalue=selectvalue.replace(/&/g,"%26");
        getstr += sel.name + "=" + selectvalue + "&";
  }
  getstr+="t_submit=1";
  postData(getstr,div,phppage);
}

////////////////////////////////////////
//
//function : 		check (version 1)
//description: 		to check textbox and textarea for null values
//usage:			set title attribute as "required"
//future updates: 	if given time, will include parsing of dates,
//					email, numbers, etc.
//
///////////////////////////////////////
function check(obj,div,phppage)
 {
 intEmptyInput=0;

 
 for (i=0; i<obj.getElementsByTagName("input").length; i++) 
 {
	 var sel = obj.getElementsByTagName("input")[i];
    if (sel.type == "text" && (sel.getAttribute('alt')=="required" || sel.getAttribute('title')=="required")) 
	{
		if (sel.value=="")
		{
			var divname=sel.name+"_div";
			var requiredspan=document.createElement('span');
			 requiredspan.className="required";
			 requiredspan.innerHTML="&nbsp;*required";
			 intEmptyInput++;
			var elem = getNextElement(sel.nextSibling);
			if(elem==null)
			{
				var parent= sel.parentNode;
				parent.appendChild(requiredspan);
			}
			else if(elem.tagName=="SPAN" || elem.tagName=="span")
			{
				elem.innerHTML="&nbsp;*required";
			}else
			{
				var parent= sel.parentNode;
				parent.insertBefore(requiredspan,elem);
			}
		}
	}
 }
  for (i=0; i<obj.getElementsByTagName("textarea").length; i++) 
 {
	 var sel = obj.getElementsByTagName("textarea")[i];
    if (sel.getAttribute('title')=="required") 
	{
	if (sel.value=="")
		{
			var divname=sel.name+"_div";
			var requiredspan=document.createElement('span');
			 requiredspan.className="required";
			 requiredspan.innerHTML="&nbsp;*required";
			 intEmptyInput++;
			 elem =getNextElement(sel.nextSibling);
			if(elem==null)
			{
				var parent= sel.parentNode;
				parent.appendChild(requiredspan);
			}
			else if(elem.tagName=="SPAN" || elem.tagName=="span"){
				elem.innerHTML="&nbsp;*required";
			}else
			{
				var parent= obj.getElementsByTagName("input")[i].parentNode;
				parent.insertBefore(requiredspan,elem);
			}
		}
	}
 }
 if(intEmptyInput==0) get(obj,div,phppage);
}


function getNextElement(node) {
if(node==null) return null;
if(node.nodeType == 1)
	{		
		return node;		
	}	
if(node.nextSibling) 
	{		
		return getNextElement(node.nextSibling)		
	}	
return null;
}

