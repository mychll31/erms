<?  @session_start();
include_once("class/DocDetail.class.php");
include_once("General.class.php");
$objDocDetail = new DocDetail;
if($_GET['mode']=="") $arrFields = $objDocDetail->getUserFields();
else $arrFields = $objDocDetail->getUserFields1();

$docID = $arrFields['docID'];
$divId = $arrFields['div'];

$rsCurrentAction = $objDocDetail->getCurrentReference($docID); 
$rsInitialAction = $objDocDetail->getInitialReference($docID); 

?>
<?php if(count($rsInitialAction)!=0) { ?>
<table width="100%"  border="0">
  <tr>
    <td>
	
	<table width="100%"  border="0">
      <tr>
        <td width="50%" valign="top"><table width="100%"  border="1" class="listings">
          <tr>
            <td colspan="2">Current Reference </td>
            </tr>
          <tr>
            <td width="17%">Date:</td>
            <td width="83%"><?php echo $rsCurrentAction[0]['dateSent'];?>
			</td>
          </tr>
          <tr>
            <td>From:</td>
            <td><?php 
			$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsCurrentAction[0]['senderId'], $rsCurrentAction[0]['senderUnit']);
			echo $currentActionSender; ?></td>
          </tr>
          <tr>
            <td>To:</td>
            <td><?php 
			$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsCurrentAction[0]['recipientId'], $rsCurrentAction[0]['recipientUnit']);
			echo $currentActionRecipient;
			?></td>
          </tr>
		   <tr>
            <td>Remarks:</td>
            <td><?php 
			echo $rsCurrentAction[0]['remarks'];
			?></td>
          </tr>
		  <tr>
            <td>Received by:</td>
            <td><?php echo $rsCurrentAction[0]['receivedBy']; ?></td>
          </tr>		  
		   <tr>
            <td colspan="2">&nbsp; </td>
            </tr>
          <tr>
          <tr>
            <td colspan="2">Initial Reference </td>
            </tr>
          <tr>
            <td width="17%">Date:</td>
            <td width="83%"><?php echo $rsInitialAction[0]['dateSent'];?></td>
          </tr>
          <tr>
            <td>From:</td>
            <td><?php 
			$initialActionSender = $objDocDetail->displayRecipientSenderNames($rsInitialAction[0]['senderId'], $rsInitialAction[0]['senderUnit']);
			if($objDocDetail->isAgencyUser($rsInitialAction[0]['senderId'])){
				$agencyOriginId = $objDocDetail->getOfficeCode($rsInitialAction[0]['senderId']);
				$initialActionSender = $objDocDetail->getOfficeName($agencyOriginId,'','','agency');
			}

			echo $initialActionSender ?></td>
          </tr>
          <tr>
            <td>To:</td>
            <td><?php 
			$initialActionRecipient = $objDocDetail->displayRecipientSenderNames($rsInitialAction[0]['recipientId'], $rsInitialAction[0]['recipientUnit']);
			echo $initialActionRecipient;
			?></td>
          </tr>
		    <tr>
            <td>Remarks:</td>
            <td><?php 
			echo $rsInitialAction[0]['remarks'];
			?></td>
          </tr>
        </table></td>
        <td>
		<table>
		<tr>
		<td>
		<div style="border : solid 1px #4297d7; background : #F2FFCC; color : #000000; padding : 4px; width : 250px; height : 250px; overflow : auto; ">
			
			<table width="100%" border="0" class="listings">
			<tr>
            <td colspan="2">&nbsp;</td>
            </tr>
			
  <?php 
  for($ctr=0; $ctr<sizeof($rsCurrentAction); $ctr++)
  {
	$currentActionSender = $objDocDetail->displayRecipientSenderNames($rsCurrentAction[$ctr]['senderId'], $rsCurrentAction[$ctr]['senderUnit']);
	if($objDocDetail->isAgencyUser($rsCurrentAction[$ctr]['senderId'])){
		$agencyOriginId = $objDocDetail->getOfficeCode($rsCurrentAction[$ctr]['senderId']);
		$currentActionSender = $objDocDetail->getOfficeName($agencyOriginId,'','','agency');
	}

	$currentActionRecipient = $objDocDetail->displayRecipientSenderNames($rsCurrentAction[$ctr]['recipientId'], $rsCurrentAction[$ctr]['recipientUnit']);
				
				if($rsCurrentAction[$ctr]['restricted'])
				{
				  $arrHead=$objDocDetail->getOfficeHead($_SESSION["office"],$_SESSION["userUnit"]);
				  $intHead=0;

				  for($cntHead=0;$cntHead<count($arrHead);$cntHead++)
				  {
					  if($_SESSION["empNum"]==$arrHead[$cntHead]){
						  $intHead=1;
						  break;
					  }
				  }
					
					if(($rsCurrentAction[$ctr]['senderUnit']=="employee"&& $_SESSION["empNum"] ==$rsCurrentAction[$ctr]['senderId']) ||
						($rsCurrentAction[$ctr]['senderUnit']!="employee" && $rsCurrentAction[$ctr]['senderId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)) ||
						($rsCurrentAction[$ctr]['senderUnit']!="employee" && $rsCurrentAction[$ctr]['senderId']== $_SESSION["office"] && $intHead))
						{ $strRemarks=$rsCurrentAction[$ctr]['remarks']; }
					elseif($rsCurrentAction[$ctr]['recipientUnit']=="employee" && $rsCurrentAction[$ctr]['recipientId']==$_SESSION["empNum"]) 
						$strRemarks=$rsCurrentAction[$ctr]['remarks'];
					else if($rsCurrentAction[$ctr]['recipientUnit']!="employee")
					{
						if ($rsCurrentAction[$ctr]['recipientId']== $_SESSION["office"] && ($_SESSION["userType"]==1 || $_SESSION["userType"]==2)){
							 $strRemarks=$rsCurrentAction[$ctr]['remarks'];
						}
						else{
							if($intHead && $rsCurrentAction[$ctr]['recipientId']== $_SESSION["office"]) $strRemarks=$rsCurrentAction[$ctr]['remarks'];
							else $strRemarks="";
						}
					}
					else
					$strRemarks="";
				}
				else{
					$strRemarks=$rsCurrentAction[$ctr]['remarks'];
				}
		  
		  $dateImplemented = $objDocDetail->getDateImplemented();
		  $dateSent = explode(' ',$rsCurrentAction[$ctr]['dateSent']);
		  $arrReceiversData = $objDocDetail->getDocumentReceiver($rsCurrentAction[$ctr]['historyId'],$rsCurrentAction[$ctr]['documentId'],$rsCurrentAction[$ctr]['recipientId']);
		  $strReceiver='';
		  $arrReceiver = array();
		  $tempReceiver = '';
		  if(strtotime($dateImplemented)-strtotime($dateSent[0])<=0){
			 for($i=0;$i<count($arrReceiversData);$i++){
			 	 if($tempReceiver != $arrReceiversData[$i]['receivedBy'].substr($arrReceiversData[$i]['dateReceived'], 0, 16)):
				 	$arrReceiver[] =$arrReceiversData[$i]['receivedBy'].' ('.$arrReceiversData[$i]['dateReceived'].')';
				 endif;
				 $tempReceiver = $arrReceiversData[$i]['receivedBy'].substr($arrReceiversData[$i]['dateReceived'], 0, 16);
			 }
			 $strReceiver = implode(',',$arrReceiver);
		  }
		  else{
			  $strReceiver = $rsCurrentAction[$ctr]['receivedBy'].' ('.$rsCurrentAction[$ctr]['dateReceived'].')';			  
		  }
		  $receivedData = "<td>$strReceiver</td>";
		  
		  
          echo '
          <tr>
            <td width="25%">Date:</td>
            <td width="83%"><p id='.$rsCurrentAction[$ctr]['historyId'].'></p><p id='.$rsCurrentAction[$ctr]['referenceId'].'></p>'.$rsCurrentAction[$ctr]['dateSent'].'</td>
          </tr>
		   <tr>
            <td>Action Taken:</td>
            <td>'.$rsCurrentAction[$ctr]['actionTakenDesc'].'</td>
          </tr>
          <tr>
            <td>From:</td>
            <td><a href="#'.$rsCurrentAction[$ctr]['referenceId'].'">'.$currentActionSender.'</a></td>
          </tr>
          <tr>
            <td>To:</td>
            <td><a href="#'.$rsCurrentAction[$ctr]['historyId'].'">'.$currentActionRecipient.'</a></td>
          </tr>
		    <tr>
            <td>Action Required:</td>
            <td>'.$rsCurrentAction[$ctr]['actionDesc'].'</td>
          </tr>';
//pao 011315
	if ($strRemarks<>"")
	 echo '<tr>
            <td>Remarks:</td>
            <td>'.$strRemarks.'</td>
          </tr>';
		  
	  echo '<tr>
            <td style="vertical-align: top;">Received by:</td>
            '.$receivedData.'
          </tr>

		   <tr>
            <td colspan="2">';
      // if(AR_PRINT == $_SESSION["office"] && $_GET['mode'] == 'incoming'):
      	echo '<div style="display: flex"><a href="reportAcknowledgementReceipt.php?docid='.$rsCurrentAction[$ctr]['documentId'].'&from='.$currentActionSender.'&to='.$currentActionRecipient.'&dateSent='.$rsCurrentAction[$ctr]['dateSent'].'" target="_"><span class="ui-icon ui-icon-print" title="Print Acknowledgement Receipt"></span></a>';
      	echo '<a href="reportAcknowledgementReceipt.php?docid='.$rsCurrentAction[$ctr]['documentId'].'&from='.$currentActionSender.'&to='.$currentActionRecipient.'&dateSent='.$rsCurrentAction[$ctr]['dateSent'].'" onclick="window.open(\'showEsignature.php?docid='.$rsCurrentAction[$ctr]['documentId'].'&historyid='.$rsCurrentAction[$ctr]['historyId'].'&recdata='.$receivedData.'\', \'newwindow\', \'width=555,height=400\'); 
              return false;"><span class="ui-icon ui-icon-pencil" title="View E-sig"></span></a></div>';
      // endif;
      echo '
            <hr></td>
          </tr>
		  ';//$rsCurrentAction[$ctr]['remarks']
		  
			}
			?>
			</table>
			

			</div>
		</td>
		</tr>
		</table>
		</td>
      </tr>
    </table></td>
  </tr>
</table>
<?php }else { echo "No actions taken yet."; } ?>