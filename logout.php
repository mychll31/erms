<?php
session_start();
session_destroy();
if(isset($_COOKIE['ermsUserName']) && isset($_COOKIE['ermsPass'])){
setcookie("ermsUserName", "", time()-100*60*24*100, "/");
setcookie("ermsPass", "", time()-100*60*24*100, "/");
setcookie("ermsUserType", "", time()-60*60*24*100, "/");
setcookie("ermsLoginName", "", time()-60*60*24*100, "/");
setcookie("ermsUserID", "", time()-60*60*24*100, "/");
setcookie("ermsUserUnit", "", time()-60*60*24*100, "/");
}

header ("Location: login.php");
?>