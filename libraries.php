<? 
include_once("class/Security.class.php");
$objSecurity = new Security;
$objSecurity->checkLibraryUserRights();
?>
<? include_once("includes.php");?>		
    <td width="764" valign="top">
		<div id="tabs">
			<ul style="font-size:9px; padding: .1em .1em 0;">
				<!--li><a href="#accounts" onClick="getData('showUserAccountsForm.php?mode=accounts','accounts');">User Accounts</a></li-->
				<!--li><a href="#doctype" onClick="getData('showDocumentTypeForm.php?mode=type','doctype');">Document Type</a></li-->
				<!--li><a href="#actTaken" onClick="getData('showActionTakenForm.php?mode=actTaken','actTaken');">Action Required</a></li-->
				<!--li><a href="#actReq" onClick="getData('showActionRequiredForm.php?mode=actReq','actReq');">Action Taken</a></li-->
				<li><a href="showOfficeForm.php?mode=office#office">Office</a></li>				
				<li><a href="showContainer.php?mode=container#contain">Cabinet</a></li>		<!--- edited by emma		-->
			    <? if($objSecurity->get("userType")==1) {?>
				<li><a href="showDocumentTypeForm.php?mode=doctype#doctype">Document Type</a></li>
				<li><a href="showActionRequiredForm.php?mode=actTaken#actTaken">Action Required</a></li>
				<li><a href="showActionTakenForm.php?mode=actReq#actReq">Action Taken</a></li>
				<li><a href="showBackUpForm.php?mode=backup#backup">Back Up</a></li-->
				<li><a href="showManageCustodian.php?mode=view#manCust">Manage Custodian</a></li>
				<li><a href="showOfficeGroups.php?mode=view#oGroups">Groups</a></li>
				<li style="width:100"><a href="showUserAccounts.php?mode=view#uAccounts">User Account</a></li>
				<? }?>
			</ul>
			<div id="office" align="center">
		
			</div>
			<!-- 	***************************		Incoming		    ********************************************	-->			
			<div id="doctype" align="center">
			</div>
			<!-- 	***************************		Outgoing		    ********************************************	-->						
			<div id="actTaken" align="center">
			</div>
			<!-- 	***************************		Intra Office		    ********************************************	-->			
			<div id="actReq" align="center">
			</div>
			<!-- 	***************************		Cabinet		    ********************************************	-->			
			<div id="contain" align="center"><!---- edited by emma-->
			</div>

			<!-- 	***************************		Reports		    ********************************************	-->			
			<div id="backup" align="center">
			</div>	
			<div id="manCust" align="center">
			</div>
			<div id="oGroups" align="center">
			</div>	
			<div id="uAccounts" align="center">
			</div>		
		</div>	
	</td>
