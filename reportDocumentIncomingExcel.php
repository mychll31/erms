<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ListOfIncoming.xls");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

include_once("class/Records.class.php");
$objRecordDetail = new Records;
        
include_once("class/DocDetail.class.php");
$objDocDetail = new DocDetail;
        
if($_SESSION['office']=='RMS'){
    $rsRecordDetail = $objRecordDetail->getDocListRMS($_SESSION['office'],0,$_GET['dateFrom'],$_GET['dateTo']);
}else{
    $rsRecordDetail = $objRecordDetail->getDocList($_SESSION['office']);
}

echo "DOST CENTRAL OFFICE<br>";
echo $objDocDetail->displayRecipientSenderNames($_SESSION['office'], $_SESSION['userUnit'])."<br>";

echo "<strong><center>LIST OF INCOMING RECORDS</center></strong><br>";
echo "<strong><center>(".date('d M Y',strtotime($_GET['dateFrom']))." TO ".date('d M Y',strtotime($_GET['dateTo'])).")</center></strong><br>";
?>


<table border="1">
    <thead>
        <tr bgcolor="#CCCCCC">
            <th ></th>
            <th width="100">DOC ID</th>
            <th width="150">DOC TYPE</th>
            <th>STATUS</th>
            <th>ORIGIN</th>
            <th>SUBJECT</th>
            <th>DATE ENCODED</th>
        </tr>
    </thead>
    <tbody>
       <?php 
        if($_SESSION['office']!='RMS'):
            for($i=0;$i<count($rsRecordDetail);$i++) 
            {                                   
            
                if($_GET['doctype']=='all'){
                    if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==0)
                    {
                        $ctr++;
                        $arrData = array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']);
                        echo '<tr>';
                            foreach($arrData as $data):
                                echo '<td>'.$data.'</td>';
                            endforeach;
                        echo '</tr>';
                    }
                }else{
                    if($rsRecordDetail[$i]['dateAdded']>=$_GET['dateFrom'] && $rsRecordDetail[$i]['dateAdded']<=$_GET['dateTo'] && $rsRecordDetail[$i]['status']==0 && $rsRecordDetail[$i]['documentTypeId']==$_GET['doctype'])
                    {
                        $ctr++;
                        $arrData = array($ctr,$rsRecordDetail[$i]['documentId'], $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),$objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),$objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),$rsRecordDetail[$i]['subject'],$rsRecordDetail[$i]['dateAdded']);
                        echo '<tr>';
                            foreach($arrData as $data):
                                echo '<td>'.$data.'</td>';
                            endforeach;
                        echo '</tr>';
                    }   
                }
            }
        // if RMS
        else:
            $rowctr=0;
            for($i=0;$i<count($rsRecordDetail);$i++) 
            {
                $ctr++; 
                if($_GET['doctype']=='all'){
                    $arrData = array($ctr,
                                $rsRecordDetail[$i]['documentId'],
                                $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),
                                $objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),
                                $objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),
                                $rsRecordDetail[$i]['subject'],
                                $rsRecordDetail[$i]['dateAdded']);
                    echo '<tr>';
                        foreach($arrData as $data):
                            echo '<td>'.$data.'</td>';
                        endforeach;
                    echo '</tr>';
                    
                }else{
                    if($rsRecordDetail[$i]['documentTypeId']==$_GET['doctype']){
                        $rowctr++;
                        $arrData = array($rowctr,
                                $rsRecordDetail[$i]['documentId'],
                                $objRecordDetail->getDocumentType($rsRecordDetail[$i]['documentTypeId']),
                                $objRecordDetail->getDocumentStatus($rsRecordDetail[$i]['status']),
                                $objDocDetail->displayRecipientSenderNames($rsRecordDetail[$i]['originId'],$rsRecordDetail[$i]['originUnit']),
                                $rsRecordDetail[$i]['subject'],
                                $rsRecordDetail[$i]['dateAdded']);
                        echo '<tr>';
                            foreach($arrData as $data):
                                echo '<td>'.$data.'</td>';
                            endforeach;
                        echo '</tr>';
                        
                    }
                }
            }
        endif;
        ?>
    </tbody>
    
</table>
