<?php  @session_start();
include_once("class/ManageContainer.class.php");
$objContainer = new ManageContainer;

$usrOffice = $_SESSION['grp']==""?$_SESSION['office']:$_SESSION['grp'];
$arrCabinet = $objContainer->getCabinet($usrOffice);

	if($_REQUEST["cabinet"]!="" OR $_REQUEST["mode"]=="showDrawer")
	{	
		getDraw($_REQUEST["cabinet"]);
		die();
	}
	
	if($_REQUEST["mode"]=="showCabinet")
	{	
		getCab($usrOffice);
		exit(1);
	}
	
function getDraw($cab)
{
	$objContainer = new ManageContainer;
	$arrDrawer=$objContainer->getDrawer($cab);
		
		echo '<select id="drawer" name="drawer">';
		$c2 = sizeof($arrDrawer);
		for($i2=0;$i2<$c2;$i2++)
		{
		?>		
			<option value="<? echo $arrDrawer[$i2]["container2Id"];?>" ><? echo $arrDrawer[$i2]["label"];?></option>";
		<?
		}
		echo "</select>";
	
}	
function getCab($office)
{
	$objContainer = new ManageContainer;
	$arrCabinet=$objContainer->getCabinet($office);
	//echo $office;
    ?>

  	<select name="cabinet" id="cabinet" onChange="changeDrawer();">
		<option value="-1">&nbsp;</option>
		<?
			for($i=0;$i<sizeof($arrCabinet);$i++) 
				{?>	
                <OPTION value="<? echo $arrCabinet[$i]['containerId'];?>"><? echo $arrCabinet[$i]['label'];?></OPTION> 
			<?	}?>
		
	</select>
<?
}
?>


<script language="javascript" type="text/javascript"> 
var validator='';
  jQuery("#conRefresh").click( function(){ 
    //if($("#cabEdit").attr('value') == 'Save') $("#cabEdit").attr('value','Edit');
	jQuery('#containerGrid').GridUnload();
	jQuery(document).ready(function(){
	jQuery("#containerGrid").jqGrid({
		url:'xmlparser.php?nd='+new Date().getTime(),
		datatype: "xml",
		colNames:['Container ID','Cabinet','Drawer ID','Drawer', 'Container'],
		colModel:[
			{name:'containerId',index:'cabinet', width:50,hidden:true},
			{name:'cabinet',index:'cabinet', width:65},
			{name:'drawerId',index:'drawerId', width:65,hidden:true},
			{name:'drawer',index:'drawer', width:90},
			{name:'container',index:'container', width:100}
		],
		rowNum:10,
		width: 700,
		rowList:[10,20,30,'All'],
		imgpath: gridimgpath,
		multiselect: true,
		pager: jQuery('#conPager'),
		postData:{table:'tblContainer3',searchField:'officeCode',searchString:"<? echo $_SESSION['office']; ?>",searchOn:'true',searchOper:'ew'},
		sortname: 'cabinet',
		viewrecords: true,
		sortorder: "asc", //desc
		caption:"File Cabinet"
	}).navGrid('#conPager',{edit:false,add:false,del:false});}); 
  });
  
    $('#conClear').click( function(){ 
	   $("#conId").val(''); 	   
	   $("#conLabel").val(''); 
	   $("#cabinet").val(''); 
	   changeDrawer();
	   $("#conSave").css('display','none');
	   $("#conAdd").css('display','block');
	   $('#Clear_cabinet').text(" Clear ");
	   $('#conRefresh').click();
	   validator.resetForm();
    });	
	
//Edit & Save function	
	jQuery("#conEdit").click( function(){ 
	  var id = jQuery("#containerGrid").getGridParam('selrow'); 
		
	  if (id) {
	   var ret = jQuery("#containerGrid").getRowData(id); 
	   $("#conId").val(ret.containerId); 	   
	   $("#conLabel").val(ret.container); 
	   $("#cabinet").val(ret.cabinet); 
	   changeDrawer(ret.drawer);
		
   	   //$("#drawer").val(ret.drawer); 
	   if($("#conEdit span").attr('class') == "ui-icon ui-icon-pencil")
	   {
   	    $("#conSave").css('display','block');
		$("#conAdd").css('display','none');
		$('#Clear_cabinet').text(" Cancel ");
	   }}
	   else { jAlert('Please select row to edit', 'Warning');} 
	}); 
	
	
	jQuery("#conSave").click( function(){ 
	  var conId = $("#conId").val();
 	  var label = $("#conLabel").val();
   	  var ofCode = $("#drawer").val();
	    $("#containerGrid").setPostData({mode:"save",table:'tblContainer3',ID:conId,CODE:ofCode,DESC:label,MODULE:'Container'});
	    $("#containerGrid").trigger("reloadGrid");
 	    $('#conClear').click();
		jAlert('Succesfully Saved','Information');
	});
//onchange function for drawer
function changeDrawer(drawer="")
{
	var cabinet = $("#cabinet").val();
   	//getData("showContainer.php?cabinet="+cabinet,"drawerDiv");
	$.ajax ({
			type: "GET",
			url: "showContainer.php?cabinet="+cabinet,
			cache: false,
			success: function(html)
				{
					$('#drawerDiv').html(html);
					$("#drawer").val(drawer); 
					//alert(html);
					//window.location.replace(strUrl+"user");
					//$(location).attr(strUrl+'/user/');
				} 
		});	
	//alert(drawer);
	
	
}
//Add function	   
	//$("#cabAdd").click( function(){ 
	function AddRec(){
	  var cabId  = $("#conId").val();
 	  var label = $("#conLabel").val();
   	  var ofCode = $("#drawer").val();
	  
      $("#containerGrid").setPostData({mode:"add",table:'tblContainer3',ID:cabId,CODE:ofCode,DESC:label,MODULE:'Container3'});
	  $("#containerGrid").trigger("reloadGrid");
  	  $('#conClear').click();
	  jAlert('Succesfully Added','Information'); }
	 
	 
	$("#conDel").click(function(){ 
	 var id = jQuery("#containerGrid").getGridParam('selarrrow'); //selrow - for 1 row

	 if(id!= ''){ 
  	  if(id == '1'){ 
	   jAlert('You can not delete this record', 'Warning');
	   $('#conClear').click(); }
      else{
		 jConfirm('Proceed deleting this record?',false, 'ERMS Confirmation Dialog', function(r) {
		 if(r==true)
		 	{
			  $("#containerGrid").setPostData({mode:"del",table:'tblContainer3',ID:id.toString(),MODULE:'Cabinet'});
			  $("#containerGrid").trigger("reloadGrid"); 
			  $('#conClear').click();
			  jAlert('Succesfully deleted', 'Confirmation Results'); 
			}
		  }); 
		  } 
	  }
	  else jAlert('Please select row to delete', 'Warning');  });

     $('#conRefresh').click();
 	 $('#conAdd').click(function() {
      $('#frmContainer').submit();
     });

//Form validation
 	$().ready(function() {
	 validator = $("#frmContainer").validate({
		rules: { conLabel: "required" },
		messages: {	conLabel: "Cabinet Label is required" },
		errorPlacement: function(error, element) {
			if ( element.is(":radio") )
				error.appendTo( element.parent().next().next() );
			else if ( element.is(":checkbox") )
				error.appendTo ( element.next() );
			else
				error.appendTo( element.parent().next() );
		},
		submitHandler: function() {
			AddRec();
		},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		}
	});
 });	 	 
</script>

<style type="text/css">
	.cabinetFolder{
		background: url('images/folder/folder.gif') 0 0 no-repeat;
		padding: 1px 0 1px 16px;
		display: block;
	}
	.collapseFolder{
		background: url('images/folder/treeview-famfamfam.gif') -64px -25px no-repeat;
		height: 16px;
		width: 16px;
		margin-left: -16px;
		float: left;
		cursor: pointer;
	}
	.labelcabinet {
		padding-left: 10px;
	}
	.labeldrawer {
		padding-left: 20px !important;
	}
	.labelcont {
		padding-left: 30px !important;
	}
	select#cabinet, select#drawer, input#conLabel {
		width: 152px !important;
	}
	/*div#gbox_containerGrid {
		display: none;
	}*/
</style>

<?php


?>
<form id="frmContainer" autocomplete="off" method="get">
<table>
  <!--DWLayoutTable-->
  <tr> 
    <td class="label labelcabinet"><div class="collapseFolder"></div><span class="cabinetFolder">Cabinet&nbsp;</span></td>
    <td class="field">
	    <div id="cabinetDiv">
    		<?php
            getCab($usrOffice);
			?>
        </div>
    </td>
	<td class="status"> <span class='ui-icon ui-icon-plus' title='Add Cabinet' onclick="displayDialog('showCabinet_popup.php?mode=cabinet');"></span></td>
	<input type="hidden" id="cabId" value="">

  </tr>
  
   <tr> 
    <td class="label labeldrawer"><div class="collapseFolder"></div><span class="cabinetFolder">Drawer&nbsp;</span></td>
    <td class="field">
    	<div id="drawerDiv">
	    	<select id="drawer" name="drawer">
			</select>
        </div>
    </td>
	<td class="status"> <span class='ui-icon ui-icon-plus' title='Add Drawer' onclick="displayDialog('showCabinet_popup.php?mode=drawer');"></span></td>
	<input type="hidden" id="drawId" value="">
  </tr>
  
   <tr> 
    <td class="label labelcont"><span class="cabinetFolder">Container&nbsp;</span></td>
    <td class="field"><input type="text" id="conLabel" name="conLabel" /></td>
	<td class="status"> </td>
	<input type="hidden" id="conId" value="">
  </tr>
   
 <tr>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="conEdit"><span class='ui-icon ui-icon-pencil' title='edit'></span></li>  
   <li class='ui-state-default ui-corner-all' id="conDel"><span class='ui-icon ui-icon-trash' title='delete'></span></li>
  </ul>
  </td>
  <td class="field">
  <ul id='icons' class='ui-widget ui-helper-clearfix'>
   <li class='ui-state-default ui-corner-all' id="conAdd"><span title='add'></span>&nbsp;Add&nbsp;</li> 
   <li class='ui-state-default ui-corner-all' id="conSave" style="display:none"><span title='save' ></span>&nbsp;Save&nbsp;</li>
   <li class='ui-state-default ui-corner-all' id="conClear"><span title='Clear'></span><div id="Clear_cabinet">&nbsp;Clear&nbsp;</div></li>
   <li class='ui-state-default ui-corner-all' id="conRefresh" style="display:none"><span title='refresh'></span>refresh</li>
  </ul>
  </td>
 </tr>  
</table>
<table id="containerGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="conPager" class="scroll" style="text-align:center;"></div>
</form>			