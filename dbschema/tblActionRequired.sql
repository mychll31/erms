-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 02:15 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dberms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblActionRequired`
--

CREATE TABLE IF NOT EXISTS `tblActionRequired` (
  `actionCodeId` int(5) NOT NULL AUTO_INCREMENT,
  `actionDesc` varchar(100) NOT NULL DEFAULT '',
  `actionCode` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`actionCodeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tblActionRequired`
--

INSERT INTO `tblActionRequired` (`actionCodeId`, `actionDesc`, `actionCode`) VALUES
(1, 'For Action', 'ACT'),
(2, 'For Advice/Clearance', 'ADV'),
(3, 'For Approval', 'APP'),
(4, 'For Compliance', 'CMP'),
(5, 'For Comments/Review', 'COM'),
(6, 'For Consideration', 'CON'),
(7, 'For Discussion', 'DSC'),
(8, 'For Endorsement', 'END'),
(9, 'For Follow-up', 'FFU'),
(10, 'For File', 'FIL'),
(11, 'For Processing', 'FPR'),
(12, 'For Information', 'INF'),
(13, 'For Posting', 'PST'),
(14, 'For Recommendation', 'RCM'),
(15, 'For Referral', 'REF'),
(16, 'For Revision', 'REV'),
(17, 'For Signature/Approv', 'SIG'),
(18, 'For Study', 'STD'),
(20, 'For Release', 'REL'),
(21, 'FDRST', 'For Distribution'),
(22, 'For Distribution', 'DISTRI'),
(23, 'For Pick-up', 'PUP'),
(35, 'Pre-Examined', 'Pre-Examined'),
(34, 'libcap', 'libcap'),
(26, 'VER', 'For Verification'),
(27, 'For confirmation', 'For confirmation'),
(28, 'JEV', 'JEV'),
(29, 'For Obligation', 'For Obligation'),
(30, 'For Consolidation', 'For Consolidation'),
(31, 'For Dissemination', 'For Dissemination'),
(32, 'For Mailing', 'For Mailing'),
(33, 'Deliberation', 'Deliberation'),
(36, 'For Coordination', 'coor'),
(37, 'For Confirmation', 'For Confirmation'),
(38, 'For Certification', 'For Certification'),
(39, 'For Concurrence', 'For Concurrence'),
(40, 'For Editing', 'For Editing'),
(41, 'For Submission', 'For Submission'),
(42, 'For Initial', 'For Initial'),
(43, 'For Validation', 'For Validation'),
(44, 'Return', 'Return'),
(45, 'For Review', 'For Review'),
(46, 'For ONAR', 'For ONAR'),
(47, 'For LDDAP-ADA', 'For LDDAP-ADA'),
(48, 'for funds availabili', 'funding'),
(49, 'cc:', 'cc:'),
(50, 'For LOI', 'For LOI'),
(51, 'For Email', 'For Email'),
(52, 'For Order of Payment', 'For Order of Payment'),
(53, 'For SAA Preparation', 'For SAA Preparation'),
(54, 'For Information (LEGAL)', 'FILEGAL'),
(57, 'For Submission of Re', 'For Submission of Re'),
(56, 'For Reporting', 'REP'),
(58, 'For publication in the Official Gazzette', 'FBOG'),
(59, 'For Publication in News Paper of General Circulation', 'FBNPGC'),
(60, 'For Deposit in the NAP', 'FDN'),
(61, 'For Filing in the UP Law Center', 'FFUP'),
(62, 'For Filing in the HOR-LLAM', 'FFHOR'),
(63, 'For Posting in the Bulletin Board', 'FPBB');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
