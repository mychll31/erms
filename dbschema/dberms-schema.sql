-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 01:04 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dberms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl201Documents`
--

CREATE TABLE IF NOT EXISTS `tbl201Documents` (
  `documentId` varchar(35) NOT NULL DEFAULT '',
  `empNumber` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblActionRequired`
--

CREATE TABLE IF NOT EXISTS `tblActionRequired` (
  `actionCodeId` int(5) NOT NULL AUTO_INCREMENT,
  `actionDesc` varchar(100) NOT NULL DEFAULT '',
  `actionCode` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`actionCodeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblActionTaken`
--

CREATE TABLE IF NOT EXISTS `tblActionTaken` (
  `actionCodeId` int(5) NOT NULL AUTO_INCREMENT,
  `actionDesc` varchar(100) NOT NULL DEFAULT '',
  `actionCode` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`actionCodeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblBackup`
--

CREATE TABLE IF NOT EXISTS `tblBackup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) NOT NULL DEFAULT '',
  `backupDate` varchar(50) NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblBackupConfig`
--

CREATE TABLE IF NOT EXISTS `tblBackupConfig` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `time_interval` double DEFAULT NULL,
  `ftp` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(20) NOT NULL DEFAULT '',
  `fname` varchar(20) NOT NULL DEFAULT '',
  `time_counter` varchar(10) NOT NULL DEFAULT '',
  `location` varchar(100) NOT NULL DEFAULT '',
  `time_last_run` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblcalendar`
--

CREATE TABLE IF NOT EXISTS `tblcalendar` (
  `cal_id` int(11) NOT NULL AUTO_INCREMENT,
  `cal_title` varchar(500) DEFAULT NULL,
  `cal_dateStart` date DEFAULT NULL,
  `cal_dateEnd` date DEFAULT NULL,
  `cal_timeStart` time DEFAULT NULL,
  `cal_timeEnd` time DEFAULT NULL,
  `cal_days` int(11) DEFAULT NULL,
  `cal_isAllday` int(11) NOT NULL DEFAULT '0',
  `cal_bgcolor` varchar(100) DEFAULT NULL,
  `cal_url` varchar(250) DEFAULT NULL,
  `cal_addedby` int(11) DEFAULT NULL,
  `cal_addedDate` datetime DEFAULT NULL,
  `cal_lastUpdatedBy` int(11) DEFAULT NULL,
  `cal_lastUpdatedDate` datetime DEFAULT NULL,
  `cal_isRemove` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblChangeLog`
--

CREATE TABLE IF NOT EXISTS `tblChangeLog` (
  `logId` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(15) NOT NULL DEFAULT '',
  `officeCode` varchar(10) NOT NULL DEFAULT '',
  `empNumber` varchar(20) NOT NULL DEFAULT '',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sql` text NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT '',
  `module` varchar(50) NOT NULL DEFAULT '',
  `section` varchar(30) NOT NULL DEFAULT '',
  `page` varchar(50) NOT NULL DEFAULT '',
  `pageValues` text NOT NULL,
  `remarks` text NOT NULL,
  `empID` varchar(20) NOT NULL,
  PRIMARY KEY (`logId`),
  UNIQUE KEY `logId` (`logId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12806 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblContainer`
--

CREATE TABLE IF NOT EXISTS `tblContainer` (
  `containerId` int(15) NOT NULL AUTO_INCREMENT,
  `label` varchar(20) NOT NULL DEFAULT '',
  `officeCode` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`containerId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblContainer2`
--

CREATE TABLE IF NOT EXISTS `tblContainer2` (
  `container2Id` int(10) NOT NULL AUTO_INCREMENT,
  `container` int(10) NOT NULL,
  `label` varchar(200) NOT NULL,
  PRIMARY KEY (`container2Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=242 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblContainer3`
--

CREATE TABLE IF NOT EXISTS `tblContainer3` (
  `container3Id` int(10) NOT NULL AUTO_INCREMENT,
  `container2` int(10) NOT NULL,
  `label` varchar(200) NOT NULL,
  PRIMARY KEY (`container3Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblControl`
--

CREATE TABLE IF NOT EXISTS `tblControl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblControlledDocument`
--

CREATE TABLE IF NOT EXISTS `tblControlledDocument` (
  `controlledId` int(11) NOT NULL AUTO_INCREMENT,
  `documentId` varchar(200) DEFAULT NULL,
  `controlleddoc` int(11) DEFAULT NULL COMMENT 'maintained, retained, forms, references',
  `copyno` varchar(200) DEFAULT NULL,
  `copyholder` varchar(200) DEFAULT NULL,
  `mannerOfDisposal` varchar(200) DEFAULT NULL,
  `revisionno` varchar(200) DEFAULT NULL,
  `personUnitResponsible` varchar(200) DEFAULT NULL,
  `withrawalno` varchar(200) DEFAULT NULL,
  `addedby_unit` varchar(200) DEFAULT NULL,
  `addedby_office` varchar(200) DEFAULT NULL,
  `lastupdated_by` varchar(200) DEFAULT NULL,
  `lastupdated_date` datetime DEFAULT NULL,
  UNIQUE KEY `documentId` (`documentId`,`addedby_office`),
  KEY `controlledId` (`controlledId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78334 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblCustodian`
--

CREATE TABLE IF NOT EXISTS `tblCustodian` (
  `custodianId` int(5) NOT NULL AUTO_INCREMENT,
  `officeCode` varchar(20) NOT NULL DEFAULT '',
  `empNumber` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `groupCode` varchar(20) NOT NULL DEFAULT '',
  `admin` char(1) NOT NULL DEFAULT '',
  `auto_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '',
  `empID` varchar(10) NOT NULL,
  PRIMARY KEY (`custodianId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=542 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblDisposal`
--

CREATE TABLE IF NOT EXISTS `tblDisposal` (
  `disposalid` int(11) NOT NULL AUTO_INCREMENT,
  `documentId` varchar(50) DEFAULT NULL,
  `disposeRemark` text,
  `userid` varchar(50) DEFAULT NULL,
  `disposal_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`disposalid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblDocument`
--

CREATE TABLE IF NOT EXISTS `tblDocument` (
  `documentId` varchar(35) NOT NULL DEFAULT '',
  `referenceId` varchar(225) DEFAULT NULL,
  `docNum` varchar(50) DEFAULT NULL,
  `documentTypeId` int(5) NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `documentDate` date DEFAULT '0000-00-00',
  `officeSig` varchar(15) NOT NULL DEFAULT '',
  `originId` varchar(20) DEFAULT '0',
  `originGroupId` varchar(20) NOT NULL DEFAULT '',
  `originUnit` varchar(50) NOT NULL DEFAULT '',
  `sender` varchar(50) NOT NULL DEFAULT '',
  `deadline` date DEFAULT '0000-00-00',
  `remarks` text,
  `fileContainer` int(15) DEFAULT NULL,
  `modeofDelivery` varchar(15) DEFAULT NULL,
  `addedById` varchar(20) NOT NULL DEFAULT '0',
  `old_addedById` varchar(20) NOT NULL,
  `addedByOfficeId` varchar(20) NOT NULL DEFAULT '0',
  `dateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dateReceived` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `confidential` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `folderId` int(11) DEFAULT '0',
  `isDelete` tinyint(4) NOT NULL DEFAULT '0',
  `isDispose` tinyint(1) NOT NULL DEFAULT '0',
  `disposeRemark` text NOT NULL,
  `empID` varchar(20) NOT NULL,
  `fileContainer2` int(10) NOT NULL,
  `fileContainer3` int(10) NOT NULL,
  `iscomplete` int(11) DEFAULT '0',
  `datecomplete` datetime DEFAULT NULL,
  `completed_by` varchar(50) DEFAULT NULL,
  `completed_byOffice` varchar(100) DEFAULT NULL,
  `controlleddoc` int(11) DEFAULT NULL,
  `contdoc_copyno` varchar(100) DEFAULT NULL,
  `contdoc_copyHolder` varchar(100) DEFAULT NULL,
  `contdoc_mannerOfDisposal` varchar(100) DEFAULT NULL,
  `contdoc_revisionno` varchar(100) DEFAULT NULL,
  `contdoc_personUnitResposible` varchar(100) DEFAULT NULL,
  `doc_code_retained` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`documentId`),
  FULLTEXT KEY `index_text` (`documentId`,`subject`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblDocumentFolders`
--

CREATE TABLE IF NOT EXISTS `tblDocumentFolders` (
  `folderID` int(11) NOT NULL DEFAULT '0',
  `documentId` varchar(35) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblDocumentReceived`
--

CREATE TABLE IF NOT EXISTS `tblDocumentReceived` (
  `receivedId` int(11) NOT NULL AUTO_INCREMENT,
  `historyId` int(20) NOT NULL DEFAULT '0',
  `empNumber` varchar(20) NOT NULL DEFAULT '',
  `documentId` varchar(35) NOT NULL DEFAULT '',
  `dateReceived` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `receivedBy` varchar(100) NOT NULL DEFAULT '',
  `empID` varchar(20) NOT NULL,
  PRIMARY KEY (`receivedId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=385493 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblDocumentType`
--

CREATE TABLE IF NOT EXISTS `tblDocumentType` (
  `documentTypeId` int(5) NOT NULL AUTO_INCREMENT,
  `documentTypeAbbrev` varchar(50) NOT NULL DEFAULT '',
  `documentTypeDesc` varchar(100) NOT NULL DEFAULT '',
  `retentionPeriod` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documentTypeId`),
  FULLTEXT KEY `index_text` (`documentTypeAbbrev`,`documentTypeDesc`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=428 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblEmpDTR`
--

CREATE TABLE IF NOT EXISTS `tblEmpDTR` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empNumber` varchar(15) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `dtrDate` date NOT NULL DEFAULT '0000-00-00',
  `inAM` time NOT NULL DEFAULT '00:00:00',
  `outAM` time NOT NULL DEFAULT '00:00:00',
  `inPM` time NOT NULL DEFAULT '00:00:00',
  `outPM` time NOT NULL DEFAULT '00:00:00',
  `inOT` time NOT NULL DEFAULT '00:00:00',
  `outOT` time NOT NULL DEFAULT '00:00:00',
  `remarks` varchar(255) NOT NULL DEFAULT '',
  `otherInfo` varchar(255) NOT NULL DEFAULT '',
  `OT` int(1) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `ip` text NOT NULL,
  `editdate` text NOT NULL,
  `perdiem` char(1) NOT NULL DEFAULT '',
  `oldValue` text,
  PRIMARY KEY (`id`),
  KEY `idx_dtrDate` (`dtrDate`),
  KEY `idx_empNumber` (`empNumber`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105576 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblFIle`
--

CREATE TABLE IF NOT EXISTS `tblFIle` (
  `fileId` int(10) NOT NULL AUTO_INCREMENT,
  `documentId` varchar(35) NOT NULL DEFAULT '',
  `filename` text NOT NULL,
  `size` int(20) DEFAULT '0',
  `path` varchar(150) NOT NULL DEFAULT '',
  `uploadedByID` varchar(20) NOT NULL DEFAULT '',
  `old_uploadedByID` varchar(20) NOT NULL,
  `uploadDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `empID` varchar(20) NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `documentId` (`documentId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135616 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblFolders`
--

CREATE TABLE IF NOT EXISTS `tblFolders` (
  `folderID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` varchar(15) NOT NULL DEFAULT '0',
  `folderName` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`folderID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblGroup`
--

CREATE TABLE IF NOT EXISTS `tblGroup` (
  `groupId` int(4) NOT NULL AUTO_INCREMENT,
  `officeCode` varchar(20) NOT NULL DEFAULT '',
  `groupCode` varchar(20) NOT NULL DEFAULT '',
  `groupName` text NOT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblHistory`
--

CREATE TABLE IF NOT EXISTS `tblHistory` (
  `historyId` int(20) NOT NULL AUTO_INCREMENT,
  `referenceId` int(20) DEFAULT '0',
  `documentId` varchar(35) NOT NULL DEFAULT '',
  `senderId` varchar(20) NOT NULL DEFAULT '',
  `senderUnit` varchar(30) DEFAULT NULL,
  `actionTakenCodeID` int(10) NOT NULL DEFAULT '0',
  `dateSent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recipientId` varchar(20) NOT NULL DEFAULT '',
  `recipientDeadline` date DEFAULT NULL,
  `mode` char(3) NOT NULL DEFAULT '',
  `recipientUnit` varchar(50) NOT NULL DEFAULT '',
  `receivedBy` varchar(100) DEFAULT NULL,
  `dateReceived` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remarks` text NOT NULL,
  `actionCodeId` int(5) NOT NULL DEFAULT '0',
  `addedById` varchar(20) NOT NULL DEFAULT '0',
  `old_addedById` varchar(20) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `restricted` char(1) NOT NULL DEFAULT '0',
  `reply` char(1) NOT NULL DEFAULT '',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `empID` varchar(30) NOT NULL,
  PRIMARY KEY (`historyId`),
  KEY `documentId` (`documentId`,`receivedBy`,`actionCodeId`,`addedById`),
  KEY `receivedById` (`receivedBy`),
  KEY `addedById` (`addedById`),
  KEY `actionCodeId` (`actionCodeId`),
  KEY `historyId` (`historyId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=727123 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblLocation`
--

CREATE TABLE IF NOT EXISTS `tblLocation` (
  `locationid` int(11) NOT NULL AUTO_INCREMENT,
  `documentId` varchar(200) NOT NULL DEFAULT '',
  `cabinetId` int(11) DEFAULT NULL,
  `drawerId` int(11) DEFAULT NULL,
  `containerId` int(11) DEFAULT NULL,
  `addedby_unit` varchar(200) DEFAULT NULL,
  `addedby_office` varchar(200) DEFAULT NULL,
  `lastupdate_by` varchar(200) DEFAULT NULL,
  `lastupdated_date` datetime DEFAULT NULL,
  UNIQUE KEY `documentId` (`documentId`,`addedby_office`),
  KEY `locationid` (`locationid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28093 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblNotifyEmail`
--

CREATE TABLE IF NOT EXISTS `tblNotifyEmail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empNumber` varchar(20) NOT NULL DEFAULT '',
  `empID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblOriginOffice`
--

CREATE TABLE IF NOT EXISTS `tblOriginOffice` (
  `originId` int(5) NOT NULL AUTO_INCREMENT,
  `officeName` text,
  `contact` varchar(20) DEFAULT NULL,
  `address` varchar(70) DEFAULT NULL,
  `contactPerson` varchar(50) DEFAULT NULL,
  `ownerOffice` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`originId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1829 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblReadHistory`
--

CREATE TABLE IF NOT EXISTS `tblReadHistory` (
  `documentId` varchar(50) NOT NULL DEFAULT '',
  `userId` varchar(50) NOT NULL DEFAULT '',
  `old_userId` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblSignature`
--

CREATE TABLE IF NOT EXISTS `tblSignature` (
  `historyId` int(11) NOT NULL,
  `documentId` varchar(20) NOT NULL,
  `signature_location` varchar(200) NOT NULL,
  KEY `historyId` (`historyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblTrash`
--

CREATE TABLE IF NOT EXISTS `tblTrash` (
  `userID` varchar(20) NOT NULL DEFAULT '',
  `historyId` int(20) NOT NULL DEFAULT '0',
  `isDelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblzipcode`
--

CREATE TABLE IF NOT EXISTS `tblzipcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode` varchar(100) NOT NULL,
  `place` text NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lng` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5434 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblzipcodes`
--

CREATE TABLE IF NOT EXISTS `tblzipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ZIPCode` varchar(5) NOT NULL DEFAULT '',
  `Area` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2229 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
