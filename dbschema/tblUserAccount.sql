-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 01:14 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ermsApr2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblUserAccount`
--

CREATE TABLE IF NOT EXISTS `tblUserAccount` (
  `userId` int(4) NOT NULL AUTO_INCREMENT,
  `empNumber` varchar(20) NOT NULL DEFAULT '',
  `surname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `middleInitial` varchar(10) NOT NULL DEFAULT '',
  `nameExtension` varchar(10) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL,
  `officeCode` varchar(20) NOT NULL DEFAULT '',
  `agencyCode` int(10) NOT NULL,
  `userName` varchar(20) NOT NULL DEFAULT '',
  `userPassword` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

--
-- Dumping data for table `tblUserAccount`
--

INSERT INTO `tblUserAccount` (`userId`, `empNumber`, `surname`, `firstname`, `middleInitial`, `nameExtension`, `email`, `officeCode`, `agencyCode`, `userName`, `userPassword`) VALUES
(167, '100', 'admin', 'admin', 'a', '', 'admin@sample.com', '', 0, 'admin', '62da2f5c0c17429d265c858a5d2960b6');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
