-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 02:15 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dberms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblActionTaken`
--

CREATE TABLE IF NOT EXISTS `tblActionTaken` (
  `actionCodeId` int(5) NOT NULL AUTO_INCREMENT,
  `actionDesc` varchar(100) NOT NULL DEFAULT '',
  `actionCode` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`actionCodeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `tblActionTaken`
--

INSERT INTO `tblActionTaken` (`actionCodeId`, `actionDesc`, `actionCode`) VALUES
(1, 'Adviced', 'ADVD'),
(48, 'For Email', 'For Email  '),
(3, 'Complied', 'CMPD'),
(4, 'Commented/Reviewed', 'COMD'),
(7, 'Endorsed', 'ENDD'),
(36, 'Pre-Examined', 'Pre-Examined'),
(9, 'Filed', 'FILD'),
(10, 'Processed', 'FPRD'),
(11, 'Informed', 'INFD'),
(13, 'Recommended', 'RCMD'),
(14, 'Referred', 'REFD'),
(15, 'Revised', 'REVD'),
(16, 'Signed', 'SIGD'),
(37, 'For Editing', 'For Editing'),
(19, 'Released', 'REL'),
(22, 'Cleared', 'CLRD'),
(23, 'Received', 'RCVD'),
(24, 'For Approval', 'For Approval'),
(25, 'Conforme/ PO/WO', 'Conforme/ PO/WO'),
(26, 'Mailing', 'Mailing'),
(27, 'Lib-cap', 'Lib-cap'),
(28, 'VER', 'Verified'),
(29, 'For Post Audit', 'For Post Audit'),
(30, 'Verified', 'Verified'),
(31, 'For Info', 'For Info'),
(32, 'Prepared', 'Prepared'),
(33, 'Deliberation', 'Deliberation'),
(35, 'Returned', 'RET'),
(38, 'For Initial', 'For Initial'),
(39, 'For Validation', 'For Validation'),
(40, 'For Review', 'For Review'),
(41, 'For LDDAP-ADA', 'For LDDAP-ADA'),
(42, 'EDTD', 'edited'),
(43, 'edited', 'edited'),
(44, 'for funds availabili', 'funding'),
(45, 'FOR LOI', 'FOR LOI'),
(2, 'Approved', 'APPD'),
(49, 'Noted', 'Noted'),
(50, 'For Order of Payment', 'For Order of Payment'),
(51, 'For SAA Preparation', 'For SAA Preparation'),
(52, 'Forwarded', 'FWRD'),
(54, 'Reported', 'Reported'),
(55, 'Emailed', 'Emailed'),
(56, 'Released to DOST-CO ', 'Released to DOST-CO '),
(57, 'Released to Agencies', 'Released to Agencies'),
(58, 'Sent thru Email', 'Sent thru Email'),
(59, 'Sent thru Libcap', 'Sent thru Libcap'),
(60, 'Sent thru Postal', 'Sent thru Postal'),
(61, 'for posting', 'posting'),
(62, 'for posting', 'posting'),
(63, 'Published in the Official Gazzette', 'POG'),
(64, 'Published in News Paper of General Circulation', 'PNPGC'),
(65, 'Deposited in the NAP', 'PDN'),
(66, 'Published in the UP Law Center', 'PFUP'),
(67, 'Published in the HOR-LLAM', 'PHOR'),
(68, 'Published in the Bulletin Board', 'PBB');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
