-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 01:15 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ermsApr2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblCustodian`
--

CREATE TABLE IF NOT EXISTS `tblCustodian` (
  `custodianId` int(5) NOT NULL AUTO_INCREMENT,
  `officeCode` varchar(20) NOT NULL DEFAULT '',
  `empNumber` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `groupCode` varchar(20) NOT NULL DEFAULT '',
  `admin` char(1) NOT NULL DEFAULT '',
  `auto_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '',
  `empID` varchar(10) NOT NULL,
  PRIMARY KEY (`custodianId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=512 ;

--
-- Dumping data for table `tblCustodian`
--

INSERT INTO `tblCustodian` (`custodianId`, `officeCode`, `empNumber`, `groupCode`, `admin`, `auto_barcode`, `email`, `empID`) VALUES
(511, 'ADMIN', '100', '', '1', 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
