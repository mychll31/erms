-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2018 at 01:03 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dberms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblReports`
--

CREATE TABLE IF NOT EXISTS `tblReports` (
  `reportCode` varchar(10) NOT NULL DEFAULT '',
  `reportName` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`reportCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblReports`
--

INSERT INTO `tblReports` (`reportCode`, `reportName`) VALUES
('LRD', 'List of Records with Deadlines'),
('LIR', 'List of Incoming Records'),
('LOR', 'List of Outgoing Records'),
('NDP', 'Number of Documents Processed'),
('RA', 'Record Actions'),
('RP', 'Records Profile'),
('UD', 'Unacted Documents'),
('MMPR', 'Monthly Monitoring of Processed Records'),
('AR', 'List of Recorded Actions'),
('SRD', 'Summary of For Process Documents'),
('UDSD', 'Unacted Document by the Department'),
('BRCD', 'Record Registry'),
('IAO', 'List of Incoming and Outgoing Report'),
('MLMD', 'Masterlist of Maintained Documents'),
('MLF', 'Masterlist of Forms'),
('MLRD', 'Masterlist of Retained Documents'),
('MLER', 'Masterlist of External References'),
('MLIR', 'Masterlist of Internal References'),
('MLOD', 'Masterlist of Obsolete Documents'),
('RRDR', 'Records Register Daily Report'),
('DMPR', 'Daily Monitoring of Processed Records'),
('LRR', 'List of Received Documents'),
('MCSRR', 'Monitoring of Compliance with Satutory  and Regulatory Reports'),
('MPRFS', 'Monitoring of Process Records from Recipient to Sender'),
('MLOC', 'Masterlist of Confidential Documents'),
('CUSTOM1', 'Custom Report 1'),
('SOD', 'Summary of Documents');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
