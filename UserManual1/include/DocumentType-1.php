<p class="xbig"><strong>DOCUMENT TYPE</strong></p>

<p align="center"><img src="images/DocumentType.png" width= "400"/></p>

<p>
<ul>
	<li>To add a record, type the <strong>Document Type Abbreviation</strong>, <strong>Document Type Description</strong>, and <strong>Retention Period</strong> on the textboxes provided. You may click on the Clear button <img src="images/Office1.png" /> to erase the previously inputted information. Otherwise, click on the Add button <img src="images/Office2.png" /> to save the record. A confirmation message saying that the record has been added will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To edit a record, click on the check box at the left of the corresponding record to be edited, then click the Edit icon <img src="images/Office3.png" />. The information will load in the textboxes, perform the necessary changes then click on Save button <img src="images/Office4.png" /> to update the record. A confirmation message saying that the action is successful will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To delete a record, click on the checkbox at the left of the corresponding record to be deleted, then click the Delete icon <img src="images/Office5.png" />. A confirmation message saying that the action is successful will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To find records, click on the Find Records icon <img src="images/Office6.png" /> below the grid. This will allow you to search for a specific record. When clicked, a search window will appear. Supply the search from with specific information. Select the field from the first drop-down list. Provide the search value in the textbox provided then click Find button <img src="images/Office7.png" />. The matching records will be displayed on the grid.</li><br>
	<li>To reload the grid, simply click the Reload Grid icon <img src="images/Office8.png" />.</li><br>
	<li>To specify the number of records you want to be displayed at once, click the drop-down list and select an option.<br>
	<p align="center"><img src="images/Office9.png" width="350" /></p></li>
</ul>
</p>

<p align="right"><a href="documentType-Records-Officer.php">&uarr; Back To Top</a></p>