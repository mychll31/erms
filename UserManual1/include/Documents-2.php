<p class="big"><strong>Documents</strong></p>

<p>Clicking on the <strong>Documents</strong> option from the <strong>Folders</strong> menu allows the user to access functionalities on Documents Management (e.g. adding Incoming/Outgoing Documents, Generating Reports, etc.)</p>

<p align="center"><img src="images/DocumentsX2.png" width= "450" /></p>
