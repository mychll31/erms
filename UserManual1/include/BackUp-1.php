<p class="xbig"><strong>BACK UP</strong></p>

<p>By default, ERMS is scheduled to backup data once a week. But you may change the frequency of the automated backup to your preferred schedule or you may manually backup the database anytime you want to.</p>

<p align="center"><img src="images/BackUp1.png" width= "400"/></p>

<p>
<ul>
	<li>To modify the schedule of automated backup, click the <strong>scheduled</strong> link. You will be redirected to the Configure Database Backup Schedule form. Change the frequency of database backup to your desired schedule. You may modify the Optional Configuration part to your liking. When done, click Save button.<br>
	<p align="center"><img src="images/BackUp2.png" width="400" /></p></li>
	<li>To manually backup the database, click the <strong>Backup Now</strong> link. The backup file will instantly be listed on the grid.</li><br>
	<li>To download backup file, click the Download icon <img src="images/BackUp3.png" /> across the backup file you want to download.</li><br>
	<li>To delete backup file, click the Delete icon <img src="images/BackUp4.png" /> across the backup file you want to delete. An alert message will prompt confirming deletion of file, click OK button to proceed with delete, Cancel button otherwise.</li><br>
	<li>To find records, click on the Find Records icon <img src="images/Office6.png" /> below the grid. This will allow you to search for a specific record. When clicked, a search window will appear. Supply the search from with specific information. Select the field from the first drop-down list. Provide the search value in the textbox provided then click Find button <img src="images/Office7.png" />. The matching records will be displayed on the grid.</li><br>
	<li>To reload the grid, simply click the Reload Grid icon <img src="images/Office8.png" />.</li><br>
	<li>To specify the number of records you want to be displayed at once, click the drop-down list and select an option.<br>
	<p align="center"><img src="images/Office9.png" width="350" /></p></li>
</ul>
</p>

<p align="right"><a href="backUp-Records-Officer.php">&uarr; Back To Top</a></p>