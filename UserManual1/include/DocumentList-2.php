<p class="big"><strong>Document List</strong></p>

<p align="center"><img src="images/DocumentListX2.png" width= "400" /></p>

<p>The Document List tab displays the list of all documents associated with the user (regardless of its type or status). Arranged according to Document ID (<strong>DOCID</strong>), this tab also includes the following information: <strong>Subject</strong>, <strong>Status</strong>, Document Type (<strong>DOC TYPE</strong>), Document Date (<strong>DOC DATE</strong>), and <strong>Origin</strong>.</p>

<p>
<ul>
	<li><strong>Document ID</strong> � system-generated auto-incrementing document identification information.</li><br>
	<li><strong>Subject</strong> � subject matter of the document. This information is usually indicated in the printed copy of the document.</li><br>
	<li><strong>Status</strong> � classification of document as either Incoming or Outgoing.</li><br>
	<li><strong>Document Type</strong> � type of document.</li><br>
	<li><strong>Document Date</strong> � date specified in the document.</li><br>
	<li><strong>Origin</strong> � source of the document. This may refer to an individual, office or agency.</li><br>

</ul>
</p>
<p class="notebox"><strong>NOTE:</strong> <em>Documents can be arranged by clicking on the table header. For instance, clicking on the <strong>ORIGIN</strong> header link will arrange the document list alphabetically according to Document Origin.</em></p>