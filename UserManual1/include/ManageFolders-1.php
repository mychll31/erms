<p class="xbig"><strong>MANAGE FOLDERS</strong></p>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="boxyellow">
<tr>
	<td width="33%" align="center" style="border-right:thin #FFFF00 solid"><a href="#CNF">Create New Folder</a></td>
	<td width="34%" align="center" style="border-right:thin #FFFF00 solid"><a href="#RF">Rename Folder</a></td>
	<td width="33%" align="center"><a href="#DF">Delete Folder</a></td>
</tr>
</table>

<p>Manage Folders allows the user to manage folders where document scan be organized.</p>

<p>
<ul>
	<li>To manage folders, click the <strong>Manage Folders</strong> link from the Received Documents Screen.</li>
</ul>
</p>

<p align="center"><img src="images/ManageFolders.png" width="400" /></p>

<a name="CNF"></a>
<p class="big"><strong>Create New Folder</strong></p>

<p>
<ul>
	<li>To create new folder, click the <strong>Manage Folders</strong> link from the Received Documents Screen.</li><br>
	<li>Type the name of folder you want to create in the Create folder textbox. Then click <strong>Create</strong> button.<br>
		<p align="center"><img src="images/ManageFoldersA1.png" width="400" /></p>
	</li>
	<li>The created folder will automatically appear on the list below.<br>
		<p align="center"><img src="images/ManageFoldersA2.png" width="400" /></p>
	</li>
</ul>
</p>

<p align="right"><a href="manageFolders-Records-Officer.php">&uarr; Back To Top</a></p>

<a name="RF"></a>
<p class="big"><strong>Rename Folder</strong></p>

<p>
<ul>
	<li>To rename folder, click the <strong>Manage Folders</strong> link from the Received Documents Screen.</li><br>
	<li>Select the folder you want to rename from the list. Then click its corresponding <strong>rename</strong> link.<br>
		<p align="center"><img src="images/ManageFoldersB1.png" width="400" /></p>
	</li>
	<li>The folders details will load in the textbox, rename the folder then click <strong>Rename</strong> button. Click Cancel button otherwise.  Its folder name will now be edited in the list below.<br>
		<p align="center"><img src="images/ManageFoldersB2.png" width="400" /></p>
	</li>
</ul>
</p>

<p align="right"><a href="manageFolders-Records-Officer.php">&uarr; Back To Top</a></p>

<a name="DF"></a>
<p class="big"><strong>Delete Folder</strong></p>

<p>
<ul>
	<li>To delete folder, click the <strong>Manage Folders</strong> link from the Received Documents Screen.</li><br>
	<li>Select the folder you want to delete from the list. Then click its corresponding <strong>delete</strong> link.<br>
		<p align="center"><img src="images/ManageFoldersC1.png" width="400" /></p>
	</li>
	<li>The folders details will load in the textbox, then click <strong>Delete</strong> button. Click Cancel button otherwise.  Its folder name will now be deleted from the list below.</li><br>
</ul>
</p>

<p align="right"><a href="manageFolders-Records-Officer.php">&uarr; Back To Top</a></p>
