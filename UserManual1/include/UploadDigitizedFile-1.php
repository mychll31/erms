<p class="big"><strong>Upload a Digitized File</strong></p>

<p>
<ul>
	<li>From the Document List tab, click the <strong>Upload Document</strong> icon.<br>
	<p align="center"><img src="images/UploadDigitizedFile.png" width="400" /></p>
	</li>
	<li>Refer to <a href="manageFiles-Records-Officer.php">Manage Files</a> section of this online manual.</li><br>
</ul>
</p>