<p class="xbig"><strong>LIBRARIES</strong></p>

<p align="center"><img src="images/Libraries1.png" width= "450"/></p>

<p>Clicking on the <strong>Libraries</strong> option from the <strong>Folders</strong>	 menu serves as the storage of information which can be used as options for users when accessing ERMS, especially when adding an incoming or outgoing document. It has the following sections:</p>

<p>
<ul>
	<li><a href="office-Records-Officer.php">Office</a></li><br>
	<li><a href="cabinet-Records-Officer.php">Cabinet</a></li><br>
	<li><a href="documentType-Records-Officer.php">Document Type</a></li><br>
	<li><a href="actionRequired-Records-Officer.php">Action Required</a></li><br>
	<li><a href="actionTaken-Records-Officer.php">Action Taken</a></li><br>
	<li><a href="backUp-Records-Officer.php">Back Up</a></li><br>
	<li><a href="manageCustodian-Records-Officer.php">Manage Custodian</a></li><br>
	<li><a href="groups-Records-Officer.php">Groups</a></li><br>
	<li><a href="userAccount-Records-Officer.php">User Account</a></li><br>
</ul>
</p>
