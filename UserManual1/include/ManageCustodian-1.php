<p class="xbig"><strong>MANAGE CUSTODIAN</strong></p>

<p align="center"><img src="images/ManageCustodian.png" width= "400"/></p>

<p>
<ul>
	<li>To add a record, select the <strong>Office</strong>, <strong>Office Group</strong> and <strong>Custodian</strong> from the drop-down list.  If you want to make the Custodian an Administrator, tick the <strong>Admin</strong> checkbox as an indication. You may click on the Clear button <img src="images/Office1.png" /> to erase the previously inputted information. Otherwise, click on the Add button <img src="images/Office2.png" /> to save the record. A confirmation message saying that the record has been added will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To edit a record, click on the check box at the left of the corresponding record to be edited, then click the Edit icon <img src="images/Office3.png" />. The information will load in the textboxes, perform the necessary changes then click on Save button <img src="images/Office4.png" /> to update the record. A confirmation message saying that the action is successful will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To delete a record, click on the checkbox at the left of the corresponding record to be deleted, then click the Delete icon <img src="images/Office5.png" />. A confirmation message saying that the action is successful will be displayed. Otherwise, an error message will be shown.</li><br>
	<li>To reload the grid, simply click the Reload Grid icon <img src="images/Office8.png" />.</li><br>
	<li>To specify the number of records you want to be displayed at once, click the drop-down list and select an option.<br>
	<p align="center"><img src="images/Office9.png" width="350" /></p></li>
</ul>
</p>

<p align="right"><a href="manageCustodian-Records-Officer.php">&uarr; Back To Top</a></p>