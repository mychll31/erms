<p class="xbig"><strong>LIBRARIES</strong></p>

<p align="center"><img src="images/Libraries1.png" width= "450"/></p>

<p>Clicking on the <strong>Libraries</strong> option from the <strong>Folders</strong>	 menu serves as the storage of information which can be used as options for users when accessing ERMS, especially when adding an incoming or outgoing document. It has the following sections:</p>

<p>
<ul>
	<li><a href="office-Records-Custodian.php">Office</a></li><br>
	<li><a href="cabinet-Records-Custodian.php">Cabinet</a></li><br>
</ul>
</p>
