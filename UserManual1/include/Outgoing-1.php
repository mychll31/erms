<p class="xbig"><strong>OUTGOING</strong></p>

<p align="center"><img src="images/Outgoing1.png" width="400" /></p>

<p>Outgoing refers to the documents originate from the office (DOST-CO) to other agencies. When adding a record of an outgoing document, the following information is needed:</p>

<p>
<ul>
	<li><strong>Document ID</strong> � system-generated auto-incrementing document identification code.</li><br>
	<li><strong>Document Date</strong> � date indicated on the document.</li><br>
	<li><strong>Document Type</strong> � type of the document.</li><br>
	<li><strong>Deadline</strong> � the date when the document should complete the necessary actions.</li><br>
	<li><strong>Reference ID</strong> � control number of the document for easy referencing.</li><br>
	<li><strong>Doc. No.</strong> � number of the document assigned by the office.</li><br>
	<li><strong>Subject</strong> � subject matter of the document. This information is usually in the printed copy of the document.</li><br>
	<li><strong>Origin</strong> � source of the document. This may refer to an employee, office or agency.</li><br>
	<li><strong>Signatory</strong> � the one who sent the document.</li><br>
	<li><strong>Remarks</strong> � additional information and other necessary details regarding the record.</li><br>
	<li><strong>File Container</strong> � information on the physical location of the printed document.</li><br>
	<li><strong>Confidential</strong> � a checkbox indicating that the document is considered as confidential.</li><br>
</ul>
</p>

<p align="right"><a href="outgoing-Records-Officer.php">&uarr; Back To Top</a></p>

<p class="big"><strong>Add an Outgoing Document</strong></p>

<p>
<ul>
	<li>To add a new outgoing document, click the <strong>Outgoing</strong> tab. You will see the Add Document form.<br>
		<p align="center"><img src="images/Outgoing2.png" width="400" /></p>
	</li>
	<li>Supply the necessary document information. The <strong>Document ID</strong> is a system-generated auto-numbered code. No need to edit this field.</li><br>
	<li>You may edit the <strong>Document Date</strong> by typing the date on the textbox (in the following format: <em>YYYY-MM-DD</em>) or click the calendar icon (located at the right side of the Document Date textbox).</li><br>
	<li>After doing so, you may choose the <strong>Document Type</strong> from the look-up feature. You can do this by typing some characters in the textbox, then the look-up feature will automatically generate a list of words matching the word you typed.</li><br>
	<li>You may edit the <strong>Deadline</strong> by typing the date on the textbox (in the following format: <em>YYYY-MM-DD</em>) or click the calendar icon (located at the right side of the Deadline textbox).</li><br>
	<li>Type the <strong>Reference ID</strong> on the textbox provided.</li><br>
	<li>Provide the <strong>Doc. No.</strong> (e.g. A.O. 023).</li><br>
	<li>Type the <strong>Subject</strong> in the textbox provided.</li><br>
	<li>Choose the document <strong>Origin</strong> from the look-up feature.</li><br>
	<li>Type in the name of the <strong>Signatory</strong>.</li><br>
	<li>Type in the <strong>Remarks</strong> (if any).</li><br>
	<li>Choose the <strong>File Container</strong> from the drop-down list.</li><br>
	<li>If the document is <strong>Confidential</strong>, tick the checkbox as an indication.</li><br>
	<li>When done, click <strong>Save</strong> button <img src="images/Outgoing3.png" /> to add the record into the database. To abort adding of new document, click <strong>Cancel</strong> button <img src="images/Outgoing4.png" />.</li><br>
</ul>
</p>

<p align="right"><a href="outgoing-Records-Officer.php">&uarr; Back To Top</a></p>
