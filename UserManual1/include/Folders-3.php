<p class="xbig"><strong>FOLDERS</strong></p>

<p>Folders serve as the �main menu� of the features and functionalities of the system.</p>

<p align="center"><img src="images/Folders.png" width= "450" /></p>

<p class="big"><strong>Received Documents</strong></p>

<p>Clicking on the <strong>Received Documents</strong> folder will allow the user to view the list of all documents regardless of its type or status. It includes the following information: <em>Action Required</em>, <em>Sender</em>, <em>Document Identification</em> (DOCID), <em>Subject</em>, <em>Status</em>, <em>Document Type</em> (DOC TYPE), <em>Document Date</em> (DOC DATE), and <em>Origin</em>.</p>

<p class="notebox"><strong>NOTE:</strong> <em>Since this is an employee�s account, documents are automatically received. However, those records shown in bold font style refers to the documents which were not yet viewed by the user.</em></p>

<p align="center"><img src="images/ReceivedDocuments.png" width= "450" /></p>

<p align="right"><a href="folders-Employee.php">&uarr; Back To Top</a></p>
