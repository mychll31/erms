<p class="big"><strong>Add Action Reply to a Received Document</strong></p>

<p>
<ul>
	<li>Go to the specific documentís Document Details tab. Click the <strong>Update Action</strong> tab.<br>
	<p align="center"><img src="images/AddActionReply1.png" width="400" /></p>
	</li>
	<li>Just below the Update Action form id the <strong>FOR ACTIONS</strong> table. In this table you will see the required actions on the record by the sender.<br>
	<p align="center"><img src="images/AddActionReply2.png" width="350" /></p>
	</li>
	<li>Click the <strong>Add Action Reply</strong> button <img src="images/AddReply3.png" />to make an action to the received document. A window will pop-up:<br>
	<p align="center"><img src="images/AddReply4.png" width="250" /></p>
	</li>
	<li>Select an <strong>Action Taken</strong> from the drop-down list.</li><br>
	<li>Select an <strong>Action Required</strong> from the document recipient from the drop-down list.</li><br>
	<li>Type in your <strong>Remarks</strong>, if any.</li><br>
	<li>If a reply is expected from the recipient, tick the <strong>Reply expected</strong> checkbox as an indication.</li><br>
	<li>Click <strong>Submit</strong> button <img src="images/AddReply5.png"> when done.</li><br>
	<li>You may also click the <strong>X</strong> button to revert to the previous page.</li><br>
	<li>You may also set your own deadline on the document by clicking on the <strong>Set Own Deadline</strong> icon <img src="images/AddReply6.png" />. The following window will appear:<br>
	<p align="center"><img src="images/AddReply7.png" width="250" /></p>
	</li>
	<li>You may edit by typing the <strong>Deadline</strong> on the textbox (in the following format: <em>YYYY-MM-DD</em>) or click the calendar icon (located at the right side of the corresponding textbox).</li><br>
	<li>Click <strong>Submit</strong> button <img src="images/AddReply5.png"> when done.</li><br>
</ul>
</p>

<p align="right"><a href="addActionReply-Records-Custodian.php">&uarr; Back To Top</a></p>



