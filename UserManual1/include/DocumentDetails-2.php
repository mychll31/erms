<p class="big"><strong>Document Details</strong></p>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="boxyellow">
<tr>
	<td width="50%" align="center" style="border-right:thin #FFFF00 solid"><a href="#EDI">Editing Document Information</a></td>
	<td width="50%" align="center"><a href="#AND">Adding a New Document</a></td>
</tr>
</table>

<p>Document Details shows the necessary information regarding a document.</p>

<p align="center"><img src="images/DocumentDetailsX.png" width= "400" /></p>

<a name="EDI"></a>
<p><strong>Editing Document Information</strong></p>

<p>
<ul>
	<li>To edit Document Information, click the <strong>Edit Info</strong> button <img src="images/EditInfo.png" /> (at the upper right area of the page) from the <strong>Document Details</strong> screen.<br>
	<p align="center"><img src="images/EditInfo1.png" width="400" /></p>
	</li>
	<li>You will be directed to a screen that displays editable information of a specific document.<br>
	<p class="notebox"><strong>NOTE:</strong> <em>Editing depends on the status of the document [Incoming or Outgoing]. Some information may not be subject to modification.</em></p>
	<p align="center"><img src="images/EditInfo2.png" width="400" /></p>
	</li>
	<li>Edit the necessary information you wish to modify. Click <strong>Update</strong> button <img src="images/UpdateButton.png" /> when done editing. If you wish to reset the form to its previous state, click <strong>Clear</strong> button <img src="images/Incoming3.png" />. To cancel editing of the document, click <strong>Cancel</strong> button <img src="images/Outgoing4.png" />.</li><br>
</ul>
</p>

<p align="right"><a href="documentDetails-Records-Custodian.php">&uarr; back To Top</a></p>

<a name="AND"></a>
<p><strong>Adding a New Document</strong></p>

<p>
<ul>
	<li>To add new document of the same status (Incoming or Outgoing), click the Add New button <img src="images/UploadDigitizedFiles1.png" /> (at the upper right area of the page) from the <strong>Document Details</strong> screen.<br>
	<p align="center"><img src="images/AddNew1.png" width="400" /></p>
	</li>
	<li>You will be directed to a screen that displays editable information of a specific document.<br>
	<p class="notebox"><strong>NOTE:</strong> <em>Adding depends on the status of the document [Incoming or Outgoing]. Some information are pre-defined.</em></p>
	<p align="center"><img src="images/AddNew2.png" width="400" /></p>
	</li>
	<li>Supply the necessary document information. Click <strong>Save</strong> button <img src="images/Outgoing3.png" /> to add the record into the database. If you wish to reset the form to its previous state, click <strong>Clear</strong> button <img src="images/Incoming3.png" />. To abort adding of new document, click <strong>Cancel</strong> button <img src="images/Outgoing4.png" />.</li><br>
</ul>
</p>

<p align="right"><a href="documentDetails-Records-Custodian.php">&uarr; back To Top</a></p>
