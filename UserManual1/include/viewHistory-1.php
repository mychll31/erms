<p class="big"><strong>View History</strong></p>

<p>View History contains the information on document movement.</p>

<p align="center"><img src="images/ViewHistoryA.png" width= "400" /></p>

<p>Document Movement contains the following:
<ul>
	<li><strong>Current Reference</strong> refers to the most recent update made on the document.</li>
	<li><strong>Initial Reference</strong> refers to the starting point of the document movement.</li>
</ul>
</p>

<p>The document movement, which is arranged according to date and time (with the latest first), can be viewed on the right side.</p>

