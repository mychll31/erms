<p class="big"><strong>View Document Information</strong></p>

<p>
<ul>
	<li>To view Document Information, simply click the <strong>DOCID</strong> link of the specific document from the Document List. This will redirect to the specific documentís Document Details.<br>
	<p align="center"><img src="images/ViewDocumentInfo.png" width="400" /></p>
	</li>
</ul>
</p>



