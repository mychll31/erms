<p class="xbig"><strong>INTRODUCTION</strong></p>

<p>DOST  CO ERMS is a web-based system that automates management of record within DOST  Central Office on a centralized approach. Users can access it instantaneously without the need for installation as long as there is an available internet connection.</p>

<p>The system is composed of the following modules: Records Officer, Records Custodian and Employee. Access to the system functions is limited. Users are only able to carry out functions which are permitted by the user role(s) to which they are assigned. </p>

<p>A users login information (Username and Password) from DOST  CO HRMIS will be used to gain access to DOST  CO ERMS.</p>

<p>DOST  CO ERMS shall produce error messages which will be meaningful and appropriate, and shall offer immediate prompts for actions to resolve the error(s) whenever possible.</p>

<p>This User Manual shall provide information on how to use the system accordingly.</p>

<p align="center"><img src="images/001.png" width= "450" /></p>
 
