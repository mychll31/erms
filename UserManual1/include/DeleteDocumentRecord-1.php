<p class="big"><strong>Delete a Document Record</strong></p>

<p>
<ul>
	<li>From the Document List tab, click the <strong>Delete</strong> icon.<br>
	<p align="center"><img src="images/DeleteDocumentRecord.png" width="400" /></p>
	</li>
	<li>The document will then be moved to the <strong>Trash</strong> Folder.</li><br>
</ul>
</p>