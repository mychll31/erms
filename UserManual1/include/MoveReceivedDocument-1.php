<p class="big"><strong>Move Received Document</strong></p>

<p>
<ul>
	<li>From the Received Documents screen, select the document you want to move to another folder by ticking the checkbox (located at the left part of the document record). Then click the Move Document icon <img src="images/MoveReceivedDocument1.png" /> (located at the top of the table/grid).<br>
	<p align="center"><img src="images/MoveReceivedDocument2a.png" width="400" /></p>
	</li>
	<li>The following window will appear:<br>
	<p align="center"><img src="images/MoveReceivedDocument3.png" width="250" /></p>
	</li>
	<li>Select folder to move the document to from the drop-down list. Then click <strong>Move</strong> button to move document. To abort moving the document, click the <strong>X</strong> button instead.
	</li>
	<li>You will then be directed to the documentís Document Details.</li>
</ul>
</p>



