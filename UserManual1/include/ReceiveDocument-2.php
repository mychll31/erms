<p class="big"><strong>Receive a Document</strong></p>

<p>
<ul>
	<li>To receive a document, click on the Document ID (<strong>DOCID</strong>) link of the specific document.<br>
	<p align="center" class="notebox"><strong>NOTE:</strong> <em>Only those documents displayed with bold font style can be received.</em></p>
	<p align="center"><img src="images/ReceiveDocumentX.png" width="400" /></p>
	</li>
	<li>The following window will appear:<br>
	<p align="center"><img src="images/ReceiveDocument2.png" width="250" /></p>
	</li>
	<li>Type your name on the <strong>Received By</strong> textbox. Then click <strong>Receive</strong> button to view the document record. To abort receiving the document, click the <strong>X</strong> button instead.</li><br>
	<li>You will then be directed to the documentís Document Details.</li>
</ul>
</p>



