<p class="xbig"><strong>SEARCH</strong></p>

<p align="center"><img src="images/Search3.png" width= "500"/></p>

<p class="big"><strong>Advanced Search</strong></p>

<p>Advanced search provides a more comprehensive searching of records that matched the search criteria. Click on the <strong>Advanced Search</strong> link located at the upper-right side of the screen. You will be directed to the Advanced Search form.</p>

<p align="center"><img src="images/SearchAdvanced.png" width="400"/></p>

<p>
<ul>
	<li>Select the specific metadata from the drop-down lists: <em>Subject</em>, <em>Document Type</em>, <em>Status</em>, and <em>Deadline</em>. Type the search keyword on the textbox provided. Click the Search button <img src="images/SearchAdvanced1.png"/> when done.</li>
</ul>
</p>

<p align="right"><a href="search-Employee.php">&uarr;  Back To Top</a></p>
