<p class="big"><strong>Flag Received Document</strong></p>

<p>
<ul>
	<li>From the Received Documents screen, select the document you want to flag by ticking the checkbox (located at the left part of the document record). Then click the Flag icon <img src="images/FlagDocument1.png" /> (located at the top of the table/grid).<br>
	<p align="center"><img src="images/FlagDocumentA2.png" width="400" /></p>
	</li>
	<li>A flag image shall suffice at the left side of the document chosen, beside the checkbox.<br>
	<p align="center"><img src="images/FlagDocument3.png" width="350" /></p>
	</li>
</ul>
</p>



