<? session_start(); ?>
<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script src="facebox/facebox.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$('a[rel*=facebox]').facebox({
		loading_image : '/facebox/loading.gif',
		close_image   : '/facebox/closelabel.gif'
		});
		
	});

	function disposeDialogMultiple()
	{
		var custdocs = '';
		if(document.frmDisposals.chk.length == undefined)
			{
			if(document.frmDisposals.chk.checked)	
				custdocs = document.getElementById('t_strDocId0').value;
			}
		else
			{
			for(var i=0; i < document.frmDisposals.chk.length; i++){
				if(document.frmDisposals.chk[i].checked)
					{
					var id = 't_strDocId'+i;
					//if(i>1)
						custdocs = custdocs + document.getElementById(id).value + ';';
					//else
						//custdocs = document.getElementById(id).value;	
					}	
			}
		}
		
	if(custdocs!="")
		{	
		//document.getElementById('docid').innerHTML=custdocs;
		//document.getElementById('moveid').innerHTML='".$rs[$t]['documentId']."'; 
		//document.getElementById('fid').innerHTML=document.getElementById('t_intFolderID').value;
		//$('#movedialog2').dialog('open');
		var url = 'showDisposeDialog.php?mode=show&docid='+custdocs;
		jQuery.facebox({ ajax: url });
		}
	}

	function disposeDialog(docid)
		{
		var url = 'showDisposeDialog.php?mode=show&docid='+docid;
		jQuery.facebox({ ajax: url });	
		}

</script>
<? 
include_once("class/MySQLHandler.class.php");
include_once("class/Dispose.class.php");
$objDispose = new Dispose;

if($_GET['mode']=="show")
{

	
$t_dtmNow = date("Y-m-d");
$arrDate = explode("-",$t_dtmNow);
$addURLString = '#';
$limit = 15;
if($page == "") 
	$page = 0;
else if($page<>"")
	$page = $_GET['page'];
if($_GET['ascdesc']=="ASC")
	$ascdesc = "DESC";
else
	$ascdesc = "ASC";
$rsCount = $objDispose->getForDisposalList($_GET['t_strSearch']);
$total_pages = count($rsCount);
$rs = $objDispose->getForDisposalList($_GET['t_strSearch'], $page, $limit, $_GET['order'], $ascdesc);
if(count($rs))
	{
	echo "<table width=100% align=center border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td align=75% align=left><ul id='icons' class='ui-widget ui-helper-clearfix'>
			<li class='ui-state-default ui-corner-all'>
				<span class='ui-icon ui-icon-transferthick-e-w' title='Dispose Document' onClick='disposeDialogMultiple();'>
				</span>
			</li>
		  </ul></td>
		<td align=right width=10%><input type='textbox' id='t_strSearch' value='".$_GET['t_strSearch']."'></td>
		<td align=left width=15%>
			<ul id='icons' class='ui-widget ui-helper-clearfix'>
				<li class='ui-state-default ui-corner-all' style='height:11px;'  onClick=\"getData('showDisposals.php?mode=show&t_strSearch='+document.getElementById('t_strSearch').value,'disposals');\">
				<span class='ui-icon ui-icon-search' title='Search'></span><span style='font-size:11px'>Search&nbsp;&nbsp;
				</span>
				</li>
			</ul>
		</td>
	</tr>
	</table>";	
	/*
	echo "<ul id='icons' class='ui-widget ui-helper-clearfix'>
			<li class='ui-state-default ui-corner-all'>
				<span class='ui-icon ui-icon-transferthick-e-w' title='Dispose Document' onClick='disposeDialogMultiple();'>
				</span>
			</li>
		  </ul>";*/
	
	echo '<form name="frmDisposals" method="post">
		<table cellpadding="0" cellspacing="0" align="center" class="listings" width="100%" border="0">
			<tr class="listheader">
				<td width=1%>
					<input type=checkbox onClick=\'checkedAll("frmDisposals");\'>
				</td>
				<td width=18%>DOCID</td>
				<td>SUBJECT</td>
				<td width=5% align=center>DOC STATUS</td>
				<td width=10%>DOC TYPE</td>
				<td width=10%>DOC DATE</td>
				<td width=8%>ORIGIN</td>';
	// begin disposal for admin
	if($_SESSION['userType'] == 1)
		echo '<td width=3%>&nbsp;</td>';
	echo '</tr>';
	// end disposal for admin
	$i=0;
	for($t=0;$t<count($rs);$t++)
		{
		//echo $rs[$t]['documentYear']+$rs[$t]['retentionPeriod']."=".$arrDate[0]."<br>"; 
		//if(($rs[$t]['documentYear']+$rs[$t]['retentionPeriod'])==$arrDate[0])
		if(count($rs)>0)
			{
			$intDocStatus = $objDispose->getDocumentStatus($rs[$t]['status']);
			$intDocType = $objDispose->getDocumentType($rs[$t]['documentTypeId']);
			$strOfficeName = $objDispose->getOfficeName($rs[$t]['originId'],$rs[$t]['officeSig'],$rs[$t]['status'],$rs[$i]['originUnit']);
			$strPageLink = $intDocStatus=="inc"?"showIncoming.php":"showOutgoing.php";
			$t_strBgcolor = $objDispose->altRowBgColor($i);
			echo '<tr onMouseOver="this.bgColor=\'#FFFFCC\'" onMouseOut="this.bgColor=\''.$t_strBgcolor.'\'" bgcolor="'.$t_strBgcolor.'">';
			echo '<td><input type="checkbox" name="chk" id="chk" value="'.$rs[$t]['historyId'].'"><input type="hidden" name="t_strDocId'.$t.'" id="t_strDocId'.$t.'" value="'.$rs[$t]['documentId'].'"></td>';			
			echo "<td align='center'>&nbsp;<a href='#' onClick='getData(\"".$strPageLink."?mode=view&id=".$rs[$t]["documentId"]."\",\"intra\");'>".str_replace(strtoupper($_GET['t_strSearch']),"<font color=red><strong>".strtoupper($_GET['t_strSearch'])."</strong></font>",strtoupper($rs[$t]['documentId']))."</a></td>";
			echo "<td align='center'>&nbsp;".str_replace(strtoupper($_GET['t_strSearch']),"<font color=red><strong>".strtoupper($_GET['t_strSearch'])."</strong></font>",strtoupper($rs[$t]['subject']))."</td>
				  <td align='center'>&nbsp;".$intDocStatus."</td>
				  <td align='center'>&nbsp;".$intDocType."</td>
				  <td align='center'>&nbsp;".$rs[$t]['documentDate']."</td>
				  <td align=center>&nbsp;".$strOfficeName."</td>
				  <td align='center'>";
			// begin disposal for admin
			if($_SESSION['userType'] == 1)
				echo "<span class='ui-icon ui-icon-transferthick-e-w' title='Dispose Document' onClick=\"disposeDialog('".$rs[$t]['documentId']."');\" onmouseover=\"this.style.cursor='pointer';\"></span></td>";
			echo "</tr>";
			// end disposal for admin

			// COMMENTED echo "<span class='ui-icon ui-icon-transferthick-e-w' title='Dispose Document' onClick=\"disposeDialog('".$rs[$t]['documentId']."');\" onmouseover=\"this.style.cursor='pointer';\"></span></td></tr>";
			
			}
			$i++;
		}
	echo 	'</table></form><br>';		
	echo $objDispose->showPagination($addURLString, $page, $total_pages, $limit,"disposals","showDisposals.php?mode=show");
	}
	else
		echo "<br><Br><br>No Document for Disposal<br><Br><Br>";
}



if($_GET['mode']=="dispose")
{
	$arr = explode(";",$_GET['id']);
	for($i=0;$i<count($arr);$i++)
		if(trim($arr[$i])!="")
			$objDispose->disposeDocument($arr[$i], $_GET['rmk']);
	echo "<script language='javascript'>getData('showDisposals.php?mode=show','disposals');</script>";
}
if($_GET['mode']=="viewid")
{
	echo $_GET['id'];
}
?>